# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class FC_PCO_Logic_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
   	self.theUnicosProject = self.thePlugin.getUnicosProject()
	self.thePlugin.writeInUABLog("initialize in Jython for FC_PCO_Logic.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for FC_PCO_Logic.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for FC_PCO_Logic.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'

   def process(self, *params):
	deviceVector = params[0]
	theXMLConfig = params[1]
	genGlobalFilesForAllSections = params[2].booleanValue() # Comes from "Global files scope" dropdown on Wizard. true = All sections. false = Selected sections.
	self.thePlugin.writeInUABLog("processInstances in Jython for Compilation_Logic.")
	self.thePlugin.writeSiemensLogic('''FUNCTION FC_PCO_LOGIC : VOID
TITLE = 'FC_PCO_LOGIC'
//
// Call all PCO sections
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic'
FAMILY: 'UNICOS'
BEGIN''')
	
	
	for theCurrentPco in deviceVector :
		thePcoSections = theCurrentPco.getSections()
		thePcoDependentSections = theCurrentPco.getDependentSections()
		theDependentDevices = theCurrentPco.getDependentDevices()
		theCurrentPcoName = theCurrentPco.getDeviceName()
		self.thePlugin.writeSiemensLogic('''// Calling the PCO Sections for '''+theCurrentPcoName)
		for theCurrentSection in thePcoSections:
			if theCurrentSection.getGenerateSection() or genGlobalFilesForAllSections:
				theCurrentSectionName = theCurrentSection.getFullSectionName()
				self.thePlugin.writeSiemensLogic(theCurrentSectionName+'''();''')
		self.thePlugin.writeSiemensLogic('''// Calling the DL of the Dependent Devices for '''+theCurrentPcoName)
		for theCurrentDependentDevice in theDependentDevices:
			theDependentSections = theCurrentDependentDevice.getDependentSections()
			if theCurrentPcoName.lower() == "no_master":
				for section in theDependentSections:
					if section.getGenerateSection() or genGlobalFilesForAllSections:
						theCurrentSectionName = section.getFullSectionName()
						self.thePlugin.writeSiemensLogic(theCurrentSectionName+'''();''')
			else:
				for section in theDependentSections:
					if section.getGenerateSection() or genGlobalFilesForAllSections:
						theCurrentSectionName = section.getFullSectionName()
						self.thePlugin.writeSiemensLogic(theCurrentSectionName+'''();''')
			
	
	
	
	self.thePlugin.writeSiemensLogic('''	
END_FUNCTION''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for FC_PCO_Logic.")


   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for FC_PCO_Logic.")
		
