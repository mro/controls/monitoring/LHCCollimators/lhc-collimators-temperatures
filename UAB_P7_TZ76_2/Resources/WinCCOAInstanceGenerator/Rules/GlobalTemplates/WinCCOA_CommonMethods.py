# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
##
# This file contains all the common functions for the WinCCOA 
##

from java.util import Vector
from java.util import ArrayList

#==========================================================================

def getExpertName(instance):
    """
    This function returns the expert name (if available) from a given instance.
    If the expert name is not defined, then will return the object name.
    
    The arguments are:
    @param instance: object instance

    This function returns:
    @return: name: the expert name (or name) of the object for use in WinCCOA db file
    """ 

    if instance.getAttributeData("DeviceIdentification:Expert Name") != "":
        return instance.getAttributeData("DeviceIdentification:Expert Name")
    else:
        return instance.getAttributeData("DeviceIdentification:Name")
        
#==========================================================================

def appendDeviceLinkListFromSpec(DeviceLinkListSpec,DeviceLinkList,thePlugin,strAllDeviceTypes):
    """
    This function returns the DeviceLinkList with the device links from the spec appended.
    Separated by SEPARATOR
    For each device link in the spec, will look up expert name if it exists.
    
    The arguments are:
    @param DeviceLinkListSpec: comma-separated list of device links from the spec
    @param DeviceLinkList: comma-separated list of device links for the object
    @param thePlugin: for access to methods
    @param strAllDeviceTypes: string of all device types

    This function returns:
    @return: DeviceLinkList: comma-separated list of device links
    """ 
    L_DeviceLinkListSpec = DeviceLinkListSpec.split(",")
    if DeviceLinkListSpec != "":
        DeviceLinkList = DeviceLinkList + ",SEPARATOR"
        for devlink in L_DeviceLinkListSpec:
            DeviceLinkList = DeviceLinkList + "," + thePlugin.getLinkedExpertName(devlink,strAllDeviceTypes)
    return DeviceLinkList

#==========================================================================

def getPvssAlarmAckDigital(specAlarmAck, specAlarmOnState):
    """
    This function returns the AlarmAck property given spec specAlarmAck input and specAlarmOnState
    for a digital device (DI, DO)
    
    @param specAlarmAck: SCADADeviceAlarms:Alarm Config:Auto Acknowledge from the spec (blank, true, false)
    @param specAlarmOnState: SCADADeviceAlarms:Binary State:Alarm On State from the spec (blank, true, false)

    @return: pvssAlarmAck: Alarm Ack property for pvss importation line
    """ 
    specAlarmAck = specAlarmAck.strip().lower()
    specAlarmOnState = specAlarmOnState.strip()
    if specAlarmAck == "":
        if specAlarmOnState == "": # condition when there's no pvss alarm -> ack property can't be stored
            return "false"
        else:
            return "true"
    elif specAlarmAck == "false":
        return "true"
    elif specAlarmAck == "true":
        return "false"

#==========================================================================

def getPvssAlarmAckAnalog(specAlarmAck, specHHLimit, specLLLimit):
    """
    This function returns the AlarmAck property given spec specAlarmAck input and specHHLimit, specLLLimit
    for an analog device (AI, AIR, AO, AOR)
    
    @param specAlarmAck: SCADADeviceAlarms:Alarm Config:Auto Acknowledge from the spec (blank, true, false)
    @param specHHLimit: SCADADeviceAlarms:Analog Thresholds:HH Alarm from the spec 
    @param specLLLimit: SCADADeviceAlarms:Analog Thresholds:LL Alarm from the spec

    @return: pvssAlarmAck: Alarm Ack property for pvss importation line
    """ 
    specAlarmAck = specAlarmAck.strip().lower()
    if specAlarmAck == "": #if not defined, only Ack if 
        if specHHLimit == "" and specLLLimit == "": # condition when there's no pvss alarm -> ack property can't be stored
            return "false"
        else:
            return "true"
    elif specAlarmAck == "false":
        return "true"
    elif specAlarmAck == "true":
        return "false"

#==========================================================================

def getPvssAlarmActiveAnalog(specAlarmMasked, specHHLimit, specHLimit, specLLimit, specLLLimit):
    """
    This function returns the AlarmActive property given spec specAlarmMasked input and specHHLimit, specLLLimit
    for an analog device (AI, AIR, AO, AOR)
    
    @param specAlarmMasked: SCADADeviceAlarms:Alarm Config:Masked from the spec (blank, true, false)
    @param specHHLimit: SCADADeviceAlarms:Analog Thresholds:HH Alarm from the spec 
    @param specHLimit: SCADADeviceAlarms:Analog Thresholds:H Warning from the spec 
    @param specLLimit: SCADADeviceAlarms:Analog Thresholds:L Warning from the spec 
    @param specLLLimit: SCADADeviceAlarms:Analog Thresholds:LL Alarm from the spec

    @return: pvssAlarmActive: Alarm Active property for pvss importation line
    """ 
    specAlarmMasked = specAlarmMasked.strip().lower()
    #AlarmMasked --> NOT Alarm Active
    if specAlarmMasked == "true":
        return "false"
    elif specAlarmMasked == "false":
        return "true"
    else:
        if specHHLimit == "" and specHLimit == "" and specLLimit == "" and specLLLimit == "": # condition when there's no pvss alarm -> cannot be active
            return "false"
        else:
            return "true"
