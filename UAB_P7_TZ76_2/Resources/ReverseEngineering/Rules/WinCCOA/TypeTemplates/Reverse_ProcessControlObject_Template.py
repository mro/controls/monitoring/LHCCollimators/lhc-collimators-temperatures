# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
import Reverse_Generic_Template
reload(Reverse_Generic_Template)
import Reverse_GlobalFunctions_Template
reload(Reverse_GlobalFunctions_Template)
    
class ProcessControlObject_Template(Reverse_Generic_Template.Generic_Template): 
    theDeviceType = "ProcessControlObject"
    
    def processInstance(self, instance, generatedInstance):
        key,value = self.getDescriptionWithoutElectricalDiagram(instance) #overwrite result from generic function
        generatedInstance.put(key,value)
        
        key,value = self.getPCOName(instance)
        generatedInstance.put(key,value)
    
        for i in range(1,9):
            key,value = self.getModeName(instance,i)
            generatedInstance.put(key,value)
            key,value = self.getModeAllowance(instance,i)
            generatedInstance.put(key,value)      
            
        key,value = self.getMaster(instance)
        generatedInstance.put(key,value)    
        
        key,value = self.getMaskEvent(instance)
        generatedInstance.put(key,value)    
        
        key,value = self.getManualRestartAfterFullStop(instance)
        generatedInstance.put(key,value)               
