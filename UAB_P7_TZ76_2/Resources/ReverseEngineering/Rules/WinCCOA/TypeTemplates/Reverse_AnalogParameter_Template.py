# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
import Reverse_Generic_Template
reload(Reverse_Generic_Template)
import Reverse_GlobalFunctions_Template
reload(Reverse_GlobalFunctions_Template)
    
class AnalogParameter_Template(Reverse_Generic_Template.Generic_Template): 
    theDeviceType = "AnalogParameter"
    
    def processInstance(self, instance, generatedInstance):
        
        key,value = self.getDescriptionWithoutElectricalDiagram(instance) #overwrite result from generic function
        generatedInstance.put(key,value)
        
        key,value = self.getUnit(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getFormat(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getDefaultValue(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getMaskEvent(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getArchiveModeWithTimeFilter(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getTimeFilter(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getDriverDeadbandValue(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getDriverDeadbandType(instance)
        generatedInstance.put(key,value)

        key,value = self.getRangeMin(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getRangeMax(instance)
        generatedInstance.put(key,value)
    
