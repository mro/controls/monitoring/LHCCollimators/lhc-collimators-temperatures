# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin	#REQUIRED
from research.ch.cern.unicos.core import CoreManager #REQUIRED
from java.util import ArrayList
import sys

class DeviceFormat_Template(IUnicosTemplate):    
	thePlugin = 0
	theUnicosProject = 0
	theDeviceType = "WinCCOA DeviceFormat"

	def initialize(self):
		self.thePlugin = APlugin.getPluginInterface()
		self.theUnicosProject = self.thePlugin.getUnicosProject()
		self.thePlugin.writeInUABLog("initialize in Jython for WinCCOA DeviceFormat.")

	def check(self):
		self.thePlugin.writeInUABLog("check in Jython for WinCCOA DeviceFormat.")

	def begin(self):
		self.thePlugin.writeInUABLog("begin in Jython for WinCCOA DeviceFormat.")

	def process(self, *params):  
		self.thePlugin.writeInUABLog("process in Jython for WinCCOA DeviceFormat.")
		templates = self.thePlugin.winnCCOAUtilities.getTemplates()
		instanceHeader = self.thePlugin.winnCCOAUtilities.getInstanceHeader()
		for dType in templates:
			self.thePlugin.writeInUABLog("Reading definition for: " + dType)
			#templates[dType] = templates[dType].replace("WinCCOA_", "")
			for tmpPath in self.thePlugin.winnCCOAUtilities.getSystemPaths():
				sys.path.append(tmpPath)

			deviceTypeModule = __import__(templates[dType] + "_Template", globals={}, locals={}, fromlist=[dType + "_Template"], level=-1)
			deviceTypeClass = getattr(deviceTypeModule,dType + "_Template") 
			deviceTypeInstance = deviceTypeClass()
			if (dType == "WordStatus"):
				instanceHeader["CPC_WordStatus"] = ArrayList(deviceTypeInstance.deviceFormat("WordStatus"))
				instanceHeader["CPC_Word2AnalogStatus"] = ArrayList(deviceTypeInstance.deviceFormat("Word2AnalogStatus"))
			else:
				instanceHeader["CPC_" + dType] = ArrayList(deviceTypeInstance.deviceFormat())


	def end(self):
		self.thePlugin.writeInUABLog("end in Jython for WinCCOA DeviceFormat.")

	def shutdown(self):
		self.thePlugin.writeInUABLog("shutdown in Jython for WinCCOA DeviceFormat.")   