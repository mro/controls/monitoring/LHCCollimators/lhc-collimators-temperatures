# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class AnalogInput_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for AI.")
	
   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for AI.")

   def begin(self):
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'
  
   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for AI.")
		
	# General Steps for Optimized objects:
	# 1. Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances) It's called DB_Type
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from DeviceTypeDefinition.xml created by TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5. Create the Type_Error DB if "Diagnostic" is required
	# 6. Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 7. Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#	 We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	# This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	Diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	
	# Step 1: Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances)

	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Analog Input DB Creation file: UNICOS application
''')
	self.thePlugin.writeInstanceInfo('''
DATA_BLOCK DB_AI CPC_FB_AI
BEGIN
END_DATA_BLOCK
''')
	



	# Step 3: Create all the needed Structures from the TCT	
	
	# UDT for DB_AI_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE AI_ManRequest
TITLE = AI_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isCommVerification = attribute.getIsCommunicated()
				if isCommVerification:
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')



	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE AI_bin_Status
TITLE = AI_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE AI_event
TITLE = AI_event
//
// parameters of AI Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				# isEventAttribute = attribute.getIsEventAttribute()
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE AI_ana_Status
TITLE = AI_ana_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributeName = attribute.getAttributeName()
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_AI_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_AI_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_AI_ManRequest
TITLE = DB_AI_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    AI_Requests : ARRAY [1..'''+instanceNb+'''] OF AI_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')

	# Step 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 
	
	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_AI Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_AI
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type AI
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_AI : INT := '''+str(NbType)+''';
	AI_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF AI_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
BEGIN
END_DATA_BLOCK


''')	
	
	self.thePlugin.writeInstanceInfo('''
 (*Status of the AIs************************************************)
DATA_BLOCK DB_bin_status_AI
TITLE = 'DB_bin_status_AI'
//
// Global binary status DB of AI
//
  // List of variables:''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AI_bin_Status)
''')

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF AI_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(*old Status of the AIs************************************************)
DATA_BLOCK DB_bin_status_AI_old
TITLE = 'DB_bin_status_AI_old'
//
// Old Global binaries status DB of DI
//
  // List of variables:''')
  
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AI_bin_Status)
''')

		
	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF AI_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
// -----------------------------------------------------------------------------------------------------
(*analog status of the AIs************************************************)
DATA_BLOCK DB_ana_Status_AI
TITLE = 'DB_ana_Status_AI'
//
// Global Analog status DB of AI
//
//  List of variables:
''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AI_ana_Status)
''')	

		
	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF AI_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
// -----------------------------------------------------------------------------------------------------
(*old analog status of the AIs************************************************)
DATA_BLOCK DB_ana_Status_AI_old
TITLE = 'DB_ana_Status_AI_old'
//
// old Global Analog  status DB of AI
//
  // List of variables:
''')
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AI_ana_Status)
''')		

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF AI_ana_Status;
END_STRUCT
BEGIN

END_DATA_BLOCK
''')



	# Step 5: Create the Type_Error DB if "Diagnostic" is required

	if Diagnostic == "true":
		RecordNumber = 0
		instanceNb = str(instancesVector.size())
		self.thePlugin.writeInstanceInfo('''
(*DB for IoError on Channels with OB82*)
DATA_BLOCK AI_ERROR
TITLE = AI_ERROR
//
// DB with IOError signals of AI
//
AUTHOR: AB_CO_IS
NAME: Error
FAMILY: Error
STRUCT
 IOERROR : ARRAY[1..'''+str(instanceNb)+'''] OF CPC_IOERROR;
END_STRUCT
BEGIN
''')
		for instance in instancesVector :
			# Now looping on each device instance.
			# 1 Building shortcuts
			# 2 Write code to output
			RecordNumber = RecordNumber + 1		
			PLCChannel = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1")
			TypePLCChannel = PLCChannel[0:1]
			if TypePLCChannel == "P":
				NbPIPLCChannel = PLCChannel[3:8]
				self.thePlugin.writeInstanceInfo('''
IOERROR['''+str(RecordNumber)+'''].ADDR :=$NbPIPLCChannel$.0; ''')
			elif TypePLCChannel == "I":
				NbIPLCChannel = PLCChannel[2:8]
				self.thePlugin.writeInstanceInfo('''
IOERROR['''+str(RecordNumber)+'''].ADDR :=$NbIPLCChannel$.0; ''')
		self.thePlugin.writeInstanceInfo('''
			
END_DATA_BLOCK''')



	# Step 6: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	
	self.thePlugin.writeInstanceInfo(''' 
// ----
// ----
(*AI EXEC************************************************)
FUNCTION_BLOCK FB_AI_all
TITLE = 'FB_AI_all'
//
// Call the AI treatment
//
AUTHOR: 'UNICOS'
NAME: 'Call AI'
FAMILY: 'AI'
VAR
  // Static Variables
  AI_SET: STRUCT''')
  
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''
   $Name$       : CPC_DB_AI;''')

	self.thePlugin.writeInstanceInfo('''
 END_STRUCT;  
  
  // Different variable view declaration
  AI AT AI_SET: ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_DB_AI;
  
  // Support variables
  old_status : DWORD;
  I: INT;
END_VAR
  FOR I:=1 TO '''+str(RecordNumber)+''' DO
  
	// Call IO_ACCESS_AI
	IF (AI[I].FEType <> 0) THEN
		IO_ACCESS_AI(
			FEType := AI[I].FEType, 
			PIWDef := AI[I].PiwDef, 
			InterfaceParam1 := AI[I].perAddress,
			HFPos := AI[I].HFPos);
	END_IF;
	    
    old_status := DB_bin_status_AI.StsReg01[I].StsReg01;
    old_status := ROR(IN:=old_status, N:=16);
	''')
	
	if Diagnostic == "true":
		self.thePlugin.writeInstanceInfo('''
	AI[I].IoError := AI_ERROR.IOERROR[I].Err;
	''')
	else:
		self.thePlugin.writeInstanceInfo('''
	// No diagnostic
	// AI[I].IoError := AI_ERROR.IOERROR[I].Err;''')
	

	self.thePlugin.writeInstanceInfo('''   
	
	// Calls the Baseline function
    CPC_FB_AI.DB_AI(
		Manreg01 :=  DB_AI_ManRequest.AI_Requests[I].Manreg01    // set by WinCCOA in the DB_AI 
		,MposR :=  DB_AI_ManRequest.AI_Requests[I].MposR          // set by WinCCOA in the DB_AI   
		,StsReg01 := DB_bin_status_AI.StsReg01[I].StsReg01
		,Perst := AI[I]
    );        
       
   DB_bin_status_AI.StsReg01[I].StsReg01:= DB_AI.StsReg01; 
   DB_ana_status_AI.status[I].PosSt:= AI[I].PosSt; 
   DB_ana_status_AI.status[I].HFSt:= AI[I].HFSt; 
  
	// Event
    DB_Event_AI.AI_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_AI.StsReg01[I].StsReg01;
	
  END_FOR;
END_FUNCTION_BLOCK
     ''')
	 
	 
	# Step 7: Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	self.thePlugin.writeInstanceInfo('''
(*AI devices instantation until 1000 instances************************************************)     
DATA_BLOCK DB_AI_all  FB_AI_all
//
// Instance DB for the whole AI devices
//
BEGIN
	''')
	
	 
	# PART 5
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		PMaxRan = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max"))
		PMinRan= self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min"))
		PMaxRaw = instance.getAttributeData("FEDeviceParameters:Raw Max")
		PMinRaw = instance.getAttributeData("FEDeviceParameters:Raw Min")
		PDb = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Deadband (%)"))
		Type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
		if Type == "":
			Type = '0'
			#self.thePlugin.writeWarningInUABLog("AI instance: $Name$. Undefined Type, FEEncondType = 0 is taken.")
		self.thePlugin.writeInstanceInfo('''
		
AI_SET.$Name$.PMaxRan := '''+str(PMaxRan)+''';
AI_SET.$Name$.PMinRan := '''+str(PMinRan)+''';
AI_SET.$Name$.PMaxRaw := '''+str(PMaxRaw)+''';
AI_SET.$Name$.PMinRaw := '''+str(PMinRaw)+''';
AI_SET.$Name$.PDb := '''+str(PDb)+''';
AI_SET.$Name$.index := '''+str(RecordNumber)+''';
AI_SET.$Name$.FEType:='''+str(Type)+''';''')

		if Type == "1":
			PLCChannel = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
			Init = PLCChannel[0]
			if Init == "p":
				Fin = PLCChannel[3:]
				PIWDEF = "true"
			else:
				Fin = PLCChannel[2:]
				PIWDEF = "false"
			self.thePlugin.writeInstanceInfo('''
AI_SET.$Name$.perAddress:= $Fin$;
AI_SET.$Name$.PiwDef:= '''+str(PIWDEF)+''';''')
	
	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK''')

	#Creation of a FC even for the optimized objects in order to call it in the OB1
	#This avoid the too many symbols problem of the OB1
	#The code is ready for all the object in case we need to split in several DBs one day. Note that for the moment,
	#this has sense only for the AIR and the DI where the split is working.
	NumberOfInstances = instancesVector.size()
	LimitSizeOfInstances = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", theCurrentDeviceType.getDeviceTypeName()))
	nameRepresentation = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", theCurrentDeviceType.getDeviceTypeName())
	self.thePlugin.writeInstanceInfo('''
FUNCTION FC_$nameRepresentation$ : VOID
    FB_$nameRepresentation$_all.DB_$nameRepresentation$_all();''')
	if (NumberOfInstances > LimitSizeOfInstances):
		self.thePlugin.writeInstanceInfo('''
    FB_$nameRepresentation$_all2.DB_$nameRepresentation$_all2();    // Too many '''+str(NumberOfInstances)+''' Need a second FB_$nameRepresentation$_All2''')
	self.thePlugin.writeInstanceInfo('''
END_FUNCTION''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for AI.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for AI.")
		
