# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class Analog_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for ANALOG.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for ANALOG.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for ANALOG.")
  
   def process(self, *params):
	self.thePlugin.writeInUABLog("processInstances in Jython for ANALOG.")
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	
	# General Steps for Non-Optimized objects:
	# 1. DB creation and initialization for each instance
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from the TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5: Create the Function. This function writes the input signals on each instance DB.
	# 		  For each DB we execute the FB.DB 
	# 		  And finally we pass the Status information from this instance DB to the corresponding Status DB (e.g DB_bin_status_ANALOG)
	
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	# This method is called on every Instance of the current type by the Code Generation plug-in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	
	# Step 1: DB creation and initialization for each instance
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//ANALOG DB Creation file: UNICOS application
''')

	for instance in instancesVector :
		if (instance is not None):
											
			Name = instance.getAttributeData("DeviceIdentification:Name")
			Description = instance.getAttributeData("DeviceDocumentation:Description")
			Deadband = self.thePlugin.formatNumberPLC(instance.getAttributeData ("FEDeviceParameters:Warning Deadband Value (Unit)"))
			HardwareAnalogOutput = instance.getAttributeData ("FEDeviceEnvironmentInputs:Hardware Analog Output")
			LocalDrive = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Drive")
			FeedbackAnalog = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Analog")
			FeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On")
			FeedbackOff = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Off")
			FailSafeOpen = instance.getAttributeData	("FEDeviceParameters:ParReg:Fail-Safe")
			PEnRstart = instance.getAttributeData	("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip()
			ProcessOutput = instance.getAttributeData("FEDeviceOutputs:Process Output")
			inst_ProcessOutput_Vec = theRawInstances.findMatchingInstances ("AnalogOutput, AnalogOutputReal","'#DeviceIdentification:Name#'='$ProcessOutput$'")
			StepInc = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Manual Increase Step (Unit)"))
			StepDec = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Manual Decrease Step (Unit)"))
			ManualIncSpeed = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Manual Increase Speed (Unit/s)"))
			ManualDecSpeed = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Manual Decrease Speed (Unit/s)"))
			LimitOff = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceManualRequests:Parameter Limit Off/Closed"))
			LimitOn = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceManualRequests:Parameter Limit On/Open"))
			WarningDelay = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Warning Time Delay (s)"))
			
			RangeMax = "100.0"
			RangeMin = "0.0"
			
			if (ProcessOutput <> ""):
				for inst_ProcessOutput in inst_ProcessOutput_Vec:
					RangeMin = self.thePlugin.formatNumberPLC(inst_ProcessOutput.getAttributeData("FEDeviceParameters:Range Min"))
					RangeMax = self.thePlugin.formatNumberPLC(inst_ProcessOutput.getAttributeData("FEDeviceParameters:Range Max"))
			
			# Default values
			if WarningDelay.strip() == "":
				WarningDelay = "5.0"
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined WarningDelay, WarningDelay = $WarningDelay$ is taken.")
			if Deadband.strip() == "":
				Deadband = str((float(RangeMax)-float(RangeMin))/100)
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined Deadband, Deadband = $Deadband$ is taken.")
			if ManualIncSpeed.strip() == "":
				ManualIncSpeed = str((float(RangeMax)-float(RangeMin))/10)
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined ManualIncSpeed, ManualIncSpeed = $ManualIncSpeed$ is taken.")
			if ManualDecSpeed.strip() == "":
				ManualDecSpeed = str((float(RangeMax)-float(RangeMin))/10)
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined ManualDecSpeed, ManualDecSpeed = $ManualDecSpeed$ is taken.")
			if StepInc.strip() == "":
				StepInc = str((float(RangeMax)-float(RangeMin))/100)
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined StepInc, StepInc = $StepInc$ is taken.")
			if StepDec.strip() == "":
				StepDec = str((float(RangeMax)-float(RangeMin))/100)
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined StepDec, StepDec = $StepDec$ is taken.")
			
			#PLiOn/Off default :  +/- 10% PMaxRan/PMinRan
			if LimitOn.strip() == "":
				LimitOn = str(float(RangeMax) - 0.1*(float(RangeMax)-float(RangeMin)))
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined LimitOn, LimitOn = $LimitOn$ is taken.")
			if LimitOff.strip()  == "":
				LimitOff = str(float(RangeMin) + 0.1*(float(RangeMax)-float(RangeMin)))
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined LimitOff, LimitOff = $LimitOff$ is taken.")
			
				
			# ParReg Logic				
			if (FailSafeOpen.lower()=="off/close"):
				bit8 = "0"
				bit14 = "0"
			elif (FailSafeOpen.lower()=="on/open normal output"):
				bit8 = "1"
				bit14 = "1"		
			elif (FailSafeOpen.lower()=="on/open inverted output"):
				bit8 = "1"
				bit14 = "0"
			else:
				bit8 = "0"
				bit14 = "0"
				
			if FeedbackOn == "":
				bit9='0'
			else:
				bit9='1'
				
			if FeedbackOff == "":
				bit10='0'
			else:
				bit10='1'
			
			if FeedbackAnalog == "":
				bit11='0'
			else:
				bit11='1'
			
			if LocalDrive == "":
				bit12='0'
			else:
				bit12='1'
				
			if HardwareAnalogOutput == "":
				bit13='0'
			else:
				bit13='1'
			
			if PEnRstart.lower() == "false":
				bit0='0'
				bit1='0'				
			elif (PEnRstart.lower()=="true only if full stop disappeared"):
				bit0='1'
				bit1='0'
			else:
				bit0='1'
				bit1='1'
				
		
			self.thePlugin.writeInstanceInfo('''
DATA_BLOCK '''+Name+''' CPC_FB_ANALOG
BEGIN
	
	PAnalog.PArReg :=  2#00000$bit1$$bit0$0$bit14$$bit13$$bit12$$bit11$$bit10$$bit9$$bit8$;
	PAnalog.PWDt := T#$WarningDelay$s;
	PAnalog.PWDb := $Deadband$;
	PAnalog.PMinSpd := $ManualIncSpeed$;
	PAnalog.PMDeSpd := $ManualDecSpeed$;
	PAnalog.PMStpInV := $StepInc$;
	PAnalog.PMStpDeV := $StepDec$;
	PAnalog.PMaxRan := $RangeMax$;
	PAnalog.PMinRan := $RangeMin$;
	
END_DATA_BLOCK
''')


	
	# Step 2: Create all the needed Structures from the TCT
	
	# UDT for DB_ANALOG_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE ANALOG_ManRequest
TITLE = ANALOG_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				attributePrimitiveType = attribute.getPrimitiveType()
				attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
				
				self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')



	# UDT for all the bin status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE ANALOG_bin_Status
TITLE = ANALOG_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification:  
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE ANALOG_event
TITLE = ANALOG_event
//
// parameters of PID Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# UDT for all the ana status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE ANALOG_ana_Status
TITLE = ANALOG_ana_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')
	
					
	# Step 3. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 
	
	# DB_EVENT
	
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_ANALOG Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_ANALOG
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type ANALOG
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_ANALOG : INT := '''+str(NbType)+''';
	ANALOG_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF ANALOG_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT
BEGIN
END_DATA_BLOCK

''')


	# Step 4: Create all the need DBs to store the required signal from the object
	
	# DB_ANALOG_ManRequest
	
	self.thePlugin.writeInstanceInfo('''
// DB_ANALOG_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_ANALOG_ManRequest
TITLE = DB_ANALOG_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    ANALOG_Requests : ARRAY [1..'''+instanceNb+'''] OF ANALOG_ManRequest;
END_STRUCT;
BEGIN
''')

	for instance in instancesVector :								
		RecordNumber = instance.getInstanceNumber()
		Name = instance.getAttributeData("DeviceIdentification:Name")
		LimitOff = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceManualRequests:Parameter Limit Off/Closed"))
		LimitOn = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceManualRequests:Parameter Limit On/Open"))
		
		#PLiOn/Off default :  +/- 10% PMaxRan/PMinRan
		if LimitOn.strip() == "":
			LimitOn = str(float(RangeMax) - 0.1*(float(RangeMax)-float(RangeMin)))
			self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined LimitOn, LimitOn = $LimitOn$ is taken.")
		if LimitOff.strip()  == "":
			LimitOff = str(float(RangeMin) + 0.1*(float(RangeMax)-float(RangeMin)))
			self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined LimitOff, LimitOff = $LimitOff$ is taken.")

		self.thePlugin.writeInstanceInfo('''
	ANALOG_Requests['''+str(RecordNumber)+'''].PliOff:= $LimitOff$;
	ANALOG_Requests['''+str(RecordNumber)+'''].PliOn:= $LimitOn$;
''')



	self.thePlugin.writeInstanceInfo('''  
END_DATA_BLOCK

''')
	
	self.thePlugin.writeInstanceInfo('''
	
(*binaries Status of the ANALOG OBJECTS************************************************)
DATA_BLOCK DB_bin_status_ANALOG
TITLE = 'DB_bin_status_ANALOG'
//
// Global binary status DB of ANALOG
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : ANALOG_bin_Status;
''')


	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK

(*old binaries Status of the ANALOG OBJECTS************************************************)
DATA_BLOCK DB_bin_status_ANALOG_old
TITLE = 'DB_bin_status_ANALOG_old'
//
// Old Global binary status DB of ANALOG
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
	''')
	
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : ANALOG_bin_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK

(*analogic Status of the ANALOG OBJECTS************************************************)

DATA_BLOCK DB_ana_status_ANALOG
TITLE = 'DB_ana_status_ANALOG'
//
// Global analogic status DB of ANALOG
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : ANALOG_ana_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK


(*old analogic Status of the ANALOG OBJECTS************************************************)

DATA_BLOCK DB_ana_status_ANALOG_old
TITLE = 'DB_ana_status_ANALOG_old'
//
// Old Global analogic status DB of ANALOG
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : ANALOG_ana_Status;
''')


	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK
''')


	
	# Step 5: Create the Function. This function writes the input signals on each instance DB.
	# For each DB we execute the FB.DB 
	# and finally we pass the Status information from this instance DB to the corresponding Status DB (e.g DB_bin_status_ANALOG)
	LimitSize = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize","Analog"))
    # Computing number of vectors needed
	# crude method:
	# split into 2 vectors, based on following criteria:
	#  If no FeedbackAnalog, or it is not an AIR or it *is* in AIR DB #1 then -> instanceVector1
	#  otherwise in instanceVector2
	#  then split instanceVector1 based on LimitSize into: instanceVector1a (=FC_ANALOG) and instanceVector1b (=FC_ANALOG2)
	#  and split instanceVector2 based on LimitSize into: instanceVector2a (=FC_ANALOG3) and instanceVector2b (=FC_ANALOG4)
	instanceVector1 = []
	instanceVector2 = []
 
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		FeedbackAnalog = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Analog")
		if FeedbackAnalog <> "":
			inst_FeedbackAnalog_Vector = theRawInstances.findMatchingInstances ("AnalogInputReal","'#DeviceIdentification:Name#'='$FeedbackAnalog$'")
			if inst_FeedbackAnalog_Vector.size() > 0:
				instanceNumber = inst_FeedbackAnalog_Vector.get(0).getInstanceNumber()
				LimitSizeAIR = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize","AnalogInputReal"))
				if instanceNumber > LimitSizeAIR:
					instanceVector2.append(instance)
				else:
					instanceVector1.append(instance)
			else:
				instanceVector1.append(instance)
		else:
			instanceVector1.append(instance)
	self.thePlugin.writeDebugInUABLog("instanceVector1 size = "+str(len(instanceVector1)))    
	if len(instanceVector1) > LimitSize:
		instanceVector1a = instanceVector1[:LimitSize]
		self.thePlugin.writeDebugInUABLog("instanceVector1a size = "+str(len(instanceVector1a)))   
		instanceVector1b = instanceVector1[LimitSize:]
		self.thePlugin.writeDebugInUABLog("instanceVector1b size = "+str(len(instanceVector1b))) 
	else:
		instanceVector1a = instanceVector1
		instanceVector1b = []

	self.thePlugin.writeDebugInUABLog("instanceVector2 size = "+str(len(instanceVector2)))
	if len(instanceVector2) > LimitSize:
		instanceVector2a = instanceVector2[:LimitSize]
		self.thePlugin.writeDebugInUABLog("instanceVector2a size = "+str(len(instanceVector2a)))
		instanceVector2b = instanceVector2[LimitSize:]
		self.thePlugin.writeDebugInUABLog("instanceVector2b size = "+str(len(instanceVector2b)))
	else:
		instanceVector2a = instanceVector2
		instanceVector2b = []
                        
            
            
	self.thePlugin.writeInstanceInfo('''
(*EXEC of ANALOG************************************************)
FUNCTION FC_ANALOG : VOID
TITLE = 'FC_ANALOG'
//
// ANALOG calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_ANA'
FAMILY: 'Analog'
VAR_TEMP
	old_status1 : DWORD;
	old_status2 : DWORD;
END_VAR
BEGIN
''')

	for instance in instanceVector1a :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = instance.getInstanceNumber()
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		HardwareAnalogOutput = instance.getAttributeData ("FEDeviceEnvironmentInputs:Hardware Analog Output")
		LocalDrive = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Drive")
		FeedbackAnalog = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Analog")
		FeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On")
		FeedbackOff = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Off")
		FailSafeOpen = instance.getAttributeData	("FEDeviceParameters:ParReg:Fail-Safe")
		ProcessOutput = instance.getAttributeData("FEDeviceOutputs:Process Output")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
// ----------------------------------------------
// ---- Analog <'''+ str(RecordNumber) +'''>: $Name$
// ----------------------------------------------
old_status1 := DB_bin_status_ANALOG.$Name$.stsreg01;
old_status2 := DB_bin_status_ANALOG.$Name$.stsreg02;
old_status1 := ROR(IN:=old_status1, N:=16);
old_status2 := ROR(IN:=old_status2, N:=16);

$Name$.ManReg01:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].ManReg01;
$Name$.MPosR:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].MPosR;
$Name$.PLiOn:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].PLiOn;
$Name$.PLiOff:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].PLiOff;

''')
	
		if FeedbackOn <> "":
			s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HFOn:='''+s7db_id_result+FeedbackOn+'''.PosSt;''')

		if FeedbackOff <> "":
			s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HFOff:='''+s7db_id_result+FeedbackOff+'''.PosSt;''')


		if FeedbackAnalog <> "":
			# self.thePlugin.writeWarningInUABLog("RecordNumber = "+str(RecordNumber))   
			s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal, AnalogStatus, WordStatus")
			# self.thePlugin.writeWarningInUABLog("s7db_id_result"+FeedbackAnalog)  
			self.thePlugin.writeInstanceInfo('''
$Name$.HFPos:='''+s7db_id_result+FeedbackAnalog+'''.PosSt;''')
			
			
		if LocalDrive <> "":
			NotLocal = LocalDrive[0:4].lower()
			if NotLocal == "not ":
				LocalDrive = LocalDrive[4:]
			else:
				NotLocal = ""
				LocalDrive = LocalDrive
				
			s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HLD:='''+NotLocal+s7db_id_result+LocalDrive+'''.PosSt;''')
			
		self.thePlugin.writeInstanceInfo('''
		
// IO Error
''')
		
		if (FeedbackOn == "" and FeedbackOff == "" and FeedbackAnalog == "" and LocalDrive == "" and ProcessOutput == ""):
			self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOError
// No FeedbackOn, FeedbackOff, FeedbackAnalog, LocalDrive and ProcessOutput configured
''')
		else:
			self.thePlugin.writeInstanceInfo(Name+'''.IoError := ''')
			NbIOError = 0
			if FeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOn+'''.IoErrorW''')
				NbIOError = NbIOError +1 

			if FeedbackOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOff+'''.IoErrorW''')
				NbIOError = NbIOError +1
				
			if FeedbackAnalog <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal")
				if s7db_id_result <> "":
					if NbIOError <> 0:
						self.thePlugin.writeInstanceInfo('''
	OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackAnalog+'''.IoErrorW''')
					NbIOError = NbIOError +1

			if LocalDrive <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalDrive+'''.IoErrorW''')
				NbIOError = NbIOError +1
				
			if ProcessOutput <> "":
				s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "AnalogOutput, AnalogOutputReal")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutput+'''.IoErrorW''')
				NbIOError = NbIOError +1
			
			self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoError;
		
// IOSimu
''')
		if (FeedbackOn == "" and FeedbackOff == "" and FeedbackAnalog == "" and LocalDrive == "" and ProcessOutput == ""):
			self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOSimu
// No FeedbackOn, FeedbackOff, FeedbackAnalog, LocalDrive and ProcessOutput configured
''')
		else:
			self.thePlugin.writeInstanceInfo(Name+'''.IoSimu := ''')
			NbIOSImu = 0
			if FeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOn + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOn+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if FeedbackOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOff + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOff+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if FeedbackAnalog <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal")
				if s7db_id_result <> "":
					if NbIOSImu <> 0:
						self.thePlugin.writeInstanceInfo('''
	OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackAnalog + '''.IoSimuW OR ''' + s7db_id_result+FeedbackAnalog+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
		
			if LocalDrive <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result + LocalDrive + '''.IoSimuW OR ''' + s7db_id_result+LocalDrive+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
			
			if ProcessOutput <> "":
				s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "AnalogOutput, AnalogOutputReal")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result + ProcessOutput + '''.IoSimuW OR ''' + s7db_id_result+ProcessOutput+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
		
			self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoSimu;
''')

		self.thePlugin.writeInstanceInfo('''

// Calls the Baseline function			
CPC_FB_ANALOG.$Name$();
''')
		
		      
		 
		if ProcessOutput <> "":
			s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "AnalogOutput, AnalogOutputReal")
			self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutput+'''.AuposR := '''+Name+'''.OutOV;
''')
		self.thePlugin.writeInstanceInfo('''
//Reset AuAuMoR and AuAlAck
$Name$.AuAuMoR := FALSE;	
$Name$.AuAlAck := FALSE;	

//Recopy new status	
DB_bin_status_ANALOG.$Name$.stsreg01:= $Name$.Stsreg01;
DB_bin_status_ANALOG.$Name$.stsreg02:= $Name$.Stsreg02;
DB_ana_status_ANALOG.$Name$.PosSt:= $Name$.PosSt;
DB_ana_status_ANALOG.$Name$.AuPosRSt:= $Name$.AuPosRSt;
DB_ana_status_ANALOG.$Name$.MPosRSt:= $Name$.MPosRSt;
DB_ana_status_ANALOG.$Name$.PosRSt:= $Name$.PosRSt;
DB_Event_ANALOG.ANALOG_evstsreg['''+str(RecordNumber)+'''].evstsreg01 := old_status1 OR DB_bin_status_ANALOG.$Name$.stsreg01;
DB_Event_ANALOG.ANALOG_evstsreg['''+str(RecordNumber)+'''].evstsreg02 := old_status2 OR DB_bin_status_ANALOG.$Name$.stsreg02;
''')
	self.thePlugin.writeInstanceInfo(''' 
END_FUNCTION''')



	if len(instanceVector1b) > 0:
		self.thePlugin.writeInstanceInfo(''' 
    
(*EXEC of FC_ANALOG2************************************************)
FUNCTION FC_ANALOG2 : VOID
TITLE = 'FC_ANALOG2'
//
// ANALOG calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_AN2'
FAMILY: 'Analog'
VAR_TEMP
	old_status1 : DWORD;
	old_status2 : DWORD;
END_VAR
BEGIN
		''')

		for instance in instanceVector1b :
			# Now looping on each device instance.
			# 1 Building shortcuts
			# 2 Write code to output
			RecordNumber = instance.getInstanceNumber()
			Name = instance.getAttributeData("DeviceIdentification:Name")
			Description = instance.getAttributeData("DeviceDocumentation:Description")
			HardwareAnalogOutput = instance.getAttributeData ("FEDeviceEnvironmentInputs:Hardware Analog Output")
			LocalDrive = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Drive")
			FeedbackAnalog = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Analog")
			FeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On")
			FeedbackOff = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Off")
			FailSafeOpen = instance.getAttributeData	("FEDeviceParameters:ParReg:Fail-Safe")
			ProcessOutput = instance.getAttributeData("FEDeviceOutputs:Process Output")
			
			if (instance is not None):
				self.thePlugin.writeInstanceInfo('''
// ----------------------------------------------
// ---- Analog <'''+ str(RecordNumber) +'''>: $Name$
// ----------------------------------------------
old_status1 := DB_bin_status_ANALOG.$Name$.stsreg01;
old_status2 := DB_bin_status_ANALOG.$Name$.stsreg02;
old_status1 := ROR(IN:=old_status1, N:=16);
old_status2 := ROR(IN:=old_status2, N:=16);

$Name$.ManReg01:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].ManReg01;
$Name$.MPosR:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].MPosR;
$Name$.PLiOn:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].PLiOn;
$Name$.PLiOff:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].PLiOff;

''')
	
			if FeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo('''
$Name$.HFOn:='''+s7db_id_result+FeedbackOn+'''.PosSt;''')
			
			
			if FeedbackOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
				self.thePlugin.writeInstanceInfo('''
$Name$.HFOff:='''+s7db_id_result+FeedbackOff+'''.PosSt;''')


			if FeedbackAnalog <> "":
				# self.thePlugin.writeWarningInUABLog("RecordNumber = "+str(RecordNumber))   
				s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal, AnalogStatus, WordStatus")
				# self.thePlugin.writeWarningInUABLog("s7db_id_result"+FeedbackAnalog)  
				self.thePlugin.writeInstanceInfo('''
$Name$.HFPos:='''+s7db_id_result+FeedbackAnalog+'''.PosSt;''')
			
			
			if LocalDrive <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
				self.thePlugin.writeInstanceInfo('''
$Name$.HLD:='''+s7db_id_result+LocalDrive+'''.PosSt;''')
			
			self.thePlugin.writeInstanceInfo('''
		
// IO Error
''')
		
			if (FeedbackOn == "" and FeedbackOff == "" and FeedbackAnalog == "" and LocalDrive == "" and ProcessOutput == ""):
				self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOError
// No FeedbackOn, FeedbackOff, FeedbackAnalog, LocalDrive and ProcessOutput configured
''')
			else:
				self.thePlugin.writeInstanceInfo(Name+'''.IoError := ''')
				NbIOError = 0
				if FeedbackOn <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
					self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOn+'''.IoErrorW''')
					NbIOError = NbIOError +1 
			
				if FeedbackOff <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
					if NbIOError <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOff+'''.IoErrorW''')
					NbIOError = NbIOError +1
				
				if FeedbackAnalog <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal")
					if s7db_id_result <> "":
						if NbIOError <> 0:
							self.thePlugin.writeInstanceInfo('''
	OR ''')
						self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackAnalog+'''.IoErrorW''')
						NbIOError = NbIOError +1
				
				if LocalDrive <> "":
					s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
					if NbIOError <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result+LocalDrive+'''.IoErrorW''')
					NbIOError = NbIOError +1
				
				if ProcessOutput <> "":
					s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "AnalogOutput, AnalogOutputReal")
					if NbIOError <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutput+'''.IoErrorW''')
					NbIOError = NbIOError +1
			
				self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoError;
		
// IOSimu
''')
			if (FeedbackOn == "" and FeedbackOff == "" and FeedbackAnalog == "" and LocalDrive == "" and ProcessOutput == ""):
				self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOSimu
// No FeedbackOn, FeedbackOff, FeedbackAnalog, LocalDrive and ProcessOutput configured
''')
			else:
				self.thePlugin.writeInstanceInfo(Name+'''.IoSimu := ''')
				NbIOSImu = 0
				if FeedbackOn <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
					self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOn + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOn+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
				
			
				if FeedbackOff <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
					if NbIOSImu <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOff + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOff+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
				
				if FeedbackAnalog <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal")
					if s7db_id_result <> "":
						if NbIOSImu <> 0:
							self.thePlugin.writeInstanceInfo('''
	OR ''')
						self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackAnalog + '''.IoSimuW OR ''' + s7db_id_result+FeedbackAnalog+'''.FoMoSt''')
						NbIOSImu = NbIOSImu +1 
			
				if LocalDrive <> "":
					s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
					if NbIOSImu <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result + LocalDrive + '''.IoSimuW OR ''' + s7db_id_result+LocalDrive+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
				
				if ProcessOutput <> "":
					s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "AnalogOutput, AnalogOutputReal")
					if NbIOSImu <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result + ProcessOutput + '''.IoSimuW OR ''' + s7db_id_result+ProcessOutput+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
			
				self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoSimu;
''')

			self.thePlugin.writeInstanceInfo('''

// Calls the Baseline function			
CPC_FB_ANALOG.$Name$();
''')
		
		      
		 
			if ProcessOutput <> "":
				s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "AnalogOutput, AnalogOutputReal")
				self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutput+'''.AuposR := '''+Name+'''.OutOV;
''')
			self.thePlugin.writeInstanceInfo('''
//Reset AuAuMoR and AuAlAck
$Name$.AuAuMoR := FALSE;	
$Name$.AuAlAck := FALSE;	

//Recopy new status	
DB_bin_status_ANALOG.$Name$.stsreg01:= $Name$.Stsreg01;
DB_bin_status_ANALOG.$Name$.stsreg02:= $Name$.Stsreg02;
DB_ana_status_ANALOG.$Name$.PosSt:= $Name$.PosSt;
DB_ana_status_ANALOG.$Name$.AuPosRSt:= $Name$.AuPosRSt;
DB_ana_status_ANALOG.$Name$.MPosRSt:= $Name$.MPosRSt;
DB_ana_status_ANALOG.$Name$.PosRSt:= $Name$.PosRSt;
DB_Event_ANALOG.ANALOG_evstsreg['''+str(RecordNumber)+'''].evstsreg01 := old_status1 OR DB_bin_status_ANALOG.$Name$.stsreg01;
DB_Event_ANALOG.ANALOG_evstsreg['''+str(RecordNumber)+'''].evstsreg02 := old_status2 OR DB_bin_status_ANALOG.$Name$.stsreg02;
''')
		self.thePlugin.writeInstanceInfo(''' 
END_FUNCTION''')        


	if len(instanceVector2a) > 0:
		self.thePlugin.writeInstanceInfo(''' 
    
(*EXEC of FC_ANALOG3************************************************)
FUNCTION FC_ANALOG3 : VOID
TITLE = 'FC_ANALOG3'
//
// ANALOG calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_AN3'
FAMILY: 'Analog'
VAR_TEMP
	old_status1 : DWORD;
	old_status2 : DWORD;
END_VAR
BEGIN
		''')

		for instance in instanceVector2a :
			# Now looping on each device instance.
			# 1 Building shortcuts
			# 2 Write code to output
			RecordNumber = instance.getInstanceNumber()
			Name = instance.getAttributeData("DeviceIdentification:Name")
			Description = instance.getAttributeData("DeviceDocumentation:Description")
			HardwareAnalogOutput = instance.getAttributeData ("FEDeviceEnvironmentInputs:Hardware Analog Output")
			LocalDrive = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Drive")
			FeedbackAnalog = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Analog")
			FeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On")
			FeedbackOff = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Off")
			FailSafeOpen = instance.getAttributeData	("FEDeviceParameters:ParReg:Fail-Safe")
			ProcessOutput = instance.getAttributeData("FEDeviceOutputs:Process Output")
			
			if (instance is not None):
				self.thePlugin.writeInstanceInfo('''
// ----------------------------------------------
// ---- Analog <'''+ str(RecordNumber) +'''>: $Name$
// ----------------------------------------------
old_status1 := DB_bin_status_ANALOG.$Name$.stsreg01;
old_status2 := DB_bin_status_ANALOG.$Name$.stsreg02;
old_status1 := ROR(IN:=old_status1, N:=16);
old_status2 := ROR(IN:=old_status2, N:=16);

$Name$.ManReg01:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].ManReg01;
$Name$.MPosR:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].MPosR;
$Name$.PLiOn:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].PLiOn;
$Name$.PLiOff:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].PLiOff;

''')
	
			if FeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo('''
$Name$.HFOn:='''+s7db_id_result+FeedbackOn+'''.PosSt;''')
			
			
			if FeedbackOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
				self.thePlugin.writeInstanceInfo('''
$Name$.HFOff:='''+s7db_id_result+FeedbackOff+'''.PosSt;''')


			if FeedbackAnalog <> "":
				# self.thePlugin.writeWarningInUABLog("RecordNumber = "+str(RecordNumber))   
				s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal, AnalogStatus, WordStatus")
				# self.thePlugin.writeWarningInUABLog("s7db_id_result"+FeedbackAnalog)  
				self.thePlugin.writeInstanceInfo('''
$Name$.HFPos:='''+s7db_id_result+FeedbackAnalog+'''.PosSt;''')
			
			
			if LocalDrive <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
				self.thePlugin.writeInstanceInfo('''
$Name$.HLD:='''+s7db_id_result+LocalDrive+'''.PosSt;''')
			
			self.thePlugin.writeInstanceInfo('''
		
// IO Error
''')
		
			if (FeedbackOn == "" and FeedbackOff == "" and FeedbackAnalog == "" and LocalDrive == "" and ProcessOutput == ""):
				self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOError
// No FeedbackOn, FeedbackOff, FeedbackAnalog, LocalDrive and ProcessOutput configured
''')
			else:
				self.thePlugin.writeInstanceInfo(Name+'''.IoError := ''')
				NbIOError = 0
				if FeedbackOn <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
					self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOn+'''.IoErrorW''')
					NbIOError = NbIOError +1 
			
				if FeedbackOff <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
					if NbIOError <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOff+'''.IoErrorW''')
					NbIOError = NbIOError +1
				
				if FeedbackAnalog <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal")
					if s7db_id_result <> "":
						if NbIOError <> 0:
							self.thePlugin.writeInstanceInfo('''
	OR ''')
						self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackAnalog+'''.IoErrorW''')
						NbIOError = NbIOError +1
				
				if LocalDrive <> "":
					s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
					if NbIOError <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result+LocalDrive+'''.IoErrorW''')
					NbIOError = NbIOError +1
				
				if ProcessOutput <> "":
					s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "AnalogOutput, AnalogOutputReal")
					if NbIOError <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutput+'''.IoErrorW''')
					NbIOError = NbIOError +1
			
				self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoError;
		
// IOSimu
''')
			if (FeedbackOn == "" and FeedbackOff == "" and FeedbackAnalog == "" and LocalDrive == "" and ProcessOutput == ""):
				self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOSimu
// No FeedbackOn, FeedbackOff, FeedbackAnalog, LocalDrive and ProcessOutput configured
''')
			else:
				self.thePlugin.writeInstanceInfo(Name+'''.IoSimu := ''')
				NbIOSImu = 0
				if FeedbackOn <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
					self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOn + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOn+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
				
			
				if FeedbackOff <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
					if NbIOSImu <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOff + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOff+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
				
				if FeedbackAnalog <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal")
					if s7db_id_result <> "":
						if NbIOSImu <> 0:
							self.thePlugin.writeInstanceInfo('''
	OR ''')
						self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackAnalog + '''.IoSimuW OR ''' + s7db_id_result+FeedbackAnalog+'''.FoMoSt''')
						NbIOSImu = NbIOSImu +1 
			
				if LocalDrive <> "":
					s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
					if NbIOSImu <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result + LocalDrive + '''.IoSimuW OR ''' + s7db_id_result+LocalDrive+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
				
				if ProcessOutput <> "":
					s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "AnalogOutput, AnalogOutputReal")
					if NbIOSImu <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result + ProcessOutput + '''.IoSimuW OR ''' + s7db_id_result+ProcessOutput+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
			
				self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoSimu;
''')

			self.thePlugin.writeInstanceInfo('''

// Calls the Baseline function			
CPC_FB_ANALOG.$Name$();
''')
		
		      
		 
			if ProcessOutput <> "":
				s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "AnalogOutput, AnalogOutputReal")
				self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutput+'''.AuposR := '''+Name+'''.OutOV;
''')
			self.thePlugin.writeInstanceInfo('''
//Reset AuAuMoR and AuAlAck
$Name$.AuAuMoR := FALSE;	
$Name$.AuAlAck := FALSE;	

//Recopy new status			
DB_bin_status_ANALOG.$Name$.stsreg01:= $Name$.Stsreg01;
DB_bin_status_ANALOG.$Name$.stsreg02:= $Name$.Stsreg02;
DB_ana_status_ANALOG.$Name$.PosSt:= $Name$.PosSt;
DB_ana_status_ANALOG.$Name$.AuPosRSt:= $Name$.AuPosRSt;
DB_ana_status_ANALOG.$Name$.MPosRSt:= $Name$.MPosRSt;
DB_ana_status_ANALOG.$Name$.PosRSt:= $Name$.PosRSt;
DB_Event_ANALOG.ANALOG_evstsreg['''+str(RecordNumber)+'''].evstsreg01 := old_status1 OR DB_bin_status_ANALOG.$Name$.stsreg01;
DB_Event_ANALOG.ANALOG_evstsreg['''+str(RecordNumber)+'''].evstsreg02 := old_status2 OR DB_bin_status_ANALOG.$Name$.stsreg02;
''')
		self.thePlugin.writeInstanceInfo(''' 
END_FUNCTION''')        


	if len(instanceVector2b) > 0:
		self.thePlugin.writeInstanceInfo(''' 
    
(*EXEC of FC_ANALOG4************************************************)
FUNCTION FC_ANALOG4 : VOID
TITLE = 'FC_ANALOG4'
//
// ANALOG calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_AN4'
FAMILY: 'Analog'
VAR_TEMP
	old_status1 : DWORD;
	old_status2 : DWORD;
END_VAR
BEGIN
		''')

		for instance in instanceVector2b :
			# Now looping on each device instance.
			# 1 Building shortcuts
			# 2 Write code to output
			RecordNumber = instance.getInstanceNumber()
			Name = instance.getAttributeData("DeviceIdentification:Name")
			Description = instance.getAttributeData("DeviceDocumentation:Description")
			HardwareAnalogOutput = instance.getAttributeData ("FEDeviceEnvironmentInputs:Hardware Analog Output")
			LocalDrive = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Drive")
			FeedbackAnalog = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Analog")
			FeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On")
			FeedbackOff = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Off")
			FailSafeOpen = instance.getAttributeData	("FEDeviceParameters:ParReg:Fail-Safe")
			ProcessOutput = instance.getAttributeData("FEDeviceOutputs:Process Output")
			
			if (instance is not None):
				self.thePlugin.writeInstanceInfo('''
// ----------------------------------------------
// ---- Analog <'''+ str(RecordNumber) +'''>: $Name$
// ----------------------------------------------
old_status1 := DB_bin_status_ANALOG.$Name$.stsreg01;
old_status2 := DB_bin_status_ANALOG.$Name$.stsreg02;
old_status1 := ROR(IN:=old_status1, N:=16);
old_status2 := ROR(IN:=old_status2, N:=16);

$Name$.ManReg01:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].ManReg01;
$Name$.MPosR:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].MPosR;
$Name$.PLiOn:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].PLiOn;
$Name$.PLiOff:=DB_ANALOG_ManRequest.ANALOG_Requests['''+ str(RecordNumber) +'''].PLiOff;

''')
	
			if FeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo('''
$Name$.HFOn:='''+s7db_id_result+FeedbackOn+'''.PosSt;''')
			
			
			if FeedbackOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
				self.thePlugin.writeInstanceInfo('''
$Name$.HFOff:='''+s7db_id_result+FeedbackOff+'''.PosSt;''')


			if FeedbackAnalog <> "":
				# self.thePlugin.writeWarningInUABLog("RecordNumber = "+str(RecordNumber))   
				s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal, AnalogStatus, WordStatus")
				# self.thePlugin.writeWarningInUABLog("s7db_id_result"+FeedbackAnalog)  
				self.thePlugin.writeInstanceInfo('''
$Name$.HFPos:='''+s7db_id_result+FeedbackAnalog+'''.PosSt;''')
			
			
			if LocalDrive <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
				self.thePlugin.writeInstanceInfo('''
$Name$.HLD:='''+s7db_id_result+LocalDrive+'''.PosSt;''')
			
			self.thePlugin.writeInstanceInfo('''
		
// IO Error
''')
		
			if (FeedbackOn == "" and FeedbackOff == "" and FeedbackAnalog == "" and LocalDrive == "" and ProcessOutput == ""):
				self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOError
// No FeedbackOn, FeedbackOff, FeedbackAnalog, LocalDrive and ProcessOutput configured
''')
			else:
				self.thePlugin.writeInstanceInfo(Name+'''.IoError := ''')
				NbIOError = 0
				if FeedbackOn <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
					self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOn+'''.IoErrorW''')
					NbIOError = NbIOError +1 
			
				if FeedbackOff <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
					if NbIOError <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOff+'''.IoErrorW''')
					NbIOError = NbIOError +1
				
				if FeedbackAnalog <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal")
					if s7db_id_result <> "":
						if NbIOError <> 0:
							self.thePlugin.writeInstanceInfo('''
	OR ''')
						self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackAnalog+'''.IoErrorW''')
						NbIOError = NbIOError +1
				
				if LocalDrive <> "":
					s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
					if NbIOError <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result+LocalDrive+'''.IoErrorW''')
					NbIOError = NbIOError +1
				
				if ProcessOutput <> "":
					s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "AnalogOutput, AnalogOutputReal")
					if NbIOError <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutput+'''.IoErrorW''')
					NbIOError = NbIOError +1
			
				self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoError;
		
// IOSimu
''')
			if (FeedbackOn == "" and FeedbackOff == "" and FeedbackAnalog == "" and LocalDrive == "" and ProcessOutput == ""):
				self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOSimu
// No FeedbackOn, FeedbackOff, FeedbackAnalog, LocalDrive and ProcessOutput configured
''')
			else:
				self.thePlugin.writeInstanceInfo(Name+'''.IoSimu := ''')
				NbIOSImu = 0
				if FeedbackOn <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
					self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOn + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOn+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
				
			
				if FeedbackOff <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
					if NbIOSImu <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOff + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOff+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
				
				if FeedbackAnalog <> "":
					s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal")
					if s7db_id_result <> "":
						if NbIOSImu <> 0:
							self.thePlugin.writeInstanceInfo('''
	OR ''')
						self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackAnalog + '''.IoSimuW OR ''' + s7db_id_result+FeedbackAnalog+'''.FoMoSt''')
						NbIOSImu = NbIOSImu +1 
			
				if LocalDrive <> "":
					s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
					if NbIOSImu <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result + LocalDrive + '''.IoSimuW OR ''' + s7db_id_result+LocalDrive+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
				
				if ProcessOutput <> "":
					s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "AnalogOutput, AnalogOutputReal")
					if NbIOSImu <> 0:
						self.thePlugin.writeInstanceInfo('''
OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result + ProcessOutput + '''.IoSimuW OR ''' + s7db_id_result+ProcessOutput+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
			
				self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoSimu;
''')

			self.thePlugin.writeInstanceInfo('''

// Calls the Baseline function			
CPC_FB_ANALOG.$Name$();
''')
		
		      
		 
			if ProcessOutput <> "":
				s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "AnalogOutput, AnalogOutputReal")
				self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutput+'''.AuposR := '''+Name+'''.OutOV;
''')
			self.thePlugin.writeInstanceInfo('''
//Reset AuAuMoR and AuAlAck
$Name$.AuAuMoR := FALSE;	
$Name$.AuAlAck := FALSE;	

//Recopy new status		
DB_bin_status_ANALOG.$Name$.stsreg01:= $Name$.Stsreg01;
DB_bin_status_ANALOG.$Name$.stsreg02:= $Name$.Stsreg02;
DB_ana_status_ANALOG.$Name$.PosSt:= $Name$.PosSt;
DB_ana_status_ANALOG.$Name$.AuPosRSt:= $Name$.AuPosRSt;
DB_ana_status_ANALOG.$Name$.MPosRSt:= $Name$.MPosRSt;
DB_ana_status_ANALOG.$Name$.PosRSt:= $Name$.PosRSt;
DB_Event_ANALOG.ANALOG_evstsreg['''+str(RecordNumber)+'''].evstsreg01 := old_status1 OR DB_bin_status_ANALOG.$Name$.stsreg01;
DB_Event_ANALOG.ANALOG_evstsreg['''+str(RecordNumber)+'''].evstsreg02 := old_status2 OR DB_bin_status_ANALOG.$Name$.stsreg02;
''')

		self.thePlugin.writeInstanceInfo(''' 
END_FUNCTION''')        
 


   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for ANALOG.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for ANALOG.")
		
