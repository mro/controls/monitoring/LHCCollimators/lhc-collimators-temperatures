# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class WordParameter_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for WPAR.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for WPAR.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for WPAR.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'

   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for WPAR.")
		
	# General Steps for Optimized objects:
	# 1. Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances) It's called DB_Type
	# 2. Create all the needed Structures from DeviceTypeDefinition.xml created by TCT
	# 3. Create all the need DBs to store the required signal from the object
	# 4. Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 5. Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#	 We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	# This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	
	# Step 1: Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances)
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Word Parameter DB Creation file: UNICOS application
''')
	self.thePlugin.writeInstanceInfo('''
DATA_BLOCK DB_WPAR CPC_FB_WPAR
BEGIN
END_DATA_BLOCK
''')

	# Step 3: Create all the needed Structures from the TCT

	# UDT for DB_WPAR_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE WPAR_ManRequest
TITLE = WPAR_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isCommVerification = attribute.getIsCommunicated()
				if isCommVerification:
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE

''')


	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE WPAR_bin_Status
TITLE = WPAR_bin_Status
//
// parameters of WPAR Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')
	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE WPAR_event
TITLE = WPAR_event
//
// parameters of WPAR Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				# isEventAttribute = attribute.getIsEventAttribute()
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for the ana status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE WPAR_ana_Status
TITLE = WPAR_ana_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributeName = attribute.getAttributeName()
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE

''')

	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_WPAR_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_WPAR_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_WPAR_ManRequest
TITLE = DB_WPAR_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    WPAR_Requests : ARRAY [1..'''+instanceNb+'''] OF WPAR_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')

	# Step 2: Create all the needed Structures from the TCT	
		
	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_WPAR Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_WPAR
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type WPAR
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_WPAR : INT := '''+str(NbType)+''';
	WPAR_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF WPAR_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')
		
	self.thePlugin.writeInstanceInfo('''
(************************* binary Status of the WPARs ************************)
DATA_BLOCK DB_bin_status_WPAR
TITLE = 'DB_bin_status_WPAR'
//
// Global binary status DB of WPAR
//
// List of variables:''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (WPAR_bin_Status)
''')

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF WPAR_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(********************* old binary Status of the WPARs ****************************)
DATA_BLOCK DB_bin_status_WPAR_old
TITLE = 'DB_bin_status_WPAR_old'
//
// Old Global binary status DB of WPAR
//
''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (WPAR_bin_Status)
''')
		
	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF WPAR_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
 (*analog status of the WPARs************************************************)
DATA_BLOCK DB_ana_status_WPAR
TITLE = 'DB_ana_status_WPAR'
//
// Global Analog status DB of WPAR
//
//  List of variables:''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
	//  ['''+str(RecordNumber)+''']    $Name$      (WPAR_ana_Status)
''')	

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF WPAR_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
// -----------------------------------------------------------------------------------------------------
(*old analog status of the WPARs************************************************)
DATA_BLOCK DB_ana_Status_WPAR_old
TITLE = 'DB_ana_Status_WPAR_old'
//
// old Global Analog  status DB of WPAR
//
// List of variables:  
''')
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (WPAR_ana_Status)
''')		

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF WPAR_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
      ''')

	# Step 5: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	 
	self.thePlugin.writeInstanceInfo('''
  
(*WPAR execution ********************************************)
FUNCTION_BLOCK FB_WPAR_all
TITLE = 'FB_WPAR_all'
//
// Call the WPAR treatment
//
AUTHOR: 'UNICOS'
NAME: 'CallWPAR'
FAMILY: 'WPAR'
VAR
  // Static Variables
  WPAR_SET: STRUCT

''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
   $Name$       : CPC_DB_WPAR;
''')		
		
	self.thePlugin.writeInstanceInfo('''
  END_STRUCT;  
  
  // Different variable view declaration
  WPAR AT WPAR_SET: ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_DB_WPAR;
  
  // Support variables
  old_status : DWORD;
  I: INT;
END_VAR
  FOR I:=1 TO '''+str(RecordNumber)+''' DO

''')

	self.thePlugin.writeInstanceInfo('''

	// Recovery static variables
	old_status := DB_bin_status_WPAR.StsReg01[I].StsReg01;
    old_status := ROR(IN:=old_status, N:=16);
    
	// Calls the Baseline function
    CPC_FB_WPAR.DB_WPAR(	
		 MposR :=  DB_WPAR_ManRequest.WPAR_Requests[I].MposR        // set by WinCCOA in the DB_WPAR_ManRequest   
		,Manreg01 :=  DB_WPAR_ManRequest.WPAR_Requests[I].Manreg01		// set by WinCCOA in the DB_WPAR_ManRequest
		,StsReg01:= DB_bin_status_WPAR.StsReg01[I].StsReg01
		,Perst := WPAR[I]
	); 
    
	// call the IO_ACCESS_WPAR
	IF (WPAR[I].FEType <> 0) THEN
		IO_ACCESS_WPAR(PQWDef := WPAR[I].PQWDef,
			  FEType := WPAR[I].FEType,
			  InterfaceParam1 := WPAR[I].InterfaceParam1, 
			  InterfaceParam2 := WPAR[I].InterfaceParam2,
			  PosSt := WPAR[I].PosSt);
	END_IF;
			  
	// Update Binary Status
	DB_bin_status_WPAR.StsReg01[I].StsReg01:= DB_WPAR.StsReg01; 
   
    // Update Analog Status
    DB_ana_status_WPAR.status[I].MPosRSt:= WPAR[I].MPosRSt;   
    DB_ana_status_WPAR.status[I].PosSt:= WPAR[I].PosSt;

	// Update Events
    DB_Event_WPAR.WPAR_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_WPAR.StsReg01[I].StsReg01;
    
  END_FOR;
END_FUNCTION_BLOCK
     ''')

	 
	# Step 6: Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	self.thePlugin.writeInstanceInfo('''
(* All WPAR devices instance DB  **************************************)     
DATA_BLOCK DB_WPAR_all  FB_WPAR_all
//
// Instance DB for the whole WPAR devices initialization
//
BEGIN
''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
		if (FEType ==""):
			FEType = "0"
			#self.thePlugin.writeWarningInUABLog("WPAR instance: $Name$. Undefined Type, FEEncondType = 0 is taken.")
		InterfaceParam1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
		InterfaceParam2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
		DefaultValue = instance.getAttributeData("FEDeviceParameters:Default Value")
		RangeMax 		= instance.getAttributeData ("FEDeviceParameters:Range Max").strip()
		RangeMin 		= instance.getAttributeData ("FEDeviceParameters:Range Min").strip()
		# check consistency
		if float(DefaultValue) < float(RangeMin) or float(DefaultValue) >float(RangeMax): 
			self.thePlugin.writeWarningInUABLog("Apar instance: $Name$. The default value:($DefaultValue$) is outside its range [$RangeMin$ - $RangeMax$].")
		if float(RangeMax) < float(RangeMin) : 
			self.thePlugin.writeWarningInUABLog("Apar instance: $Name$. The value Max:($RangeMax$) should be bigger than the Min:($RangeMin$).")

		#convert default value in word (16bit format) 
		DefaultHexaValue = hex(int(DefaultValue))
		DefaultHexValue = DefaultHexaValue[2:len(DefaultHexaValue)]	

		self.thePlugin.writeInstanceInfo('''
WPAR_SET.$Name$.index := '''+str(RecordNumber)+''';
WPAR_SET.$Name$.PosSt := W#16#$DefaultHexValue$;		
WPAR_SET.$Name$.FEType:=$FEType$;
''')
		#if (FEType =="0"):
		#	self.thePlugin.writeWarningInUABLog("WPAR instance: $Name$. FEEncondType = 0 No address configured.")
		if (FEType =="1"):
			Init = InterfaceParam1[0]
			if Init == "p":
				Fin = InterfaceParam1[3:]
				PQWDef = "true"
			else:
				Fin = InterfaceParam1[2:]
				PQWDef = "false"
			self.thePlugin.writeInstanceInfo('''
WPAR_SET.$Name$.InterfaceParam1:= $Fin$;
WPAR_SET.$Name$.PQWDef:= $PQWDef$;''')
		if (FEType == "101"):
			if InterfaceParam1[0:2] == "db":
				InterfaceParam1 = InterfaceParam1[2:]
			if InterfaceParam2[0:2] ==  "db":
				InterfaceParam2 = InterfaceParam2[3:]
			self.thePlugin.writeInstanceInfo('''
WPAR_SET.$Name$.InterfaceParam1:=$InterfaceParam1$;
WPAR_SET.$Name$.InterfaceParam2:=$InterfaceParam2$;
		 ''')
		if (FEType == "100"):
			if InterfaceParam1[0] == "m":
				InterfaceParam1 = InterfaceParam1[2:]
			self.thePlugin.writeInstanceInfo('''
WPAR_SET.$Name$.InterfaceParam1:=$InterfaceParam1$;
		 ''')

	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK
''')

	#Creation of a FC even for the optimized objects in order to call it in the OB1
	#This avoid the too many symbols problem of the OB1
	#The code is ready for all the object in case we need to split in several DBs one day. Note that for the moment,
	#this has sense only for the AIR and the DI where the split is working.
	NumberOfInstances = instancesVector.size()
	LimitSizeOfInstances = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", theCurrentDeviceType.getDeviceTypeName()))
	nameRepresentation = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", theCurrentDeviceType.getDeviceTypeName())
	self.thePlugin.writeInstanceInfo('''
FUNCTION FC_$nameRepresentation$ : VOID
    FB_$nameRepresentation$_all.DB_$nameRepresentation$_all();''')
	if (NumberOfInstances > LimitSizeOfInstances):
		self.thePlugin.writeInstanceInfo('''
    FB_$nameRepresentation$_all2.DB_$nameRepresentation$_all2();    // Too many '''+str(NumberOfInstances)+''' Need a second FB_$nameRepresentation$_All2''')
	self.thePlugin.writeInstanceInfo('''
END_FUNCTION
''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for WPAR.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for WPAR.")
		
