# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class AnalogParameter_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for APAR.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for APAR.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for APAR.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'
	
   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for APAR.")
		
	# General Steps for Optimized objects:
	# 1. Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances) It's called DB_Type
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from DeviceTypeDefinition.xml created by TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5. Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 6. Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#	 We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	# This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	
	# Step 1: Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances)
		
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Analog Parameter DB Creation file: UNICOS application
''')
	self.thePlugin.writeInstanceInfo('''
DATA_BLOCK DB_APAR CPC_FB_APAR
BEGIN
END_DATA_BLOCK
''')



	# Step 3: DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	
	# UDT for DB_APAR_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE APAR_ManRequest
TITLE = APAR_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isCommVerification = attribute.getIsCommunicated()
				if isCommVerification:
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# Step 4: Create all the needed Structures from the TCT	
	
	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE APAR_bin_Status
TITLE = APAR_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for the ana status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE APAR_ana_Status
TITLE = APAR_ana_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributeName = attribute.getAttributeName()
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE APAR_event
TITLE = APAR_event
//
// parameters of ANADIG Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				# isEventAttribute = attribute.getIsEventAttribute()
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# Step 5: Create all the need DBs to store the required signal from the object

	# DB_APAR_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_APAR_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_APAR_ManRequest
TITLE = DB_APAR_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    APAR_Requests : ARRAY [1..'''+instanceNb+'''] OF APAR_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')
	
	# Step 2: Create all the needed Structures from the TCT	
		
	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_APAR Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_APAR
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type APAR
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_APAR : INT := '''+str(NbType)+''';
	APAR_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF APAR_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')
	
	self.thePlugin.writeInstanceInfo('''
(************************* binary Status of the APARs ************************)
DATA_BLOCK DB_bin_status_APAR
TITLE = 'DB_bin_status_APAR'
//
// Global binary status DB of APAR
//
// List of variables:''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (APAR_bin_Status)
''')

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF APAR_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(********************* old binary Status of the APARs ****************************)
DATA_BLOCK DB_bin_status_APAR_old
TITLE = 'DB_bin_status_APAR_old'
//
// Old Global binary status DB of APAR
//
''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (APAR_bin_Status)
''')

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF APAR_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(*analog status of the APARs************************************************)
DATA_BLOCK DB_ana_status_APAR
TITLE = 'DB_ana_status_APAR'
//
// Global Analog status DB of APAR
//
//  List of variables:''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
	//  ['''+str(RecordNumber)+''']    $Name$      (APAR_ana_Status)
''')	


	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF APAR_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
// -----------------------------------------------------------------------------------------------------
(*old analog status of the APARs************************************************)
DATA_BLOCK DB_ana_Status_APAR_old
TITLE = 'DB_ana_Status_APAR_old'
//
// old Global Analog  status DB of APAR
//
// List of variables:  
''')
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (APAR_ana_Status)
''')		

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF APAR_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
   ''')

	# Step 6: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	 
	self.thePlugin.writeInstanceInfo('''
(*APAR execution ********************************************)
FUNCTION_BLOCK FB_APAR_all
TITLE = 'FB_APAR_all'
//
// Call the APAR treatment
//
AUTHOR: 'UNICOS'
NAME: 'CallAPAR'
FAMILY: 'APAR'
VAR
  // Static Variables
  APAR_SET: STRUCT

''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
   $Name$       : CPC_DB_APAR;
''')		

	self.thePlugin.writeInstanceInfo('''
  END_STRUCT;  
  
  // Different variable view declaration
  APAR AT APAR_SET: ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_DB_APAR;
  
  // Support variables
  old_status : DWORD;
  I: INT;
END_VAR
  FOR I:=1 TO '''+str(RecordNumber)+''' DO

''')

	self.thePlugin.writeInstanceInfo('''
	// Recovery static variables
	old_status := DB_bin_status_APAR.StsReg01[I].StsReg01;
    old_status := ROR(IN:=old_status, N:=16);
    
	// Status
	DB_APAR.StsReg01:=DB_bin_status_APAR.StsReg01[I].StsReg01;
	
	// Calls the Baseline function
    CPC_FB_APAR.DB_APAR(	
		 MPosR :=  DB_APAR_ManRequest.APAR_Requests[I].MPosR          // set by WinCCOA in the DB_APAR_ManRequest
		,Manreg01 :=  DB_APAR_ManRequest.APAR_Requests[I].Manreg01		// set by WinCCOA in the DB_APAR_ManRequest
		,StsReg01 := DB_bin_status_APAR.StsReg01[I].StsReg01
		,Perst := APAR[I]
			); 	
				
	// Update Event
    DB_Event_APAR.APAR_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_APAR.StsReg01[I].StsReg01;

	// Update Analog Status
	DB_ana_status_APAR.status[I].MPosRSt:= APAR[I].MPosRSt; 
	DB_ana_status_APAR.status[I].PosSt:= APAR[I].PosSt;
		
  END_FOR;
END_FUNCTION_BLOCK
''')

	 
	# Step 7: Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	self.thePlugin.writeInstanceInfo('''
(* All APAR devices instance DB  **************************************)     
DATA_BLOCK DB_APAR_all  FB_APAR_all
//
// Instance DB for the whole APAR devices initialization
//
BEGIN
''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		# Latch = instance.getAttributeData("FEDeviceAutoRequests:Latch")
		DefaultValue = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Default Value"))
		RangeMax 		= instance.getAttributeData ("FEDeviceParameters:Range Max").strip()
		RangeMin 		= instance.getAttributeData ("FEDeviceParameters:Range Min").strip()
		# check consistency
		if float(DefaultValue) < float(RangeMin) or float(DefaultValue) >float(RangeMax): 
			self.thePlugin.writeWarningInUABLog("Apar instance: $Name$. The default value:($DefaultValue$) is outside its range [$RangeMin$ - $RangeMax$].")
		if float(RangeMax) < float(RangeMin) : 
			self.thePlugin.writeWarningInUABLog("Apar instance: $Name$. The value Max:($RangeMax$) should be bigger than the Min:($RangeMin$).")
			
		if DefaultValue == "":
			DefaultValue = "0.0"
		self.thePlugin.writeInstanceInfo('''
APAR_SET.$Name$.PosSt := $DefaultValue$;		
''')

		
	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK
''')

	#Creation of a FC even for the optimized objects in order to call it in the OB1
	#This avoid the too many symbols problem of the OB1
	#The code is ready for all the object in case we need to split in several DBs one day. Note that for the moment,
	#this has sense only for the AIR and the DI where the split is working.
	NumberOfInstances = instancesVector.size()
	LimitSizeOfInstances = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", theCurrentDeviceType.getDeviceTypeName()))
	nameRepresentation = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", theCurrentDeviceType.getDeviceTypeName())
	self.thePlugin.writeInstanceInfo('''
FUNCTION FC_$nameRepresentation$ : VOID
    FB_$nameRepresentation$_all.DB_$nameRepresentation$_all();''')
	if (NumberOfInstances > LimitSizeOfInstances):
		self.thePlugin.writeInstanceInfo('''
    FB_$nameRepresentation$_all2.DB_$nameRepresentation$_all2();    // Too many '''+str(NumberOfInstances)+''' Need a second FB_$nameRepresentation$_All2''')
	self.thePlugin.writeInstanceInfo('''
END_FUNCTION
''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for APAR.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for APAR.")
		
