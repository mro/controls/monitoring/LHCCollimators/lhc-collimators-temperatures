# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

class AnalogInputReal_Template(IUnicosTemplate):
   theSemanticVerifier = 0
   thePlugin = 0
   isDataValid = 1
   theCurrentDeviceType = 0
   
   def initialize(self):
   	self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("check rules: initialize")

   def check(self):
	self.thePlugin.writeInUABLog("check rules: check")

   def begin(self):
	self.thePlugin.writeInUABLog("check rules: begin")
	
   def process(self, *params): 
	theCurrentDeviceTypeName = params[0]
	theCurrentDeviceTypeDefinition = params[1]
	theUnicosProject = self.thePlugin.getUnicosProject()
   	theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)	
   	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	self.thePlugin.writeInUABLog(""+str(theCurrentDeviceTypeName)+" Specific Semantic Rules")
	
   	for instance in instancesVector :
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		Remarks = instance.getAttributeData("DeviceDocumentation:Remarks")
		FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type").strip()
		InterfaceParam1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1")
		InterfaceParam2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2")
		InterfaceParam3 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam3")
		InterfaceParam4 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam4")
		InterfaceParam5 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5")
		S_InterfaceParam1 = InterfaceParam1.lower().strip()
		S_InterfaceParam2 = InterfaceParam2.lower().strip()
		S_InterfaceParam3 = InterfaceParam3.lower().strip()
		S_InterfaceParam4 = InterfaceParam4.lower().strip()
		S_InterfaceParam5 = InterfaceParam5.lower().strip()
		nameSize = len(Name)
		theManufacturer = self.thePlugin.getPlcManufacturer()
		# 1. checking the length of the names
		if (theManufacturer.lower() == "siemens") and nameSize > 24:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 24")
			
		elif (theManufacturer.lower() == "schneider") and nameSize > 23:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 23")
						
		# 2. Checking the Analog Thresholds
		HHAlarm = instance.getAttributeData("SCADADeviceAlarms:Analog Thresholds:HH Alarm")
		HWarning = instance.getAttributeData("SCADADeviceAlarms:Analog Thresholds:H Warning")
		LWarning = instance.getAttributeData("SCADADeviceAlarms:Analog Thresholds:L Warning")
		LLAlarm = instance.getAttributeData("SCADADeviceAlarms:Analog Thresholds:LL Alarm")
		RangeMax = self.thePlugin.formatNumberPLC(instance.getAttributeData ("FEDeviceParameters:Range Max").replace(" ",""))
		RangeMin = self.thePlugin.formatNumberPLC(instance.getAttributeData ("FEDeviceParameters:Range Min").replace(" ",""))
		
		# Checking threshold range
		if not self.thePlugin.isString(HHAlarm) and not (min(float(RangeMin),float(RangeMax)) <= float(HHAlarm) <= max(float(RangeMin),float(RangeMax))):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold HHAlarm($HHAlarm$) is out of the range ["+str(min(float(RangeMin),float(RangeMax)))+", "+str(max(float(RangeMin),float(RangeMax)))+"] defined in the FEDeviceParameters")	
		if not self.thePlugin.isString(HWarning) and not (min(float(RangeMin),float(RangeMax)) <= float(HWarning) <= max(float(RangeMin),float(RangeMax))):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold HWarning($HWarning$) is out of the range ["+str(min(float(RangeMin),float(RangeMax)))+", "+str(max(float(RangeMin),float(RangeMax)))+"] defined in the FEDeviceParameters")	
		if not self.thePlugin.isString(LWarning) and not (min(float(RangeMin),float(RangeMax)) <= float(LWarning) <= max(float(RangeMin),float(RangeMax))):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold LWarning($LWarning$) is out of the range ["+str(min(float(RangeMin),float(RangeMax)))+", "+str(max(float(RangeMin),float(RangeMax)))+"] defined in the FEDeviceParameters")	
		if not self.thePlugin.isString(LLAlarm) and not (min(float(RangeMin),float(RangeMax)) <= float(LLAlarm) <= max(float(RangeMin),float(RangeMax))):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold LLAlarm($LLAlarm$) is out of the range ["+str(min(float(RangeMin),float(RangeMax)))+", "+str(max(float(RangeMin),float(RangeMax)))+"] defined in the FEDeviceParameters")	

		# Checking threshold order: HH>H>L>LL
		if not self.thePlugin.isString(HHAlarm): 
			HHAlarm_nbr = float(HHAlarm)
		if not self.thePlugin.isString(HWarning): 
			HWarning_nbr = float(HWarning)		
		if not self.thePlugin.isString(LWarning): 
			LWarning_nbr = float(LWarning)
		if not self.thePlugin.isString(LLAlarm): 
			LLAlarm_nbr = float(LLAlarm)

		if not self.thePlugin.isString(HHAlarm) and not self.thePlugin.isString(HWarning) and (HHAlarm_nbr < HWarning_nbr):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold HHAlarm($HHAlarm$) is not > HWarning($HWarning$)")	

		if not self.thePlugin.isString(HHAlarm) and not self.thePlugin.isString(LWarning) and (HHAlarm_nbr < LWarning_nbr):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold HHAlarm($HHAlarm$) is not > LWarning($LWarning$)")	

		if not self.thePlugin.isString(HHAlarm) and not self.thePlugin.isString(LLAlarm) and (HHAlarm_nbr < LLAlarm_nbr):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold HHAlarm($HHAlarm$) is not > LLAlarm($LLAlarm$)")	
					
		if not self.thePlugin.isString(HWarning) and not self.thePlugin.isString(LWarning) and (HWarning_nbr < LWarning_nbr):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold HWarning($HWarning$) is not > LWarning($LWarning$)")	

		if not self.thePlugin.isString(HWarning) and not self.thePlugin.isString(LLAlarm) and (HWarning_nbr < LLAlarm_nbr):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold HWarning($HWarning$) is not > LLAlarm($LLAlarm$)")	

		if not self.thePlugin.isString(LWarning) and not self.thePlugin.isString(LLAlarm) and (LWarning_nbr < LLAlarm_nbr):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold LWarning($LWarning$) is not > LLAlarm($LLAlarm$)")	
				
		# 3. Checking the FE Encoding Type
		if (theManufacturer.lower() == "siemens"): 
			if (FEType <> "") and (FEType <> "0") and (FEType <> "101") and (FEType <> "102") and (FEType <> "201") and (FEType <> "202") and (FEType <> "203") and (FEType <> "204") and (FEType <> "205") and (FEType <> "103"):
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The FE Encoding Type defined "+FEType+" is not allowed.")
			elif (FEType == "1"):
				if S_InterfaceParam1 == "" or S_InterfaceParam2 == "":
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. InterfaceParam1 and InterfaceParam2 must *both* be defined if the FE Encoding Type is $FEType$")
				else:
					if len(S_InterfaceParam1) > 2 and S_InterfaceParam1[0:2] == "db":
						S_InterfaceParam1 = S_InterfaceParam1[2:]
					if len(S_InterfaceParam2) > 3 and S_InterfaceParam2[0:3] ==  "dbd":
						S_InterfaceParam2 = S_InterfaceParam2[3:]
					if not S_InterfaceParam1.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is DBxx, where xx is a number")
					if not S_InterfaceParam2.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam2 ($InterfaceParam2$) is not well defined. The correct format is DBDxx, where xx is a number")

			elif (FEType == "102") or (FEType == "103"):
				if S_InterfaceParam1 == "" or S_InterfaceParam2 == "" or S_InterfaceParam3 == "" or S_InterfaceParam4 == "" or S_InterfaceParam5 == "":
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. InterfaceParam1 through InterfaceParam5 inclusive must *all* be defined if the FE Encoding Type is $FEType$")
				else:
					if len(S_InterfaceParam1) > 2 and S_InterfaceParam1[0:2] == "db":
						S_InterfaceParam1 = S_InterfaceParam1[2:]
					if len(S_InterfaceParam2) > 3 and S_InterfaceParam2[0:3] ==  "dbd":
						S_InterfaceParam2 = S_InterfaceParam2[3:]
					if len(S_InterfaceParam3) > 2 and S_InterfaceParam3[0:2] == "db":
						S_InterfaceParam3 = S_InterfaceParam3[2:]
					if len(S_InterfaceParam4) > 2 and S_InterfaceParam4[0:3] == "dbx":
						S_InterfaceParam4 = S_InterfaceParam4[3:]

					if not S_InterfaceParam1.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is DBxx, where xx is a number")
					if not S_InterfaceParam2.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam2 ($InterfaceParam2$) is not well defined. The correct format is DBDxx, where xx is a number")
					if not S_InterfaceParam3.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam3 ($InterfaceParam3$) is not well defined. The correct format is DBxx, where xx is a number")
					if not S_InterfaceParam4.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam4 ($InterfaceParam4$) is not well defined. The correct format is DBXxx, where xx is a number")
					if not S_InterfaceParam5.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam5 ($InterfaceParam5$) is not well defined. The correct format is xx, where xx is a number")
			
			elif (FEType == "201") or (FEType == "202") or (FEType == "203") or (FEType == "204") or (FEType == "205"):
				if S_InterfaceParam1 == "":
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 must be defined if the FE Encoding Type is $FEType$")
				else:
					if not S_InterfaceParam1.startswith('pid') and not S_InterfaceParam1.startswith('id'):
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is a DOUBLE WORD (PIDxxx or IDxxx where xxx is a number)")
					else:
						if not S_InterfaceParam1[S_InterfaceParam1.find('id')+2:].isnumeric():
							self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is a DOUBLE WORD (PIDxxx or IDxxx where xxx is a number)")
			
   def end(self):
	self.thePlugin.writeInUABLog("check rules: end")

   def shutdown(self):
	self.thePlugin.writeInUABLog("check rules: shutdown")
		
