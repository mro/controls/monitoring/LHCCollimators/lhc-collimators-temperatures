# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved

import os
import re
from research.ch.cern.unicos.utilities import AbsolutePathBuilder
import xml.etree.ElementTree as ET
from xml.parsers.expat import *
from research.ch.cern.unicos.plugins.interfaces import APlugin

class Finder():
    tree = 0
    root = 0
    plcBlocks = 0
    sources = {}    # project sources
    thePlugin = APlugin.getPluginInterface()

    def isSiemens(self):
        return self.thePlugin.getXMLConfig().getPLCDeclarations().get(0).getClass().getSimpleName() == "SiemensPLC"
    def isSchneider(self):
        return self.thePlugin.getXMLConfig().getPLCDeclarations().get(0).getClass().getSimpleName() == "SchneiderPLC"
    def isCodesys(self):
        return self.thePlugin.getXMLConfig().getPLCDeclarations().get(0).getClass().getSimpleName() == "CodesysPLC"

    def loadSources(self):
        if self.isSiemens():
            return self.loadSources_siemens()
        elif self.isSchneider():
            return self.loadSources_schneider()
        elif self.isCodesys():
            self.thePlugin.writeWarningInUABLog("Finder.loadSources not developed for CodesysPLC yet. Please contact icecontrols.support@cern.ch for more info.")
            return []

    # load sources to memory, Siemens version
    def loadSources_siemens(self):
        try:
            outputFolder = AbsolutePathBuilder.getTechnicalPathParameter("S7LogicGenerator:OutputParameters:OutputFolder")
            output = [outputFolder + filename for filename in os.listdir(outputFolder)]
        
            for file in output:
                self.sources[file] = self.readfile(file)
            self.thePlugin.writeInUABLog("Finder.loadSources_siemens: Sources loaded")
            return True
        except:
            self.thePlugin.writeWarningInUABLog("Finder.loadSources_siemens: Unable to load the sources")
            return False
    
    # load sources to memory, Schneider version    
    def loadSources_schneider(self):
        try:
            self.initTree_schneider()
        
            for program in self.root.findall("program"):
                self.sources[program] = program.findall("STSource")[0].text
            self.thePlugin.writeInUABLog("Finder.loadSources_schneider: Sources loaded")
            return True
        except:
            self.thePlugin.writeWarningInUABLog("Finder.loadSources_schneider: Unable to load the sources")
            return False
        
    def findUsedIn(self, instance):
        if self.isSiemens():
            return self.findUsedIn_siemens(instance)
        elif self.isSchneider():
            return self.findUsedIn_schneider(instance)
        elif self.isCodesys():
            self.thePlugin.writeWarningInUABLog("Finder.findUsedIn not developed for CodesysPLC yet. Please contact icecontrols.support@cern.ch for more info.")
            return []
            
    # findUsedIn for multiple instances
    # names are passed only
    def multiFindUsedIn(self, instancesNames):
        if self.isSiemens():
            return self.multiFindUsedIn_siemens(instancesNames)
        elif self.isSchneider():
            return self.multiFindUsedIn_schneider(instancesNames)
        elif self.isCodesys():
            self.thePlugin.writeWarningInUABLog("Finder.multiFindUsedIn not developed for CodesysPLC yet. Please contact icecontrols.support@cern.ch for more info.")
            return []

    def findUsedIn_siemens(self, instance):
        try:
            outputFolder = AbsolutePathBuilder.getTechnicalPathParameter("S7LogicGenerator:OutputParameters:OutputFolder")
            output = [outputFolder + filename for filename in os.listdir(outputFolder)]
            name = instance.getAttributeData("DeviceIdentification:Name")
            #return [self.getPCONameFromFilename_siemens(file) for file in output if self.grep(file, name)]
            filenames = []
            for file in output:
                #if self.grep(file, name):
                content = self.readfile(file)
                # find whole word using r'\b' word boundary
                match = re.search(r'\b' + name + r'\b', content, re.IGNORECASE)
                if match:
                    #self.thePlugin.writeDebugInUABLog(match.group() + ". Found in " + str(file))
                    filenames.append(file)
            
            myset = set()
            for file in filenames:
                if self.getPCONameFromFilename_siemens(file) not in myset:
                    myset.add(self.getPCONameFromFilename_siemens(file))

            return list(myset)
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findUsedIn_siemens directory " + outputFolder + " : " + ioe.strerror)
            pass # don't want to write warning each time look for a parameter in the log
        return list()
        
    # finds usage of multiple instances
    def multiFindUsedIn_siemens(self, instancesNames):
        try:
            filenames = {}  # dictionary instanceName:[list of files where instance used]
            for name in instancesNames: 
                filenames[name] = []    # empty init
                
            for file in self.sources:   # go through all the sources
                content = self.sources[file]
                
                for name in instancesNames: # go through all the sources             
                    if name.lower() in content.lower(): # pre-check occurence as re.search is slow
                        # find whole word using r'\b' word boundary
                        if re.search(r'\b' + name + r'\b', content, re.IGNORECASE):
                            filenames[name].append(file)    # remember that instance is in this source
            
            finalSet = {}   # dictionary instanceName[list of places where used]
            for name in instancesNames:
                finalSet[name] = set()
                for file in filenames[name]:
                    if self.getPCONameFromFilename_siemens(file) not in finalSet[name]:
                        finalSet[name].add(self.getPCONameFromFilename_siemens(file))
                    
            return finalSet
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findUsedIn_siemens directory " + outputFolder + " : " + ioe.strerror)
            pass # don't want to write warning each time look for a parameter in the log
        return dict()

    def grep(self, file, pattern):
        ff = open(file)
        for line in open(file):
            if pattern.lower() in line.lower():
                ff.close()
                return True
        ff.close()
        return False

    def getPCONameFromFilename_siemens(self, filename):
        return re.sub(r"_[^_]{2,4}.SCL", "", os.path.basename(filename))


    def findUsedIn_schneider(self, instance):
        try:
            self.initTree_schneider()
            name = instance.getAttributeData("DeviceIdentification:Name")
            output = []
            for program in self.root.findall("program"):
                src = program.findall("STSource")[0].text
                if src.lower().find(name.lower()) >= 0: #case in-sensitive
                    blockName = program.findall("identProgram")[0].attrib['name']
                    output.append(blockName)
            
            myset = set()
            for blockName in output:
                if self.getPCONameFromBlockName_schneider(blockName) not in myset:
                    myset.add(self.getPCONameFromBlockName_schneider(blockName))

            return list(myset)
        except ExpatError:
            self.thePlugin.writeWarningInUABLog("Finder.findUsedIn_schneider xml file " + self.getFile_schneider() + " is badly formatted.")
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findUsedIn_schneider xml file " + self.getFile_schneider() + " : " + ioe.strerror)
            pass # don't want to write warning each time look for a parameter in the log
        return list()
        
    # finds usage of multiple instances 
    def multiFindUsedIn_schneider(self, instancesNames):
        try:
        
            blocks = {}     # dictionary instanceName:[list of blocks where used]
            for name in instancesNames:
                blocks[name] = []   # init empty
            
            for program in self.sources:    # go through all the sources
                src = self.sources[program]
                for name in instancesNames: # go through all instances
                    if src.lower().find(name.lower()) >= 0: #case in-sensitive  # if there is occurence in block...
                        blockName = program.findall("identProgram")[0].attrib['name']
                        blocks[name].append(blockName)      # ...remember block name
            
            finalSet = {}   # dictionary instanceName:[places where used]
            for name in instancesNames:
                finalSet[name] = set()
                for blockName in blocks[name]:
                    if self.getPCONameFromBlockName_schneider(blockName) not in finalSet[name]:
                        finalSet[name].add(self.getPCONameFromBlockName_schneider(blockName))

            return finalSet
        except ExpatError:
            self.thePlugin.writeWarningInUABLog("Finder.multiFindUsedIn_schneider xml file " + self.getFile_schneider() + " is badly formatted.")
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findUsedIn_schneider xml file " + self.getFile_schneider() + " : " + ioe.strerror)
            pass # don't want to write warning each time look for a parameter in the log
        return dict()

    def getPCONameFromBlockName_schneider(self, blockName):
        return re.sub(r'_[^_]{2,4}\Z', "", blockName)

    def initTree_schneider(self):
        if self.tree == 0:
            self.tree = ET.parse(self.getFile_schneider())
            self.root = self.tree.getroot()
            self.plcBlocks = map(lambda e : e.attrib['name'] , self.tree.getroot().findall("program/identProgram"))

    def getFile_schneider(self):
        return AbsolutePathBuilder.getTechnicalPathParameter("UnityLogicGenerator:OutputParameters:OutputFolder") + \
            self.thePlugin.getXMLConfig().getTechnicalParameter("UnityLogicGenerator:OutputParameters:OutputFile")

# --------------------------------------------------------
# Functions to find logic assigned to given input pin
# i.e. for code "instanceName.inputPin := XXXXX ;"
# findGivenInput(instance,inputPin) will return XXXXX
# --------------------------------------------------------
    def findGivenInput(self, instance, inputPin):
        if self.isSiemens():
            return self.findGivenInput_siemens(instance, inputPin)
        elif self.isSchneider():
            return self.findGivenInput_schneider(instance, inputPin)
        elif self.isCodesys():
            self.thePlugin.writeWarningInUABLog("Finder.findGivenInput not developed for CodesysPLC yet. Please contact icecontrols.support@cern.ch for more info.")
            return ""
    
    # finds given input for multiple instances
    # instances names are passed
    def multiFindGivenInput(self, instancesNames, inputPin):
        if self.isSiemens():
            return self.multiFindGivenInput_siemens(instancesNames, inputPin)
        elif self.isSchneider():
            return self.multiFindGivenInput_schneider(instancesNames, inputPin)
        elif self.isCodesys():
            self.thePlugin.writeWarningInUABLog("Finder.multiFindGivenInput not developed for CodesysPLC yet. Please contact icecontrols.support@cern.ch for more info.")
            return ""

    def findGivenInput_siemens(self, instance, inputPin):
        try:
            outputFolder = AbsolutePathBuilder.getTechnicalPathParameter("S7LogicGenerator:OutputParameters:OutputFolder")
            output = [outputFolder + filename for filename in os.listdir(outputFolder)]
            name = instance.getAttributeData("DeviceIdentification:Name")
            for file in output:
                content = self.readfile(file)
                logicString = self.find_assignment(name + "\." + inputPin,content)
                if logicString != "":
                    #self.thePlugin.writeDebugInUABLog(name + "." + inputPin + " := " + logicString)
                    return logicString
            return ""
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findGivenInput_siemens directory " + outputFolder + " : " + ioe.strerror)
            pass # don't want to write warning each time look for a parameter in the log
        return ""
        
    def multiFindGivenInput_siemens(self, instancesNames, inputPin):        
        try:
            outputSet = {}
            
            for name in instancesNames: # init empty
                outputSet[name] = ""
            
            for file in self.sources:   # go through all the sources
                content = self.sources[file]
                for name in instancesNames: # go through all the instances
                    if outputSet[name] == "":       # if instance already has input found, skip it
                        if name.lower() in content.lower(): # pre-check, faster than find_assignment
                            outputSet[name] = self.find_assignment(name + "\." + inputPin,content)
                            
            return outputSet
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findGivenInput_siemens directory " + outputFolder + " : " + ioe.strerror)
            pass # don't want to write warning each time look for a parameter in the log
        return dict()   # empty dict in case of fail!

    def readfile(self, file):
        f = open(file)
        content = f.read()
        f.close()
        return content

    def find_assignment(self, variable, content):
        match = re.search(variable + "\s*:=([^;]*);", content, re.MULTILINE)
        if match:
            return "\n".join(match.groups())
        else:
            return ""

    def findGivenInput_schneider(self, instance, inputPin):
        try:
            self.initTree_schneider()
            name = instance.getAttributeData("DeviceIdentification:Name")
            for program in self.root.findall("program"):
                src = program.findall("STSource")[0].text
                logicString = self.find_assignment(name + "_" + inputPin,src)
                if logicString != "":
                    #self.thePlugin.writeDebugInUABLog(name + "." + inputPin + " := " + logicString)
                    return logicString
            return ""
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findGivenInput_siemens directory " + outputFolder + " : " + ioe.strerror)
            pass # don't want to write warning each time look for a parameter in the log
        return ""
    
    # finds given input for multiple instances
    def multiFindGivenInput_schneider(self, instancesNames, inputPin):
        try:
            outputSet = {}
            
            for name in instancesNames: # init empty
                outputSet[name] = ""

            for program in self.sources:    # go through all the sources
                src = self.sources[program] 
                for name in instancesNames: # go through all the instances
                    if outputSet[name] == "":   # if input for isntance already found - skip it
                        if name.lower() in src.lower(): # pre-check, faster than find_assignment
                            outputSet[name] = self.find_assignment(("$name$_$inputPin$"),src)
                            
            return outputSet
        except Exception, ioe:
            #self.thePlugin.writeWarningInUABLog("Finder.findGivenInput_siemens directory " + outputFolder + " : " + ioe.strerror)
            pass # don't want to write warning each time look for a parameter in the log
        return dict()
