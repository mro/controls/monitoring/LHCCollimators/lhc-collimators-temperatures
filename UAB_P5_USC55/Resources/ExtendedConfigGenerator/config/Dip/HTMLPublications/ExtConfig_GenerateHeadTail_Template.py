# Jython source file to create DIP Configs and DIP publications
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from research.ch.cern.unicos.utilities import AbsolutePathBuilder	#REQUIRED
from java.util import Vector
from datetime import datetime
from java.lang import System
import getpass
import os
import platform
import sys


class GenerateHeadTail_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   

   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
   	self.theUnicosProject = self.thePlugin.getUnicosProject()
	self.thePlugin.writeInUABLog("Initialize in Jython for GenerateHeadTail.")


   def check(self):
	self.thePlugin.writeInUABLog("Check in Jython for GenerateHeadTail.")


   def begin(self):
	self.thePlugin.writeInUABLog("Begin in Jython for GenerateHeadTail.")
	projectName 	= self.thePlugin.getConfigParameter("name")
	applicationName = self.thePlugin.getApplicationParameter("GeneralData:ApplicationName")
	pluginId 	= self.thePlugin.getId()
	pluginVersion	= self.thePlugin.getVersionId()
	userName	= getpass.getuser()
	computerName	= os.getenv('COMPUTERNAME')
	currentDate 	= datetime.now()
	generationDate	= str(currentDate.day) + "/" + str(currentDate.month) + "/" + str(currentDate.year) +" " + str(currentDate.hour) + ":" + str(currentDate.minute) + ":" + str(currentDate.second)

	headFilePath = self.thePlugin.getPluginParameter("Services:Dip:OutputFormats:HTMLPublications:HeadFile")
	headFilePath = AbsolutePathBuilder.getAbsolutePath(headFilePath)
	
	tailFilePath = self.thePlugin.getPluginParameter("Services:Dip:OutputFormats:HTMLPublications:TailFile")
	tailFilePath = AbsolutePathBuilder.getAbsolutePath(tailFilePath)
	
	



	headFileContent = '''<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
.header-sitetitle {	
	PADDING-BOTTOM: 1px; MARGIN: 0px; PADDING-LEFT: 5px; PADDING-RIGHT: 0px; FONT-FAMILY: verdana; VERTICAL-ALIGN: middle; PADDING-TOP: 4px; COLOR: #3861aa 
}
TABLE {
	PADDING-BOTTOM: 0px; BORDER-RIGHT-WIDTH: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; BORDER-COLLAPSE: collapse; BORDER-TOP-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 1px; BORDER-LEFT-WIDTH: 1px; PADDING-TOP: 0px
}
TABLE.card {
	BORDER-BOTTOM: 1px solid; BORDER-LEFT: 1px solid; MARGIN-BOTTOM: 4px; BORDER-TOP: 1px solid; BORDER-RIGHT: 1px solid; FONT-FAMILY: verdana; font-size: 12px
}
TR.tableHeader TD {
	BORDER-BOTTOM: 1px solid; HEIGHT: 28px; VERTICAL-ALIGN: middle; BORDER-TOP: 1px solid; FONT-WEIGHT: bold; BORDER-BOTTOM-COLOR: #79a7e3; BACKGROUND-COLOR: #79a7e3; BORDER-TOP-COLOR: #79a7e3; BACKGROUND-REPEAT: repeat-x; BACKGROUND-POSITION: 50% top; COLOR: #003399; FONT-FAMILY: verdana; font-size: 12px
}
TR.dipItem TD {
	BORDER-BOTTOM: 1px solid; HEIGHT: 28px; VERTICAL-ALIGN: middle; BORDER-TOP: 1px solid; FONT-WEIGHT: bold; BORDER-BOTTOM-COLOR: #79a7e3; BACKGROUND-COLOR: #79BBFF; BORDER-TOP-COLOR: #79a7e3; BACKGROUND-REPEAT: repeat-x; BACKGROUND-POSITION: 50% top; COLOR: #003399; FONT-FAMILY: verdana; font-size: 12px
}
TR.managerNumber TD {
	BORDER-BOTTOM: 1px solid; HEIGHT: 28px; VERTICAL-ALIGN: middle; BORDER-TOP: 1px solid; FONT-WEIGHT: bold; BORDER-BOTTOM-COLOR: #79a7e3; BACKGROUND-COLOR: #B7DBFF; BORDER-TOP-COLOR: #79a7e3; BACKGROUND-REPEAT: repeat-x; BACKGROUND-POSITION: 50% top; COLOR: #003399; FONT-FAMILY: verdana; font-size: 12px
}
TR.publicationHeader TD {
	BORDER-BOTTOM: 1px solid; HEIGHT: 28px; VERTICAL-ALIGN: middle; BORDER-TOP: 1px solid; FONT-WEIGHT: bold; BORDER-BOTTOM-COLOR: #79a7e3; BACKGROUND-COLOR: #79DDAA; BORDER-TOP-COLOR: #79a7e3; BACKGROUND-REPEAT: repeat-x; BACKGROUND-POSITION: 50% top; COLOR: #003399; FONT-FAMILY: verdana; font-size: 12px
}
TR.cardHead TH {
	BORDER-BOTTOM: 1px solid; HEIGHT: 28px; BORDER-TOP: 1px solid; FONT-WEIGHT: bold
}
TABLE.card TD {
	PADDING-BOTTOM: 2px; MARGIN: 0px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px
}
TABLE.card TD TD {
	PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px
}
TR.subcard TD {
	BORDER-BOTTOM: 1px solid
}
TR.subcardHead TD {
	BORDER-BOTTOM: 1px solid; BORDER-TOP: 1px solid; FONT-WEIGHT: bold
}
.publicationsTable tr td{
	border-color: #000000; border-width: 1px; border-style: solid; padding: 25px; font-family: verdana; font-size: 12px
}

</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CERN - Dip Publications</title>


</head>

<body>
<div>
    <table cellpadding="0" cellspacing="0" border="0">
	  <tr>
    	    <td width="63"><img src="../../../Resources/$pluginId$/config/Dip/HTMLPublications/cernlogo.jpg" alt="CERN logo" border="0"/></td>
            <td width="915" class="header-sitetitle"><H1 class="header-sitetitle">DIP Publications</H1></td>
          </tr>
    </table>
    <br/>
</div>
<div>

<TABLE >
	  <TBODY>
	    <TR class="tableHeader">
		<TD colSpan="2">Generation Information</TD>
	    </TR>
  	    <TR>
		<TD colSpan="2">
			<table width="100%" border="0" >
			    <tr valign="top" style="border-style:None;">
				<td width="20%"><b>Project:&nbsp;</b></td><td width="80%">$projectName$</td>
			    </tr><tr valign="top" style="border-style:None;">
				<td><b>Application:&nbsp;</b></td><td>$applicationName$</td>
			    </tr><tr valign="top" style="border-style:None;">
				<td><b>Generation Plug-in:&nbsp;</b></td><td>$pluginId$ v$pluginVersion$</td>
			    </tr><tr valign="top" style="border-style:None;">
				<td><b>Generation date:&nbsp;</b></td><td>$generationDate$</td>
			    </tr><tr valign="top" style="border-style:None;">
				<td><b>User:&nbsp;</b></td><td>$userName$</td>
			    </tr><tr valign="top" style="border-style:None;">
				<td><b>Computer:&nbsp;</b></td><td>$computerName$</td>
			    </tr>
			</table></TD>
	    </TR>
	    <TR class="subcardHead">
		<TD colspan="2"><br/>DIP Publications Table</TD>
	    </TR>
	    <TR>
		<TD colSpan="2">
<table rules="all" border="1" id="publicationsTable">'''

	file = open(headFilePath, 'w')
	file.write(str(headFileContent))
	file.close()
	
		
   def process(self, *params): 
    self.thePlugin.writeInUABLog("Process in Jython for GenerateHeadTail.")
    return		

   def end(self):
	self.thePlugin.writeInUABLog("End in Jython for GenerateHeadTail.")


   def shutdown(self):
	self.thePlugin.writeInUABLog("Shutdown in Jython for GenerateHeadTail.")
		
