# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved

from java.util import Vector

class SharedGenericFunctions:
    def writeWarning(self, instance, text):
        if instance == None:
            self.thePlugin.writeWarningInUABLog(str(text))
        else:
            alias = instance.getAttributeData("DeviceIdentification:Name")
            theDeviceType = instance.getDeviceTypeName()
            self.thePlugin.writeWarningInUABLog("$theDeviceType$ instance: $alias$. " + str(text))

    def writeError(self, instance, text):
        if instance == None:
            self.thePlugin.writeErrorInUABLog(str(text))
        else:
            alias = instance.getAttributeData("DeviceIdentification:Name")
            theDeviceType = instance.getDeviceTypeName()
            self.thePlugin.writeErrorInUABLog("$theDeviceType$ instance: $alias$. " + str(text))

    def writeDebug(self, instance, text):
        if instance == None:
            self.thePlugin.writeDebugInUABLog(str(text))
        else:
            alias = instance.getAttributeData("DeviceIdentification:Name")
            theDeviceType = instance.getDeviceTypeName()
            self.thePlugin.writeDebugInUABLog("$theDeviceType$ instance: $alias$. " + str(text))

    def getWordStatusPattern(self, instance):
        ''' Function returns the pattern of WordStatus formatted as an array 
        e.g. for "a=1,b=2" it returns [["a", "1"], ["b", "2"]] '''
        pattern = instance.getAttributeData("SCADADeviceGraphics:Pattern")
        patternList = pattern.split(",") if (pattern != "" and pattern != "-") else []
        patternList = filter(lambda pair: pair.strip() != "", patternList) # remove empty elements
        patternList = map(lambda pair: pair.split("="), patternList) # split k=v
        for pair in patternList:
            while len(pair) < 2: pair.append("")
            pair[0] = pair[0].lower().strip() # format key
            pair[1] = pair[1]                 # format value
        return patternList

    # http://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-in-python
    def is_number(self, s):
        try:
            float(s)
            return True
        except ValueError:
            return False

    # Courtesy of TE-CRG / Tomasz Wolak
    # https://git.cern.ch/web/cryo-controls-lhc-tunnel.git/blob/HEAD:/appskel/app_skel_crg/Resources/SharedTemplates/crgutil.py
    """ ***********************     java <-> python helpers 
    """
    def list_to_java_vector(self,pylist):
        vector = Vector()
        for i in pylist:
            vector.addElement(i)
        return vector


    def java_vector_to_list(self,vector):
        a_list = []
        for i in vector:
            a_list.append(i)
        return a_list
        
    def getPLCName(self):
        thePlcDeclaration = self.getPLCDeclaration()
        if thePlcDeclaration == None:
            return ""
        thePlcName = str(thePlcDeclaration.getPLCName().getValue())
        if thePlcName == "":
            thePlcName = str(thePlcDeclaration.getPLCName().getDefaultValue())
        return thePlcName

    def getPLCDeclaration(self):
    	thePlcDeclarations = self.thePlugin.getXMLConfig().getPLCDeclarations()
        for thePlcDeclaration in thePlcDeclarations:
            if (thePlcDeclaration is not None):
                return thePlcDeclaration
        return None
