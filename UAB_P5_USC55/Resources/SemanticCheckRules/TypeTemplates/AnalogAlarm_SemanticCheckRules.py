# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.utilities import DeviceTypeFactory
from research.ch.cern.unicos.plugins.interfaces import APlugin

class AnalogAlarm_Template(IUnicosTemplate):
   theSemanticVerifier = 0
   thePlugin = 0
   isDataValid = 1
   theCurrentDeviceType = 0
   
   def initialize(self):
   	self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("check rules: initialize")

   def check(self):
	self.thePlugin.writeInUABLog("check rules: check")

   def begin(self):
	self.thePlugin.writeInUABLog("check rules: begin")
	
   def process(self, *params): 
	theCurrentDeviceTypeName = params[0]
	theCurrentDeviceTypeDefinition = params[1]
	theUnicosProject = self.thePlugin.getUnicosProject()
	theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName) 
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	self.thePlugin.writeInUABLog(""+str(theCurrentDeviceTypeName)+" Specific Semantic Rules")
	
	# Take the list with the differents types deffined in the AA deviceTypeDefinition
	deviceInstance = DeviceTypeFactory.getInstance()
	theDeviceTypeDefinition = deviceInstance.getDeviceType(theCurrentDeviceTypeName);		
	deviceTypeFamilies = theDeviceTypeDefinition.getAttributeFamily()
	for deviceTypeFamily in deviceTypeFamilies:
		familyName = deviceTypeFamily.getAttributeFamilyName()
		if familyName == "FEDeviceAlarm":
			attributes = deviceTypeFamily.getAttribute()
			for attribute in attributes:
				attributeName = attribute.getAttributeName()
				if attributeName == None:
					self.thePlugin.writeErrorInUABLog("There is not an attribute in the attributeFamily "+familyName)
					return 
				if attributeName == "Type":
					isSpecificationAttribute = attribute.getIsSpecificationAttribute()
					if isSpecificationAttribute == None:
						self.thePlugin.writeErrorInUABLog("There is not isSpecificationAttribute in the attribute "+attributeName)
						return 
					permittedValueList = isSpecificationAttribute.getPermittedValue()
						
			break
	
	
	# Specific semantic rules
   	for instance in instancesVector :
		Name 		= instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		Remarks 	= instance.getAttributeData("DeviceDocumentation:Remarks")
		HH			= instance.getAttributeData ("FEDeviceManualRequests:HH Alarm").strip()
		LL			= instance.getAttributeData ("FEDeviceManualRequests:LL Alarm").strip()
		H			= instance.getAttributeData ("FEDeviceManualRequests:H Warning").strip()
		L			= instance.getAttributeData ("FEDeviceManualRequests:L Warning").strip()
		I			= instance.getAttributeData ("FEDeviceEnvironmentInputs:Input").strip()
		Delay       = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
		nameSize = len(Name)
		# Checking the length of the names
		theManufacturer = self.thePlugin.getPlcManufacturer()
		if (theManufacturer.lower() == "siemens") and nameSize > 24:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: "+Name+ ". Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 24")
			
		elif (theManufacturer.lower() == "schneider") and nameSize > 23:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: "+Name+ ". Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 23")
							
		# Delay Alarm verification
		if self.thePlugin.isString(Delay) and Delay.strip().lower() <> "logic" and Delay.strip().lower() <> "":
			DelayParamExist = self.theSemanticVerifier.doesObjectExist(Delay, theUnicosProject)
			if DelayParamExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: "+Name+ ". The Delay Alarm $Delay$, defined as an AnalogParameter or an AnalogStatus, doesn't exist in the device "+str(theCurrentDeviceTypeName)+"")
		elif not self.thePlugin.isString(Delay):
			if (theManufacturer.lower() == "siemens") and (round(float(Delay)) != float(Delay)):
				self.thePlugin.writeWarningInUABLog(""+str(theCurrentDeviceTypeName)+" instance: "+Name+ ". The Delay Alarm time $Delay$ sec, is not an integer. It will be rounded to "+str(int(round(float(Delay))))+" sec due to Siemens limitation of non integer delay times")
		
		# Alarm Type verification
		typeAlarm = instance.getAttributeData("FEDeviceAlarm:Type").replace(",", " ")
		alarmMaster = instance.getAttributeData("LogicDeviceDefinitions:Master").replace(",", " ")
		MultipleTypesAlarm = instance.getAttributeData("FEDeviceAlarm:Multiple Types").replace(",", " ")
		
		#Multiple Alarms
		if (typeAlarm == "Multiple"):
			typeAlarmList = MultipleTypesAlarm.split()
			typeAlarmListLen = len(typeAlarmList)

			alarmMasterList = alarmMaster.split()
			alarmMasterListLen = len(alarmMasterList)
			
			# Checking if the numbers of Masters and Types are the same
			if ((typeAlarmListLen <> alarmMasterListLen) and (alarmMaster <> "")):
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: "+Name+ ". The numbers of Masters and Types are not the same.")
				
			# Checking if all the Alarm Types defined in the EXCEL file are defined in the deviceTypeDefinition
			for type in typeAlarmList:
				if type not in permittedValueList:
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: "+Name+ ". The Alarm type "+str(type)+" is not defined in the deviceType")
		#Single Alarm
		else:
			if typeAlarm=="" and alarmMaster<>"":
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: "+Name+ ". When the Alarm master is defined the alarm type is mandatory.")
			# Checking if a master is defined when the alarm is a FS, TS or SI
			if (typeAlarm == "FS" or typeAlarm == "TS" or typeAlarm == "SI") and alarmMaster == "": 
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: "+Name+ ". When the Alarm is a FS, TS or SI the master definition is mandatory.")
			
			if (typeAlarm not in permittedValueList) and typeAlarm<>"":
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: "+Name+ ". The Alarm type $typeAlarm$ is not defined in the deviceType")				
			
			if MultipleTypesAlarm <> "":
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: "+Name+ ". This Alarm has been defined as a simple alarm ($typeAlarm$). Then it's not allowed to add some information in the column: Multiple Types.")				
			
		if self.thePlugin.isString(HH):
			if (HH!="" and HH.lower() != "logic"): 
				HHExist = self.theSemanticVerifier.doesObjectExist(HH, theUnicosProject)
				if HHExist is not True:						
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The HH $HH$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")
	
		if self.thePlugin.isString(H) :
			if (H!="" and H.lower() != "logic"): 
				HExist = self.theSemanticVerifier.doesObjectExist(H, theUnicosProject)
				if HExist is not True:						
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The H $H$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")
		
		if self.thePlugin.isString(L):
			if (L!="" and L.lower() != "logic"): 
				LExist = self.theSemanticVerifier.doesObjectExist(L, theUnicosProject)
				if LExist is not True:						
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The L $L$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")

		if self.thePlugin.isString(LL) :
			if (LL!="" and LL.lower() != "logic"): 
				LLExist = self.theSemanticVerifier.doesObjectExist(LL, theUnicosProject)
				if LLExist is not True:						
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The LL $LL$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")

		if self.thePlugin.isString(I):
			if (I!=""): 
				IExist = self.theSemanticVerifier.doesObjectExist(I, theUnicosProject)
				if IExist is not True:						
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The Input $I$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")		
			
			
		# Checking threshold order: HH>H>L>LL
		if not self.thePlugin.isString(HH): 
			HH_nbr = float(HH)
		if not self.thePlugin.isString(H): 
			H_nbr = float(H)		
		if not self.thePlugin.isString(L): 
			L_nbr = float(L)
		if not self.thePlugin.isString(LL): 
			LL_nbr = float(LL)

		if not self.thePlugin.isString(HH) and not self.thePlugin.isString(H) and (HH_nbr < H_nbr):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold HH($HH$) is not > H($H$)")	

		if not self.thePlugin.isString(HH) and not self.thePlugin.isString(L) and (HH_nbr < L_nbr):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold HH($HH$) is not > L($L$)")	

		if not self.thePlugin.isString(HH) and not self.thePlugin.isString(LL) and (HH_nbr < LL_nbr):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold HH($HH$) is not > LL($LL$)")	
					
		if not self.thePlugin.isString(H) and not self.thePlugin.isString(L) and (H_nbr < L_nbr):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold H($H$) is not > L($L$)")	

		if not self.thePlugin.isString(H) and not self.thePlugin.isString(LL) and (H_nbr < LL_nbr):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold H($H$) is not > LL($LL$)")	

		if not self.thePlugin.isString(L) and not self.thePlugin.isString(LL) and (L_nbr < LL_nbr):
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The threshold L($L$) is not > LL($LL$)")	
					
   def end(self):
	self.thePlugin.writeInUABLog("check rules: end")

   def shutdown(self):
	self.thePlugin.writeInUABLog("check rules: shutdown")
		
