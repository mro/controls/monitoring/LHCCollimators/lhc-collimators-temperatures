# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

class AnaDO_Template(IUnicosTemplate):
   theSemanticVerifier = 0
   thePlugin = 0
   isDataValid = 1
   theCurrentDeviceType = 0
   
   def initialize(self):
   	self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("check rules: initialize")

   def check(self):
	self.thePlugin.writeInUABLog("check rules: check")

   def begin(self):
	self.thePlugin.writeInUABLog("check rules: begin")
	
   def process(self, *params): 
	theCurrentDeviceTypeName = params[0]
	theCurrentDeviceTypeDefinition = params[1]
	theUnicosProject = self.thePlugin.getUnicosProject()
   	theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)	
   	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	self.thePlugin.writeInUABLog(""+str(theCurrentDeviceTypeName)+" Specific Semantic Rules")
	
	# 1. checking the length of the names
   	for instance in instancesVector :
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		Remarks = instance.getAttributeData("DeviceDocumentation:Remarks")
		nameSize = len(Name)
		theManufacturer = self.thePlugin.getPlcManufacturer()
		if (theManufacturer.lower() == "siemens") and nameSize > 19:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 19")
			
		elif (theManufacturer.lower() == "schneider") and nameSize > 23:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 23")
		
		# FEDevice inputs verification
		FeedbackOn = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On").strip()
		LocalDrive = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Drive").strip()
		LocalON = instance.getAttributeData("FEDeviceEnvironmentInputs:Local On").strip()
		LocalOFF = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Off").strip()
		FeedbackAnalog = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Analog").strip()
		HardwareAnalogOutput = instance.getAttributeData("FEDeviceEnvironmentInputs:Hardware Analog Output").strip()
		
		if LocalDrive <> "":
			subString = LocalDrive[0:4].lower()
			if subString == "not ":
				LocalDrive = LocalDrive[4:]
			else:
				LocalDrive = LocalDrive
				
		if FeedbackOn <> "":
			FeedbackOnExist = self.theSemanticVerifier.doesObjectExist(FeedbackOn, theUnicosProject)
			if FeedbackOnExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The FeedbackOn $FeedbackOn$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")
		if LocalDrive <> "":
			LocalDriveExist = self.theSemanticVerifier.doesObjectExist(LocalDrive, theUnicosProject)
			if LocalDriveExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The LocalDrive $LocalDrive$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")
		if FeedbackAnalog <> "":
			FeedbackAnalogExist = self.theSemanticVerifier.doesObjectExist(FeedbackAnalog, theUnicosProject)
			if FeedbackAnalogExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The FeedbackAnalog $FeedbackAnalog$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")		
		if HardwareAnalogOutput <> "":
			HardwareAnalogOutputExist = self.theSemanticVerifier.doesObjectExist(HardwareAnalogOutput, theUnicosProject)
			if HardwareAnalogOutputExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The HardwareAnalogOutput $HardwareAnalogOutput$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")		
		if LocalON <> "":
			LocalONExist = self.theSemanticVerifier.doesObjectExist(LocalON, theUnicosProject)
			if LocalONExist is not True:
				self.thePlugin.writeErrorInUABLog("OnOff instance: $Name$. The LocalON $LocalON$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")		
		if LocalOFF <> "":
			LocalOFFExist = self.theSemanticVerifier.doesObjectExist(LocalOFF, theUnicosProject)
			if LocalOFFExist is not True:
				self.thePlugin.writeErrorInUABLog("OnOff instance: $Name$. The LocalOFF $LocalOFF$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")		

		# FEDevice outputs verification
		ProcessOutput = instance.getAttributeData("FEDeviceOutputs:Analog Process Output").strip()
		if ProcessOutput <> "":
			ProcessOutputExist = self.theSemanticVerifier.doesObjectExist(ProcessOutput, theUnicosProject)
			if ProcessOutputExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The ProcessOutput $ProcessOutput$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")
		
		DigitalProcessOutput = instance.getAttributeData("FEDeviceOutputs:Digital Process Output").strip()
		if DigitalProcessOutput <> "":
			DigitalProcessOutputExist = self.theSemanticVerifier.doesObjectExist(DigitalProcessOutput, theUnicosProject)
			if ProcessOutputExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The DigitalProcessOutput $DigitalProcessOutput$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")
		
			
   def end(self):
	self.thePlugin.writeInUABLog("check rules: end")

   def shutdown(self):
	self.thePlugin.writeInUABLog("check rules: shutdown")
		
