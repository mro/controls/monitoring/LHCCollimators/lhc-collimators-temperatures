# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
import Reverse_Generic_Template
reload(Reverse_Generic_Template)
import Reverse_GlobalFunctions_Template
reload(Reverse_GlobalFunctions_Template)
    
class DigitalOutput_Template(Reverse_Generic_Template.Generic_Template): 
    theDeviceType = "DigitalOutput"
    
    def processInstance(self, instance, generatedInstance):
        key,value = self.getElectricalDiagram(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getSMSCategory(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getMaskEvent(instance)
        generatedInstance.put(key,value)
    
        key,value = self.getAlarmMessage(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getFEType(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getAlarmAcknowledge(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getMasked(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getAlarmOnState(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getArchiveMode(instance)
        generatedInstance.put(key,value)
    
