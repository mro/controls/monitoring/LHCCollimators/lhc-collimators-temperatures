# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

class Controller_Template(IUnicosTemplate):
   theSemanticVerifier = 0
   thePlugin = 0
   isDataValid = 1
   theCurrentDeviceType = 0
   
   def initialize(self):
   	self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("check rules: initialize")

   def check(self):
	self.thePlugin.writeInUABLog("check rules: check")

   def begin(self):
	self.thePlugin.writeInUABLog("check rules: begin")
	
   def process(self, *params): 
	theCurrentDeviceTypeName = params[0]
	theCurrentDeviceTypeDefinition = params[1]
	theUnicosProject = self.thePlugin.getUnicosProject()
   	theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)	
   	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	self.thePlugin.writeInUABLog(""+str(theCurrentDeviceTypeName)+" Specific Semantic Rules")
	
	# 1. checking the length of the names
   	for instance in instancesVector :
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		Remarks = instance.getAttributeData("DeviceDocumentation:Remarks")
		nameSize = len(Name)
		theManufacturer = self.thePlugin.getPlcManufacturer()
		if (theManufacturer.lower() == "siemens") and nameSize > 19:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 19")			
		elif (theManufacturer.lower() == "schneider") and nameSize > 23:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 23")

		
		# FEDevice inputs verification
		MeasuredValue = instance.getAttributeData("FEDeviceEnvironmentInputs:Measured Value").strip()
		if MeasuredValue <> "":
			MeasuredValueExist = self.theSemanticVerifier.doesObjectExist(MeasuredValue, theUnicosProject)
			if MeasuredValueExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The MeasuredValue $MeasuredValue$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")

		# FEDevice outputs verification
		ControlledObjects = instance.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ")
		ControlledObjectsList = ControlledObjects.split()
		for ControlledObject in ControlledObjectsList:
			if ControlledObject <> "":
				ControlledObjectExist = self.theSemanticVerifier.doesObjectExist(ControlledObject, theUnicosProject)
				if ControlledObjectExist is not True:
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The ControlledObject1 $ControlledObject$  doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")
		
		# Output Range verification
		OutputRangeMin = instance.getAttributeData("FEDeviceParameters:Controller Parameters:Output Range Min")
		OutputRangeMax = instance.getAttributeData("FEDeviceParameters:Controller Parameters:Output Range Max")
		if OutputRangeMin == "" and OutputRangeMax <> "":
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. If the Output Range Max is defined, the Output Range Min must be defined.")
		if OutputRangeMin <> "" and OutputRangeMax == "":
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. If the Output Range Min is defined, the Output Range Max must be defined.")
		
			
   def end(self):
	self.thePlugin.writeInUABLog("check rules: end")

   def shutdown(self):
	self.thePlugin.writeInUABLog("check rules: shutdown")
		
