# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

class DigitalOutput_Template(IUnicosTemplate):
   theSemanticVerifier = 0
   thePlugin = 0
   isDataValid = 1
   theCurrentDeviceType = 0
   
   def initialize(self):
   	self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("check rules: initialize")

   def check(self):
	self.thePlugin.writeInUABLog("check rules: check")

   def begin(self):
	self.thePlugin.writeInUABLog("check rules: begin")

   def process(self, *params): 
	theCurrentDeviceTypeName = params[0]
	theCurrentDeviceTypeDefinition = params[1]
	theUnicosProject = self.thePlugin.getUnicosProject()
   	theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)	
   	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	self.thePlugin.writeInUABLog(""+str(theCurrentDeviceTypeName)+" Specific Semantic Rules")
	
   	for instance in instancesVector :
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		Remarks = instance.getAttributeData("DeviceDocumentation:Remarks")
		FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type").strip()
		InterfaceParam1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1")
		InterfaceParam2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2")
		InterfaceParam3 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam3")
		InterfaceParam4 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam4")
		InterfaceParam5 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5")
		InterfaceParam6 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam6")
		S_InterfaceParam1 = InterfaceParam1.lower().strip()
		S_InterfaceParam2 = InterfaceParam2.lower().strip()
		S_InterfaceParam3 = InterfaceParam3.lower().strip()
		S_InterfaceParam4 = InterfaceParam4.lower().strip()
		S_InterfaceParam5 = InterfaceParam5.lower().strip()
		S_InterfaceParam6 = InterfaceParam6.lower().strip()
		nameSize = len(Name)
		theManufacturer = self.thePlugin.getPlcManufacturer()
		# 1. checking the length of the names
		if (theManufacturer.lower() == "siemens") and nameSize > 24:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 24")
			
		elif (theManufacturer.lower() == "schneider") and nameSize > 23:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 23")
						
		# 2. Checking the FE Encoding Type
		if (theManufacturer.lower() == "siemens"):
			if (FEType <> "") and (FEType <> "0") and (FEType <> "1") and (FEType <> "101") and (FEType <> "102") and (FEType <> "103"):
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The FE Encoding Type defined "+FEType+" is not allowed.")
			elif (FEType == "1"):
				if S_InterfaceParam1 == "":
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 must be defined if the FE Encoding Type is $FEType$")
				else:
					pointIndex = S_InterfaceParam1.find('.')
					if S_InterfaceParam1[0] <> "q" or pointIndex == -1:
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is Qxx.y, where xx is the Byte and y is the Bit")
					else:
						Byte = S_InterfaceParam1[1:pointIndex]
						Bit = S_InterfaceParam1[pointIndex + 1:]
						if not Byte.isnumeric() or not Bit.isnumeric() or len(Bit)>1 or int(Bit)<0 or int(Bit)>7:
							self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is Qxx.y, where xx is the Byte and y is the Bit")

			elif (FEType == "101"):
				if S_InterfaceParam1 == "" or S_InterfaceParam2 == "" or S_InterfaceParam3 == "":
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. InterfaceParam1, InterfaceParam2, and InterfaceParam3 must *all* be defined if the FE Encoding Type is $FEType$")
				else:
					if len(S_InterfaceParam1) > 2 and S_InterfaceParam1[0:2] == "db":
						S_InterfaceParam1 = S_InterfaceParam1[2:]
					if len(S_InterfaceParam2) > 3 and S_InterfaceParam2[0:3] ==  "dbx":
						S_InterfaceParam2 = S_InterfaceParam2[3:]

					if not S_InterfaceParam1.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is DBxx, where xx is a number")
					if not S_InterfaceParam2.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam2 ($InterfaceParam2$) is not well defined. The correct format is DBXxx, where xx is a number")
					if not S_InterfaceParam3.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam3 ($InterfaceParam3$) is not well defined. This value must be a number")

			elif (FEType == "102") or (FEType == "103"):
				if S_InterfaceParam1 == "" or S_InterfaceParam2 == "" or S_InterfaceParam3 == "" or S_InterfaceParam4 == "" or S_InterfaceParam5 == "" or S_InterfaceParam6 == "":
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. InterfaceParam1 through InterfaceParam6 inclusive must *all* be defined if the FE Encoding Type is $FEType$")
				else:
					if len(S_InterfaceParam1) > 2 and S_InterfaceParam1[0:2] == "db":
						S_InterfaceParam1 = S_InterfaceParam1[2:]
					if len(S_InterfaceParam2) > 3 and S_InterfaceParam2[0:3] ==  "dbx":
						S_InterfaceParam2 = S_InterfaceParam2[3:]
					if len(S_InterfaceParam4) > 2 and S_InterfaceParam4[0:2] == "db":
						S_InterfaceParam4 = S_InterfaceParam4[2:]
					if len(S_InterfaceParam5) > 3 and S_InterfaceParam5[0:3] ==  "dbx":
						S_InterfaceParam5 = S_InterfaceParam5[3:]

					if not S_InterfaceParam1.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is DBxx, where xx is a number")
					if not S_InterfaceParam2.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam2 ($InterfaceParam2$) is not well defined. The correct format is DBXxx, where xx is a number")
					if not S_InterfaceParam3.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam3 ($InterfaceParam3$) is not well defined. This value must be a number")
					if not S_InterfaceParam4.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam4 ($InterfaceParam4$) is not well defined. The correct format is DBxx, where xx is a number")
					if not S_InterfaceParam5.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam5 ($InterfaceParam5$) is not well defined. The correct format is DBXxx, where xx is a number")
					if not S_InterfaceParam6.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam6 ($InterfaceParam6$) is not well defined. This value must be a number")

	
   def end(self):
	self.thePlugin.writeInUABLog("check rules: end")

   def shutdown(self):
	self.thePlugin.writeInUABLog("check rules: shutdown")
		
