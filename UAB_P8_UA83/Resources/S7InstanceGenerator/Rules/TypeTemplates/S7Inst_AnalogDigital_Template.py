# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class AnalogDigital_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for ANADIG.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for ANADIG.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for ANADIG.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'

   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for ANADIG.")
		
	# General Steps for Non-Optimized objects:
	# 1. DB creation for each instance
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from the TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5: Create the Function. This function writes the input signals on each instance DB.
	# 		  For each DB we execute the FB.DB 
	# 		  And finally we pass the Status information from this instance DB to the corresponding Status DB (e.g DB_bin_status_ANADIG)
	
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	# This method is called on every Instance of the current type by the Code Generation plug-in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	
	
	# Step 1: DB creation and initialization for each instance	
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Analog Digital DB Creation file: UNICOS application
''')

	for instance in instancesVector :
		if (instance is not None):
			
			Name = instance.getAttributeData("DeviceIdentification:Name")
			Description = instance.getAttributeData("DeviceDocumentation:Description")
			ProcessOutputOn = instance.getAttributeData ("FEDeviceOutputs:Output On")
			ProcessOutputOff = instance.getAttributeData ("FEDeviceOutputs:Output Off")
			LocalDrive = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Drive")
			FeedbackAnalog = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Analog")
			FeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On")
			FeedbackOff = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Off")
			FailSafeOpen = instance.getAttributeData	("FEDeviceParameters:ParReg:Fail-Safe")
			POutMain = instance.getAttributeData	("FEDeviceParameters:ParReg:Outputs Maintained")
			PPWMMode = instance.getAttributeData	("FEDeviceParameters:ParReg:PWM Mode")
			PEnRstart = instance.getAttributeData	("FEDeviceParameters:ParReg:Manual Restart after Full Stop")
			RangeMax = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max"))
			RangeMin = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min"))
			StepInc = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Manual Increase Step (Unit)"))
			StepDec = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Manual Decrease Step (Unit)"))
			ManualIncSpeed = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Manual Increase Speed (Unit/s)"))
			ManualDecSpeed = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Manual Decrease Speed (Unit/s)"))
			WarningDelay = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Warning Time Delay (s)"))
			LimitOff = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceManualRequests:Parameter Limit Off/Closed"))
			LimitOn = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceManualRequests:Parameter Limit On/Open"))
			PulsePeriod = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:PWM Parameters:Time Period (s)"))
			MinimumDuration = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:PWM Parameters:Minimum Duration (s)"))
			MaxDeviation = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:PWM Parameters:Max Deviation"))
			HardwareAnalogOutput = instance.getAttributeData ("FEDeviceEnvironmentInputs:Hardware Analog Output")
		
			# New parameter for initialization
			Deadband	  	= self.thePlugin.formatNumberPLC(instance.getAttributeData ("FEDeviceParameters:Warning Deadband Value (Unit)"))
			
			# Default values
			if WarningDelay.strip() == "":
				WarningDelay = "5.0"
				self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined WarningDelay, WarningDelay = $WarningDelay$ is taken.")
			if Deadband.strip() == "":
				Deadband = str((float(RangeMax)-float(RangeMin))/100)
				self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined Deadband, Deadband = $Deadband$ is taken.")
			if ManualIncSpeed.strip() == "":
				ManualIncSpeed = str((float(RangeMax)-float(RangeMin))/10)
				self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined ManualIncSpeed, ManualIncSpeed = $ManualIncSpeed$ is taken.")
			if ManualDecSpeed.strip() == "":
				ManualDecSpeed = str((float(RangeMax)-float(RangeMin))/10)
				self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined ManualDecSpeed, ManualDecSpeed = $ManualDecSpeed$ is taken.")
			if StepInc.strip() == "":
				StepInc = str((float(RangeMax)-float(RangeMin))/100)
				self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined StepInc, StepInc = $StepInc$ is taken.")
			if StepDec.strip() == "":
				StepDec = str((float(RangeMax)-float(RangeMin))/100)
				self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined StepDec, StepDec = $StepDec$ is taken.")
			if PulsePeriod.strip() == "":
				PulsePeriod = "1.0"
				self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined PulsePeriod, PulsePeriod = $PulsePeriod$ is taken.")
			if MinimumDuration.strip() == "":
				MinimumDuration = "0.0"
				self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined MinimumDuration, MinimumDuration = $MinimumDuration$ is taken.")
			if MaxDeviation.strip() == "":
				MaxDeviation = str(float(RangeMax) - float(RangeMin))
				self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined MaxDeviation, MaxDeviation = $MaxDeviation$ is taken.")
			
			#PLiOn/Off default :  +/- 10% PMaxRan/PMinRan
			if LimitOn.strip() == "":
				LimitOn = str(float(RangeMax) - 0.1*(float(RangeMax)-float(RangeMin)))
				self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined LimitOn, LimitOn = $LimitOn$ is taken.")
			if LimitOff.strip()  == "":
				LimitOff = str(float(RangeMin) + 0.1*(float(RangeMax)-float(RangeMin)))
				self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined LimitOff, LimitOff = $LimitOff$ is taken.")
			
			# ParReg Logic
				
			if (FailSafeOpen.lower()=="off/close"):
				bit8='0'
			else:
				bit8='1'				
				
			if FeedbackOn == "":
				bit9='0'
			else:
				bit9='1'				
			
			if FeedbackOff == "":
				bit10='0'
			else:
				bit10='1'
			
			if FeedbackAnalog == "":
				bit11='0'
			else:
				bit11='1'				
				
			if LocalDrive == "":
				bit12='0'
			else:
				bit12='1'		

			if HardwareAnalogOutput == "":
				bit13='0'
			else:
				bit13='1'				
				
			if PPWMMode.lower() == "classic" or PPWMMode == "":
				bit14='0'
			else:
				bit14='1'

			if POutMain.lower() == "false" or POutMain == "":
				bit15='0'
			else:
				bit15='1'
				
			if PEnRstart.lower() == "false":
				bit0='0'
				bit1='0'				
			elif (PEnRstart.lower()=="true only if full stop disappeared"):
				bit0='1'
				bit1='0'
			else:
				bit0='1'
				bit1='1'
				
				
		
			self.thePlugin.writeInstanceInfo('''
DATA_BLOCK '''+Name+''' CPC_FB_ANADIG
BEGIN

	PAnalog.PArReg :=  2#00000$bit1$$bit0$$bit15$$bit14$$bit13$$bit12$$bit11$$bit10$$bit9$$bit8$;
	
	PAnalog.PMaxRan := $RangeMax$;
	PAnalog.PMinRan := $RangeMin$;
	PAnalog.PMStpInV := $StepInc$;
	PAnalog.PMStpDeV := $StepDec$;
	PAnalog.PMinSpd := $ManualIncSpeed$;
	PAnalog.PMDeSpd := $ManualDecSpeed$;
	PAnalog.PWDt := T#$WarningDelay$s;
	PAnalog.PWDb := $Deadband$;
	PPWM.PTPeriod := T#$PulsePeriod$s;
	PPWM.PTMin := T#$MinimumDuration$s;
	PPWM.PInMAx := $MaxDeviation$;
	
END_DATA_BLOCK
''')	
	

	
	# Step 3: Create all the needed Structures from the TCT
	
	# UDT for DB_ANADIG_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE ANADIG_ManRequest
TITLE = ANADIG_ManRequest
//
// parameters of ANADIG Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				attributePrimitiveType = attribute.getPrimitiveType()
				attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
				
				self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')



	# UDT for all the bin status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE ANADIG_bin_Status
TITLE = ANADIG_bin_Status
//
// parameters of ANADIG Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE ANADIG_event
TITLE = ANADIG_event
//
// parameters of ANADIG Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				# isEventAttribute = attribute.getIsEventAttribute()
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# UDT for all the ana status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE ANADIG_ana_Status
TITLE = ANADIG_ana_Status
//
// parameters of ANADIG Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')



	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_ANADIG_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_ANADIG_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_ANADIG_ManRequest
TITLE = DB_ANADIG_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    ANADIG_Requests : ARRAY [1..'''+instanceNb+'''] OF ANADIG_ManRequest;
END_STRUCT;
BEGIN
''')
	RecordNumber = 0
	for instance in instancesVector :								
		RecordNumber = RecordNumber + 1		
		Name = instance.getAttributeData("DeviceIdentification:Name")
		LimitOff = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceManualRequests:Parameter Limit Off/Closed"))
		LimitOn = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceManualRequests:Parameter Limit On/Open"))
		
		#PLiOn/Off default :  +/- 10% PMaxRan/PMinRan
		if LimitOn.strip() == "":
			LimitOn = str(float(RangeMax) - 0.1*(float(RangeMax)-float(RangeMin)))
			self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined LimitOn, LimitOn = $LimitOn$ is taken.")
		if LimitOff.strip()  == "":
			LimitOff = str(float(RangeMin) + 0.1*(float(RangeMax)-float(RangeMin)))
			self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined LimitOff, LimitOff = $LimitOff$ is taken.")

		self.thePlugin.writeInstanceInfo('''
	ANADIG_Requests['''+str(RecordNumber)+'''].PliOff:= $LimitOff$;
	ANADIG_Requests['''+str(RecordNumber)+'''].PliOn:= $LimitOn$;
''')



	self.thePlugin.writeInstanceInfo('''  
END_DATA_BLOCK

''')



	# Step 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 

	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_ANADIG Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_ANADIG
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type ANADIG
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_ANADIG : INT := '''+str(NbType)+''';
	ANADIG_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF ANADIG_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT
BEGIN
END_DATA_BLOCK

''')

	
	self.thePlugin.writeInstanceInfo('''
	
(*binaries Status of the ANADIG OBJECTS************************************************)
DATA_BLOCK DB_bin_status_ANADIG
TITLE = 'DB_bin_status_ANADIG'
//
// Global binary status DB of ANADIG
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : ANADIG_bin_Status;
''')


	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK

(*old binaries Status of the ANADIG OBJECTS************************************************)
DATA_BLOCK DB_bin_status_ANADIG_old
TITLE = 'DB_bin_status_ANADIG_old'
//
// Old Global binary status DB of ANADIG
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
	''')
	
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : ANADIG_bin_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK

(*analogic Status of the ANADIG OBJECTS************************************************)

DATA_BLOCK DB_ana_status_ANADIG
TITLE = 'DB_ana_status_ANADIG'
//
// Global analogic status DB of ANADIG
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : ANADIG_ana_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK


(*old analogic Status of the ANADIG OBJECTS************************************************)

DATA_BLOCK DB_ana_status_ANADIG_old
TITLE = 'DB_ana_status_ANADIG_old'
//
// Old Global analogic status DB of ANADIG
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : ANADIG_ana_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK
''')

	# Step 5: Create the Function. This function writes the input signals on each instance DB.
	# For each DB we execute the FB.DB 
	# and finally we pass the Status information from this instance DB to the corresponding Status DB (e.g DB_bin_status_ANADIG)

	self.thePlugin.writeInstanceInfo('''


(*EXEC of ANADIG************************************************)
FUNCTION FC_ANADIG : VOID
TITLE = 'FC_ANADIG'
//
// ANADIG calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_ANA'
FAMILY: 'AnaDig'
VAR_TEMP
	old_status1 : DWORD;
	old_status2 : DWORD;
END_VAR
BEGIN
''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		HardwareAnalogOutput = instance.getAttributeData ("FEDeviceEnvironmentInputs:Hardware Analog Output")
		LocalDrive = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Drive")
		FeedbackAnalog = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Analog")
		FeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On")
		FeedbackOff = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Off")
		FailSafeOpen = instance.getAttributeData	("FEDeviceParameters:ParReg:Fail-Safe")
		OutputOn = instance.getAttributeData("FEDeviceOutputs:Output On")
		OutputOff = instance.getAttributeData("FEDeviceOutputs:Output Off")
		
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
// ----------------------------------------------
// ---- Anadig <'''+ str(RecordNumber) +'''>: $Name$
// ----------------------------------------------
old_status1 := DB_bin_status_ANADIG.$Name$.stsreg01;
old_status2 := DB_bin_status_ANADIG.$Name$.stsreg02;
old_status1 := ROR(IN:=old_status1, N:=16);
old_status2 := ROR(IN:=old_status2, N:=16);

$Name$.ManReg01:=DB_ANADIG_ManRequest.ANADIG_Requests['''+ str(RecordNumber) +'''].ManReg01;
$Name$.MPosR:=DB_ANADIG_ManRequest.ANADIG_Requests['''+ str(RecordNumber) +'''].MPosR;
$Name$.PLiOn:=DB_ANADIG_ManRequest.ANADIG_Requests['''+ str(RecordNumber) +'''].PLiOn;
$Name$.PLiOff:=DB_ANADIG_ManRequest.ANADIG_Requests['''+ str(RecordNumber) +'''].PLiOff;

''')

	
		if FeedbackOn <> "":
			s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HFOn:='''+s7db_id_result+FeedbackOn+'''.PosSt;''')
			
			
			
		if FeedbackOff <> "":
			s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HFOff:='''+s7db_id_result+FeedbackOff+'''.PosSt;''')


		if FeedbackAnalog <> "":
			s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal, AnalogStatus")
			self.thePlugin.writeInstanceInfo('''
$Name$.HFPos:='''+s7db_id_result+FeedbackAnalog+'''.PosSt;''')
			
			
		if LocalDrive <> "":
			NotLocal = LocalDrive[0:4].lower()
			if NotLocal == "not ":
				LocalDrive = LocalDrive[4:]
			else:
				NotLocal = ""
				LocalDrive = LocalDrive
				
			s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HLD:='''+NotLocal+s7db_id_result+LocalDrive+'''.PosSt;''')
			
					
			
		self.thePlugin.writeInstanceInfo('''
		
// IO Error
''')

		if (FeedbackOn == "" and FeedbackOff == "" and FeedbackAnalog == "" and LocalDrive == "" and OutputOn == "" and OutputOff == ""):
			self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOError
// No FeedbackOn, FeedbackOff, FeedbackAnalog, LocalDrive, OutputOn and OutputOff configured
''')
		else:
			self.thePlugin.writeInstanceInfo(Name+'''.IoError := ''')
			NbIOError = 0
			if FeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOn+'''.IoErrorW''')
				NbIOError = NbIOError +1 
			
			if FeedbackOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOff+'''.IoErrorW''')
				NbIOError = NbIOError +1 
				
			if FeedbackAnalog <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackAnalog+'''.IoErrorW''')
				NbIOError = NbIOError +1 
				
			if LocalDrive <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalDrive+'''.IoErrorW''')
				NbIOError = NbIOError +1 
				
			if OutputOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(OutputOn, "DigitalOutput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+OutputOn+'''.IoErrorW''')
				NbIOError = NbIOError +1 
				
			if OutputOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(OutputOff, "DigitalOutput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+OutputOff+'''.IoErrorW''')
				NbIOError = NbIOError +1 
				
			self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoError;
		
// IOSimu
''')
		if (FeedbackOn == "" and FeedbackOff == "" and FeedbackAnalog == "" and LocalDrive == "" and OutputOn == "" and OutputOff == ""):
			self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOSimu
// No FeedbackOn, FeedbackOff, FeedbackAnalog, LocalDrive, OutputOn and OutputOff configured
''')
		else:
			self.thePlugin.writeInstanceInfo(Name+'''.IoSimu := ''')
			NbIOSImu = 0
			if FeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOn + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOn+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
			
			if FeedbackOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOff + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOff+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if FeedbackAnalog <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackAnalog + '''.IoSimuW OR ''' + s7db_id_result+FeedbackAnalog+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
			
			if LocalDrive <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result + LocalDrive + '''.IoSimuW OR ''' + s7db_id_result+LocalDrive+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if OutputOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(OutputOn, "DigitalOutput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result + OutputOn + '''.IoSimuW OR ''' + s7db_id_result+OutputOn+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if OutputOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(OutputOff, "DigitalOutput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result + OutputOff + '''.IoSimuW OR ''' + s7db_id_result+OutputOff+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
			
			self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoSimu;
''')

			self.thePlugin.writeInstanceInfo(''';

// Calls the Baseline function			
CPC_FB_ANADIG.$Name$();
''')
		
		if OutputOn <> "":
			s7db_id_result=self.thePlugin.s7db_id(OutputOn, "DigitalOutput")
			self.thePlugin.writeInstanceInfo(s7db_id_result + OutputOn + '''.AuposR := '''+Name+'''.DOutOnOV;
			''')
			
		if OutputOff <> "":
			s7db_id_result=self.thePlugin.s7db_id(OutputOff, "DigitalOutput")
			self.thePlugin.writeInstanceInfo(s7db_id_result + OutputOff + '''.AuposR := '''+Name+'''.DOutOffOV;
			''')	
			
		self.thePlugin.writeInstanceInfo('''
//Reset AuAuMoR and AuAlAck
$Name$.AuAuMoR := FALSE;	
$Name$.AuAlAck := FALSE;	
		
//Recopy new status
DB_bin_status_ANADIG.$Name$.stsreg01:= $Name$.Stsreg01;
DB_bin_status_ANADIG.$Name$.stsreg02:= $Name$.Stsreg02;
DB_ana_status_ANADIG.$Name$.PosSt:= $Name$.PosSt;
DB_ana_status_ANADIG.$Name$.AuPosRSt:= $Name$.AuPosRSt;
DB_ana_status_ANADIG.$Name$.MPosRSt:= $Name$.MPosRSt;
DB_ana_status_ANADIG.$Name$.PosRSt:= $Name$.PosRSt;
DB_ana_status_ANADIG.$Name$.PosSt:= $Name$.PosSt;
DB_Event_ANADIG.ANADIG_evstsreg['''+str(RecordNumber)+'''].evstsreg01 := old_status1 OR DB_bin_status_ANADIG.$Name$.stsreg01;
DB_Event_ANADIG.ANADIG_evstsreg['''+str(RecordNumber)+'''].evstsreg02 := old_status2 OR DB_bin_status_ANADIG.$Name$.stsreg02;
''')

	self.thePlugin.writeInstanceInfo(''' 
	
	
END_FUNCTION''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for ANADIG.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for ANADIG.")
		
