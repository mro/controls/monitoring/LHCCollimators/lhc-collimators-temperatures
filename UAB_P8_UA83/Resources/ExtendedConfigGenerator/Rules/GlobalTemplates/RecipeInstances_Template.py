# Jython source file to create an example of the Recipe Instances file
from research.ch.cern.unicos.plugins.extendedconfig.recipes.recipeinstance import RcpInstance
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED


class RecipeInstances_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0
    recipeService = 0
    instances = 0
    defaultAlarmValues = 0
    defaultControllerValues = 0
    allDeviceTypesString = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize in Jython for SelectInstances.")
        self.recipeService = self.thePlugin.getServiceInstance("Recipes")
        self.instances = self.recipeService.getInstances()
        self.defaultAlarmValues = {
            "HH": "FEDeviceManualRequests:HH Alarm",
            "H": "FEDeviceManualRequests:H Warning",
            "L": "FEDeviceManualRequests:L Warning",
            "LL": "FEDeviceManualRequests:LL Alarm"
        }
        self.defaultControllerValues = {
            "MSP": "FEDeviceVariables:Default PID Parameters:Setpoint",
            "MSPH": "FEDeviceVariables:Default PID Parameters:SP High Limit",
            "MSPL": "FEDeviceVariables:Default PID Parameters:SP Low Limit",
            "MOutH": "FEDeviceVariables:Default PID Parameters:Out High Limit",
            "MOutL": "FEDeviceVariables:Default PID Parameters:Out Low Limit",
            "MKc": "FEDeviceVariables:Default PID Parameters:Kc",
            "MTi": "FEDeviceVariables:Default PID Parameters:Ti",
            "MTd": "FEDeviceVariables:Default PID Parameters:Td",
            "MTds": "FEDeviceVariables:Default PID Parameters:Tds"
        }
        self.allDeviceTypesString = self.instances.getAllDeviceTypesString()

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for SelectInstances.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for SelectInstances.")

    def process(self, *params):
        self.thePlugin.writeInUABLog("processInstances in Jython for SelectInstances.")

        # Get the input parameters
        rcpClasses = params[0]
        rcpInstanceRoot = params[1]
        self.processRecipeClasses(rcpClasses, rcpInstanceRoot)

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for SelectInstances.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for SelectInstances.")

    def processRecipeClasses(self, rcpClasses, root):

        for rcpClass in rcpClasses:
            # Skip if the recipe class definition is not valid
            if (False == rcpClass.isDefinitionValid()):
                continue

            # Create an instance of the recipe class
            rcpType = rcpClass.getRecipeType()
            rcpInst = RcpInstance()
            rcpInst.setInstanceName(rcpClass.getClassName() + "_Default")
            rcpInst.setClassName(rcpClass.getClassName())
            rcpInst.setDescription(rcpClass.getClassDesc())
            rcpInst.setInitialRecipe(True)
            rcpInst.setDeviceType("-")
            root.getRcpInstance().add(rcpInst)

            # Iterates through all the devices in the recipe class
            for deviceAlias in rcpClass.getAllDeviceAlias():
                deviceTypes = rcpType.getAllowedDeviceTypesString()
                device = self.theUnicosProject.findInstanceByNameOrExpertName(deviceAlias, deviceTypes)

                if (device is None):
                    self.thePlugin.writeWarningInUABLog("The device " + deviceAlias + " doesn't exist.")
                    self.thePlugin.writeInUABLog(
                        "The device will not be included in the recipe 'Instance1' of the class " + rcpClass.getClassName())

                # Get the device expert name
                expertName = device.getAttributeData("DeviceIdentification:Expert Name")
                if expertName is None or expertName == "":
                    expertName = deviceAlias

                # Iterates through all the DPEs of the device
                i = 0
                for dpe in rcpClass.getDeviceDpes(expertName):
                    rcpInst = RcpInstance()

                    # Get the 'short' name of the DPE (like "HH", "H", etc.)
                    pos = dpe.rfind(".")
                    if (pos >= 0):
                        dpe = dpe[pos + 1:]

                    if (i == 0):
                        rcpInst.setDeviceType(device.getDeviceType().getDeviceTypeName())
                        rcpInst.setDeviceAlias(deviceAlias)

                    # Get the device data (defaultValue, rangeMin, rangeMax and unit)
                    deviceData = self.getDeviceData(device, dpe)
                    defaultValue = deviceData[0]
                    rangeMin = deviceData[1]
                    rangeMax = deviceData[2]
                    unit = deviceData[3]

                    # Set the device data in the recipe instance
                    rcpInst.setValue(defaultValue)
                    if (rangeMin != "" and rangeMax != ""):
                        rcpInst.setRange("[" + rangeMin + ", " + rangeMax + "]")
                    rcpInst.setUnit(unit)

                    rcpInst.setDpe(dpe)
                    root.getRcpInstance().add(rcpInst)
                    i += 1

    def getDeviceData(self, device, dpe):

        deviceType = device.getDeviceType().getDeviceTypeName()

        if deviceType == "DigitalParameter" or deviceType == "WordParameter" or deviceType == "AnalogParameter":
            list = self.getXParameterData(device)
        elif deviceType == "AnalogAlarm":
            list = self.getAnalogAlarmData(device, dpe)
        elif deviceType == "Controller":
            list = self.getControllerData(device, dpe)
        return list

    def getXParameterData(self, device):

        defaultValue = device.getAttributeData("FEDeviceParameters:Default Value")
        rangeMin = ""
        rangeMax = ""
        unit = ""
        if (device.getDeviceType().getDeviceTypeName() != "DigitalParameter"):
            rangeMin = device.getAttributeData("FEDeviceParameters:Range Min")
            rangeMax = device.getAttributeData("FEDeviceParameters:Range Max")
            unit = device.getAttributeData("SCADADeviceGraphics:Unit")

        return [defaultValue, rangeMin, rangeMax, unit]

    def getAnalogAlarmData(self, device, dpe):

        tag = self.defaultAlarmValues.get(dpe)
        defaultValue = device.getAttributeData(tag)

        inputDeviceName = device.getAttributeData("FEDeviceEnvironmentInputs:Input")
        inputDevices = self.instances.findMatchingInstances(self.allDeviceTypesString,
                                                            "'#DeviceIdentification:Name#'='" + inputDeviceName + "'")
        if (inputDevices is not None and inputDevices.size() > 0 and inputDevices.get(
                0).getDeviceTypeName() != "MassFlowController"):
            inputDevice = inputDevices.get(0)
            rangeMin = inputDevice.getAttributeData("FEDeviceParameters:Range Min")
            rangeMax = inputDevice.getAttributeData("FEDeviceParameters:Range Max")
            unit = inputDevice.getAttributeData("SCADADeviceGraphics:Unit")
        else:
            rangeMin = ""
            rangeMax = ""
            unit = ""

        return [defaultValue, rangeMin, rangeMax, unit]

    def getControllerData(self, device, dpe):
        tag = self.defaultControllerValues.get(dpe)
        defaultValue = device.getAttributeData(tag)
        return [defaultValue, "", "", ""]
