# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved

class GlobalFunctions_Template():
    
    def getNameFromAliasDeviceLinkList(self, instance):
        index = instance["aliasDeviceLinkList"].find(",")
        if index != -1:
            value = instance["aliasDeviceLinkList"][:index]
        else:
            value = instance["aliasDeviceLinkList"]
        key = "DeviceIdentification.Name"
        return key,value
    
    def getDeviceLinkListFromAliasDeviceLinkList(self, instance):
        index = instance["aliasDeviceLinkList"].find(",")
        if index != -1:
            value = instance["aliasDeviceLinkList"][index+1:]
        else:
            value = ""
        key = "SCADADeviceFunctionals.SCADADeviceClassificationTags.DeviceLinks"
        return key,value
    
    def getDescription(self, instance):
        index = instance["description"].rfind("-")
        if index != -1:
            value = instance["description"][:index].strip()
        else:
            value = instance["description"]
        key = "DeviceDocumentation.DeviceDescription"
        return key,value
    
    def getDescriptionWithoutElectricalDiagram(self, instance):
        value = instance["description"].strip()
        key = "DeviceDocumentation.DeviceDescription"
        return key,value
    
    
    def getElectricalDiagram(self, instance):
        index = instance["description"].rfind("-")
        if index != -1:
            value = instance["description"][index+1:].strip()
        else:
            value = ""
        key = "DeviceDocumentation.ElectricalDiagram"
        return key,value
    
    def getDiagnostics(self, instance):
        value = instance["diagnostics"].strip()
        key = "SCADADeviceGraphics.DiagnosticPanel"
        return key,value
        
    def getWWWLink(self, instance):
        value = instance["wwwLink"].strip()
        key = "SCADADeviceGraphics.WWWLink"
        return key,value
    
    def getSynoptic(self, instance):
        value = instance["synoptic"].strip()
        key = "SCADADeviceGraphics.Synoptic"
        return key,value
        
    def getDomain(self, instance):
        value = instance["domain"].strip()
        index = value.rfind("|")
        if index != -1:
            value = value[index + 1:]
        key = "SCADADeviceFunctionals.SCADADeviceClassificationTags.Domain"
        return key,value
    
    def getAccessControlDomain(self, instance):
        value = instance["domain"].strip()
        index = value.rfind("|")
        if index != -1:
            value = value[:index]
        key = "SCADADeviceFunctionals.AccessControlDomain"
        return key,value
        
    def getNature(self, instance):
        value = instance["nature"].strip()
        index = value.rfind("|")
        if index != -1:
            value = value[index + 1:]
        key = "SCADADeviceFunctionals.SCADADeviceClassificationTags.Nature"
        return key,value
            
    def getWidgetType(self, instance):
        value = instance["widgetType"].strip()
        key = "SCADADeviceGraphics.WidgetType"
        return key,value
    
    def getBooleanArchive(self, instance):
        value = instance["booleanArchive"].strip()
        key = "SCADADeviceDataArchiving.BooleanArch"
        return key,value
    
    def getAnalogArchive(self, instance):
        value = instance["analogArchive"].strip()
        key = "SCADADeviceDataArchiving.AnalogArch"
        return key,value
    
    def getEventArchive(self, instance):
        value = instance["eventArchive"].strip()
        key = "SCADADeviceDataArchiving.EventArch"
        return key,value
    
    def getSMSCategory(self, instance):
        value = instance["smsCategory"].strip()
        key = "SCADADeviceAlarms.AlarmConfig.SMSCategory"
        return key,value   
    
    def getMaskEvent(self, instance):  
        value = instance["maskEvent"].strip()
        if value == "1":
            value = "FALSE"
        elif value == "0":
            value = "TRUE"
        else:
            value = ""
        key = "SCADADeviceFunctionals.MaskEvent"
        return key,value  
    
    def getAlarmMessage(self, instance):   
        value = instance["alarmMessage"].strip()
        key = "SCADADeviceAlarms.Message"
        return key,value   
    
    def getFEType(self, instance):
        value = instance["type"].strip()
        key = "FEDeviceIOConfig.FEType"
        return key,value   
    
    def getAlarmAcknowledge(self, instance):
        value = instance["alarmAck"].strip()
        if value.lower() == "true" or value.lower() == "y":
            value = "FALSE"
        elif value.lower() == "false" or value.lower() == "n":
            value = "TRUE"
        key = "SCADADeviceAlarms.AlarmConfig.AutoAcknowledge"
        return key,value   

    
    def getFromNormalPosition(self, instance, type):
        value = instance["normalPosition"].strip()
        alarmOnState = ""
        alarmMasked = "FALSE"
        if value == "2":
            alarmOnState = ""
            alarmMasked = "FALSE"
        elif value == "0":
            alarmOnState = "TRUE"
            alarmMasked = "FALSE"    
        elif value == "1":
            alarmOnState = "FALSE"
            alarmMasked = "FALSE"  
        elif value == "3":
            alarmOnState = "TRUE"
            alarmMasked = "TRUE"  
        elif value == "4":
            alarmOnState = "FALSE"
            alarmMasked = "TRUE"  
        elif value == "100":
            alarmOnState = "TRUE"
            alarmMasked = "FALSE"  
        elif value == "101":
            alarmOnState = "FALSE"
            alarmMasked = "FALSE"  
        elif value == "103":
            alarmOnState = "TRUE"
            alarmMasked = "TRUE"  
        elif value == "104":
            alarmOnState = "FALSE"
            alarmMasked = "TRUE"  
        
        if type == "Masked":
            return alarmMasked
        elif type == "AlarmOnState":
            return alarmOnState
        else:
            return ""
        
    def getMasked(self, instance):
        value = self.getFromNormalPosition(instance, "Masked")
        key = "SCADADeviceAlarms.AlarmConfig.Masked"
        return key,value
    
    def getMaskedFromAlarmActive(self, instance):
        value = instance["alarmActive"].strip()
        if value.lower() == "true":
            value = "FALSE"
        elif value.lower() == "false":
            value = "TRUE"
        key = "SCADADeviceAlarms.AlarmConfig.Masked"
        return key,value
    
    def getAlarmOnState(self, instance):
        value = self.getFromNormalPosition(instance, "AlarmOnState")
        key = "SCADADeviceAlarms.BinaryState.AlarmOnState"
        return key,value
    
    def getArchiveMode(self, instance, path = "SCADADeviceDataArchiving", field = "archiveMode"):
        value = instance[field].strip()
        key = path + ".ArchiveMode"
        if value == "Y" or value == "A" or value == "O":
            value = "Old/New Comparison" #"1"
        else:
            value = "No" #"0"
        return key,value
    
    def getArchiveModeWithTimeFilter(self, instance, path = "SCADADeviceDataArchiving", fieldArchive = "archiveMode", fieldFilter = "timeFilter"):
        value = instance[fieldArchive].strip()
        valueTime = instance[fieldFilter].strip()
        key = path + ".ArchiveMode"
        if value == "N":
            value = "No" #"0"
        elif value == "Y":
            value = "Time" #"1"
        elif value == "O":
            if valueTime in ["0", ""]:
                value = "Old/New Comparison" #"2"
            else:
                value = "Old/New Comparison OR Time" #"4"
        elif value == "A":
            if valueTime in ["0", ""]:
                value = "Old/New Comparison" #"2"
            else:
                value = "Old/New Comparison AND Time" #"3"
        else:
            value = "No" #"0"
        
        return key,value
    
    def getArchiveModeDeadbandTypeDeadbandValueWithTimeFilterWithDeadBand(self,instance, path = "SCADADeviceDataArchiving", fieldArchive = "archiveMode", fieldFilter = "timeFilter"):
        value = instance[fieldArchive].strip()
        valueTime = instance[fieldFilter].strip()
        archive = ""
        deadbandValue = ""
        deadbandType = ""
        keyArchive = path + ".ArchiveMode"
        keyDeadbandValue = path + ".DeadbandValue"
        keyDeadbandType = path + ".DeadbandType"
        if value == "N":
            archive = "No" #"0"
        elif value == "Y":
            archive = "Time" #"2"
        elif value == "A":
            if valueTime in ["0", ""]:
                archive = "Old/New Comparison" #"5"
            else:
                archive = "Old/New Comparison AND Time" #"6"
        elif value == "O":
            if valueTime in ["0", ""]:
                archive = "Old/New Comparison" #"5"
            else:
                archive = "Old/New Comparison OR Time" #"7"
        elif value.startswith("VA") or value.startswith("VR"):  
            values = value.split(",") #deadbandtype,deadbandvalue,archivemode
            if values[0] == "VA":
                deadbandType = "Absolute"
            else:
                deadbandType = "Relative"
            deadbandValue = values[1]
            if values[2] == "N":
                archive = "Deadband" #"1"
            elif values[2] == "A":
                archive = "Deadband AND Time" #"3"
            elif values[2] == "O":
                archive = "Deadband OR Time" #"4"
            else:
                archive = "No" #"0"
        else:
            archive = "No" #"0"
            
        return keyArchive,archive,keyDeadbandType,deadbandType,keyDeadbandValue,deadbandValue
    
    
    
    def getTimeFilter(self, instance, path="SCADADeviceDataArchiving", field="timeFilter"):
        value = instance[field].strip()
        key = path + ".TimeFilter"
        return key,value    
    
    
    def getAlarmDelay(self, instance):
        value = instance["parameters"]
        value = value.split("=")
        if len(value) == 2:
            value = value[1].strip()
        else:
            value = ""
        key = "FEDeviceParameters.PAlDt"
        return key,value
    
    def getMaster(self, instance):
        value = instance["master"].strip()
        key = "LogicDeviceDefinitions.MasterDevice"
        return key,value
    
    def getAlarmType(self, instance):
        value = instance["type"].strip()
        valueMultiple = []
        valueType = ""
        if value.find(",") != -1:  #multiple alarm types
            values = value.split(",")
            valueType = "Multiple"
            for value in values:
                value = value.split(":")
                valueMultiple.append(value[0])
        else:
            value = value.split(":")
            valueType = value[0]
        keyType = "FEDeviceAlarm.Type"
        keyMultiple = "FEDeviceAlarm.MultipleTypes"
        valueMultiple = ",".join(valueMultiple)
        return keyType,valueType,keyMultiple,valueMultiple
    
    def getAlarmMaster(self, instance):
        value = instance["type"].strip()
        valueMaster = []
        if value.find(",") != -1:  #multiple alarm types
            values = value.split(",")
            for value in values:
                value = value.split(":")
                valueMaster.append(value[1])
        else:
            value = value.split(":")
            if len(value) < 2:
                valueMaster.append("")
            else:
                valueMaster.append(value[1])
        valueMaster = ",".join(valueMaster)
        key = "LogicDeviceDefinitions.MasterDevice"
        return key,valueMaster
    
    def getLevel(self, instance):
        value = instance["level"].strip()
        key = "SCADADeviceAlarms.AlarmConfig.Level"
        return key,value
    
    def getUnit(self, instance):
        value = instance["unit"]
        key = "SCADADeviceGraphics.PosStUnit"
        return key,value
    
    def getFormat(self, instance):
        value = instance["format"]
        key = "SCADADeviceGraphics.PosStFormat"
        return key,value
    
    def getDefaultValue(self, instance):
        value = instance["defaultValue"]
        key = "FEDeviceParameters.DefaultValue"
        return key,value
    
    def getDriverDeadbandType(self, instance, path="SCADADriverDataSmoothing", field="driverDeadbandType"):
        value = instance[field]
        if value == "1": #in spec order is different than in PVSS (abs is switched with rel)
            value = "2"
        elif value == "2":
            value = "1"
        key = path + ".DeadbandType"
        return key,value
    
    def getDriverDeadbandValue(self, instance, path="SCADADriverDataSmoothing", field="driverDeadbandValue"):
        value = instance[field]
        key = path + ".DeadbandValue"
        return key,value
    
    def getRangeMin(self, instance):
        value = instance["rangeMin"]
        key = "FEDeviceParameters.PMinRan"
        return key,value
    
    def getRangeMax(self, instance):
        value = instance["rangeMax"]
        key = "FEDeviceParameters.PMaxRan"
        return key,value
             
    def getPattern(self, instance):
        value = instance["pattern"]
        key = "SCADADeviceGraphics.Pattern"
        return key,value
    
    def getParameterLimitOn(self, instance):
        value = instance["pLiOn"]
        key = "FEDeviceManualRequests.PLiOn"
        return key,value
    
    def getParameterLimitOff(self, instance):
        value = instance["pLiOff"]
        key = "FEDeviceManualRequests.PLiOff"
        return key,value
    
    def getGivenParameter(self, instance, parameterName):
        value = instance["parameters"]
        values = value.split(",")
        paramDict = {}
        for val in values:
            val = val.split("=")
            paramDict[val[0].lower()] = val[1]
        parameterName = parameterName.lower()
        if parameterName in paramDict:
            value = paramDict[parameterName]
        else:
            value = ""
        return value
    
    def getFailSafe(self, instance):
        value = self.getGivenParameter(instance, "PFSPosOn")
        key = "FEDeviceParameters.ParReg.PFsPosOn"
        if value.lower() == "false":
            value = "Off/Close"
        else:
            value = "On/Open"
        return key,value
    
    def getFailSafeWithInverted(self, instance):
        value = self.getGivenParameter(instance, "PFSPosOn")
        key = "FEDeviceParameters.ParReg.PFsPosOn"
        if value.lower() == "false":
            value = "Off/Close"
        else:
            value = "" # cannot distinguish between 1 (On/Open Normal Output) and 2 (On/Open Inverted Output)
        return key,value
            
    def getManualRestartAfterFullStop(self, instance):
        value = ""
        key = "FEDeviceParameters.ParReg.PEnRstart"
        valueEnable = self.getGivenParameter(instance, "PEnRstart")
        valueRestart = self.getGivenParameter(instance, "PRstartFS")
        valueEnable = valueEnable.lower()
        valueRestart = valueRestart.lower()
        if valueEnable == "true" and valueRestart == "true":
            value = "2"
        elif valueEnable == "true" and valueRestart == "false":
            value = "1"
        else:
            value = "0"
        return key,value
    
    def getFeedbackOff(self, instance):
        value = self.getGivenParameter(instance, "PFeedbackSim")
        key = "FEDeviceParameters.ParReg.PFeedbackOff"
        return key,value
        
    def getInhibitTotalizer(self, instance):
        value = self.getGivenParameter(instance, "PIhMTot")
        key = "FEDeviceParameters.ParReg.PIhMVoT"
        return key,value
        
    def getConvertRatio(self, instance):
        value = self.getGivenParameter(instance, "PPercent")
        key = "FEDeviceParameters.ParReg.PPercent"
        return key,value
        
    def getNoiseFilter(self, instance):
        value = self.getGivenParameter(instance, "PNoiseFilter")
        key = "FEDeviceParameters.ParReg.PNoiseF"
        return key,value
    
    def getPCOName(self, instance):
        value = instance["pcoName"]
        key = "SCADADeviceGraphics.DisplayName"
        return key,value
    
    def getModeName(self, instance, number):
        value = instance["modeName" + str(number)]
        key = "SCADADeviceFunctionals.ModeLabel.ModeLabel" +  str(number)
        return key,value
    
    def getModeAllowance(self, instance, number):
        value = instance["modeAllowance" + str(number)]
        key = "FEDeviceParameters.POpMoTa.AllowanceOptionMode" +  str(number)
        return key,value

    def getLabelOn(self, instance):
        value = instance["labelOn"]
        key = "SCADADeviceGraphics.LabelOn"
        return key,value
    
    def getLabelOff(self, instance):
        value = instance["labelOff"]
        key = "SCADADeviceGraphics.LabelOff"
        return key,value
        
        
        
            
    
    
    
    
    
