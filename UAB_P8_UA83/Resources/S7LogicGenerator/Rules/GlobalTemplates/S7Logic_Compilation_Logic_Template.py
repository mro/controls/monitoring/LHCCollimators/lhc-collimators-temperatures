# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class Compilation_Logic_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
   	self.theUnicosProject = self.thePlugin.getUnicosProject()
	self.thePlugin.writeInUABLog("initialize in Jython for Compilation_Logic.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for Compilation_Logic.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for Compilation_Logic.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'


   def process(self, *params):
	deviceVector = params[0]
	theXMLConfig = params[1]
	genGlobalFilesForAllSections = params[2].booleanValue() # Comes from "Global files scope" dropdown on Wizard. true = All sections. false = Selected sections.
	self.thePlugin.writeInUABLog("processInstances in Jython for Compilation_Logic.")
	self.thePlugin.writeSiemensLogic('''DB_ERROR_SIMU;''')

	for theCurrentPco in deviceVector :
		Name = theCurrentPco.getDeviceName()
		thePcoSections = theCurrentPco.getSections()
		thePcoDependentSections = theCurrentPco.getDependentSections()
		theDependentDevices = theCurrentPco.getDependentDevices()
		theCurrentPcoName = theCurrentPco.getDeviceName()
		self.thePlugin.writeSiemensLogic('''// Compiling the PCO Sections for '''+theCurrentPcoName)
		for theCurrentSection in thePcoSections:
			if theCurrentSection.getGenerateSection() or genGlobalFilesForAllSections:
				theCurrentSectionName = theCurrentSection.getFullSectionName()
				self.thePlugin.writeSiemensLogic(theCurrentSectionName+''';''')
		self.thePlugin.writeSiemensLogic('''// Compiling the PCO Dependent Sections for '''+theCurrentPcoName)
		for theCurrentSection in thePcoDependentSections:
			if theCurrentSection.getGenerateSection() or genGlobalFilesForAllSections:
				theCurrentSectionName = theCurrentSection.getFullSectionName()
				self.thePlugin.writeSiemensLogic(theCurrentSectionName+''';''')
		self.thePlugin.writeSiemensLogic('''// Compiling the DL of the Dependent Devices for '''+theCurrentPcoName)
		for theCurrentDependentDevice in theDependentDevices:
			theDependentSections = theCurrentDependentDevice.getDependentSections()
			if theCurrentPcoName.lower() == "no_master":
				for section in theDependentSections:
					if section.getGenerateSection() or genGlobalFilesForAllSections:
						theCurrentSectionName = section.getFullSectionName()
						self.thePlugin.writeSiemensLogic(theCurrentSectionName+''';''')
			else:
				if theCurrentDependentDevice.getDeviceType() == "ProcessControlObject":
					text = "Do nothing. These DL are already created in the PCO Dependent Sections"
				else:
					for section in theDependentSections:
						if section.getGenerateSection() or genGlobalFilesForAllSections:
							theCurrentSectionName = section.getFullSectionName()
							self.thePlugin.writeSiemensLogic(theCurrentSectionName+''';''')
				
	
	self.thePlugin.writeSiemensLogic('''
FC_CONTROLLER;
FC_PCO_LOGIC;
	''')
	
		

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for Compilation_Logic.")


   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for Compilation_Logic.")
		
