# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogAlarm Objects.

import WinCCFlex_Generic_Template
reload(WinCCFlex_Generic_Template)

class AnalogAlarm_Template(WinCCFlex_Generic_Template.Generic_Template):
    theDeviceType = "AnalogAlarm"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'name', 'String')
        self.writeScriptTag(instance, 'description', 'String')
        self.writeScriptTag(instance, 'StsReg01', self.integer, {'acquisitionMode' : '3'})
        self.writeScriptTag(instance, 'StsReg02', self.integer, {'acquisitionMode' : '3'})
        self.writeScriptTag(instance, 'ManReg01', self.integer)

        self.writeSmartTagName(instance)
        self.writeSmartTagDescription(instance)

        self.writeAlarmInfo(instance)
