# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class Communication_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   thePluginId = 0
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
   	self.theUnicosProject = self.thePlugin.getUnicosProject()
	self.thePlugin.writeInUABLog("initialize in Jython for Communication.")
	self.thePluginId = self.thePlugin.getId()

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for Communication.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for Communication.")
  
   def process(self, *params):
	self.thePlugin.writeInUABLog("processInstances in Jython for Communication.")
	theOverallInstances = params[0]
	theXMLConfig = params[1]
	genGlobalFilesForAllTypes = params[2].booleanValue() # Comes from "Global files scope" dropdown on Wizard. true = All types. false = Selected types.
	
	# General Steps for the Communication file:
	# 1. DB_WinCCOA creation. This DB contains the TSPP parameters for WinCCOA
	# 2. DB_COMM creation. This DB contains all numbers of status DB
	# 3. DB_EventData creation. This DB contains pointers on evstsreg which have been changed
	# 4. FC_TSPP creation. This FUNCTION calls the TSPP manager function
	# 5. FC_Event creation. This FUNCTION generates an event for evstsreg01 and calls the TS_EVENT_MANAGER

	# Step 1: DB_WinCCOA creation. This DB contains the TSPP parameters for WinCCOA
	
	RecordNumber = 0
	theLocalId = theXMLConfig.getPLCParameter("SiemensSpecificParameters:PLCS7Connection:LocalId")
	theRedundantLocalId = theXMLConfig.getPLCParameter("S7-400HParameters:PLCS7Connection:RedundantLocalId")
	ListEventSize = theXMLConfig.getPLCParameter("SiemensSpecificParameters:GeneralConfiguration:EventBufferSize")
	typesVector = theOverallInstances.getAllDeviceTypes()
	DB_COMM_List = self.thePlugin.getListDBComm()
	for currentType in typesVector : 
		typeName = currentType.getDeviceTypeName()
		instNb = currentType.getAllDeviceTypeInstances().size()
		TypeToProccess = theXMLConfig.getTechnicalParametersMap(self.thePluginId + ":UNICOSTypesToProcess").get(typeName)
		if (TypeToProccess == "true" or genGlobalFilesForAllTypes) and (instNb > 0):
			for currentDB_COMM in DB_COMM_List:
				DB_COMM_Name = currentDB_COMM.getType()
				if DB_COMM_Name == typeName:
					DB_COMM_Coment = currentDB_COMM.getComment()
					DB_COMM_Type = currentDB_COMM.getName()
					DB_COMM_cat = currentDB_COMM.getCat()
					if DB_COMM_cat == "bin":
						RecordNumber = RecordNumber + 1						
					if DB_COMM_cat == "ana":
						RecordNumber = RecordNumber + 1		
	GenerateBuffers = theXMLConfig.getPLCParameter("RecipeParameters:GenerateBuffers").strip().lower()
	if GenerateBuffers == "true":
		RecordNumber = RecordNumber + 1
	specsVersion = theOverallInstances.getProjectDocumentation().getSpecsVersion()
	# Get the specs version
	resourcePackageVersion = self.thePlugin.getResourcesVersion()

	# remove the "-beta-..." if it exists
	pos = resourcePackageVersion.find("-")
	if pos > 0 :
	   resourcePackageVersion = resourcePackageVersion[:pos]
					
	# Get the version numbers
	applicationVersion = theXMLConfig.getConfigInfoParameter("version")
	self.thePlugin.writeDebugInUABLog("applicationVersion: $applicationVersion$" )
	versionNumbers = resourcePackageVersion.split(".")
	majorVersion = versionNumbers[0]
	minorVersion = versionNumbers[1]
	smallVersion = versionNumbers[2]
	self.thePlugin.writeDebugInUABLog("Major: $majorVersion$  Minor: $minorVersion$  Revision: $smallVersion$" )

	self.thePlugin.writeInstanceInfoglobal('''DATA_BLOCK DB_WinCCOA
	
	
TITLE = 'DB_WinCCOA'
// 
// Contains TSPP parameters for WinCCOA
//
AUTHOR: 'UNICOS'
NAME: 'Comm'
FAMILY: 'COMMS'
STRUCT
    address_IP: 					DINT;                   	 //Address IP of WinCCOA
    address_Counter: 				INT;                    	 //Watchdog Counter
    address_CommandInterface: 		ARRAY [1..5] OF WORD;   	 //Command Interface FOR WinCCOA (Synchro - RequestAll)
    Data: 							ARRAY [1..5] OF WORD;   	 //information about S7 PLC (cycle/Error)

    TSPP_EventPeriod: 				TIME := T#2s;            	//Period before sending Event to WinCCOA if stack not full
    TSPP_Reqlist: 					ARRAY[1..4] OF INT;      	//Reqlist of TSPP_UNICOS_Manager  
    TSPP_Error: 					BOOL;                    	//Function error indicator of TSPP
    TSPP_Error_code: 				WORD;                    	//Function error code of TSPP 
    TSPP_Special_code: 				WORD;                    	//Error code of internaly used functions
    UNICOS_LiveCounter: 			DINT;       			 	//Live counter (1 second)
    Application_version: 			REAL:= $applicationVersion$;//Application version
	ResourcePackage_version:		STRUCT				     	//Resource Package version
		Major:						INT:= $majorVersion$;
		Minor:						INT:= $minorVersion$;
		Small:						INT:= $smallVersion$;
	END_STRUCT;	
	UNICOS_NewExtensions:			ARRAY[1..40] OF WORD;    // Future extensions of S7-UNICOS
END_STRUCT
BEGIN
END_DATA_BLOCK
''')

	# Step 2: DB_COMM creation. This DB contains all numbers of status DB
	
	self.thePlugin.writeInstanceInfoglobal('''
DATA_BLOCK DB_COMM
TITLE = 'DB_COMM' 
//
// Contains all numbers of status DB
//
AUTHOR: 'UNICOS'
NAME: 'Comm'
FAMILY: 'COMMS'
STRUCT
    nbDB    : INT;
    DB_List : ARRAY [1..'''+str(RecordNumber)+'''] OF CPC_DB_COMM;
END_STRUCT;
BEGIN''')
	
	if GenerateBuffers == "true":
		DB_RECIPES_INTERFACE_Address = self.thePlugin.getAddress("DB_RECIPES_INTERFACE")
		DB_RECIPES_INTERFACE_old_Address = self.thePlugin.getAddress("DB_RECIPES_INTERFACE_old")
		HeaderBufferSize = theXMLConfig.getPLCParameter("RecipeParameters:HeaderBufferSize").strip()
		BufferSize = theXMLConfig.getPLCParameter("RecipeParameters:BufferSize").strip()
		BufferSizeCalculated = int(BufferSize)
		if (BufferSizeCalculated%120 != 0):
			BufferSizeCalculated = (int(((BufferSizeCalculated-1)/120))+1)*120
			if BufferSizeCalculated > 1000:
				BufferSizeCalculated = 1000
		
		if int(BufferSizeCalculated)>1000:
			self.thePlugin.writeErrorInUABLog("The maximun buffer size allowed is 1000. BufferSize = 1000 has been taken")
			BufferSizeCalculated = 1000
		if int(BufferSizeCalculated)<1:
			self.thePlugin.writeErrorInUABLog("The buffer size must be a positive integer. BufferSize = 120 has been taken")
			BufferSizeCalculated = 120
		sizeTable = int(HeaderBufferSize)*4 + 2
	
		self.thePlugin.writeInstanceInfoglobal('''
(* Recipes interface DB *)
DB_List[1].Status_DB := '''+str(DB_RECIPES_INTERFACE_Address)+''';
DB_List[1].Status_DB_old := '''+str(DB_RECIPES_INTERFACE_old_Address)+''';
DB_List[1].size := '''+str(sizeTable)+''';
// nbDB:= 1
	''')
		RecordNumber = 1
	else:
		RecordNumber = 0
	typesVector = theOverallInstances.getAllDeviceTypes()
	for currentType in typesVector : 
		typeName = currentType.getDeviceTypeName()
		instNb = currentType.getAllDeviceTypeInstances().size()
		TypeToProccess = theXMLConfig.getTechnicalParametersMap(self.thePluginId + ":UNICOSTypesToProcess").get(typeName)
		if (TypeToProccess == "true" or genGlobalFilesForAllTypes) and (instNb > 0):
			for currentDB_COMM in DB_COMM_List:
				DB_COMM_Name = currentDB_COMM.getType()
				if DB_COMM_Name == typeName:
					DB_COMM_Coment = currentDB_COMM.getComment()
					DB_COMM_Type = currentDB_COMM.getName()
					DB_COMM_cat = currentDB_COMM.getCat()
					DB_COMM_size = currentDB_COMM.getSizeObject()
					DB_COMM_size_Final = DB_COMM_size*instNb
					if DB_COMM_cat == "bin":
						RecordNumber = RecordNumber + 1
						name_bin = "DB_bin_Status_"+DB_COMM_Type
						name_bin_old="DB_bin_Status_"+DB_COMM_Type+"_old"
						AddressSymbolResource = self.thePlugin.getAddress(name_bin)
						AddressSymbolResourceOld = self.thePlugin.getAddress(name_bin_old)
						self.thePlugin.writeInstanceInfoglobal('''
(* '''+DB_COMM_Coment+'''*)''')
						self.thePlugin.writeInstanceInfoglobal('''DB_List['''+str(RecordNumber)+'''].Status_DB := '''+str(AddressSymbolResource)+''';''')
						self.thePlugin.writeInstanceInfoglobal('''DB_List['''+str(RecordNumber)+'''].Status_DB_old := '''+str(AddressSymbolResourceOld)+''';''')
						self.thePlugin.writeInstanceInfoglobal('''DB_List['''+str(RecordNumber)+'''].size := '''+str(DB_COMM_size_Final)+''';''')
						self.thePlugin.writeInstanceInfoglobal('''// nbDB:= '''+str(RecordNumber))
					if DB_COMM_cat == "ana":
						RecordNumber = RecordNumber + 1
						name_ana = "DB_ana_Status_"+DB_COMM_Type
						name_ana_old="DB_ana_Status_"+DB_COMM_Type+"_old"
						AddressSymbolResource = self.thePlugin.getAddress(name_ana)
						AddressSymbolResourceOld = self.thePlugin.getAddress(name_ana_old)
						self.thePlugin.writeInstanceInfoglobal('''
(* '''+DB_COMM_Coment+'''*)''')
						self.thePlugin.writeInstanceInfoglobal('''DB_List['''+str(RecordNumber)+'''].Status_DB := '''+str(AddressSymbolResource)+''';''')
						self.thePlugin.writeInstanceInfoglobal('''DB_List['''+str(RecordNumber)+'''].Status_DB_old := '''+str(AddressSymbolResourceOld)+''';''')
						self.thePlugin.writeInstanceInfoglobal('''DB_List['''+str(RecordNumber)+'''].size := '''+str(DB_COMM_size_Final)+''';''')
						self.thePlugin.writeInstanceInfoglobal('''// nbDB:= '''+str(RecordNumber))
	
	self.thePlugin.writeInstanceInfoglobal('''nbDB:= '''+str(RecordNumber)+''';''')
	self.thePlugin.writeInstanceInfoglobal('''END_DATA_BLOCK
''')

   	# Step 3: DB_EventData creation. This DB contains pointers on evstsreg which have been changed
	
	self.thePlugin.writeInstanceInfoglobal('''
DATA_BLOCK DB_EventData
TITLE = 'EventData'
//
// Contains pointers on evstsreg which have been changed
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT
   
        Nb_Event    : INT;
        List_Event   :  ARRAY [1..'''+str(ListEventSize)+'''] OF STRUCT
                                            S7_ID     : BYTE;
                                            DataType  : BYTE;
                                            NbOfData  : INT;
                                            DBNumber  : INT;
                                            Address   : DWORD;
                                     END_STRUCT;
END_STRUCT;
BEGIN
END_DATA_BLOCK''')				
	
   	# Step 4: FC_TSPP creation. This FUNCTION calls the TSPP manager function	

	self.thePlugin.writeInstanceInfoglobal('''
	
FUNCTION FC_TSPP: VOID
TITLE = 'FC_TSPP'
//
// Calls the TSPP manager function
//
AUTHOR: 'UNICOS'
NAME: 'Comm'
FAMILY: 'COMMS'
VAR_INPUT
    NE: BOOL;
    RA: BOOL; 
END_VAR
  // Optimal TSPP parameters: 
  //    MaxStatusTable = NbStatusTables solved in the TSPP file generated
  //	MaxTableInOneSend = MaxTableInOneSend solved in the TSPP file generated
  //    StatusWordSize = 100

 
  TSPP_UNICOS_Manager.TSPP_UNICOS_DB(
                        Init :=  FALSE
                       ,SendID0 :=  B#16#'''+str(theLocalId)+'''
                       ,SendID1 :=  B#16#'''+str(theRedundantLocalId)+'''
                       ,SendEventPeriod := DB_WinCCOA.TSPP_EventPeriod
                       ,NewEvent :=  NE
                       ,EventTSIncluded := FALSE 
                       ,MultipleEvent :=  TRUE
                       ,EventListDB :=  DB_EventData
                       ,SendAllStatus := RA
                       ,ListOfStatusTable := DB_COMM 
                       ,WatchDog := DB_WinCCOA.Address_Counter
                       );
END_FUNCTION
''')

   	# Step 5: FC_Event creation. This FUNCTION generates an event for evstsreg01 and calls the TS_EVENT_MANAGER
	
	self.thePlugin.writeInstanceInfoglobal('''
    
(*FUNCTION WHICH GENERATE EVENT FOR evstsreg01 and CALL TS_EVENT_MANAGER*)
FUNCTION FC_Event : VOID
TITLE = 'FC_Event'
//
// FUNCTION WHICH GENERATE EVENT FOR evstsreg01 and CALL TS_EVENT_MANAGER
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
VAR
    NewEvent : 		BOOL;
	RequestAll: 	BOOL;
    Synchro: 		BOOL;
    i: 				INT;
    j: 				INT; 
    k: 				INT;
    First_obj: 		INT;
    dummyVar: 		INT;
	ListEventSize:  INT;
 END_VAR
BEGIN
DB_EventData.Nb_Event 	:= 0;
First_obj 				:= 0;
k						:= 2;
j						:= TSPP_Unicos_DB.ID_NewEvent;
NewEvent				:= 0;
RequestAll				:= FALSE;
Synchro					:= FALSE;
ListEventSize           := '''+str(ListEventSize)+''';''')

	typesVector = theOverallInstances.getAllDeviceTypes()
	for currentType in typesVector : 
		typeName = currentType.getDeviceTypeName()
		NbType = theOverallInstances.getDeviceType(typeName).getAllDeviceTypeInstances().size()
		TypeToProccess = theXMLConfig.getTechnicalParametersMap(self.thePluginId + ":UNICOSTypesToProcess").get(typeName)
		if (NbType > 0) & (TypeToProccess == "true" or genGlobalFilesForAllTypes):
			attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
			representationName = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", typeName)
			for attributeFamily in attributeFamilyList:
				attributeFamilyName = attributeFamily.getAttributeFamilyName()
				if (attributeFamilyName == "FEDeviceOutputs"):
					attributeList = attributeFamily.getAttribute()
					Stsreg01Exist = 0
					Stsreg02Exist = 0
					for attribute in attributeList:
						attributeName = attribute.getAttributeName()
						if (attributeName == "StsReg01"):
							Stsreg01Exist = 1
						elif (attributeName == "StsReg02"):
							Stsreg02Exist = 1
						
					if Stsreg01Exist and not Stsreg02Exist:		
						self.thePlugin.writeInstanceInfoglobal('''
(*Test if Evstsreg01 of '''+representationName+''' have changed*)
FOR i:= 1 TO DB_event_'''+representationName+'''.Nb_'''+representationName+''' DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_'''+representationName+'''.'''+representationName+'''_evstsreg[i].evstsreg01) <> DWORD_TO_WORD(ROR(IN:=DB_event_'''+representationName+'''.'''+representationName+'''_evstsreg[i].evStsReg01, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_'''+representationName+'''));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*4+k),N:=3) OR DW#16#84000000 ;
    END_IF;
END_FOR;

''')

					if Stsreg01Exist and Stsreg02Exist:
							self.thePlugin.writeInstanceInfoglobal('''
							
(*Test if Evstsreg01 of '''+representationName+''' have changed*)
FOR i:= 1 TO DB_event_'''+representationName+'''.Nb_'''+representationName+''' DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_'''+representationName+'''.'''+representationName+'''_evstsreg[i].evstsreg01) <> DWORD_TO_WORD(ROR(IN:=DB_event_'''+representationName+'''.'''+representationName+'''_evstsreg[i].evStsReg01, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_'''+representationName+'''));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*8+k),N:=3) OR DW#16#84000000 ;
    END_IF;
END_FOR;


(*Test if Evstsreg02 of '''+representationName+''' have changed*)
First_obj := DB_event_'''+representationName+'''.Nb_'''+representationName+''';
FOR i:= 1 TO DB_event_'''+representationName+'''.Nb_'''+representationName+''' DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_'''+representationName+'''.'''+representationName+'''_evstsreg[i].evstsreg02) <> DWORD_TO_WORD(ROR(IN:=DB_event_'''+representationName+'''.'''+representationName+'''_evstsreg[i].evstsreg02, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_'''+representationName+'''));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*8+k+4),N:=3) OR DW#16#84000000;
    END_IF;
END_FOR;
''')	
		


	#FOOTER. Must be called at the end. After looping through all object types
	self.thePlugin.writeInstanceInfoglobal('''
DB_EventData.Nb_Event :=  j;
TSPP_Unicos_DB.ID_NewEvent := j;
(*Action by WinCCOA via the Command Interface*)
CASE WORD_TO_INT(DB_WinCCOA.Address_CommandInterface[1]) OF
    1 :   Synchro := TRUE;// Synchronisation of timing from WinCCOA
          DB_WinCCOA.Address_CommandInterface[1] := 0;
    2:    RequestAll := TRUE;// Request All : All object status are sent to WinCCOA
          DB_WinCCOA.Address_CommandInterface[1] := 0;
END_CASE;
// First Time: Events are not sent when cold start (overload of events !!)
IF Comm = 0 THEN Comm := 1;
ELSE 
    FC_TSPP(NE :=NewEvent & comm,RA := RequestAll);
END_IF;   
                       
Synchro := FALSE;
RequestAll := FALSE;
 DB_WinCCOA.Data[1] := DINT_TO_WORD(TIME_TO_DINT(T_CYCLE)); //PLC cycle
 DB_WinCCOA.Data[2] := INT_TO_WORD(TSPP_UNICOS_DB.NbOfTables);
 DB_WinCCOA.Data[3] := INT_TO_WORD(TSPP_UNICOS_DB.NbOfStatusReq);
 DB_WinCCOA.Data[4] := TSPP_UNICOS_DB.Error_code;
 DB_WinCCOA.Data[5] := TSPP_UNICOS_DB.Special_code;
                       
(*From TS Event Manager*)
 DB_WinCCOA.TSPP_ReqList        := TSPP_UNICOS_DB.Reqlist; 
 DB_WinCCOA.TSPP_Error          := TSPP_UNICOS_DB.Error;
 DB_WinCCOA.TSPP_Error_code     := TSPP_UNICOS_DB.Error_code;    
 DB_WinCCOA.TSPP_Special_code   := TSPP_UNICOS_DB.Special_code;
 // Live Counter (Time in seconds from last startUP)
 DB_WinCCOA.UNICOS_LiveCounter := UNICOS_LiveCounter;
''')

	self.thePlugin.writeInstanceInfoglobal('''
END_FUNCTION''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for Communication.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for Communication.")
		
