# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
#from research.ch.cern.unicos.types.ai import Instance                 	#REQUIRED
from time import strftime

class AnalogOutput_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for AO.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for AO.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for AO.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'
	
   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for AO.")
		
	# General Steps for Optimized objects:
	# 1. Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances) It's called DB_Type
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from DeviceTypeDefinition.xml created by TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5. Create the Type_Error DB if "Diagnostic" is required
	# 6. Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 7. Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#	 We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	# This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	Diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	
	# Step 1: Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances)
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Analog Output DB Creation file: UNICOS application
''')

	self.thePlugin.writeInstanceInfo('''
DATA_BLOCK DB_AO CPC_FB_AO
BEGIN
END_DATA_BLOCK
''')


	# Step 3: Create all the needed Structures from the TCT	
	
	# UDT for DB_AO_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE AO_ManRequest
TITLE = AO_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isCommVerification = attribute.getIsCommunicated()
				if isCommVerification:
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE AO_bin_Status
TITLE = AO_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE AO_event
TITLE = AO_event
//
// parameters of ANADIG Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE AO_ana_Status
TITLE = AO_ana_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributeName = attribute.getAttributeName()
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_AO_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_AO_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_AO_ManRequest
TITLE = DB_AO_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    AO_Requests : ARRAY [1..'''+instanceNb+'''] OF AO_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')

	# Step 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 
	
	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_AO Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_AO
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type AO
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_AO : INT := '''+str(NbType)+''';
	AO_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF AO_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
BEGIN
END_DATA_BLOCK


''')

	self.thePlugin.writeInstanceInfo('''
(************************* binary Status of the AOs ************************)
DATA_BLOCK DB_bin_status_AO
TITLE = 'DB_bin_status_AO'
//
// Global binary status DB of AO
//
// List of variables:''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AO_bin_Status)
''')

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF AO_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(********************* old binary Status of the AOs ****************************)
DATA_BLOCK DB_bin_status_AO_old
TITLE = 'DB_bin_status_AO_old'
//
// Old Global binary status DB of AO
//
''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AO_bin_Status)
''')
		
	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF AO_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(******************* analog Status of the AOs ******************************)
DATA_BLOCK DB_ana_Status_AO
TITLE = 'DB_ana_status_AO'
//
// Global analog status DB of AO
//
// List of variables:
''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AO_ana_Status)
''')	

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF AO_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
// -----------------------------------------------------------------------------------------------------
(*old analog status of the AOs************************************************)
DATA_BLOCK DB_ana_Status_AO_old
TITLE = 'DB_ana_Status_AO_old'
//
// old Global Analog  status DB of AO
//
  // List of variables:
''')
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AO_ana_Status)
''')		

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF AO_ana_Status;
END_STRUCT
BEGIN

END_DATA_BLOCK
''')

	# Step 5: Create the Type_Error DB if "Diagnostic" is required

	if Diagnostic == "true":
		self.thePlugin.writeInstanceInfo('''
(*DB for IoError on Channels with OB82*)
DATA_BLOCK AO_ERROR
TITLE = AO_ERROR
//
// DB with IOError signals of AO
//
AUTHOR: AB_CO_IS
NAME: Error
FAMILY: Error
STRUCT
 IOERROR : ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_IOERROR;
END_STRUCT
BEGIN
''')

		RecordNumber = 0
		for instance in instancesVector :
			# Now looping on each device instance.
			# 1 Building shortcuts
			# 2 Write code to output
			RecordNumber = RecordNumber + 1
			PLCChannel = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
			TypePLCChannel = PLCChannel[0:1]
			if TypePLCChannel == "p":
				NbPIPLCChannel = PLCChannel[3:8]
				self.thePlugin.writeInstanceInfo('''
IOERROR['''+str(RecordNumber)+'''].ADDR :=$NbPIPLCChannel$.0; ''')
			elif TypePLCChannel == "q":
				NbIPLCChannel = PLCChannel[2:8]
				self.thePlugin.writeInstanceInfo('''
IOERROR['''+str(RecordNumber)+'''].ADDR :=$NbIPLCChannel$.0; ''')
			
		self.thePlugin.writeInstanceInfo('''
			
END_DATA_BLOCK''')

	# Step 6: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	
	self.thePlugin.writeInstanceInfo(''' 
	
(************************** EXEC Of AOs ***********************)
FUNCTION_BLOCK FB_AO_all
TITLE = 'FB_AO_all'
//
// Call the AO treatment
//
AUTHOR: 'UNICOS'
NAME: 'Call AO'
FAMILY: 'AO'

VAR
  // Static Variables
  AO_SET: STRUCT''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''
   $Name$       : CPC_DB_AO;''')

	self.thePlugin.writeInstanceInfo('''	
   END_STRUCT;
  // Different variable view declaration
  AO AT AO_SET: ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_DB_AO;
  
  // Support variables
  old_status : DWORD;
  I: INT;
END_VAR
  FOR I:=1 TO '''+str(RecordNumber)+''' DO
  
    // status
    old_status := DB_bin_status_AO.StsReg01[I].StsReg01;
    old_status := ROR(IN:=old_status, N:=16);
          
''')
	
	if Diagnostic == "true":
		self.thePlugin.writeInstanceInfo('''
	AO[I].IoError := AO_ERROR.IOERROR[I].Err;''')
	else:
		self.thePlugin.writeInstanceInfo('''
	AO[I].IoError := False;''')
	self.thePlugin.writeInstanceInfo('''
	
	// Calls the Baseline function
	CPC_FB_AO.DB_AO(  
		Manreg01 :=  DB_AO_ManRequest.AO_Requests[I].Manreg01    // set by WinCCOA in the DB_AO 
		,MposR :=  DB_AO_ManRequest.AO_Requests[I].MposR          // set by WinCCOA in the DB_AO  
		,StsReg01 := DB_bin_status_AO.StsReg01[I].StsReg01
		,Perst := AO[I]
	);

	// Call IO_ACCESS_AO
	IF (AO[I].FEType <> 0) THEN
		IO_ACCESS_AO(FEType := AO[I].FEType, 
					 PQWDef := AO[I].PqwDef, 
					 InterfaceParam1 := AO[I].perAddress,
					 OutOV := AO[I].OutOV);
	END_IF;
							  
	// Status
	DB_ana_Status_AO.status[I].PosSt := AO[I].PosSt;
	DB_ana_Status_AO.status[I].AuPosRSt := AO[I].AuPosRSt;

	// Event
	DB_Event_AO.AO_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_AO.StsReg01[I].StsReg01;
   
  END_FOR;
END_FUNCTION_BLOCK
     ''')

	 

	# Step 7: Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.

	self.thePlugin.writeInstanceInfo('''
(************************** All AO devices instantation ***********************)     
DATA_BLOCK DB_AO_all  FB_AO_all
//
// Instance DB for the whole AO devices
//
BEGIN
	''')
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		PMaxRan = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max"))
		PMinRan= self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min"))
		PMaxRaw = instance.getAttributeData("FEDeviceParameters:Raw Max")
		PMinRaw = instance.getAttributeData("FEDeviceParameters:Raw Min")
		Type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type").strip()
		if Type == "":
			Type = '0'
			#self.thePlugin.writeWarningInUABLog("AO instance: $Name$. Undefined Type, FEEncondType = 0 is taken.")
		self.thePlugin.writeInstanceInfo('''
		
AO_SET.$Name$.PMaxRan := '''+str(PMaxRan)+''';
AO_SET.$Name$.PMinRan := '''+str(PMinRan)+''';
AO_SET.$Name$.PMaxRaw := '''+str(PMaxRaw)+''';
AO_SET.$Name$.PMinRaw := '''+str(PMinRaw)+''';
AO_SET.$Name$.FEType := '''+str(Type)+''';
AO_SET.$Name$.index := '''+str(RecordNumber)+''';''')


		if Type == "1":
			PLCChannel = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
			Init = PLCChannel[0]
			if Init.lower() == "p":
				Fin = PLCChannel[3:]
				PQWDEF = "true"
			else:
				Fin = PLCChannel[2:]
				PQWDEF = "false"
			self.thePlugin.writeInstanceInfo('''
AO_SET.$Name$.perAddress:= $Fin$;
AO_SET.$Name$.PqwDef:= '''+str(PQWDEF)+''';''')
	
	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK''')

	#Creation of a FC even for the optimized objects in order to call it in the OB1
	#This avoid the too many symbols problem of the OB1
	#The code is ready for all the object in case we need to split in several DBs one day. Note that for the moment,
	#this has sense only for the AIR and the DI where the split is working.
	NumberOfInstances = instancesVector.size()
	LimitSizeOfInstances = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", theCurrentDeviceType.getDeviceTypeName()))
	nameRepresentation = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", theCurrentDeviceType.getDeviceTypeName())
	self.thePlugin.writeInstanceInfo('''
FUNCTION FC_$nameRepresentation$ : VOID
    FB_$nameRepresentation$_all.DB_$nameRepresentation$_all();''')
	if (NumberOfInstances > LimitSizeOfInstances):
		self.thePlugin.writeInstanceInfo('''
    FB_$nameRepresentation$_all2.DB_$nameRepresentation$_all2();    // Too many '''+str(NumberOfInstances)+''' Need a second FB_$nameRepresentation$_All2''')
	self.thePlugin.writeInstanceInfo('''
END_FUNCTION
''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for AO.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for AO.")
		
