# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		    #REQUIRED
from time import strftime

class DigitalInput_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for DI.")
	
			
   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for DI.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for DI.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'
	
   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for DI.")
			
	# General Steps for Optimized objects:
	# 1. Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances) It's called DB_Type
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from DeviceTypeDefinition.xml created by TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5. Create the Type_Error DB if "Diagnostic" is required
	# 6. Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 7. Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#	 We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	# This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	Diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	
	# Step 1: Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances)
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Digital Input DB Creation file: UNICOS application
''')
	self.thePlugin.writeInstanceInfo('''
DATA_BLOCK DB_DI CPC_FB_DI
BEGIN
END_DATA_BLOCK

''')

	
	# Step 3: Create all the needed Structures from the TCT	
	
	# UDT for DB_DI_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE DI_ManRequest
TITLE = DI_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				attributePrimitiveType = attribute.getPrimitiveType()
				attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
				
				self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')



	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE DI_bin_Status
TITLE = DI_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE DI_event
TITLE = DI_event
//
// parameters of DI Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				# isEventAttribute = attribute.getIsEventAttribute()
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_DI_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_DI_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_DI_ManRequest
TITLE = DB_DI_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = instancesVector.size()
	self.thePlugin.writeInstanceInfo('''    DI_Requests : ARRAY [1..'''+str(instanceNb)+'''] OF DI_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')
	
	
	# Step 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 
	
	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_DI Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_DI
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type DI
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_DI : INT := '''+str(NbType)+''';
	DI_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF DI_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
BEGIN
END_DATA_BLOCK


''')
		

	self.thePlugin.writeInstanceInfo('''
(*Status of the DIs************************************************)
DATA_BLOCK DB_bin_status_DI
TITLE = 'DB_bin_status_DI'
//
// Global binaries status DB of DI
//
//  List of variables:''')


	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
   //  ['''+str(RecordNumber)+''']    $Name$      (DI_bin_Status)''')


	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF DI_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(*old Status of the DIs************************************************)
DATA_BLOCK DB_bin_status_DI_old
TITLE = 'DB_bin_status_DI_old'
//
// Old Global binaries status DB of DI
//
  // List of variables:''')
  

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
   //  ['''+str(RecordNumber)+''']    $Name$      (DI_bin_Status)''')
	

		
	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF DI_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
''')
	
	
	
	# Step 5: Create the Type_Error DB if "Diagnostic" is required
	
	if Diagnostic == "true":
		self.thePlugin.writeInstanceInfo('''
(*DB for IoError on Channels with OB82*)
DATA_BLOCK DI_ERROR
TITLE = 'DI_ERROR'
//
// DB with IOError signals of DI
//
AUTHOR: 'EN/ICE'
NAME: Error
FAMILY: Error
STRUCT
 IOERROR : ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_IOERROR;
END_STRUCT
BEGIN
''')

		RecordNumber = 0
		for instance in instancesVector :
			RecordNumber = RecordNumber + 1
			FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
			if FEType == "1":
				PLCChannel = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1")
				TypePLCChannel = PLCChannel[0:1]
				if TypePLCChannel == "P":
					NbPIPLCChannel = PLCChannel[2:8]
					self.thePlugin.writeInstanceInfo('''
	IOERROR['''+str(RecordNumber)+'''].ADDR :=$NbPIPLCChannel$; ''')
				elif TypePLCChannel == "I":
					NbIPLCChannel = PLCChannel[1:8]
					self.thePlugin.writeInstanceInfo('''
	IOERROR['''+str(RecordNumber)+'''].ADDR :=$NbIPLCChannel$; ''')
			
		self.thePlugin.writeInstanceInfo('''
			
END_DATA_BLOCK''')


	LimitSize = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "DigitalInput"))
	sIsLargeApplication = self.thePlugin.getXMLConfig().getPLCParameter("GeneralConfiguration:LargeApplication", False)
	if sIsLargeApplication == None:
		bIsLargeApplication = False
	else:
		bIsLargeApplication = (sIsLargeApplication.lower() == 'true')

	if (bIsLargeApplication): #UCPC-1670
		self.thePlugin.writeInstanceInfo('''
(* SIMPLIFIED DIGITAL INPUT OBJECT DATA STRUCTURE *****************)

TYPE CPC_DB_DI_S
TITLE ='CPC_DB_DI_S'
AUTHOR: 'EN/ICE'
VERSION: 6.0

STRUCT     
   PosSt:                   BOOL;          
   FoMoSt:                  BOOL;    
   IOErrorW:                BOOL;   
   IOSimuW:                 BOOL;   
    
END_STRUCT     
END_TYPE

(* SIMPLIFIED DB instance DI ******************************************)
DATA_BLOCK DB_DI_ALL_S
//
// SIMPLIFIED DB instance DI
//
    STRUCT
        DI_SET : STRUCT
''')
		RecordNumber = 0
		for instance in instancesVector :
			# Now looping on each device instance.
			# 1 Building shortcuts
			# 2 Write code to output
			RecordNumber = RecordNumber + 1
			Name = instance.getAttributeData("DeviceIdentification:Name")
			self.thePlugin.writeInstanceInfo('''
            $Name$       : CPC_DB_DI_S;	// DI number <'''+str(RecordNumber)+'''>
''')
		self.thePlugin.writeInstanceInfo('''
        END_STRUCT;
    END_STRUCT
BEGIN

END_DATA_BLOCK
''')

	# Step 6: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	
	self.thePlugin.writeInstanceInfo(''' 
// -- 
// -- 
(*EXEC OF DIs************************************************)
FUNCTION_BLOCK FB_DI_all
TITLE = 'FB_DI_all'
//
// DI calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_DI'
FAMILY: 'DI'

CONST
	// Constants for the CPC_DB_AIR_S ENS-7060
	SIZE_DB_DI_S_IN_BYTES:=2; //one FLOAT + one WORD
	OFFSET_POSST_IN_BYTES:=0; //one FLOAT
	OFFSET_FOMOST_IN_BYTES:=0; //one FLOAT
	OFFSET_IOERRORW_IN_BYTES:=0; //one FLOAT
	OFFSET_IOSIMUW_IN_BYTES:=0; //one FLOAT
	BIT_POSST:=0;
	BIT_FOMOST:=1;
	BIT_IOERRORW:=2;
	BIT_IOSIMUW:=3;
END_CONST

VAR 
   // Static variables
   DI_SET: STRUCT''')
	

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (RecordNumber <= LimitSize):
			self.thePlugin.writeInstanceInfo('''
   $Name$       : CPC_DB_DI;	// DI number <'''+str(RecordNumber)+'''>''')	

	MinNb = RecordNumber
	if (RecordNumber >= LimitSize):
		MinNb = LimitSize

	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
   
   DI AT DI_SET: ARRAY[1..'''+str(MinNb)+'''] OF CPC_DB_DI;
  
  // Support variables  
  old_status : DWORD;
  I: INT;
  IOError_Var: BOOL;
END_VAR
FOR I:=1 TO '''+str(MinNb)+''' DO

	// Call the IO_ACCESS_DI function
	IF (DI[I].FEType <> 0) THEN
		IO_ACCESS_DI(
			 Channel := DI[I].perByte, 
			 Bit := DI[I].perBit,
			 FEType := DI[I].FEType,
			 InterfaceParam1 := DI[I].DBnum,
			 InterfaceParam2 := DI[I].perByte,
			 InterfaceParam3 := DI[I].perBit,
			 InterfaceParam4 := DI[I].DBnumIoError,
			 InterfaceParam5 := DI[I].DBposIoError,
			 InterfaceParam6 := DI[I].DBbitIoError,
			 HFPos := DI[I].HFPos,
			 IOError := IOError_Var);
	END_IF;
	
    old_status := DB_bin_status_DI.StsReg01[I].StsReg01;  
    old_status := ROR(IN:=old_status, N:=16);	
	''')
	
	if Diagnostic == "true":
		self.thePlugin.writeInstanceInfo('''

	// Assign the IOERROR
    IF (DI[I].FEType = 1) THEN                     
        DI[I].IoError := DI_ERROR.IOERROR[I].Err; // IO Hardware  
	ELSIF (DI[I].FEType = 0) THEN 
        ; // Object without connections.
    ELSE
        DI[I].IoError := IOError_Var;
    END_IF;

	''')
	else:
		self.thePlugin.writeInstanceInfo('''
    // No diagnostic
	// DI[I].IoError := IOError_Var;
	''')
	
	self.thePlugin.writeInstanceInfo('''
    
	// Calls the Baseline function
    CPC_FB_DI.DB_DI(                                           
		Manreg01 :=  DB_DI_ManRequest.DI_Requests[I].Manreg01    // set by WinCCOA in the DB_DI_ManRequest
		,StsReg01 := DB_bin_status_DI.StsReg01[I].StsReg01
		,Perst := DI[I]
    ); 
''')
	if (bIsLargeApplication):
		self.thePlugin.writeInstanceInfo('''    
    DB_DI_All_S.DX [((I-1)*SIZE_DB_DI_S_IN_BYTES)+OFFSET_POSST_IN_BYTES,BIT_POSST]:= DI[I].PosSt;
    DB_DI_All_S.DX [((I-1)*SIZE_DB_DI_S_IN_BYTES)+OFFSET_FOMOST_IN_BYTES,BIT_FOMOST]:= DI[I].FoMoSt;
    DB_DI_All_S.DX [((I-1)*SIZE_DB_DI_S_IN_BYTES)+OFFSET_IOERRORW_IN_BYTES,BIT_IOERRORW]:= DI[I].IOErrorW;
    DB_DI_All_S.DX [((I-1)*SIZE_DB_DI_S_IN_BYTES)+OFFSET_IOSIMUW_IN_BYTES,BIT_IOSIMUW]:= DI[I].IOSimuW;
''')
	self.thePlugin.writeInstanceInfo('''    
    // Events
    DB_Event_DI.DI_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_DI.StsReg01[I].StsReg01;
	
 END_FOR;
END_FUNCTION_BLOCK
   ''')

 
	# Step 7: Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.

	self.thePlugin.writeInstanceInfo('''	
(* DB instance DI ******************************************)
DATA_BLOCK DB_DI_all  FB_DI_all
//
// Instance DB for the whole DI devices
//
BEGIN
	''')
			
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		if (RecordNumber <= LimitSize):
			Name = instance.getAttributeData("DeviceIdentification:Name")
			FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
			if FEType == "":
				FEType = 0
				#self.thePlugin.writeWarningInUABLog("DI instance: $Name$. Undefined Type, FEEncondType = 0 is taken.")
			InterfaceParam1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
			InterfaceParam2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
			InterfaceParam3 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam3").strip().lower()
			InterfaceParam4 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam4").strip().lower()
			InterfaceParam5 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5").strip().lower()
			InterfaceParam6 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam6").strip().lower()
			self.thePlugin.writeInstanceInfo('''
DI_SET.$Name$.index := '''+str(RecordNumber)+''';
DI_SET.$Name$.FEType := '''+str(FEType)+''';''')
			#if FEType == "0":
			#	self.thePlugin.writeWarningInUABLog("DI instance: $Name$. FEEncondType = 0 No address configured.")
			if FEType == "1":
				IndexI = InterfaceParam1.index('i')
				pointIndex = InterfaceParam1.index('.')
				IndexByte = IndexI + 1
				Byte = InterfaceParam1[IndexByte:pointIndex]
				IndexBit = pointIndex + 1
				Bit = InterfaceParam1[IndexBit]
				self.thePlugin.writeInstanceInfo('''
DI_SET.$Name$.perByte := $Byte$;
DI_SET.$Name$.perBit := $Bit$;
''')	
			if FEType == "101":
				if InterfaceParam1[0:2] == "db":
					InterfaceParam1 = InterfaceParam1[2:]
				if InterfaceParam2[0:2] ==  "db":
					InterfaceParam2 = InterfaceParam2[3:]
				self.thePlugin.writeInstanceInfo('''
DI_SET.$Name$.DBnum:=$InterfaceParam1$;
DI_SET.$Name$.perByte:=$InterfaceParam2$;
DI_SET.$Name$.perBit:=$InterfaceParam3$;
		 ''')
			if FEType == "102":
				if InterfaceParam1[0:2] == "db":
					InterfaceParam1 = InterfaceParam1[2:]
				if InterfaceParam2[0:2] ==  "db":
					InterfaceParam2 = InterfaceParam2[3:]
				if InterfaceParam4[0:2] ==  "db":
					InterfaceParam4 = InterfaceParam4[2:]
				if InterfaceParam5[0:2] ==  "db":
					InterfaceParam5 = InterfaceParam5[3:]
				self.thePlugin.writeInstanceInfo('''
DI_SET.$Name$.DBnum:=$InterfaceParam1$;
DI_SET.$Name$.perByte:=$InterfaceParam2$;
DI_SET.$Name$.perBit:=$InterfaceParam3$;
DI_SET.$Name$.DBnumIoError:=$InterfaceParam4$;
DI_SET.$Name$.DBposIoError:=$InterfaceParam5$;
DI_SET.$Name$.DBbitIoError:=$InterfaceParam6$;
		 ''')
			if FEType == "103":
				if InterfaceParam1[0:2] == "db":
					InterfaceParam1 = InterfaceParam1[2:]
				if InterfaceParam2[0:2] ==  "db":
					InterfaceParam2 = InterfaceParam2[3:]
				if InterfaceParam4[0:2] ==  "db":
					InterfaceParam4 = InterfaceParam4[2:]
				if InterfaceParam5[0:2] ==  "db":
					InterfaceParam5 = InterfaceParam5[3:]
				self.thePlugin.writeInstanceInfo('''
DI_SET.$Name$.DBnum:=$InterfaceParam1$;
DI_SET.$Name$.perByte:=$InterfaceParam2$;
DI_SET.$Name$.perBit:=$InterfaceParam3$;
DI_SET.$Name$.DBnumIoError:=$InterfaceParam4$;
DI_SET.$Name$.DBposIoError:=$InterfaceParam5$;
DI_SET.$Name$.DBbitIoError:=$InterfaceParam6$;
		 ''')		
	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK''')

	RecordNumber = instancesVector.size()
	LimitSize = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "DigitalInput"))
	if (RecordNumber > LimitSize):
		self.thePlugin.writeInstanceInfo(''' 
// -- 
// -- 
(*EXEC OF DIs************************************************)
FUNCTION_BLOCK FB_DI_all2
TITLE = 'FB_DI_all2'
//
// DI calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_DI2'
FAMILY: 'DI'

CONST
	// Constants for the CPC_DB_AIR_S ENS-7060
	SIZE_DB_DI_S_IN_BYTES:=2; //one FLOAT + one WORD
	OFFSET_POSST_IN_BYTES:=0; //one FLOAT
	OFFSET_FOMOST_IN_BYTES:=0; //one FLOAT
	OFFSET_IOERRORW_IN_BYTES:=0; //one FLOAT
	OFFSET_IOSIMUW_IN_BYTES:=0; //one FLOAT
	BIT_POSST:=0;
	BIT_FOMOST:=1;
	BIT_IOERRORW:=2;
	BIT_IOSIMUW:=3;
END_CONST

VAR 
   // Static variables
   DI_SET: STRUCT''')
	

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		if (RecordNumber > LimitSize):
			Name = instance.getAttributeData("DeviceIdentification:Name")
			self.thePlugin.writeInstanceInfo('''
   $Name$       : CPC_DB_DI;	// DI number <'''+str(RecordNumber)+'''>''')

	if (RecordNumber > LimitSize):
		self.thePlugin.writeInstanceInfo('''
END_STRUCT;
   
   DI AT DI_SET: ARRAY['''+str(LimitSize+1)+'''..'''+str(RecordNumber)+'''] OF CPC_DB_DI;
  
  // Support variables  
  old_status : DWORD;
  I: INT;
  IOError_Var: BOOL;
END_VAR
FOR I:='''+str(LimitSize+1)+''' TO '''+str(RecordNumber)+''' DO

	// Call the IO_ACCESS_DI function
	IF (DI[I].FEType <> 0) THEN
		IO_ACCESS_DI(
			 Channel := DI[I].perByte, 
			 Bit := DI[I].perBit,
			 FEType := DI[I].FEType,
			 InterfaceParam1 := DI[I].DBnum,
			 InterfaceParam2 := DI[I].perByte,
			 InterfaceParam3 := DI[I].perBit,
			 InterfaceParam4 := DI[I].DBnumIoError,
			 InterfaceParam5 := DI[I].DBposIoError,
			 InterfaceParam6 := DI[I].DBbitIoError,
			 HFPos := DI[I].HFPos,
			 IOError := IOError_Var);
	END_IF;
	
    old_status := DB_bin_status_DI.StsReg01[I].StsReg01;  
    old_status := ROR(IN:=old_status, N:=16);	
	''')
	
		if Diagnostic == "true":
			self.thePlugin.writeInstanceInfo('''

	// Assign the IOERROR
    IF (DI[I].FEType = 1) THEN                     
        DI[I].IoError := DI_ERROR.IOERROR[I].Err; // IO Hardware  
	ELSIF (DI[I].FEType = 0) THEN 
        ; // Object without connections.
    ELSE
        DI[I].IoError := IOError_Var;
    END_IF;

	''')
		else:
			self.thePlugin.writeInstanceInfo('''
    // No diagnostic
	// DI[I].IoError := IOError_Var;
	''')
	
	if (RecordNumber > LimitSize):
		self.thePlugin.writeInstanceInfo('''
    
	// Calls the Baseline function
    CPC_FB_DI.DB_DI(                                           
		Manreg01 :=  DB_DI_ManRequest.DI_Requests[I].Manreg01    // set by WinCCOA in the DB_DI_ManRequest
		,StsReg01 := DB_bin_status_DI.StsReg01[I].StsReg01
		,Perst := DI[I]
    ); 
''')
		if (bIsLargeApplication):
			self.thePlugin.writeInstanceInfo('''           
    DB_DI_All_S.DX [((I-1)*SIZE_DB_DI_S_IN_BYTES)+OFFSET_POSST_IN_BYTES,BIT_POSST]:= DI[I].PosSt;
    DB_DI_All_S.DX [((I-1)*SIZE_DB_DI_S_IN_BYTES)+OFFSET_FOMOST_IN_BYTES,BIT_FOMOST]:= DI[I].FoMoSt;
    DB_DI_All_S.DX [((I-1)*SIZE_DB_DI_S_IN_BYTES)+OFFSET_IOERRORW_IN_BYTES,BIT_IOERRORW]:= DI[I].IOErrorW;
    DB_DI_All_S.DX [((I-1)*SIZE_DB_DI_S_IN_BYTES)+OFFSET_IOSIMUW_IN_BYTES,BIT_IOSIMUW]:= DI[I].IOSimuW;
''')
		self.thePlugin.writeInstanceInfo('''    
    // Events
    DB_Event_DI.DI_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_DI.StsReg01[I].StsReg01;
	
 END_FOR;
END_FUNCTION_BLOCK
   ''')

 
	# Step 8: Create a instance DB_Type_all2 from FB_Type_all2 where we can write config and parameters for the Front-End
		#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.

	if (RecordNumber > LimitSize):
		self.thePlugin.writeInstanceInfo('''	
(* DB instance DI ******************************************)
DATA_BLOCK DB_DI_all2  FB_DI_all2
//
// Instance DB for the whole DI devices
//
BEGIN
	''')
			
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		if (RecordNumber > LimitSize):
			Name = instance.getAttributeData("DeviceIdentification:Name")
			FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
			if FEType == "":
				FEType = 0
				#self.thePlugin.writeWarningInUABLog("DI instance: $Name$. Undefined Type, FEEncondType = 0 is taken.")
			InterfaceParam1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
			InterfaceParam2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
			InterfaceParam3 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam3").strip().lower()
			InterfaceParam4 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam4").strip().lower()
			InterfaceParam5 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5").strip().lower()
			InterfaceParam6 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam6").strip().lower()
			self.thePlugin.writeInstanceInfo('''
DI_SET.$Name$.index := '''+str(RecordNumber)+''';
DI_SET.$Name$.FEType := '''+str(FEType)+''';''')
			#if FEType == "0":
			#	self.thePlugin.writeWarningInUABLog("DI instance: $Name$. FEEncondType = 0 No address configured.")
			if FEType == "1":
				IndexI = InterfaceParam1.index('i')
				pointIndex = InterfaceParam1.index('.')
				IndexByte = IndexI + 1
				Byte = InterfaceParam1[IndexByte:pointIndex]
				IndexBit = pointIndex + 1
				Bit = InterfaceParam1[IndexBit]
				self.thePlugin.writeInstanceInfo('''
DI_SET.$Name$.perByte := $Byte$;
DI_SET.$Name$.perBit := $Bit$;
''')	
			if FEType == "101":
				if InterfaceParam1[0:2] == "db":
					InterfaceParam1 = InterfaceParam1[2:]
				if InterfaceParam2[0:2] ==  "db":
					InterfaceParam2 = InterfaceParam2[3:]
				self.thePlugin.writeInstanceInfo('''
DI_SET.$Name$.DBnum:=$InterfaceParam1$;
DI_SET.$Name$.perByte:=$InterfaceParam2$;
DI_SET.$Name$.perBit:=$InterfaceParam3$;
		 ''')
			if FEType == "102":
				if InterfaceParam1[0:2] == "db":
					InterfaceParam1 = InterfaceParam1[2:]
				if InterfaceParam2[0:2] ==  "db":
					InterfaceParam2 = InterfaceParam2[3:]
				if InterfaceParam4[0:2] ==  "db":
					InterfaceParam4 = InterfaceParam4[2:]
				if InterfaceParam5[0:2] ==  "db":
					InterfaceParam5 = InterfaceParam5[3:]
				self.thePlugin.writeInstanceInfo('''
DI_SET.$Name$.DBnum:=$InterfaceParam1$;
DI_SET.$Name$.perByte:=$InterfaceParam2$;
DI_SET.$Name$.perBit:=$InterfaceParam3$;
DI_SET.$Name$.DBnumIoError:=$InterfaceParam4$;
DI_SET.$Name$.DBposIoError:=$InterfaceParam5$;
DI_SET.$Name$.DBbitIoError:=$InterfaceParam6$;
		 ''')
			if FEType == "103":
				if InterfaceParam1[0:2] == "db":
					InterfaceParam1 = InterfaceParam1[2:]
				if InterfaceParam2[0:2] ==  "db":
					InterfaceParam2 = InterfaceParam2[3:]
				if InterfaceParam4[0:2] ==  "db":
					InterfaceParam4 = InterfaceParam4[2:]
				if InterfaceParam5[0:2] ==  "db":
					InterfaceParam5 = InterfaceParam5[3:]
				self.thePlugin.writeInstanceInfo('''
DI_SET.$Name$.DBnum:=$InterfaceParam1$;
DI_SET.$Name$.perByte:=$InterfaceParam2$;
DI_SET.$Name$.perBit:=$InterfaceParam3$;
DI_SET.$Name$.DBnumIoError:=$InterfaceParam4$;
DI_SET.$Name$.DBposIoError:=$InterfaceParam5$;
DI_SET.$Name$.DBbitIoError:=$InterfaceParam6$;
		 ''')		
	if (RecordNumber > LimitSize):         
		self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK''')

	#Creation of a FC even for the optimized objects in order to call it in the OB1
	#This avoid the too many symbols problem of the OB1
	#The code is ready for all the object in case we need to split in several DBs one day. Note that for the moment,
	#this has sense only for the AIR and the DI where the split is working.
	NumberOfInstances = instancesVector.size()
	LimitSizeOfInstances = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", theCurrentDeviceType.getDeviceTypeName()))
	nameRepresentation = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", theCurrentDeviceType.getDeviceTypeName())
	self.thePlugin.writeInstanceInfo('''
FUNCTION FC_$nameRepresentation$ : VOID
    FB_$nameRepresentation$_all.DB_$nameRepresentation$_all();''')
	if (NumberOfInstances > LimitSizeOfInstances):
		self.thePlugin.writeInstanceInfo('''
    FB_$nameRepresentation$_all2.DB_$nameRepresentation$_all2();    // Too many '''+str(NumberOfInstances)+''' Need a second FB_$nameRepresentation$_All2''')
	self.thePlugin.writeInstanceInfo('''
END_FUNCTION
''')


   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for DI.")


   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for DI.")
		
