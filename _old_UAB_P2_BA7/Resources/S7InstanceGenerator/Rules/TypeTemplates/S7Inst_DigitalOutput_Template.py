# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class DigitalOutput_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for DO.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for DO.")

   def begin(self):
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'
	self.thePlugin.writeInUABLog("begin in Jython for DO.")

   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for DO.")
		
	# General Steps for Optimized objects:
	# 1. Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances) It's called DB_Type
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from DeviceTypeDefinition.xml created by TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5. Create the Type_Error DB if "Diagnostic" is required
	# 6. Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 7. Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#	 We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	# This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	Diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	
	# Step 1: Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances)
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Digital Output DB Creation file: UNICOS application
''')
	self.thePlugin.writeInstanceInfo('''
DATA_BLOCK DB_DO CPC_FB_DO
BEGIN
END_DATA_BLOCK
''')
	

	
	
	# Step 3: Create all the needed Structures from the TCT	
		
	# UDT for DB_DI_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE DO_ManRequest
TITLE = DO_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				attributePrimitiveType = attribute.getPrimitiveType()
				attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
				
				self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')



	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE DO_bin_Status
TITLE = DO_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE DO_event
TITLE = DO_event
//
// parameters of AI Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				# isEventAttribute = attribute.getIsEventAttribute()
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_DO_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_DO_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_DO_ManRequest
TITLE = DB_DO_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    DO_Requests : ARRAY [1..'''+instanceNb+'''] OF DO_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')
		
	# Step 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 
	
	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_DO Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_DO
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type DO
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_DO : INT := '''+str(NbType)+''';
	DO_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF DO_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')
	
	self.thePlugin.writeInstanceInfo('''
(****************** Status of the DOs *****************)
DATA_BLOCK DB_bin_status_DO
TITLE = 'DB_bin_status_DO'
//
// Global binaries status DB of DO
//
// List of variables:''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
   //  ['''+str(RecordNumber)+''']    $Name$      (DO_bin_Status)''')


	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF DO_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK

(****************** old Status of the DOs **********************)
DATA_BLOCK DB_bin_status_DO_old
TITLE = 'DB_bin_status_DO_old'
//
// Old Global binaries status DB of DO
//
// List of variables:''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
   //  ['''+str(RecordNumber)+''']    $Name$      (DO_bin_Status)''')

		
	self.thePlugin.writeInstanceInfo('''
	
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF DO_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK''')

	
	# Step 5: Create the Type_Error DB if "Diagnostic" is required
	
	if Diagnostic == "true":
		self.thePlugin.writeInstanceInfo('''
(*DB for IoError on Channels with OB82*)
DATA_BLOCK DO_ERROR
TITLE = DO_ERROR
//
// DB with IOError signals of DO
//
AUTHOR: AB_CO_IS
NAME: Error
FAMILY: Error
STRUCT
	
 IOERROR : ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_IOERROR;

END_STRUCT

BEGIN			
''')

		RecordNumber = 0
		for instance in instancesVector :
			# Now looping on each device instance.
			# 1 Building shortcuts
			# 2 Write code to output
			RecordNumber = RecordNumber + 1
			FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
			PLCChannel = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1")
			TypePLCChannel = PLCChannel[0:1]
			if TypePLCChannel == "P":
				NbPQPLCChannel = PLCChannel[2:8]
				self.thePlugin.writeInstanceInfo('''
IOERROR['''+str(RecordNumber)+'''].ADDR :=$NbPQPLCChannel$; ''')
			elif TypePLCChannel == "Q":
				NbQPLCChannel = PLCChannel[1:8]
				self.thePlugin.writeInstanceInfo('''
IOERROR['''+str(RecordNumber)+'''].ADDR :=$NbQPLCChannel$; ''')

		self.thePlugin.writeInstanceInfo('''
			
END_DATA_BLOCK''')

	# Step 6: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	
	self.thePlugin.writeInstanceInfo(''' 

(***************** EXEC OF DOs **************************)
FUNCTION_BLOCK FB_DO_all
TITLE = 'FB_DO_all'
//
// DO calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_DO'
FAMILY: 'DO'
VAR
   // Static variables
   DO_SET: STRUCT''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''
$Name$       : CPC_DB_DO;''')
   
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
   
   DDO AT DO_SET: ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_DB_DO;
  
  // Support variables  
  old_status : DWORD;
  I: INT;
  
END_VAR
FOR I:=1 TO '''+str(RecordNumber)+''' DO

    old_status := DB_bin_status_DO.StsReg01[I].StsReg01;
    old_status := ROR(IN:=old_status, N:=16);

''')
		
	if Diagnostic == "true":
		self.thePlugin.writeInstanceInfo('''	
 	IF (DDO[I].FEType = 1) THEN 
		DDO[I].IoError :=  DO_ERROR.IOERROR[I].Err;
	ELSE 
        ; // Object without connections.
    END_IF;   
		''')
	else:
		self.thePlugin.writeInstanceInfo('''
	// No diagnostic
	// DDO[I].IoError :=  DO_ERROR.IOERROR[I].Err;
		''')
	
	self.thePlugin.writeInstanceInfo('''
	
	// Calls the Baseline function
    CPC_FB_DO.DB_DO( 
		ManReg01:= DB_DO_ManRequest.DO_Requests[I].Manreg01    // set by WinCCOA in the DB_DO_ManRequest 
		,StsReg01:=DB_bin_status_DO.StsReg01[I].StsReg01
		,Perst := DDO[I]);


	// Call the IO_ACCESS_DO function
	IF (DDO[I].FEType <> 0) THEN
		IO_ACCESS_DO(Channel := DDO[I].perByte, 
             Bit := DDO[I].perBit,
             FEType := DDO[I].FEType,
             InterfaceParam1 := DDO[I].DBnum,
             InterfaceParam2 := DDO[I].perByte,
             InterfaceParam3 := DDO[I].perBit,
             InterfaceParam4 := DDO[I].DBnumIoError,
             InterfaceParam5 := DDO[I].DBposIoError,
             InterfaceParam6 := DDO[I].DBbitIoError,
             PosSt := DDO[I].PosSt,
             IOError := DDO[I].IoErrorW);
	END_IF;

	// Events
    DB_Event_DO.DO_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_DO.StsReg01[I].StsReg01;
    
END_FOR;
END_FUNCTION_BLOCK
     ''')
	 
	 
	# Step 7: Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.

	self.thePlugin.writeInstanceInfo('''
(******************* DB instance DO ************************)
DATA_BLOCK DB_DO_all  FB_DO_all
//
// Instance DB for the whole DO devices
//
BEGIN
	''')
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
		if FEType == "":
			FEType = 0
			#self.thePlugin.writeWarningInUABLog("DO instance: $Name$. Undefined Type, FEEncondType = 0 is taken.")
		InterfaceParam1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
		InterfaceParam2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
		InterfaceParam3 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam3").strip().lower()
		InterfaceParam4 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam4").strip().lower()
		InterfaceParam5 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5").strip().lower()
		InterfaceParam6 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam6").strip().lower()
		self.thePlugin.writeInstanceInfo('''
DO_SET.$Name$.index := '''+str(RecordNumber)+''';
DO_SET.$Name$.FEType := '''+str(FEType)+''';''')
		#if FEType == "0":
		#	self.thePlugin.writeWarningInUABLog("DO instance: $Name$. FEEncondType = 0 No address configured.")
		if FEType == "1":
			IndexQ = InterfaceParam1.index('q')
			pointIndex = InterfaceParam1.index('.')
			IndexByte = IndexQ + 1
			Byte = InterfaceParam1[IndexByte:pointIndex]
			IndexBit = pointIndex + 1
			Bit = InterfaceParam1[IndexBit]
			self.thePlugin.writeInstanceInfo('''
DO_SET.$Name$.perByte := $Byte$;
DO_SET.$Name$.perBit := $Bit$;
''')
		if FEType == "101":
			if InterfaceParam1[0:2] == "db":
				InterfaceParam1 = InterfaceParam1[2:]
			if InterfaceParam2[0:2] ==  "db":
				InterfaceParam2 = InterfaceParam2[3:]
			self.thePlugin.writeInstanceInfo('''
DO_SET.$Name$.DBnum:=$InterfaceParam1$;
DO_SET.$Name$.perByte:=$InterfaceParam2$;
DO_SET.$Name$.perBit:=$InterfaceParam3$;
		 ''')
		if FEType == "102":
			if InterfaceParam1[0:2] == "db":
				InterfaceParam1 = InterfaceParam1[2:]
			if InterfaceParam2[0:2] ==  "db":
				InterfaceParam2 = InterfaceParam2[3:]
			if InterfaceParam4[0:2] ==  "db":
				InterfaceParam4 = InterfaceParam4[2:]
			if InterfaceParam5[0:2] ==  "db":
				InterfaceParam5 = InterfaceParam5[3:]
			self.thePlugin.writeInstanceInfo('''
DO_SET.$Name$.DBnum:=$InterfaceParam1$;
DO_SET.$Name$.perByte:=$InterfaceParam2$;
DO_SET.$Name$.perBit:=$InterfaceParam3$;
DO_SET.$Name$.DBnumIoError:=$InterfaceParam4$;
DO_SET.$Name$.DBposIoError:=$InterfaceParam5$;
DO_SET.$Name$.DBbitIoError:=$InterfaceParam6$;
		 ''')
		if FEType == "103":
			if InterfaceParam1[0:2] == "db":
				InterfaceParam1 = InterfaceParam1[2:]
			if InterfaceParam2[0:2] ==  "db":
				InterfaceParam2 = InterfaceParam2[3:]
			if InterfaceParam4[0:2] ==  "db":
				InterfaceParam4 = InterfaceParam4[2:]
			if InterfaceParam5[0:2] ==  "db":
				InterfaceParam5 = InterfaceParam5[3:]
			self.thePlugin.writeInstanceInfo('''
DO_SET.$Name$.DBnum:=$InterfaceParam1$;
DO_SET.$Name$.perByte:=$InterfaceParam2$;
DO_SET.$Name$.perBit:=$InterfaceParam3$;
DO_SET.$Name$.DBnumIoError:=$InterfaceParam4$;
DO_SET.$Name$.DBposIoError:=$InterfaceParam5$;
DO_SET.$Name$.DBbitIoError:=$InterfaceParam6$;
		 ''')
	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK''')

	#Creation of a FC even for the optimized objects in order to call it in the OB1
	#This avoid the too many symbols problem of the OB1
	#The code is ready for all the object in case we need to split in several DBs one day. Note that for the moment,
	#this has sense only for the AIR and the DI where the split is working.
	NumberOfInstances = instancesVector.size()
	LimitSizeOfInstances = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", theCurrentDeviceType.getDeviceTypeName()))
	nameRepresentation = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", theCurrentDeviceType.getDeviceTypeName())
	self.thePlugin.writeInstanceInfo('''
FUNCTION FC_$nameRepresentation$ : VOID
    FB_$nameRepresentation$_all.DB_$nameRepresentation$_all();''')
	if (NumberOfInstances > LimitSizeOfInstances):
		self.thePlugin.writeInstanceInfo('''
    FB_$nameRepresentation$_all2.DB_$nameRepresentation$_all2();    // Too many '''+str(NumberOfInstances)+''' Need a second FB_$nameRepresentation$_All2''')
	self.thePlugin.writeInstanceInfo('''
END_FUNCTION
''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for DO.")


   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for DO.")
		
