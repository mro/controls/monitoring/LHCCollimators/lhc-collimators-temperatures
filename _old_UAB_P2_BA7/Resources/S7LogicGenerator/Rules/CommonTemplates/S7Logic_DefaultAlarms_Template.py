# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
##
# This file contains all the common functions of the logic templates.
##

from java.util import Vector
from java.util import ArrayList
from java.lang import System

def getLparametersSplit(LparamVector):
	"""
	These variable are set by default to map directly to the corresponding Specification parameters. E.g. Lparam1 = custom logic parameter1
	This (hopefully) should avoid any confusion with the 0-based index of LparamVector array.

	The arguments are:
	@param: LparamVector: the vector containing the 10 logic parameters

	This function returns:
	@return: The 10 parameters in independant variables

	"""
	Lparam1=LparamVector[0]
	Lparam2=LparamVector[1]
	Lparam3=LparamVector[2]
	Lparam4=LparamVector[3]
	Lparam5=LparamVector[4]
	Lparam6=LparamVector[5]
	Lparam7=LparamVector[6]
	Lparam8=LparamVector[7]
	Lparam9=LparamVector[8]
	Lparam10=LparamVector[9]

	return Lparam1,Lparam2,Lparam3,Lparam4,Lparam5,Lparam6,Lparam7,Lparam8,Lparam9,Lparam10

def getLparameters(inst):
	"""
	The arguments are:
	@param inst: object which is one instance i.e one line in the specification file,

	This function returns:
	@return: LparamVector: all the Lparameters in a vector
	"""

	Lparam1 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter1")
	Lparam2 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")
	Lparam3 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter3")
	Lparam4 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter4")
	Lparam5 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter5")
	Lparam6 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter6")
	Lparam7 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter7")
	Lparam8 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter8")
	Lparam9 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter9")
	Lparam10 = inst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter10")
	LparamVector = [Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10]
	return LparamVector

def getDigitalAlarms(theRawInstances, name):
	"""
	The arguments are:
	@param theRawInstances: object which represents the specification file,
	@param name: the name of the master

	This function returns:
	@return: theDigitalAlarms: all the simple digital alarm instances of the master currently treated (name), 
	@return: theDigitalAlarmsMultiple: all the multiple digital alarms instances of the master currently treated (name), 
	@return: allTheDigitalAlarms: all the digital alarms instances of the master currently treated (name),
	@return: DAListPosition: all the master position for each multiple alarm. The ith element of DAListPosition corresponds to the position of the master currently treated (name) in the master list of the ith element of theDigitalAlarmsMultiple
	""" 

	DAListPosition = ArrayList()
	theDigitalAlarms = theRawInstances.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Type#'!='', '#FEDeviceAlarm:Type#'!='Multiple'")
	theDigitalAlarmsMultiple = theRawInstances.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'!=''", DAListPosition)
	allTheDigitalAlarms = Vector(theDigitalAlarms)
	allTheDigitalAlarms.addAll(theDigitalAlarmsMultiple)
	return theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition

 
def getAnalogAlarms(theRawInstances, name):
	"""
	The arguments are:
	@param theRawInstances: object which represents the specification file,
	@param name: the name of the master

	This function returns: 
	@return theAnalogAlarms: all the simple analog alarm instances of the master currently treated (name), 
	@return theAnalogAlarmsMultiple: all the multiple analog alarms instances of the master currently treated (name), 
	@return allTheDigitalAlarms: all the analog alarms instances of the master currently treated (name), 
	@return AAListPosition: all the master position for each multiple alarm. The ith element of AAListPosition corresponds to the position of the master currently treated (name) in the master list of the ith element of theAnalogAlarmsMultiple
	"""

	AAListPosition = ArrayList()
	theAnalogAlarms = theRawInstances.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Type#'!='', '#FEDeviceAlarm:Type#'!='Multiple'")
	theAnalogAlarmsMultiple = theRawInstances.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'!=''", AAListPosition)
	allTheAnalogAlarms = Vector(theAnalogAlarms)
	allTheAnalogAlarms.addAll(theAnalogAlarmsMultiple)
	return theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition

def getFsAlarms(theRawInstances, name):
	"""
	The arguments are:
	@param theRawInstances: object which represents the specification file,
	@param name: the name of the master

	This function returns:
	@return: theDAFsAlarms: all the simple digital alarm instances of FS type of the master currently treated (name), 
	@return: theDAFsAlarmsMultiple: all the multiple digital alarms instances of FS type of the master currently treated (name), 
	@return: allTheDigitalAlarms: all the digital alarms instances of FS type of the master currently treated (name),
	@return: theAAFsAlarms: all the simple analog alarms instances of FS type of the master currently treated (name), 
	@return: theAAFsAlarmsMultiple: all the multiple analog alarms instances of FS type of the master currently treated (name), 
	@return: allTheAAFsAlarms: all the analog alarms instances of FS type of the master currently treated (name)
	"""

	theDAFsAlarms = theRawInstances.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Type#'='FS'")
	theDAFsAlarmsMultiple = theRawInstances.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='FS'")
	allTheDAFsAlarms = Vector(theDAFsAlarms)
	allTheDAFsAlarms.addAll(theDAFsAlarmsMultiple)
	
	theAAFsAlarms = theRawInstances.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Type#'='FS'")
	theAAFsAlarmsMultiple = theRawInstances.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='FS'")
	allTheAAFsAlarms = Vector(theAAFsAlarms)
	allTheAAFsAlarms.addAll(theAAFsAlarmsMultiple)
	return theDAFsAlarms, theDAFsAlarmsMultiple, allTheDAFsAlarms, theAAFsAlarms, theAAFsAlarmsMultiple, allTheAAFsAlarms

def getTsAlarms(theRawInstances, name):
	"""
	The arguments are:
	@param theRawInstances: object which represents the specification file,
	@param name: the name of the master

	This function returns:
	@return: theDATsAlarms: all the simple digital alarm instances of TS type of the master currently treated (name), 
	@return: theDATsAlarmsMultiple: all the multiple digital alarms instances of TS type of the master currently treated (name), 
	@return: allTheDATsAlarms: all the digital alarms instances of TS type of the master currently treated (name),
	@return: theAATsAlarms: all the simple analog alarms instances of TS type of the master currently treated (name), 
	@return: theAATsAlarmsMultiple: all the multiple analog alarms instances of TS type of the master currently treated (name), 
	allTheAATsAlarms: all the analog alarms instances of TS type of the master currently treated (name)
	"""

	theDATsAlarms = theRawInstances.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Type#'='TS'")
	theDATsAlarmsMultiple = theRawInstances.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='TS'")
	allTheDATsAlarms = Vector(theDATsAlarms)
	allTheDATsAlarms.addAll(theDATsAlarmsMultiple)
	
	theAATsAlarms = theRawInstances.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Type#'='TS'")
	theAATsAlarmsMultiple = theRawInstances.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='TS'")
	allTheAATsAlarms = Vector(theAATsAlarms)
	allTheAATsAlarms.addAll(theAATsAlarmsMultiple)
	return theDATsAlarms, theDATsAlarmsMultiple, allTheDATsAlarms, theAATsAlarms, theAATsAlarmsMultiple, allTheAATsAlarms

def getSiAlarms(theRawInstances, name):
	"""
	The arguments are:
	@param theRawInstances: object which represents the specification file,
	@param name: string containing the name of the master

	This function returns:
	@return: theDASiAlarms: all the simple digital alarm instances of SI type of the master currently treated (name), 
	@return: theDASiAlarmsMultiple: all the multiple digital alarms instances of SI type of the master currently treated (name), 
	@return: allTheDASiAlarms: all the digital alarms instances of SI type of the master currently treated (name),
	@return: theAASiAlarms: all the simple analog alarms instances of SI type of the master currently treated (name), 
	@return: theAASiAlarmsMultiple: all the multiple analog alarms instances of SI type of the master currently treated (name), 
	allTheAASiAlarms: all the analog alarms instances of SI type of the master currently treated (name)
	"""

	theDASiAlarms = theRawInstances.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Type#'='SI'")
	theDASiAlarmsMultiple = theRawInstances.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='SI'")
	allTheDASiAlarms = Vector(theDASiAlarms)
	allTheDASiAlarms.addAll(theDASiAlarmsMultiple)
	
	theAASiAlarms = theRawInstances.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Type#'='SI'")
	theAASiAlarmsMultiple = theRawInstances.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='SI'")
	allTheAASiAlarms = Vector(theAASiAlarms)
	allTheAASiAlarms.addAll(theAASiAlarmsMultiple)
	return theDASiAlarms, theDASiAlarmsMultiple, allTheDASiAlarms, theAASiAlarms, theAASiAlarmsMultiple, allTheAASiAlarms

def getAlAlarms(theRawInstances, name):
	"""
	The arguments are:
	@param theRawInstances: object which represents the specification file,
	@param name: string containing the name of the master

	This function returns:
	@return: theDAAlAlarms: all the simple digital alarm instances of AL type of the master currently treated (name), 
	@return: theDAAlAlarmsMultiple: all the multiple digital alarms instances of AL type of the master currently treated (name), 
	@return: allTheDAAlAlarms: all the digital alarms instances of AL type of the master currently treated (name),
	@return: theAAAlAlarms: all the simple analog alarms instances of AL type of the master currently treated (name), 
	@return: theAAAlAlarmsMultiple: all the multiple analog alarms instances of AL type of the master currently treated (name), 
	allTheAAAlAlarms: all the analog alarms instances of AL type of the master currently treated (name)
	"""

	theDAAlAlarms = theRawInstances.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Type#'='AL'")
	theDAAlAlarmsMultiple = theRawInstances.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='AL'")
	allTheDAAlAlarms = Vector(theDAAlAlarms)
	allTheDAAlAlarms.addAll(theDAAlAlarmsMultiple)
	
	theAAAlAlarms = theRawInstances.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Type#'='AL'")
	theAAAlAlarmsMultiple = theRawInstances.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Multiple Types#'='AL'")
	allTheAAAlAlarms = Vector(theAAAlAlarms)
	allTheAAAlAlarms.addAll(theAAAlAlarmsMultiple)
	return theDAAlAlarms, theDAAlAlarmsMultiple, allTheDAAlAlarms, theAAAlAlarms, theAAAlAlarmsMultiple, allTheAAAlAlarms


def writeConfiguredDAParameters(thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition):
	"""
	This function will write PLC code concerning the digital alarms. It will only write the code concerning the parameters of the digital alarms which are configured in the specification file.

	The arguments are:
	@param thePlugin: object representing the java plug-in 
	@param theDigitalAlarms: vector containing the simple digital alarms which have to be treated
	@param theDigitalAlarmsMultiple: vector containing the multiple digital alarms which have to be treated
	@param DAListPosition: list containing the position of the master for each multiple alarm. The ith element of DAListPosition should corresponds to the ith element of theDigitalAlarmsMultiple

	This function returns:
	nothing
	"""

	#thePlugin.writeDebugInUABLog("defaultAlarmsTemplate: executing writeConfiguredAlarmParameters function")
	
	# Step 1.3: Interlock Conditions (both for analog and digital alarms) and IOError/IOSimu for the Alarms (both for analog and digital alarms)
	thePlugin.writeSiemensLogic('''
(*Configured alarm parameters: Interlock Conditions to fill in according to the logic spec *)
(*Digital Interlock Conditions to fill in according to the logic spec*)
	// Simple Type DA conditions
''')
	
	for inst in theDigitalAlarms:
		DAName = inst.getAttributeData ("DeviceIdentification:Name")
		DAI = inst.getAttributeData ("FEDeviceEnvironmentInputs:Input")
		Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
		Description = inst.getAttributeData("DeviceDocumentation:Description")
		bufferWriteLogic=""
		
		# Input conditions
		if (DAI[0:3].lower() == "not"):
			DAI = DAI[4:len(DAI)].strip()
			Negation = "NOT"
		else:
			Negation = ""
		
		if DAI != "":
			s7db_id_result=thePlugin.s7db_id(DAI, "DigitalInput", True)  #return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications
			bufferWriteLogic = bufferWriteLogic +'''	DB_DA_All.DA_SET.$DAName$.I := $Negation$ '''+s7db_id_result+DAI+'''.PosSt;
	DB_DA_All.DA_SET.$DAName$.IOError := '''+s7db_id_result+DAI+'''.IOErrorW;
	DB_DA_All.DA_SET.$DAName$.IOSimu := '''+s7db_id_result+DAI+'''.IOSimuW OR '''+s7db_id_result+DAI+'''.FoMoSt;
'''

		# Delay Alarm conditions
		if Delay.strip() != "" and thePlugin.isString(Delay) and Delay.strip().lower() <> "logic":
			s7db_id_result=thePlugin.s7db_id(Delay, "AnalogParameter, AnalogStatus")
			bufferWriteLogic = bufferWriteLogic +'''	DB_DA_All.DA_SET.$DAName$.PAlDt := REAL_TO_INT('''+s7db_id_result+Delay+'''.PosSt);
			'''
		if len(bufferWriteLogic) > 0:	
			thePlugin.writeSiemensLogic('''	(* $Description$ *)
''')
		thePlugin.writeSiemensLogic(bufferWriteLogic)
		
	thePlugin.writeSiemensLogic('''	// Multiple Type DA conditions
''')
	
	i=0
	for inst in theDigitalAlarmsMultiple:
		DAName = inst.getAttributeData ("DeviceIdentification:Name")
		DAI = inst.getAttributeData ("FEDeviceEnvironmentInputs:Input")
		Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
		Description = inst.getAttributeData("DeviceDocumentation:Description")
		bufferWriteLogic=""
		positionDAMaster = DAListPosition[i]
		i = i+1
		if positionDAMaster == 0:
			# Input conditions
			if (DAI[0:3].lower() == "not"):
				DAI = DAI[4:len(DAI)].strip()
				Negation = "NOT"
			else:
				Negation = ""
			
			if DAI != "":
				s7db_id_result=thePlugin.s7db_id(DAI, "DigitalInput", True) #return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications
				bufferWriteLogic = bufferWriteLogic +'''	DB_DA_All.DA_SET.$DAName$.I := $Negation$ '''+s7db_id_result+DAI+'''.PosSt;
	DB_DA_All.DA_SET.$DAName$.IOError := '''+s7db_id_result+DAI+'''.IOErrorW;
	DB_DA_All.DA_SET.$DAName$.IOSimu := '''+s7db_id_result+DAI+'''.IOSimuW OR '''+s7db_id_result+DAI+'''.FoMoSt;
'''
			# Delay Alarm conditions
			if Delay.strip() != "" and thePlugin.isString(Delay) and Delay.strip().lower() <> "logic":
				s7db_id_result=thePlugin.s7db_id(Delay, "AnalogParameter, AnalogStatus")
				bufferWriteLogic = bufferWriteLogic +'''	DB_DA_All.DA_SET.$DAName$.PAlDt := REAL_TO_INT('''+s7db_id_result+Delay+'''.PosSt);
'''
			if len(bufferWriteLogic) > 0:	
				thePlugin.writeSiemensLogic('''	(* $Description$ *)
''')
			thePlugin.writeSiemensLogic(bufferWriteLogic)
		else:
			masterString = inst.getAttributeData ("LogicDeviceDefinitions:Master").replace(",", " ")
			masterList = masterString.split()
			firstMasterName = masterList[0]
			thePlugin.writeSiemensLogic('''	(* $Description$ *)
''')
			thePlugin.writeSiemensLogic('''	// For the Digital Alarm "$DAName$" the conditions have been created in the section: $firstMasterName$_DL
''')

def writeConfiguredAAParameters(thePlugin, theRawInstances, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition):
	"""
	This function will write PLC code concerning the analog alarms. It will only write the code concerning the parameters of the analog alarms which are configured in the specification file.

	The arguments are:
	@param thePlugin: object representing the java plug-in 
	@param theAnalogAlarms: vector containing the simple analog alarms which have to be treated
	@param theAnalogAlarmsMultiple: vector containing the multiple analog alarms which have to be treated
	@param AAListPosition: list containing the position of the master for each multiple alarm. The ith element of AAListPosition should corresponds to the ith element of theAnalogAlarmsMultiple

	This function returns:
	nothing
	"""

	thePlugin.writeSiemensLogic('''
(*Analog Interlock Conditions to fill in according to the logic spec*)
 ''')
	thePlugin.writeSiemensLogic('''	// Simple Type AA conditions
''')
	
	for inst in theAnalogAlarms:
		AAName = inst.getAttributeData ("DeviceIdentification:Name")
		AAI = inst.getAttributeData ("FEDeviceEnvironmentInputs:Input")
		Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
		HHAA = inst.getAttributeData ("FEDeviceManualRequests:HH Alarm")
		HWAA = inst.getAttributeData ("FEDeviceManualRequests:H Warning")
		LWAA = inst.getAttributeData ("FEDeviceManualRequests:L Warning")
		LLAA = inst.getAttributeData ("FEDeviceManualRequests:LL Alarm")
		Description = inst.getAttributeData("DeviceDocumentation:Description")
		bufferWriteLogic=""
		# Input conditions
		if AAI != "":
			s7db_id_result=thePlugin.s7db_id(AAI, "AnalogInput, AnalogInputReal, AnalogStatus", True) #return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.I := '''+s7db_id_result+AAI+'''.PosSt;
'''
			inst_I = theRawInstances.findMatchingInstances ("AnalogInput, AnalogInputReal, AnalogStatus","'#DeviceIdentification:Name#'='$AAI$'")
			for inst_I1 in inst_I:
				AA_I_Type = inst_I1.getDeviceType().getDeviceTypeName()
			if AA_I_Type == "AnalogInput" or AA_I_Type == "AnalogInputReal":
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.IOError := '''+s7db_id_result+AAI+'''.IOErrorW;
	DB_AA_All.AA_SET.$AAName$.IOSimu := '''+s7db_id_result+AAI+'''.IOSimuW OR '''+s7db_id_result+AAI+'''.FoMoSt;
'''
			else:
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.IOError := FALSE;
	DB_AA_All.AA_SET.$AAName$.IOSimu := FALSE;
'''
		# Delay Alarm conditions
		if Delay.strip() != "" and thePlugin.isString(Delay) and Delay.strip().lower() <> "logic":
			s7db_id_result=thePlugin.s7db_id(Delay, "AnalogParameter, AnalogStatus")
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.PAlDt := REAL_TO_INT('''+s7db_id_result+Delay+'''.PosSt);
'''

		# HH conditions
		if HHAA != "" and thePlugin.isString(HHAA) and HHAA.lower() <> "logic":
			s7db_id_result=thePlugin.s7db_id(HHAA, "AnalogParameter, AnalogStatus")
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.HH := '''+s7db_id_result+HHAA+'''.PosSt;
'''

		# HW conditions
		if HWAA != "" and thePlugin.isString(HWAA) and HWAA.lower() <> "logic" :
			s7db_id_result=thePlugin.s7db_id(HWAA, "AnalogParameter, AnalogStatus")
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.H := '''+s7db_id_result+HWAA+'''.PosSt;
'''

		# LW conditions
		if LWAA != "" and thePlugin.isString(LWAA) and LWAA.lower() <> "logic":
			s7db_id_result=thePlugin.s7db_id(LWAA, "AnalogParameter, AnalogStatus")
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.L := '''+s7db_id_result+LWAA+'''.PosSt;
'''

		# LL conditions
		if LLAA != "" and thePlugin.isString(LLAA) and LLAA.lower() <> "logic":
			s7db_id_result=thePlugin.s7db_id(LLAA, "AnalogParameter, AnalogStatus")
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.LL := '''+s7db_id_result+LLAA+'''.PosSt;
'''
		if len(bufferWriteLogic) > 0:	
			thePlugin.writeSiemensLogic('''	(* $Description$ *)
''')
		thePlugin.writeSiemensLogic(bufferWriteLogic)

	thePlugin.writeSiemensLogic('''	// Multiple Type AA conditions
''')
	
	i=0
	for inst in theAnalogAlarmsMultiple:
		AAName = inst.getAttributeData ("DeviceIdentification:Name")
		AAI = inst.getAttributeData ("FEDeviceEnvironmentInputs:Input")
		Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
		HHAA = inst.getAttributeData ("FEDeviceManualRequests:HH Alarm")
		HWAA = inst.getAttributeData ("FEDeviceManualRequests:H Warning")
		LWAA = inst.getAttributeData ("FEDeviceManualRequests:L Warning")
		LLAA = inst.getAttributeData ("FEDeviceManualRequests:LL Alarm")
		Description = inst.getAttributeData("DeviceDocumentation:Description")
		bufferWriteLogic=""
		
		positionAAMaster = AAListPosition[i]
		i = i+1
		if positionAAMaster == 0:		
			# Input conditions
			if AAI != "":
				s7db_id_result=thePlugin.s7db_id(AAI, "AnalogInput, AnalogInputReal, AnalogStatus", True)
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.I := '''+s7db_id_result+AAI+'''.PosSt;
'''
				inst_I = theRawInstances.findMatchingInstances ("AnalogInput, AnalogInputReal, AnalogStatus","'#DeviceIdentification:Name#'='$AAI$'")
				for inst_I1 in inst_I:
					AA_I_Type = inst_I1.getDeviceType().getDeviceTypeName()
				if AA_I_Type == "AnalogInput" or AA_I_Type == "AnalogInputReal":
					bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.IOError := '''+s7db_id_result+AAI+'''.IOErrorW;
	DB_AA_All.AA_SET.$AAName$.IOSimu := '''+s7db_id_result+AAI+'''.IOSimuW OR '''+s7db_id_result+AAI+'''.FoMoSt;
'''
				else:
					bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.IOError := FALSE;
	DB_AA_All.AA_SET.$AAName$.IOSimu := FALSE;
'''
			# Delay Alarm conditions
			if Delay.strip() != "" and thePlugin.isString(Delay) and Delay.strip().lower() <> "logic":
				s7db_id_result=thePlugin.s7db_id(Delay, "AnalogParameter, AnalogStatus")
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.PAlDt := REAL_TO_INT('''+s7db_id_result+Delay+'''.PosSt);
'''
			# HH conditions
			if HHAA != "" and thePlugin.isString(HHAA) and HHAA.lower() <> "logic":
				s7db_id_result=thePlugin.s7db_id(HHAA, "AnalogParameter, AnalogStatus")
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.HH := '''+s7db_id_result+HHAA+'''.PosSt;
'''
			# HW conditions
			if HWAA != "" and thePlugin.isString(HWAA) and HWAA.lower() <> "logic":
				s7db_id_result=thePlugin.s7db_id(HWAA, "AnalogParameter, AnalogStatus")
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.H := '''+s7db_id_result+HWAA+'''.PosSt;
'''

			# LW conditions
			if LWAA != "" and thePlugin.isString(LWAA) and LWAA.lower() <> "logic":
				s7db_id_result=thePlugin.s7db_id(LWAA, "AnalogParameter, AnalogStatus")
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.L := '''+s7db_id_result+LWAA+'''.PosSt;
'''

			# LL conditions
			if LLAA != "" and thePlugin.isString(LLAA) and LLAA.lower() <> "logic":
				s7db_id_result=thePlugin.s7db_id(LLAA, "AnalogParameter, AnalogStatus")
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.LL := '''+s7db_id_result+LLAA+'''.PosSt;
'''
			if len(bufferWriteLogic) > 0:	
				thePlugin.writeSiemensLogic('''	(* $Description$ *)
''')
			thePlugin.writeSiemensLogic(bufferWriteLogic)
			
		else:
			masterString = inst.getAttributeData ("LogicDeviceDefinitions:Master").replace(",", " ")
			masterList = masterString.split()
			firstMasterName = masterList[0]
			thePlugin.writeSiemensLogic('''	(* $Description$ *)
''')
			thePlugin.writeSiemensLogic('''        // For the Analog Alarm "$AAName$" the conditions have been created in the section: $firstMasterName$_DL
''')		


def logicErrorSimuAssignment(thePlugin, theRawInstances, allTheDigitalAlarms, allTheAnalogAlarms, name):
	"""
	This function will write PLC code concerning the digital alarms and the analogAlarms. More particularly it will write the code concerning the IOError and IOSimu. A link is created between the alarms and their master in order to propagate the IOError and the IOSimu signal coming from the alarms to their master.
	IOError is typically true when there is a problem in the INPUT-OUTPUT card.
	IOSimu is typically true when we pass on input or output in forced mode.

	The arguments are:
	@param thePlugin: object representing the java plug-in 
	@param theRawInstances: object which represents the specification file,
	@param allTheDigitalAlarms: vector containing the digital alarms which have to be treated
	@param allTheAnalogAlarms: vector containing the analog alarms which have to be treated
	@param name: string containing the name of the master

	This function returns:
	nothing
	"""

	#thePlugin.writeDebugInUABLog("defaultAlamrsTemplate: executing logicErrorSimuAssignment function")
	
	# Step 1.5. OnOff IoSimu and IoError: Adding of the IoError from the Logic related to the OnOff object
	thePlugin.writeSiemensLogic('''

(*Adding of the IoError from the Logic related to $name$*********)
$name$.IoError := DB_ERROR_SIMU.$name$_DL_E OR 
''')
	# First with the theDigitalAlarms
	for inst in allTheDigitalAlarms:
		DAName = inst.getAttributeData ("DeviceIdentification:Name")
		s7db_id_result=thePlugin.s7db_id(DAName, "DigitalAlarm")
		thePlugin.writeSiemensLogic('''       '''+s7db_id_result+DAName+'''.IOErrorW OR
''')
			
	# Second with the theAnalogAlarms
	for inst in allTheAnalogAlarms:
		AAName = inst.getAttributeData ("DeviceIdentification:Name")
		s7db_id_result=thePlugin.s7db_id(AAName, "AnalogAlarm")
		thePlugin.writeSiemensLogic('''       '''+s7db_id_result+AAName+'''.IOErrorW OR
''')
	
	thePlugin.writeSiemensLogic('''       0;

(*Adding of the IoSimu from the Logic related to $name$*********)
$name$.IoSimu := DB_ERROR_SIMU.$name$_DL_S OR 
''')

	# First with the theDigitalAlarms
	for inst in allTheDigitalAlarms:
		DAName = inst.getAttributeData ("DeviceIdentification:Name")
		s7db_id_result=thePlugin.s7db_id(DAName, "DigitalAlarm")
		thePlugin.writeSiemensLogic('''       '''+s7db_id_result+DAName+'''.IoSimuW OR
''')
			
	# Second with the theAnalogAlarms
	for inst in allTheAnalogAlarms:
		AAName = inst.getAttributeData ("DeviceIdentification:Name")
		s7db_id_result=thePlugin.s7db_id(AAName, "AnalogAlarm")
		thePlugin.writeSiemensLogic('''       '''+s7db_id_result+AAName+'''.IoSimuW OR
''')
			
	thePlugin.writeSiemensLogic('''       0;''')	
				
		
def AuAlAckAlarmsMethod(thePlugin, theRawInstances, name, master, theDigitalAlarms, theDigitalAlarmsMultiple, theAnalogAlarms, theAnalogAlarmsMultiple, DAListPosition, AAListPosition):
	"""
	This function will write PLC code concerning the digital alarms and the analogAlarms. More particularly it will write the code concerning the auto acknowledgment. A link is created between the alarms and their master in order to acknowledge the alarm if the master is acknowledged.

	The arguments are:
	@param thePlugin: object representing the java plug-in 
	@param theRawInstances: object which represents the specification file,
	@param name: string containing the name of the master
	@param theDigitalAlarms: vector containing the simple digital alarms which have to be treated
	@param theDigitalAlarmsMultiple: vector containing the multiple digital alarms which have to be treated
	@param theAnalogAlarms: vector containing the simple analog alarms which have to be treated
	@param theAnalogAlarmsMultiple: vector containing the multiple analog alarms which have to be treated
	@param DAListPosition: list containing the position of the master for each multiple alarm. The ith element of DAListPosition should corresponds to the ith element of theDigitalAlarmsMultiple
	@param AAListPosition: list containing the position of the master for each multiple alarm. The ith element of AAListPosition should corresponds to the ith element of theAnalogAlarmsMultiple

	This function returns:
	nothing
	"""

	#thePlugin.writeDebugInUABLog("defaultAlamrsTemplate: executing AuAlAckAlarmsMethod function")
	#thePlugin.writeInUABLog('''name = $name$. master = $master$''')
	CRLF = System.getProperty("line.separator")
	# Step 1.6: Instantiation of the AuAlAck for the Alarms objects related to the ANALOG object
	thePlugin.writeSiemensLogic('''
	
(*instantiation of the AuAlAck for the DigitalAlarm objects related to $name$*********)
	// Simple Type DA instantiation''')
	for inst in theDigitalAlarms:
		DAName = inst.getAttributeData ("DeviceIdentification:Name")
		thePlugin.writeSiemensLogic('''
	DB_DA_All.DA_SET.$DAName$.AuAlAck:=$name$.FE_AlUnAck;
''')
	
	thePlugin.writeSiemensLogic('''	// Multiple Type DA instantiation''')
	i=0
	for inst in theDigitalAlarmsMultiple:
		DAName = inst.getAttributeData ("DeviceIdentification:Name")
		positionDAMaster = DAListPosition[i]
		i = i+1
		if positionDAMaster == 0:
			alarmMaster = inst.getAttributeData("LogicDeviceDefinitions:Master").replace(",", " ")
			AuAlAckAssignment= '0'
			alarmMasterList = alarmMaster.split()
			for masterElement in alarmMasterList:
				AuAlAckAssignment = AuAlAckAssignment + " OR " + CRLF + masterElement + ".FE_AlUnAck"
			thePlugin.writeSiemensLogic('''
	DB_DA_All.DA_SET.$DAName$.AuAlAck:='''+AuAlAckAssignment+''';''')
	
	thePlugin.writeSiemensLogic('''
(*instantiation of the AuAlAck for the AnalogAlarm objects related to $name$*********)
	// Simple Type AA instantiation''')
	for inst in theAnalogAlarms:
		AAName = inst.getAttributeData ("DeviceIdentification:Name")
		thePlugin.writeSiemensLogic('''
	DB_AA_All.AA_SET.$AAName$.AuAlAck:=$name$.FE_AlUnAck;
''')
		
	thePlugin.writeSiemensLogic('''
	// Multiple Type AA instantiation''')
	i=0
	for inst in theAnalogAlarmsMultiple:
		AAName = inst.getAttributeData ("DeviceIdentification:Name")
		positionAAMaster = AAListPosition[i]
		i = i+1
		if positionAAMaster == 0:
			alarmMaster = inst.getAttributeData("LogicDeviceDefinitions:Master").replace(",", " ")
			AuAlAckAssignment= '0'
			alarmMasterList = alarmMaster.split()
			for masterElement in alarmMasterList:
				AuAlAckAssignment = AuAlAckAssignment + " OR " + CRLF + masterElement + ".FE_AlUnAck"
			thePlugin.writeSiemensLogic('''
	DB_AA_All.AA_SET.$AAName$.AuAlAck:='''+AuAlAckAssignment+''';
''')



def interlockAlarmsMethod(thePlugin, theRawInstances, allTheDASiAlarms, allTheAASiAlarms, allTheDAFsAlarms, allTheAAFsAlarms, allTheDATsAlarms, allTheAATsAlarms, allTheDAAlAlarms, allTheAAAlAlarms, name):
	"""
	This function will write PLC code concerning the digital alarms and the analogAlarms. More particularly it will write the code concerning the interlock.

	The arguments are:
	@param thePlugin: object representing the java plug-in 
	@param theRawInstances: object which represents the specification file,
	@param allTheDASiAlarms: vector containing the digital alarms of SI type which have to be treated
	@param allTheAASiAlarms: vector containing the analog alarms of SI type which have to be treated
	@param allTheDAFsAlarms: vector containing the digital alarms of FS type which have to be treated
	@param allTheAAFsAlarms: vector containing the analog alarms of FS type which have to be treated
	@param allTheDATsAlarms: vector containing the digital alarms of TS type which have to be treated
	@param allTheAATsAlarms: vector containing the analog alarms of TS type which have to be treated
	@param allTheDAAlAlarms: vector containing the digital alarms of AL type which have to be treated
	@param allTheAAAlAlarms: vector containing the analog alarms of AL type which have to be treated
	@param name: string containing the name of the master

	This function returns:
	nothing
	"""

	#thePlugin.writeDebugInUABLog("defaultAlamrsTemplate: executing interlockAlarmsMethod function")
					
	# Step 1.7: Interlock: Both for DA and AA. We fix the 2 kind of interlocks for Field Objects: SI and ST
	thePlugin.writeSiemensLogic('''
	
(*Interlock*************************)''')
	
	# Start Interlock (SI)
	thePlugin.writeSiemensLogic('''
$name$.StartI := 
	// Start Interlock for DA''')
	generatedTextDA = theRawInstances.createSectionText(allTheDASiAlarms, 1, 1, '''
    DB_DA_All.DA_SET.#DeviceIdentification:Name#.ISt OR''')
	thePlugin.writeSiemensLogic(generatedTextDA)
	thePlugin.writeSiemensLogic('''
	// Start Interlock for AA''')
	generatedTextAA = theRawInstances.createSectionText(allTheAASiAlarms, 1, 1, '''
    DB_AA_All.AA_SET.#DeviceIdentification:Name#.ISt OR''')
	thePlugin.writeSiemensLogic(generatedTextAA)
	thePlugin.writeSiemensLogic('''
    0;''')
				
	
	# Full Stop Interlock (FS)
	thePlugin.writeSiemensLogic('''
$name$.FuStopI := 
	// Full Stop Interlock for DA''')
	generatedTextDA = theRawInstances.createSectionText(allTheDAFsAlarms, 1, 1, '''
    DB_DA_All.DA_SET.#DeviceIdentification:Name#.ISt OR''')
	thePlugin.writeSiemensLogic(generatedTextDA)
	thePlugin.writeSiemensLogic('''
	// Full Stop Interlock for AA''')	
	generatedTextAA = theRawInstances.createSectionText(allTheAAFsAlarms, 1, 1, '''
    DB_AA_All.AA_SET.#DeviceIdentification:Name#.ISt OR''')	
	thePlugin.writeSiemensLogic(generatedTextAA)
	thePlugin.writeSiemensLogic('''
    0;''')
				
	
	# Temporary Stop Interlock (TS)
	thePlugin.writeSiemensLogic('''
$name$.TStopI := 
	// Temporary Stop Interlock for DA''')
	generatedTextDA = theRawInstances.createSectionText(allTheDATsAlarms, 1, 1, '''
    DB_DA_All.DA_SET.#DeviceIdentification:Name#.ISt OR''')
	thePlugin.writeSiemensLogic(generatedTextDA)
	thePlugin.writeSiemensLogic('''
	// Temporary Stop Interlock for AA''')	
	generatedTextAA = theRawInstances.createSectionText(allTheAATsAlarms, 1, 1, '''
    DB_AA_All.AA_SET.#DeviceIdentification:Name#.ISt OR''')	
	thePlugin.writeSiemensLogic(generatedTextAA)
	thePlugin.writeSiemensLogic('''
    0;''')

	# Al (AL)
	thePlugin.writeSiemensLogic('''
$name$.Al := 
	// Alarm for DA''')
	generatedTextDA = theRawInstances.createSectionText(allTheDAAlAlarms, 1, 1, '''
    DB_DA_All.DA_SET.#DeviceIdentification:Name#.ISt OR''')
	thePlugin.writeSiemensLogic(generatedTextDA)
	thePlugin.writeSiemensLogic('''
	// Alarm for AA''')	
	generatedTextAA = theRawInstances.createSectionText(allTheAAAlAlarms, 1, 1, '''
    DB_AA_All.AA_SET.#DeviceIdentification:Name#.ISt OR''')	
	thePlugin.writeSiemensLogic(generatedTextAA)
	thePlugin.writeSiemensLogic('''
    0;''')


def blockAlarmsMethod(thePlugin, theRawInstances, allTheDigitalAlarms, allTheAnalogAlarms, name):
	"""
	This function will write PLC code concerning the digital alarms and the analogAlarms. More particularly it will write the code concerning the blocked alarm. When an alarm is blocked the corresponding master have to know it that is why we create a link.

	The arguments are:
	@param thePlugin: object representing the java plug-in 
	@param theRawInstances: object which represents the specification file,
	@param allTheDigitalAlarms: vector containing the digital alarms which have to be treated
	@param allTheAnalogAlarms: vector containing the analog alarms which have to be treated
	@param name: string containing the name of the master

	This function returns:
	nothing
	"""

	# Step 1.8: Blocked Alarm warning: both for DA and AA
	thePlugin.writeSiemensLogic('''

(*Blocked Alarm warning ********************)
$name$.AlB := ''')
	generatedText = theRawInstances.createSectionText(allTheDigitalAlarms, 1, 1, '''
    DB_DA_All.DA_SET.#DeviceIdentification:Name#.MAlBRSt OR''')
	thePlugin.writeSiemensLogic(generatedText)
	generatedText = theRawInstances.createSectionText(allTheAnalogAlarms, 1, 1, '''
    DB_AA_All.AA_SET.#DeviceIdentification:Name#.MAlBRSt OR''')
	thePlugin.writeSiemensLogic(generatedText)
	thePlugin.writeSiemensLogic('''
    0;''')
	
def writeNotConfiguredDAParameters(thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition):
	"""
	This function will write PLC code concerning the digital alarms. It will only write the code concerning the parameters of the digital alarms which are not configured in the specification file.

	The arguments are:
	@param thePlugin: object representing the java plug-in 
	@param theDigitalAlarms: vector containing the simple digital alarms which have to be treated
	@param theDigitalAlarmsMultiple: vector containing the multiple digital alarms which have to be treated
	@param DAListPosition: list containing the position of the master for each multiple alarm. The ith element of DAListPosition should corresponds to the ith element of theDigitalAlarmsMultiple

	This function returns:
	nothing
	"""

	# Step 1.3: Interlock Conditions (both for analog and digital alarms) and IOError/IOSimu for the Alarms (both for analog and digital alarms)
	thePlugin.writeSiemensLogic('''
(*Not configured alarm parameters: Interlock Conditions to fill in according to the logic spec *)
(*Digital Interlock Conditions to fill in according to the logic spec*)
	// Simple Type DA conditions
''')
	
	for inst in theDigitalAlarms:
		DAName = inst.getAttributeData ("DeviceIdentification:Name")
		DAI = inst.getAttributeData ("FEDeviceEnvironmentInputs:Input")
		Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
		Description = inst.getAttributeData("DeviceDocumentation:Description")
		bufferWriteLogic=""
		
		# Input conditions
		if DAI == "":
			bufferWriteLogic = bufferWriteLogic + '''	DB_DA_All.DA_SET.$DAName$.I := 0; // To complete 
	DB_DA_All.DA_SET.$DAName$.IOError := 0; // To complete 
	DB_DA_All.DA_SET.$DAName$.IOSimu := 0; // To complete 
'''
		
		# Delay Alarm conditions
		if Delay.strip().lower() == "logic":
			bufferWriteLogic = bufferWriteLogic + ''' DB_DA_All.DA_SET.$DAName$.PAlDt := 0; // To complete 
'''
		if len(bufferWriteLogic) > 0:	
			thePlugin.writeSiemensLogic('''	(* $Description$ *)
''')
		
		thePlugin.writeSiemensLogic(bufferWriteLogic)
	thePlugin.writeSiemensLogic('''	// Multiple Type DA conditions
''')
	
	i=0
	for inst in theDigitalAlarmsMultiple:
		DAName = inst.getAttributeData ("DeviceIdentification:Name")
		DAI = inst.getAttributeData ("FEDeviceEnvironmentInputs:Input")
		Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
		Description = inst.getAttributeData("DeviceDocumentation:Description")
		bufferWriteLogic=""

		positionDAMaster = DAListPosition[i]
		i = i+1
		if positionDAMaster == 0:
			# Input conditions
			if DAI == "":
				bufferWriteLogic = bufferWriteLogic + '''	DB_DA_All.DA_SET.$DAName$.I := 0; // To complete 
	DB_DA_All.DA_SET.$DAName$.IOError := 0; // To complete 
	DB_DA_All.DA_SET.$DAName$.IOSimu := 0; // To complete 
'''
			
			# Delay Alarm conditions
			if Delay.strip().lower() == "logic":
				bufferWriteLogic = bufferWriteLogic +'''	DB_DA_All.DA_SET.$DAName$.PAlDt := 0; // To complete 
'''
			if len(bufferWriteLogic) > 0:	
				thePlugin.writeSiemensLogic('''	(* $Description$ *)
''')
			thePlugin.writeSiemensLogic(bufferWriteLogic)

def writeNotConfiguredAAParameters(thePlugin, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition):
	"""
	This function will write PLC code concerning the analog alarms. It will only write the code concerning the parameters of the analog alarms which are not configured in the specification file.

	The arguments are:
	@param thePlugin: object representing the java plug-in 
	@param theAnalogAlarms: vector containing the simple analog alarms which have to be treated
	@param theAnalogAlarmsMultiple: vector containing the multiple analog alarms which have to be treated
	@param AAListPosition: list containing the position of the master for each multiple alarm. The ith element of AAListPosition should corresponds to the ith element of theAnalogAlarmsMultiple

	This function returns:
	nothing
	"""

	thePlugin.writeSiemensLogic('''
(*Analog Interlock Conditions to fill in according to the logic spec*)
 ''')
	thePlugin.writeSiemensLogic('''	// Simple Type AA conditions
''')
	
	for inst in theAnalogAlarms:
		AAName = inst.getAttributeData ("DeviceIdentification:Name")
		AAI = inst.getAttributeData ("FEDeviceEnvironmentInputs:Input")
		Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
		HHAA = inst.getAttributeData ("FEDeviceManualRequests:HH Alarm")
		HWAA = inst.getAttributeData ("FEDeviceManualRequests:H Warning")
		LWAA = inst.getAttributeData ("FEDeviceManualRequests:L Warning")
		LLAA = inst.getAttributeData ("FEDeviceManualRequests:LL Alarm")
		Description = inst.getAttributeData("DeviceDocumentation:Description")
		bufferWriteLogic=""

		# Input conditions
		if AAI == "":
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.I := 0.0; // To complete 
	DB_AA_All.AA_SET.$AAName$.IOError := 0; // To complete 
	DB_AA_All.AA_SET.$AAName$.IOSimu := 0; // To complete 
'''


		# Delay Alarm conditions
		if Delay.strip().lower() == "logic":
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.PAlDt := 0; // To complete 
'''
		
		# HH conditions
		if HHAA.lower() == "logic":
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.HH := 0.0; // To complete
	DB_AA_All.AA_SET.$AAName$.AuEHH := TRUE; // To complete
'''
		elif HHAA != "":
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.AuEHH := TRUE; // To complete
'''

		# HW conditions
		if HWAA.lower() == "logic":
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.H := 0.0; // To complete
	DB_AA_All.AA_SET.$AAName$.AuEH := TRUE; // To complete
'''
		elif HWAA != "":
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.AuEH := TRUE; // To complete
'''
		
		# LW conditions
		if LWAA.lower() == "logic":
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.L := 0.0; // To complete
	DB_AA_All.AA_SET.$AAName$.AuEL := TRUE; // To complete
'''
		elif LWAA != "":
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.AuEL := TRUE; // To complete
'''

		
		# LL conditions
		if LLAA.lower() == "logic":
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.LL := 0.0; // To complete
	DB_AA_All.AA_SET.$AAName$.AuELL := TRUE; // To complete
'''	
		elif LLAA != "":
			bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.AuELL := TRUE; // To complete
'''
		if len(bufferWriteLogic) > 0:	
			thePlugin.writeSiemensLogic('''	(* $Description$ *)
''')
		thePlugin.writeSiemensLogic(bufferWriteLogic)

	thePlugin.writeSiemensLogic('''	// Multiple Type AA conditions
''')
	
	i=0
	for inst in theAnalogAlarmsMultiple:
		AAName = inst.getAttributeData ("DeviceIdentification:Name")
		AAI = inst.getAttributeData ("FEDeviceEnvironmentInputs:Input")
		Delay = inst.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
		HHAA = inst.getAttributeData ("FEDeviceManualRequests:HH Alarm")
		HWAA = inst.getAttributeData ("FEDeviceManualRequests:H Warning")
		LWAA = inst.getAttributeData ("FEDeviceManualRequests:L Warning")
		LLAA = inst.getAttributeData ("FEDeviceManualRequests:LL Alarm")
		Description = inst.getAttributeData("DeviceDocumentation:Description")
		bufferWriteLogic=""
		
		positionAAMaster = AAListPosition[i]
		i = i+1
		if positionAAMaster == 0:		
			# Input conditions
			if AAI == "":
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.I := 0.0; // To complete 
	DB_AA_All.AA_SET.$AAName$.IOError := 0; // To complete 
	DB_AA_All.AA_SET.$AAName$.IOSimu := 0; // To complete 
'''
			# Delay Alarm conditions
			if Delay.strip().lower() == "logic":
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.PAlDt := 0; // To complete 
'''	

			# HH conditions
			if HHAA.lower() == "logic":
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.HH := 0.0; // To complete
	DB_AA_All.AA_SET.$AAName$.AuEHH := TRUE; // To complete
'''
			elif HHAA != "":
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.AuEHH := TRUE; // To complete
'''

			# HW conditions
			if HWAA.lower() == "logic":
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.H := 0.0; // To complete
	DB_AA_All.AA_SET.$AAName$.AuEH := TRUE; // To complete
'''
			elif HWAA != "":
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.AuEH := TRUE; // To complete
'''

			# LW conditions
			if LWAA.lower() == "logic":
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.L := 0.0; // To complete
	DB_AA_All.AA_SET.$AAName$.AuEL := TRUE; // To complete
'''
			elif LWAA != "":
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.AuEL := TRUE; // To complete
'''

			# LL conditions
			if LLAA.lower() == "logic":
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.LL := 0.0; // To complete
	DB_AA_All.AA_SET.$AAName$.AuELL := TRUE; // To complete
'''	
			elif LLAA != "":
				bufferWriteLogic = bufferWriteLogic +'''	DB_AA_All.AA_SET.$AAName$.AuELL := TRUE; // To complete
'''
			if len(bufferWriteLogic) > 0:	
				thePlugin.writeSiemensLogic('''	(* $Description$ *)
''')
			thePlugin.writeSiemensLogic(bufferWriteLogic)
