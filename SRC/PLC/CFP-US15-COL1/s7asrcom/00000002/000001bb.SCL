//Specs version used for this generation: 1.0

//Digital Output DB Creation file: UNICOS application

DATA_BLOCK DB_DO CPC_FB_DO
BEGIN
END_DATA_BLOCK

TYPE DO_ManRequest
TITLE = DO_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
ManReg01 :WORD;
END_STRUCT
END_TYPE



TYPE DO_bin_Status
TITLE = DO_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
StsReg01 :WORD;
END_STRUCT
END_TYPE



TYPE DO_event
TITLE = DO_event
//
// parameters of AI Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
evStsReg01 : DWORD;
END_STRUCT
END_TYPE



// DB_DO_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_DO_ManRequest
TITLE = DB_DO_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
    DO_Requests : ARRAY [1..4] OF DO_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

			

// DB_Event_DO Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_DO
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type DO
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
	Nb_DO : INT := 4;
	DO_evstsreg : ARRAY [1..4] OF DO_event;
END_STRUCT;
BEGIN
END_DATA_BLOCK


(****************** Status of the DOs *****************)
DATA_BLOCK DB_bin_status_DO
TITLE = 'DB_bin_status_DO'
//
// Global binaries status DB of DO
//
// List of variables:
   //  [1]    BIC_US15_B1_PermitA      (DO_bin_Status)
   //  [2]    BIC_US15_B1_PermitB      (DO_bin_Status)
   //  [3]    BIC_US15_B2_PermitA      (DO_bin_Status)
   //  [4]    BIC_US15_B2_PermitB      (DO_bin_Status)
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..4] OF DO_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK

(****************** old Status of the DOs **********************)
DATA_BLOCK DB_bin_status_DO_old
TITLE = 'DB_bin_status_DO_old'
//
// Old Global binaries status DB of DO
//
// List of variables:
   //  [1]    BIC_US15_B1_PermitA      (DO_bin_Status)
   //  [2]    BIC_US15_B1_PermitB      (DO_bin_Status)
   //  [3]    BIC_US15_B2_PermitA      (DO_bin_Status)
   //  [4]    BIC_US15_B2_PermitB      (DO_bin_Status)
	
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..4] OF DO_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(*DB for IoError on Channels with OB82*)
DATA_BLOCK DO_ERROR
TITLE = DO_ERROR
//
// DB with IOError signals of DO
//
AUTHOR: AB_CO_IS
NAME: Error
FAMILY: Error
STRUCT
	
 IOERROR : ARRAY[1..4] OF CPC_IOERROR;

END_STRUCT

BEGIN			

IOERROR[1].ADDR :=0.0; 
IOERROR[2].ADDR :=0.1; 
IOERROR[3].ADDR :=0.2; 
IOERROR[4].ADDR :=0.3; 
			
END_DATA_BLOCK 

(***************** EXEC OF DOs **************************)
FUNCTION_BLOCK FB_DO_all
TITLE = 'FB_DO_all'
//
// DO calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_DO'
FAMILY: 'DO'
VAR
   // Static variables
   DO_SET: STRUCT
BIC_US15_B1_PermitA       : CPC_DB_DO;
BIC_US15_B1_PermitB       : CPC_DB_DO;
BIC_US15_B2_PermitA       : CPC_DB_DO;
BIC_US15_B2_PermitB       : CPC_DB_DO;
END_STRUCT;
   
   DDO AT DO_SET: ARRAY[1..4] OF CPC_DB_DO;
  
  // Support variables  
  old_status : DWORD;
  I: INT;
  
END_VAR
FOR I:=1 TO 4 DO

    old_status := DB_bin_status_DO.StsReg01[I].StsReg01;
    old_status := ROR(IN:=old_status, N:=16);

	
 	IF (DDO[I].FEType = 1) THEN 
		DDO[I].IoError :=  DO_ERROR.IOERROR[I].Err;
	ELSE 
        ; // Object without connections.
    END_IF;   
		
	
	// Calls the Baseline function
    CPC_FB_DO.DB_DO( 
		ManReg01:= DB_DO_ManRequest.DO_Requests[I].Manreg01    // set by WinCCOA in the DB_DO_ManRequest 
		,StsReg01:=DB_bin_status_DO.StsReg01[I].StsReg01
		,Perst := DDO[I]);


	// Call the IO_ACCESS_DO function
	IF (DDO[I].FEType <> 0) THEN
		IO_ACCESS_DO(Channel := DDO[I].perByte, 
             Bit := DDO[I].perBit,
             FEType := DDO[I].FEType,
             InterfaceParam1 := DDO[I].DBnum,
             InterfaceParam2 := DDO[I].perByte,
             InterfaceParam3 := DDO[I].perBit,
             InterfaceParam4 := DDO[I].DBnumIoError,
             InterfaceParam5 := DDO[I].DBposIoError,
             InterfaceParam6 := DDO[I].DBbitIoError,
             PosSt := DDO[I].PosSt,
             IOError := DDO[I].IoErrorW);
	END_IF;

	// Events
    DB_Event_DO.DO_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_DO.StsReg01[I].StsReg01;
    
END_FOR;
END_FUNCTION_BLOCK
     
(******************* DB instance DO ************************)
DATA_BLOCK DB_DO_all  FB_DO_all
//
// Instance DB for the whole DO devices
//
BEGIN
	
DO_SET.BIC_US15_B1_PermitA.index := 1;
DO_SET.BIC_US15_B1_PermitA.FEType := 1;
DO_SET.BIC_US15_B1_PermitA.perByte := 0;
DO_SET.BIC_US15_B1_PermitA.perBit := 0;

DO_SET.BIC_US15_B1_PermitB.index := 2;
DO_SET.BIC_US15_B1_PermitB.FEType := 1;
DO_SET.BIC_US15_B1_PermitB.perByte := 0;
DO_SET.BIC_US15_B1_PermitB.perBit := 1;

DO_SET.BIC_US15_B2_PermitA.index := 3;
DO_SET.BIC_US15_B2_PermitA.FEType := 1;
DO_SET.BIC_US15_B2_PermitA.perByte := 0;
DO_SET.BIC_US15_B2_PermitA.perBit := 2;

DO_SET.BIC_US15_B2_PermitB.index := 4;
DO_SET.BIC_US15_B2_PermitB.FEType := 1;
DO_SET.BIC_US15_B2_PermitB.perByte := 0;
DO_SET.BIC_US15_B2_PermitB.perBit := 3;

END_DATA_BLOCK
FUNCTION FC_DO : VOID
    FB_DO_all.DB_DO_all();
END_FUNCTION
