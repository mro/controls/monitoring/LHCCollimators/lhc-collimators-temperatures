//UNICOS
//� Copyright CERN 2013 all rights reserved

(* ANALOG OUTPUT OBJECT DATA STRUCTURE **************************)

TYPE CPC_DB_AO
TITLE ='CPC_DB_AO'
AUTHOR: 'EN/ICE'
VERSION: 6.0

  STRUCT     
     
     perAddress:        INT;   //Input periphery address
     FEType:            INT; 
     index:             INT;   //Index of the AO (recognize its status position) 
     AuPosR:            REAL;  //Real value
     PMinRan:           REAL;  //Range Min
     PMaxRan:           REAL;  //Range Max
     PMinRaw:           INT;   //Scale Min
     PMaxRaw:           INT;   //Scale Max
     PosSt:             REAL;  //Status (real value)
     MPosRSt:           REAL;  //Manual Position Request Status 
     AuPosRSt:          REAL;  //Auto Position Request Status 
     OutOV:             INT;   //Output order
     AuIhFoMo :         BOOL;  //Force mode blocked by logic (StsReg01 bit=13)
     AuMoSt:            BOOL;  //Auto Mode status (StsReg01 bit=2)
     FoMoSt:            BOOL;  //Force Mode status (StsReg01 bit 4)
     IOErrorW:          BOOL;  //Error in input (StsReg01 bit 6)
     IOSimuW:           BOOL;  //Input simulated (StsReg01 bit 7)
     IOError:           BOOL;
     IOSimu:            BOOL;
     FoDiAuW:           BOOL;  //Forced discordance (StsReg01 bit 8)
     PqwDef:            BOOL;  //PQW definition (true) or QW definition (false)
     MIOErBRSt:         BOOL;  //Manual IOError Block Request Status
     MIOErBSetRst_old:  BOOL;  //Manual IOError Block Set/Reset old value
     MNewMR_old:        BOOL;
  
  
  END_STRUCT     
END_TYPE

(* ANALOG OUTPUT OBJECT FUNCTION BLOCK **************************)

FUNCTION_BLOCK CPC_FB_AO
TITLE = 'CPC_FB_AO'
//
//  AO: ANALOG OUTPUT
//
VERSION: '6.0'
AUTHOR: 'EN/ICE'
NAME: 'OBJECT'
FAMILY: 'IO'


VAR_INPUT               
    
    Manreg01:                WORD;
    Manreg01b AT Manreg01:   ARRAY [0..15] OF BOOL;
    MPosR:                   REAL;

END_VAR

VAR_IN_OUT
   
    StsReg01:                WORD;
    Perst:                   CPC_DB_AO;
    
END_VAR    

VAR_TEMP                     
    
    E_MIOErBSetRst:          BOOL;
    Outovold:                INT;
    TempStsReg01:            WORD;
    StsReg01b AT TempStsReg01:   ARRAY [0..15] OF BOOL;
    
END_VAR 

BEGIN

(* tranfer StsReg01 to a temporary variable to be able to treat bits individually.
Not possible if the variable is declared as IN_OUT variable*)

TempStsReg01 := StsReg01;
 
(* INPUT MANAGER *)
E_MIOErBSetRst  := R_EDGE(new:=ManReg01b[2],old:=Perst.MIOErBSetRst_old);   (* MIOErBSetRst Manual IO Error Block Set/Reset*)

(*MODE MANAGER *)
     (*AUTO*)
         IF ManReg01b[8] OR Perst.AuIhFoMo THEN      (* MAuMoR *)
             Perst.FoMoSt := FALSE;                      
         END_IF;
     
     (*FORCED*)
             IF Manreg01b[10]  AND NOT Perst.AuIhFoMo  THEN             (* MFoMoR *)
             Perst.FoMoSt := TRUE;                      
         END_IF;
         
         Perst.AuMoSt := NOT Perst.FoMoSt;    
        
(*PROCESS_INPUT *)
(*REQUEST_MANAGER*)
    (*FORCED*) 
        IF ManReg01b[14] AND (Perst.MNewMR_old XOR ManReg01b[14])  THEN   (* MNewPosR *)
             Perst.MPosRSt := MPosR ;    
         END_IF;
        
        Perst.MNewMR_old := ManReg01b[14] ;
    
    (*AUTOMATIC*)   
        Perst.AuPosRSt := Perst.AuPosR ;
        
            IF Perst.AuPosRSt > Perst.PMaxRan  THEN
            Perst.AuPosRSt := Perst.PMaxRan ;
        END_IF;
        
        IF Perst.AuPosRSt < Perst.PMinRan  THEN
            Perst.AuPosRSt := Perst.PMinRan ;
        END_IF;   
               
    (*MODE EVALUATION*)
        IF Perst.FoMoSt THEN        
            Perst.PosSt := Perst.MPosRSt ;
            Perst.FoDiAuW := (ABS(Perst.AuPosRSt - Perst.MPosRSt) / ABS(Perst.PMaxRan - Perst.PMinRan)) > 0.01  ;   
        ELSE
            Perst.PosSt   := Perst.AuPosRSt ;
            Perst.MPosRSt := Perst.AuPosRSt ;
            Perst.FoDiAuW   := FALSE ;        
        END_IF;        
     
    
    (*BLOCK IO ERROR*)
        IF E_MIOErBSetRst THEN Perst.MIOErBRSt := NOT Perst.MIOErBRSt;         (* MIOErBRSt *)
        END_IF;

(*OUTPUT MANAGER *)
    (*OUTPUT VALUE*)
    Perst.OutOV := REAL_TO_INT(((INT_TO_REAL(Perst.PMaxRaw) - INT_TO_REAL(Perst.PMinRaw)) / (Perst.PMaxRan - Perst.PMinRan)) * (Perst.PosSt - Perst.PMinRan) + INT_TO_REAL(Perst.PMinRaw));
    Outovold := Perst.OutOV ;
    
(*SURVEILLANCE*)
    (* IOError IOSimu *)
         Perst.IOErrorW := Perst.IOError AND NOT Perst.MIOErBRSt;     
         Perst.IOSimuW  := Perst.IOSimu;               

    
    (* STATUS REGISTER *)  
    
    StsReg01b[8]  := 0;                   //StsReg01 Bit 00
    StsReg01b[9]  := 0;                   //StsReg01 Bit 01 
    StsReg01b[10] := Perst.AuMoSt;        //StsReg01 Bit 02
    StsReg01b[11] := 0;                   //StsReg01 Bit 03
    StsReg01b[12] := Perst.FoMoSt;        //StsReg01 Bit 04
    StsReg01b[13] := 0;                   //StsReg01 Bit 05
    StsReg01b[14] := Perst.IOErrorW;      //StsReg01 Bit 06
    StsReg01b[15] := Perst.IOSimuW;       //StsReg01 Bit 07
    StsReg01b[0]  := Perst.FoDiAuW;       //StsReg01 Bit 08
    StsReg01b[1]  := Perst.MIOErBRSt;     //StsReg01 Bit 09
    StsReg01b[2]  := 0;                   //StsReg01 Bit 10
    StsReg01b[3]  := 0;                   //StsReg01 Bit 11
    StsReg01b[4]  := 0;                   //StsReg01 Bit 12
    StsReg01b[5]  := Perst.AuIhFoMo;      //StsReg01 Bit 13
    StsReg01b[6]  := 0;                   //StsReg01 Bit 14
    StsReg01b[7]  := 0;                   //StsReg01 Bit 15

(* Values back to temporary variables*)
StsReg01 := TempStsReg01;

END_FUNCTION_BLOCK