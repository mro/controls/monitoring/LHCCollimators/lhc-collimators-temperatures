//UNICOS
//� Copyright CERN 2013 all rights reserved

(* DIGITAL ALARM OBJECT DATA STRUCTURE *****************)

TYPE CPC_DB_DA
TITLE ='CPC_DB_DA'
AUTHOR:'EN/ICE'
VERSION: 6.3

STRUCT
   
   PAlDt:              INT;
   PAuAckAl:           BOOL;
   I:                  BOOL; 
   AuAlAck:            BOOL; 
   ISt:                BOOL; 
   AlUnAck:            BOOL; 
   MAlBRSt:            BOOL;
   AuIhMB:             BOOL;
   IOErrorW:           BOOL;
   IOSimuW:            BOOL; 
   IOError:            BOOL;
   IOSimu:             BOOL;
   AuEAl:              BOOL := TRUE;
   
   // Old values Kept
   MAlBSetRst_old:     BOOL;
   MAlAck_old:         BOOL;
   AuAlAck_old:        BOOL;
   Alarm_Cond_old:     BOOL;
   AlUnAck_old:        BOOL;
       
   // Delayed properties
   AlarmPh1:           BOOL;
   AlarmPh2:           BOOL;
   
   WISt:               BOOL;
   TimeAlarm:          DINT;
   Iinc:               INT;


END_STRUCT;
END_TYPE


(* DIGITAL ALARM OBJECT FUNCTION BLOCK *****************)

FUNCTION_BLOCK CPC_FB_DA
TITLE = 'CPC_FB_DA'
//
//  DA: DIGITAL ALARM
//
VERSION: '6.3'
AUTHOR: 'EN/ICE'
NAME: 'OBJECT'
FAMILY: 'ALARM'


VAR_INPUT               
    
    Manreg01:                   WORD;
    Manreg01b AT Manreg01:      ARRAY [0..15] OF BOOL;
    PAuAckAl:                   BOOL;         (* Implementation note: use BOOL not ParReg bit 4 for optimization of memory *)
    
END_VAR

VAR_IN_OUT              
    
    StsReg01:                   WORD;
    Perst:                      CPC_DB_DA;

END_VAR

VAR_TEMP

    //R_EDGEs
    E_MAlBSetRst:               BOOL;
    E_MAlAck:                   BOOL;
    E_AuAlAck:                  BOOL;
    E_Alarm_Cond:               BOOL;
    RE_AlUnAck:                 BOOL;
    FE_AlUnAck:                 BOOL;   
    
    //Internal variables
    PosW:                       BOOL;
    ConfigW:                    BOOL;
    TempStsReg01:               WORD;
    StsReg01b AT TempStsReg01:  ARRAY [0..15] OF BOOL;

	//Variables for Alarm Status Status delay handling
    PulseWidth:             REAL;
	
END_VAR

BEGIN

(* tranfer StsReg01 to a temporary variable to be able to treat bits individually.
Not possible if the variable is declared as IN_OUT variable*)

TempStsReg01 := StsReg01;

(* INPUT_MANAGER *)

E_MAlBSetRst  := R_EDGE(new:=ManReg01b[2],old:=Perst.MAlBSetRst_old);   (* MAlBSetRst Manual Alarm Block Set/Reset *)
E_MAlAck      := R_EDGE(new:=ManReg01b[7],old:=Perst.MAlAck_old);     (* MAlAck *)

E_AuAlAck := R_EDGE(new:=Perst.AuAlAck,old:=Perst.AuAlAck_old);

(* Alarm Action Blocked *)
IF E_MAlBSetRst THEN 
    Perst.MAlBRSt:= NOT Perst.MAlBRSt;
END_IF;        

(* Manual inhibit block alarm *)
IF Perst.AuIhMB THEN 
    Perst.MAlBRSt:= FALSE;
END_IF;

(* Alarm input and Alarm delay mechanism *)

IF Perst.I AND Perst.AuEAl THEN 
    Perst.AlarmPh1:=true; 
ELSE
    Perst.AlarmPh1:=false;
    Perst.AlarmPh2:=false;
END_IF;

PosW := FALSE;

IF Perst.AlarmPh1 THEN 
    IF NOT(Perst.AlarmPh2) THEN                           // Time Smoothing activated 
        Perst.TimeAlarm:=UNICOS_LiveCounter + INT_TO_DINT(Perst.PAlDt);
        Perst.AlarmPh2:= TRUE;  
    END_IF;

    IF Perst.AlarmPh2  THEN
        IF (UNICOS_LiveCounter >= Perst.TimeAlarm)  THEN 
            IF Perst.MAlBRSt THEN
                Perst.ISt := FALSE;
                PosW := TRUE;
            ELSE
                Perst.ISt := TRUE;
                PosW := FALSE;
            END_IF;
        ELSIF Perst.MAlBRSt THEN
            Perst.ISt := FALSE;
            PosW := FALSE;
        ELSE
            Perst.ISt := FALSE;
            PosW := TRUE;
        END_IF;
    END_IF ;
        // Dealing with reinitialization of the PLC and then UNICOS_LiveCounter=0
        IF ((Perst.TimeAlarm - INT_TO_DINT(Perst.PAlDt)) > UNICOS_LiveCounter) THEN
            Perst.TimeAlarm := UNICOS_LiveCounter + INT_TO_DINT(Perst.PAlDt);
        END_IF ;
ELSE
   Perst.TimeAlarm:=0;
   Perst.ISt:=FALSE;
END_IF;  

E_Alarm_Cond := R_EDGE(new:=Perst.ISt,old:=Perst.Alarm_Cond_old);
  
(* Alarm Acknowledge *)

 IF E_MAlAck OR E_AuAlAck OR Perst.MAlBRSt OR Perst.PAuAckAl THEN           
     Perst.AlUnAck :=  FALSE; 
       ELSIF (E_Alarm_Cond) THEN 
             Perst.AlUnAck :=  TRUE; 
 END_IF;

(* Configuration Warning *)
 
 ConfigW := NOT Perst.AuEAl;
 
(* SURVEILLANCE: IO-ERROR/IO-SIMU WARNING *)

Perst.IOErrorW := Perst.IOError;
Perst.IOSimuW  := Perst.IOSimu;
 
(* Maintain Alarm Status 1.5s in Stsreg for PVSS *) 

PulseWidth := 1500 (* msec*) /DINT_TO_REAL(TIME_TO_DINT(T_CYCLE));

IF Perst.ISt OR Perst.Iinc > 0 THEN 
    Perst.Iinc := Perst.Iinc + 1; 
    Perst.WISt := TRUE; 
END_IF;
    
IF Perst.Iinc > PulseWidth OR (NOT Perst.ISt AND Perst.Iinc = 0) THEN 
    Perst.Iinc := 0; 
    Perst.WISt := Perst.ISt; 
END_IF;
     
(* STATUS REGISTER *)  
    
    StsReg01b[8]  := Perst.WISt;            //StsReg01 Bit 00
    StsReg01b[9]  := 0;                     //StsReg01 Bit 01
    StsReg01b[10] := 0;                     //StsReg01 Bit 02
    StsReg01b[11] := 0;                     //StsReg01 Bit 03
    StsReg01b[12] := 0;                     //StsReg01 Bit 04
    StsReg01b[13] := ConfigW;               //StsReg01 Bit 05
    StsReg01b[14] := Perst.IOErrorW;        //StsReg01 Bit 06
    StsReg01b[15] := Perst.IOSimuW;         //StsReg01 Bit 07
    StsReg01b[0]  := Perst.AuEAl;           //StsReg01 Bit 08
    StsReg01b[1]  := PosW;                  //StsReg01 Bit 09
    StsReg01b[2]  := 0;                     //StsReg01 Bit 10
    StsReg01b[3]  := 0;                     //StsReg01 Bit 11
    StsReg01b[4]  := Perst.AlUnAck;         //StsReg01 Bit 12
    StsReg01b[5]  := 0;                     //StsReg01 Bit 13
    StsReg01b[6]  := Perst.MAlBRSt;         //StsReg01 Bit 14
    StsReg01b[7]  := Perst.AuIhMB;          //StsReg01 Bit 15


(* Edges *)

  DETECT_EDGE(new:=Perst.AlUnAck,old:=Perst.AlUnAck_old,re:=RE_AlUnAck,fe:=FE_AlUnAck);
  
(* Values back to temporary variables*)

StsReg01 := TempStsReg01; 
  
END_FUNCTION_BLOCK