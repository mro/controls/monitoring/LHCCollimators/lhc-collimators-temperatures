//UNICOS
//� Copyright CERN 2013 all rights reserved

(* ANALOG INPUT OBJECT DATA STRUCTURE **************************)

TYPE CPC_DB_AI
TITLE ='CPC_DB_AI'
AUTHOR : 'EN/ICE'
VERSION: 6.0

  STRUCT     
   
   perAddress:        INT;    //Input periphery address
   FEType:            INT;    //Type of AI (0-> Direct address, 2-> Sipart Profibus PA)
   index:             INT;    //Index of the AI (recognize its status position) 
   HFPos:             INT;    //Value from periphery (INTEGER)
   PMinRan:           REAL;   //Range Min
   PMaxRan:           REAL;   //Range Max
   PMinRaw:           INT;    //Raw Min
   PMaxRaw:           INT;    //Raw Max
   PDb:               REAL;   //Dead Band
   PosSt:             REAL;   //Position Status
   HFSt:              REAL;   //Hardware Position Status
   MPosRSt:           REAL;   //Manual Position Request Status
   Autoi_old:         REAL;
   MNewMR_old:        BOOL;
   AuIhFoMo:          BOOL;   //Forced mode inhibited by logic
   AuMoSt:            BOOL;   //Auto Mode status (StsReg01 bit 2)
   FoMoSt:            BOOL;   //Force Mode status (StsReg01 bit 4)
   IOErrorW:          BOOL;   //Error in input (StsReg01 bit 6)
   IOSimuW:           BOOL;   //Input simulated (StsReg01 bit 7)
   IOError:           BOOL;
   IOSimu:            BOOL;
   FoDiProW:          BOOL;   //Forced Mode-Process discordance (StsReg01 bit 8)
   PiwDef:            BOOL;   //PIW definition (true) or IW definition (false)
   MIOErBRSt:         BOOL;   //Manual IOError Block Request Status
   MIOErBSetRst_old:  BOOL;   //Manual IOError Block Set/Reset Old value
  
  END_STRUCT     
END_TYPE



(* ANALOG INPUT OBJECT FUNCTION BLOCK **************************)

FUNCTION_BLOCK CPC_FB_AI 
TITLE = 'CPC_FB_AI'
//
// AI: ANALOG INPUT 
//
VERSION: '6.0'
AUTHOR: 'EN/ICE'
NAME: 'OBJECT'
FAMILY: 'IO'


VAR_INPUT  
                 
    Manreg01:                     WORD;
    Manreg01b AT Manreg01:        ARRAY [0..15] OF BOOL;
    MposR:                        REAL;

END_VAR


VAR_IN_OUT
    
    StsReg01:                     WORD;
    Perst:                        CPC_DB_AI;
    
END_VAR    


VAR_TEMP
    
    TempStsReg01:                 WORD;    
    StsReg01b AT TempStsReg01 :   ARRAY [0..15] OF BOOL;
    Autoi:                        REAL; 
    E_MIOErBSetRst:               BOOL;

END_VAR


BEGIN

(* tranfer StsReg01 to a temporary variable to be able to treat bits individually.
Not possible if the variable is declared as IN_OUT variable*)
TempStsReg01 := StsReg01;
 
(* INPUT MANAGER *)
E_MIOErBSetRst  := R_EDGE(new:=ManReg01b[2],old:=Perst.MIOErBSetRst_old);   (* MIOErBSetRst Manual IO Error Block Set/Reset*)

(*MODE MANAGER *)
     (*AUTO*)
         IF ManReg01b[8] OR Perst.AuIhFoMo THEN                 (* MAuMoR *)
             Perst.FoMoSt := FALSE;                      
         END_IF;
         
     (*FORCED*)
         IF Manreg01b[10]  AND NOT  Perst.AuIhFoMo  THEN         (* MFoMoR *) 
             Perst.FoMoSt := TRUE;                      
         END_IF;
         
         Perst.AuMoSt := NOT Perst.FoMoSt;    

(*PROCESS_INPUT *)
   (*ACQUISITION*)
         IF ManReg01b[14] AND (Perst.MNewMR_old XOR ManReg01b[14])  THEN      (* MNewMR *)
             Perst.MPosRSt := MPosR;    
         END_IF;
         Perst.MNewMR_old := ManReg01b[14];
    
    (*CONVERSION*)
         Autoi := ((Perst.PMaxRan - Perst.PMinRan) / (INT_TO_REAL(Perst.PMaxRaw) - INT_TO_REAL(Perst.PMinRaw))) * (INT_TO_REAL(Perst.HFPos) - INT_TO_REAL(Perst.PMinRaw)) + Perst.PMinRan  ;    
       
    (*REFRESHMENT*)
        IF ABS(Autoi - Perst.Autoi_old) * 100.0 / ABS(Perst.PMaxRan - Perst.PMinRan) >= Perst.PDb THEN 
           Perst.Autoi_old := Autoi;
        END_IF;
        Autoi := Perst.Autoi_old;
        Perst.HFSt := Autoi;
    
    (*MODE EVALUATION*)
        IF NOT (Perst.FoMoSt) THEN     (* AuMoSt *)
            Perst.MPosRSt := Autoi;
            Perst.PosSt := Autoi;
            Perst.FoDiProW := FALSE;  (* FoDiProW *)
        ELSE
            Perst.PosSt := Perst.MPosRSt ; 
            Perst.FoDiProW := (ABS(Perst.MPosRSt - Autoi) / ABS(Perst.PMaxRan - Perst.PMinRan)) > 0.01; (* FoDiProW *)
        END_IF;

     (*BLOCK IO ERROR*)
        IF E_MIOErBSetRst THEN Perst.MIOErBRSt := NOT Perst.MIOErBRSt;                   (* MIOErBSetRst *)
    END_IF;
       
(*SURVEILLANCE*)
    (* IOError IOSimu *)
        Perst.IOErrorW := Perst.IOError AND NOT Perst.MIOErBRSt;     (* MIOErBRSt, Manual IO Error block status *)
        Perst.IOSimuW  := Perst.IOSimu;               

(*OUTPUT MANAGER *)
(* STATUS REGISTER *)  
    
    StsReg01b[8]  := 0;                         //StsReg01 Bit 00
    StsReg01b[9]  := 0;                         //StsReg01 Bit 01 
    StsReg01b[10] := Perst.AuMoSt;              //StsReg01 Bit 02
    StsReg01b[11] := 0;                         //StsReg01 Bit 03
    StsReg01b[12] := Perst.FoMoSt;              //StsReg01 Bit 04
    StsReg01b[13] := 0;                         //StsReg01 Bit 05
    StsReg01b[14] := Perst.IOErrorW;            //StsReg01 Bit 06
    StsReg01b[15] := Perst.IOSimuW;             //StsReg01 Bit 07
    StsReg01b[0]  := Perst.FoDiProW;            //StsReg01 Bit 08
    StsReg01b[1]  := Perst.MIOErBRSt;           //StsReg01 Bit 09
    StsReg01b[2]  := 0;                         //StsReg01 Bit 10
    StsReg01b[3]  := 0;                         //StsReg01 Bit 11
    StsReg01b[4]  := 0;                         //StsReg01 Bit 12
    StsReg01b[5]  := Perst.AuIhFoMo;            //StsReg01 Bit 13
    StsReg01b[6]  := 0;                         //StsReg01 Bit 14
    StsReg01b[7]  := 0;                         //StsReg01 Bit 15

(* Values back to temporary variables*)
StsReg01 := TempStsReg01;

END_FUNCTION_BLOCK
