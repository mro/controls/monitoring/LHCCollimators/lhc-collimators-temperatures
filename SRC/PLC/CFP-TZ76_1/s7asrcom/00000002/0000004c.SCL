//UNICOS
//� Copyright CERN 2013 all rights reserved

(* PCO OBJECT FUNCTION BLOCK ****************************************)

FUNCTION_BLOCK CPC_FB_PCO
TITLE = 'CPC_FB_PCO'
//
//  PCO: Process Control Object
//
VERSION: '6.6'
AUTHOR: 'EN/ICE'
NAME: 'OBJECT'
FAMILY: 'PCO'


VAR_INPUT               

    IOError:               BOOL;
    IOSimu:                BOOL;
    AlB:                   BOOL;
    StartI:                BOOL;
    TStopI:                BOOL;
    FuStopI:               BOOL;
    Al:                    BOOL;
    Fon:                   BOOL;
    FOff:                  BOOL;
    CStopFin:              BOOL;
    AuOnR:                 BOOL;
    AuOffR:                BOOL;
    AuCStopR:              BOOL;
    AuAuMoR:               BOOL;
    AuIhMMo:               BOOL;
    AuIhFoMo:              BOOL;
    AuAuDepR:              BOOL;
    IhAuMRW:               BOOL;        
    AuRstart:              BOOL;
    AuAlAck:               BOOL;
    PPCO:                  CPC_PCO_PARAM;
    PPCOb AT PPCO:         STRUCT
    ParRegb:               ARRAY [0..15] OF BOOL;
                           END_STRUCT;
                           
    AuOpMoR:               REAL;
    MOpMoR:                REAL;
    POpMoTa:               ARRAY [0..7] OF BYTE; 
    POpMoTab AT POpMoTa:   ARRAY [0..7,0..7] OF BOOL;
    Manreg01:              WORD;
    Manreg01b AT Manreg01: ARRAY [0..15] OF BOOL;
    

END_VAR

VAR_OUTPUT              
    
    Stsreg01:              WORD;
    Stsreg01b AT Stsreg01: ARRAY [0..15] OF BOOL;
    Stsreg02:              WORD;
    Stsreg02b AT Stsreg02: ARRAY [0..15] OF BOOL;
    OpMoSt:                REAL;
    AuOpMoSt:              REAL;
    RunOSt:                BOOL;
    CStopOSt:              BOOL;
    AuDepOSt:              BOOL;
    OnSt:                  BOOL;
    OffSt:                 BOOL;
    AuMoSt:                BOOL;
    MMoSt:                 BOOL;
    FoMoSt:                BOOL;
    SoftLDSt:              BOOL;
    AlUnAck:               BOOL;
    TStopISt:              BOOL;
    FuStopISt:             BOOL;
    StartISt:              BOOL;
    AlSt:                  BOOL;
    IOErrorW:              BOOL;
    IOSimuW:               BOOL;
    AuMRW:                 BOOL;
    AlBW:                  BOOL;
    EnRstartSt:            BOOL := TRUE;
    RdyStartSt:            BOOL;
    
 END_VAR

 VAR
  
    //edges  
    E_MAuMoR:              BOOL; 
    E_MMMoR:               BOOL;  
    E_MFoMoR:              BOOL; 
    E_MOnR:                BOOL;
    E_MOffR:               BOOL;  
    E_MCStopR:             BOOL; 
    E_MAlAckR:             BOOL; 
    E_StartI:              BOOL;
    E_FuStopI:             BOOL;
    E_TStopI:              BOOL;
    E_Al:                  BOOL;
    E_FuStop_I:            BOOL;
    E_TStop_I:             BOOL;
    E_AuOnR:               BOOL;
    E_AuOffR:              BOOL;
    E_AuCStopR:            BOOL;
    E_AuAuMoR:             BOOL;
    E_MSetTSasFSSetRst:    BOOL;
    E_MAlBSetRst:          BOOL;
    E_MNewOpMoR:           BOOL;
    E_MAuDepR:             BOOL;
    E_MSoftLDR:            BOOL;
    E_MEnRstartR:          BOOL;
    E_AuAlAck:             BOOL;
    //old values
    MAuMoR_old:            BOOL; 
    MMMoR_old:             BOOL;  
    MFoMoR_old:            BOOL; 
    MOnR_old:              BOOL;
    MOffR_old:             BOOL;  
    MCStopR_old:           BOOL; 
    MOpMoR_old:            BOOL; 
    MAlAckR_old:           BOOL; 
    StartI_old:            BOOL;
    FuStopI_old:           BOOL;
    TStopI_old:            BOOL;
    Al_old:                BOOL;
    FuStop_I_old:          BOOL;
    TStop_I_old:           BOOL;
    AuOnR_old:             BOOL;
    AuOffR_old:            BOOL;
    AuCStopR_old:          BOOL;
    AuAuMoR_old:           BOOL;
    MSetTSasFSSetRst_old:  BOOL;
    MAlBSetRst_old:        BOOL;
    RunOSt_old:            BOOL;
    CStopOSt_old:          BOOL;
    AuDepOSt_old:          BOOL;
    AlUnAck_old:           BOOL;
    MSoftLDR_old:          BOOL; 
    MEnRstartR_old:        BOOL;
    MNewOpMoR_old:         BOOL;
    MAuDepR_old:           BOOL;
    AuAlAck_old:           BOOL;
     
    RE_RunOSt:             BOOL;
    FE_RunOSt:             BOOL;
    RE_CStopOSt:           BOOL;
    FE_CStopOSt:           BOOL;
    RE_AuDepOSt:           BOOL;
    FE_AuDepOSt:           BOOL;
    RE_AlUnAck:            BOOL;
    FE_AlUnAck:            BOOL;

    fullNotAcknowledged:   BOOL;
    AuRunOrder:            BOOL;
    AuOnRSt:               BOOL;
    MOnRSt:                BOOL;
    MAlBRSt:               BOOL;
    MTSasFSRSt:            BOOL;
    PEnRstart:             BOOL;
    PRstartFS:             BOOL;
    InterlockR:            BOOL;
    
    //variabes to handle delay in Interlock status
	WFuStopISt:            BOOL;
	WTStopISt:             BOOL;
	WStartISt:             BOOL;
	WAlSt:                 BOOL;
	PulseWidth:            REAL;
	FSIinc:                INT;
	TSIinc:                INT;
	SIinc:                 INT;
	Alinc:                 INT;
    
END_VAR

BEGIN


(* INPUT MANAGER *)
     
     E_MAuMoR           := R_EDGE(new:=ManReg01b[8],old:=MAuMoR_old); 
     E_MMMoR            := R_EDGE(new:=ManReg01b[9],old:=MMMoR_old);  
     E_MFoMoR           := R_EDGE(new:=ManReg01b[10],old:=MFoMoR_old);
     E_MSoftLDR         := R_EDGE(new:=ManReg01b[11],old:=MSoftLDR_old);         (*Manual Software Local Drive Request*)
     E_MOnR             := R_EDGE(new:=ManReg01b[12],old:=MOnR_old);
     E_MOffR            := R_EDGE(new:=ManReg01b[13],old:=MOffR_old);  
     E_MCStopR          := R_EDGE(new:=ManReg01b[14],old:=MCStopR_old); 

     E_MSetTSasFSSetRst := R_EDGE(new:=ManReg01b[0],old:=MSetTSasFSSetRst_old);  (* Manual Temporay Stop as Full Stop Set/Reset *)
     E_MEnRstartR       := R_EDGE(new:=ManReg01b[1],old:=MEnRstartR_old);        (* Manual Restart after full stop Request *)    
     E_MAlBSetRst       := R_EDGE(new:=ManReg01b[2],old:=MAlBSetRst_old);        (* Manual Alarm Blocked Set Request *)
     E_MNewOpMoR        := R_EDGE(new:=ManReg01b[4],old:=MNewOpMoR_old);         (* Manual New Option Mode Request *)
     E_MAuDepR          := R_EDGE(new:=ManReg01b[5],old:=MAuDepR_old);           (* Manual Request Auto to Dependant *)
     E_MAlAckR          := R_EDGE(new:=ManReg01b[7],old:=MAlAckR_old);     
        
     PEnRstart          := PPCOb.ParRegb[0];                                     (* Enable Restart after Full Stop *)
     PRstartFS          := PPCOb.ParRegb[1];                                     (* Enable Restart when Full Stop still active *)
     
     E_StartI           := R_EDGE(new:=StartI,old:=StartI_old);
     E_FuStopI          := R_EDGE(new:=FuStopI,old:=FuStopI_old);
     E_TStopI           := R_EDGE(new:=TStopI,old:=TStopI_old);
     E_Al               := R_EDGE(new:=Al,old:=Al_old); 
          
     E_FuStop_I         := R_EDGE(new:=FuStopI AND NOT MAlBRSt,old:=FuStop_I_old);
     E_TStop_I          := R_EDGE(new:=TStopI AND NOT MAlBRSt,old:=TStop_I_old);

     E_AuAuMoR          := R_EDGE(new:=AuAuMoR,old:=AuAuMoR_old); 
     E_AuAlAck          := R_EDGE(new:=AuAlAck,old:=AuAlAck_old); 
     
 (*INTERLOCK + ACKNOWLEDGED *)
         
     StartISt  := StartI ;
     TStopISt  := TStopI;
     FuStopISt := (FuStopI OR (TStopISt AND MTSasFSRSt));
     AlSt      := Al;
     
        IF ((PEnRstart AND (E_MEnRstartR OR AuRstart) AND NOT FuStopISt) OR (PEnRstart AND PRstartFS AND (E_MEnRstartR OR AuRstart))) AND NOT fullNotAcknowledged OR MAlBRSt THEN
            EnRstartSt := TRUE;
        END_IF;   
        
        IF E_MAlAckR OR E_AuAlAck THEN
            fullNotAcknowledged := FALSE; 
            AlUnAck :=   FALSE;  
        END_IF; 
  
     InterlockR := TStopISt OR FuStopISt OR FullNotAcknowledged OR NOT EnRstartSt OR (StartISt AND NOT RunOSt);
    
 (* MODE MANAGER *)
  
         (* Forced Mode *)
             IF (AuMoSt OR MMoSt OR SoftLDSt) AND 
                 E_MFoMoR AND NOT AuIhFoMo THEN
                    AuMoSt   := FALSE;
                    MMoSt    := FALSE;
                    FoMoSt   := TRUE;
                    SoftLDSt := FALSE;
            END_IF; 
         
         (* Manual Mode *)
             IF (AuMoSt OR FoMoSt OR SoftLDSt) AND 
                 E_MMMoR AND NOT AuIhMMo  THEN
                    AuMoSt   := FALSE;
                    MMoSt    := TRUE;
                    FoMoSt   := FALSE;
                    SoftLDSt := FALSE;
             END_IF;      
         
         (* Auto Mode *)
             IF (MMoSt AND (E_MAuMoR OR E_AuAuMoR )) OR
                (FoMoSt AND E_MAuMoR) OR 
                (SoftLDSt AND E_MAuMoR) OR
                (MMoSt AND AuIhMMo) OR 
                (FoMoSt AND AuIhFoMo)OR 
				(SoftLDSt AND AuIhFoMo) OR
                 NOT(AuMoSt OR MMoSt OR FoMoSt OR SoftLDSt) THEN
                    AuMoSt   := TRUE;
                    MMoSt    := FALSE;
                    FoMoSt   := FALSE;
                    SoftLDSt := FALSE;
             END_IF;
         
         (* Software Local Mode *)    
             IF (AuMoSt OR MMoSt) AND E_MSoftLDR AND NOT AuIhFoMo THEN   
                    AuMoSt   := FALSE;
                    MMoSt    := FALSE;
                    FoMoSt   := FALSE;
                    SoftLDSt := TRUE;
             END_IF;
         
 (* ON-OFF MANAGER *)
     (* ON *)
         OnSt  := FOn AND RunOSt;
     (* OFF *)
         OffSt := FOff AND NOT RunOSt;
           
 (* REQUEST MANAGER *)
     
     (*Manual*)
     
     IF E_MOnR AND (EnRstartSt OR MAlBRSt) THEN  
         MOnRSt := TRUE;
     END_IF;
     
     IF E_MOffR THEN 
         MOnRSt := FALSE;
     END_IF;
     
     (*Auto*)
     
     IF AuOnR AND NOT AuCStopR AND ((EnRstartSt AND NOT(fullNotAcknowledged OR TStopISt OR FuStopISt)) OR MAlBRSt)   THEN
         AuRunOrder := TRUE;
     END_IF;
     
     IF AuOffR THEN
         AuRunOrder := FALSE;
     END_IF;    
     
     AuOpMoSt  := AuOpMoR; 
               
     (*Control Stop*)
     
     IF  RunOSt AND ((AuCStopR AND AuMoSt) OR (E_MCStopR AND (MMoSt OR FoMoSt OR SoftLDSt))) THEN
         CStopOSt := TRUE;
     END_IF;        
     
     (* AUTO REQUEST / SELECT *)
         
     IF AuMoSt THEN    
        
        (*Avoid the starting of PCO with a start Interlock*)
        IF NOT (StartISt AND NOT RunOSt) THEN       
         RunOSt    := AuRunOrder;
         MOnRSt    := AuRunOrder;
        END_IF;
        
         AuDepOSt  := AuAuDepR;
         IF (0.0 < AuOpMoR) AND (AuOpMoR < 9.0) THEN     
                 IF OffSt THEN
                       OpMoSt := AuOpMoR;
                 ELSE
                       IF POpMoTab[REAL_TO_INT(OpMoSt-1),REAL_TO_INT(8-AuOpMoR)]  THEN
                           OpMoSt := AuOpMoR;
                        END_IF;              
                 END_IF;
         END_IF; 
             
     (* MANUAL REQUEST / SELECT *)    
         
     ELSIF MMoSt OR FoMoSt OR SoftLDSt THEN
               
              (*Avoid the starting of PCO with a start Interlock*) 
              IF NOT (StartISt AND NOT RunOSt) THEN
                   RunOSt := MOnRSt;                 
              END_IF; 
              
               AuDepOSt  := E_MAuDepR;
               IF E_MNewOpMoR AND (0.0 < MOpMoR) AND (MOpMoR < 9.0) THEN
                      IF OffSt OR FoMoSt THEN 
                           OpMoSt := MOpMoR; 
                      ELSE
                           IF POpMoTab[REAL_TO_INT(OpMoSt-1),REAL_TO_INT(8-MOpMoR)] THEN
                              OpMoSt := MOpMoR;
                           END_IF;   
                      END_IF;     
               END_IF;    
     END_IF;   
 
     (* RESET CONTROL STOP ORDER *)
     
         IF CStopFin AND CStopOSt THEN
             CStopOSt := FALSE;
             MOnRSt := FALSE;  
             IF AuCStopR THEN
                 AuRunOrder := FALSE;
             END_IF;
         END_IF;
         
         IF NOT(RunOSt) THEN
             CStopOSt := FALSE;
         END_IF;
             
     (* Alarm Acknowledgement *)
  
         IF E_StartI OR E_TStopI OR E_FuStopI OR E_Al THEN
             AlUnAck := TRUE;
         END_IF;        
                          
     (* FULL STOP INTERLOCK *)
         IF E_FuStop_I THEN
             fullNotAcknowledged := TRUE;
             MOnRSt := FALSE; 
             AuRunOrder := FALSE;
             IF PEnRstart THEN           
               EnRstartSt:= FALSE;
            END_IF;
         END_IF;
         
     (* TEMPORARY STOP as Full Stop INTERLOCK *)
  
         IF E_TStop_I THEN 
            IF MTSasFSRSt THEN
               fullNotAcknowledged := TRUE;
               MOnRSt := FALSE;
               AuRunOrder := FALSE;
               IF PEnRstart THEN
                    EnRstartSt:= FALSE;
               END_IF;

            END_IF;    
        END_IF;   
        
    (* INTERLOCK *)
    IF InterlockR AND NOT MAlBRSt THEN
        RunOSt:= FALSE;
    END_IF;                                                
     
     (* BLOCK ALARM *)
     
        IF E_MAlBSetRst THEN 
           MAlBRSt := NOT MAlBRSt; 
        END_IF;
     
     (* TEMPORARY STOP INTERLOCK AS FULL STOP INTERLOCK *) 
         
        IF E_MSetTSasFSSetRst THEN 
           MTSasFSRSt := NOT MTSasFSRSt; 
        END_IF;
     
    (* Ready to Start Status *)
        RdyStartSt := NOT InterlockR;

 (* SURVEILLANCE *)
     (* IO-ERROR-WARNING *)
         IOErrorW := IOError;
         IOSimuW  := IOSimu; 
     
     (* AUTO-MANUAL REQUEST WARNING / VERIFY *)
         AuMRW := (MMoSt OR FoMoSt OR SoftLDSt) AND (MOnRSt <> AuOnRSt) AND NOT(IhAuMRW) ;
    
 (* Option Mode forbidden value management *)  
    
    IF OpMoSt = 0.0 THEN
       OpMoSt:=1.0;
    END_IF;
    
(* Maintain interlocks status for 1.5s in Stsreg for PVSS*) 

PulseWidth := 1500 (* msec*) /DINT_TO_REAL(TIME_TO_DINT(T_CYCLE));


IF FuStopISt OR FSIinc > 0 THEN 
    FSIinc := FSIinc + 1; 
    WFuStopISt := TRUE; 
END_IF;

IF FSIinc > PulseWidth OR (NOT FuStopISt AND FSIinc = 0) THEN 
    FSIinc := 0; 
    WFuStopISt := FuStopISt; 
END_IF;

IF TStopISt OR TSIinc > 0 THEN 
    TSIinc := TSIinc + 1; 
    WTStopISt := True; 
END_IF;

IF TSIinc > PulseWidth OR (NOT TStopISt AND TSIinc = 0) THEN 
    TSIinc := 0; 
    WTStopISt := TStopISt; 
END_IF;    

if StartISt OR SIinc > 0 THEN 
    SIinc := SIinc + 1; 
    WStartISt:= True;
END_IF;

IF SIinc > PulseWidth OR (NOT StartISt AND SIinc = 0) THEN 
    SIinc := 0;
    WStartISt := StartISt; 
END_IF;

IF AlSt OR Alinc > 0 THEN 
    Alinc := Alinc + 1; 
    WAlSt := True; end_if;
    
IF Alinc > PulseWidth OR (NOT AlSt AND Alinc = 0) THEN 
    Alinc := 0; 
    WAlSt := AlSt; 
END_IF;

  
 (* STATUS REGISTER *)    
    
    AuOnRSt  :=  AuOnR AND NOT(AuCStopR);
    
    Stsreg01b[8]  := OnSt;          //StsReg01 Bit 00
    Stsreg01b[9]  := OffSt;         //StsReg01 Bit 01
    Stsreg01b[10] := AuMoSt;        //StsReg01 Bit 02
    Stsreg01b[11] := MMoSt;         //StsReg01 Bit 03
    Stsreg01b[12] := FoMoSt;        //StsReg01 Bit 04
    Stsreg01b[13] := 0;             //StsReg01 Bit 05
    Stsreg01b[14] := IOErrorW;      //StsReg01 Bit 06
    Stsreg01b[15] := IOSimuW;       //StsReg01 Bit 07
    stsreg01b[0]  := AuMRW;         //StsReg01 Bit 08
    Stsreg01b[1]  := WFuStopISt;     //StsReg01 Bit 09
    Stsreg01b[2]  := WStartISt;      //StsReg01 Bit 10
    Stsreg01b[3]  := WTStopISt;      //StsReg01 Bit 11
    Stsreg01b[4]  := AlUnAck;       //StsReg01 Bit 12
    Stsreg01b[5]  := AuIhFoMo;      //StsReg01 Bit 13
    Stsreg01b[6]  := WAlSt;          //StsReg01 Bit 14
    Stsreg01b[7]  := AuIhMMo;       //StsReg01 Bit 15
                                         
    Stsreg02b[8]  := RunOSt;        //StsReg02 Bit 00
    Stsreg02b[9]  := AuOnRSt;       //StsReg02 Bit 01
    Stsreg02b[10] := MOnRSt;        //StsReg02 Bit 02
    Stsreg02b[11] := AuAuDepR;      //StsReg02 Bit 03
    Stsreg02b[12] := AuDepOSt;      //StsReg02 Bit 04
    Stsreg02b[13] := AuCStopR;      //StsReg02 Bit 05
    Stsreg02b[14] := MAlBRSt;       //StsReg02 Bit 06
    Stsreg02b[15] := MTSasFSRSt;    //StsReg02 Bit 07
    stsreg02b[0]  := CStopOSt;      //StsReg02 Bit 08
    Stsreg02b[1]  := 0;             //StsReg02 Bit 09
    Stsreg02b[2]  := 0;             //StsReg02 Bit 10
    Stsreg02b[3]  := EnRstartSt;    //StsReg02 Bit 11
    Stsreg02b[4]  := SoftLDSt;      //StsReg02 Bit 12
    Stsreg02b[5]  := AlB;           //StsReg02 Bit 13
    Stsreg02b[6]  := 0;             //StsReg02 Bit 14
    Stsreg02b[7]  := 0;             //StsReg02 Bit 15

(* Edges *)

  DETECT_EDGE(new:=RunOSt,old:=RunOSt_old,re:=RE_RunOSt,fe:=FE_RunOSt);
  DETECT_EDGE(new:=CStopOSt,old:=CStopOSt_old,re:=RE_CStopOSt,fe:=FE_CStopOSt);
  DETECT_EDGE(new:=AuDepOSt,old:=AuDepOSt_old,re:=RE_AuDepOSt,fe:=FE_AuDepOSt);
  DETECT_EDGE(new:=AlUnAck,old:=AlUnAck_old,re:=RE_AlUnAck,fe:=FE_AlUnAck);    



END_FUNCTION_BLOCK

