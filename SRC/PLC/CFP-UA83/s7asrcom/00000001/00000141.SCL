//Specs version used for this generation: 1.0

//Analog Parameter DB Creation file: UNICOS application

DATA_BLOCK DB_APAR CPC_FB_APAR
BEGIN
END_DATA_BLOCK

TYPE APAR_ManRequest
TITLE = APAR_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
ManReg01 :WORD;
MPosR :REAL;
END_STRUCT
END_TYPE



TYPE APAR_bin_Status
TITLE = APAR_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
StsReg01 :WORD;
END_STRUCT
END_TYPE



TYPE APAR_ana_Status
TITLE = APAR_ana_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
PosSt :REAL;
MPosRSt :REAL;
END_STRUCT
END_TYPE



TYPE APAR_event
TITLE = APAR_event
//
// parameters of ANADIG Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
evStsReg01 : DWORD;
END_STRUCT
END_TYPE



// DB_APAR_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_APAR_ManRequest
TITLE = DB_APAR_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
    APAR_Requests : ARRAY [1..3] OF APAR_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

			

// DB_Event_APAR Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_APAR
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type APAR
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
	Nb_APAR : INT := 3;
	APAR_evstsreg : ARRAY [1..3] OF APAR_event;
END_STRUCT;
BEGIN
END_DATA_BLOCK


(************************* binary Status of the APARs ************************)
DATA_BLOCK DB_bin_status_APAR
TITLE = 'DB_bin_status_APAR'
//
// Global binary status DB of APAR
//
// List of variables:
//  [1]    BIC_UA83_UA83_AA_tt      (APAR_bin_Status)

//  [2]    BIC_UA83_UA83_DA_tt      (APAR_bin_Status)

//  [3]    BIC_UA83_UA83_AA_MAX_T      (APAR_bin_Status)

AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..3] OF APAR_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(********************* old binary Status of the APARs ****************************)
DATA_BLOCK DB_bin_status_APAR_old
TITLE = 'DB_bin_status_APAR_old'
//
// Old Global binary status DB of APAR
//

//  [1]    BIC_UA83_UA83_AA_tt      (APAR_bin_Status)

//  [2]    BIC_UA83_UA83_DA_tt      (APAR_bin_Status)

//  [3]    BIC_UA83_UA83_AA_MAX_T      (APAR_bin_Status)

AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..3] OF APAR_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(*analog status of the APARs************************************************)
DATA_BLOCK DB_ana_status_APAR
TITLE = 'DB_ana_status_APAR'
//
// Global Analog status DB of APAR
//
//  List of variables:
	//  [1]    BIC_UA83_UA83_AA_tt      (APAR_ana_Status)

	//  [2]    BIC_UA83_UA83_DA_tt      (APAR_ana_Status)

	//  [3]    BIC_UA83_UA83_AA_MAX_T      (APAR_ana_Status)

AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..3] OF APAR_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
// -----------------------------------------------------------------------------------------------------
(*old analog status of the APARs************************************************)
DATA_BLOCK DB_ana_Status_APAR_old
TITLE = 'DB_ana_Status_APAR_old'
//
// old Global Analog  status DB of APAR
//
// List of variables:  

//  [1]    BIC_UA83_UA83_AA_tt      (APAR_ana_Status)

//  [2]    BIC_UA83_UA83_DA_tt      (APAR_ana_Status)

//  [3]    BIC_UA83_UA83_AA_MAX_T      (APAR_ana_Status)

AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..3] OF APAR_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
   
(*APAR execution ********************************************)
FUNCTION_BLOCK FB_APAR_all
TITLE = 'FB_APAR_all'
//
// Call the APAR treatment
//
AUTHOR: 'UNICOS'
NAME: 'CallAPAR'
FAMILY: 'APAR'
VAR
  // Static Variables
  APAR_SET: STRUCT


   BIC_UA83_UA83_AA_tt       : CPC_DB_APAR;

   BIC_UA83_UA83_DA_tt       : CPC_DB_APAR;

   BIC_UA83_UA83_AA_MAX_T       : CPC_DB_APAR;

  END_STRUCT;  
  
  // Different variable view declaration
  APAR AT APAR_SET: ARRAY[1..3] OF CPC_DB_APAR;
  
  // Support variables
  old_status : DWORD;
  I: INT;
END_VAR
  FOR I:=1 TO 3 DO


	// Recovery static variables
	old_status := DB_bin_status_APAR.StsReg01[I].StsReg01;
    old_status := ROR(IN:=old_status, N:=16);
    
	// Status
	DB_APAR.StsReg01:=DB_bin_status_APAR.StsReg01[I].StsReg01;
	
	// Calls the Baseline function
    CPC_FB_APAR.DB_APAR(	
		 MPosR :=  DB_APAR_ManRequest.APAR_Requests[I].MPosR          // set by WinCCOA in the DB_APAR_ManRequest
		,Manreg01 :=  DB_APAR_ManRequest.APAR_Requests[I].Manreg01		// set by WinCCOA in the DB_APAR_ManRequest
		,StsReg01 := DB_bin_status_APAR.StsReg01[I].StsReg01
		,Perst := APAR[I]
			); 	
				
	// Update Event
    DB_Event_APAR.APAR_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_APAR.StsReg01[I].StsReg01;

	// Update Analog Status
	DB_ana_status_APAR.status[I].MPosRSt:= APAR[I].MPosRSt; 
	DB_ana_status_APAR.status[I].PosSt:= APAR[I].PosSt;
		
  END_FOR;
END_FUNCTION_BLOCK

(* All APAR devices instance DB  **************************************)     
DATA_BLOCK DB_APAR_all  FB_APAR_all
//
// Instance DB for the whole APAR devices initialization
//
BEGIN

APAR_SET.BIC_UA83_UA83_AA_tt.PosSt := 3.0;		

APAR_SET.BIC_UA83_UA83_DA_tt.PosSt := 9.0;		

APAR_SET.BIC_UA83_UA83_AA_MAX_T.PosSt := 500.0;		

END_DATA_BLOCK

FUNCTION FC_APAR : VOID
    FB_APAR_all.DB_APAR_all();
END_FUNCTION
