//UNICOS
//� Copyright CERN 2013 all rights reserved

(* Controller Object Function Block ***********************************************************)

FUNCTION_BLOCK CPC_FB_PID
TITLE = 'CPC_FB_PID'
//
//  PID: Controller Object (PID)
//
VERSION: '6.6'
AUTHOR: 'EN/ICE'
NAME: 'PID'
FAMILY: 'FO'

VAR_INPUT              
    
    HMV:                      REAL;
    HOutO:                    REAL;
    IOError:                  BOOL;
    IOSimu:                   BOOL;
    AuActR:                   BOOL;
    AuOutPR:                  BOOL;
    AuRegR:                   BOOL;
    AuTrR:                    BOOL;
    AuAuMoR:                  BOOL;
    AuIhSR:                   BOOL;      
    AuPosR:                   REAL;
    AuSPR:                    REAL;
    AuESP:                    BOOL;
    AuIhMMo:                  BOOL;
    AuIhFoMo:                 BOOL;
    AuSPSpd:                  CPC_RAMP_PARAM;
    AuPRest:                  BOOL;      // Automatic Params restore 
    AuPPID:                   CPC_PID_LIB_PARAM;  
    Manreg01:                 WORD;
    Manreg02:                 WORD;
    Manreg01b AT Manreg01:    ARRAY [0..15] OF BOOL;
    Manreg02b AT Manreg02:    ARRAY [0..15] OF BOOL;
    MPosR:                    REAL;
    MSP:                      REAL;
    MSPH:                     REAL;
    MSPL:                     REAL;
    MOutH:                    REAL;
    MOutL:                    REAL;
    MKc:                      REAL;
    MTd:                      REAL;
    MTi:                      REAL;
    MTds:                     REAL;
    PControl:                 CPC_PID_PARAM;

END_VAR

VAR_OUTPUT              
    
    OutOV:                    REAL;
    AuRegSt:                  BOOL;
    Stsreg01:                 WORD;
    Stsreg01b AT Stsreg01:    ARRAY [0..15] OF BOOL;
    Stsreg02:                 WORD;
    Stsreg02b AT Stsreg02:    ARRAY [0..15] OF BOOL;
    AuMoSt:                   BOOL;
    MMoSt:                    BOOL;
    FoMoSt:                   BOOL;
    SoftLDSt:                 BOOL;
    RegSt:                    BOOL;
    OutPSt:                   BOOL;
    TrSt:                     BOOL;
    IOErrorW:                 BOOL;
    IOSimuW:                  BOOL;
    ActSP:                    REAL;
    MSPSt:                    REAL;
    AuSPSt:                   REAL;
    MPosRSt:                  REAL;    
    AuPosRSt:                 REAL;
    ActKc:                    REAL;
    ActTi:                    REAL;
    ActTd:                    REAL;
    ActTds:                   REAL;
    ActSPH:                   REAL;
    ActSPL:                   REAL;
    ActOutH:                  REAL;
    ActOutL:                  REAL;
    MV:                       REAL;    
    
END_VAR

VAR
    // Default PID params
    DefKc:                    REAL;     // Default value: Proportinal Term
    DefTd:                    REAL;     // Default value: Derivative Term
    DefTi:                    REAL;     // Default value: Integral Term
    DefTds:                   REAL;     // Default value: derivative Filter
    
    // Default LIMITS params 
    DefSPH:                   REAL;     // Default value: SetPoint High
    DefSPL:                   REAL;     // Default value: SetPoint Low
    DefOutH:                  REAL;     // Default value: Output High
    DefOutL:                  REAL;     // Default value: Output Low
    
    // Default SP
    DefSP:                    REAL;     // Default value: SetPoint

    //edges  
    E_MAuMoR:                 BOOL;
    E_MMMoR:                  BOOL;
    E_MFoMoR:                 BOOL; 
    E_MPRest:                 BOOL;      // Default parameters activation RESTORE
    E_MPSav:                  BOOL;      // Default parameters SAVE   
    E_MNewPosR:               BOOL;
    E_MNewSPR:                BOOL;
    E_MNewSPHR:               BOOL;
    E_MNewSPLR:               BOOL;
    E_MNewOutHR:              BOOL;
    E_MNewOutLR:              BOOL;
    E_MNewKcR:                BOOL;
    E_MNewTdR:                BOOL;
    E_MNewTiR:                BOOL;
    E_MNewTdsR:               BOOL;
    E_MRegR:                  BOOL;
    E_MOutPR:                 BOOL;
    E_AuAuMoR:                BOOL;
    E_ArmRcp:                 BOOL;
    E_ActRcp:                 BOOL;
    E_MSoftLDR:               BOOL;
    
     //old values (edge detection)
    MAuMoR_old:               BOOL;
    MMMoR_old:                BOOL;
    MFoMoR_old:               BOOL; 
    MPRest_old:               BOOL;      // Default parameters activation RESTORE
    MPDefold:                 BOOL;      // Default parameters SAVE   
    MNewPosR_old:             BOOL;
    MNewSPR_old:              BOOL;
    MNewSPHR_old:             BOOL;
    MNewSPLR_old:             BOOL;
    MNewOutHR_old:            BOOL;
    MNewOutLR_old:            BOOL;
    MNewKcR_old:              BOOL;
    MNewTdR_old:              BOOL;
    MNewTiR_old:              BOOL;
    MNewTdsR_old:             BOOL;
    MRegR_old:                BOOL;
    MOutPR_old:               BOOL;
    AuAuMoR_old:              BOOL;
    ArmRcp_old:               BOOL;
    ActRcp_old:               BOOL;
    MSoftLDR_old:             BOOL; 
    
    //temp var
    KcDiDef:                  BOOL;
    TiDiDef:                  BOOL;
    TdDiDef:                  BOOL;
    TdsDiDef:                 BOOL;
    SPHDiDef:                 BOOL;
    SPLDiDef :                BOOL;
    OutHDiDef:                BOOL;
    OutLDiDef:                BOOL;
    ArmRcpSt:                 BOOL; 
    
    last_RegSt:               BOOL;      //Last working mode was regulation mode
    SPScaled:                 REAL;
    HMVScaled:                REAL;
    SPHScaled:                REAL;
    SPLScaled:                REAL;
    OutHScaled:               REAL;
    OutLScaled:               REAL;
    tracking_control:         BOOL;
    tracking_value :          REAL;
    tracking_value_Scaled:    REAL;
    ActSPR:                   REAL;
    dev:                      REAL;
    PID_Out:                  REAL;
    
    PID_activation:           BOOL;
     
    //FBs
    ROC_LIM:                  ROC_LIM;
    MVFILTER:                 LAG1ST;
    PID:                      PID;
    LMNGEN_C:                 LMNGEN_C;

END_VAR

BEGIN

(* INPUT_MANAGER *)
    
     E_MAuMoR     := R_EDGE(new:=ManReg01b[8],old:=MAuMoR_old);          (* 0: Manual Auto Mode Request   *)
     E_MMMoR      := R_EDGE(new:=ManReg01b[9],old:=MMMoR_old);           (* 1: Manual Manual Mode Request *)
     E_MFoMoR     := R_EDGE(new:=ManReg01b[10],old:=MFoMoR_old);         (* 2: Manual Forced Mode Request *)    
     E_ActRcp     := R_EDGE(new:=ManReg01b[11],old:=ActRcp_old);         (* 3: ActRcp Activate Recipe *)
     E_ArmRcp     := R_EDGE(new:=ManReg01b[12],old:=ArmRcp_old);         (* 4: ArmRcp Arm Recipe *)
     E_MPSav      := R_EDGE(new:=ManReg01b[13],old:=MPDefold);           (* 3: Default values activation SAVE *)
     E_MPRest     := R_EDGE(new:=ManReg01b[14],old:=MPRest_old);         (* 2: Default values activation RESTORE *)
     E_MNewSPR    := R_EDGE(new:=ManReg01b[15],old:=MNewSpR_old);        (* 7: MNewSpR Manual Setpoint Change *)
     
     E_MNewSPHR   := R_EDGE(new:=ManReg01b[0],old:=MNewSpHR_old);        (* 8: MNewSpHLR Manual Setpoint High Limit Change *)
     E_MNewSPLR   := R_EDGE(new:=ManReg01b[1],old:=MNewSpLR_old);        (* 9: MNewSpLLR Manual Setpoint Low Limit Change *)
     E_MNewOutHR  := R_EDGE(new:=ManReg01b[2],old:=MNewOutHR_old);       (* 10: MNewPosHLR Manual Output High Limit Change *)
     E_MNewOutLR  := R_EDGE(new:=ManReg01b[3],old:=MNewOutLR_old);       (* 11: MNewPosLLR Manual Output Low Limit Change *)
     E_MNewKcR    := R_EDGE(new:=ManReg01b[4],old:=MNewKcR_old);         (* 12: MNewKpR Manual Kc Parameter Request *)
     E_MNewTdR    := R_EDGE(new:=ManReg01b[5],old:=MNewTdR_old);         (* 13: MNewTdR Manual Td Parameter Request *)
     E_MNewTiR    := R_EDGE(new:=ManReg01b[6],old:=MNewTiR_old);         (* 14: MNewTiR Manual Ti Parameter Request *)
     E_MNewTdsR   := R_EDGE(new:=ManReg01b[7],old:=MNewTdsR_old);        (* 15: MNewKdR Manual Tds Parameter Request *)
         
     E_MRegR      := R_EDGE(new:=ManReg02b[8],old:=MRegR_old);           (* 0: MRegR MANUAL REGULATION REQUEST *)
     E_MOutPR     := R_EDGE(new:=ManReg02b[9],old:=MOutPR_old);          (* 1: MOutPR MANUAL Output positioning REQUEST *)
     E_MSoftLDR   := R_EDGE(new:=ManReg02b[10],old:=MSoftLDR_old);       (* 5: Manual Software Local Drive Request *)
     E_MNewPosR   := R_EDGE(new:=ManReg02b[11],old:=MNewPosR_old);       (* 6: MNewPosR Manual Position Request *)
     
     E_AuAuMoR    := R_EDGE(new:=AuAuMoR,old:=AuAuMoR_old);              (* Au_AU_Mo_R *)
    
              
(* Initialization of SP and Parameters if all are equal to zero *)
    
	IF  (ActSP = 0.0) AND (ActSPL = 0.0) AND (ActSPH = 0.0) AND (ActKc = 0.0) AND (ActTd = 0.0) AND (ACtTd = 0.0) AND (ACtTds = 0.0) AND (ActOutH = 0.0) AND (ActOutL = 0.0) THEN
            ActSP   := DefSP;
            ActSPL  := DefSPL;
            ActSPH  := DefSPH;
            ActKc   := DefKc;
            ACtTd   := DefTd;
            ActTi   := DefTi;
            ActTds  := DefTds;
            ActOutH := DefOutH;
            ActOutL := DefOutL;    
    END_IF;  
			  
(* OPERATION MODE MANAGER : Forced, Manual, Auto and Soft Local *)  
 
         (* Forced Mode *)
             IF (AuMoSt OR MMoSt OR SoftLDSt) AND 
                 E_MFoMoR AND NOT(AuIhFoMo) THEN
                    AuMoSt   := FALSE;
                    MMoSt    := FALSE;
                    FoMoSt   := TRUE;
                    SoftLDSt := FALSE;
            END_IF; 
         
         (* Manual Mode *)
             IF (AuMoSt OR FoMoSt OR SoftLDSt) AND 
                 E_MMMoR AND NOT(AuIhMMo) THEN
                    AuMoSt   := FALSE;
                    MMoSt    := TRUE;
                    FoMoSt   := FALSE;
                    SoftLDSt := FALSE;
             END_IF;      
         
         (* Auto Mode *)
             IF (MMoSt AND (E_MAuMoR OR E_AuAuMoR )) OR
                (FoMoSt AND E_MAuMoR) OR 
                (SoftLDSt AND E_MAuMoR) OR
                (MMoSt AND AuIhMMo) OR 
                (FoMoSt AND AuIhFoMo)OR 
                (SoftLDSt AND AuIhFoMo) OR
				 NOT(AuMoSt OR MMoSt OR FoMoSt OR SoftLDSt) THEN
                    AuMoSt   := TRUE;
                    MMoSt    := FALSE;
                    FoMoSt   := FALSE;
                    SoftLDSt := FALSE;
             END_IF;
         
         (* Software Local Mode *)    
             IF (AuMoSt OR MMoSt) AND 
                E_MSoftLDR AND NOT (E_MAuMoR OR E_MMMoR) AND NOT AuIhFoMo THEN 
                    AuMoSt   := FALSE;
                    MMoSt    := FALSE;
                    FoMoSt   := FALSE;
                    SoftLDSt := TRUE;
             END_IF;
      
  (* WORKING STATE MANAGER : Regulation, Output Positioning or Tracking*)
     (*Regulation Working State*)
         IF (AuMoSt AND AuRegR AND NOT TrSt) OR 
            ((MMoSt OR FoMoSt OR SoftLDSt) AND E_MRegR AND NOT TrSt) OR 
            (TrSt = TRUE AND AuTrR = FALSE AND last_RegSt = TRUE) THEN
             RegSt:= TRUE;                 
             OutPSt:= FALSE;
             TrSt := FALSE;
             Last_RegSt := TRUE;
             tracking_Control :=FALSE;
         END_IF;      
         
         
      (*Output Positioning Working State*)
        IF (AuMoSt AND AuOutPR AND NOT TrSt) OR 
           ((MMoSt OR FoMoSt OR SoftLDSt) AND E_MOutPR AND NOT TrSt) OR 
           (TrSt AND NOT AuTrR AND NOT last_RegSt) THEN
             RegSt:= FALSE;                 
             OutPSt:= TRUE;
             TrSt := FALSE;
             Last_RegSt := FALSE;
             tracking_Control :=TRUE;
         END_IF;      
   
    (*Tracking Working State*)
        IF AuTrR THEN
             RegSt:= FALSE;                 
             OutPSt:= FALSE;
             TrSt := TRUE;
             tracking_Control :=TRUE;
         END_IF;  
         
 (* SURVEILLANCE *)
     (* IO WARNING *)
         IOErrorW  := IOError;
         IOSimuW   := IOSimu;
         
 
(* ARM RECIPE *)
	IF E_ArmRcp AND NOT(FoMoSt OR SoftLDSt) THEN 
	   ArmRcpSt := TRUE;
	END_IF;

(* CANCEL RECIPE *)
	IF E_ArmRcp AND E_ActRcp THEN
	   ArmRcpSt := FALSE ;
	END_IF;
  
 (* SETPOINT MANAGER *)
     (* AUTO SET POINT ENABLE *)            
	 IF AuESP THEN                     // Logic Auto   
		 ActSPR := AuSPR;
	 ELSIF AuPRest THEN                // Logic auto default (RunOrder)
		ActSPR:= DefSP;               
	 ELSIF (E_MNewSPR AND NOT ArmRcpSt) OR 
		   (E_MNewSPR AND ArmRcpSt AND E_ActRcp) THEN
		 IF (E_MPSav) THEN        // SAVE Action
			DefSP:=MSP;           
		 ELSIF E_MPRest THEN       // RESTORE Action   
			ActSPR:= DefSP;            
		 ELSE                           // Manual Setting
			ActSPR := MSP;             
		 END_IF;
	 END_IF;
         
     (* LIMIT HIGH SATURATION *)        
	 IF AuPPID.ESPH THEN           // Logic Auto
		 ActSPH := AuPPID.SPH;    
	 ELSIF AuPRest THEN                // Logic auto default (RunOrder)
		 ActSPH := DefSPH;         
	 ELSIF (E_MNewSPHR AND NOT ArmRcpSt) OR 
		   (E_MNewSPHR AND ArmRcpSt AND E_ActRcp) THEN        
		IF (E_MPSav) THEN         // SAVE action
			DefSPH:=MSPH;             
		ELSIF E_MPRest THEN        // RESTORE Action
			ActSPH := DefSPH;         
		ELSE                            // Manual setting
			ActSPH := MSPH;           
		END_IF;
	 END_IF;
         
	 IF ActSPH > PControl.PMaxRan OR ActSPH < PControl.PMinRan THEN
		ActSPH := PControl.PMaxRan;
	 END_IF;
         
  (* LIMIT LOW SATURATION *)        
	 IF AuPPID.ESPL THEN           // Logic Auto
		 ActSPL := AuPPID.SPL;    
	 ELSIF AuPRest THEN                // Logic auto default (RunOrder)
		 ActSPL := DefSPL;         
	 ELSIF (E_MNewSPLR AND NOT ArmRcpSt) OR 
		   (E_MNewSPLR AND ArmRcpSt AND E_ActRcp) THEN            
		IF (E_MPSav) THEN         // SAVE action
			DefSPL:=MSPL;             
		ELSIF E_MPRest THEN        // RESTORE Action
			ActSPL := DefSPL;         
		ELSE                            // Manual setting
			ActSPL := MSPL;           
		END_IF;
	 END_IF;
         
	 IF ActSPL < PControl.PMinRan OR ActSPL > PControl.PMaxRan THEN
		 ActSPL := PControl.PMinRan;
	 END_IF;
                          
   (* SETPOINT DELTA *)             
     ROC_LIM (INV      := ActSPR, 
              UPRLM_P  := AuSPSpd.InSpd,
              DNRLM_P  := AuSPSpd.DeSpd,
              UPRLM_N  := AuSPSpd.InSpd,
              DNRLM_N  := AuSPSpd.DeSpd,
              H_LM     := ActSPH,
              L_LM     := ActSPL,
              PV       := HMV,
              DF_OUTV  := 0.0,
              DFOUT_ON := FALSE,
              TRACK    := FALSE,
              MAN_ON   := tracking_control,
              CYCLE    := "T_CYCLE");

  
     ActSP := ROC_LIM.OUTV;  
          
 (* CONTROLLER PARAM *)
     
     (* In case starting/stopping PCO or just asking for default values from WinCC OA then automatic parameters *)
          
	 (* Proportional term *)
	 IF AuPPID.EKc THEN                      // Logic auto
		 Actkc := AuPPID.Kc;
	 ELSIF AuPRest THEN                     // Logic auto default (RunOrder) 
		 Actkc := DefKc;                                          
	 ELSIF (E_MNewKcR AND NOT ArmRcpSt) OR 
		   (E_MNewKcR AND ArmRcpSt AND E_ActRcp) THEN               
		 IF (E_MPSav) THEN             // SAVE action 
			DefKc:=Mkc;     
		 ELSIF (E_MPRest) THEN
			Actkc := DefKc;        // RESTORE action                                    
		 ELSE
			Actkc := MKc;           // Manual setting
		 END_IF;
	 END_IF;     
         
	 (* Integral Term *)
	 IF AuPPID.ETi THEN                     // Logic auto
		 Actti :=AuPPID.Ti;
	 ELSIF AuPRest THEN                    // Logic auto default (RunOrder)      
		 Actti :=  DefTi;  
	 ELSIF (E_MNewTiR AND NOT ArmRcpSt) OR 
		   (E_MNewTiR AND ArmRcpSt AND E_ActRcp) THEN     
		 IF (E_MPSav) THEN            // SAVE action        
			DefTi:=MTi;                                
		 ELSIF E_MPRest THEN           // RESTORE action
			Actti :=  DefTi;  
		 ELSE                               // Manual setting
			Actti := MTi;   
		 END_IF;
	 END_IF;
         
	 (* Derivative Term *)
	 IF AuPPID.ETd THEN                         // Logic auto
		 Acttd := AuPPID.Td;
	 ELSIF AuPRest THEN                        // Logic auto default (RunOrder)
		 Acttd := DefTd;    
	 ELSIF (E_MNewTdR AND NOT ArmRcpSt) OR 
		   (E_MNewTdR AND ArmRcpSt AND E_ActRcp) THEN     
		 IF (E_MPSav) THEN                // SAVE action 
			DefTd:=MTd;
		 ELSIF E_MPRest THEN               // RESTORE action
			Acttd := DefTd;    
		 ELSE                                   // Manual setting 
			Acttd := MTd;    
		 END_IF;
	 END_IF;

	 (* Derivative FILTER *)
	 IF AuPPID.ETds THEN                          // Logic auto
		 ActTds :=AuPPID.Tds;
	 ELSIF AuPRest THEN                         // Logic auto default (RunOrder) 
		 ActTds := DefTds;   
	 ELSIF (E_MNewTdsR AND NOT ArmRcpSt) OR 
		   (E_MNewTdsR AND ArmRcpSt AND E_ActRcp) THEN     
		 IF (E_MPSav) THEN                 // SAVE action 
			DefTds:=MTds;                    
		 ELSIF E_MPRest THEN                // RESTORE action
			ActTds := DefTds;    
		 ELSE                                    // Manual setting
			ActTds := MTds;   
		END_IF;
	 END_IF;
     
    (*FILTERING MEASURED VALUE *)
    (* Saturation of HMV to avoid problems in the MVFILER *) 
	
	IF HMV >= PControl.PMaxRan THEN
		HMV := PControl.PMaxRan;
	END_IF;
	
	IF HMV <= PControl.PMinRan THEN
		HMV := PControl.PMinRan;
	END_IF;
	
	MVFILTER (INV    := HMV, // IN: REAL
              TM_LAG := PControl.MVFiltTime, // IN: TIME
              CYCLE  := PID_EXEC_CYCLE);  // IN: TIME
                   
  (* Reset Recipe *)
  
     IF (ArmRcpSt AND E_ActRcp) THEN ArmRcpSt := FALSE;
     END_IF;  
 
 (* OBJECT LOGIC *)
 (* OUTPUT MANAGER *)                        
   
     (* SATURATION HIGH *)
	 IF AuPPID.EOutH THEN                    // Logic auto
		 ActOutH := AuPPID.OutH;          
	 ELSIF AuPRest THEN       // Logic auto default (RunOrder) 
		 ActOutH := DefOutH;                  
	 ELSIF E_MNewOutHR THEN
		IF NOT ArmRcpSt THEN
			IF E_MPSav THEN                  // SAVE action 
				DefOutH := MOutH;
			ELSIF E_MPRest THEN              // RESTORE action
				ActOutH := DefOutH;                   
			ELSE
				ActOutH := MOutH;            // Manual setting
			END_IF;   
		ELSIF E_ActRcp THEN
			ActOutH := MOutH;
		END_IF;
	 END_IF;
         	 
         
       (*Percentage Scaling*)
       IF (PControl.ScaMethod = 1 AND (ActOutH>100.0 OR ActOutH<0.0)) THEN
            ActOutH:=100.0;
       END_IF;
       
       (*Full Scaling*)
       IF (PControl.ScaMethod = 2 AND (ActOutH > PControl.POutMaxRan  OR ActOutH < PControl.POutMinRan)) THEN
            ActOutH:=PControl.POutMaxRan;
       END_IF;
            
       (*No Scaling*)
       IF (PControl.ScaMethod = 3 AND (ActOutH > PControl.POutMaxRan  OR ActOutH < PControl.POutMinRan)) THEN
            ActOutH:=PControl.POutMaxRan;
       END_IF;
         
       (* SATURATION LOW *)
         IF AuPPID.EOutL THEN                    // Logic auto
             ActOutL := AuPPID.OutL;          
         ELSIF AuPRest THEN       // Logic auto default (RunOrder) 
             ActOutL := DefOutL;                  
         ELSIF E_MNewOutLR THEN
			IF NOT ArmRcpSt THEN
				IF E_MPSav THEN                  // SAVE action 
					DefOutL := MOutL;
				ELSIF E_MPRest THEN              // RESTORE action
					ActOutL := DefOutL;                   
				ELSE
					ActOutL := MOutL;            // Manual setting
				END_IF;   
			ELSIF E_ActRcp THEN
				ActOutL := MOutL;
			END_IF;
		 END_IF;
                 
       (*Percentage Scaling*)
       IF(PControl.ScaMethod = 1 AND (ActOutL > 100.0 OR ActOutL < 0.0)) THEN
            ActOutL := 0.0;
       END_IF;
       
       (*Full Scaling*)
       IF (PControl.ScaMethod = 2 AND (ActOutL > PControl.POutMaxRan  OR ActOutL < PControl.POutMinRan)) THEN
            ActOutL := PControl.POutMinRan;
       END_IF;
            
       (*No Scaling*)
       IF (PControl.ScaMethod = 3 AND (ActOutL > PControl.POutMaxRan  OR ActOutL < PControl.POutMinRan)) THEN
            ActOutL := PControl.POutMinRan;
       END_IF;
         
         
     (* MODE EVALUATION *)
     AuPosRSt := AuPosR;
     
     (*OUTPUT POSITIONING *)
     IF OutPSt THEN
        ActSP:=HMV;
        IF(MMoSt OR FoMoSt OR SoftLDSt) THEN
             IF E_MNewPosR THEN  
                 MPosRSt := MPosR;
             END_IF;     
             tracking_value   := MPosRSt;
         ELSE
            AuPosRSt := AuPosR;
            MPosRSt  := AuPosR;
            tracking_value   := AuPosRSt;
        END_IF;       
     END_IF;

    (*REGULATION*)    
    IF RegSt THEN
        IF(MMoSt OR FoMoSt OR SoftLDSt) THEN 
             MPosRSt := OutOV;
         ELSE
             MPosRSt := AuPosR;
         END_IF;
     END_IF;

    (*TRACKING*)
    IF TrSt THEN
        ActSP:=HMV;
        tracking_value   := HOutO;
     END_IF;
             
 (* TRACKING MANAGER *)
    
    (* PID SCALING*)
    (* Percentage Scaling*)
     IF (PControl.ScaMethod = 1) THEN
        SPScaled := 100.0 * (ActSP - PControl.PMinRan)/(PControl.PMaxRan - PControl.PMinRan);
        HMVScaled := 100.0 * (MVFILTER.OUTV - PControl.PMinRan)/(PControl.PMaxRan - PControl.PMinRan);
        SPHScaled :=  100.0 * (ActSPH - PControl.PMinRan)/(PControl.PMaxRan - PControl.PMinRan);
        SPLScaled :=  100.0 * (ActSPL - PControl.PMinRan)/(PControl.PMaxRan - PControl.PMinRan);
        OutHScaled := ActOutH;
        OutLScaled := ActOutL;
        tracking_value_Scaled := tracking_value;
    END_IF;
    
    (*Full Scaling*)
    IF (PControl.ScaMethod = 2) THEN
        SPScaled := 100.0 * (ActSP - PControl.PMinRan)/(PControl.PMaxRan - PControl.PMinRan);
        HMVScaled := 100.0 * (MVFILTER.OUTV - PControl.PMinRan)/(PControl.PMaxRan - PControl.PMinRan);
        SPHScaled :=  100.0 * (ActSPH - PControl.PMinRan)/(PControl.PMaxRan - PControl.PMinRan);
        SPLScaled :=  100.0 * (ActSPL - PControl.PMinRan)/(PControl.PMaxRan - PControl.PMinRan);
        OutHScaled := 100.0 * (ActOutH - PControl.POutMinRan)/(PControl.POutMaxRan - PControl.POutMinRan);
        OutLScaled := 100.0 * (ActOutL - PControl.POutMinRan)/(PControl.POutMaxRan - PControl.POutMinRan);
        tracking_value_Scaled := 100.0 * (tracking_value - PControl.POutMinRan)/(PControl.POutMaxRan - PControl.POutMinRan);    
    END_IF;
    
    (*No Scaling*)
    IF (PControl.ScaMethod = 3) THEN
        SPScaled := ActSP;
        HMVScaled := MVFILTER.OUTV;
        SPHScaled := ActSPH;
        SPLScaled :=  ActSPL;
        OutHScaled := ActOutH;
        OutLScaled := ActOutL;
        tracking_value_Scaled := tracking_value;
    END_IF;
    
          
 (* PID *)
    
IF (PControl.RA) THEN
       dev := HMVSCaled - SPScaled; //action inverse 
ELSE
       dev := SPScaled - HMVScaled; //action directe
END_IF;
      
     // PID action only at its sampling time   
     IF PID_activation THEN 
        PID_activation:=false;
        PID (   ER:= dev,
                PV := HMVScaled,
                DISV := 0.0,
                GAIN := ActKc,
                TI := DINT_TO_TIME(REAL_TO_DINT(ActTi*1000.0)),
                TD := DINT_TO_TIME(REAL_TO_DINT(ActTd*1000.0)),
                TM_LAG := DINT_TO_TIME(REAL_TO_DINT(ActTds*1000.0)),
                PFDB_SEL := FALSE,
                DFDB_SEL := FALSE,
                DISV_SEL := TRUE,
                CYCLE := PControl.PIDCycle);
      END_IF;
   
       LMNGEN_C( LMN_HLM := OutHScaled,
                 LMN_LLM := OutLScaled,
                 LMNRC_ON := false,
                 MAN := tracking_value_Scaled,
                 MAN_ON := tracking_control,
                 PID_LMNG := PID.PID_LMNG, //liaison entre les 2 blocs
                 CYCLE := PID_EXEC_CYCLE );
              
    PID.LMNG_PID := LMNGEN_C.LMNG_PID;   //rebouclage des 2 blocs
       
    PID_Out := LMNGEN_C.LMN;    //sortie PID
    
    (* PID OUTPUT SCALING If Full Scaling*)
    IF(PControl.ScaMethod = 2) THEN
        OutOV := PControl.POutMinRan + PID_Out*(PControl.POutMaxRan  - PControl.POutMinRan)/100.0;    
    ELSE
        OutOV := PID_Out;
    END_IF;

        
 (* STATUS REGISTER(INS) AND (OUT) *) 
    IF ActKc<>DefKc AND Not(AuPPID.EKc) THEN     (* Current Kp Parameter different from Automatic *)
         KcDiDef:= TRUE;                (* Flag By Operator *)
     ELSE
         KcDiDef:= FALSE;
     END_IF;
     IF ActTi<>DefTi AND Not(AuPPID.ETi) THEN (* Current Ti Parameter different from Automatic *)
         TiDiDef:= TRUE;                (* Flag By Operator *)
     ELSE
         TiDiDef:= FALSE;
     END_IF;
     IF ActTd<>DefTd AND Not(AuPPID.ETd) THEN (* Current Td Parameter different from Automatic *)
         TdDiDef:= TRUE;                (* Flag By Operator *)
     ELSE
         TdDiDef:= FALSE;
     END_IF;
     IF ActTds<>DefTds AND Not(AuPPID.ETds) THEN (* Current Tds Parameter different from Automatic *)
         TdsDiDef:= TRUE;                (* Flag By Operator *)
     ELSE
        TdsDiDef:= FALSE;
     END_IF;
     
     IF ActOutH<>DefOutH AND Not(AuPPID.EOutH) THEN           (* Current AtOutH different from Automatic *)
         OutHDiDef:= TRUE;                (* Flag By Operator *)
     ELSE
         OutHDiDef:= FALSE;
     END_IF;
     IF ActOutL<>DefOutL AND Not(AuPPID.EOutL) THEN           (* Current AtOutH different from Automatic *)  // AtOutL<>AuPPID.lowLimit
         OutLDiDef:= TRUE;                (* Flag By Operator *)
     ELSE
         OutLDiDef:= FALSE;
     END_IF;
     
     IF ActSPH<>DefSPH AND Not(AuPPID.ESPH) THEN           (* Current AtSPoH different from Default *)
         SPHDiDef:= TRUE;                (* Flag By Operator *)
     ELSE
         SPHDiDef:= FALSE;
     END_IF;
     IF ActSPL<>DefSPL AND Not(AuPPID.ESPL) THEN           (* Current AtSPoL different from Default *)
         SPLDiDef:= TRUE;                (* Flag By Operator *)
     ELSE
         SPLDiDef:= FALSE;
     END_IF;
     
     MV     := MVFILTER.OUTV; 
     MSPSt  := MSP;
     AuSPSt := AuSPR;
     AuRegSt:= AuRegR;
     
     // RESET detection pulses   
     MAuMoR_old    := FALSE;
     MMMoR_old     := FALSE;
     MFoMoR_old    := FALSE;
     MRegR_old     := FALSE;
     MOutPR_old    := FALSE;
     MPRest_old    := FALSE;
     MPDefold      := FALSE;
     
     MNewPosR_old  := FALSE;
     MNewSPR_old   := FALSE;
     MNewSPHR_old  := FALSE;
     MNewSPLR_old  := FALSE;
     MNewOutHR_old := FALSE;
     MNewOutLR_old := FALSE;
     
     MNewKcR_old   := FALSE;
     MNewTdR_old   := FALSE;
     MNewTiR_old   := FALSE;   
     MNewTdsR_old  := FALSE;

     (* Status Register *)                
    
    Stsreg01b[8]  := SoftLDSt;                       //StsReg01 Bit 00
    Stsreg01b[9]  := AuActR;                         //StsReg01 Bit 01
    Stsreg01b[10] := AuMoSt;                         //StsReg01 Bit 02
    Stsreg01b[11] := MMoSt;                          //StsReg01 Bit 03
    Stsreg01b[12] := FoMoSt;                         //StsReg01 Bit 04
    Stsreg01b[13] := ArmRcpSt;                       //StsReg01 Bit 05
    Stsreg01b[14] := IOErrorW;                       //StsReg01 Bit 06
    Stsreg01b[15] := IOSimuW;                        //StsReg01 Bit 07
    Stsreg01b[0]  := AuRegR;                         //StsReg01 Bit 08
    Stsreg01b[1]  := RegSt;                          //StsReg01 Bit 09
    Stsreg01b[2]  := TrSt;                           //StsReg01 Bit 10
    Stsreg01b[3]  := OutPSt;                         //StsReg01 Bit 11
    Stsreg01b[4]  := AuIhSR;                         //StsReg01 Bit 12
    Stsreg01b[5]  := AuIhFoMo;                       //StsReg01 Bit 13
    Stsreg01b[6]  := AuESP;                          //StsReg01 Bit 14
    Stsreg01b[7]  := AuIhMMo;                        //StsReg01 Bit 15
  
    Stsreg02b[8]  := AuPPID.EKc;                     //StsReg02 Bit 00
    Stsreg02b[9]  := AuPPID.ETi;                     //StsReg02 Bit 01
    Stsreg02b[10] := AuPPID.ETd;                     //StsReg02 Bit 02
    Stsreg02b[11] := AuPPID.ETds;                    //StsReg02 Bit 03
    Stsreg02b[12] := KcDiDef;                        //StsReg02 Bit 04
    Stsreg02b[13] := TiDiDef;                        //StsReg02 Bit 05
    Stsreg02b[14] := TdDiDef;                        //StsReg02 Bit 06
    Stsreg02b[15] := TdsDiDef;                       //StsReg02 Bit 07
    stsreg02b[0]  := SPHDiDef;                       //StsReg02 Bit 08
    Stsreg02b[1]  := SPLDiDef;                       //StsReg02 Bit 09
    Stsreg02b[2]  := OutHDiDef;                      //StsReg02 Bit 10
    Stsreg02b[3]  := OutLDiDef;                      //StsReg02 Bit 11
    Stsreg02b[4]  := AuPPID.ESPH;                    //StsReg02 Bit 12
    Stsreg02b[5]  := AuPPID.ESPL;                    //StsReg02 Bit 13
    Stsreg02b[6]  := AuPPID.EOutH;                   //StsReg02 Bit 14
    Stsreg02b[7]  := AuPPID.EOutL;                   //StsReg02 Bit 15
                           
END_FUNCTION_BLOCK                   





