//UNICOS
//� Copyright CERN 2013 all rights reserved

(* DIGITAL PARAMETER OBJECT DATA STRUCTURE *****************)

TYPE CPC_DB_DPAR
TITLE ='CPC_DB_DPAR'
AUTHOR: 'EN/ICE'
VERSION: 6.0

STRUCT
 
   PosSt:          BOOL;
   ArmRcp_old:     BOOL;
   ActRcp_old:     BOOL;
   MOnR_old:       BOOL;
   MOffR_old:      BOOL;
   ArmRcpSt:       BOOL;
   MOnRSt:         BOOL;
   MOffRSt:        BOOL;
   
END_STRUCT;
END_TYPE

(* DIGITAL PARAMETER OBJECT FUNCTION BLOCK *****************)

FUNCTION_BLOCK CPC_FB_DPAR
TITLE = 'CPC_FB_DPAR'
//
//  DPAR: DIGITAL PARAMETER
//
VERSION: '6.0'
AUTHOR: 'EN/ICE'
NAME: 'OBJECT'
FAMILY: 'INTERF'


VAR_INPUT               
    
    Manreg01:                  WORD;
    Manreg01b AT Manreg01:     ARRAY [0..15] OF BOOL;
    
END_VAR

VAR_IN_OUT


    Perst:                     CPC_DB_DPAR;
    StsReg01:                  WORD;
 
END_VAR
    
VAR_TEMP
   
    E_ArmRcp:                   BOOL;
    E_ActRcp:                   BOOL;
    E_MNewMR:                   BOOL;
    E_MOnR:                     BOOL;
    E_MOffR:                    BOOL;
    TempStsReg01:               WORD;
    StsReg01b AT TempStsReg01:  ARRAY [0..15] OF BOOL;

END_VAR

BEGIN

(* tranfer StsReg01 to a temporary variable to be able to treat bits individually.
Not possible if the variable is declared as IN_OUT variable*)

TempStsReg01 := StsReg01;

(* INPUT MANAGER *)

E_ArmRcp  := R_EDGE(new:=ManReg01b[10],old:=Perst.ArmRcp_old);                      (* ArmRcp Arm Recipe *)
E_ActRcp  := R_EDGE(new:=ManReg01b[11],old:=Perst.ActRcp_old);                      (* ActRcp Activate Recipe *)
E_MOnR    := R_EDGE(new:=ManReg01b[12],old:=Perst.MOnR_old);                        (* Manual On Request *)
E_MOffR   := R_EDGE(new:=ManReg01b[13],old:=Perst.MOffR_old);                       (* Manual Off Request *)

(* ARM RECIPE *)
IF E_ArmRcp THEN 
   Perst.ArmRcpSt := TRUE;
END_IF;

(* CANCEL RECIPE *)
IF E_ArmRcp AND E_ActRcp THEN
   Perst.ArmRcpSt := FALSE ;
END_IF;

(* OUTPUT MANAGER *)
(* ON REQUEST *) 

IF (E_MOnR AND NOT Perst.ArmRcpSt) OR 
   (E_MOnR AND Perst.ArmRcpSt AND E_ActRcp)
THEN Perst.PosSt := TRUE;
END_IF;

(* OFF REQUEST *)

IF (E_MOffR AND NOT Perst.ArmRcpSt) OR 
   (E_MOffR AND Perst.ArmRcpSt AND E_ActRcp)
THEN Perst.PosSt := FALSE;
END_IF;

 (* Manual On/Off Request status*) 
       
IF E_MOffR  THEN Perst.MOnRSt :=   FALSE; 
   ELSE IF 
       E_MOnR THEN Perst.MOnRSt :=  TRUE;
   END_IF;
END_IF;
Perst.MOffRSt:= NOT(Perst.MOnRSt);    


(* RESET RECIPE *)

IF (Perst.ArmRcpSt AND E_ActRcp) THEN Perst.ArmRcpSt := FALSE;
END_IF;


(* STATUS REGISTER  *)   
       
    StsReg01b[8]  := Perst.PosSt;         //StsReg01 Bit 00
    StsReg01b[9]  := 0;                   //StsReg01 Bit 01
    StsReg01b[10] := 0;                   //StsReg01 Bit 02
    StsReg01b[11] := Perst.ArmRcpSt;      //StsReg01 Bit 03
    StsReg01b[12] := Perst.MOnRSt;        //StsReg01 Bit 04
    StsReg01b[13] := Perst.MOffRSt;       //StsReg01 Bit 05
    StsReg01b[14] := 0;                   //StsReg01 Bit 06
    StsReg01b[15] := 0;                   //StsReg01 Bit 07
    StsReg01b[0]  := 0;                   //StsReg01 Bit 08
    StsReg01b[1]  := 0;                   //StsReg01 Bit 09
    StsReg01b[2]  := 0;                   //StsReg01 Bit 10
    StsReg01b[3]  := 0;                   //StsReg01 Bit 11
    StsReg01b[4]  := 0;                   //StsReg01 Bit 12
    StsReg01b[5]  := 0;                   //StsReg01 Bit 13
    StsReg01b[6]  := 0;                   //StsReg01 Bit 14
    StsReg01b[7]  := 0;                   //StsReg01 Bit 15
    
(* Values back to temporary variables*)

StsReg01 := TempStsReg01;
                     
END_FUNCTION_BLOCK