(*
This file contains :
   - starting OB (100,102)
   - Errors: 81, 83, 84 (only S7-400)
   - Diagnostic functions : FC_Diag1&2
   - Diagnostic OB (82)
   - OB1(main cycle)
   - OB35 (sampling PIDs)
   - OB32 (UNICOS live counter)
  - All other errors treatment are included in the baseline
*)
(*RESTART (WARM RESTART)*********************************************) 
ORGANIZATION_BLOCK OB100 
TITLE = 'Warm restart'
//
// Called during warm restart
//
AUTHOR: 'UNICOS'
NAME: 'OB100'
FAMILY: 'EXEC'
VAR_TEMP
    OB100_EV_CLASS  : BYTE;
    OB100_STRUP     : BYTE;
    OB100_PRIORITY  : BYTE;
    OB100_OB_NUMBR  : BYTE;
    OB100_RESERVED_1: BYTE;
    OB100_RESERVED_2: BYTE;
    OB100_STOP      : WORD;
    OB100_STRT_INFO : DWORD;
    OB100_DATE_TIME   : DATE_AND_TIME;
END_VAR
BEGIN 
//Time Smoothing (alarms)
    UNICOS_TimeSmooth := 10;

//Init of First_Cycle variable	
	First_Cycle := TRUE;

//Init of Live counter marker form Live counter datablock	
	UNICOS_LiveCounter := DB_WINCCOA.UNICOS_LiveCounter;	
	

END_ORGANIZATION_BLOCK
(*DB WHICH CONTAINS THE RESULT OF THE DIAGNOSTIC FOR A PROBLEM ON A CHANNEL******************************)
DATA_BLOCK DB_DIAGNOSTIC
TITLE = Diagnostic
//
// Contains data of IO Error diagnostic
//
AUTHOR: UNICOS
NAME: Diagnose
FAMILY: DIAG
STRUCT
    
    Call_Diag1  : BOOL;
    Call_Diag2  : BOOL;
    IOFlag      : BYTE; // = B#16#54 for an Input and B#16#55 for an Output
    MDL_ADDR    : WORD; //Address of the module
    
    Data        : ARRAY [0..15] OF BYTE; //Data sent by SFC59 for the diagnostic
    ERR         : INT;  //SFC59 error
    Busy        : BOOL; //SFC 59 Busy flag
    
    
  END_STRUCT
BEGIN
END_DATA_BLOCK

(*DIAGNOSTIC FUNCTION WITH I/O CARDS NOT GOUPED FOR THE DIAGNOSTIC : 1 Diagnostic by channel******************************)
(*YOU HAVE TO ACTIVATE DIAGNOSTIC FUNCTIONS OF I/O CARDS*)
FUNCTION FC_Diag1: VOID 
//
// DIAGNOSTIC FUNCTION WITH I/O CARDS NOT GOUPED FOR THE DIAGNOSTIC : 1 Diagnostic by channel
//
AUTHOR: UNICOS
NAME: Diagnose
FAMILY: DIAG
VAR_TEMP
    return_value    : INT;
    test_busy       : BOOL;
    Data            : ARRAY[0..15] OF BYTE;
    PLC_ADDR        : REAL;
    T               : INT;  //type of card (DI=1 AI=2 DO=3 AO=4)
    Chan_Error      : BOOL;
    i               : INT;    
    j               : INT; 
    mask            : BYTE;  
    break           : BOOL;
    Var_req         : BOOL;
    
END_VAR
BEGIN

	//CALL SFC59(RD_REC) : module diagnostic informations
      Var_Req := 1;
      
      REPEAT
      return_value := RD_REC( 
                            REQ     := Var_Req,
                            IOID    := DB_DIAGNOSTIC.IOFlag,
                            LADDR   := DB_DIAGNOSTIC.MDL_ADDR,
                            RECNUM  := B#16#1,
                            BUSY    := test_busy,
                            RECORD  := Data
                            ); 
                            
                            Var_Req := 0;
                            
                            DB_DIAGNOSTIC.Busy := test_busy;
                            DB_DIAGNOSTIC.Data := Data;
                            DB_DIAGNOSTIC.ERR := return_Value;
       
      UNTIL test_busy = 0
      END_REPEAT;
    //Type of chan card                        
    IF (B#16#7F AND Data[4]) = B#16#70 THEN T:= 1; END_IF; //DI
    IF (B#16#7F AND Data[4]) = B#16#71 THEN T:= 2; END_IF; //AI
    IF (B#16#7F AND Data[4]) = B#16#72 THEN T:= 3; END_IF; //DO
    IF (B#16#7F AND Data[4]) = B#16#73 THEN T:= 4; END_IF; //AO
    
            
 (*  Which channel is in default?
     Calculation OF the PLC address according to the type   
     The number of chan is in the Byte 6 of Data  
     ATTENTION : INDIVIDUAL DIAGNOSTIC BY CHANNEL HERE
 
*)
//MORE THAN 0 CHAN : LOOK CHAN 0->7 in Data[7]
IF BYTE_TO_INT(Data[6]) > 0 THEN
    FOR i := 0 TO 7 DO
       (*init*)
        break := 0;
        mask := B#16#01;
        mask := ROL(IN:=mask, N:=i);
       
       (*Calculation of PLC Adress  according to the type of channel*)
        CASE T OF    
        1 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR)) + INT_TO_REAL(i)/10 ; //Digital INPUT 
        2 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR)) + i*2;   //Analog INPUT 
        3 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR)) + INT_TO_REAL(i)/10 ; //Digital OUTPUT 
        4 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR)) + i*2;    //Analog OUTPUT 
        
        END_CASE;
                       
        (*Detecting if error*)
            IF (Data[7] AND mask) = mask THEN 
                Chan_Error := 1 ;       //error on chan i
            ELSE 
                Chan_Error:= 0  ;       //no error on chan i
            END_IF;
         
         (*Find UNICOS object and set Error at 1 or 0*)
            j:= 1;
         
         CASE T OF //Affect Error according to the type of channel
 2 : WHILE (j<35+1 AND break = 0) DO  //AI CHANNEL 
                    IF AI_ERROR.IOERROR[j].ADDR = PLC_ADDR THEN AI_ERROR.IOERROR[j].Err := Chan_Error;break := 1;END_IF;
                        j := j+1;
            END_WHILE;
 3 : WHILE (j<4+1 AND break = 0) DO //DO CHANNEL 
                        IF DO_ERROR.IOERROR[j].ADDR = PLC_ADDR THEN DO_ERROR.IOERROR[j].Err := Chan_Error;break := 1;END_IF;
                        j := j+1;
            END_WHILE;

      END_CASE;
    END_FOR;
END_IF;
//MORE THAN 8 CHAN : LOOK CHAN 8->15 in Data[8]
IF BYTE_TO_INT(Data[6]) > 8 THEN
    FOR i := 0 TO 7 DO
       (*init*)
        break := 0;
        mask := B#16#01;
        mask := ROL(IN:=mask, N:=i);
       
       (*Calculation of PLC Adress  according to the type of channel*)
        CASE T OF    
        1 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ 1 + INT_TO_REAL(i)/10 ; //Digital INPUT 
        2 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ 16 + i*2;   //Analog INPUT 
        3 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ 1 + INT_TO_REAL(i)/10 ; //Digital OUTPUT 
        4 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ 16 + i*2;    //Analog OUTPUT 
        
        END_CASE;
                       
        (*Detecting if error*)
            IF (Data[8] AND mask) = mask THEN 
                Chan_Error := 1 ;       //error on chan i
            ELSE 
                Chan_Error:= 0  ;       //no error on chan i
            END_IF;
         
         (*Find UNICOS object and set Error at 1 or 0*)
            j:= 1;
         
CASE T OF //Affect Error according to the type of channel
 2 : WHILE (j<35+1 AND break = 0) DO  //AI CHANNEL 
                    IF AI_ERROR.IOERROR[j].ADDR = PLC_ADDR THEN AI_ERROR.IOERROR[j].Err := Chan_Error;break := 1;END_IF;
                        j := j+1;
            END_WHILE;
 3 : WHILE (j<4+1 AND break = 0) DO //DO CHANNEL 
                        IF DO_ERROR.IOERROR[j].ADDR = PLC_ADDR THEN DO_ERROR.IOERROR[j].Err := Chan_Error;break := 1;END_IF;
                        j := j+1;
            END_WHILE;

		END_CASE;
    END_FOR;
END_IF;
//MORE THAN 16 CHAN : LOOK CHAN 16->23 in Data[9]
IF BYTE_TO_INT(Data[6]) > 16 THEN
    FOR i := 0 TO 7 DO
       (*init*)
        break := 0;
        mask := B#16#01;
        mask := ROL(IN:=mask, N:=i);
       
       (*Calculation of PLC Adress  according to the type of channel*)
        CASE T OF    
        1 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ 2 + INT_TO_REAL(i)/10 ; //Digital INPUT 
        2 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ 32 + i*2;   //Analog INPUT 
        3 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ 2 + INT_TO_REAL(i)/10 ; //Digital OUTPUT 
        4 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ 32 + i*2;    //Analog OUTPUT 
        
        END_CASE;
                       
        (*Detecting if error*)
            IF (Data[9] AND mask) = mask THEN 
                Chan_Error := 1 ;       //error on chan i
            ELSE 
                Chan_Error:= 0  ;       //no error on chan i
            END_IF;
         
         (*Find UNICOS object and set Error at 1 or 0*)
            j:= 1;
         
CASE T OF //Affect Error according to the type of channel
 2 : WHILE (j<35+1 AND break = 0) DO  //AI CHANNEL 
                    IF AI_ERROR.IOERROR[j].ADDR = PLC_ADDR THEN AI_ERROR.IOERROR[j].Err := Chan_Error;break := 1;END_IF;
                        j := j+1;
            END_WHILE;
 3 : WHILE (j<4+1 AND break = 0) DO //DO CHANNEL 
                        IF DO_ERROR.IOERROR[j].ADDR = PLC_ADDR THEN DO_ERROR.IOERROR[j].Err := Chan_Error;break := 1;END_IF;
                        j := j+1;
            END_WHILE;

		END_CASE;
    END_FOR;
END_IF;
//MORE THAN 24 CHAN : LOOK CHAN 24->31 in Data[10]
IF BYTE_TO_INT(Data[6]) > 24 THEN
    FOR i := 0 TO 7 DO
       (*init*)
        break := 0;
        mask := B#16#01;
        mask := ROL(IN:=mask, N:=i);
       
       (*Calculation of PLC Adress  according to the type of channel*)
        CASE T OF    
        1 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ 3 + INT_TO_REAL(i)/10 ; //Digital INPUT 
        2 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ 48 + i*2;   //Analog INPUT 
        3 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ 3 + INT_TO_REAL(i)/10 ; //Digital OUTPUT 
        4 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ 48 + i*2;    //Analog OUTPUT 
        
        END_CASE;
                       
        (*Detecting if error*)
            IF (Data[10] AND mask) = mask THEN 
                Chan_Error := 1 ;       //error on chan i
            ELSE 
                Chan_Error:= 0  ;       //no error on chan i
            END_IF;
         
         (*Find UNICOS object and set Error at 1 or 0*)
            j:= 1;
         
         CASE T OF //Affect Error according to the type of channel
 2 : WHILE (j<35+1 AND break = 0) DO  //AI CHANNEL 
                    IF AI_ERROR.IOERROR[j].ADDR = PLC_ADDR THEN AI_ERROR.IOERROR[j].Err := Chan_Error;break := 1;END_IF;
                        j := j+1;
            END_WHILE;
 3 : WHILE (j<4+1 AND break = 0) DO //DO CHANNEL 
                        IF DO_ERROR.IOERROR[j].ADDR = PLC_ADDR THEN DO_ERROR.IOERROR[j].Err := Chan_Error;break := 1;END_IF;
                        j := j+1;
            END_WHILE;

      END_CASE;
    END_FOR;
END_IF;
DB_DIAGNOSTIC.Call_diag1 := 0;

END_FUNCTION
(*DIAGNOSTIC FUNCTION  WITH I/O CARDS GOUPED BY 2 FOR THE DIAGNOSTIC******************************)
(*YOU HAVE TO ACTIVATE DIAGNOSTIC FUNCTIONS OF I/O CARDS*)
FUNCTION FC_Diag2 : VOID
//
// DIAGNOSTIC FUNCTION  WITH I/O CARDS GOUPED BY 2 FOR THE DIAGNOSTIC
//
AUTHOR: UNICOS
NAME: Diagnose
FAMILY: DIAG
VAR_TEMP
    return_value    : INT;
    test_busy       : BOOL;
    Data            : ARRAY[0..15] OF BYTE;
    PLC_ADDR        : REAL;
    PLC_ADDR2        : REAL;
    T               : INT;  //type of card (DI=1 AI=2 DO=3 AO=4)
    Chan_Error      : BOOL;
    i               : INT;    
    j               : INT; 
    mask            : BYTE;  
    break1           : BOOL;
    break2           : BOOL;
    Var_req         : BOOL;
    
END_VAR
BEGIN

  //CALL SFC59(RD_REC) : module diagnostic informations
      Var_Req := 1;
      
      REPEAT
      return_value := RD_REC( 
                            REQ     := Var_Req,
                            IOID    := DB_DIAGNOSTIC.IOFlag,
                            LADDR   := DB_DIAGNOSTIC.MDL_ADDR,
                            RECNUM  := B#16#1,
                            BUSY    := test_busy,
                            RECORD  := Data
                            ); 
                            
                            Var_Req := 0;
                            
                            DB_DIAGNOSTIC.Busy := test_busy;
                            DB_DIAGNOSTIC.Data := Data;
                            DB_DIAGNOSTIC.ERR := return_Value;
       
      UNTIL test_busy = 0
      END_REPEAT;
    //Type of chan card                        
    IF (B#16#7F AND Data[4]) = B#16#70 THEN T:= 1; END_IF; //DI
    IF (B#16#7F AND Data[4]) = B#16#71 THEN T:= 2; END_IF; //AI
    IF (B#16#7F AND Data[4]) = B#16#72 THEN T:= 3; END_IF; //DO
    IF (B#16#7F AND Data[4]) = B#16#73 THEN T:= 4; END_IF; //AO
    
            
 (*  Which channel is in default?
     Calculation OF the PLC address according to the type   
     The number of chan is in the Byte 6 of Data  
     ATTENTION : CHANNELS ARE GROUPED BY 2 
 
*)
    FOR i := 0 TO 7 DO
       (*init*)
        break1 := 0;
        break2 := 0;
        mask := B#16#01;
        mask := ROL(IN:=mask, N:=i);
       
       (*Calculation of the 2 PLC Address according to the type of channel*)
        CASE T OF    
        1 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR)) + INT_TO_REAL((i*2) MOD 8)/10.0 +  INT_TO_REAL((i*2) DIV 8); //Digital INPUT first channel of group
            PLC_ADDR2 := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR)) + INT_TO_REAL((i*2) MOD 8 + 1)/10.0 +  INT_TO_REAL((i*2) DIV 8); //Digital INPUT second channel OF group
        2 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ i*4;   //Analog INPUT first channel of group
            PLC_ADDR2 := PLC_ADDR + 2; //Analog INPUT second channel OF group
        3 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR)) + INT_TO_REAL((i*2) MOD 8)/10.0 +  INT_TO_REAL((i*2) DIV 8); //Digital OUTPUT first channel of group
            PLC_ADDR2 :=INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR)) + INT_TO_REAL((i*2) MOD 8 + 1)/10.0 +  INT_TO_REAL((i*2) DIV 8); //Digital OUTPUT second channel OF group
        4 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(DB_DIAGNOSTIC.MDL_ADDR))+ i*4;    //Analog OUTPUT first channel of group
            PLC_ADDR2 := PLC_ADDR + 2; //Analog OUTPUT second channel OF group
       
        END_CASE;
                       
        (*Detecting if error*)
            IF (Data[7] AND mask) = mask THEN 
                Chan_Error := 1 ;       //error on chan i
            ELSE 
                Chan_Error:= 0  ;       //no error on chan i
            END_IF;
         
         (*Find UNICOS object and set Error at 1 or 0*)
            j:= 1;
         
         CASE T OF //Affect Error according to the type of channel
 2 : WHILE (j<35+1 AND ( break1 = 0 OR break2 = 0 )) DO  //DI CHANNEL  
                    IF AI_ERROR.IOERROR[j].ADDR = PLC_ADDR THEN AI_ERROR.IOERROR[j].Err := Chan_Error; Break1 := 1; END_IF;
                    IF AI_ERROR.IOERROR[j].ADDR = PLC_ADDR2 THEN AI_ERROR.IOERROR[j].Err := Chan_Error; Break2 := 1; END_IF;
                          j := j+1;
        END_WHILE;
 3 : WHILE (j<4+1 AND ( break1 = 0 OR break2 = 0 )) DO  //DI CHANNEL  
                    IF DO_ERROR.IOERROR[j].ADDR = PLC_ADDR THEN DO_ERROR.IOERROR[j].Err := Chan_Error; Break1 := 1; END_IF;
                    IF DO_ERROR.IOERROR[j].ADDR = PLC_ADDR2 THEN DO_ERROR.IOERROR[j].Err := Chan_Error; Break2 := 1; END_IF;
                          j := j+1;
        END_WHILE;
      END_CASE;
    END_FOR;
DB_DIAGNOSTIC.Call_diag2 := 0;

END_FUNCTION

(*Diagnostic OB for detect IOError**********************************************) 
ORGANIZATION_BLOCK OB82
TITLE = 'OB82'
//
// Called during an error on a channel if the diagnostic option is activated
//
AUTHOR: 'ICE/PLC'
NAME: 'OB82'
FAMILY: 'UNICOS'
VAR_TEMP
    OB82_EV_CLASS       : BYTE;
    OB82_FLT_ID         : BYTE;
    OB82_PRIORITY       : BYTE;
    OB82_OB_NUMBR       : BYTE;
    OB82_RESERVED_1     : BYTE;
    OB82_IO_FLAG        : BYTE;
    OB82_MDL_ADDR       : WORD;
    OB82_MDL_DEFECT     : BOOL;
    OB82_INT_FAULT      : BOOL;
    OB82_EXT_FAULT      : BOOL;
    OB82_PNT_INFO       : BOOL;
    OB82_EXT_VOLTAGE    : BOOL;
    OB82_FLD_CONNCTR    : BOOL;
    OB82_NO_CONFIG      : BOOL;
    OB82_CONFIG_ERR     : BOOL;
    OB82_MDL_TYPE       : BYTE;
    OB82_SUB_MDL_ERR    : BOOL;
    OB82_COMM_FAULT     : BOOL;
    OB82_MDL_STOP       : BOOL;
    OB82_WTCH_DOG_FLT   : BOOL;
    OB82_INT_PS_FLT     : BOOL;
    OB82_PRIM_BATT_FLT  : BOOL;
    OB82_BCKUP_BATT_FLT : BOOL;
    OB82_RESERVED_2     : BOOL;
    OB82_RACK_FLT       : BOOL;
    OB82_PROC_FLT       : BOOL;
    OB82_EPROM_FLT      : BOOL;
    OB82_RAM_FLT        : BOOL;
    OB82_ADU_FLT        : BOOL;
    OB82_FUSE_FLT       : BOOL;
    OB82_HW_INTR_FLT    : BOOL;
    OB82_RESERVED_3     : BOOL;
    OB82_DATE_TIME      : DATE_AND_TIME;    
    return_value    : INT;
    test_busy       : BOOL;
    Data            : ARRAY[0..15] OF BYTE;
    PLC_ADDR        : REAL;
    T               : INT;  //type of card (DI=1 AI=2 DO=3 AO=4)
    Chan_Error      : BOOL;
    i               : INT;    
    j               : INT; 
    mask            : BYTE;  
    break           : BOOL;
END_VAR
BEGIN
 DB_DIAGNOSTIC.IOFlag    := OB82_IO_FLAG;
    DB_DIAGNOSTIC.MDL_ADDR   := OB82_MDL_ADDR;
    
    
    (* You can Change this part according to your IO cards.
        Select interval of inputs and of outputs where the diagnostic is :
         - set on every channel : Call_Diag1 := TRUE
         - set every 2 channels, they are grouped by 2 for diagnostic : Call_Diag2 := TRUE;
     *)
       
    
   // INPUTS
    IF OB82_IO_FLAG = B#16#54 THEN
          FC_Diag1(); // FC_Diag2();  
    END_IF;
        
   //ALL OUTPUTS CARD   
   IF OB82_IO_FLAG = B#16#55 THEN
          FC_Diag1();   
   END_IF;
;
END_ORGANIZATION_BLOCK
(*OB1***************************************************************************)
ORGANIZATION_BLOCK OB1 
TITLE = 'OB1'
//
// Called during each cycle
//
AUTHOR: 'ICE/PLC'
NAME: 'OB1'
FAMILY: 'UNICOS'
VAR_TEMP
    OB1_EV_CLASS : BYTE;
    OB1_SCAN_1 : BYTE;
    OB1_PRIORITY : BYTE;
    OB1_OB_NUMBR : BYTE;
    OB1_RESERVED_1 : BYTE;
    OB1_RESERVED_2 : BYTE;
    OB1_PREV_CYCLE : INT;
    OB1_MIN_CYCLE : INT;
    OB1_MAX_CYCLE : INT;
    OB1_DATE_TIME : DATE_AND_TIME;
    
END_VAR
BEGIN 
     "T_CYCLE" := DINT_TO_TIME(INT_TO_DINT(OB1_PREV_CYCLE)); //getting OB1 sampling time
     
    (*Getting inputs*)
    FC_AI();    // Analog inputs
    FC_DPAR();    // DigitalParameter objects
    FC_APAR();    // AnalogParameter objects
    FC_WS();    // WordStatus objects

    // Accelerating communications in large applications (comms without events)
    IF Comm = 1 AND OB1_PREV_CYCLE > 250 THEN 
        FC_TSPP(NE :=false,RA := false);
    END_IF;

	(*Process Logic Control *)
    FC_PCO_LOGIC();       // Process Control object logic
	
    (*UNICOS objects calculating*)
    FC_ONOFF();    // OnOff objects
    FC_DA();   // DIGITAL ALARM objects
    FC_AA();   // ANALOG ALARM objects

    (*Setting Outputs*)
    FC_DO();    //Digital Outputs 

    // Calling the Recipes Mechanism and passing the information from WinCCOA (DB_RECIPES_INTERFACE) to the CPC_DB_RECIPES
	CPC_FB_RECIPES.CPC_DB_RECIPES(
		Header := DB_RECIPES_INTERFACE.RecipeHeader	  //Header from DB_RECIPES_INTERFACE to CPC_DB_RECIPES
		,DBnum := 116                  //DB number of Recipe buffers
		,HeaderBase := 2                   //Header buffer base address
		,StatusBase := 42                   //Recipe Status buffer base address
		,ManRegAddrBase := 82           //Manual Requests addresses buffer base address
		,ManRegValBase := 2082             //Manual Reuqests values buffer base address
		,ReqAddrBase := 4082                 //Request Address buffer base address
		,ReqValBase := 6082                   //Request Values buffer base address
		,BuffersBase := 82                 //Buffers base address
		,BuffersEnd := 10082                   //Buffers last address
		,Timeout := T#100s                         //Recipe transfer timeout
	);
	
	DB_RECIPES_INTERFACE.RecipeStatus := CPC_DB_RECIPES.Status; //Status from CPC_DB_RECIPES to DB_RECIPES_INTERFACE
	
     
     //Allow calling TS_Event_Manager for TSPP communication with the SCADA after the 1st cycle
     IF Comm = 0 THEN Comm := 1; 
     ELSE FC_Event(); 
     END_IF;  
	 First_Cycle:=FALSE; //First cycle terminated
	 

// 1 second counter to simulate OB32 in a S7-300
UNICOS_Counter1:=UNICOS_Counter1+OB1_PREV_CYCLE;
IF UNICOS_Counter1 > 1000 THEN
    UNICOS_Counter1:= UNICOS_Counter1 -1000;
	UNICOS_LiveCounter:=UNICOS_LiveCounter+1;
END_IF;

END_ORGANIZATION_BLOCK
// Scheduling:  No controller defintion 
