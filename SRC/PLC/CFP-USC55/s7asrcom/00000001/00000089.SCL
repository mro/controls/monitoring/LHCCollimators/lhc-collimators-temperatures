//UNICOS
//� Copyright CERN 2013 all rights reserved

(* ANALOG PARAMETER OBJECT DATA STRUCTURE **************************)

TYPE CPC_DB_APAR
TITLE= 'CPC_DB_APAR'
AUTHOR: 'EN/ICE'
VERSION: 6.0

STRUCT
   
   PosSt:          REAL;  
   MPosRSt:        REAL;
   ArmRcp_old:     BOOL;
   ActRcp_old:     BOOL;
   MNewMR_old:     BOOL;
   ArmRcpSt:       BOOL;

END_STRUCT;
END_TYPE

(* ANALOG PARAMETER OBJECT FUNCTION BLOCK **************************)

FUNCTION_BLOCK CPC_FB_APAR
TITLE = 'CPC_FB_APAR'
//
//  APAR: ANALOG PARAMETER
//
VERSION: '6.0'
AUTHOR: 'EN/ICE'
NAME: 'OBJECT'
FAMILY: 'INTERF'


VAR_INPUT               

    Manreg01:               WORD;
    Manreg01b AT Manreg01:  ARRAY [0..15] OF BOOL;
    MPosR:                  REAL; 
    
END_VAR

VAR_IN_OUT

    StsReg01:               WORD;
    Perst:                  CPC_DB_APAR;
    
END_VAR

VAR_TEMP
   
    E_ArmRcp:               BOOL;
    E_ActRcp:               BOOL;
    E_MNewMR:               BOOL;
    TempStsReg01:           WORD;
    StsReg01b AT TempStsReg01:  ARRAY [0..15] OF BOOL;   
    
END_VAR


BEGIN

(* tranfer StsReg01 to a temporary variable to be able to treat bits individually.
Not possible if the variable is declared as IN_OUT variable*)

TempStsReg01 := StsReg01;

(* INPUT MANAGER *)

E_ArmRcp  := R_EDGE(new:=ManReg01b[10],old:=Perst.ArmRcp_old);                      (* ArmRcp Arm Recipe *)
E_ActRcp  := R_EDGE(new:=ManReg01b[11],old:=Perst.ActRcp_old);                      (* ActRcp Activate Recipe *)
E_MNewMR  := R_EDGE(new:=ManReg01b[14],old:=Perst.MNewMR_old);                      (* MNewMR Manual New Manual Request *)


(* ARM RECIPE *)
IF E_ArmRcp THEN 
   Perst.ArmRcpSt := TRUE;
END_IF;

(* CANCEL RECIPE *)
IF E_ArmRcp AND E_ActRcp THEN
   Perst.ArmRcpSt := FALSE ;
END_IF;

(* OUTPUT MANAGER *)

Perst.MPosRSt := MPosR;

IF (E_MNewMR AND NOT Perst.ArmRcpSt) OR 
   (E_MNewMR AND Perst.ArmRcpSt AND E_ActRcp)
THEN Perst.PosSt := MPosR;
END_IF;

(* RESET RECIPE *)

IF (Perst.ArmRcpSt AND E_ActRcp) THEN Perst.ArmRcpSt := FALSE;
END_IF;

(* STATUS REGISTER *)   
       
    StsReg01b[8]  := 0;                   //StsReg01 Bit 00
    StsReg01b[9]  := 0;                   //StsReg01 Bit 01
    StsReg01b[10] := 0;                   //StsReg01 Bit 02
    StsReg01b[11] := Perst.ArmRcpSt ;     //StsReg01 Bit 03
    StsReg01b[12] := 0;                   //StsReg01 Bit 04
    StsReg01b[13] := 0;                   //StsReg01 Bit 05
    StsReg01b[14] := 0;                   //StsReg01 Bit 06
    StsReg01b[15] := 0;                   //StsReg01 Bit 07
    StsReg01b[0]  := 0;                   //StsReg01 Bit 08
    StsReg01b[1]  := 0;                   //StsReg01 Bit 09
    StsReg01b[2]  := 0;                   //StsReg01 Bit 10
    StsReg01b[3]  := 0;                   //StsReg01 Bit 11
    StsReg01b[4]  := 0;                   //StsReg01 Bit 12
    StsReg01b[5]  := 0;                   //StsReg01 Bit 13
    StsReg01b[6]  := 0;                   //StsReg01 Bit 14
    StsReg01b[7]  := 0;                   //StsReg01 Bit 15

(* Values back to temporary variables*)

StsReg01 := TempStsReg01;
            
END_FUNCTION_BLOCK