//Specs version used for this generation: 1.0

//Digital Parameter DB Creation file: UNICOS application

DATA_BLOCK DB_DPAR CPC_FB_DPAR
BEGIN
END_DATA_BLOCK

TYPE DPAR_ManRequest
TITLE = DPAR_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
ManReg01 :WORD;
END_STRUCT
END_TYPE



TYPE DPAR_bin_Status
TITLE = DPAR_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
StsReg01 :WORD;
END_STRUCT
END_TYPE



TYPE DPAR_event
TITLE = DPAR_event
//
// parameters of ANADIG Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
evStsReg01 : DWORD;
END_STRUCT
END_TYPE


			

// DB_Event_DPAR Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_DPAR
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type DPAR
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
	Nb_DPAR : INT := 6;
	DPAR_evstsreg : ARRAY [1..6] OF DPAR_event;
END_STRUCT;
BEGIN
END_DATA_BLOCK


// DB_DPAR_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_DPAR_ManRequest
TITLE = DB_DPAR_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
    DPAR_Requests : ARRAY [1..6] OF DPAR_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK


(************************* binary Status of the DPARs ************************)
DATA_BLOCK DB_bin_status_DPAR
TITLE = 'DB_bin_status_DPAR'
//
// Global binary status DB of DPAR
//
// List of variables:
//  [1]    BIC_TZ76_3_B1_PermitA_T      (DPAR_bin_Status)

//  [2]    BIC_TZ76_3_B1_PermitB_T      (DPAR_bin_Status)

//  [3]    BIC_TZ76_3_B2_PermitA_T      (DPAR_bin_Status)

//  [4]    BIC_TZ76_3_B2_PermitB_T      (DPAR_bin_Status)

//  [5]    BIC_TZ76_3_B1_Permit_TM      (DPAR_bin_Status)

//  [6]    BIC_TZ76_3_B2_Permit_TM      (DPAR_bin_Status)

AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..6] OF DPAR_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(********************* old binary Status of the DPARs ****************************)
DATA_BLOCK DB_bin_status_DPAR_old
TITLE = 'DB_bin_status_DPAR_old'
//
// Old Global binary status DB of DPAR
//

//  [1]    BIC_TZ76_3_B1_PermitA_T      (DPAR_bin_Status)

//  [2]    BIC_TZ76_3_B1_PermitB_T      (DPAR_bin_Status)

//  [3]    BIC_TZ76_3_B2_PermitA_T      (DPAR_bin_Status)

//  [4]    BIC_TZ76_3_B2_PermitB_T      (DPAR_bin_Status)

//  [5]    BIC_TZ76_3_B1_Permit_TM      (DPAR_bin_Status)

//  [6]    BIC_TZ76_3_B2_Permit_TM      (DPAR_bin_Status)

AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..6] OF DPAR_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK

  
(*DPAR execution ********************************************)
FUNCTION_BLOCK FB_DPAR_all
TITLE = 'FB_DPAR_all'
//
// Call the DPAR treatment
//
AUTHOR: 'UNICOS'
NAME: 'CallDPAR'
FAMILY: 'DPAR'
VAR
  // Static Variables
  DPAR_SET: STRUCT


   BIC_TZ76_3_B1_PermitA_T       : CPC_DB_DPAR;

   BIC_TZ76_3_B1_PermitB_T       : CPC_DB_DPAR;

   BIC_TZ76_3_B2_PermitA_T       : CPC_DB_DPAR;

   BIC_TZ76_3_B2_PermitB_T       : CPC_DB_DPAR;

   BIC_TZ76_3_B1_Permit_TM       : CPC_DB_DPAR;

   BIC_TZ76_3_B2_Permit_TM       : CPC_DB_DPAR;

    END_STRUCT;  
  
  // Different variable view declaration
  DPAR AT DPAR_SET: ARRAY[1..6] OF CPC_DB_DPAR;
  
  // Support variables
  old_status : DWORD;
  I: INT;
END_VAR
  FOR I:=1 TO 6 DO
  
    // Recovery static variables
    old_status := DB_bin_status_DPAR.StsReg01[I].StsReg01;
    old_status := ROR(IN:=old_status, N:=16);
     
	// Calls the Baseline function
    CPC_FB_DPAR.DB_DPAR(    
        ManReg01 :=  DB_DPAR_ManRequest.DPAR_Requests[I].ManReg01   // set by WinCCOA in the DB_DPAR_ManRequest   
		,StsReg01 := DB_bin_status_DPAR.StsReg01[I].StsReg01
		,Perst:= DPAR[I]
    );
    
    // Event
    DB_Event_DPAR.DPAR_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_DPAR.StsReg01[I].StsReg01;
    
  END_FOR;
END_FUNCTION_BLOCK
     
(* All DPAR devices instance DB  **************************************)     
DATA_BLOCK DB_DPAR_all  FB_DPAR_all
//
// Instance DB for the whole DPAR devices initialization
//
BEGIN


DPAR_SET.BIC_TZ76_3_B1_PermitA_T.PosSt := W#16#0;		

DPAR_SET.BIC_TZ76_3_B1_PermitB_T.PosSt := W#16#0;		

DPAR_SET.BIC_TZ76_3_B2_PermitA_T.PosSt := W#16#0;		

DPAR_SET.BIC_TZ76_3_B2_PermitB_T.PosSt := W#16#0;		

DPAR_SET.BIC_TZ76_3_B1_Permit_TM.PosSt := W#16#0;		

DPAR_SET.BIC_TZ76_3_B2_Permit_TM.PosSt := W#16#0;		

END_DATA_BLOCK

FUNCTION FC_DPAR : VOID
    FB_DPAR_all.DB_DPAR_all();
END_FUNCTION
