//UNICOS
//� Copyright CERN 2013 all rights reserved

(* DIGITAL OUTPUT OBJECT DATA STRUCTURE *****************)

TYPE CPC_DB_DO
TITLE ='CPC_DB_DO'
AUTHOR: 'EN/ICE'
VERSION: 6.0

STRUCT     
  
   FEType:                   INT; 
   index:                    INT;       
   DBnum:                    INT;
   perByte:                  INT;   
   perBit:                   INT;   
   DBnumIoError:             INT;    
   DBposIoError:             INT;     
   DBbitIoError:             INT;      
   AuIhFoMo:                 BOOL;   
   PosSt:                    BOOL;      
   MPosRSt:                  BOOL;    
   AuPosRSt:                 BOOL;   
   AuMoSt:                   BOOL;    
   FoMoSt:                   BOOL;     
   IOErrorW:                 BOOL;   
   IOSimuW:                  BOOL;    
   IOError:                  BOOL;
   IOSimu:                   BOOL;
   FoDiAuW:                  BOOL;    
   AuPosR:                   BOOL;    
   MIOErBRSt:                BOOL;
   MIOErBSetRst_old:         BOOL;
   
END_STRUCT     
END_TYPE


(* DIGITAL INPUT OBJECT FUNCTION BLOCK *****************)

FUNCTION_BLOCK CPC_FB_DO
TITLE = 'CPC_FB_DO'
//
// DO: DIGITAL OUTPUT 
//
VERSION: '6.0'
AUTHOR: 'EN/ICE'
NAME: 'OBJECT'
FAMILY: 'IO'


VAR_INPUT                 
    
    Manreg01:                 WORD;
    Manreg01b AT Manreg01:    ARRAY [0..15] OF BOOL;

END_VAR

VAR_IN_OUT
    
    StsReg01:                  WORD;
    Perst:                     CPC_DB_DO;    
    
END_VAR    

VAR_TEMP     
                   
    E_MIOErBSetRst:             BOOL;
    TempStsReg01:               WORD;
    StsReg01b AT TempStsReg01:  ARRAY [0..15] OF BOOL;
    
END_VAR 

BEGIN

(* tranfer StsReg01 to a temporary variable to be able to treat bits individually.
Not possible if the variable is declared as IN_OUT variable*)

TempStsReg01 := StsReg01;

(* INPUT MANAGER *)
E_MIOErBSetRst  := R_EDGE(new:=ManReg01b[2],old:=Perst.MIOErBSetRst_old);   (* MIOErBSetRst Manual IO Error Block Set/Reset*)

(*MODE MANAGER *)
     (*AUTO*)
         IF ManReg01b[8] OR Perst.AuIhFoMo THEN                        (* MAuMoR *)
             Perst.FoMoSt := FALSE;                                    (* FoMoSt *)
         END_IF;
     
     (*FORCED*)
         IF Manreg01b[10]  AND NOT  Perst.AuIhFoMo  THEN               (* MFoMoR *)    
             Perst.FoMoSt := TRUE;                                     (* FoMoSt *)
         END_IF;
         
         Perst.AuMoSt := NOT Perst.FoMoSt;                   		   (* AuMoSt *)
         
(*SURVEILLANCE *)
    (*IO-WARNING*)
  (*BLOCK IO ERROR*)
       IF E_MIOErBSetRst THEN 
                Perst.MIOErBRSt := NOT Perst.MIOErBRSt;                        (* MIOErBRSt *)
       END_IF;                       
 
   (*WARNINGS*)
        Perst.IOErrorW := Perst.IOError AND NOT Perst.MIOErBRSt;       (* IOErrorW *)
        Perst.IOSimuW  := Perst.IOSimu;                                (* IOSimuW *)
        
(*REQUEST_MANAGER *)
    (*FORCED*) 
        IF ManReg01b[12] THEN                                          (* MOnR *)
            Perst.MPosRSt := TRUE; 
        END_IF;
        
        IF ManReg01b[13] THEN                                          (* MOffR *)
            Perst.MPosRSt := FALSE;  
        END_IF;    
              
    (*MODE EVALUATION*)
        IF Perst.FoMoSt THEN                                           (* FoMoSt *)
            Perst.PosSt   := Perst.MPosRSt ;                             
            Perst.FoDiAuW := Perst.MPosRSt XOR Perst.AuPosR;           (* FoDiAuW *)
        ELSE
            Perst.PosSt   := Perst.AuPosR;                             
            Perst.MPosRSt := Perst.AuPosR;                           
            Perst.FoDiAuW := FALSE;                                    (* FoDiAuW *)
        END_IF;
 
 (*OUTPUT MANAGER *)    

    Perst.AuPosRSt := Perst.AuPosR;

(* STATUS REGISTER *)  
    StsReg01b[8]  := Perst.PosSt;               //StsReg01 Bit 00
    StsReg01b[9]  := 0;                         //StsReg01 Bit 01 
    StsReg01b[10] := Perst.AuMoSt;              //StsReg01 Bit 02
    StsReg01b[11] := 0;                         //StsReg01 Bit 03
    StsReg01b[12] := Perst.FoMoSt;              //StsReg01 Bit 04
    StsReg01b[13] := 0;                         //StsReg01 Bit 05
    StsReg01b[14] := Perst.IOErrorW;            //StsReg01 Bit 06
    StsReg01b[15] := Perst.IOSimuW;             //StsReg01 Bit 07
    StsReg01b[0]  := Perst.FoDiAuW;             //StsReg01 Bit 08
    StsReg01b[1]  := Perst.MIOErBRSt;           //StsReg01 Bit 09
    StsReg01b[2]  := 0;                         //StsReg01 Bit 10
    StsReg01b[3]  := 0;                         //StsReg01 Bit 11
    StsReg01b[4]  := 0;                         //StsReg01 Bit 12
    StsReg01b[5]  := Perst.AuIhFoMo;            //StsReg01 Bit 13
    StsReg01b[6]  := Perst.AuPosRSt;            //StsReg01 Bit 14
    StsReg01b[7]  := Perst.MPosRSt;             //StsReg01 Bit 15

(* Values back to temporary variables*)
StsReg01 := TempStsReg01;

END_FUNCTION_BLOCK
