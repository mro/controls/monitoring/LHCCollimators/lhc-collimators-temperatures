(*
This file contains :
   - starting OB (100,102)
   - Errors: 81, 83, 84 (only S7-400)
   - Diagnostic functions : FC_Diag1&2
   - Diagnostic OB (82)
   - OB1(main cycle)
   - OB35 (sampling PIDs)
   - OB32 (UNICOS live counter)
  - All other errors treatment are included in the baseline
*)
(*RESTART (WARM RESTART)*********************************************) 
ORGANIZATION_BLOCK OB100 
TITLE = 'Warm restart'
//
// Called during warm restart
//
AUTHOR: 'UNICOS'
NAME: 'OB100'
FAMILY: 'EXEC'
VAR_TEMP
    OB100_EV_CLASS  : BYTE;
    OB100_STRUP     : BYTE;
    OB100_PRIORITY  : BYTE;
    OB100_OB_NUMBR  : BYTE;
    OB100_RESERVED_1: BYTE;
    OB100_RESERVED_2: BYTE;
    OB100_STOP      : WORD;
    OB100_STRT_INFO : DWORD;
    OB100_DATE_TIME   : DATE_AND_TIME;
END_VAR
BEGIN 
//Time Smoothing (alarms)
    UNICOS_TimeSmooth := 10;

//Init of First_Cycle variable	
	First_Cycle := TRUE;

//Init of Live counter marker form Live counter datablock	
	UNICOS_LiveCounter := DB_WINCCOA.UNICOS_LiveCounter;	
	

END_ORGANIZATION_BLOCK

(*Diagnostic OB for detect IOError**********************************************) 
ORGANIZATION_BLOCK OB82
TITLE = 'OB82'
//
// Called during an error on a channel if the diagnostic option is activated
//
AUTHOR: 'ICE/PLC'
NAME: 'OB82'
FAMILY: 'UNICOS'
VAR_TEMP
    OB82_EV_CLASS       : BYTE;
    OB82_FLT_ID         : BYTE;
    OB82_PRIORITY       : BYTE;
    OB82_OB_NUMBR       : BYTE;
    OB82_RESERVED_1     : BYTE;
    OB82_IO_FLAG        : BYTE;
    OB82_MDL_ADDR       : WORD;
    OB82_MDL_DEFECT     : BOOL;
    OB82_INT_FAULT      : BOOL;
    OB82_EXT_FAULT      : BOOL;
    OB82_PNT_INFO       : BOOL;
    OB82_EXT_VOLTAGE    : BOOL;
    OB82_FLD_CONNCTR    : BOOL;
    OB82_NO_CONFIG      : BOOL;
    OB82_CONFIG_ERR     : BOOL;
    OB82_MDL_TYPE       : BYTE;
    OB82_SUB_MDL_ERR    : BOOL;
    OB82_COMM_FAULT     : BOOL;
    OB82_MDL_STOP       : BOOL;
    OB82_WTCH_DOG_FLT   : BOOL;
    OB82_INT_PS_FLT     : BOOL;
    OB82_PRIM_BATT_FLT  : BOOL;
    OB82_BCKUP_BATT_FLT : BOOL;
    OB82_RESERVED_2     : BOOL;
    OB82_RACK_FLT       : BOOL;
    OB82_PROC_FLT       : BOOL;
    OB82_EPROM_FLT      : BOOL;
    OB82_RAM_FLT        : BOOL;
    OB82_ADU_FLT        : BOOL;
    OB82_FUSE_FLT       : BOOL;
    OB82_HW_INTR_FLT    : BOOL;
    OB82_RESERVED_3     : BOOL;
    OB82_DATE_TIME      : DATE_AND_TIME;    
    return_value    : INT;
    test_busy       : BOOL;
    Data            : ARRAY[0..15] OF BYTE;
    PLC_ADDR        : REAL;
    T               : INT;  //type of card (DI=1 AI=2 DO=3 AO=4)
    Chan_Error      : BOOL;
    i               : INT;    
    j               : INT; 
    mask            : BYTE;  
    break           : BOOL;
END_VAR
BEGIN
;
END_ORGANIZATION_BLOCK
(*OB1***************************************************************************)
ORGANIZATION_BLOCK OB1 
TITLE = 'OB1'
//
// Called during each cycle
//
AUTHOR: 'ICE/PLC'
NAME: 'OB1'
FAMILY: 'UNICOS'
VAR_TEMP
    OB1_EV_CLASS : BYTE;
    OB1_SCAN_1 : BYTE;
    OB1_PRIORITY : BYTE;
    OB1_OB_NUMBR : BYTE;
    OB1_RESERVED_1 : BYTE;
    OB1_RESERVED_2 : BYTE;
    OB1_PREV_CYCLE : INT;
    OB1_MIN_CYCLE : INT;
    OB1_MAX_CYCLE : INT;
    OB1_DATE_TIME : DATE_AND_TIME;
    
END_VAR
BEGIN 
     "T_CYCLE" := DINT_TO_TIME(INT_TO_DINT(OB1_PREV_CYCLE)); //getting OB1 sampling time
     
    (*Getting inputs*)
    FC_AI();    // Analog inputs
    FC_DPAR();    // DigitalParameter objects
    FC_APAR();    // AnalogParameter objects
    FC_WS();    // WordStatus objects

    // Accelerating communications in large applications (comms without events)
    IF Comm = 1 AND OB1_PREV_CYCLE > 250 THEN 
        FC_TSPP(NE :=false,RA := false);
    END_IF;

	(*Process Logic Control *)
    FC_PCO_LOGIC();       // Process Control object logic
	
    (*UNICOS objects calculating*)
    FC_ONOFF();    // OnOff objects
    FC_DA();   // DIGITAL ALARM objects
    FC_AA();   // ANALOG ALARM objects

    (*Setting Outputs*)
    FC_DO();    //Digital Outputs 

    // Calling the Recipes Mechanism and passing the information from WinCCOA (DB_RECIPES_INTERFACE) to the CPC_DB_RECIPES
	CPC_FB_RECIPES.CPC_DB_RECIPES(
		Header := DB_RECIPES_INTERFACE.RecipeHeader	  //Header from DB_RECIPES_INTERFACE to CPC_DB_RECIPES
		,DBnum := 116                  //DB number of Recipe buffers
		,HeaderBase := 2                   //Header buffer base address
		,StatusBase := 42                   //Recipe Status buffer base address
		,ManRegAddrBase := 82           //Manual Requests addresses buffer base address
		,ManRegValBase := 2082             //Manual Reuqests values buffer base address
		,ReqAddrBase := 4082                 //Request Address buffer base address
		,ReqValBase := 6082                   //Request Values buffer base address
		,BuffersBase := 82                 //Buffers base address
		,BuffersEnd := 10082                   //Buffers last address
		,Timeout := T#100s                         //Recipe transfer timeout
	);
	
	DB_RECIPES_INTERFACE.RecipeStatus := CPC_DB_RECIPES.Status; //Status from CPC_DB_RECIPES to DB_RECIPES_INTERFACE
	
     
     //Allow calling TS_Event_Manager for TSPP communication with the SCADA after the 1st cycle
     IF Comm = 0 THEN Comm := 1; 
     ELSE FC_Event(); 
     END_IF;  
	 First_Cycle:=FALSE; //First cycle terminated
	 

// 1 second counter to simulate OB32 in a S7-300
UNICOS_Counter1:=UNICOS_Counter1+OB1_PREV_CYCLE;
IF UNICOS_Counter1 > 1000 THEN
    UNICOS_Counter1:= UNICOS_Counter1 -1000;
	UNICOS_LiveCounter:=UNICOS_LiveCounter+1;
END_IF;

END_ORGANIZATION_BLOCK
// Scheduling:  No controller defintion 
