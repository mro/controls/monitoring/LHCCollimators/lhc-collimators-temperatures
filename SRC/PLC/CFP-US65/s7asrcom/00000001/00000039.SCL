//UNICOS
//� Copyright CERN 2013 all rights reserved

(* ANALOG INPUT REAL OBJECT DATA STRUCTURE **************************)

TYPE CPC_DB_AIR
TITLE ='CPC_DB_AIR'
AUTHOR : 'UNICOS'
VERSION: 6.1

  STRUCT     
  
   FEType:           INT;    //(0-> no addressing, 1-> DB addresss, 2-> type 1 + DB adress Ioerror) 
   DBnum:            INT;    //Front End Type InterfaceParam1. Db number (i.e.: 10 -> DB10)
   DBpos:            INT;    //Front End Type InterfaceParam2. Db position (i.e: 20 -> DBxx.DBD20)
   DBnumIoError:     INT;    //Front End Type InterfaceParam3. Db number (i.e.: 10 -> DB10
   DBposIoError:     INT;    //Front End Type InterfaceParam4. Db position (i.e: 20 -> DBxx.DBD20.y) 
   DBbitIoError:     INT;    //Front End Type InterfaceParam5. Db bit (i.e: 5 -> DBxx.DBD20.5)  
   index:            INT;    //Index of the AIC (recognize its status position)
   HFPos:            REAL;   //Value from  (REAL)
   PMinRan:          REAL;   //Range Min
   PMaxRan:          REAL;   //Range Max
   PMinRaw:          REAL;   //Scale Min
   PMaxRaw:          REAL;   //Scale Max
   PDb:              REAL;   //Dead Band
   MPosRSt:          REAL;   //Manual Position Request Status 
   PosSt:            REAL;   //Position Status
   HFSt:             REAL;   //Hardware Position Status
   Autoi_old:        REAL;
   MNewMR_old:       BOOL;
   AuIhFoMo:         BOOL;   //Force mode blocked by logic
   AuMoSt:           BOOL;   //Auto Mode status (StsReg01 bit=2)
   FoMoSt:           BOOL;   //Force Mode status (StsReg01 bit 4)
   IOErrorW:         BOOL;   //Error in input (StsReg01 bit 6)
   IOSimuW:          BOOL;   //Input simulated (StsReg01 bit 7)
   IOError:          BOOL;
   IOSimu:           BOOL;
   FoDiProW:         BOOL;   //Forced discordance (StsReg01 bit 8)
   MIOErBRSt:        BOOL;   //Manual IOError Block Request Status
   MIOErBSetRst_old: BOOL;   //Manual IOError Block Request Set/Reset old value
   PIWDef:           BOOL;
   
  END_STRUCT     
END_TYPE

(* ANALOG INPUT REAL OBJECT FUNCTION BLOCK **************************)

FUNCTION_BLOCK CPC_FB_AIR 
TITLE = 'CPC_FB_AIR'
//
// AIR: ANALOG INPUT REAL
//
VERSION: '6.1'
AUTHOR: 'EN/ICE'
NAME: 'OBJECT'
FAMILY: 'IO'

VAR_INPUT               
    
    Manreg01:                   WORD;
    Manreg01b AT Manreg01:      ARRAY [0..15] OF BOOL;
    MposR:                      REAL;

END_VAR

VAR_IN_OUT
    
    StsReg01:                   WORD;
    Perst:                      CPC_DB_AIR;
    
END_VAR    

VAR_TEMP                    
    
    Autoi:                      REAL;
    E_MIOErBSetRst:             BOOL;
    TempStsReg01:               WORD;
    StsReg01b AT TempStsReg01:  ARRAY [0..15] OF BOOL;
    
END_VAR 

BEGIN

(* tranfer StsReg01 to a temporary variable to be able to treat bits individually.
Not possible if the variable is declared as IN_OUT variable*)
TempStsReg01 := StsReg01;

(* INPUT MANAGER *)
E_MIOErBSetRst  := R_EDGE(new:=ManReg01b[2],old:=Perst.MIOErBSetRst_old);   (* MIOErBSetRst Manual IO Error Block Set/Reset*)

(*MODE MANAGER *)
     (*AUTO*)
         IF ManReg01b[8] OR Perst.AuIhFoMo THEN                   (* MAuMoR *)
             Perst.FoMoSt:= FALSE ;   
     END_IF;
     
         (*FORCED*)
         IF Manreg01b[10]  AND NOT  Perst.AuIhFoMo  THEN          (* MFoMoR *) 
             Perst.FoMoSt:= TRUE ;    
         END_IF;
         Perst.AuMoSt := NOT Perst.FoMoSt;    
         
(*SURVEILLANCE *)
    (*IO-WARNING*)
        Perst.IOErrorW := Perst.IOError AND NOT Perst.MIOErBRSt;  (* IoErrorW *)
        Perst.IOSimuW  := Perst.IOSimu;                           (* IoSimuW *)
 
(*PROCESS_INPUT *)
   (*ACQUISITION*)
        IF ManReg01b[14] AND (Perst.MNewMR_old XOR ManReg01b[14])  THEN      (* MNewMR *)
             Perst.MPosRSt := MPosR ;    
         END_IF;
         Perst.MNewMR_old := ManReg01b[14] ;
    
    (*CONVERSION*)
        Autoi := ((Perst.PMaxRan - Perst.PMinRan) / (Perst.PMaxRaw - Perst.PMinRaw)) * (Perst.HFPos - Perst.PMinRaw) + Perst.PMinRan;        
                                  
    (*REFRESHMENT*)
        IF ABS(Autoi - Perst.Autoi_old) * 100.0 / ABS(Perst.PMaxRan - Perst.PMinRan) >= Perst.PDb  THEN 
           Perst.Autoi_old := Autoi ;
        END_IF;
        Autoi := Perst.Autoi_old ;
        Perst.HFSt := Autoi ;
    
    (*MODE EVALUATION*)
        IF Perst.AuMoSt THEN       (* AuMoSt *)
            Perst.MPosRSt  := Autoi;
            Perst.PosSt    := Autoi;
            Perst.FoDiProW := FALSE;   (* FoDiProW *)
        ELSE
            Perst.PosSt    := Perst.MPosRSt ; 
            Perst.FoDiProW := (ABS(Perst.MPosRSt - Autoi) / ABS(Perst.PMaxRan - Perst.PMinRan)) > 0.01  ; 
        END_IF;

    (*BLOCK IO ERROR*)
        IF E_MIOErBSetRst THEN 
			Perst.MIOErBRSt := NOT Perst.MIOErBRSt;                       
        END_IF;
        
(*OUTPUT MANAGER *)
(* STATUS REGISTER *)  
    
    StsReg01b[8]  := 0;                         //StsReg01 Bit 00
    StsReg01b[9]  := 0;                         //StsReg01 Bit 01 
    StsReg01b[10] := Perst.AuMoSt;              //StsReg01 Bit 02
    StsReg01b[11] := 0;                         //StsReg01 Bit 03
    StsReg01b[12] := Perst.FoMoSt;              //StsReg01 Bit 04
    StsReg01b[13] := 0;                         //StsReg01 Bit 05
    StsReg01b[14] := Perst.IOErrorW;            //StsReg01 Bit 06
    StsReg01b[15] := Perst.IOSimuW;             //StsReg01 Bit 07
    StsReg01b[0]  := Perst.FoDiProW;            //StsReg01 Bit 08
    StsReg01b[1]  := Perst.MIOErBRSt;           //StsReg01 Bit 09
    StsReg01b[2]  := 0;                         //StsReg01 Bit 10
    StsReg01b[3]  := 0;                         //StsReg01 Bit 11
    StsReg01b[4]  := 0;                         //StsReg01 Bit 12
    StsReg01b[5]  := Perst.AuIhFoMo;            //StsReg01 Bit 13
    StsReg01b[6]  := 0;                         //StsReg01 Bit 14
    StsReg01b[7]  := 0;                         //StsReg01 Bit 15

(* Values back to temporary variables*)
StsReg01 := TempStsReg01;

END_FUNCTION_BLOCK