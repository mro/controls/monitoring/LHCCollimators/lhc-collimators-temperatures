//UNICOS
//� Copyright CERN 2013 all rights reserved

(*ANADO OBJECT FUNCTION BLOCK****************************)

FUNCTION_BLOCK CPC_FB_ANADO 
TITLE = 'CPC_FB_ANADO'
//
//  ANADO OBJECT
//
VERSION: '6.6'
AUTHOR: 'EN/ICE'
NAME: 'OBJECT'
FAMILY: 'FO'

VAR_INPUT              
    
    HFPos:                  REAL;
    HAOut:                  REAL;
    AuPosR:                 REAL;
    AuInSpd:                REAL;
    AuDeSpd:                REAL;
    PliOff:                 REAL;
    PliOn:                  REAL;
    MPosR:                  REAL;
    Manreg01:               WORD;
    Manreg01b AT Manreg01:  ARRAY [0..15] OF BOOL;
    HLD:                    BOOL;
    HFOn:                   BOOL;    
    HOnR:                   BOOL;
    HOffR:                  BOOL;
    IOError:                BOOL;
    IOSimu:                 BOOL;
    AlB:                    BOOL;
    StartI:                 BOOL;    
    TStopI:                 BOOL; 
    FuStopI:                BOOL;
    Al:                     BOOL;
    AuOnR:                  BOOL; 
    AuOffR:                 BOOL; 
    AuAuMoR:                BOOL; 
    AuIhMMo:                BOOL; 
    AuIhFoMo:               BOOL; 
    AuAlAck:                BOOL;
    IhAuMRW:                BOOL;
    AuRstart:               BOOL;
    PAnalog:                CPC_ANALOG_PARAM;
    PAnalogb AT PAnalog:    STRUCT
                            ParRegb:   ARRAY [0..15] OF BOOL;
                            PMaxRan:   REAL;
                            PMinRan:   REAL;
                            PMStpInV:  REAL;
                            PMStpDeV:  REAL;
                            PMInSpd:   REAL;
                            PMDeSpd:   REAL;
                            PWDt:      TIME;   
                            PWDb:      REAL;
                         END_STRUCT;  
    
END_VAR

VAR_OUTPUT             
    
    Stsreg01:               WORD;
    Stsreg01b AT Stsreg01:  ARRAY [0..15] OF BOOL;
    Stsreg02:               WORD;
    Stsreg02b AT Stsreg02:  ARRAY [0..15] OF BOOL;
    OutOV:                  REAL;
    AuPosRSt:               REAL;
    MPosRSt:                REAL;
    PosSt:                  REAL;
    PosRSt:                 REAL;
    OutOnOV:                BOOL;
    OutOnOVSt:              BOOL;
    MOnRSt:                 BOOL;
    MOffRSt:                BOOL;
    AuOnRSt:                BOOL;
    AuOffRSt:               BOOL;
    HOnRSt:                 BOOL;
    HOffRSt:                BOOL;
    OnSt:                   BOOL;
    AuMoSt:                 BOOL;
    MMoSt:                  BOOL;
    LDSt:                   BOOL;
    SoftLDSt:               BOOL;
    FoMoSt:                 BOOL;
    IOErrorW:               BOOL;
    IOSimuW:                BOOL;
    AuMRW:                  BOOL;
    PosW:                   BOOL;
    StartISt:               BOOL;
    TStopISt:               BOOL;
    FuStopISt:              BOOL;
    AlSt:                   BOOL;
    AlUnAck:                BOOL;
    AlBW:                   BOOL;
    EnRstartSt:             BOOL := TRUE;
    RdyStartSt:             BOOL;
    
END_VAR

VAR
  
    //edges  
    E_MAuMoR:               BOOL;
    E_MMMoR:                BOOL;
    E_MFoMoR:               BOOL;
    E_MOnR:                 BOOL;
    E_MOffR:                BOOL;
    E_MAlAckR:              BOOL;
    E_StartI:               BOOL;
    E_TStopI:               BOOL;
    E_FuStopI:              BOOL;
    E_Al:                   BOOL;
    E_AuAuMoR:              BOOL;
    E_AuAlAckR:             BOOL;
    E_MNewPosR:             BOOL;      
    E_MStpInR:              BOOL;
    E_MStpDeR:              BOOL;
    E_MSoftLDR:             BOOL;
    E_MEnRstartR:           BOOL;
    RE_AlUnAck:             BOOL;
    FE_AlUnAck:             BOOL;
    
    //old values
    MAuMoR_old:             BOOL;
    MMMoR_old:              BOOL;
    MFoMoR_old:             BOOL;
    MOnR_old:               BOOL;
    MOffR_old:              BOOL;
    MAlAckR_old:            BOOL;
    AuAuMoR_old:            BOOL;
    AuAlAckR_old:           BOOL;
    StartI_old:             BOOL;
    TStopI_old:             BOOL;
    FuStopI_old:            BOOL;
    Al_old:                 BOOL;
    MNewPosR_old:           BOOL;      
    MStpInR_old:            BOOL;
    MStpDeR_old:            BOOL;
    AlUnAck_old:            BOOL;
    MSoftLDR_old:           BOOL; 
    MEnRstartR_old:         BOOL;
    
    //temp var
    PosR:                   REAL;
    PFsPosOn:               BOOL;        
    PHFOn:                  BOOL;        
    PHFPos:                 BOOL;
    PPulse:                 BOOL;
    PHLD:                   BOOL;
    PHLDCmd:                BOOL; 
    PEnRstart:              BOOL;
    PRstartFS:              BOOL;
    AnalogOnSt:             BOOL;
    AnalogOffSt:            BOOL;
    AuMoSt_aux:             BOOL;
    MMoSt_aux:              BOOL;
    FoMoSt_aux:             BOOL;   
    SoftLDSt_aux:           BOOL;
    fullNotAcknowledged:    BOOL;
    InterlockR:             BOOL;
        
    Ramp_parameters:        STRUCT 
                            inc_rate: REAL;
                            dec_rate: REAL;
                            END_STRUCT;
             
    //FB RAMP
    ROC_LIM:                ROC_LIM;
    
    //IEC Timers
    Time_Warning:           TIME; 
    Timer_Warning:          TON;

	//Variables to handle delay of Interlock status
	PulseWidth:             REAL;
	FSIinc:                 INT;
	TSIinc:                 INT;
	SIinc:                  INT;
	Alinc:                  INT;
	WAlSt:                  BOOL;
	WFuStopISt:             BOOL;
	WTStopISt:              BOOL;
	WStartISt:              BOOL;

END_VAR

BEGIN

(* INPUT_MANAGER *)
         
     E_MAuMoR     := R_EDGE(new:=ManReg01b[8],old:=MAuMoR_old);       (* Manual Auto Mode Request   *)
     E_MMMoR      := R_EDGE(new:=ManReg01b[9],old:=MMMoR_old);        (* Manual Manual Mode Request *)
     E_MFoMoR     := R_EDGE(new:=ManReg01b[10],old:=MFoMoR_old);      (* Manual Forced Mode Request *)    
     E_MSoftLDR   := R_EDGE(new:=ManReg01b[11],old:=MSoftLDR_old);    (* Manual Software Local Drive Request *)
     E_MOnR       := R_EDGE(new:=ManReg01b[12],old:=MOnR_old);        (* Manual On/Open Request     *)
     E_MOffR      := R_EDGE(new:=ManReg01b[13],old:=MOffR_old);       (* Manual Off/close Request   *)
     E_MNewPosR   := R_EDGE(new:=ManReg01b[14],old:=MNewPosR_old);    (* Mamual New Manual position Request *)
     E_MStpInR    := R_EDGE(new:=ManReg01b[15],old:=MStpInR_old);     (* Manual Step Increase Request       *)
     E_MStpDeR    := R_EDGE(new:=ManReg01b[0],old:=MStpDeR_old);      (* Manual Step Decrease Request       *)
     E_MEnRstartR := R_EDGE(new:=ManReg01b[1],old:=MEnRstartR_old);   (* Manual Restart after full stop Request *)
     E_MAlAckR    := R_EDGE(new:=ManReg01b[7],old:=MAlAckR_old);      (* Manual Alarm Ack. Request  *)

 
     PFsPosOn  := PAnalogb.ParRegb[8];         (* Faile safe position 0:Off/close 1:On/Open *)
     PHFOn     := PAnalogb.ParRegb[9];         (* Hardware feedback On present*)
     PHFPos    := PAnalogb.ParRegb[11];        (* Hardware Position Feedback Present *)
     PHLD      := PAnalogb.ParRegb[12];        (* Local Drive mode Allowed *)
     PHLDCmd   := PAnalogb.ParRegb[13];        (* Local Command Allowed *)
     PEnRstart := PAnalogb.ParRegb[0];         (* Enable Restart after Full Stop *)
     PRstartFS := PAnalogb.ParRegb[1];         (* Enable Restart when Full Stop still active *)
     
     E_AuAuMoR  := R_EDGE(new:=AuAuMoR,old:=AuAuMoR_old);          (* Auto Auto Mode Request  *)
     E_AuAlAckR := R_EDGE(new:=AuAlAck,old:=AuAlAckR_old);         (* Auto Alarm Ack. Request *)
         
     E_StartI  := R_EDGE(new:=StartI,old:=StartI_old); 
     E_TStopI  := R_EDGE(new:=TStopI,old:=TStopI_old); 
     E_FuStopI := R_EDGE(new:=FuStopI,old:=FuStopI_old);
     E_Al      := R_EDGE(new:=Al,old:=Al_old); 
     
     StartISt  := StartI;               (* Start Interlock present*)
     TStopISt  := TStopI;               (* Temporary Stop Interlock present*)
     FuStopISt := FuStopI;              (* Full Stop Interlock present*)
     
 (*INTERLOCK & ACKNOWLEDGE  *)             
     
        IF (E_MAlAckR OR E_AuAlAckR) THEN     
            fullNotAcknowledged := FALSE;
            AlUnAck :=   FALSE;  
        ELSIF (E_TStopI OR  E_StartI OR E_FuStopI or E_Al) THEN   
            AlUnAck :=   TRUE; 
        END_IF;
        
        IF ((PEnRstart AND (E_MEnRstartR OR AuRstart) AND NOT FuStopISt) OR (PEnRstart AND PRstartFS AND (E_MEnRstartR OR AuRstart))) AND NOT fullNotAcknowledged THEN
            EnRstartSt := TRUE;
        END_IF;

        InterlockR :=   TStopISt OR FuStopISt OR FullNotAcknowledged OR NOT EnRstartSt OR 
                        (NOT PFsPosOn AND StartISt AND NOT OutOnOVSt) OR  
                        (PFsPosOn AND StartISt AND OutOnOVSt);  

 (* MODE MANAGER *)
  
     IF NOT (HLD AND PHLD ) THEN
         
         (* Forced Mode *)
             IF (AuMoSt_aux OR MMoSt_aux OR SoftLDSt_aux) AND 
                 E_MFoMoR AND NOT(AuIhFoMo) THEN
                    AuMoSt_aux   := FALSE;
                    MMoSt_aux    := FALSE;
                    FoMoSt_aux   := TRUE;
                    SoftLDSt_aux := FALSE;
            END_IF; 
         
         (* Manual Mode *)
             IF (AuMoSt_aux OR FoMoSt_aux OR SoftLDSt_aux) AND  E_MMMoR AND NOT(AuIhMMo) THEN
                    AuMoSt_aux   := FALSE;
                    MMoSt_aux    := TRUE;
                    FoMoSt_aux   := FALSE;
                    SoftLDSt_aux := FALSE;
             END_IF;      
         
         (* Auto Mode *)
             IF (MMoSt_aux AND (E_MAuMoR OR E_AuAuMoR )) OR
                (FoMoSt_aux AND E_MAuMoR) OR 
                (SoftLDSt_aux AND E_MAuMoR) OR
                (MMoSt_aux AND AuIhMMo) OR 
                (FoMoSt_aux AND AuIhFoMo)OR 
                (SoftLDSt_aux AND AuIhFoMo) OR
				 NOT(AuMoSt_aux OR MMoSt_aux OR FoMoSt_aux OR SoftLDSt_aux) THEN
                    AuMoSt_aux   := TRUE;
                    MMoSt_aux    := FALSE;
                    FoMoSt_aux   := FALSE;
                    SoftLDSt_aux := FALSE;
             END_IF;
         
         (* Software Local Mode *)    
             IF (AuMoSt_aux OR MMoSt_aux) AND E_MSoftLDR AND NOT AuIhFoMo THEN   
                    AuMoSt_aux  := FALSE;
                    MMoSt_aux   := FALSE;
                    FoMoSt_aux  := FALSE;
                    SoftLDSt_aux:= TRUE;
             END_IF;
    
            (* Status setting *)    
            LDSt     := FALSE;   
            AuMoSt   := AuMoSt_aux;
            MMoSt    := MMoSt_aux;
            FoMoSt   := FoMoSt_aux;
            SoftLDSt := SoftLDSt_aux;
     ELSE    
         (* Local Drive Mode *)
            AuMoSt  := FALSE;
            MMoSt   := FALSE;
            FoMoSt  := FALSE;
            LDSt    := TRUE;
            SoftLDSt:= FALSE;
     END_IF;     
     
 (* POSITION MANAGER *)
     
         IF PHFPos THEN
             PosSt := HFPos; 
         ELSIF PHLDCmd  THEN
             PosSt := HAOut;
         ELSE 
             PosSt := PosRSt;
         END_IF;
         
              
     (* LIMIT MANAGER *)
     
    (* On/Open Evaluation *)    
     OnSt:=  (PHFOn  AND HFOn) OR (NOT PHFOn AND OutOnOVSt);

    (* On/Open Evaluation on AO *)    
    AnalogOnSt:=  (PosSt >= PLiOn);
     
    (* Off/Close Evaluation on AO *)        
    AnalogOffSt:= (PosSt <= PLiOff);

         
 (* DO REQUEST MANAGER *) 
  
    (* Auto On/Off Request*) 
        
		IF AuOffR THEN           
        	AuOnRSt := FALSE; 
        ELSIF AuOnR THEN 
        	AuOnRSt := TRUE;
        ELSIF fullNotAcknowledged OR FuStopISt OR NOT EnRstartSt THEN
            AuOnRSt :=  PFsPosOn;
		END_IF;      
        AuOffRSt:= NOT AuOnRSt;
         
     (* Manual On/Off Request*) 
       
        IF (((E_MOffR  AND (MMoSt OR FoMoSt OR SoftLDSt)) 
           OR (AuOffRSt AND AuMoSt)
           OR (LDSt AND PHLDCmd AND HOffRSt) AND EnRstartSt)
           OR (E_FuStopI AND NOT PFsPosOn)) THEN 
           MOnRSt := FALSE; //RESET
       
        ELSIF (((E_MOnR  AND (MMoSt OR FoMoSt OR SoftLDSt))  
              OR (AuOnRSt  AND AuMoSt)  
              OR (LDSt AND PHLDCmd AND HOnRSt) AND EnRstartSt)
              OR (E_FuStopI AND PFsPosOn)) THEN 
              MOnRSt := TRUE; //SET
        END_IF;
        MOffRSt:= NOT(MOnRSt);    
         
     (* Local Drive Request *)
       
        IF HOffR THEN           
            HOnRSt :=   FALSE; //RESET
        ELSE IF HOnR THEN 
            HOnRSt := TRUE; //SET
        END_IF;
        END_IF;
        HOffRSt :=  NOT(HOnRSt);        


      (* Output On Request *)
       OutOnOVSt := (MOnRSt AND (MMoSt OR FoMoSt OR SoftLDSt)) OR (AuOnRSt AND AuMoSt) OR (HOnRSt AND LDSt AND PHLDCmd);  
  
         
 (* AO REQUEST MANAGER *)
      
     (* Manual Requests *)
      IF EnRstartSt THEN
         IF E_MNewPosR THEN
                MPosRSt := MPosR;
         END_IF;

         IF E_MStpInR THEN 
             MPosRSt := MPosRSt + PAnalog.PMStpInV * ABS((PAnalog.PMaxRan - PAnalog.PMinRan) / 100.0);
         END_IF;
         IF E_MStpDeR THEN 
             MPosRSt := MPosRSt - PAnalog.PMStpDeV * ABS((PAnalog.PMaxRan - PAnalog.PMinRan) / 100.0);
         END_IF;
         IF E_MOffR  THEN 
             MPosRSt := PAnalog.PMinRan;
         END_IF;      
      ELSE
         IF NOT(PFsPosOn) AND E_MOffR THEN
               MPosRSt:=PAnalog.PMinRan;
         END_IF;
     END_IF;
         
     (* Manual Saturation *)
         IF MPosRSt > PAnalog.PMaxRan THEN 
             MPosRSt := PAnalog.PMaxRan;
         END_IF;
         IF MPosRSt < PAnalog.PMinRan THEN 
             MPosRSt   := PAnalog.PMinRan;
         END_IF;
         
     (* Auto Request *)
        AuPosRSt:=AuPosR;
        
        IF AuOffR  THEN 
             AuPosRSt :=  PAnalog.PMinRan;  
         END_IF;
         
     (* Auto Saturation *)
         IF AuPosRSt > PAnalog.PMaxRan THEN 
             AuPosRSt := PAnalog.PMaxRan;  
         END_IF;
         IF AuPosRSt < PAnalog.PMinRan THEN 
             AuPosRSt   := PAnalog.PMinRan;    
         END_IF;
         
     (* Mode Evaluation *)
         IF AuMoSt THEN 
             PosR:= AuPosRSt;
             MPosRSt := AuPosRSt;
         ELSIF MMoSt OR FoMoSt OR SoftLDSt THEN 
             PosR:= MPosRSt;
         ELSE
             IF PHLDCmd AND PHLD THEN
                MPosRSt := HAOut;
                PosR    := HAOut;
             ELSE             
                MPosRSt := PosSt;
                PosR    := PosSt;
             END_IF;    
             Ramp_parameters.inc_rate := 0.0;
             Ramp_parameters.dec_rate := 0.0;
         END_IF;
               
      (* Interlocks *)
        IF E_FuStopI THEN
            fullNotAcknowledged:=TRUE;
            IF NOT AuMoSt THEN
                IF NOT(PFsPosOn) THEN
                    MPosRSt:=PAnalog.PMinRan;
                ELSE                
                    MPosRSt:=PAnalog.PMaxRan;                
                END_IF;
            END_IF;
            IF PEnRstart THEN           
               EnRstartSt:= FALSE;
            END_IF;
        END_IF; 

        IF NOT PFsPosOn THEN
            IF InterlockR  THEN
                PosR:= PAnalog.PMinRan; 
                OutOnOVSt:= FALSE;
            END_IF;
        ELSE 
            IF InterlockR  THEN
                PosR:=  PAnalog.PMaxRan; 
                OutOnOVSt:= TRUE;
            END_IF;
        END_IF;
                

       (*Alarms*)
        
        AlSt := Al;
        
 (* OUTPUT_MANAGER *)
     
     (* Auto Evaluation*)
         IF AuMoSt THEN 
                Ramp_parameters.inc_rate := AuInSpd;
                Ramp_parameters.dec_rate := AuDeSpd;
                
     (*Manual Evaluation *)    
         ELSIF MMoSt OR FoMoSt OR SoftLDSt THEN 
                Ramp_parameters.inc_rate := PAnalog.PMInSpd;
                Ramp_parameters.dec_rate := PAnalog.PMDeSpd;
    END_IF;
      
        (* Ramp Parameters for Stop Interlocks *)
         IF TStopI OR FuStopI  THEN
                Ramp_parameters.inc_rate := AuInSpd;
                Ramp_parameters.dec_rate := AuDeSpd;
         END_IF; 
      
        ROC_LIM (INV      := PosR, 
                 UPRLM_P  := Ramp_parameters.inc_rate,
                 DNRLM_P  := Ramp_parameters.dec_rate,
                 UPRLM_N  := Ramp_parameters.inc_rate,
                 DNRLM_N  := Ramp_parameters.dec_rate,
                 DFOUT_ON := FALSE,
                 DF_OUTV  := PosRSt,
                 H_LM     := PAnalog.PMaxRan,
                 L_LM     := PAnalog.PMinRan,
                 CYCLE    := "T_CYCLE");
       
        PosRSt := ROC_LIM.OUTV;  
        
        (* Ready to Start Status *)
        RdyStartSt :=   NOT InterlockR;
 (* SURVEILLANCE *)
  
     (* I/O Warning *)
         IOErrorW := IOError;
         IOSimuW := IOSimu; 
     
     (* Auto <> Manual Warning *)
        AuMRW :=(MMoSt OR FoMoSt OR SoftLDSt) AND (ABS(AuPosRSt - MPosRSt) > PAnalog.PWDb ) AND NOT IhAuMRW
                 OR  (MMoSt OR FoMoSt OR SoftLDSt) AND ((AuOnRSt XOR MOnRSt) OR (AuOffRSt XOR MOffRSt)) AND NOT IhAuMRW;;
     
     (* Position Warning*) 
      IF PHFPos AND PHFOn THEN
        Timer_Warning( IN:=(ABS(HFPos - MPosRSt)) > PAnalog.PWDb OR (NOT OutOnOVSt AND OnSt) OR (OutOnOVSt AND NOT OnSt),
                        PT:= PAnalog.PWDt);
     ELSIF PHFOn THEN
        Timer_Warning( IN:=(NOT OutOnOVSt AND OnSt) OR (OutOnOVSt AND NOT OnSt),
                        PT:= PAnalog.PWDt);
     ELSIF PHFPos  THEN
        Timer_Warning( IN:=(ABS(HFPos - MPosRSt)) > PAnalog.PWDb,
                        PT:= PAnalog.PWDt);
    ELSE                        
        Timer_Warning( IN:=0,
                        PT:= PAnalog.PWDt);
    END_IF;

 
     IF PHFPos OR PHFOn THEN
        PosW:= Timer_Warning.Q;
     ELSE
        PosW:=FALSE;
         END_IF;
         
    Time_Warning:=Timer_Warning.ET;                    

 (* OUTPUT REGISTER *)
     
     (* Analog and Digital Values *)     
         IF PFsPosOn THEN
            OutOV := PAnalog.PMaxRan + PAnalog.PMinRan - PosRSt;
            OutOnOV := NOT OutOnOVSt;
         ELSE
            OutOV := PosRSt;
            OutOnOV := OutOnOVSt;
         END_IF;
  
  (* Alarm Blocked Warning *)
    
    AlBW:=AlB;
   
   (* Maintain interlocks status 1.5s in Stsreg for PVSS*) 

PulseWidth := 1500 (* msec*) /DINT_TO_REAL(TIME_TO_DINT(T_CYCLE));

IF FuStopISt OR FSIinc > 0 THEN 
    FSIinc := FSIinc + 1; 
    WFuStopISt := TRUE; 
END_IF;

IF FSIinc > PulseWidth OR (NOT FuStopISt AND FSIinc = 0) THEN 
    FSIinc := 0; 
    WFuStopISt := FuStopISt; 
END_IF;

IF TStopISt OR TSIinc > 0 THEN 
    TSIinc := TSIinc + 1; 
    WTStopISt := True; 
END_IF;

IF TSIinc > PulseWidth OR (NOT TStopISt AND TSIinc = 0) THEN 
    TSIinc := 0; 
    WTStopISt := TStopISt; 
END_IF;    

if StartISt OR SIinc > 0 THEN 
    SIinc := SIinc + 1; 
    WStartISt:= True;
END_IF;

IF SIinc > PulseWidth OR (NOT StartISt AND SIinc = 0) THEN 
    SIinc := 0;
    WStartISt := StartISt; 
END_IF;

IF AlSt OR Alinc > 0 THEN 
    Alinc := Alinc + 1; 
    WAlSt := True; end_if;
    
IF Alinc >PulseWidth OR (NOT AlSt AND Alinc = 0) THEN 
    Alinc := 0; 
    WAlSt := AlSt; 
END_IF;
     
    (* STATUS REGISTER *)  
    
    Stsreg01b[8]  := OnSt;           //StsReg01 Bit 00
    Stsreg01b[9]  := 0;              //StsReg01 Bit 01
    Stsreg01b[10] := AuMoSt;         //StsReg01 Bit 02
    Stsreg01b[11] := MMoSt;          //StsReg01 Bit 03
    Stsreg01b[12] := FoMoSt;         //StsReg01 Bit 04
    Stsreg01b[13] := LDSt;           //StsReg01 Bit 05
    Stsreg01b[14] := IOErrorW;       //StsReg01 Bit 06
    Stsreg01b[15] := IOSimuW;        //StsReg01 Bit 07
    stsreg01b[0]  := AuMRW;          //StsReg01 Bit 08
    Stsreg01b[1]  := PosW;           //StsReg01 Bit 09
    Stsreg01b[2]  := WStartISt;       //StsReg01 Bit 10
    Stsreg01b[3]  := WTStopISt;       //StsReg01 Bit 11
    Stsreg01b[4]  := AlUnAck;        //StsReg01 Bit 12
    Stsreg01b[5]  := AuIhFoMo;       //StsReg01 Bit 13
    Stsreg01b[6]  := WAlSt;           //StsReg01 Bit 14
    Stsreg01b[7]  := AuIhMMo;        //StsReg01 Bit 15
    
    Stsreg02b[8]  := OutOnOVSt;      //StsReg02 Bit 00
    Stsreg02b[9]  := AuOnRst;        //StsReg02 Bit 01
    Stsreg02b[10] := MOnRSt;         //StsReg02 Bit 02
    Stsreg02b[11] := AuOffRSt;       //StsReg02 Bit 03
    Stsreg02b[12] := MOffRSt;        //StsReg02 Bit 04
    Stsreg02b[13] := HOnRst;         //StsReg02 Bit 05
    Stsreg02b[14] := HOffRSt;        //StsReg02 Bit 06
    Stsreg02b[15] := 0;              //StsReg02 Bit 07
    stsreg02b[0]  := AnalogOnSt;     //StsReg02 Bit 08
    Stsreg02b[1]  := AnalogOffSt;    //StsReg02 Bit 09
    Stsreg02b[2]  := WFuStopISt;      //StsReg02 Bit 10
    Stsreg02b[3]  := EnRstartSt;     //StsReg02 Bit 11
    Stsreg02b[4]  := SoftLDSt;       //StsReg02 Bit 12
    Stsreg02b[5]  := AlBW;           //StsReg02 Bit 13
    Stsreg02b[6]  := 0;              //StsReg02 Bit 14
    Stsreg02b[7]  := 0;              //StsReg02 Bit 15

(* Edges *)

  DETECT_EDGE(new:=AlUnAck,old:=AlUnAck_old,re:=RE_AlUnAck,fe:=FE_AlUnAck);
  
END_FUNCTION_BLOCK
