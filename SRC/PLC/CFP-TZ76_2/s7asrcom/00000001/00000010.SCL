//UNICOS
//� Copyright CERN 2013 all rights reserved

(* ON/OFF OBJECT FUNCTION BLOCK ********************************************)

FUNCTION_BLOCK CPC_FB_ONOFF
TITLE = 'CPC_FB_ONOFF'
//
//  ONOFF Object
//
VERSION: '6.6'
AUTHOR: 'EN/ICE'
NAME: 'OBJECT'
FAMILY: 'FO'

VAR_INPUT               
    
    HFOn:                   BOOL;
    HFOff:                  BOOL;
    HLD:                    BOOL;
    IOError:                BOOL;
    IOSimu:                 BOOL;
    AlB:                    BOOL;
    Manreg01:               WORD;
    Manreg01b AT Manreg01:  ARRAY [0..15] OF BOOL;
    HOnR:                   BOOL;
    HOffR:                  BOOL;
    StartI:                 BOOL;    
    TStopI:                 BOOL; 
    FuStopI:                BOOL;
    Al:                     BOOL;
    AuOnR:                  BOOL; 
    AuOffR:                 BOOL; 
    AuAuMoR:                BOOL; 
    AuIhMMo:                BOOL; 
    AuIhFoMo:               BOOL; 
    AuAlAck:                BOOL;
    IhAuMRW:                BOOL;
    AuRstart:               BOOL;
    POnOff:                 CPC_ONOFF_PARAM;
    POnOffb AT POnOff:      STRUCT
    ParRegb:                ARRAY [0..15] OF BOOL;
    PPulseLeb:              TIME;
    PWDtb:                  TIME;   
                            END_STRUCT;  


END_VAR

VAR_OUTPUT              
    
    Stsreg01:               WORD;
    Stsreg01b AT Stsreg01:  ARRAY [0..15] OF BOOL;
    Stsreg02:               WORD;
    Stsreg02b AT Stsreg02:  ARRAY [0..15] OF BOOL;
    OutOnOV:                BOOL;
    OutOffOV:               BOOL;
    OnSt:                   BOOL;
    OffSt:                  BOOL;
    AuMoSt:                 BOOL;
    MMoSt:                  BOOL;
    LDSt:                   BOOL;
    SoftLDSt:               BOOL;
    FoMoSt:                 BOOL;
    AuOnRSt:                BOOL;
    AuOffRSt:               BOOL;
    MOnRSt:                 BOOL;
    MOffRSt:                BOOL;
    HOnRSt:                 BOOL;
    HOffRSt:                BOOL;
    IOErrorW:               BOOL;
    IOSimuW:                BOOL;
    AuMRW:                  BOOL;
    AlUnAck:                BOOL;
    PosW:                   BOOL;
    StartISt:               BOOL;
    TStopISt:               BOOL;
    FuStopISt:              BOOL;
    AlSt:                   BOOL;
    AlBW:                   BOOL;
    EnRstartSt:             BOOL := TRUE;
    RdyStartSt:             BOOL;
    
    
END_VAR

VAR  //Internal Variables
  
    //Variables for Edge detection
    E_MAuMoR:               BOOL;
    E_MMMoR:                BOOL;
    E_MFoMoR:               BOOL;
    E_MOnR:                 BOOL;
    E_MOffR:                BOOL;
    E_MAlAckR:              BOOL;
    E_StartI:               BOOL;
    E_TStopI:               BOOL;
    E_FuStopI:              BOOL;
    E_Al:                   BOOL;
    E_AuAuMoR:              BOOL;
    E_AuAlAck:              BOOL;
    E_MSoftLDR:             BOOL;
    E_MEnRstartR:           BOOL;
    RE_AlUnAck:             BOOL;
    FE_AlUnAck:             BOOL;
    RE_PulseOn:             BOOL;
    FE_PulseOn:             BOOL;
    RE_PulseOff:            BOOL;
    RE_OutOVSt_aux:         BOOL;
    FE_OutOVSt_aux:         BOOL;
    FE_InterlockR:          BOOL;
    
    //Variables for old values
    MAuMoR_old:             BOOL;
    MMMoR_old:              BOOL;
    MFoMoR_old:             BOOL;
    MOnR_old:               BOOL;
    MOffR_old:              BOOL;
    MAlAckR_old:            BOOL;
    AuAuMoR_old:            BOOL;
    AuAlAck_old:            BOOL;
    StartI_old:             BOOL;
    TStopI_old:             BOOL;
    FuStopI_old:            BOOL;
    Al_old:                 BOOL;
    AlUnAck_old:            BOOL;
    MSoftLDR_old:           BOOL; 
    MEnRstartR_old:         BOOL;
    RE_PulseOn_old:         BOOL;
    FE_PulseOn_old:         BOOL;
    RE_PulseOff_old:        BOOL;
    RE_OutOVSt_aux_old:     BOOL;
    FE_OutOVSt_aux_old:     BOOL;
    FE_InterlockR_old:      BOOL;
    
    //General internal variables
    PFsPosOn:               BOOL;        
    PFsPosOn2:              BOOL;
    PHFOn:                  BOOL;        
    PHFOff:                 BOOL; 
    PPulse:                 BOOL;
    PPulseCste:              BOOL;
	PHLD:                   BOOL;
    PHLDCmd:                BOOL;
    PAnim:                  BOOL;
    POutOff:                BOOL;
    PEnRstart:              BOOL;
    PRstartFS:              BOOL;    
    OutOnOVSt:              BOOL;
    OutOffOVSt:             BOOL;    
    AuMoSt_aux:             BOOL;
    MMoSt_aux:              BOOL;
    FoMoSt_aux:             BOOL;   
    SoftLDSt_aux:           BOOL;
    PulseOn:                BOOL;
    PulseOff:               BOOL;
    PosW_aux:               BOOL;
    OutOVSt_aux:            BOOL;
    fullNotAcknowledged:    BOOL;
    PulseOnR:               BOOL;
    PulseOffR:              BOOL;
    InterlockR:             BOOL;
    
    //Variables for IEC Timers
    Time_Warning:           TIME; 
    Timer_PulseOn:          TP;
    Timer_PulseOff:         TP;
    Timer_Warning:          TON;
    
	//Variables for interlock Ststus delay handling
    PulseWidth:             REAL;
    FSIinc:                 INT;
	TSIinc:                 INT;
    SIinc:                  INT;   
	Alinc:                  INT;
	WTStopISt:              BOOL;
    WStartISt:              BOOL;
    WAlSt:                  BOOL;
	WFuStopISt:             BOOL;

END_VAR

BEGIN

(* INPUT MANAGER *)
     
     E_MAuMoR     := R_EDGE(new:=ManReg01b[8],old:=MAuMoR_old);               (* Manual Auto Mode Request   *)
     E_MMMoR      := R_EDGE(new:=ManReg01b[9],old:=MMMoR_old);                (* Manual Manual Mode Request *)
     E_MFoMoR     := R_EDGE(new:=ManReg01b[10],old:=MFoMoR_old);              (* Manual Forced Mode Request *)    
     E_MSoftLDR   := R_EDGE(new:=ManReg01b[11],old:=MSoftLDR_old);            (* Manual Software Local Drive Request *)
     E_MOnR       := R_EDGE(new:=ManReg01b[12],old:=MOnR_old);                (* Manual On/Open Request     *)
     E_MOffR      := R_EDGE(new:=ManReg01b[13],old:=MOffR_old);               (* Manual Off/close Request   *)
     E_MEnRstartR := R_EDGE(new:=ManReg01b[1],old:=MEnRstartR_old);           (* Manual Restart after full stop Request *)
     E_MAlAckR    := R_EDGE(new:=ManReg01b[7],old:=MAlAckR_old);              (* Manual Alarm Ack. Request  *)
 
     PFsPosOn  := POnOffb.ParRegb[8];                                      (* 1st Parameter bit to define Fail safe position behaviour *)
     PHFOn     := POnOffb.ParRegb[9];                                      (* Hardware feedback On present*)
     PHFOff    := POnOffb.ParRegb[10];                                     (* Hardware feedback Off present*)
     PPulse    := POnOffb.ParRegb[11];                                     (* Object is pulsed pulse duration : POnOff.PulseLe*)
     PHLD      := POnOffb.ParRegb[12];                                     (* Local Drive mode Allowed *)
     PHLDCmd   := POnOffb.ParRegb[13];                                     (* Local Drive Command allowed *)
     PAnim     := POnOffb.ParRegb[14];                                     (* Inverted Output*)
     POutOff   := POnOffb.ParRegb[15];
     PEnRstart := POnOffb.ParRegb[0];                                      (* Enable Restart after Full Stop *)
     PRstartFS := POnOffb.ParRegb[1];                                      (* Enable Restart when Full Stop still active *)    
     PFsPosOn2 := POnOffb.ParRegb[2];                                      (* 2nd Parameter bit to define Fail safe position behaviour *)
     PPulseCste := POnOffb.ParRegb[3];     								   (* Pulse Constant duration irrespective of the feedback status *)
	 
     E_AuAuMoR := R_EDGE(new:=AuAuMoR,old:=AuAuMoR_old);                   (* Auto Auto Mode Request  *)
     E_AuAlAck := R_EDGE(new:=AuAlAck,old:=AuAlAck_old);                   (* Auto Alarm Ack. Request *)
     
     E_StartI   := R_EDGE(new:=StartI,old:=StartI_old); 
     E_TStopI   := R_EDGE(new:=TStopI,old:=TStopI_old); 
     E_FuStopI  := R_EDGE(new:=FuStopI,old:=FuStopI_old);
     E_Al       := R_EDGE(new:=Al,old:=Al_old);    
      
     StartISt  := StartI;                                                  (* Start Interlock present *)
     TStopISt  := TStopI;                                                  (* Temporary Stop Interlock present  *)
     FuStopISt := FuStopI;                                                 (* Full Stop Interlock present  *)
     
 (* INTERLOCK & ACKNOWLEDGE *)

    IF (E_MAlAckR OR E_AuAlAck) THEN     
        fullNotAcknowledged := FALSE;
        AlUnAck := FALSE;  
    ELSIF (E_TStopI OR  E_StartI OR E_FuStopI OR E_Al) THEN   
        AlUnAck := TRUE; 
    END_IF;
    
    IF ((PEnRstart AND (E_MEnRstartR OR AuRstart) AND NOT FuStopISt) OR (PEnRstart AND PRstartFS AND (E_MEnRstartR OR AuRstart))) AND NOT fullNotAcknowledged THEN
        EnRstartSt := TRUE;
    END_IF;
    
    IF E_FuStopI THEN
        fullNotAcknowledged := TRUE;
		IF PEnRstart THEN           
			EnRstartSt := FALSE;
		END_IF;
    END_IF;
        
    InterlockR :=  TStopISt OR FuStopISt OR FullNotAcknowledged OR NOT EnRstartSt OR
                   (StartISt AND NOT POutOff AND NOT OutOnOV) OR
                   (StartISt AND POutOff AND ((PFsPosOn AND OutOVSt_aux) OR (NOT PFsPosOn AND NOT OutOVSt_aux)));

    FE_InterlockR  := F_EDGE (new:=InterlockR,old:=FE_InterlockR_old);
    
 (* MODE MANAGER *)
  
     IF NOT (HLD AND PHLD) THEN
         
         (* Forced Mode *)
             IF (AuMoSt_aux OR MMoSt_aux OR SoftLDSt_aux) AND 
                 E_MFoMoR AND NOT(AuIhFoMo) THEN
                    AuMoSt_aux   := FALSE;
                    MMoSt_aux    := FALSE;
                    FoMoSt_aux   := TRUE;
                    SoftLDSt_aux := FALSE;
            END_IF; 
         
         (* Manual Mode *)
             IF (AuMoSt_aux OR FoMoSt_aux OR SoftLDSt_aux) AND 
                 E_MMMoR AND NOT(AuIhMMo) THEN
                    AuMoSt_aux   := FALSE;
                    MMoSt_aux    := TRUE;
                    FoMoSt_aux   := FALSE;
                    SoftLDSt_aux := FALSE;
             END_IF;      
         
         (* Auto Mode *)
             IF (MMoSt_aux AND (E_MAuMoR OR E_AuAuMoR )) OR
                (FoMoSt_aux AND E_MAuMoR) OR 
                (SoftLDSt_aux AND E_MAuMoR) OR
                (MMoSt_aux AND AuIhMMo) OR 
                (FoMoSt_aux AND AuIhFoMo)OR 
				(SoftLDSt_aux AND AuIhFoMo) OR
                 NOT(AuMoSt_aux OR MMoSt_aux OR FoMoSt_aux OR SoftLDSt_aux) THEN
                    AuMoSt_aux   := TRUE;
                    MMoSt_aux    := FALSE;
                    FoMoSt_aux   := FALSE;
                    SoftLDSt_aux := FALSE;
             END_IF;
         
         (* Software Local Mode *)    
             IF (AuMoSt_aux OR MMoSt_aux) AND E_MSoftLDR AND NOT AuIhFoMo THEN   
                    AuMoSt_aux  := FALSE;
                    MMoSt_aux   := FALSE;
                    FoMoSt_aux  := FALSE;
                    SoftLDSt_aux:= TRUE;
             END_IF;
    
            (* Status setting *)    
            LDSt     := FALSE;   
            AuMoSt   := AuMoSt_aux;
            MMoSt    := MMoSt_aux;
            FoMoSt   := FoMoSt_aux;
            SoftLDSt := SoftLDSt_aux;
     ELSE    
         (* Local Drive Mode *)
            AuMoSt  := FALSE;
            MMoSt   := FALSE;
            FoMoSt  := FALSE;
            LDSt    := TRUE;
            SoftLDSt:= FALSE;
     END_IF; 
     
 (* LIMIT MANAGER *) 
  
     (* On/Open Evaluation *)
         OnSt:= (HFOn AND PHFOn) OR                                 (*Feedback ON present*)
                (NOT PHFOn AND PHFOff AND PAnim AND NOT HFOff) OR   (*Feedback ON not present and PAnim = TRUE*)
                (NOT PHFOn AND NOT PHFOff AND OutOVSt_aux);           

           
         
     (* Off/Closed Evaluation *)    
         OffSt:=(HFOff AND PHFOff) OR                               (*Feedback OFF present*)
                (NOT PHFOff AND PHFOn AND PAnim AND NOT HFOn) OR    (*Feedback OFF not present and PAnim = TRUE*)
                (NOT PHFOn AND NOT PHFOff AND NOT OutOVSt_aux);
         
 (* REQUEST MANAGER *) 
  
     (* Auto On/Off Request*) 
        
		IF AuOffR THEN           
        	AuOnRSt := FALSE; 
        ELSIF AuOnR THEN 
        	AuOnRSt := TRUE;
        ELSIF fullNotAcknowledged OR FuStopISt OR NOT EnRstartSt THEN
            AuOnRSt :=  PFsPosOn;
		END_IF;      
        AuOffRSt:= NOT AuOnRSt;
				 
     (* Manual On/Off Request*) 
       
        IF (((E_MOffR AND (MMoSt OR FoMoSt OR SoftLDSt))
           OR (AuOffRSt AND AuMoSt) 
           OR (LDSt AND PHLDCmd AND HOffRSt)
           OR (FE_PulseOn AND PPulse AND NOT POutOff) AND EnRstartSt)  
           OR (E_FuStopI AND NOT PFsPosOn)) THEN 
            
				MOnRSt := FALSE;
       
        ELSIF (((E_MOnR  AND (MMoSt OR FoMoSt OR SoftLDSt))  
              OR (AuOnRSt  AND AuMoSt)  
              OR (LDSt AND PHLDCmd AND HOnRSt) AND EnRstartSt)
              OR (E_FuStopI AND PFsPosOn)) THEN 
               
				MOnRSt := TRUE; 
        END_IF;
        
        MOffRSt:= NOT MOnRSt;    
         
     (* Local Drive Request *)
       
        IF HOffR THEN           
            HOnRSt := FALSE; 
        ELSE IF HOnR THEN 
            HOnRSt := TRUE; 
			 END_IF;
        END_IF;
        HOffRSt :=  NOT(HOnRSt); 
               
     
  (* PULSE REQUEST MANAGER*)    
  IF PPulse THEN
        IF InterlockR THEN  
           PulseOnR:= (PFsPosOn AND NOT PFsPosOn2) OR (PFsPosOn AND PFsPosOn2);
           PulseOffR:= (NOT PFsPosOn AND NOT PFsPosOn2) OR (PFsPosOn AND PFsPosOn2);
        ELSIF FE_InterlockR THEN 		(*Clear PulseOnR/PulseOffR to be sure you get a new pulse after InterlockR*)
           PulseOnR:= FALSE;
           PulseOffR:= FALSE;
		   Timer_PulseOn (IN:=FALSE,PT:=T#0s);
           Timer_PulseOff (IN:=FALSE,PT:=T#0s);
        ELSIF (MOffRSt AND (MMoSt OR FoMoSt OR SoftLDSt)) OR (AuOffRSt AND AuMoSt) OR (HOffR AND LDSt AND  PHLDCmd) THEN  //Off Request
           PulseOnR:= FALSE;
           PulseOffR:= TRUE;  
        ELSIF (MOnRSt AND (MMoSt OR FoMoSt OR SoftLDSt)) OR (AuOnRSt AND AuMoSt) OR (HOnR AND LDSt AND PHLDCmd) THEN //On Request
            PulseOnR:= TRUE;
            PulseOffR:= FALSE;
        ELSE    
           PulseOnR:= FALSE;
           PulseOffR:= FALSE;
        END_IF;
        
        
        //Pulse functions                   
        Timer_PulseOn (IN:= PulseOnR,PT:=POnOffb.PPulseLeb);
        Timer_PulseOff (IN:=PulseOffR,PT:=POnOffb.PPulseLeb);
           
        RE_PulseOn   := R_EDGE(new:=PulseOn,old:=RE_PulseOn_old);
        FE_PulseOn   := F_EDGE(new:=PulseOn,old:=FE_PulseOn_old);
        RE_PulseOff  := R_EDGE(new:=PulseOff,old:=RE_PulseOff_old);
              
        //The pulse functions have to be reset when changing from On to Off
        IF RE_PulseOn THEN
            Timer_PulseOff (IN:=FALSE,PT:=T#0s);
        END_IF;
       
        IF RE_PulseOff THEN
           Timer_PulseOn (IN:=FALSE,PT:=T#0s);
        END_IF;
       
		IF PPulseCste THEN 		(* Pulse constant duration irrespective of feedback status *)
			PulseOn  := Timer_PulseOn.Q AND NOT PulseOffR;
			PulseOff := Timer_PulseOff.Q AND NOT PulseOnR;
		ELSE
			PulseOn  := Timer_PulseOn.Q AND NOT PulseOffR AND (NOT PHFOn OR (PHFOn AND NOT HFOn)); 
			PulseOff := Timer_PulseOff.Q AND NOT PulseOnR AND (NOT PHFOff OR (PHFOff AND NOT HFOff)); 
		END_IF;
 END_IF;
 
     (* Output On Request *)
       OutOnOVSt := (PPulse AND PulseOn) OR
                    (NOT PPulse AND ((MOnRSt AND (MMoSt OR FoMoSt OR SoftLDSt)) OR 
                    (AuOnRSt AND AuMoSt) OR 
                    (HOnRST AND LDSt AND PHLDCmd)));  
        
     (* Output Off Request *)
       IF POutOff THEN
            OutOffOVSt := (PulseOff AND PPulse) OR
                          (NOT(PPulse) AND ((MOffRSt AND (MMoSt OR FoMoSt OR SoftLDSt)) OR (AuOffRSt AND AuMoSt) OR (HOffRST AND LDSt AND  PHLDCmd)));   
       END_IF;
       
     (* Interlocks / FailSafe *)
        
       IF POutOff THEN
			IF InterlockR THEN
                IF PPulse AND NOT PFsPosOn2 THEN
                    IF PFsPosOn THEN
                        OutOnOVSt := PulseOn;
                        OutOffOVSt :=  FALSE;
                    ELSE
                        OutOnOVSt := FALSE;
                        OutOffOVSt :=  PulseOff;
                    END_IF;
                ELSE        
                    OutOnOVSt := (PFsPosOn AND NOT PFsPosOn2) OR (PFsPosOn AND PFsPosOn2);
                    OutOffOVSt:= (NOT PFsPosOn AND NOT PFsPosOn2) OR (PFsPosOn AND PFsPosOn2); 
                END_IF;
            END_IF;
        ELSE
            IF InterlockR THEN
                    OutOnOVSt:= PFsPosOn; 
            END_IF;
        END_IF;     
     
     (* Ready to Start Status *)
     
     RdyStartSt := NOT InterlockR;

     (*Alarms*)
        
     AlSt := Al;
 
 (* SURVEILLANCE *) 
  
     (* I/O Warning *)
     IOErrorW := IOError;
     IOSimuW  := IOSimu; 
     
     (* Auto<> Manual Warning *)
     AuMRW    := (MMoSt OR FoMoSt OR SoftLDSt) AND 
                 ((AuOnRSt XOR MOnRSt) OR (AuOffRSt XOR MOffRSt)) AND NOT IhAuMRW;
     
   
                   
 (* OUTPUT_MANAGER AND OUTPUT REGISTER *)
    IF NOT POutOff THEN
        IF PFsPosOn THEN
             OutOnOV := NOT OutOnOVSt;
        ELSE
             OutOnOV := OutOnOVSt;
        END_IF;
    ELSE
        OutOnOV  := OutOnOVSt;
        OutOffOV := OutOffOVSt;
    END_IF;       

(* Position warning *) 
        
        (* Set reset of the OutOnOVSt *) 
         IF OutOnOVSt OR (PPulse AND PulseOnR) THEN
               OutOVSt_aux := TRUE;
        END_IF;
        IF  (OutOffOVSt AND POutOff) OR (NOT OutOnOVSt AND NOT POutOff) OR (PPulse AND PulseOffR) THEN
               OutOVSt_aux := FALSE;
        END_IF;
  
        RE_OutOVSt_aux   := R_EDGE(new:=OutOVSt_aux,old:=RE_OutOVSt_aux_old);
        FE_OutOVSt_aux   := F_EDGE(new:=OutOVSt_aux,old:=FE_OutOVSt_aux_old);
    
    IF ((OutOVSt_aux AND ((PHFOn AND NOT OnSt) OR (PHFOff AND OffSt))) 
        OR (NOT OutOVSt_aux AND ((PHFOff AND NOT OffSt) OR (PHFON AND OnSt))) 
        OR (OffSt AND OnSt))
        AND (NOT PPulse OR (POutOff AND PPulse AND NOT OutOnOV AND NOT OutOffOV))
    THEN
        PosW_aux:= TRUE;
    END_IF;
    
    IF  NOT ((OutOVSt_aux AND ((PHFOn AND NOT OnSt) OR (PHFOff AND OffSt)))
        OR (NOT OutOVSt_aux AND ((PHFOff AND NOT OffSt) OR (PHFON AND OnSt))) 
        OR (OffSt AND OnSt)) 
        OR RE_OutOVSt_aux 
        OR FE_OutOVSt_aux 
        OR (PPulse AND POutOff AND OutOnOV)
        OR (PPulse AND POutOff AND OutOffOV)
    THEN
        PosW_aux := FALSE;
    END_IF;
 
    Timer_Warning(IN := PosW_aux,
                      PT := POnOffb.PWDtb);
     
    PosW := Timer_Warning.Q;
    Time_Warning := Timer_Warning.ET;    
 
 (* Alarm Blocked Warning*)
    
    AlBW := AlB;
    
 (* Maintain Interlock status 1.5s in Stsreg for PVSS *) 

PulseWidth := 1500 (* msec*) / DINT_TO_REAL(TIME_TO_DINT(T_CYCLE));


IF FuStopISt OR FSIinc > 0 THEN 
    FSIinc := FSIinc + 1; 
    WFuStopISt := TRUE; 
END_IF;

IF FSIinc > PulseWidth OR (NOT FuStopISt AND FSIinc = 0) THEN 
    FSIinc := 0; 
    WFuStopISt := FuStopISt; 
END_IF;

IF TStopISt OR TSIinc > 0 THEN 
    TSIinc := TSIinc + 1; 
    WTStopISt := TRUE; 
END_IF;

IF TSIinc > PulseWidth OR (NOT TStopISt AND TSIinc = 0) THEN 
    TSIinc := 0; 
    WTStopISt := TStopISt; 
END_IF;    

if StartISt OR SIinc > 0 THEN 
    SIinc := SIinc + 1; 
    WStartISt:= TRUE;
END_IF;

IF SIinc > PulseWidth OR (NOT StartISt AND SIinc = 0) THEN 
    SIinc := 0;
    WStartISt := StartISt; 
END_IF;

IF AlSt OR Alinc > 0 THEN 
    Alinc := Alinc + 1; 
    WAlSt := TRUE; 
END_IF;
    
IF Alinc > PulseWidth OR (NOT AlSt AND Alinc = 0) THEN 
    Alinc := 0; 
    WAlSt := AlSt; 
END_IF;
    
           
 (* STATUS REGISTER *)  
    
    Stsreg01b[8]  := OnSt;             //StsReg01 Bit 00
    Stsreg01b[9]  := OffSt;            //StsReg01 Bit 01 
    Stsreg01b[10] := AuMoSt;           //StsReg01 Bit 02
    Stsreg01b[11] := MMoSt;            //StsReg01 Bit 03
    Stsreg01b[12] := FoMoSt;           //StsReg01 Bit 04
    Stsreg01b[13] := LDSt;             //StsReg01 Bit 05
    Stsreg01b[14] := IOErrorW;         //StsReg01 Bit 06
    Stsreg01b[15] := IOSimuW;          //StsReg01 Bit 07
    stsreg01b[0]  := AuMRW;            //StsReg01 Bit 08
    Stsreg01b[1]  := PosW;             //StsReg01 Bit 09
    Stsreg01b[2]  := WStartISt;         //StsReg01 Bit 10
    Stsreg01b[3]  := WTStopISt;         //StsReg01 Bit 11
    Stsreg01b[4]  := AlUnAck;          //StsReg01 Bit 12
    Stsreg01b[5]  := AuIhFoMo;         //StsReg01 Bit 13
    Stsreg01b[6]  := WAlSt;             //StsReg01 Bit 14
    Stsreg01b[7]  := AuIhMMo;          //StsReg01 Bit 15
    
    Stsreg02b[8]  := OutOnOVSt;        //StsReg02 Bit 00 
    Stsreg02b[9]  := AuOnRSt;          //StsReg02 Bit 01
    Stsreg02b[10] := MOnRSt;           //StsReg02 Bit 02
    Stsreg02b[11] := AuOffRSt;         //StsReg02 Bit 03
    Stsreg02b[12] := MOffRSt;          //StsReg02 Bit 04
    Stsreg02b[13] := HOnRSt;           //StsReg02 Bit 05
    Stsreg02b[14] := HOffRSt;          //StsReg02 Bit 06
    Stsreg02b[15] := 0;                //StsReg02 Bit 07
    stsreg02b[0]  := 0;                //StsReg02 Bit 08
    Stsreg02b[1]  := 0;                //StsReg02 Bit 09
    Stsreg02b[2]  := WFuStopISt ;       //StsReg02 Bit 10
    Stsreg02b[3]  := EnRstartSt;       //StsReg02 Bit 11
    Stsreg02b[4]  := SoftLDSt;         //StsReg02 Bit 12
    Stsreg02b[5]  := AlBW;             //StsReg02 Bit 13
    Stsreg02b[6]  := OutOffOVSt;       //StsReg02 Bit 14
    Stsreg02b[7]  := 0;                //StsReg02 Bit 15
           
(* Edges *)

  DETECT_EDGE(new:=AlUnAck,old:=AlUnAck_old,re:=RE_AlUnAck,fe:=FE_AlUnAck);

           
END_FUNCTION_BLOCK
