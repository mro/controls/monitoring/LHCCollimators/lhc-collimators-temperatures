DATA_BLOCK DB_WinCCOA
	
	
TITLE = 'DB_WinCCOA'
// 
// Contains TSPP parameters for WinCCOA
//
AUTHOR: 'UNICOS'
NAME: 'Comm'
FAMILY: 'COMMS'
STRUCT
    address_IP: 					DINT;                   	 //Address IP of WinCCOA
    address_Counter: 				INT;                    	 //Watchdog Counter
    address_CommandInterface: 		ARRAY [1..5] OF WORD;   	 //Command Interface FOR WinCCOA (Synchro - RequestAll)
    Data: 							ARRAY [1..5] OF WORD;   	 //information about S7 PLC (cycle/Error)

    TSPP_EventPeriod: 				TIME := T#2s;            	//Period before sending Event to WinCCOA if stack not full
    TSPP_Reqlist: 					ARRAY[1..4] OF INT;      	//Reqlist of TSPP_UNICOS_Manager  
    TSPP_Error: 					BOOL;                    	//Function error indicator of TSPP
    TSPP_Error_code: 				WORD;                    	//Function error code of TSPP 
    TSPP_Special_code: 				WORD;                    	//Error code of internaly used functions
    UNICOS_LiveCounter: 			DINT;       			 	//Live counter (1 second)
    Application_version: 			REAL:= 1.0;//Application version
	ResourcePackage_version:		STRUCT				     	//Resource Package version
		Major:						INT:= 1;
		Minor:						INT:= 7;
		Small:						INT:= 0;
	END_STRUCT;	
	UNICOS_NewExtensions:			ARRAY[1..40] OF WORD;    // Future extensions of S7-UNICOS
END_STRUCT
BEGIN
END_DATA_BLOCK

DATA_BLOCK DB_COMM
TITLE = 'DB_COMM' 
//
// Contains all numbers of status DB
//
AUTHOR: 'UNICOS'
NAME: 'Comm'
FAMILY: 'COMMS'
STRUCT
    nbDB    : INT;
    DB_List : ARRAY [1..14] OF CPC_DB_COMM;
END_STRUCT;
BEGIN

(* Recipes interface DB *)
DB_List[1].Status_DB := 116;
DB_List[1].Status_DB_old := 117;
DB_List[1].size := 82;
// nbDB:= 1
	

(* Global binary status DB of AnalogInput*)
DB_List[2].Status_DB := 148;
DB_List[2].Status_DB_old := 149;
DB_List[2].size := 180;
// nbDB:= 2

(* Global Analog status DB of AnalogInput*)
DB_List[3].Status_DB := 155;
DB_List[3].Status_DB_old := 156;
DB_List[3].size := 720;
// nbDB:= 3

(* Global binary status DB of DigitalOutput*)
DB_List[4].Status_DB := 168;
DB_List[4].Status_DB_old := 169;
DB_List[4].size := 8;
// nbDB:= 4

(* Global binary status DB of DigitalParameter*)
DB_List[5].Status_DB := 196;
DB_List[5].Status_DB_old := 197;
DB_List[5].size := 12;
// nbDB:= 5

(* Global binary status DB of AnalogParameter*)
DB_List[6].Status_DB := 214;
DB_List[6].Status_DB_old := 215;
DB_List[6].size := 4;
// nbDB:= 6

(* Global Analog status DB of AnalogParameter*)
DB_List[7].Status_DB := 221;
DB_List[7].Status_DB_old := 222;
DB_List[7].size := 16;
// nbDB:= 7

(* Global Analog status DB of WordStatus*)
DB_List[8].Status_DB := 229;
DB_List[8].Status_DB_old := 230;
DB_List[8].size := 4;
// nbDB:= 8

(* Global binary status DB of OnOff*)
DB_List[9].Status_DB := 244;
DB_List[9].Status_DB_old := 245;
DB_List[9].size := 16;
// nbDB:= 9

(* Global binary status DB of ProcessControlObject*)
DB_List[10].Status_DB := 282;
DB_List[10].Status_DB_old := 283;
DB_List[10].size := 4;
// nbDB:= 10

(* Global Analog status DB of ProcessControlObject*)
DB_List[11].Status_DB := 286;
DB_List[11].Status_DB_old := 287;
DB_List[11].size := 8;
// nbDB:= 11

(* Global binary status DB of DigitalAlarm*)
DB_List[12].Status_DB := 289;
DB_List[12].Status_DB_old := 290;
DB_List[12].size := 4;
// nbDB:= 12

(* Global binary status DB of AnalogAlarm*)
DB_List[13].Status_DB := 297;
DB_List[13].Status_DB_old := 298;
DB_List[13].size := 360;
// nbDB:= 13

(* Global Analog status DB of AnalogAlarm*)
DB_List[14].Status_DB := 304;
DB_List[14].Status_DB_old := 305;
DB_List[14].size := 1800;
// nbDB:= 14
nbDB:= 14;
END_DATA_BLOCK

DATA_BLOCK DB_EventData
TITLE = 'EventData'
//
// Contains pointers on evstsreg which have been changed
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT
   
        Nb_Event    : INT;
        List_Event   :  ARRAY [1..1000] OF STRUCT
                                            S7_ID     : BYTE;
                                            DataType  : BYTE;
                                            NbOfData  : INT;
                                            DBNumber  : INT;
                                            Address   : DWORD;
                                     END_STRUCT;
END_STRUCT;
BEGIN
END_DATA_BLOCK

	
FUNCTION FC_TSPP: VOID
TITLE = 'FC_TSPP'
//
// Calls the TSPP manager function
//
AUTHOR: 'UNICOS'
NAME: 'Comm'
FAMILY: 'COMMS'
VAR_INPUT
    NE: BOOL;
    RA: BOOL; 
END_VAR
  // Optimal TSPP parameters: 
  //    MaxStatusTable = NbStatusTables solved in the TSPP file generated
  //	MaxTableInOneSend = MaxTableInOneSend solved in the TSPP file generated
  //    StatusWordSize = 100

 
  TSPP_UNICOS_Manager.TSPP_UNICOS_DB(
                        Init :=  FALSE
                       ,SendID0 :=  B#16#1
                       ,SendID1 :=  B#16#2
                       ,SendEventPeriod := DB_WinCCOA.TSPP_EventPeriod
                       ,NewEvent :=  NE
                       ,EventTSIncluded := FALSE 
                       ,MultipleEvent :=  TRUE
                       ,EventListDB :=  DB_EventData
                       ,SendAllStatus := RA
                       ,ListOfStatusTable := DB_COMM 
                       ,WatchDog := DB_WinCCOA.Address_Counter
                       );
END_FUNCTION

    
(*FUNCTION WHICH GENERATE EVENT FOR evstsreg01 and CALL TS_EVENT_MANAGER*)
FUNCTION FC_Event : VOID
TITLE = 'FC_Event'
//
// FUNCTION WHICH GENERATE EVENT FOR evstsreg01 and CALL TS_EVENT_MANAGER
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
VAR
    NewEvent : 		BOOL;
	RequestAll: 	BOOL;
    Synchro: 		BOOL;
    i: 				INT;
    j: 				INT; 
    k: 				INT;
    First_obj: 		INT;
    dummyVar: 		INT;
	ListEventSize:  INT;
 END_VAR
BEGIN
DB_EventData.Nb_Event 	:= 0;
First_obj 				:= 0;
k						:= 2;
j						:= TSPP_Unicos_DB.ID_NewEvent;
NewEvent				:= 0;
RequestAll				:= FALSE;
Synchro					:= FALSE;
ListEventSize           := 1000;

(*Test if Evstsreg01 of AI have changed*)
FOR i:= 1 TO DB_event_AI.Nb_AI DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_AI.AI_evstsreg[i].evstsreg01) <> DWORD_TO_WORD(ROR(IN:=DB_event_AI.AI_evstsreg[i].evStsReg01, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_AI));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*4+k),N:=3) OR DW#16#84000000 ;
    END_IF;
END_FOR;


(*Test if Evstsreg01 of DO have changed*)
FOR i:= 1 TO DB_event_DO.Nb_DO DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_DO.DO_evstsreg[i].evstsreg01) <> DWORD_TO_WORD(ROR(IN:=DB_event_DO.DO_evstsreg[i].evStsReg01, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_DO));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*4+k),N:=3) OR DW#16#84000000 ;
    END_IF;
END_FOR;


(*Test if Evstsreg01 of DPAR have changed*)
FOR i:= 1 TO DB_event_DPAR.Nb_DPAR DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_DPAR.DPAR_evstsreg[i].evstsreg01) <> DWORD_TO_WORD(ROR(IN:=DB_event_DPAR.DPAR_evstsreg[i].evStsReg01, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_DPAR));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*4+k),N:=3) OR DW#16#84000000 ;
    END_IF;
END_FOR;


(*Test if Evstsreg01 of APAR have changed*)
FOR i:= 1 TO DB_event_APAR.Nb_APAR DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_APAR.APAR_evstsreg[i].evstsreg01) <> DWORD_TO_WORD(ROR(IN:=DB_event_APAR.APAR_evstsreg[i].evStsReg01, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_APAR));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*4+k),N:=3) OR DW#16#84000000 ;
    END_IF;
END_FOR;


							
(*Test if Evstsreg01 of ONOFF have changed*)
FOR i:= 1 TO DB_event_ONOFF.Nb_ONOFF DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_ONOFF.ONOFF_evstsreg[i].evstsreg01) <> DWORD_TO_WORD(ROR(IN:=DB_event_ONOFF.ONOFF_evstsreg[i].evStsReg01, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_ONOFF));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*8+k),N:=3) OR DW#16#84000000 ;
    END_IF;
END_FOR;


(*Test if Evstsreg02 of ONOFF have changed*)
First_obj := DB_event_ONOFF.Nb_ONOFF;
FOR i:= 1 TO DB_event_ONOFF.Nb_ONOFF DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_ONOFF.ONOFF_evstsreg[i].evstsreg02) <> DWORD_TO_WORD(ROR(IN:=DB_event_ONOFF.ONOFF_evstsreg[i].evstsreg02, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_ONOFF));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*8+k+4),N:=3) OR DW#16#84000000;
    END_IF;
END_FOR;

							
(*Test if Evstsreg01 of PCO have changed*)
FOR i:= 1 TO DB_event_PCO.Nb_PCO DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_PCO.PCO_evstsreg[i].evstsreg01) <> DWORD_TO_WORD(ROR(IN:=DB_event_PCO.PCO_evstsreg[i].evStsReg01, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_PCO));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*8+k),N:=3) OR DW#16#84000000 ;
    END_IF;
END_FOR;


(*Test if Evstsreg02 of PCO have changed*)
First_obj := DB_event_PCO.Nb_PCO;
FOR i:= 1 TO DB_event_PCO.Nb_PCO DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_PCO.PCO_evstsreg[i].evstsreg02) <> DWORD_TO_WORD(ROR(IN:=DB_event_PCO.PCO_evstsreg[i].evstsreg02, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_PCO));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*8+k+4),N:=3) OR DW#16#84000000;
    END_IF;
END_FOR;

(*Test if Evstsreg01 of DA have changed*)
FOR i:= 1 TO DB_event_DA.Nb_DA DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_DA.DA_evstsreg[i].evstsreg01) <> DWORD_TO_WORD(ROR(IN:=DB_event_DA.DA_evstsreg[i].evStsReg01, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_DA));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*4+k),N:=3) OR DW#16#84000000 ;
    END_IF;
END_FOR;


							
(*Test if Evstsreg01 of AA have changed*)
FOR i:= 1 TO DB_event_AA.Nb_AA DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_AA.AA_evstsreg[i].evstsreg01) <> DWORD_TO_WORD(ROR(IN:=DB_event_AA.AA_evstsreg[i].evStsReg01, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_AA));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*8+k),N:=3) OR DW#16#84000000 ;
    END_IF;
END_FOR;


(*Test if Evstsreg02 of AA have changed*)
First_obj := DB_event_AA.Nb_AA;
FOR i:= 1 TO DB_event_AA.Nb_AA DO
    //if new_stsreg <> old_stsreg
    IF DWORD_TO_WORD(DB_event_AA.AA_evstsreg[i].evstsreg02) <> DWORD_TO_WORD(ROR(IN:=DB_event_AA.AA_evstsreg[i].evstsreg02, N:= 16)) THEN
        NewEvent := 1;
        j := j+1;
        IF j>ListEventSize THEN j:=1; END_IF;
        DB_EventData.List_Event[j].S7_ID := B#16#10;
        DB_EventData.List_Event[j].DataType := 6; //DWORD
        DB_EventData.List_Event[j].NbOfData := 1;                
        DB_EventData.List_Event[j].DBNumber := WORD_TO_INT(BLOCK_DB_TO_WORD(DB_event_AA));
        DB_EventData.List_Event[j].Address := SHL(IN:=INT_TO_DWORD((i-1)*8+k+4),N:=3) OR DW#16#84000000;
    END_IF;
END_FOR;

DB_EventData.Nb_Event :=  j;
TSPP_Unicos_DB.ID_NewEvent := j;
(*Action by WinCCOA via the Command Interface*)
CASE WORD_TO_INT(DB_WinCCOA.Address_CommandInterface[1]) OF
    1 :   Synchro := TRUE;// Synchronisation of timing from WinCCOA
          DB_WinCCOA.Address_CommandInterface[1] := 0;
    2:    RequestAll := TRUE;// Request All : All object status are sent to WinCCOA
          DB_WinCCOA.Address_CommandInterface[1] := 0;
END_CASE;
// First Time: Events are not sent when cold start (overload of events !!)
IF Comm = 0 THEN Comm := 1;
ELSE 
    FC_TSPP(NE :=NewEvent & comm,RA := RequestAll);
END_IF;   
                       
Synchro := FALSE;
RequestAll := FALSE;
 DB_WinCCOA.Data[1] := DINT_TO_WORD(TIME_TO_DINT(T_CYCLE)); //PLC cycle
 DB_WinCCOA.Data[2] := INT_TO_WORD(TSPP_UNICOS_DB.NbOfTables);
 DB_WinCCOA.Data[3] := INT_TO_WORD(TSPP_UNICOS_DB.NbOfStatusReq);
 DB_WinCCOA.Data[4] := TSPP_UNICOS_DB.Error_code;
 DB_WinCCOA.Data[5] := TSPP_UNICOS_DB.Special_code;
                       
(*From TS Event Manager*)
 DB_WinCCOA.TSPP_ReqList        := TSPP_UNICOS_DB.Reqlist; 
 DB_WinCCOA.TSPP_Error          := TSPP_UNICOS_DB.Error;
 DB_WinCCOA.TSPP_Error_code     := TSPP_UNICOS_DB.Error_code;    
 DB_WinCCOA.TSPP_Special_code   := TSPP_UNICOS_DB.Special_code;
 // Live Counter (Time in seconds from last startUP)
 DB_WinCCOA.UNICOS_LiveCounter := UNICOS_LiveCounter;

END_FUNCTION
