//UNICOS
//� Copyright CERN 2013 all rights reserved

(* ANALOG ALARM OBJECT DATA STRUCTURE *****************)

TYPE CPC_DB_AA
TITLE ='CPC_DB_AA'
AUTHOR : UNICOS
VERSION: 6.3

STRUCT
   
   I:                     REAL; 
   PosSt:                 REAL;
   HHSt:                  REAL;
   HSt:                   REAL;
   LSt:                   REAL;
   LLSt:                  REAL;
   HH:                    REAL;
   H:                     REAL;
   L:                     REAL;
   LL:                    REAL;   
   PAlDt:                 INT;
   PAA:                   CPC_ANALOGALARM_PARAM;
   AuAlAck:               BOOL; 
   ISt:                   BOOL; 
   WSt:                   BOOL;
   AlUnAck:               BOOL; 
   MAlBRSt:               BOOL;
   AuIhMB:                BOOL;
   IOErrorW:              BOOL;
   IOSimuW:               BOOL; 
   IOError:               BOOL;
   IOSimu:                BOOL;
   AuEHH:                 BOOL;
   AuEH:                  BOOL;
   AuEL:                  BOOL;
   AuELL:                 BOOL;
   HHAlSt:                BOOL;
   HWSt:                  BOOL;
   LWSt:                  BOOL;
   LLAlSt:                BOOL;
   ConfigW:               BOOL;
   EHHSt:                 BOOL;
   EHSt:                  BOOL;
   ELSt:                  BOOL;
   ELLSt:                 BOOL;
   ArmRcpSt:              BOOL;
   PAuAckAl:              BOOL;
   
   // Old values Kept
   MAlBSetRst_old:        BOOL;
   MAlAck_old:            BOOL;
   AuAlAck_old:           BOOL;
   Alarm_Cond_old:        BOOL;
   MNewHHR_old:           BOOL;
   MNewHR_old:            BOOL;
   MNewLR_old:            BOOL;
   MNewLLR_old:           BOOL;
   ArmRcp_old:            BOOL;
   ActRcp_old:            BOOL; 
   AlUnAck_old:           BOOL;
   
   // Delayed properties
   HH_AlarmPh1:           BOOL;
   HH_AlarmPh2:           BOOL;
   H_AlarmPh1:            BOOL;
   H_AlarmPh2:            BOOL;
   L_AlarmPh1:            BOOL;
   L_AlarmPh2:            BOOL;
   LL_AlarmPh1:           BOOL;
   LL_AlarmPh2:           BOOL;

   HH_TimeAlarm:          DINT;
   H_TimeAlarm:           DINT;
   L_TimeAlarm:           DINT;
   LL_TimeAlarm:          DINT;
        
   TimeAlarm:        	  DINT;
   
   Iinc:				  INT;
   Winc:                  INT;
   HHinc:				  INT;
   Hinc:                  INT;
   Linc:				  INT;
   LLinc:                 INT;
   
   WISt:                  BOOL;
   WWSt:                  BOOL;
   WHHAlSt:				  BOOL;               
   WHWSt:				  BOOL;                 
   WLWSt:				  BOOL;                
   WLLAlSt:				  BOOL;   
	
	
END_STRUCT;
END_TYPE

(* ANALOG ALARM OBJECT FUNCTION BLOCK *****************)

FUNCTION_BLOCK CPC_FB_AA
TITLE = 'CPC_FB_AA'
//
//  AA: ANALOG ALARM
//
VERSION: '6.3'
AUTHOR: 'EN/ICE'
NAME: 'OBJECT'
FAMILY: 'ALARM'

VAR_INPUT               
    Manreg01:                   WORD;
    Manreg01b AT Manreg01:      ARRAY [0..15] OF BOOL;
    HH:                         REAL;
    H:                          REAL;
    L:                          REAL;
    LL:                         REAL;     
END_VAR

VAR_IN_OUT              
    StsReg01:                   WORD;
    StsReg02:                   WORD;
    Perst:                      CPC_DB_AA;
     
END_VAR

VAR_TEMP
    //R_EDGEs
    E_MAlBSetRst:               BOOL;
    E_MAlAck:                   BOOL;
    E_AuAlAck:                  BOOL;
    E_Alarm_Cond:               BOOL;
    
    E_MNewHHR:                  BOOL;
    E_MNewHR:                   BOOL;
    E_MNewLR:                   BOOL;
    E_MNewLLR:                  BOOL;
    
    E_ArmRcp:                   BOOL;
    E_ActRcp:                   BOOL;
    
    RE_AlUnAck:                 BOOL;
    FE_AlUnAck:                 BOOL;           

    //Threshold Sub-Alarms
    PosHHW:                     BOOL;
    PosHW:                      BOOL;
    PosLW:                      BOOL;
    PosLLW:                     BOOL;
    
    //Temp variables
    ConfigW:                    BOOL;
    IhMHHSt:                    BOOL;                                         
    IhMHSt:                     BOOL;                                       
    IhMLSt:                     BOOL;                               
    IhMLLSt:                    BOOL;
    PAuAckAl:                   BOOL;
    TempStsReg01:               WORD;
    StsReg01b AT TempStsReg01:  ARRAY [0..15] OF BOOL;
    TempStsReg02:               WORD;
    StsReg02b AT TempStsReg02:  ARRAY [0..15] OF BOOL;   
    TempPAA:                    CPC_ANALOGALARM_PARAM;
    PAAb AT TempPAA:            STRUCT
    ParRegb:                    ARRAY [0..15] OF BOOL;
								END_STRUCT;
	//Variable for Alarm Status Status delay handling
    PulseWidth:                 REAL;
	

END_VAR

BEGIN

(* tranfer StsReg01, StsReg02 and PAA to temporary variables to be able to treat bits individually.
Not possible if the variable is declared as IN_OUT variable*)

TempStsReg01 := StsReg01;
TempStsReg02 := StsReg02;
TempPAA      := Perst.PAA;

(* INPUT_MANAGER *)

E_ArmRcp      := R_EDGE(new:=ManReg01b[10],old:=Perst.ArmRcp_old);                      (* ArmRcp Arm Recipe *)
E_ActRcp      := R_EDGE(new:=ManReg01b[11],old:=Perst.ActRcp_old);                      (* ActRcp Activate Recipe *)
E_MNewHHR     := R_EDGE(new:=ManReg01b[0],old:=Perst.MNewHHR_old);                      (* MNewHHR Manual New HH Request *)
E_MNewHR      := R_EDGE(new:=ManReg01b[1],old:=Perst.MNewHR_old);                       (* MNewHR Manual New H Request *)
E_MAlBSetRst  := R_EDGE(new:=ManReg01b[2],old:=Perst.MAlBSetRst_old);                   (* MBDAlAct Block/Deblock*)
E_MNewLR      := R_EDGE(new:=ManReg01b[4],old:=Perst.MNewLR_old);                       (* MNewLR Manual New L Request *) 
E_MNewLLR     := R_EDGE(new:=ManReg01b[5],old:=Perst.MNewLLR_old);                      (* MNewLLR Manual New LL Request *)
E_MAlAck      := R_EDGE(new:=ManReg01b[7],old:=Perst.MAlAck_old);                       (* MAlAck *)

E_AuAlAck     := R_EDGE(new:=Perst.AuAlAck,old:=Perst.AuAlAck_old);

IhMHHSt       := PAAb.ParRegb[8];                                                       (* Parameter Inhibit Manual HH Threshold *)
IhMHSt        := PAAb.ParRegb[9];                                                       (* Parameter Inhibit Manual H Threshold *)
IhMLSt        := PAAb.ParRegb[10];                                                      (* Parameter Inhibit Manual L Threshold *)
IhMLLSt       := PAAb.ParRegb[11];                                                      (* Parameter Inhibit Manual LL Threshold *)
PAuAckAl      := PAAb.ParRegb[12]; 

(* Alarm Action Blocked *)
IF E_MAlBSetRst THEN 
    Perst.MAlBRSt := NOT Perst.MAlBRSt;
END_IF;        

(* Manual inhibit block alarm *)
IF Perst.AuIhMB THEN 
    Perst.MAlBRSt := FALSE;
END_IF;


(* THRESHOLD HANDLING. Recipe mechanism ******************)

(* Auto Enable *)
Perst.EHHSt := Perst.AuEHH;
Perst.EHSt  := Perst.AuEH;
Perst.ELSt  := Perst.AuEL;
Perst.ELLSt := Perst.AuELL;
  
(* ARM RECIPE *)
IF E_ArmRcp AND NOT((E_MNewHHR AND IhMHHSt) OR (E_MNewHR AND IhMHSt) OR (E_MNewLR AND IhMLSt) OR (E_MNewLLR AND IhMLLSt))THEN 
   Perst.ArmRcpSt := TRUE;
END_IF;

(* CANCEL RECIPE *)
IF E_ArmRcp AND E_ActRcp THEN
   Perst.ArmRcpSt := FALSE ;
END_IF;


(* Thresholds Handling *)
// Initialisation 
IF First_Cycle THEN
        Perst.HHSt := HH;
        Perst.HSt := H;
        Perst.LSt := L;
        Perst.LLSt := LL;
END_IF;

IF IhMHHSt THEN 
    Perst.HHSt := Perst.HH;
ELSIF((E_MNewHHR AND NOT Perst.ArmRcpSt) OR (E_MNewHHR AND Perst.ArmRcpSt AND E_ActRcp)) THEN
        Perst.HHSt := HH;
END_IF;

IF IhMHSt THEN 
    Perst.HSt := Perst.H;
ELSIF((E_MNewHR AND NOT Perst.ArmRcpSt) OR (E_MNewHR AND Perst.ArmRcpSt AND E_ActRcp)) THEN
        Perst.HSt := H;
END_IF;

IF IhMLSt THEN 
    Perst.LSt := Perst.L;
ELSIF((E_MNewLR AND NOT Perst.ArmRcpSt) OR (E_MNewLR AND Perst.ArmRcpSt AND E_ActRcp)) THEN
        Perst.LSt := L;
END_IF;

IF IhMLLSt THEN 
    Perst.LLSt := Perst.LL;
ELSIF((E_MNewLLR AND NOT Perst.ArmRcpSt) OR (E_MNewLLR AND Perst.ArmRcpSt AND E_ActRcp)) THEN
        Perst.LLSt := LL;
END_IF;


(* Alarm Input HH ***************************************)
IF  Perst.EHHSt AND (Perst.I>Perst.HHSt) THEN 
    Perst.HH_AlarmPh1:=TRUE; 
ELSE
    Perst.HH_AlarmPh1:=FALSE;
    Perst.HH_AlarmPh2:=FALSE;
END_IF;


IF Perst.HH_AlarmPh1 THEN 
    IF NOT(Perst.HH_AlarmPh2) THEN                           // Time Smoothing activated 
        Perst.HH_TimeAlarm:=UNICOS_LiveCounter + INT_TO_DINT(Perst.PAlDt);
        Perst.HH_AlarmPh2:=TRUE;  
    
    END_IF;

    IF Perst.HH_AlarmPh2  THEN
        IF (UNICOS_LiveCounter >= Perst.HH_TimeAlarm)  THEN 
            IF Perst.MAlBRSt THEN
                Perst.HHAlSt := FALSE;
                PosHHW := TRUE;
            ELSE
                Perst.HHAlSt := TRUE;
                PosHHW := FALSE;
            END_IF;
        ELSIF Perst.MAlBRSt THEN
            Perst.HHAlSt := FALSE;
            PosHHW := FALSE;
        ELSE
            Perst.HHAlSt := FALSE;
            PosHHW := TRUE;
        END_IF;
    END_IF ;    

    // Dealing with reinitialization of the PLC and them UNICOS_LiveCounter=0
    IF ((Perst.HH_TimeAlarm - INT_TO_DINT(Perst.PAlDt)) > UNICOS_LiveCounter) THEN
            Perst.HH_TimeAlarm:=UNICOS_LiveCounter + INT_TO_DINT(Perst.PAlDt);  

    END_IF;
ELSE
Perst.HH_TimeAlarm := 0;
Perst.HHAlSt       := FALSE;
PosHHW             := FALSE;
END_IF;  

(* Alarm Input LL ************************************)
IF  Perst.ELLSt AND (Perst.I<Perst.LLSt) THEN 
    Perst.LL_AlarmPh1:=TRUE; 
ELSE
    Perst.LL_AlarmPh1:=FALSE;
    Perst.LL_AlarmPh2:=FALSE;
END_IF;

IF Perst.LL_AlarmPh1 THEN 
    IF NOT(Perst.LL_AlarmPh2) THEN                           // Time Smoothing activated 
        Perst.LL_TimeAlarm:=UNICOS_LiveCounter + INT_TO_DINT(Perst.PAlDt);
        Perst.LL_AlarmPh2:=TRUE;  
    
    END_IF;
    
    IF Perst.LL_AlarmPh2  THEN
        IF (UNICOS_LiveCounter >= Perst.LL_TimeAlarm)  THEN 
            IF Perst.MAlBRSt THEN
                Perst.LLAlSt := FALSE;
                PosLLW := TRUE;
            ELSE
                Perst.LLAlSt := TRUE;
                PosLLW := FALSE;
            END_IF;
        ELSIF Perst.MAlBRSt THEN
            Perst.LLAlSt := FALSE;
            PosLLW := FALSE;
        ELSE
            Perst.LLAlSt := FALSE;
            PosLLW := TRUE;
        END_IF;
    END_IF ;
        // Dealing with reinitialization of the PLC and them UNICOS_LiveCounter=0
        IF ((Perst.LL_TimeAlarm - INT_TO_DINT(Perst.PAlDt)) > UNICOS_LiveCounter) THEN
            Perst.LL_TimeAlarm:=UNICOS_LiveCounter + INT_TO_DINT(Perst.PAlDt);
        END_IF;

ELSE
Perst.LL_TimeAlarm := 0;
Perst.LLAlSt       := FALSE;
PosLLW             := FALSE;
END_IF;  

(* Warning input H **********************************)
IF  Perst.EHSt AND (Perst.I>Perst.HSt) THEN 
    Perst.H_AlarmPh1:=TRUE; 
ELSE
    Perst.H_AlarmPh1:=FALSE;
    Perst.H_AlarmPh2:=FALSE;
END_IF;

IF Perst.H_AlarmPh1 THEN 
    IF NOT(Perst.H_AlarmPh2) THEN                           // Time Smoothing activated 
        Perst.H_TimeAlarm:=UNICOS_LiveCounter + INT_TO_DINT(Perst.PAlDt);
        Perst.H_AlarmPh2:=TRUE;  
    END_IF;

    IF Perst.H_AlarmPh2  THEN
        IF (UNICOS_LiveCounter >= Perst.H_TimeAlarm)  THEN 
            IF Perst.MAlBRSt THEN
                Perst.HWSt := FALSE;
                PosHW := TRUE;
            ELSE
                Perst.HWSt := TRUE;
                PosHW := FALSE;
            END_IF;
        ELSIF Perst.MAlBRSt THEN
            Perst.HWSt := FALSE;
            PosHW := FALSE;
        ELSE
            Perst.HWSt := FALSE;
            PosHW := TRUE;
        END_IF;
    END_IF ;
        // Dealing with reinitialization of the PLC and them UNICOS_LiveCounter=0
        IF ((Perst.H_TimeAlarm - INT_TO_DINT(Perst.PAlDt)) > UNICOS_LiveCounter) THEN
            Perst.H_TimeAlarm:=UNICOS_LiveCounter + INT_TO_DINT(Perst.PAlDt);
        END_IF;

ELSE
Perst.H_TimeAlarm := 0;
Perst.HWSt        := FALSE;
PosHW             := FALSE;
END_IF; 

(* Warning input L *******************************)
IF  Perst.ELSt AND (Perst.I<Perst.LSt) THEN 
    Perst.L_AlarmPh1:=TRUE; 
ELSE
    Perst.L_AlarmPh1:=FALSE;
    Perst.L_AlarmPh2:=FALSE;
END_IF;


IF Perst.L_AlarmPh1 THEN 
    IF NOT(Perst.L_AlarmPh2) THEN                           // Time Smoothing activated 
        Perst.L_TimeAlarm:=UNICOS_LiveCounter + INT_TO_DINT(Perst.PAlDt);
        Perst.L_AlarmPh2:=TRUE;  
    END_IF;

       IF Perst.L_AlarmPh2  THEN
        IF (UNICOS_LiveCounter >= Perst.L_TimeAlarm)  THEN 
            IF Perst.MAlBRSt THEN
                Perst.LWSt := FALSE;
                PosLW := TRUE;
            ELSE
                Perst.LWSt := TRUE;
                PosLW := FALSE;
            END_IF;
        ELSIF Perst.MAlBRSt THEN
            Perst.LWSt := FALSE;
            PosLW := FALSE;
        ELSE
            Perst.LWSt := FALSE;
            PosLW := TRUE;
        END_IF;
    END_IF ;
        // Dealing with reinitialization of the PLC and them UNICOS_LiveCounter=0
        IF ((Perst.L_TimeAlarm - INT_TO_DINT(Perst.PAlDt)) > UNICOS_LiveCounter) THEN
            Perst.L_TimeAlarm:=UNICOS_LiveCounter + INT_TO_DINT(Perst.PAlDt);
        END_IF;

ELSE
Perst.L_TimeAlarm := 0;
Perst.LWSt        := FALSE;
PosLW             := FALSE;
END_IF; 

(* Alarm and Warning output *********************)
Perst.ISt    := Perst.HHAlSt OR Perst.LLAlSt;
Perst.WSt    := Perst.HWSt OR Perst.LWSt;

E_Alarm_Cond := R_EDGE(new:=Perst.ISt,old:=Perst.Alarm_Cond_old);

(* OUTPUT MANAGER *****************************)
IF E_MAlAck OR E_AuAlAck OR Perst.MAlBRSt OR PAuAckAl THEN           
            Perst.AlUnAck :=   FALSE; 
        ELSIF (E_Alarm_Cond) THEN 
            Perst.AlUnAck :=   TRUE; 
END_IF;

(* SURVEILLANCE: IO-ERROR/IO-SIMU WARNING *)
Perst.IOErrorW := Perst.IOError;
Perst.IOSimuW := Perst.IOSimu;
  
(* Configuration Warning if thresholds are not crescendo*)
ConfigW :=  (Perst.ELLSt AND Perst.ELSt  AND (Perst.LSt<Perst.LLSt))  OR 
            (Perst.EHSt  AND Perst.ELSt  AND (Perst.HSt<Perst.LSt))   OR 
            (Perst.EHHSt AND Perst.EHSt  AND (Perst.HHSt<Perst.HSt))  OR 
            (Perst.ELLSt AND Perst.EHSt  AND (Perst.LLSt>Perst.HSt))  OR 
            (Perst.ELLSt AND Perst.EHHSt AND (Perst.LLSt>Perst.HHSt)) OR 
            (Perst.ELSt  AND Perst.EHHSt AND (Perst.LSt>Perst.HHSt))  OR
            (NOT Perst.EHHSt AND NOT Perst.EHSt AND NOT Perst.ELSt AND NOT Perst.ELLSt);

(* Output PosSt *)
Perst.PosSt := Perst.I;

(* RESET RECIPE *)
IF Perst.ArmRcpSt AND E_ActRcp THEN
   Perst.ArmRcpSt := FALSE;
END_IF;

(* Maintain Alarm Status 1.5s in Stsreg for PVSS *) 

PulseWidth := 1500 (* msec*) /DINT_TO_REAL(TIME_TO_DINT(T_CYCLE));

IF Perst.ISt OR Perst.Iinc > 0 THEN 
    Perst.Iinc := Perst.Iinc + 1; 
    Perst.WISt := TRUE; 
END_IF;
    
IF Perst.Iinc > PulseWidth OR (NOT Perst.ISt AND Perst.Iinc = 0) THEN 
    Perst.Iinc := 0; 
    Perst.WISt := Perst.ISt; 
END_IF;

(* Maintain Warning Status 1.5s in Stsreg for PVSS *) 

IF Perst.WSt OR Perst.Winc > 0 THEN 
    Perst.Winc := Perst.Winc + 1; 
    Perst.WWSt := TRUE; 
END_IF;
    
IF Perst.Winc > PulseWidth OR (NOT Perst.WSt AND Perst.Winc = 0) THEN 
    Perst.Winc := 0; 
    Perst.WWSt := Perst.WSt; 
END_IF;

(* Maintain HH Status 1.5s in Stsreg for PVSS *) 

IF Perst.HHAlSt OR Perst.HHinc > 0 THEN 
    Perst.HHinc := Perst.HHinc + 1; 
    Perst.WHHAlSt := TRUE; 
END_IF;
    
IF Perst.HHinc > PulseWidth OR (NOT Perst.HHAlSt AND Perst.HHinc = 0) THEN 
    Perst.HHinc := 0; 
    Perst.WHHAlSt := Perst.HHAlSt; 
END_IF;

(* Maintain H Status 1.5s in Stsreg for PVSS *) 

IF Perst.HWSt OR Perst.Hinc > 0 THEN 
    Perst.Hinc := Perst.Hinc + 1; 
    Perst.WHWSt := TRUE; 
END_IF;
    
IF Perst.Hinc > PulseWidth OR (NOT Perst.HWSt AND Perst.Hinc = 0) THEN 
    Perst.Hinc := 0; 
    Perst.WHWSt := Perst.HWSt; 
END_IF;

(* Maintain L Status 1.5s in Stsreg for PVSS *) 

IF Perst.LWSt OR Perst.Linc > 0 THEN 
    Perst.Linc := Perst.Linc + 1; 
    Perst.WLWSt := TRUE; 
END_IF;
    
IF Perst.Linc > PulseWidth OR (NOT Perst.LWSt AND Perst.Linc = 0) THEN 
    Perst.Linc := 0; 
    Perst.WLWSt := Perst.LWSt; 
END_IF;

(* Maintain LL Status 1.5s in Stsreg for PVSS *) 

IF Perst.LLAlSt OR Perst.LLinc > 0 THEN 
    Perst.LLinc := Perst.LLinc + 1; 
    Perst.WLLAlSt := TRUE; 
END_IF;
    
IF Perst.LLinc > PulseWidth OR (NOT Perst.LLAlSt AND Perst.LLinc = 0) THEN 
    Perst.LLinc := 0; 
    Perst.WLLAlSt := Perst.LLAlSt; 
END_IF;

(* STATUS REGISTER *)   
       
    StsReg01b[8]  := Perst.WISt;                  //StsReg01 Bit 00
    StsReg01b[9]  := Perst.WWSt;                  //StsReg01 Bit 01
    StsReg01b[10] := 0;                           //StsReg01 Bit 02
    StsReg01b[11] := Perst.ArmRcpSt;              //StsReg01 Bit 03
    StsReg01b[12] := 0;                           //StsReg01 Bit 04
    StsReg01b[13] := ConfigW;                     //StsReg01 Bit 05
    StsReg01b[14] := Perst.IOErrorW;              //StsReg01 Bit 06
    StsReg01b[15] := Perst.IOSimuW;               //StsReg01 Bit 07
    StsReg01b[0]  := PosHHW;                      //StsReg01 Bit 08
    StsReg01b[1]  := PosHW;                       //StsReg01 Bit 09
    StsReg01b[2]  := PosLW;                       //StsReg01 Bit 10
    StsReg01b[3]  := PosLLW;                      //StsReg01 Bit 11
    StsReg01b[4]  := Perst.AlUnAck;               //StsReg01 Bit 12
    StsReg01b[5]  := 0;                           //StsReg01 Bit 13
    StsReg01b[6]  := Perst.MAlBRSt;               //StsReg01 Bit 14
    StsReg01b[7]  := Perst.AuIhMB;                //StsReg01 Bit 15

    StsReg02b[8]  := Perst.EHHSt;                 //StsReg02 Bit 00
    StsReg02b[9]  := Perst.EHSt;                  //StsReg02 Bit 01
    StsReg02b[10] := Perst.ELSt;                  //StsReg02 Bit 02
    StsReg02b[11] := Perst.ELLSt;                 //StsReg02 Bit 03
    StsReg02b[12] := Perst.WHHAlSt;                //StsReg02 Bit 04
    StsReg02b[13] := Perst.WHWSt;                  //StsReg02 Bit 05
    StsReg02b[14] := Perst.WLWSt;                  //StsReg02 Bit 06
    StsReg02b[15] := Perst.WLLAlSt;                //StsReg02 Bit 07
    StsReg02b[0]  := IhMHHSt;                     //StsReg02 Bit 08
    StsReg02b[1]  := IhMHSt;                      //StsReg02 Bit 09
    StsReg02b[2]  := IhMLSt;                      //StsReg02 Bit 10
    StsReg02b[3]  := IhMLLSt;                     //StsReg02 Bit 11
    StsReg02b[4]  := 0;                           //StsReg02 Bit 12
    StsReg02b[5]  := 0;                           //StsReg02 Bit 13
    StsReg02b[6]  := 0;                           //StsReg02 Bit 14
    StsReg02b[7]  := 0;                           //StsReg02 Bit 15
    
(* Edges *)
DETECT_EDGE(new:=Perst.AlUnAck,old:=Perst.AlUnAck_old,re:=RE_AlUnAck,fe:=FE_AlUnAck);
  
(* Values back to temporary variables*)
StsReg01  := TempStsReg01;
StsReg02  := TempStsReg02; 
Perst.PAA := TempPAA;
  
END_FUNCTION_BLOCK
