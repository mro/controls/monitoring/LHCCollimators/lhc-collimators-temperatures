ALTER TABLE FW_SYS_STAT_PVSS_PROJECT add CREATED_BY varchar2(128);

ALTER TABLE FW_SYS_STAT_COMPUTER RENAME COLUMN "COMMENT" TO COMMENT_TEXT;
ALTER TABLE FW_SYS_STAT_PVSS_PROJECT RENAME COLUMN "COMMENT" TO COMMENT_TEXT;
ALTER TABLE FW_SYS_STAT_WCCOA_APPLICATIONS RENAME COLUMN "COMMENT" TO COMMENT_TEXT;
----
------Pkg GMOON defining procedures to instantiate project through the APEX application:

CREATE OR REPLACE package FW_SYS_STAT_SAS_PKG as 
  PROCEDURE CREATE_PROJECT(PROJ_NAME IN VARCHAR2, 
                         SYSTEM_NAME IN VARCHAR2, 
                         SYSTEM_NUMBER  IN NUMBER, 
                         PMON_PORT IN NUMBER,
                         DATA_PORT IN NUMBER, 
                         EVENT_PORT IN NUMBER,
                         DIST_PORT IN NUMBER,
                         REDU_PORT IN NUMBER, 
                         SPLIT_PORT IN NUMBER,
                         HOSTNAME IN VARCHAR2,
                         REDU_HOSTNAME IN VARCHAR2,
                         IS_REDUNDANT IN BOOLEAN,
                         REDU_PEER IN NUMBER,
                         RESPONSIBLE IN VARCHAR2,
                         PVSS_VERSION IN VARCHAR2,
						 CREATED_BY IN VARCHAR2);

  FUNCTION GET_OR_CREATE_COMPUTER(COMPUTER_HOSTNAME IN VARCHAR2) RETURN NUMBER;
  
  PROCEDURE UPDATE_PROJECT_INFO(PID IN NUMERIC, 
                                RESPONSIBLE IN VARCHAR2, 
                                STATUS IN VARCHAR2, 
                                COMMENT_TEXT IN VARCHAR2,
                                WCCOA_URL IN VARCHAR2,
                                INFO_URL IN VARCHAR2,
                                APP_DOMAIN IN VARCHAR2,
                                SERVICE_ACCOUNT IN VARCHAR2);
end FW_SYS_STAT_SAS_PKG;

CREATE OR REPLACE package body FW_SYS_STAT_SAS_PKG as

PROCEDURE CREATE_PROJECT(PROJ_NAME IN VARCHAR2, 
                         SYSTEM_NAME IN VARCHAR2, 
                         SYSTEM_NUMBER  IN NUMBER, 
                         PMON_PORT IN NUMBER,
                         DATA_PORT IN NUMBER, 
                         EVENT_PORT IN NUMBER,
                         DIST_PORT IN NUMBER,
                         REDU_PORT IN NUMBER, 
                         SPLIT_PORT IN NUMBER,
                         HOSTNAME IN VARCHAR2,
                         REDU_HOSTNAME IN VARCHAR2,
                         IS_REDUNDANT IN BOOLEAN,
                         REDU_PEER IN NUMBER,
                         RESPONSIBLE IN VARCHAR2,
                         PVSS_VERSION IN VARCHAR2,
						 CREATED_BY IN VARCHAR2) AS
  system_id  number;
  computer_id number := 1;
  redu_computer_id number := 1;
  project_id number;
tmp varchar2(1000);
BEGIN
    commit;
    SET TRANSACTION NAME 'new_project';
    insert into fw_sys_stat_pvss_system(id, system_name, system_number, data_port, event_port, dist_port, valid_from, redu_port, split_port)
    values (FW_SYS_STAT_PVSS_SYSTEM_SQ.NEXTVAL, SYSTEM_NAME, SYSTEM_NUMBER, DATA_PORT, EVENT_PORT, DIST_PORT, CURRENT_DATE, REDU_PORT, SPLIT_PORT)
    returning id into system_id;

    computer_id := GET_OR_CREATE_COMPUTER(HOSTNAME);
    IF IS_REDUNDANT THEN
        redu_computer_id := GET_OR_CREATE_COMPUTER(REDU_HOSTNAME);
    ELSE
        redu_computer_id := computer_id;
    END IF;

    insert into fw_sys_stat_pvss_project (id, computer_id, project_name, pmon_port, system_id, redu_computer_id, responsible, pvss_version, CREATED_BY, STATUS)
    values (FW_SYS_STAT_PVSS_PROJECT_SQ.NEXTVAL, computer_id, PROJ_NAME, PMON_PORT, system_id, redu_computer_id, RESPONSIBLE, PVSS_VERSION, CREATED_BY, 'CREATION_PENDING')
    returning id into project_id;

    commit;
EXCEPTION
WHEN OTHERS THEN
    tmp := 'some error, nothing should be written in db ' || SQLCODE || ' -ERROR- ' || SQLERRM;
  rollback;
  RAISE;
END CREATE_PROJECT;

FUNCTION GET_OR_CREATE_COMPUTER(COMPUTER_HOSTNAME IN VARCHAR2) RETURN NUMBER AS
    computer_count number := 0;
    computer_id number := 0;
BEGIN
    select count(id) into computer_count from fw_sys_stat_computer computer where upper(computer.hostname) = upper(COMPUTER_HOSTNAME) and valid_until is null;
    IF computer_count = 0 THEN
        insert into fw_sys_stat_computer (id, hostname) values (fw_sys_stat_computer_sq.nextval, upper(COMPUTER_HOSTNAME))
        returning id into computer_id;
    ELSE
        select id into computer_id from fw_sys_stat_computer computer where upper(computer.hostname) = upper(COMPUTER_HOSTNAME) and valid_until is null;
    END IF;
    RETURN computer_id;
END;

PROCEDURE UPDATE_PROJECT_INFO(PID IN NUMERIC, 
                              RESPONSIBLE IN VARCHAR2, 
                              STATUS IN VARCHAR2, 
                              COMMENT_TEXT IN VARCHAR2,
                              WCCOA_URL IN VARCHAR2,
                              INFO_URL IN VARCHAR2,
                              APP_DOMAIN IN VARCHAR2,
                              SERVICE_ACCOUNT IN VARCHAR2) AS
BEGIN
    update fw_sys_stat_pvss_project
    set responsible = RESPONSIBLE,
        status = STATUS,
        comment_text = COMMENT_TEXT,
        wccoa_url = WCCOA_URL,
        info_url = INFO_URL,
        application_domain = APP_DOMAIN,
        service_account = SERVICE_ACCOUNT
    where
        id = PID;
END;

end FW_SYS_STAT_SAS_PKG;

------------------end of FW_SYS_STAT_SAS_PKG 

--Upgrade Schema version
CREATE OR REPLACE VIEW fw_sys_stat_schema (version) AS
	SELECT '5.1.3'
	FROM dual;
COMMIT;
