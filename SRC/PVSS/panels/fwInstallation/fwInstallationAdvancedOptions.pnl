V 12
2
LANG:1 0 
LANG:0 8 (NoName)
PANEL,-1 -1 550 741 N "_3DFace" 0
"/**

fwInstallationAdvandedOptions.pnl

This panel is used for creating the new installation directory. 
If the directory does not exist - it is created and added to the project config file. 
The user can also change the installation directory to one of the directories contained in the proj_path of a config file. 
The panel also displays the information about the current installation directory.

Required $-parameters:

none

Panels called from this panel:

none

*/
main()
{
	
  dyn_string proj_paths;
  int i;
  string destinationDir;
  int dpGetResult;
  string dp = fwInstallation_getInstallationDp();
  

    
  if(dpExists(dp + \".installationDirectoryPath\"))
  {
    dpGetResult = dpGet(dp + \".installationDirectoryPath:_original.._value\", destinationDir);
    fwInstallation_getProjPaths(proj_paths);
    for(i = 1; i <= dynlen(proj_paths); i++)
    {
	sliProjPaths.appendItem(proj_paths[i]);
    }
    sliProjPaths.selectedText = destinationDir;
    dpConnect(\"fwInstallation_changeTxtDestinationDir\", dp + \".installationDirectoryPath:_original.._value\");
  }
  
    
  initDbDisplay();

  initReduncancyDisplay();
  
  //show symbol for OWS synchronization if UNICOS is installed:
  if(isFunctionDefined(\"unConfigGenericFunctions_Create_Config_ows\"))
  {
    addSymbol(myModuleName(), myPanelName(), \"fwInstallation/fwInstallation_unicosOWS.pnl\", \"UNICOS OWS\", makeDynString(), 10, 620, 0, 1, 1);
  }

}

 
fwInstallation_changeTxtDestinationDir(string dp1, string destinationDir)
{
 	sliProjPaths.selectedText = destinationDir;
}











" 0
 E E E E 1 -1 -1 0  28 56
""0  1
E "#uses \"fwInstallation.ctl\"
#uses \"fwInstallationDB.ctl\"

bool g_previousUseDB;

void initReduncancyDisplay()
{
  bool enabled = false;
  bool isReduncantProject = fwInstallationRedu_isRedundant();
  if(isReduncantProject)
  {
    // we shouldn't use DB or we should use it but the project is not centrally managed
    enabled = (!fwInstallationDB_getUseDB() || !fwInstallationDB_getCentrallyManaged());
  } 
  
  showRedundancySettings(enabled);
  
  rbReduProjInst.number = fwInstallation_getInstallOnlyInSplit();
}

void showRedundancySettings(bool bState)
{
  rbReduProjInst.visible(bState);
  installComponentsLabel.visible(bState);
  reduFrame.visible(bState);
  noReduBg.visible(!bState);
  noReduText.visible(!bState);
}

void initDbDisplay()
{
  int projectId;
  int projectRegistered = true; 
  dyn_string ds;
  dyn_float df;   
  string schemaVersion;
  int restartProject = 0;
  string dpa = fwInstallation_getAgentDp();
  
  if(!fwInstallationDB_getUseDB())
  {
    btnUseDB.enabled = true;
    btnUseDB.text = \"Connect\";
    dbConnectedRectangle.backCol(\"red\");
    dbConnectedLabel.text = \"Disconnected\";
    rbManagement.enabled = false;
    managementModeLabel.enabled(false);
    return;
  } 
  else
  {
    if(fwInstallationDB_connect() != 0)
    {
      ChildPanelOnCentralModal(\"vision/MessageInfo1\", \"DB ERROR\",makeDynString(\"$1:Could not connect to DB. Check log-viewer for error details.\"));
      fwInstallationDB_setUseDB(FALSE);
      btnUseDB.enabled = false;
      rbManagement.enabled = false;
      managementModeLabel.enabled(false);
      return;
    }
  }
    
  string msg = \"\";
  if(!fwInstallationDB_compareSchemaVersion()) 
  {
    fwInstallationDB_getSchemaVersion(schemaVersion);
    if(schemaVersion == \"\")
      msg = \"Could not read schema version from the DB\";
    else
      msg = \"Invalid DB schema version: \" + schemaVersion + \". This tool requires version \" + FW_INSTALLATION_DB_REQUIRED_SCHEMA_VERSION + \" or higher.\";  
            
    ChildPanelOnCentral(\"vision/MessageInfo1\", \"DB ERROR\", makeDynString(\"$1:\" + msg)); 
    fwInstallationDB_setUseDB(FALSE);
    btnUseDB.enabled = false;
    rbManagement.enabled = false;
    managementModeLabel.enabled(false);
    return;
  }
 
  dpGet(dpa + \".db.useDB\", g_previousUseDB);;
  dpConnect(\"setUseDBCB\", dpa + \".db.useDB\"); 
  
  if(fwInstallationDB_getUseDB())
  {
    //Check if the project is properly registered in the DB:
    if(fwInstallationDB_isProjectRegistered(projectId) != 0)
    {
      ChildPanelOnCentral(\"vision/MessageInfo1\", \"DB ERROR\", makeDynString(\"$1:Could not connect to DB.\\nCheck connection parameters.\"));
    }
   
    if(projectId <= 0)
    {
      //check whether the project registration is enabled in the DB
      int regEnabled;
      fwInstallationDB_getProjectAutoregistration(regEnabled);
      if (regEnabled == 1)
      {
        ChildPanelOnCentralReturn(\"fwInstallation/fwInstallation_messageInfo.pnl\", 
                                  \"Project Registration ...\", 
                                  makeDynString(\"$text:Project \" + PROJ + \" not registered in DB.\\nDo you want to register it now?\"), 
                                  df, ds);
        if(dynlen(df) && df[1] >= 1.)
        {
          openProgressBar(\"FW Component Installation\", \"copy.gif\", \"Exporting project configuration to DB. This may take a few seconds...\",\"\", \"Please wait\", 1); 
          if(fwInstallationDBAgent_synchronize(restartProject) != 0)
          {
            ChildPanelOnCentral(\"vision/MessageInfo1\", \"ERROR\", makeDynString(\"$1:Could not export project configuration to DB.\\nCheck DB connection.\"));
            projectRegistered = false;
          }
          else
          {
            ChildPanelOnCentral(\"vision/MessageInfo1\", \"ERROR\", makeDynString(\"$1:Project configuration successfully exported to DB.\"));
            projectRegistered = true; 
          }
          closeProgressBar();         
        }
        else
        {
          projectRegistered = false; 
        }
      }
    }
    
    if(projectRegistered)
    {
      rbManagement.enabled = true;
      managementModeLabel.enabled(true);
      rbManagement.number(fwInstallationDB_getCentrallyManaged());
    }
    else
    {
      managementModeLabel.enabled(false);
      rbManagement.enabled = false;
    }
    
    btnUseDB.toggleState(TRUE);
    btnUseDB.text = \"Disconnect\";
    dbConnectedRectangle.backCol(\"green\");
    dbConnectedLabel.text = \"Connected\";
  } 

  if(restartProject)
    ChildPanelOnCentralModal(\"fwInstallation/fwInstallation_projectRestart.pnl\", \"Project restart required\", makeDynString(\"\"));
  
}

void setUseDBCB(string dpe, bool useDB)
{
  if(useDB)
  {   
    btnUseDB.text	= \"Disconnect\";
    dbConnectedRectangle.backCol(\"green\");
    dbConnectedLabel.text = \"Connected\";
  }
  else
  {    
    btnUseDB.text	= \"Connect\";
    dbConnectedRectangle.backCol(\"red\");
    dbConnectedLabel.text = \"Disconnected\";
  }
  
  rbManagement.enabled(useDB);
  managementModeLabel.enabled(useDB);
}









" 0
 2
"CBRef" "1"
"EClose" E
""
NC
DISPLAY_LAYER, 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0
LAYER, 0 
1
LANG:1 6 Layer1
19 48
"rbReduProjInst"
""
1 28 550 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
39 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  26 548 272 601
2
T 
1
LANG:1 19 Only in active peer

1 
1
LANG:1 0 
E E
0 0 0 0 0
T 
1
LANG:1 26 Only in split passive peer

0 
1
LANG:1 0 
E E
0 0 0 0 0
1
E E
2 3
"newPathLabel"
""
1 19 208.9999999999999 E E E 1 E 1 E N {0,0,0} E N "_Transparent" E E
 E E
7 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:0 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 21 184.9999999999999 147 201
0 2 0 "0s" 0 0 0 192 0 0  21 184.9999999999999 1
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 2
LANG:1 21 Add new project path:
LANG:0 60 Select the new installation directory ( it will be created )
2 11
"txtWarning"
""
1 31 259.9999999999999 E E E 1 E 0 E N {255,0,51} E N "_Transparent" E E
 E E
8 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:0 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 33 236.9999999999999 138 253
0 2 0 "0s" 0 0 0 192 0 0  33 236.9999999999999 1
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 2
LANG:1 15 Warning message
LANG:0 15 Warning message
30 15
"installDirectoryFrame"
""
1 10 411 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
11 0 0 0 0 0
E E E
1
2
LANG:1 0 
LANG:0 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E 1.233100233100235 0 1.349397590361446 -2.331002331002338 -289.2530120481928 0 E 10 244 440 411
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 2
LANG:1 22 Installation directory
LANG:0 29 Choose Installation directory
30 30
"configDBFrame"
""
1 10 521 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
17 0 0 0 0 0
E E E
1
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E 1.233100233100235 0 0.9595959595959598 -2.331002331002345 -128.9898989898992 0 E 10 421 440 521
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 1
LANG:1 23 System Configuration DB
13 22
"btnCancelAll"
""
1 463.9999999999999 690 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
1 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:0 0 

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  462 688 540 716

T 
2
LANG:1 6 Cancel
LANG:0 6 Cancel
"main()
{
  	PanelOffReturn(makeDynFloat(0.), makeDynString(\"Cancel\"));	

}" 0
 E E E
13 21
"btnOKAll"
""
1 382 690 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
2 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:0 0 

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  380 688 458 716

T 
2
LANG:1 5 Apply
LANG:0 2 OK
"main()
{
	dyn_string ds;
	dyn_float df;
  string dp = fwInstallation_getInstallationDp();
	
  // save installation directory
  string destinationDir = sliProjPaths.selectedText();
  string dp = fwInstallation_getInstallationDp();
  
	dpSet(dp + \".installationDirectoryPath:_original.._value\", destinationDir);
  
	fwInstallation_changeProjPaths(sliProjPaths.items);	
	dpSetWait(dp + \".addManagersDisabled\", !addManagerEnabled.state(0),
                  dp + \".activateManagersDisabled\", !activateManagerEnabled.state(0));

  if(rbReduProjInst.visible)
  {
    int installOnlyInSplit = fwInstallation_getInstallOnlyInSplit();
    int newInstallOnlyInSplit = rbReduProjInst.number;
    if (installOnlyInSplit != newInstallOnlyInSplit)
    {
      fwInstallation_setInstallOnlyInSplit(newInstallOnlyInSplit);
    }
  }
}

" 0
 E E E
14 10
"txtDestinationDir"
""
1 21 204.9999999999999 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
3 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:0 0 

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  19 203 419 229
3 "0s" 0 0 0 0 0 -1  E E E
13 9
"btnSelectSourceDir"
""
1 421 130.9999999999999 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
4 0 0 0 0 0
E E E
0
2
LANG:1 20 Select the directory
LANG:0 20 Select the directory

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  419 201 448 230

P 
14215660
"pictures/StandardIcons/Open_20.png"
1
LANG:1 0 
"main()
{

string sPath;

int iErr;

	txtWarning.visible = false;
  warningArrow.visible = false;
		
	sPath = txtDestinationDir.text;   // Content of textfield

	iErr = folderSelector( sPath );

	if(( iErr >= 1 ) && ( strlen( sPath ) > 0 ))
	{

	    if (_WIN32) 
	    	strreplace( sPath, \"/\", \"//\" ); 
		// if NT -> replace 
	
		strreplace( sPath, \"//\", \"/\" );
	
	    txtDestinationDir.text = sPath;  

  }
  
}
" 0
 E E E
13 5
"btnOK"
""
1 454 131.9999999999999 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
5 0 0 0 0 0
E E E
0
2
LANG:1 48 Adds path and creates new directory if necessary
LANG:0 0 

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  452 202 530 230

T 
2
LANG:1 3 Add
LANG:0 5 Apply
"main()
{
  string path = txtDestinationDir.text;
  
  if(path == \"\")
    return;

	if(fwInstallation_createPath(path))
  {
     txtWarning.text = \"Failed to register the new project path. Check log-viewer for details.\";
     txtWarning.visible = true;
     warningArrow.visible = true;
     return;
  }
  
  txtWarning.visible = false;
  warningArrow.visible = false;
  dyn_string projPaths = sliProjPaths.items;
  dynInsertAt(projPaths, path,1);
  sliProjPaths.items = projPaths;
  sliProjPaths.selectedPos = 1;
}







" 0
 E E E
20 27
"addManagerEnabled"
""
1 272 516.9999999999993 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
17 0 0 0 0 0
E E E
0
1
LANG:1 168 WARNING! If you disable the automatic addition of managers, component might not work correctly. This function is intended for NON-STANDARD JCOP Framework projects only!

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  280 421 434 444
1
T 
1
LANG:1 12 Add managers

0 
1
LANG:1 0 
E E
0 0 0 0 0
1
E E
"main()
{
	bool disabled;
        string dp = fwInstallation_getInstallationDp();
        
	dpGet(dp + \".addManagersDisabled\", disabled);

	this.state(0) = !disabled;
}
" 0
"main(int button, bool state)
{
	if(!state)
		activateManagerEnabled.state(0) = FALSE;
}" 0
17 23
"sliProjPaths"
""
1 20.99999999999999 81.99999999999994 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
13 0 0 0 0 0
E E E
0
2
LANG:1 39 Define order of different project paths
LANG:0 0 

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  19 80 501 181
0

E
E
E

0 0
13 24
"upButton"
""
1 503 80.99999999999994 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
14 0 0 0 0 0
E E E
0
2
LANG:1 21 Move selected path up
LANG:0 0 

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  501 79 530 108

P 
10862530
"pictures/StandardIcons/arrow_top_20.png"
1
LANG:1 0 
"main()
{
	dyn_string listStrings = sliProjPaths.items;
	string selectedString = sliProjPaths.selectedText;
	int selectedPosition = sliProjPaths.selectedPos;
  string strPath = selectedString;
  string strProjPath = PROJ_PATH;
        
  strreplace(strPath, \"\\\\\", \"\");
  strreplace(strPath, \"/\", \"\");
        
  strreplace(strProjPath, \"\\\\\", \"\");
  strreplace(strProjPath, \"/\", \"\");
        
  strPath = strtolower(strPath);
  strProjPath = strtolower(strProjPath);
        
  if(strPath == strProjPath)
  {
    ChildPanelOnCentralModal(\"vision/MessageInfo1\", \"Action not permitted\", makeDynString(\"This project path cannot be moved.\"));
    return;
  }
	
	if(selectedPosition != 1)
	{
		dynRemove(listStrings, selectedPosition);
		dynInsertAt(listStrings, selectedString, selectedPosition - 1);
	}	

	sliProjPaths.items = listStrings;
	sliProjPaths.selectedText = selectedString;

}" 0
 E E E
13 26
"downButton"
""
1 503 154.9999999999999 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
15 0 0 0 0 0
E E E
0
2
LANG:1 23 Move selected path down
LANG:0 0 

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  501 153 530 182

P 
10862530
"pictures/StandardIcons/arrow_bottom_20.png"
1
LANG:1 0 
"main()
{
	dyn_string listStrings = sliProjPaths.items;
	string selectedString = sliProjPaths.selectedText;
	int selectedPosition = sliProjPaths.selectedPos;
	string strPath = selectedString;
        string strProjPath = PROJ_PATH;
        
        strreplace(strPath, \"\\\\\", \"\");
        strreplace(strPath, \"/\", \"\");
        
        strreplace(strProjPath, \"\\\\\", \"\");
        strreplace(strProjPath, \"/\", \"\");
        
        strPath = strtolower(strPath);
        strProjPath = strtolower(strProjPath);
        
        if(strPath == strProjPath)
        {
          ChildPanelOnCentralModal(\"vision/MessageInfo1\", \"Action not permitted\", makeDynString(\"This project path cannot be moved.\"));
          return;
        }

	if(selectedPosition != dynlen(listStrings))
	{
		dynRemove(listStrings, selectedPosition);
		dynInsertAt(listStrings, selectedString, selectedPosition + 1);
	}	

	sliProjPaths.items = listStrings;
	sliProjPaths.selectedText = selectedString;

}" 0
 E E E
20 29
"activateManagerEnabled"
""
1 275.333333333333 975.9999999999993 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
17 0 0 0 0 0
E E E
0
1
LANG:1 168 WARNING! If you disable the automatic addition of managers, component might not work correctly. This function is intended for NON-STANDARD JCOP Framework projects only!

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  280 444 434 467
1
T 
1
LANG:1 17 Activate managers

0 
1
LANG:1 0 
E E
0 0 0 0 0
1
E E
"main()
{
	bool disabled;
        string dp = fwInstallation_getInstallationDp();
        
	dpGet(dp + \".activateManagersDisabled\", disabled);

	this.state(0) = !disabled;
}
" 0
E13 34
"btnUseDB"
""
1 30.99999999999999 384.9999999999992 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
21 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  19 293 142 321

T 
1
LANG:1 10 Disconnect
"main()
{
  bool useDB = fwInstallationDB_getUseDB();
  bool newUseDB = !useDB;
  dyn_string ds;
  dyn_float df;

  fwInstallationDB_setUseDB(newUseDB); 
  
  if(newUseDB){
    fwInstallationDB_connect();     
    initDbDisplay();
  }
}" 0
 E E E
19 38
"rbManagement"
""
1 240 372.9999999999992 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
25 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  280 312 458 364
2
T 
1
LANG:1 26 Local (WinCC OA is master)

1 
1
LANG:1 0 
E E
0 0 0 0 0
T 
1
LANG:1 22 Central (DB is master)

0 
1
LANG:1 0 
E E
0 0 0 0 0
1
E "main(int button)
{
  dyn_float df;
  dyn_string ds;
  string msg;
  dyn_int status;
  
  if(button == 0)
    msg = \"Changing management mode to \\\"Local\\\" overwrites \\nDB content. Proceed?\"; 
  else
    msg = \"Changing management mode to \\\"Central\\\" overwrites \\nproject content. Proceed?\";

  ChildPanelOnCentralReturn(\"fwInstallation/fwInstallation_messageInfo.pnl\", \"Change Mode ...\", makeDynString(\"$text:\" + msg), df, ds);
  if(dynlen(df) && df[1] > 0.){
    fwInstallationDB_setCentrallyManaged(button);  
    fwInstallationDBAgent_checkIntegrity(status);  
  }
  else
    if(button == 0)
      this.number(1);
    else
      this.number = 0;
  
}

" 0

13 40
"connectDbButton"
""
1 21 394.9999999999992 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
28 0 0 0 0 0
E E E
0
1
LANG:1 0 

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  19 333 142 361

T 
1
LANG:1 12 Setup DB ...
"main()
{
  dyn_float df;
  dyn_string ds;
  
  ChildPanelOnCentralModalReturn(\"fwInstallation/fwInstallationDB_connectionSetup.pnl\", \"DB Connection Setup\",makeDynString(\"\"), df, ds);

  
  if(dynlen(df) && df[1] >= 1.)
  {
    initDbDisplay(); 
  }
  
}" 0
 E E E
20 41
"cbDist"
""
1 36.99999999999999 519.9999999999993 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
30 0 0 0 0 0
E E E
0
1
LANG:1 168 WARNING! If you disable the automatic addition of managers, component might not work correctly. This function is intended for NON-STANDARD JCOP Framework projects only!

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  26 421 90 444
1
T 
1
LANG:1 4 Dist

0 
1
LANG:1 0 
E E
0 0 0 0 0
1
E E
"main()
{
	int state;
        string dp = fwInstallation_getAgentDp();
	dpGet(dp + \".managers.stopDist\", state);
	
	this.state(0) = state;
}" 0
"main(int button, bool state)
{
    string dp = fwInstallation_getAgentDp();
    dpSet(dp + \".managers.stopDist\", state);

}" 0
20 42
"uiManagerCheckbox"
""
1 36.99999999999999 542.9999999999993 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
32 0 0 0 0 0
E E E
0
1
LANG:1 168 WARNING! If you disable the automatic addition of managers, component might not work correctly. This function is intended for NON-STANDARD JCOP Framework projects only!

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  26 444 70 467
1
T 
1
LANG:1 3 UIs

0 
1
LANG:1 0 
E E
0 0 0 0 0
1
E E
"main()
{
	int state;
        string dp = fwInstallation_getAgentDp();
	dpGet(dp + \".managers.stopUIs\", state);
	
	this.state(0) = state;

}" 0
"main(int button, bool state)
{
      string dp = fwInstallation_getAgentDp();
      dpSet(dp + \".managers.stopUIs\", state);

}" 0
20 43
"ctrlManagerCheckbox"
""
1 36.99999999999999 565.9999999999993 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
34 0 0 0 0 0
E E E
0
1
LANG:1 168 WARNING! If you disable the automatic addition of managers, component might not work correctly. This function is intended for NON-STANDARD JCOP Framework projects only!

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  26 467 80 490
1
T 
1
LANG:1 4 Ctrl

0 
1
LANG:1 0 
E E
0 0 0 0 0
1
E E
"main()
{
	int state;
        string dp = fwInstallation_getAgentDp();
	dpGet(dp + \".managers.stopCtrl\", state);
	
	this.state(0) = state;

}" 0
"main(int button, bool state)
{
      string dp = fwInstallation_getAgentDp();
      dpSet(dp + \".managers.stopCtrl\", state);

}" 0
30 44
"managersFrame"
""
1 10 521 E E E 1 E 1 E N "_3DText" E N "_Transparent" E E
 E E
35 0 0 0 0 0
E E E
1
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E 1.233100233100235 0 1.171717171717171 -2.331002331002341 -112.2929292929289 0 E 10 421 440 521
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 1
LANG:1 8 Managers
30 46
"reduFrame"
""
1 10 564 E E E 1 E 1 E N "_WindowText" E N {0,0,0} E E
 E E
37 0 0 0 0 0
E E E
1
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 0 1 0 1 E 1.037254901960784 0 1.714285714285718 -0.3725490196078404 -458.8571428571453 0 E 10 564 521 621
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 1
LANG:1 10 Redundancy
2 49
"directoryLabel"
""
1 21 61.99999999999994 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
40 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  0 E 21 61.99999999999994 258 78
0 2 2 "0s" 0 0 0 192 0 0  21 61.99999999999994 1
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 1
LANG:1 42 Select directory to install components in:
10 50
"warningArrow"
""
1 10 770 E E E 1 E 0 E N "red" E N "red" E E
 E E
41 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:10001 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E 0.7259259259259259 0 0.6825000000000002 13.74074074074074 -287.5250000000003 1 E 5 10 770
10 790
20 780
10 770
10 790
 1
6 51
"dbConnectedRectangle"
""
1 270 310 E E E 1 E 1 E N "_Transparent" E N "red" E E
 E E
42 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:10001 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E 1 0 1 129 8.999999999999883 1 E 20 290 35 305
2 52
"dbConnectedLabel"
""
1 168 299 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
43 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:10001 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  0 E 168 299 230 315
0 2 2 "0s" 0 0 0 192 0 0  168 299 1
2
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
LANG:10001 25 Arial,8,-1,5,50,0,0,0,0,0
0 2
LANG:1 9 Connected
LANG:10001 64 Component Instalaltion will be skipped as XML file is not found.
2 53
"managementModeLabel"
""
1 275 296.9999999999998 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
44 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  0 E 275 296.9999999999998 311 313
0 2 2 "0s" 0 0 0 192 0 0  275 296.9999999999998 1
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 1
LANG:1 5 Mode:
2 54
"stopManagerLabel"
""
1 21 403.0000000000002 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
45 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  0 E 21 403.0000000000002 157 419
0 2 2 "0s" 0 0 0 192 0 0  21 403.0000000000002 1
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 1
LANG:1 25 Stop during installation:
2 55
"automaticLabel"
""
1 275 403.0000000000002 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
46 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  0 E 275 403.0000000000002 358 419
0 2 2 "0s" 0 0 0 192 0 0  275 403.0000000000002 1
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 1
LANG:1 14 Automatically:
2 56
"installComponentsLabel"
""
1 21 530 E E E 1 E 1 E N "_WindowText" E N "_Window" E E
 E E
47 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  0 E 21 530 134 546
0 2 2 "0s" 0 0 0 192 0 0  21 530 1
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0 1
LANG:1 19 Install components:
6 58
"noReduBg"
""
1 8 48 E E E 0 E 0 E N {0,0,0} E N "lightgrey" E E
 E E
49 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:10001 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E 1 0 1 0 24.99999999999994 1 E 10 483 539 579
2 59
"noReduText"
""
1 273 544 E E E 0 E 0 E N "_WindowText" E N "_Transparent" E E
 E E
50 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:10001 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 167 546 383 568
0 2 0 "0s" 0 0 0 193 0 0  275 546 1
2
LANG:1 26 Arial,14,-1,5,50,0,0,0,0,0
LANG:10001 26 Arial,14,-1,5,50,0,0,0,0,0
0 2
LANG:1 24 No redundant connection.
LANG:10001 56 Click "Add" to initialize faceplate trend for this type.
6 60
"bgRectangle"
""
1 20 530 E E E 1 E 1 E N "_Transparent" E N "FwInstallationCorporateColor" E E
 E E
51 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:10001 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 2 1 E U  1 E -1 -1 1268 29
2 61
"title"
""
1 7.999999999999986 2 E E E 1 E 1 E N "white" E N "_Transparent" E E
 E E
52 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:10001 0 

1
"dashclr"N "_Transparent"
E E 0 1 1 0 1 E U  0 E 9.999999999999986 4 166 26
0 2 0 "0s" 0 0 0 192 0 0  9.999999999999986 4 1
2
LANG:1 26 Arial,-1,19,5,50,0,0,0,0,0
LANG:10001 26 Arial,-1,19,5,40,0,0,0,0,0
0 2
LANG:1 16 Advanced Options
LANG:10001 47 JCOP Framework Installation Description Creator
13 62
"PUSH_BUTTON1"
""
1 300 690 E E E 1 E 1 E N "_ButtonText" E N "_Button" E E
 E E
53 0 0 0 0 0
E E E
0
2
LANG:1 0 
LANG:0 0 

0
1
LANG:1 26 Arial,10,-1,5,50,0,0,0,0,0
0  298 688 376 716

T 
2
LANG:1 2 OK
LANG:0 2 OK
"main()
{
	dyn_string ds;
	dyn_float df;
  string dp = fwInstallation_getInstallationDp();
	
  // save installation directory
  string destinationDir = sliProjPaths.selectedText();
  string dp = fwInstallation_getInstallationDp();
  
	dpSet(dp + \".installationDirectoryPath:_original.._value\", destinationDir);
  
	fwInstallation_changeProjPaths(sliProjPaths.items);	
	dpSetWait(dp + \".addManagersDisabled\", !addManagerEnabled.state(0),
                  dp + \".activateManagersDisabled\", !activateManagerEnabled.state(0));

  if(rbReduProjInst.visible)
  {
    int installOnlyInSplit = fwInstallation_getInstallOnlyInSplit();
    int newInstallOnlyInSplit = rbReduProjInst.number;
    if (installOnlyInSplit != newInstallOnlyInSplit)
    {
      fwInstallation_setInstallOnlyInSplit(newInstallOnlyInSplit);
    }
  }
	
  PanelOffReturn(makeDynFloat(), makeDynString(\"OK\"));	
}

" 0
 E E E
0
LAYER, 1 
1
LANG:1 6 Layer2
0
LAYER, 2 
1
LANG:1 6 Layer3
0
LAYER, 3 
1
LANG:1 6 Layer4
0
LAYER, 4 
1
LANG:1 6 Layer5
0
LAYER, 5 
1
LANG:1 6 Layer6
0
LAYER, 6 
1
LANG:1 6 Layer7
0
LAYER, 7 
1
LANG:1 6 Layer8
0
0
