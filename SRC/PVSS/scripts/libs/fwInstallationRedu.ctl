/**@file
 *
 * This library contains helper functions to address redundant systems
 *
 * @author Fernando Varela Rodriguez (EN-ICE)
 * @date   August 2010
 */

/** Version of this library.
 * Used to determine the coherency of all libraries of the installtion tool
 * @ingroup Constants
*/
const string csFwInstallationReduLibVersion = "7.1.5";
const int fwInstallationRedu_WAIT_COUNT = 10;



/** Starts control script connecting to both event manager to exit the split mode.
@param pairToKeep   peer to keep alive
*/
void fwInstallationRedu_setReduSplitOff(int pairToKeep) {
  fwInstallationRedu_execScriptConnectedToBothPeers("fwInstallationExitSplitMode.ctl",makeDynString(pairToKeep));
}

void fwInstallationRedu_execScriptConnectedToBothPeers(string script, dyn_string parameters) {
    dyn_string hosts = fwInstallationRedu_getDataHosts();
    if (dynlen(hosts) <2) {
        fwInstallation_throw("fwInstallationRedu_execScriptConnectedToBothPeers: Cannot run on a non redundant system");
        return;
    }
    
    string parameterString;
    
    fwGeneral_dynStringToString(parameters, parameterString, " " );
    parameterString = " " + parameterString;
    
    
    string firstPeer = hosts[1];
    string secondPeer = hosts[2];
    int dataP = dataPort();
    int eventP = eventPort();
    
    string cmd = PVSS_BIN_PATH + fwInstallation_getWCCOAExecutable("ctrl") +
                 " -data " + firstPeer + ":" + dataP + "$" + secondPeer + ":" + dataP 
                 + " -event "+ firstPeer + ":" + eventP + "$" + secondPeer + ":" + eventP 
                 + " -proj " + PROJ  
                 + " -num 101 " 
                 + " " +script +  parameterString;
   DebugN(cmd);
   
   system(cmd);   
}


/** Exists the split mode 
@praram sSystem    PVSS system name
@param bAsk       ask user input
@param stayHost   host that survives
*/
int fwInstallationRedu_reduSetSplitOff(string sSystem, bool bAsk=true, int stayHost = 1)
{
  dyn_string dpe;
  dyn_string ds;
  dyn_bool   value;
  dyn_float dfReturn;
  dyn_string dsReturn;
  dyn_string dsDataHosts, dsDataHosts_2; 
  bool bSplitMode, bSplitMode_2;
  int manId = convManIdToInt(EVENT_MAN,0);
  int manId_2 = convManIdToInt(EVENT_MAN,0,0,2);
  
  string h1, h2;
  
  time tExit, tExit_2;
  
  if (!getUserPermission(3))  //check user permissions
  {
    if (bAsk)
      ChildPanelOnCentral("vision/MessageWarning",getCatStr("va","headerPanelhost"),
                          makeDynString(getCatStr("general","permission"),
                                        getCatStr("general","cancel")));
    else
      throwError(makeError("", PRIO_SEVERE, ERR_SYSTEM, 0, getCatStr("va","headerPanelhost"), getCatStr("va","headerPanelhost")));
    return -1;
  }
  
  dpGet( sSystem + "_ReduManager.SplitMode:_original.._value",   bSplitMode,
         sSystem + "_ReduManager_2.SplitMode:_original.._value", bSplitMode_2);
         
  if (!(bSplitMode || bSplitMode_2))        // not in splitmode -> cancel
     return 0;
  
  dpGet(sSystem + "_Connections.Data.HostNames:_online.._value",   dsDataHosts,
        sSystem + "_Connections_2.Data.HostNames:_online.._value", dsDataHosts_2);
  
  if (dynlen(dsDataHosts) >= 1)        h1 = dsDataHosts[1];
  if (dynlen(dsDataHosts) == 2)        h2 = dsDataHosts[2];
  else if (dynlen(dsDataHosts_2) == 1) h2 = dsDataHosts_2[1];
  else if (dynlen(dsDataHosts_2) == 2) h2 = dsDataHosts_2[2];
  
  dyn_int diDistMan, diDistMan_2;
  
  
  bool bEventConnection = isConnOpen(manId);
  bool bEventConnection_2 = isConnOpen(manId_2);
//  DebugN("bEventConnection" ,bEventConnection);
//  DebugN("bEventConnection_2" ,bEventConnection_2);
  if (bEventConnection && bEventConnection_2)   // both EB running
  {
    dpGet(sSystem + "_Managers.Exit:_original.._stime", tExit,
          sSystem + "_Managers_2.Exit:_original.._stime", tExit_2); 
    
    if (tExit + 10 > getCurrentTime() || tExit_2 + 10 > getCurrentTime())
    {
      if (bAsk)
        ChildPanelOnCentral("vision/MessageWarning", getCatStr("general", "headerswitchToRedundantMode"),
                            makeDynString(getCatStr("general","notCorrectStateForSwitch"),
                                          getCatStr("general","cancel")));
      else
        throwError(makeError("", PRIO_SEVERE, ERR_SYSTEM, 0, getCatStr("general", "headerswitchToRedundantMode"), getCatStr("general","notCorrectStateForSwitch")));
      return -1;
    }    
    
    if (bAsk)
    {
      ChildPanelOnCentralModalReturn("vision/MessageInfo3",getCatStr("general", "headerswitchToRedundantMode"),
                                     makeDynString(getCatStr("general","switchToRedundantMode"),
                                                   h1, h2,
                                                   getCatStr("general","cancel")),
                                     dsReturn, dfReturn);
      
      if( dynlen(dsReturn)<1 || dsReturn[1]=="2")        // !OK
         return 0;
      else if (dsReturn[1]=="0")    // host2 stays active -> shutdown host 1
        stayHost = 2;
      else if (dsReturn[1]=="1")    // host1 stays active -> shutdown host 2
        stayHost = 1;
    }

    if (getCtrlManagerRunningScript(sSystem, "fileSync.ctl")>-1)
      fsStartSync(stayHost==1 ? 2 : 1, TRUE);
    

    bEventConnection = isConnOpen(manId);      // check ob noch immer da
    bEventConnection_2 = isConnOpen(manId_2);
    
    //Wenn nur zu einem verbunden
    if ((stayHost==2 && !bEventConnection_2) || (stayHost==1 && !bEventConnection))
    {
      if (bAsk)
        ChildPanelOnCentral("vision/MessageWarning", getCatStr("general", "headerswitchToRedundantMode"),
                            makeDynString(getCatStr("general","notCorrectStateForSwitch"),
                                          getCatStr("general","cancel")));
      else
        fwInstallation_throw(getCatStr("general", "headerswitchToRedundantMode") + getCatStr("general","notCorrectStateForSwitch"));
      return -1;
    }
    else
    {
      if ( ((stayHost == 1) && (myReduHostNum() == 2)) ||
           ((stayHost == 2) && (myReduHostNum() == 1)) )
      {
        fwInstallation_throw("Switch to REDUMode by "+ getUserName() + " on "+getHostname() + " "+sSystem, "INFO", 10);

        //Alle auf FALSE
        dynClear(dpe);
        dynAppend(dpe, ds = dpNames(sSystem + "*.MS:_original.._value",                     "_DriverCommon"));
        dynAppend(dpe, ds = dpNames(sSystem + "*.SplitMode:_original.._value",              "_ReduManager"));
        dynAppend(dpe, ds = dpNames(sSystem + "*.DC:_original.._value",                     "_DriverCommon"));
        dynAppend(dpe, ds = dpNames(sSystem + "*.Redundancy.ManualState:_original.._value", "_DistManager"));
        dynAppend(dpe, ds = dpNames(sSystem + "*.SplitActive:_original.._value",            "_ReduManager"));
  
        for (int i=1; i<=dynlen(dpe); i++)
          value[i] = FALSE;  
        dpSet(dpe, value);
      }

      if (stayHost==1)       //Reduhost 1 soll laufen bleiben Reduhost 2 neu starten
      {
         //DebugTN("dpSet",sSystem + "_Managers_2.Exit:_original.._userbit1", 1); //prepare for exit
        dpSet(sSystem + "_Managers_2.Exit:_original.._userbit1", 1); //prepare for exit
        delay(0,500);
        //DebugTN("dpSet",sSystem + "_Managers_2.Exit:_original.._userbit1", 0); //prepare for exit
        dpSet(sSystem + "_Managers_2.Exit:_original.._userbit1", 0); //prepare for exit
        //DebugTN("dpSet",sSystem + "_Managers_2.Exit:_original.._value", 256);
        dpSet(sSystem + "_Managers_2.Exit:_original.._value", 256); // exit
        // Set -1 again, to avoid inifinte loop of recovery after change back from split to redumode IM106100
        //DebugTN("dpSet",sSystem + "_Managers_2.Exit:_original.._value", -1);
        dpSet(sSystem + "_Managers_2.Exit:_original.._value", -1);
        //IM 109425
//        bRestart_2[sSystem] = TRUE;
//        setAllObjectsVisible(FALSE, "_2");
      }
      else if (stayHost==2)  //Reduhost 2 soll laufen bleiben Reduhost 1 neu starten
      {
        dpSet(sSystem + "_Managers.Exit:_original.._userbit1", 1); //prepare for exit
        delay(0,500);
        dpSet(sSystem + "_Managers.Exit:_original.._userbit1", 0); //prepare for exit
        dpSet(sSystem + "_Managers.Exit:_original.._value", 256); // exit
        // Set -1 again, to avoid inifinte loop of recovery after change back from split to redumode IM106100
        dpSet(sSystem + "_Managers.Exit:_original.._value", -1);
        //IM 109425
//        bRestart[sSystem] = TRUE;
      //  setAllObjectsVisible(FALSE, "");
      }
    }
  }
  else
  {
    if (bAsk)  //Noch mal nachfragen
    {
      ChildPanelOnCentralModalReturn("vision/MessageInfo",getCatStr("general", "headerswitchToRedundantMode"),
                                     makeDynString(getCatStr("general","switchToRedundantModeSingle"),
                                                   getCatStr("general","OK"),
                                                   getCatStr("general","cancel")),
                                     dsReturn, dfReturn);    
      
      if ( dynlen(dsReturn) < 1 || dsReturn[1] == "0" )  // !OK
        return 0;
    }
    
    if ( (bEventConnection_2 && (myReduHostNum() == 1)) ||
        (!bEventConnection_2 && (myReduHostNum() == 2)) )
    {
      fwInstallation_throw("Switch to REDUMode by "+ getUserName() + " on "+getHostname() + " "+sSystem, "INFO", 10);

      //Alle auf FALSE
      dynClear(dpe);
      dynAppend(dpe, ds = dpNames(sSystem + "*.MS:_original.._value",                     "_DriverCommon"));
      dynAppend(dpe, ds = dpNames(sSystem + "*.SplitMode:_original.._value",              "_ReduManager"));
      dynAppend(dpe, ds = dpNames(sSystem + "*.DC:_original.._value",                     "_DriverCommon"));
      dynAppend(dpe, ds = dpNames(sSystem + "*.Redundancy.ManualState:_original.._value", "_DistManager"));
      dynAppend(dpe, ds = dpNames(sSystem + "*.SplitActive:_original.._value",            "_ReduManager"));
  
      for (int i=1; i<=dynlen(dpe); i++)
        value[i] = FALSE;  
      dpSet(dpe, value);
    }

    if (bEventConnection_2)    // host2 is active -> shutdown host 1
      {
        dpSet(sSystem + "_Managers.Exit:_original.._value", 256); // exit
        // Set -1 again, to avoid inifinte loop of recovery after change back from split to redumode IM106100
        dpSet(sSystem + "_Managers.Exit:_original.._value", -1);
      }
    else
      {
        dpSet(sSystem + "_Managers_2.Exit:_original.._value", 256); // exit
        // Set -1 again, to avoid inifinte loop of recovery after change back from split to redumode IM106100
        dpSet(sSystem + "_Managers_2.Exit:_original.._value", -1);
      }
  }
  
  int manIdLeft = convManIdToInt(EVENT_MAN,0, getSystemId(), 1);
  int manIdRight = convManIdToInt(EVENT_MAN,0, getSystemId(), 2);
  int i = 0;

  while (i<200) //wait max. 2 Seconds
  {
    delay(0,10);
    if ( !isConnOpen(manIdLeft) || !isConnOpen(manIdRight) )
    {
      break;
    }
    i++;
  }
   
  fwInstallation_throw("Switch to REDUMode by "+ getUserName() + " on "+getHostname() + " "+sSystem, "INFO", 10);
  
  //Alle auf FALSE
  dynClear(dpe);
  dynAppend(dpe, ds = dpNames(sSystem + "*.MS:_original.._value",                     "_DriverCommon"));
  dynAppend(dpe, ds = dpNames(sSystem + "*.SplitMode:_original.._value",              "_ReduManager"));
  dynAppend(dpe, ds = dpNames(sSystem + "*.DC:_original.._value",                     "_DriverCommon"));
  dynAppend(dpe, ds = dpNames(sSystem + "*.Redundancy.ManualState:_original.._value", "_DistManager"));
  dynAppend(dpe, ds = dpNames(sSystem + "*.SplitActive:_original.._value",            "_ReduManager"));

  for (int i=1; i<=dynlen(dpe); i++)
    value[i] = FALSE;  
  dpSet(dpe, value);
 
  return 1;
}

/**This method resolves the list of components missing in the local peer w.r.t the remote one a
*/
int fwInstallationRedu_checkComponents(bool &isOk, dyn_dyn_mixed &componentsToBeInstalled, dyn_dyn_mixed &componentsToBeRemoved)
{
  dyn_dyn_mixed componentsInfo, reduPartnerComponentsInfo;
  
  isOk = false;
  
  fwInstallation_getInstalledComponents(componentsInfo, fwInstallationRedu_myReduHostNum());
  fwInstallation_getInstalledComponents(reduPartnerComponentsInfo, fwInstallationRedu_myReduHostNum()==1?2:1);

  //find out components to be installed:
  int k = 1;
  for(int i = 1; i <= dynlen(reduPartnerComponentsInfo); i++)
  {
    bool found = false;
    for(int j = 1; j <= dynlen(componentsInfo); j++)
    {
      if(reduPartnerComponentsInfo[i][FW_INSTALLATION_DB_COMPONENT_NAME_IDX] == componentsInfo[j][FW_INSTALLATION_DB_COMPONENT_NAME_IDX] &&
         reduPartnerComponentsInfo[i][FW_INSTALLATION_DB_COMPONENT_VERSION_IDX] == componentsInfo[j][FW_INSTALLATION_DB_COMPONENT_VERSION_IDX])
      {
        found = true;
        break;
      }
    }
    if(!found)
    {
      componentsToBeInstalled[k] = reduPartnerComponentsInfo[i];
      ++k; 
    }
  }

  //find components to be removed:
  int k = 1;
  for(int i = 1; i <= dynlen(componentsInfo); i++)
  {
    bool found = false;
    for(int j = 1; j <= dynlen(reduPartnerComponentsInfo); j++)
    {
      if(reduPartnerComponentsInfo[j][FW_INSTALLATION_DB_COMPONENT_NAME_IDX] == componentsInfo[i][FW_INSTALLATION_DB_COMPONENT_NAME_IDX] &&
         reduPartnerComponentsInfo[j][FW_INSTALLATION_DB_COMPONENT_VERSION_IDX] == componentsInfo[i][FW_INSTALLATION_DB_COMPONENT_VERSION_IDX])
      {
        found = true;
        break;
      }
    }
    if(!found)
    {
      componentsToBeRemoved[k] = componentsInfo[i];
      ++k; 
    }
  }
  
  if(dynlen(componentsToBeInstalled) == 0 &&
     dynlen(componentsToBeRemoved) == 0) isOk = true; //list of components in both partners is consistent
  
  return 0; //error code to be implemented later.
}

/**This function forces the sync of component in the local system, i.e. it removes from the local system 
   components that are not installed in the remote redu peer and installs the ones that are missing in the 
   local peer but already installed in the remote one
*/

int fwInstallationRedu_synchronize(bool deleteFiles = false, 
                                   string &dontRestartProject, 
                                   dyn_string descFilesInstallComponents = makeDynString())
{
  bool isOk = false;
  bool isSubComponent = false;
  string sourceDir = "";
  int err = 0;
  
  dyn_dyn_mixed componentsToBeInstalled;
  dyn_dyn_mixed componentsToBeRemoved;
  dyn_string componentFiles;
  dyn_string componentNamesToBeRemoved;
  
  dontRestartProject = "no";  
  fwInstallationRedu_checkComponents(isOk, componentsToBeInstalled, componentsToBeRemoved);
  
  
  if(isOk) return 0;
    
  //work to be done. Reset installation log now...
  fwInstallation_resetLog();
  fwInstallationManager_stopManagersForInstallation();
//  fwInstallation_backupProjectConfigFile();

  //Install components first
  if(dynlen(descFilesInstallComponents))
  {
DebugN("fwInstallationRedu_synchronize called forcing desc files of the components to be intalled: ",descFilesInstallComponents);    
    componentFiles = descFilesInstallComponents;
  }
  else
  {
    for(int i = 1; i <= dynlen(componentsToBeInstalled); i++)
    {
      //check that the component XML file can be accessed:
  DebugN("before checking access to xml files. components to be installed: ", componentsToBeInstalled[i]);

      if(access(componentsToBeInstalled[i][FW_INSTALLATION_DB_COMPONENT_DESC_FILE_IDX], R_OK))
      {
        fwInstallation_throw("Cannot install component as the XML file is not readable: " + componentsToBeInstalled[i][FW_INSTALLATION_DB_COMPONENT_DESC_FILE_IDX] + ". This component will be skipped", "WARNING");
        ++err;
        continue;
      }
      else
      {
        DebugN("appending", componentsToBeInstalled[i][FW_INSTALLATION_DB_COMPONENT_DESC_FILE_IDX]);
        dynAppend(componentFiles, componentsToBeInstalled[i][FW_INSTALLATION_DB_COMPONENT_DESC_FILE_IDX]); 
      }
    }
  }
  
  DebugN("Now installing the following components", componentFiles);
  if(dynlen(componentFiles)>0)
    fwInstallation_installComponentSet(componentFiles, dontRestartProject);
  
  //Remove components:
  for(int i =1; i <= dynlen(componentsToBeRemoved); i++)
  {
    dynAppend(componentNamesToBeRemoved, componentsToBeRemoved[i][FW_INSTALLATION_DB_COMPONENT_NAME_IDX]);
  }
  
  DebugN("now removing", componentNamesToBeRemoved);
  if(dynlen(componentNamesToBeRemoved))
    fwInstallation_deleteComponentSet(componentNamesToBeRemoved, deleteFiles);

  //update db if required
  if(fwInstallationDB_getUseDB()) fwInstallationDB_update();    
  
  return 0;
}

/** This functions appends the suffix '_'+ myRedHostNum() to the dp passed as input argument
@param dp: name of the dp
@return dp if redu system is number 1, dp + "_" + myReduHostNumber() if redu number is greater than 1
@author F. Varela
*/
string fwInstallationRedu_getLocalDp(string dp)
{
  int reduHostNum = fwInstallationRedu_myReduHostNum();
  if(reduHostNum > 1)
    dp = dp + "_" + reduHostNum;
  
  return dp;
}


/** This functions appends the suffix '_'+ reduHostNum to the dp passed as input argument
@param dp: name of the dp
@param systemName: when it is different that the local system and no reduHostNum is provided, if the remote system is redundant the active system's redu num is used
@param reduHostNum: redu number; when 0 and systemName is the local system, the local redu number is used
@return dp if redu system is number 1, dp + "_" + reduHostNum if redu number is greater than 1 
*/
string fwInstallationRedu_getReduDp(string dp, string systemName = "", int reduHostNum = 0)
{
  if(systemName == "")
  {
    systemName = getSystemName();
  }
  if (systemName != getSystemName() && reduHostNum == 0) 
  {
    bool isSystemRedundant;
    isRemoteSystemRedundant(isSystemRedundant, systemName);
    if (isSystemRedundant) 
    {
      reduActive(reduHostNum, systemName);
    }
    else reduHostNum = 1;
  }

  if (reduHostNum == 0)
    reduHostNum = fwInstallationRedu_myReduHostNum();
  if(reduHostNum > 1)
    dp = dp + "_" + reduHostNum;
  
  return dp;
}

/** Wrapper function for the standard PVSS function dpTypeCreate to work in redundant systems.
@param elements: dp-type elements
@param types: dp-type element types
@return 0 if OK, -1 if error
@author F. Varela
*/
int fwInstallationRedu_dpTypeChange(dyn_dyn_string elements, dyn_dyn_int types)
{
  if(fwInstallationRedu_isPassive())
    return 0;
  
  return dpTypeChange(elements, types);
}


/** Wrapper function for the standard PVSS function dpTypeCreate to work in redundant systems.
@param elements: dp-type elements
@param types: dp-type element types
@return 0 if OK, -1 if error
@author F. Varela
*/
int fwInstallationRedu_dpTypeCreate(dyn_dyn_string elements, dyn_dyn_int types)
{
  if(fwInstallationRedu_isPassive())
    return 0;
  
  return dpTypeCreate(elements, types);
}

/** Wrapper function for the standard PVSS function dpCreate to work in redundant systems.
@param dpname: name of the dp to be deleted
@return 0 if OK, -1 if error
@author F. Varela
*/
int fwInstallationRedu_dpDelete(string dpname)
{
  if(fwInstallationRedu_isPassive())
    return 0;
  
  return dpDelete(dpname, dptype);
}
/** Wrapper function for the standard PVSS function dpCreate to work in redundant systems.
@param dpname name of the dp to be created
@param dptype dp-type of the new dp
@return 0 if OK, -1 if error
@author F. Varela
*/
int fwInstallationRedu_dpCreate(string dpname, string dptype)
{
  if(fwInstallationRedu_isPassive())
    return 0;
  
  return dpCreate(dpname, dptype);
}

/** Wrapper function for the standard PVSS function dpSet to work in redundant systems.
@param dpname: name of the dp to be set
@param val: value to be set
@return 0 if OK, -1 if error
@author F. Varela
*/
int fwInstallationRedu_dpSet(string dpname, anytype val)
{
  if(fwInstallationRedu_isPassive())
    return 0;
  
  return dpSet(dpname, val);
}

/** This functions checks whether the local system is the passive peer in a redundant system or not.
@return FALSE - local system is not the passive peer, TRUE the local system is the passive peer
@author F. Varela
*/
int fwInstallationRedu_isPassive()
{
  bool isPassive = true;
  int active, local;

  if(!fwInstallationRedu_isRedundant()) //if it is not a redundant system, it will always be the active one.
    return false;
    
  local = fwInstallationRedu_myReduHostNum();
  
  reduActive(active, getSystemName());
  //DebugN(local, active, local > 0, active > 0, local != active, local > 0 && active > 0 && local != active);
  if(local > 0 && active > 0 && local == active)
    isPassive = false;

  //DebugN("returning: ", isPassive);  
  return isPassive;
}
/** This functions returns the names of the hosts of a redundant project
@author F. Varela
*/
dyn_string fwInstallationRedu_getDataHosts() {
    dyn_string hosts = dataHost();
 
  for(int i = 1; i <= dynlen(hosts); i++) //get rid of network domain
  {
    if(strpos(hosts[i], ".") > 0)
      hosts[i] = substr(hosts[i], 0, strpos(hosts[i], "."));
    
    if(strtoupper(hosts[i]) == "LOCALHOST") //make sure localhost is replace with a proper host name
      hosts[i] = strtoupper(getHostname());
    hosts[i] = strtoupper(hosts[i]);    
  } 
  return hosts;
}
/** This functions returns the name of the host where the redundant pair runs
@return name of the redundant pair
@author F. Varela
*/
string fwInstallationRedu_getPair()
{
    
  dyn_string hosts = fwInstallationRedu_getDataHosts();
  
 
  if(fwInstallationRedu_isRedundant() && strtoupper(hosts[1]) == strtoupper(getHostname())) //redu system
    return strtoupper(hosts[2]);

  return strtoupper(hosts[1]);//return the name of the local host in any other case.    
}

/** This functions checks if a particular version of a component is installed in the redundant pair
@param component (in) name of the component
@param version (in) version of the component
@return True if the component is installed in the pair. Otherwise, false
@author F. Varela
*/
bool fwInstallationRedu_isComponentInstalledInPair(string component, string version)
{
  string dp, ver;
  int nok;
  
  if(fwInstallationRedu_myReduHostNum() == 1)
    dp = "fwInstallation_" + component + "_" + 2;
  else
    dp =  "fwInstallation_" + component;
  
  if(dpExists(dp))
  {
    dpGet(dp + ".componentVersionString", ver,
          dp + ".installationNotOK", nok);
    
    if(version == ver && !nok)
      return true;
  }
  
  return false;
}

/** This functions returns whether the current project is redundant
@return boolean indicating whether the project is redundant
*/
bool fwInstallationRedu_isRedundant()
{
  bool isRedu = false;
  string event = _fwInstallationRedu_getEventFromCfg(); 
  if (event != "")
  {
    isRedu = strpos(event, "$") > 0;
  }
  else
  {
    isRedu = isRedundant();
  }
  return isRedu;
}

/** This functions returns the redu num of the local host
@return local host redu num (1 when it is called for non-redundant project)
*/
int fwInstallationRedu_myReduHostNum()
{
  int hostNum = 1;
  if (fwInstallationRedu_isRedundant())
  {
    string eventHostName = _fwInstallationRedu_getEventFromCfg();
    dyn_string eventHosts = strsplit(eventHostName, "$");
    if (eventHostName != "" && dynlen(eventHosts) == 2)
    {
      string pair2 = eventHosts[2];
      int pos = strpos(pair2, ":");
      if (pos > 0)
        pair2 = substr(pair2, 0, pos);
      pair2 = fwInstallation_getHostname(pair2);
     
      string localHost = fwInstallation_getHostname();
      if (strtoupper(pair2) == strtoupper(localHost))
        hostNum = 2;
    }
  }
  return hostNum;
}

/** This functions returns the event host name from the config file
@return hostname of the event manager from the config file
*/
string _fwInstallationRedu_getEventFromCfg()
{
  string eventHostName;
  paCfgReadValue(PROJ_PATH + "/config/config", "general", "event", eventHostName); 
  return eventHostName;
}

int fwInstallationRedu_myPairReduHostNum(int reduHostNum = 0)
{
  if (reduHostNum == 0)
    reduHostNum = fwInstallationRedu_myReduHostNum();
  if (reduHostNum == 1)
    return 2;
  else return 1;
}

/** This functions returns whether split mode is enabled for a redundant project
@return true if the project is in split mode
*/
bool fwInstallationRedu_isSplitMode()
{
  bool splitMode = false;
  
  string reduManagerDp = fwInstallationRedu_getReduManagerDp();
  dpGet(reduManagerDp + ".SplitMode", splitMode);
  
  return splitMode;
}

/** This functions returns whether this is the split active pair
@return true if this is the split active pair
*/
bool fwInstallationRedu_isSplitActive()
{
  bool splitActive = false;
  bool eventActive = false;
  string reduManagerDp = fwInstallationRedu_getReduManagerDp();
  dpGet(reduManagerDp + ".SplitActive", splitActive,
        reduManagerDp + ".Status.Active", eventActive);

  return splitActive && eventActive;
}

string fwInstallationRedu_getReduManagerDp()
{
  string reduManagerDp = "_ReduManager";
  int hostNum = fwInstallationRedu_myReduHostNum();
  if (hostNum > 1)
  {
    reduManagerDp += "_" + hostNum;
  }
  return  reduManagerDp;
}

bool fwInstallatinRedu_isConnectedToEventManager(int replicaNum)
{
  if (replicaNum != 1 && replicaNum != 2)
  {
    fwInstallation_throw("fwInstallatinRedu_isConnectedToEventManager should be called with parameter equal to 1 or 2!");
    
  }
  int manID;
  if (replicaNum == 1)
  {
    manID = convManIdToInt(EVENT_MAN, 0); /* Integer corresponding to the manager identifier. See convManIdToInt() */ 
  }
  else
  { 
    manID = convManIdToInt(EVENT_MAN,0,0,2); /*ManagerIdentifier with replica 2*/ 
  }
  bool connOpened = isConnOpen(manID);

  return connOpened; 
}

bool fwInstallationRedu_ensureInstallationConditions()
{
  //return false;
  bool readyToInstall = false;
  int cnt = 0;
  if (fwInstallationRedu_isRedundant() && (fwInstallation_getInstallOnlyInSplit() == 1))
  {
    fwInstallation_throw("This is redundant project and we should install only in split", "INFO");
    if (fwInstallationRedu_isSplitMode())
    {
      readyToInstall = !fwInstallationRedu_isSplitActive() && ! fwInstallationRedu_isRecovering();
    }
    else
    {

      string project = PROJ;  
      string hostname = fwInstallationRedu_getPair();    
      hostname = strtoupper(hostname);
      dyn_mixed projectProperties;
      int project_id;
      fwInstallationDB_getProjectProperties(project, hostname, projectProperties, project_id);
      // if we are the passive system, or we are active but everything is already done in partner
      if (fwInstallationRedu_isPassive() || (project_id > 0 && projectProperties[FW_INSTALLATION_DB_PROJECT_DIST_PEERS_OK] == 1 &&
          projectProperties[FW_INSTALLATION_DB_PROJECT_PATH_OK] == 1 &&  projectProperties[FW_INSTALLATION_DB_PROJECT_COMPONENT_OK] == 1 && projectProperties[FW_INSTALLATION_DB_PROJECT_NEED_SYNCHRONIZE] == 'N'))
      {
        if (fwInstallationRedu_isRecovering()) {
          readyToInstall = false;
       } else {
          fwInstallation_throw("Is this the passive pair - " + fwInstallationRedu_isPassive(), "INFO");
          fwInstallation_throw("Is everything installed in partner? - " + project_id +  " " + 
                 projectProperties[FW_INSTALLATION_DB_PROJECT_DIST_PEERS_OK] +  " " + 
                 projectProperties[FW_INSTALLATION_DB_PROJECT_PATH_OK] +  " " +
                 projectProperties[FW_INSTALLATION_DB_PROJECT_COMPONENT_OK] + " " +
                 projectProperties[FW_INSTALLATION_DB_PROJECT_NEED_SYNCHRONIZE], "INFO");
        
          int myPartnerNum = fwInstallationRedu_myPairReduHostNum();
          if (!fwInstallationRedu_isPassive())
          {
            fwInstallation_throw("Setting partner as active..", "INFO");
            reduSetActive(getSystemName(), myPartnerNum, false);
            cnt = 1;
            while(cnt <= fwInstallationRedu_WAIT_COUNT && !fwInstallationRedu_isPassive())
            {
              delay(1);
              cnt++;
            }
            if (!fwInstallationRedu_isPassive())
            {
              fwInstallation_throw("fwInstallationRedu_ensureInstallationConditions: Cannot make partner active.", "ERROR");
              return false;
            }
          } // now partner is active

          //set split mode
          fwInstallation_throw("Setting split mode..", "INFO");
          reduSetSplitOn(getSystemName(), false);
          cnt  =1;
          while(cnt <= fwInstallationRedu_WAIT_COUNT && !fwInstallationRedu_isSplitMode())
          {
            delay(1);
            cnt++;
          }
          if (!fwInstallationRedu_isSplitMode())
          {
            fwInstallation_throw("fwInstallationRedu_ensureInstallationConditions: Cannot set split mode.", "ERROR");
            return false;
          }
          fwInstallation_throw("Project is in split mode now and the agent is ready to install", "INFO");
          delay(5);
          gFwInstallationDBAgentSetSplit = true;
          _fwInstallationRedu_setSplitInfo(true, fwInstallationRedu_myReduHostNum());
          //split mode is set and we are the passive system -> ready to install
          readyToInstall = true;
          }
        } 
      else //not installed in partner and we are passive -> do nothing
      {
        fwInstallation_throw("This is not the passive pair and it is still not installed in the partner: " + project_id + " " +   
               projectProperties[FW_INSTALLATION_DB_PROJECT_DIST_PEERS_OK] + " " +    
               projectProperties[FW_INSTALLATION_DB_PROJECT_PATH_OK] + " " +    
               projectProperties[FW_INSTALLATION_DB_PROJECT_COMPONENT_OK], "INFO");
        readyToInstall = false;
      }
    }
  }
  else
  {
    readyToInstall = !fwInstallationRedu_isPassive();
  }
  return readyToInstall;
  
}

void _fwInstallationRedu_setSplitInfo(bool splitForced, string pairToKeep)
{
  string dp = fwInstallation_getAgentDp();
  dpSetWait(dp + ".redundancy.splitModeForced", splitForced,
        dp + ".redundancy.pairToKeepAfterSplit", pairToKeep); 
}

void _fwInstallationRedu_getSplitInfo(bool& splitForced, string& pairToKeep)
{
  string dp = fwInstallation_getAgentDp();
  dpGet(dp + ".redundancy.splitModeForced", splitForced,
        dp + ".redundancy.pairToKeepAfterSplit", pairToKeep); 
}

bool fwInstallationRedu_isRecovering() {
  string dp = fwInstallationRedu_getReduManagerDp();
  string fn = getPath(DATA_REL_PATH) + "/fwInstallationDpGetResult" + period(getCurrentTime()) + ".txt";
  fwInstallationRedu_execScriptConnectedToBothPeers("fwInstallationGetDpe.ctl",makeDynString(dp + ".IsRecovering", fn));
  delay(0,10);
  string result;
  fileToString(fn,result);
  int status = (int) result;
  fwInstallation_throw("Checking is Recovering (" + dp + ") status = " + status );
  remove(fn);
  return (status != 0);  
}
