# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
#from research.ch.cern.unicos.types.ai import Instance                 	#REQUIRED
from time import strftime

class AnalogAlarm_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for AA.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for AA.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for AA.")
	
   def process(self, *params):
	self.thePlugin.writeInUABLog("processInstances in Jython for AA.")
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
			
	# General Steps for Optimized objects:
	# 1. Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances) It's called DB_Type
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from DeviceTypeDefinition.xml created by TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5. Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 6. Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	
	# This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()

	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	
	# Step 1: Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances)
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Analog Alarm DB Creation file: UNICOS application
''')
	self.thePlugin.writeInstanceInfo('''

DATA_BLOCK DB_AA CPC_FB_AA
BEGIN

END_DATA_BLOCK
''')

	# Step 3: Create all the needed Structures from the TCT	
	
	# UDT for DB_AA_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE AA_ManRequest
TITLE = AA_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isCommVerification = attribute.getIsCommunicated()
				if isCommVerification:
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE AA_bin_Status
TITLE = AA_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				# isEventAttribute = attribute.getIsEventAttribute()
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE AA_ana_Status
TITLE = AA_ana_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributeName = attribute.getAttributeName()
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# Step 4: Create all the need DBs to store the required signal from the object
	
	# DB_AA_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_AA_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_AA_ManRequest
TITLE = DB_AA_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    AA_Requests : ARRAY [1..'''+instanceNb+'''] OF AA_ManRequest;
END_STRUCT;
BEGIN''')

	RecordNumber = 0
	for instance in instancesVector:
		RecordNumber = RecordNumber + 1
		HHAA = instance.getAttributeData ("FEDeviceManualRequests:HH Alarm")
		HWAA = instance.getAttributeData ("FEDeviceManualRequests:H Warning")
		LWAA = instance.getAttributeData ("FEDeviceManualRequests:L Warning")
		LLAA = instance.getAttributeData ("FEDeviceManualRequests:LL Alarm")

		# 2 Data treatments
		#summary for the 4 levels
		#	Spec file	pin	name		Variable	InhibitManual	AutoEXX			VariableInit		Comment
		#				S_XX 					    ParReg_Bit_x	Initiale value	XX_init
		#   ------------------------------------------------------------------------------------------------------------------------------------------
		# 1	Nbr			Variable		Yes			No				TRUE				Nbr				Operator can set value, by default enabled
		# 2	Empty		xxxxxxxx		No			Yes				FALSE				0.0				Always Disabled : Operator and logic cannot set value
		# 3	"logic"		Variable 		Yes			Yes				TRUE				-				Logic manage the variable
		# 4	UObject		UObject.PosSt??	Yes			Yes				TRUE				-				value set via UObject

		# For Nbr and Empty , 
		# 		value and activation are loaded at the Initialisation. the prgm CANNOT Activate (AuEXX) and cannot modify the value.
		# 		But via the scada value and activation CAN be modified.
		# For "Logic" 
		#		The prgm has to fix a value to the internal variable and control the activation (AuEXX).
		#		The ""trick"" if the designer want a default value (Nbr) and a prgm control of the level he cannot type a Nbr in the spec, but has to type "Logic" 
		#		then in the prgm he has to assign the variable to the value "Nbr"
		# For UObject
		#		The prgm manage the UObject value, the activation is also managed by the prgm.
		#		
		# for all cases, the variable "address" is caculated event if in some cases it is not mandatory.  
		# case 4 is mainly applied for Apar Object, but it may works aslo for many other UNICOS objetcs. ( Apar or AS are recommended) 
		# All assignment are done in the logic section except the I signal, when it is linked to an UObject

	
		# HH conditions
		if self.thePlugin.isString(HHAA) and HHAA.strip() != "":		#case 3 and 4 : "logic" or UObject
			HHValue ="0.0"	
		elif HHAA.strip() == "":										#case 2 : empty
			HHValue ="0.0"	
		else:															# case 1 : Nbr
			HHValue = self.thePlugin.formatNumberPLC(HHAA)

		# H conditions
		if self.thePlugin.isString(HWAA) and HWAA.strip() != "":		#case 3 and 4
			HValue ="0.0"	
		elif HWAA.strip() == "":										#case 2
			HValue ="0.0"	
		else:															# case 1
			HValue = self.thePlugin.formatNumberPLC(HWAA)
			
		# L conditions
		if self.thePlugin.isString(LWAA) and LWAA.strip() != "":		#case 3 and 4
			LValue ="0.0"	
		elif LWAA.strip() == "":										#case 2
			LValue ="0.0"	
		else:															# case 1
			LValue = self.thePlugin.formatNumberPLC(LWAA)
						
		# LL conditions
		if self.thePlugin.isString(LLAA) and LLAA.strip() != "":		#case 3 and 4
			LLValue ="0.0"	
		elif LLAA.strip() == "":										#case 2
			LLValue ="0.0"	
		else:															# case 1
			LLValue = self.thePlugin.formatNumberPLC(LLAA)
			
		self.thePlugin.writeInstanceInfo('''

	AA_Requests['''+str(RecordNumber)+'''].HH:= $HHValue$;
	AA_Requests['''+str(RecordNumber)+'''].H:= $HValue$;
	AA_Requests['''+str(RecordNumber)+'''].L:= $LValue$;
	AA_Requests['''+str(RecordNumber)+'''].LL:= $LLValue$;


''')

	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK
''')

	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE AA_event
TITLE = AA_event
//
// parameters of AA Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# Step 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 
	
	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_AA Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_AA
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type AA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_AA : INT := '''+str(NbType)+''';
	AA_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF AA_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')

	self.thePlugin.writeInstanceInfo('''
(*Status of the ANALOG ALARMs************************************************)
DATA_BLOCK DB_bin_status_AA
TITLE = 'DB_bin_status_AA'
//
// Global binary status DB of AA
//
// List of variables:

''')

	RecordNumber = 0
	for instance in instancesVector:
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
// ['''+str(RecordNumber)+'''] $Name$      (AA_bin_Status)
''')

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
  
STRUCT
	StsReg0102: ARRAY [1..'''+str(RecordNumber)+'''] OF AA_bin_Status;
END_STRUCT
BEGIN

END_DATA_BLOCK

(*old Status of the AAs************************************************)
DATA_BLOCK DB_bin_status_AA_old
TITLE = 'DB_bin_status_AA_old'
//
// Old Global binary status DB of AA
//
// List of variables:
	''')
	
	RecordNumber = 0
	for instance in instancesVector:
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
// ['''+str(RecordNumber)+'''] $Name$      (AA_bin_Status)
''')

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
	StsReg0102: ARRAY [1..'''+str(RecordNumber)+'''] OF AA_bin_Status;
END_STRUCT
  
BEGIN

END_DATA_BLOCK

// -----------------------------------------------------------------------------------------------------
(*analog status of the AAs************************************************)
DATA_BLOCK DB_ana_Status_AA
TITLE = 'DB_ana_Status_AA'
//
// Global Analog status DB of AA
//
//  List of variables:
''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AA_ana_Status)
''')	


	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF AA_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
// -----------------------------------------------------------------------------------------------------
(*old analog status of the AAs************************************************)
DATA_BLOCK DB_ana_Status_AA_old
TITLE = 'DB_ana_Status_AA_old'
//
// old Global Analog  status DB of AA
//
  // List of variables:
''')
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AA_ana_Status)
''')		

		
	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF AA_ana_Status;
END_STRUCT
BEGIN

END_DATA_BLOCK
''')

	# Step 5: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	
	self.thePlugin.writeInstanceInfo('''
// AA EXEC: Inputs of the AA will be set by the FC logic blocks
FUNCTION_BLOCK FB_AA_ALL
TITLE = 'FB_AA_all'
//
// AAs grouped in a single DB
//
AUTHOR: 'UNICOS'
NAME: 'Call AL'
FAMILY: 'AA'
VAR
 AA_SET: STRUCT
	''')
	
	RecordNumber = 0
	for instance in instancesVector:
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
	$Name$      : CPC_DB_AA;
''')


	self.thePlugin.writeInstanceInfo('''
		END_STRUCT;
  // Different variable view declaration
  AA AT AA_SET: ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_DB_AA;
  
  // Support variables
  old_status1 : DWORD;
  old_status2 : DWORD;
  I: INT;
END_VAR
    FOR I:=1 TO '''+str(RecordNumber)+''' DO
		old_status1 := DB_bin_status_AA.StsReg0102[I].StsReg01;
		old_status2 := DB_bin_status_AA.StsReg0102[I].StsReg02;
		old_status1 := ROR(IN:=old_status1, N:=16);
		old_status2 := ROR(IN:=old_status2, N:=16);
       	
        
		// Call (Inputs will be set in the FC logic)
		CPC_FB_AA.DB_AA(
						Manreg01 := DB_AA_ManRequest.AA_Requests[I].Manreg01
						,HH := DB_AA_ManRequest.AA_Requests[I].HH
						,H := DB_AA_ManRequest.AA_Requests[I].H
						,L := DB_AA_ManRequest.AA_Requests[I].L
						,LL := DB_AA_ManRequest.AA_Requests[I].LL
						,StsReg01 := DB_bin_status_AA.StsReg0102[I].Stsreg01
						,StsReg02 := DB_bin_status_AA.StsReg0102[I].Stsreg02
						,Perst := AA[I]
						);     
						
		// Status
        DB_ana_status_AA.status[I].PosSt := AA[I].PosSt;
        DB_ana_status_AA.status[I].HHSt := AA[I].HHSt;
        DB_ana_status_AA.status[I].HSt := AA[I].HSt;
        DB_ana_status_AA.status[I].LSt := AA[I].LSt;
        DB_ana_status_AA.status[I].LLSt := AA[I].LLSt;

		// Events
        DB_Event_AA.AA_evstsreg[I].evstsreg01 := old_status1 OR DB_bin_status_AA.StsReg0102[I].StsReg01;
		DB_Event_AA.AA_evstsreg[I].evstsreg02 := old_status2 OR DB_bin_status_AA.StsReg0102[I].StsReg02;
          
    END_FOR;   
END_FUNCTION_BLOCK
  ''')

  
	# Step 6: Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	
	self.thePlugin.writeInstanceInfo('''  
// AA DB: all instance data
DATA_BLOCK DB_AA_ALL FB_AA_ALL
BEGIN	
	''')

	RecordNumber = 0
	for instance in instancesVector:
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Delay = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
		if Delay.strip() == "":
			self.thePlugin.writeInstanceInfo('''
	AA_SET.$Name$.PAlDt:= 0;
''')	
		elif self.thePlugin.isString(Delay):
			self.thePlugin.writeInstanceInfo('''
	// The Alarm Delay is defined in the logic
''')	
		else:
			Delay = int(round(float(Delay)))
			self.thePlugin.writeInstanceInfo('''
	AA_SET.$Name$.PAlDt:='''+str(Delay)+''';
''')	
		
		
		
		HHAA = instance.getAttributeData ("FEDeviceManualRequests:HH Alarm")
		HWAA = instance.getAttributeData ("FEDeviceManualRequests:H Warning")
		LWAA = instance.getAttributeData ("FEDeviceManualRequests:L Warning")
		LLAA = instance.getAttributeData ("FEDeviceManualRequests:LL Alarm")
		AutoAlarmAck = instance.getAttributeData("FEDeviceAlarm:Auto Acknowledge").strip().lower()
		if AutoAlarmAck == "true":
			PAuAckAlValuebit12 = "1"							# bit 4
		else:
			PAuAckAlValuebit12 = "0"

		# HH conditions
		if self.thePlugin.isString(HHAA) and HHAA.strip() != "":		#case 3 and 4: "Logic" or UObject
			IhMHHValuebit8 = '1'										# bit 0
			AuEHHValue = 'TRUE'
			HHValue = "0.0"
		elif HHAA.strip() == "":										#case 2 : Empty 
			IhMHHValuebit8 = '1'
			AuEHHValue = 'FALSE'
			HHValue = "0.0"
		else:															# case 1 : Nbr
			IhMHHValuebit8 = '0'
			AuEHHValue = 'TRUE'
			HHValue = self.thePlugin.formatNumberPLC(HHAA)
			
		# H conditions
		if self.thePlugin.isString(HWAA) and HWAA.strip() != "":		#case 3 and 4
			IhMHValuebit9 = '1'											# bit 1
			AuEHValue = 'TRUE'
			HValue = "0.0"
		elif HWAA.strip() == "":										#case 2
			IhMHValuebit9 = '1'
			AuEHValue = 'FALSE'	
			HValue = "0.0"
		else:															# case 1
			IhMHValuebit9 = '0'		
			AuEHValue = 'TRUE'	
			HValue = self.thePlugin.formatNumberPLC(HWAA)
			
		# L conditions
		if self.thePlugin.isString(LWAA) and LWAA.strip() != "":		#case 3 and 4
			IhMLValuebit10 = '1'										# bit 2
			AuELValue = 'TRUE'	
			LValue = "0.0"
		elif LWAA.strip() == "":										#case 2
			IhMLValuebit10 = '1'	
			AuELValue = 'FALSE'	
			LValue = "0.0"
		else:															# case 1
			IhMLValuebit10 = '0'	
			AuELValue = 'TRUE'				
			LValue = self.thePlugin.formatNumberPLC(LWAA)

			
		# LL conditions
		if self.thePlugin.isString(LLAA) and LLAA.strip() != "":		#case 3 and 4
			IhMLLValuebit11 = '1'										# bit 3
			AuELLValue = 'TRUE'	
			LLValue = "0.0"
		elif LLAA.strip() == "":										#case 2
			IhMLLValuebit11 = '1'
			AuELLValue = 'FALSE'
			LLValue =	"0.0"		
		else:															# case 1
			IhMLLValuebit11 = '0'
			AuELLValue = 'TRUE'				
			LLValue = self.thePlugin.formatNumberPLC(LLAA)
	
		self.thePlugin.writeInstanceInfo('''	
	// Threshold initialization
	AA_SET.$Name$.PAA.PArReg :=  2#0000000000$PAuAckAlValuebit12$$IhMLLValuebit11$$IhMLValuebit10$$IhMHValuebit9$$IhMHHValuebit8$;
	AA_SET.$Name$.AuEHH:= $AuEHHValue$;
	AA_SET.$Name$.AuEH:= $AuEHValue$;
	AA_SET.$Name$.AuEL:= $AuELValue$;
	AA_SET.$Name$.AuELL:= $AuELLValue$;
	AA_SET.$Name$.HH:= $HHValue$;
	AA_SET.$Name$.H:= $HValue$;
	AA_SET.$Name$.L:= $LValue$;
	AA_SET.$Name$.LL:= $LLValue$;
	AA_SET.$Name$.HHSt:= $HHValue$;
	AA_SET.$Name$.HSt:= $HValue$;
	AA_SET.$Name$.LSt:= $LValue$;
	AA_SET.$Name$.LLSt:= $LLValue$;
		''')

		
	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK
''')

	#Creation of a FC even for the optimized objects in order to call it in the OB1
	#This avoid the too many symbols problem of the OB1
	#The code is ready for all the object in case we need to split in several DBs one day. Note that for the moment,
	#this has sense only for the AIR and the DI where the split is working.
	NumberOfInstances = instancesVector.size()
	LimitSizeOfInstances = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", theCurrentDeviceType.getDeviceTypeName()))
	nameRepresentation = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", theCurrentDeviceType.getDeviceTypeName())
	self.thePlugin.writeInstanceInfo('''
FUNCTION FC_$nameRepresentation$ : VOID
    FB_$nameRepresentation$_all.DB_$nameRepresentation$_all();''')
	if (NumberOfInstances > LimitSizeOfInstances):
		self.thePlugin.writeInstanceInfo('''
    FB_$nameRepresentation$_all2.DB_$nameRepresentation$_all2();    // Too many '''+str(NumberOfInstances)+''' Need a second FB_$nameRepresentation$_All2''')
	self.thePlugin.writeInstanceInfo('''
END_FUNCTION
''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for AA.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for AA.")
		
