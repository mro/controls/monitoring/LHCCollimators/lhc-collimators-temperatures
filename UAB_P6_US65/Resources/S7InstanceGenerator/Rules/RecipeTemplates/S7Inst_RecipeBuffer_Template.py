# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Recipes.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class RecipeBuffer_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   thePluginId = 0
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
   	self.theUnicosProject = self.thePlugin.getUnicosProject()
	self.thePlugin.writeInUABLog("initialize in Jython for Recipes.")
	self.thePluginId = self.thePlugin.getId()

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for Recipes.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for Recipes.")
  
   def process(self, *params):
	self.thePlugin.writeInUABLog("processInstances in Jython for Recipes.")
	theOverallInstances = params[0]
	theXMLConfig = params[1]
	
	# General Steps for the Recipes file:
	# 1. Create the DB_RECIPES with the required buffers
	
	ActivationTimeout = theXMLConfig.getPLCParameter("RecipeParameters:ActivationTimeout")
	HeaderBufferSize = theXMLConfig.getPLCParameter("RecipeParameters:HeaderBufferSize").strip()
	BufferSize = theXMLConfig.getPLCParameter("RecipeParameters:BufferSize").strip()
	BufferSizeCalculated = int(BufferSize)
	if (BufferSizeCalculated%120 != 0):
		BufferSizeCalculated = (int(((BufferSizeCalculated-1)/120))+1)*120
		if BufferSizeCalculated > 1000:
			BufferSizeCalculated = 1000
			
	if int(BufferSizeCalculated)>1000:
		self.thePlugin.writeErrorInUABLog("The maximun buffer size allowed is 1000. BufferSize = 1000 has been taken")
		BufferSizeCalculated = 1000
	if int(BufferSizeCalculated)<1:
		self.thePlugin.writeErrorInUABLog("The buffer size must be a positive integer. BufferSize = 120 has been taken")
		BufferSizeCalculated = 120
	
	
	self.thePlugin.writeRecipesInfo('''// DB_RECIPES_INTERFACE
DATA_BLOCK DB_RECIPES_INTERFACE
TITLE = DB_RECIPES_INTERFACE

AUTHOR: 'ICE/SIC'
NAME: 'Recipes'
FAMILY: 'UNICOS'
STRUCT   
	ActivationTimeout : INT := $ActivationTimeout$;
	RecipeHeader:              STRUCT     // Recipe Header     
			RecipeIDLow:       INT;
			RecipeIDHigh:      INT;
			NbManReg:          INT;
			NbofDPAR:          INT;
			DBofDPAR:          INT;
			NbofWPAR:          INT;
			DBofWPAR:          INT;
			NbofAPAR:          INT;
			DBofAPAR:          INT;
			NbofAA:            INT;
			DBofAA:            INT;
			NbofPID:           INT;
			DBofPID:           INT;  
			SentValue:         INT;
			CRC:               INT;
			Void01:            INT;
			Void02:            INT;
			Void03:            INT;
			Void04:            INT;
			Void05:            INT;
							   END_STRUCT;
     
	RecipeStatus:              STRUCT  // Recipe Status
			RecipeIDLow:       INT;
			RecipeIDHigh:      INT;
			NbManReg:          INT;
			NbofDPAR:          INT;
			DBofDPAR:          INT;
			NbofWPAR:          INT;
			DBofWPAR:          INT;
			NbofAPAR:          INT;
			DBofAPAR:          INT;
			NbofAA:            INT;
			DBofAA:            INT;
			NbofPID:           INT;
			DBofPID:           INT;  
			RecipeStatus:      INT;
			CRCStatus:         INT;
			ErrorDetail:       INT;
			Void01:            INT;
			Void02:            INT;
			Void03:            INT;
			Void04:            INT;      
							   END_STRUCT;    
    ManRegAddr:   ARRAY[1..'''+str(BufferSizeCalculated)+'''] OF INT;   // ManReg01 Addresses
    ManRegVal:    ARRAY[1..'''+str(BufferSizeCalculated)+'''] OF INT;   // ManReg01 Values
    RequestAddr:  ARRAY[1..'''+str(BufferSizeCalculated)+'''] OF INT;   // Request Addresses
    RequestVal:   ARRAY[1..'''+str(BufferSizeCalculated)+'''] OF REAL;   // Request Values
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')

	self.thePlugin.writeRecipesInfo('''// DB_RECIPES_INTERFACE_old
DATA_BLOCK DB_RECIPES_INTERFACE_old
TITLE = DB_RECIPES_INTERFACE_old

AUTHOR: 'ICE/SIC'
NAME: 'Recipes'
FAMILY: 'UNICOS'
STRUCT   
	ActivationTimeout : INT := $ActivationTimeout$;
	RecipeHeader:              STRUCT     // Recipe Header     
			RecipeIDLow:       INT;
			RecipeIDHigh:      INT;
			NbManReg:          INT;
			NbofDPAR:          INT;
			DBofDPAR:          INT;
			NbofWPAR:          INT;
			DBofWPAR:          INT;
			NbofAPAR:          INT;
			DBofAPAR:          INT;
			NbofAA:            INT;
			DBofAA:            INT;
			NbofPID:           INT;
			DBofPID:           INT;  
			SentValue:         INT;
			CRC:               INT;
			Void01:            INT;
			Void02:            INT;
			Void03:            INT;
			Void04:            INT;
			Void05:            INT;
							   END_STRUCT;
     
	RecipeStatus:              STRUCT  // Recipe Status
			RecipeIDLow:       INT;
			RecipeIDHigh:      INT;
			NbManReg:          INT;
			NbofDPAR:          INT;
			DBofDPAR:          INT;
			NbofWPAR:          INT;
			DBofWPAR:          INT;
			NbofAPAR:          INT;
			DBofAPAR:          INT;
			NbofAA:            INT;
			DBofAA:            INT;
			NbofPID:           INT;
			DBofPID:           INT;  
			RecipeStatus:      INT;
			CRCStatus:         INT;
			ErrorDetail:       INT;
			Void01:            INT;
			Void02:            INT;
			Void03:            INT;
			Void04:            INT;      
							   END_STRUCT;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for Recipes.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for Recipes.")
		
