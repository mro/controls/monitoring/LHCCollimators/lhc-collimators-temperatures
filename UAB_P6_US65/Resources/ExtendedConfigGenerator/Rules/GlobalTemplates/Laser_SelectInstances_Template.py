# Jython source file to create LHCLogging file
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED


class SelectInstances_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.laserService = self.thePlugin.getServiceInstance("Laser")
        self.thePlugin.writeInUABLog("initialize in Jython for SelectInstances.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for SelectInstances.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for SelectInstances.")

    def process(self):
        self.thePlugin.writeInUABLog("processInstances in Jython for SelectInstances.")

        # Selects the instances from the SPECS file using the findMatchingInstances methods.
        di = self.theUnicosProject.findMatchingInstances("DigitalInput",
                                                         "'#DeviceDocumentation:Description#' not contains 'SPARE'")
        do = self.theUnicosProject.findMatchingInstances("DigitalOutput",
                                                         "'#DeviceDocumentation:Description#' not contains 'SPARE'")
        ai = self.theUnicosProject.findMatchingInstances("AnalogInput",
                                                         "'#DeviceDocumentation:Description#' not contains 'SPARE'")

        # Adds the selected instances to the LHCLogging data
        self.laserService.addLaserAlarm("PosSt", di, "0ffa", 10, 0)
        self.laserService.addLaserAlarm("PosSt", do, "0ffb", 11, 1)
        self.laserService.addLaserAlarm("PosSt", ai, "0ffc", 12, 2)

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for SelectInstances.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for SelectInstances.")
