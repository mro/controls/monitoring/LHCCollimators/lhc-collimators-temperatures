# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for WordStatus Objects.

import WinCCFlex_Generic_Template
reload(WinCCFlex_Generic_Template)

class WordStatus_Template(WinCCFlex_Generic_Template.Generic_Template):
    theDeviceType = "WordStatus"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'unit', 'String')
        self.writeScriptTag(instance, 'PosSt', self.integer)

        self.writeSmartTagUnit(instance)
        self.writeTextListPattern(instance)