# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
import Reverse_Generic_Template
reload(Reverse_Generic_Template)
import Reverse_GlobalFunctions_Template
reload(Reverse_GlobalFunctions_Template)
    
class Controller_Template(Reverse_Generic_Template.Generic_Template): 
    theDeviceType = "Controller"
    
    def processInstance(self, instance, generatedInstance):
        
        key,value = self.getDescriptionWithoutElectricalDiagram(instance) #overwrite result from generic function
        generatedInstance.put(key,value)
        
        key,value = self.getMaster(instance)
        generatedInstance.put(key,value)

        key,value = self.getMaskEvent(instance)
        generatedInstance.put(key,value)    
        
        value = instance["scalingMethod"]  
        try:
            value = int(value)
            value = value - 1
        except:
            pass
        generatedInstance.put("FEDeviceParameters.PControl.ScaMethod", str(value))  
                
        #HMV Deadband
        key,value = self.getDriverDeadbandType(instance, path="SCADADriverDataSmoothing.OutDeadband", field="driverDeadbandTypeOut")
        generatedInstance.put(key,value)
        
        key,value = self.getDriverDeadbandValue(instance, path="SCADADriverDataSmoothing.OutDeadband", field="driverDeadbandValueOut")
        generatedInstance.put(key,value)
        
        #Out Deadband
        key,value = self.getDriverDeadbandType(instance, path="SCADADriverDataSmoothing.MVDeadband", field="driverDeadbandTypeHMV")
        generatedInstance.put(key,value)
        
        key,value = self.getDriverDeadbandValue(instance, path="SCADADriverDataSmoothing.MVDeadband", field="driverDeadbandValueHMV")
        generatedInstance.put(key,value)
        
        #HMV archive
        keyArchive,valueArchive,keyDbType,valueDbType,keyDbValue,valueDbValue = self.getArchiveModeDeadbandTypeDeadbandValueWithTimeFilterWithDeadBand(instance, path = "SCADADeviceDataArchiving.MVArchiving", fieldArchive = "archiveModeHMV", fieldFilter = "timeFilterHMV")
        generatedInstance.put(keyArchive,valueArchive)
        generatedInstance.put(keyDbType,valueDbType)
        generatedInstance.put(keyDbValue,valueDbValue)
        
        key,value = self.getTimeFilter(instance, path="SCADADeviceDataArchiving.MVArchiving", field="timeFilterHMV")
        generatedInstance.put(key,value)
        
        #Out archive
        keyArchive,valueArchive,keyDbType,valueDbType,keyDbValue,valueDbValue = self.getArchiveModeDeadbandTypeDeadbandValueWithTimeFilterWithDeadBand(instance, path = "SCADADeviceDataArchiving.OutArchiving", fieldArchive = "archiveModeOut", fieldFilter = "timeFilterOut")
        generatedInstance.put(keyArchive,valueArchive)
        generatedInstance.put(keyDbType,valueDbType)
        generatedInstance.put(keyDbValue,valueDbValue)
        
        key,value = self.getTimeFilter(instance, path="SCADADeviceDataArchiving.OutArchiving", field="timeFilterHMV")
        generatedInstance.put(key,value)
        
        #Default values
        defaults = {}
        defaults["defaultKc"] = "DefKc"
        defaults["defaultTi"] = "DefTi"
        defaults["defaultTd"] = "DefTd"
        defaults["defaultTds"] = "DefTds"
        defaults["defaultSP"] = "DefSP"
        defaults["defaultSPH"] = "DefSPH"
        defaults["defaultSPL"] = "DefSPL"
        defaults["defaultOutH"] = "DefOutH"
        defaults["defaultOutL"] = "DefOutL"
        
        for default in defaults:
            value = instance[default]
            generatedInstance.put("FEDeviceVariables.DefPID." + defaults[default], value)  

        
        
