# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime
from java.lang import System
from java.util import ArrayList
from java.util import LinkedHashMap
import copy
import Reverse_GlobalFunctions_Template
reload(Reverse_GlobalFunctions_Template)
    
class Generic_Template(IUnicosTemplate, Reverse_GlobalFunctions_Template.GlobalFunctions_Template): 
    thePlugin = 0
    theUnicosProject = 0
    theDeviceType = ""
    
    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize in Jython for " + self.theDeviceType + ".")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for " + self.theDeviceType + ".")
	
    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for " + self.theDeviceType + ".")
	
    def process(self, *params):  
        self.thePlugin.writeInUABLog("process in Jython for " + self.theDeviceType + ".")
        instancesArray = self.thePlugin.readInstancesArray()
        generatedInstancesArray = ArrayList()
        
        self.thePlugin.writeInfoInUABLog("Processing " + str(len(instancesArray)) + " instances")
        
        for instance in instancesArray:
            generatedInstance = LinkedHashMap()
            generatedInstance.put("deviceType",self.theDeviceType)

            key,value = self.getNameFromAliasDeviceLinkList(instance)
            generatedInstance.put(key,value)
            
            key,value = self.getDeviceLinkListFromAliasDeviceLinkList(instance)
            generatedInstance.put(key,value)
            
            key,value = self.getDescription(instance)
            generatedInstance.put(key,value)        
            
            key,value = self.getWidgetType(instance)
            generatedInstance.put(key,value)
            
            key,value = self.getNature(instance)
            generatedInstance.put(key,value)
            
            key,value = self.getDomain(instance)
            generatedInstance.put(key,value)
            
            key,value = self.getAccessControlDomain(instance)
            generatedInstance.put(key,value)
            
            key,value = self.getSynoptic(instance)
            generatedInstance.put(key,value)
            
            key,value = self.getWWWLink(instance)
            generatedInstance.put(key,value)
            
            key,value = self.getDiagnostics(instance)
            generatedInstance.put(key,value)
            
            key,value = self.getBooleanArchive(instance)
            generatedInstance.put(key,value)
            
            key,value = self.getAnalogArchive(instance)
            generatedInstance.put(key,value)
            
            key,value = self.getEventArchive(instance)
            generatedInstance.put(key,value)
            
            self.processInstance(instance, generatedInstance)
            
            generatedInstancesArray.add(generatedInstance)
                    
        self.thePlugin.writeInstancesArray(generatedInstancesArray)
	
    # overwrite in the device template
    def processInstance(self, instance, generatedInstance):
        self.thePlugin.writeInUABLog("Implement processInstance method in the " + self.theDeviceType + " template")
        
    
    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for " + self.theDeviceType + ".")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for " + self.theDeviceType + ".")
