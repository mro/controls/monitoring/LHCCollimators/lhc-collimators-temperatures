# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

class OnOff_Template(IUnicosTemplate):
   theSemanticVerifier = 0
   thePlugin = 0
   isDataValid = 1
   theCurrentDeviceType = 0
   
   def initialize(self):
   	self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("check rules: initialize")

   def check(self):
	self.thePlugin.writeInUABLog("check rules: check")

   def begin(self):
	self.thePlugin.writeInUABLog("check rules: begin")
	
   def process(self, *params): 
	theCurrentDeviceTypeName = params[0]
	theCurrentDeviceTypeDefinition = params[1]
	theUnicosProject = self.thePlugin.getUnicosProject()
   	theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)	
   	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	self.thePlugin.writeInUABLog(""+str(theCurrentDeviceTypeName)+" Specific Semantic Rules")
	
	# 1. checking the length of the names
   	for instance in instancesVector :
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		Remarks = instance.getAttributeData("DeviceDocumentation:Remarks")
		nameSize = len(Name)
		theManufacturer = self.thePlugin.getPlcManufacturer()
		if (theManufacturer.lower() == "siemens") and nameSize > 19:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 19")
			
		elif (theManufacturer.lower() == "schneider") and nameSize > 23:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 23")

		
		# FEDevice inputs verification
		FeedbackOn = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On").strip()
		FeedbackOff = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off").strip()
		LocalDrive = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Drive").strip()
		LocalON = instance.getAttributeData("FEDeviceEnvironmentInputs:Local On").strip()
		LocalOFF = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Off").strip()
		FailSafeOpen = instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe").strip()
		ProcessOutput = instance.getAttributeData("FEDeviceOutputs:Process Output").strip()
		ProcessOutputOff = instance.getAttributeData("FEDeviceOutputs:Process Output Off").strip()
		PulseDuration = instance.getAttributeData("FEDeviceParameters:Pulse Duration (s)").strip()
		LabelOn = instance.getAttributeData("SCADADeviceGraphics:Label On").strip()
		LabelOff = instance.getAttributeData("SCADADeviceGraphics:Label Off").strip()
		
		if PulseDuration <> "":
			Pulse = True
		else:
			Pulse = False
		
		if LocalDrive <> "":
			subString = LocalDrive[0:4].lower()
			if subString == "not ":
				LocalDrive = LocalDrive[4:]
			else:
				LocalDrive = LocalDrive
		
		if FeedbackOn <> "":
			FeedbackOnExist = self.theSemanticVerifier.doesObjectExist(FeedbackOn, theUnicosProject)
			if FeedbackOnExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The FeedbackOn $FeedbackOn$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")
		if FeedbackOff <> "":
			FeedbackOffExist = self.theSemanticVerifier.doesObjectExist(FeedbackOff, theUnicosProject)
			if FeedbackOffExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The FeedbackOff $FeedbackOff$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")		
		if LocalDrive <> "":
			LocalDriveExist = self.theSemanticVerifier.doesObjectExist(LocalDrive, theUnicosProject)
			if LocalDriveExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The LocalDrive $LocalDrive$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")
		if LocalON <> "":
			LocalONExist = self.theSemanticVerifier.doesObjectExist(LocalON, theUnicosProject)
			if LocalONExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The LocalON $LocalON$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")		
		if LocalOFF <> "":
			LocalOFFExist = self.theSemanticVerifier.doesObjectExist(LocalOFF, theUnicosProject)
			if LocalOFFExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The LocalOFF $LocalOFF$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")		
				
		# Fail Safe Warning
		if FailSafeOpen.lower() == "2 do on" or (FailSafeOpen.lower() == "on/open" and Pulse and ProcessOutputOff == "") or (FailSafeOpen.lower() == "2 do off" and not Pulse and ProcessOutputOff <> ""):
			self.thePlugin.writeWarningInUABLog("ONOFF instance: $Name$. The Fail Safe Position '$FailSafeOpen$' is ambiguous because Start Interlock cannot be applied ")
		
		# Fail Safe Error		
		if (FailSafeOpen.lower() == "2 do off" or FailSafeOpen.lower() == "2 do on") and ProcessOutputOff == "":
			self.thePlugin.writeErrorInUABLog("ONOFF instance: $Name$. The Fail Safe Position '$FailSafeOpen$' cannot be selected when there is no Process Output Off")


		# FEDevice outputs verification
		if ProcessOutput <> "":
			ProcessOutputExist = self.theSemanticVerifier.doesObjectExist(ProcessOutput, theUnicosProject)
			if ProcessOutputExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The ProcessOutput $ProcessOutput$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")
		if ProcessOutputOff <> "":
			ProcessOutputOffExist = self.theSemanticVerifier.doesObjectExist(ProcessOutputOff, theUnicosProject)
			if ProcessOutputOffExist is not True:
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The ProcessOutputOff $ProcessOutputOff$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")
		# Labels
		if len(LabelOn) > 13:
			self.thePlugin.writeWarningInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The LabelOn field's length ($LabelOn$) is > 13 characters. Text may wrap around on the faceplate. ")
		if len(LabelOff) > 13:
			self.thePlugin.writeWarningInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The LabelOff field's length ($LabelOff$) is > 13 characters. Text may wrap around on the faceplate. ")
			
   def end(self):
	self.thePlugin.writeInUABLog("check rules: end")

   def shutdown(self):
	self.thePlugin.writeInUABLog("check rules: shutdown")
		
