# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

class DigitalParameter_Template(IUnicosTemplate):
   theSemanticVerifier = 0
   thePlugin = 0
   isDataValid = 1
   theCurrentDeviceType = 0
   
   def initialize(self):
   	self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("check rules: initialize")

   def check(self):
	self.thePlugin.writeInUABLog("check rules: check")

   def begin(self):
	self.thePlugin.writeInUABLog("check rules: begin")
	
   def process(self, *params): 
	theCurrentDeviceTypeName = params[0]
	theCurrentDeviceTypeDefinition = params[1]
	theUnicosProject = self.thePlugin.getUnicosProject()
   	theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)	
   	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	self.thePlugin.writeInUABLog(""+str(theCurrentDeviceTypeName)+" Specific Semantic Rules")
	
	# 1. checking the length of the names
   	for instance in instancesVector :
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		Remarks = instance.getAttributeData("DeviceDocumentation:Remarks")
		nameSize = len(Name)
		theManufacturer = self.thePlugin.getPlcManufacturer()
		if (theManufacturer.lower() == "siemens") and nameSize > 24:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 24")
			
		elif (theManufacturer.lower() == "schneider") and nameSize > 23:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 23")
		
			
   def end(self):
	self.thePlugin.writeInUABLog("check rules: end")

   def shutdown(self):
	self.thePlugin.writeInUABLog("check rules: shutdown")
		
