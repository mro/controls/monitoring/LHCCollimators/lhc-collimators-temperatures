# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

class AnalogStatus_Template(IUnicosTemplate):
   theSemanticVerifier = 0
   thePlugin = 0
   isDataValid = 1
   theCurrentDeviceType = 0
   
   def initialize(self):
   	self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("check rules: initialize")

   def check(self):
	self.thePlugin.writeInUABLog("check rules: check")

   def begin(self):
	self.thePlugin.writeInUABLog("check rules: begin")
	
   def process(self, *params): 
	theCurrentDeviceTypeName = params[0]
	theCurrentDeviceTypeDefinition = params[1]
	theUnicosProject = self.thePlugin.getUnicosProject()
   	theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)	
   	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	self.thePlugin.writeInUABLog(""+str(theCurrentDeviceTypeName)+" Specific Semantic Rules")
	
   	for instance in instancesVector :
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type").strip()
		InterfaceParam1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1")
		InterfaceParam2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2")
		S_InterfaceParam1 = InterfaceParam1.lower().strip()
		S_InterfaceParam2 = InterfaceParam2.lower().strip()
		nameSize = len(Name)
		theManufacturer = self.thePlugin.getPlcManufacturer()
		# 1. checking the length of the names
		if (theManufacturer.lower() == "siemens") and nameSize > 24:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 24")
			
		elif (theManufacturer.lower() == "schneider") and nameSize > 23:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 23")
		
		# 2. Checking the FE Encoding Type
		if (theManufacturer.lower() == "siemens"):
			if (FEType <> "") and (FEType <> "0") and (FEType <> "1") and (FEType <> "101") and (FEType <> "100"):
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The FE Encoding Type defined "+FEType+" is not allowed.")
			elif (FEType == "1"):
				if S_InterfaceParam1 == "":
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 must be defined if the FE Encoding Type is $FEType$")
				else:
					if not S_InterfaceParam1.startswith('pid') and not S_InterfaceParam1.startswith('id'):
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is a DOUBLE WORD (PIDxxx or IDxxx where xxx is a number)")
					else:
						if not S_InterfaceParam1[S_InterfaceParam1.find('id')+2:].isnumeric():
							self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is a DOUBLE WORD (PIDxxx or IDxxx where xxx is a number)")

			elif (FEType == "101"):
				if S_InterfaceParam1 == "" or S_InterfaceParam2 == "":
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. InterfaceParam1 and InterfaceParam2 must *both* be defined if the FE Encoding Type is $FEType$")
				else:
					if len(S_InterfaceParam1) > 2 and S_InterfaceParam1[0:2] == "db":
						S_InterfaceParam1 = S_InterfaceParam1[2:]
					if len(S_InterfaceParam2) > 3 and S_InterfaceParam2[0:3] ==  "dbd":
						S_InterfaceParam2 = S_InterfaceParam2[3:]
					if not S_InterfaceParam1.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is DBxx, where xx is a number")
					if not S_InterfaceParam2.isnumeric():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam2 ($InterfaceParam2$) is not well defined. The correct format is DBDxx, where xx is a number")

			elif (FEType == "100"):
				if S_InterfaceParam1 == "":
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 must be defined if the FE Encoding Type is $FEType$")
				else:
					if not S_InterfaceParam1.startswith('md'):
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is a DOUBLE WORD (MDxxx where xxx is a number)")
					else:
						if not S_InterfaceParam1[S_InterfaceParam1.find('md')+2:].isnumeric():
							self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is a DOUBLE WORD (MDxxx where xxx is a number)")
			
			
   def end(self):
	self.thePlugin.writeInUABLog("check rules: end")

   def shutdown(self):
	self.thePlugin.writeInUABLog("check rules: shutdown")
		
