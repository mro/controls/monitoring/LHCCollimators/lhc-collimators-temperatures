# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime
from java.lang import System
import os
from research.ch.cern.unicos.utilities import AbsolutePathBuilder
import Shared_UCPC_SpecFunctions
reload (Shared_UCPC_SpecFunctions)

class convertSpecToText_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
    # Get a reference to the plug-in
   	self.thePlugin = APlugin.getPluginInterface()	
	# Get a reference to the specs file.
   	self.theUnicosProject = self.thePlugin.getUnicosProject()
	self.thePlugin.writeInUABLog("initialize convertSpecToText_Template.")

   def check(self):
	self.thePlugin.writeInUABLog("check convertSpecToText_Template.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for convertSpecToText_Template.")
	self.dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'
	self.theApplicationName = self.thePlugin.getApplicationName()
	self.thePluginId = self.thePlugin.getId()
	self.thePluginVersion = self.thePlugin.getVersionId()
	self.theUserName = System.getProperty("user.name")
	self.CRLF = System.getProperty("line.separator")
		
   def process(self, *params):   	
	self.thePlugin.writeInUABLog("Starting processing convertSpecToText_Template.")
	#Get the absolute path of the specs
	absolutePath = self.theUnicosProject.getSpecsPath()
	#Get the relative path of the specs
	filename = os.path.basename(absolutePath)
	self.thePlugin.writeDebugInUABLog("filename = $filename$")
	
	outputText = Shared_UCPC_SpecFunctions.convert2text(self.thePlugin,self.theUnicosProject)
	
	self.thePlugin.writeFile(filename + ".converted.txt",outputText)
	self.thePlugin.writeInfoInUABLog("Created " + filename + ".converted.txt")
	
   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for convertSpecToText_Template.")


   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for convertSpecToText_Template.")
		
