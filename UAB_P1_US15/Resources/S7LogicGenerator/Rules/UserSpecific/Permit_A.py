# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import S7Logic_DefaultAlarms_Template
reload(S7Logic_DefaultAlarms_Template)


def OnOffLogic(thePlugin, theRawInstances, master, name, LparamVector):

	Lparam1,Lparam2,Lparam3,Lparam4,Lparam5,Lparam6,Lparam7,Lparam8,Lparam9,Lparam10=S7Logic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

	
	thePlugin.writeSiemensLogic('''//
FUNCTION $name$_DL : VOID
TITLE = '$name$_DL'
//
// Dependent Logic of $name$
//
// Master: 	$master$
// Name: 	$name$
// Lparam1:	$Lparam1$
// Lparam2:	$Lparam2$
// Lparam3:	$Lparam3$
// Lparam4:	$Lparam4$
// Lparam5:	$Lparam5$
// Lparam6:	$Lparam6$
// Lparam7:	$Lparam7$
// Lparam8:	$Lparam8$
// Lparam9:	$Lparam9$
// Lparam10:	$Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_DL'
FAMILY: 'ONOFF'
VAR_TEMP
   old_status : DWORD;
END_VAR
BEGIN
''')
	thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

	# Step 1.2: Position Request calculation. The user should complet this part according the logic process. By default we put that to "0".
	
	if master.lower() <> "no_master":
			Permit_TM = theRawInstances.findMatchingInstances("DigitalParameter","'#DeviceIdentification:Name#'contains'$Lparam1$_Permit_TM'")
			for TP in Permit_TM:
				TPName = TP.getAttributeData ("DeviceIdentification:Name")
			PermitA = theRawInstances.findMatchingInstances("DigitalParameter","'#DeviceIdentification:Name#'contains'$Lparam1$_PermitA_T'")
			for TPA in PermitA:
				TPAName = TPA.getAttributeData ("DeviceIdentification:Name")
			PermitB = theRawInstances.findMatchingInstances("DigitalParameter","'#DeviceIdentification:Name#'contains'$Lparam1$_PermitB_T'")
			for TPB in PermitB:
				TPBName = TPB.getAttributeData ("DeviceIdentification:Name")
			DOPermitA = theRawInstances.findMatchingInstances("DigitalOutput","'#DeviceIdentification:Name#'contains'$Lparam1$_PermitA'")
			for TDOA in DOPermitA:
				TDOAName = TDOA.getAttributeData ("DeviceIdentification:Name")
			DOPermitB = theRawInstances.findMatchingInstances("DigitalOutput","'#DeviceIdentification:Name#'contains'$Lparam1$_PermitB'")
			for TDOB in DOPermitB:
				TDOBName = TDOB.getAttributeData ("DeviceIdentification:Name")
			MODE = theRawInstances.findMatchingInstances("WordStatus","'#DeviceIdentification:Name#'contains'$Lparam1$_MODE'")
			for TM in MODE:
				TMName = TM.getAttributeData ("DeviceIdentification:Name")
			DATempStop= theRawInstances.findMatchingInstances("DigitalAlarm","'#DeviceIdentification:Name#'contains'$Lparam1$_Permit'")				
			for TSDA in DATempStop:
				TSDAName = TSDA.getAttributeData ("DeviceIdentification:Name")
			thePlugin.writeSiemensLogic('''
			
	
(*Position Management*)
IF DB_DPAR_all.DPAR_SET.$TPName$.PosSt THEN
	DB_DA_All.DA_SET.$TSDAName$.AuEAl := FALSE;
	$name$.AuOnR := DB_DPAR_all.DPAR_SET.$TPAName$.PosSt;
	$name$.AuOffR := NOT $name$.AuOnR;
    DB_WS_all.WS_SET.$TMName$.AuposR := INT_TO_WORD(1);    // "BIC Test Mode" in Word Status
ELSE
	DB_DA_All.DA_SET.$TSDAName$.AuEAl := TRUE;
	$name$.AuOnR := TRUE;
	$name$.AuOffR := NOT $name$.AuOnR;
    DB_WS_all.WS_SET.$TMName$.AuposR := INT_TO_WORD(0);
END_IF;

    $name$.AuAlAck :=  FALSE;
	
				''')
	else:
		thePlugin.writeSiemensLogic('''
(*Position Management*)
$name$.AuOnR := 0; // To complete
$name$.AuOffR := 0; // To complete
	''')


	# Step 1.4: Fill the IOError and IOSimu from the Inputs linked by the user logic (if procceds)
	thePlugin.writeSiemensLogic('''
(*IoSimu and IoError*****************)
// The user must connect the IOError and IOSimu from the linked devices ("IN" variable) if proceeds
DB_ERROR_SIMU.$name$_DL_E := 0; // To complete

DB_ERROR_SIMU.$name$_DL_S :=  0; // To complete
''')

	# Not configured alarms
	#Gets all the Digital Alarms that are child of the 'master' object
	theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition = S7Logic_DefaultAlarms_Template.getDigitalAlarms(theRawInstances, name)
	#Gets all the Analog Alarms that are child of the 'master' object
	theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition = S7Logic_DefaultAlarms_Template.getAnalogAlarms(theRawInstances, name)


	#-------------------------------------------------------------------------------------------------------------
	# The "writeNotConfiguredDAParameters" function writes default values for DA parameters: 
	#
	# If Input (in spec) is empty, writes IOError, IOSimu and I
	# Else if Input is not empty, IOError, IOSimu and I will be written by main template function
	# If Delay (in spec) is "logic", writes PAlDt
	# Else if Delay is not "logic", PAlDt will be written by main template function
	#
	# NOTE: the user can choose to remove this function call from the user template, in which case he will be 
	# responsible for writing the code for the appropriate pins himself
	#
	#-------------------------------------------------------------------------------------------------------------

	#-------------------------------------------------------------------------------------------------------------
	# The "writeNotConfiguredAAParameters" function writes default values for AA parameters: 
	#
	# If HH Alarm (in spec) is a number, writes AuEHH. In this case HH will be written by main template function
	# If HH Alarm is "logic", writes AuEHH and HH
	# If HH Alarm is a reference to an object, writes AuEHH. In this case HH will be written by main template function
	# NOTE: the same applies for H Warning, L Warning, and LL Alarm
	# If Input (in spec) is empty, writes IOError, IOSimu and I
	# If Delay (in spec) is "logic", writes PAlDt
	#
	# NOTE: the user can choose to remove this function call from the user template, in which case he will be 
	# responsible for writing the code for the appropriate pins himself
	#
	#-------------------------------------------------------------------------------------------------------------
	S7Logic_DefaultAlarms_Template.writeNotConfiguredAAParameters(thePlugin, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition)
	theAAAlAlarms = theRawInstances.findMatchingInstances("AnalogAlarm","'#LogicDeviceDefinitions:Master#'contains'$Lparam1$_A_OF'")
	allTheAAAlAlarms = Vector(theAAAlAlarms)
	thePlugin.writeSiemensLogic('''
    (*Digital Alarms not configured*)
DB_DA_All.DA_SET.$TSDAName$.I := 
''')

	generatedTextAA = theRawInstances.createSectionText(allTheAAAlAlarms, 1, 1, '''
	(DB_AA_All.AA_SET.#DeviceIdentification:Name#.ISt AND (DB_AI_All.AI_SET.#FEDeviceEnvironmentInputs:Input#.PosSt <
	DB_APAR_All.APAR_SET.$master$_AA_MAX_T.PosSt)) OR''')
	thePlugin.writeSiemensLogic(generatedTextAA)
	thePlugin.writeSiemensLogic('''
	0;
	
	DB_DA_All.DA_SET.$TSDAName$.IOError := 
	''')

	generatedTextAAer = theRawInstances.createSectionText(allTheAAAlAlarms, 1, 1, '''
	DB_AA_All.AA_SET.#DeviceIdentification:Name#.IOErrorW OR''')
	thePlugin.writeSiemensLogic(generatedTextAAer)
	thePlugin.writeSiemensLogic('''
	0;
	DB_DA_All.DA_SET.$TSDAName$.IOSimu := 
	''')
	generatedTextAAsim = theRawInstances.createSectionText(allTheAAAlAlarms, 1, 1, '''
	DB_AA_All.AA_SET.#DeviceIdentification:Name#.IOSimuW OR''')
	thePlugin.writeSiemensLogic(generatedTextAAsim)
	thePlugin.writeSiemensLogic('''
	0;
	
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
