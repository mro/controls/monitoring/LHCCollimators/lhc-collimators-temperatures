# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
import Reverse_Generic_Template
reload(Reverse_Generic_Template)
import Reverse_GlobalFunctions_Template
reload(Reverse_GlobalFunctions_Template)
    
class MassFlowController_Template(Reverse_Generic_Template.Generic_Template): 
    theDeviceType = "MassFlowController"
    
    def processInstance(self, instance, generatedInstance):
        key,value = self.getDescriptionWithoutElectricalDiagram(instance) #overwrite result from generic function
        generatedInstance.put(key,value)
        
        key,value = self.getMaster(instance)
        generatedInstance.put(key,value)        

        key,value = self.getMaskEvent(instance)
        generatedInstance.put(key,value)    
        
        #Flow Unit and Format
        value = instance["flowUnit"]  
        generatedInstance.put("SCADADeviceGraphics.Flow.Unit", value)  
        
        value = instance["flowFormat"]
        generatedInstance.put("SCADADeviceGraphics.Flow.Format", value)  
        
        #Totalizer Unit and Format
        value = instance["totalizerUnit"]  
        generatedInstance.put("SCADADeviceGraphics.Totalizer.Unit", value)  
        
        value = instance["totalizerFormat"]
        generatedInstance.put("SCADADeviceGraphics.Totalizer.Format", value)  
        
        key,value = self.getDriverDeadbandType(instance, path="SCADADriverDataSmoothing.Flow", field="driverFlowDbType")
        generatedInstance.put(key,value)
        
        key,value = self.getDriverDeadbandValue(instance, path="SCADADriverDataSmoothing.Flow", field="driverDeadbandValue")
        generatedInstance.put(key,value)
        
        key,value = self.getDriverDeadbandType(instance, path="SCADADriverDataSmoothing.Totalizer", field="driverTotalizerDbType")
        generatedInstance.put(key,value)
        
        key,value = self.getDriverDeadbandValue(instance, path="SCADADriverDataSmoothing.Totalizer", field="driverTotalizerDbValue")
        generatedInstance.put(key,value)
        
        #Flow archive
        keyArchive,valueArchive,keyDbType,valueDbType,keyDbValue,valueDbValue = self.getArchiveModeDeadbandTypeDeadbandValueWithTimeFilterWithDeadBand(instance, path = "SCADADeviceDataArchiving.Flow", fieldArchive = "archiveFlowMode", fieldFilter = "archiveFlowTime")
        generatedInstance.put(keyArchive,valueArchive)
        generatedInstance.put(keyDbType,valueDbType)
        generatedInstance.put(keyDbValue,valueDbValue)
        
        key,value = self.getTimeFilter(instance, path="SCADADeviceDataArchiving.Flow", field="archiveFlowTime")
        generatedInstance.put(key,value)
        
        #Totalizer archive
        keyArchive,valueArchive,keyDbType,valueDbType,keyDbValue,valueDbValue = self.getArchiveModeDeadbandTypeDeadbandValueWithTimeFilterWithDeadBand(instance, path = "SCADADeviceDataArchiving.Totalizer", fieldArchive = "archiveTotalizerMode", fieldFilter = "archiveTotalizerTime")
        generatedInstance.put(keyArchive,valueArchive)
        generatedInstance.put(keyDbType,valueDbType)
        generatedInstance.put(keyDbValue,valueDbValue)
        
        key,value = self.getTimeFilter(instance, path="SCADADeviceDataArchiving.Totalizer", field="archiveTotalizerTime")
        generatedInstance.put(key,value)
        
        key, value = self.getArchiveMode(instance, path = "SCADADeviceDataArchiving.Modes", field = "archiveModeMode")
        generatedInstance.put(key,value)
        
        #Calibration unit
        value = instance["calibrationUnit"]  
        generatedInstance.put("SCADADeviceGraphics.Calibration.Unit", value)  
        
        #Calibration gas name
        value = instance["calibrationGasName"]  
        generatedInstance.put("SCADADeviceGraphics.Calibration.GasName", value)  
        
        #Calibration curves
        value = instance["calibrationCurves"] 
        values = value.split(",")
        key = "SCADADeviceGraphics.CurveName.CC"
        for value in values:
            val = value.split("=")
            key = "SCADADeviceGraphics.CurveName.CC" + val[0]
            generatedInstance.put(key, val[1])
            
        key,value = self.getFailSafe(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getManualRestartAfterFullStop(instance)
        generatedInstance.put(key,value)    
        
        key,value = self.getFeedbackOff(instance)
        generatedInstance.put(key,value)  
        
        key,value = self.getInhibitTotalizer(instance)
        generatedInstance.put(key,value)  
        
        key,value = self.getConvertRatio(instance)
        generatedInstance.put(key,value)  
        
        key,value = self.getNoiseFilter(instance)
        generatedInstance.put(key,value)  
        
        
        
        
