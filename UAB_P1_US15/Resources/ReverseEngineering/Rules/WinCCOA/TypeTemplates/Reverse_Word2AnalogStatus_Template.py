# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
import Reverse_Generic_Template
reload(Reverse_Generic_Template)
import Reverse_GlobalFunctions_Template
reload(Reverse_GlobalFunctions_Template)
    
class Word2AnalogStatus_Template(Reverse_Generic_Template.Generic_Template): 
    theDeviceType = "Word2AnalogStatus"
    
    def processInstance(self, instance, generatedInstance):
        generatedInstance.put("deviceType","WordStatus")
        
        key,value = self.getDescriptionWithoutElectricalDiagram(instance) #overwrite result from generic function
        generatedInstance.put(key,value)
        
        key,value = self.getUnit(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getFormat(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getDriverDeadbandValue(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getDriverDeadbandType(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getRangeMin(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getRangeMax(instance)
        generatedInstance.put(key,value)
        
        keyArchive,valueArchive,keyDbType,valueDbType,keyDbValue,valueDbValue = self.getArchiveModeDeadbandTypeDeadbandValueWithTimeFilterWithDeadBand(instance)
        generatedInstance.put(keyArchive,valueArchive)
        generatedInstance.put(keyDbType,valueDbType)
        generatedInstance.put(keyDbValue,valueDbValue)
        
        key,value = self.getTimeFilter(instance)
        generatedInstance.put(key,value)
        
        
        value = instance["fRangeMin"]
        generatedInstance.put("SCADADeviceParameters.FMinRan",value)
        value = instance["fRangeMax"]
        generatedInstance.put("SCADADeviceParameters.FMaxRan",value)
