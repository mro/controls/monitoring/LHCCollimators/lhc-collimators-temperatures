# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class CompilationInstances_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0 
   thePluginId = 0
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
   	self.thePluginId = self.thePlugin.getId()
	self.thePlugin.writeInUABLog("initialize in Jython for Compilation.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for Compilation.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for Compilation.")

   def process(self, *params): 
	self.thePlugin.writeInUABLog("processInstances in Jython for Compilation.")
	theOverallInstances = params[0]
	theXMLConfig = params[1]
	genGlobalFilesForAllTypes = params[2].booleanValue() # Comes from "Global files scope" dropdown on Wizard. true = All types. false = Selected types.
		
	# General Steps for the nstances compilation file:
	# 1. To compile the instances files the next order is required:
	#	- IOObjects
	#	- DPARObject (the others interface objects uses the DPAR object, so it must be compiled before them)
	#	- InterfaceObjects
	#	- FieldObject
	#	- ControlObject
	#	- COMMUNICATION
	# Note: to follow this order, a set of vectors must be created to preprocess all the DeviceTypes and call the files in the right order
	
	typesVector = theOverallInstances.getAllDeviceTypes()
	IOObjectVector = []
	InterfaceObjectVector = []
	DPARVector = []
	FieldObjectVector = []
	ControlObjectVector = []
	
	for currentType in typesVector :
		typeName = currentType.getDeviceTypeName()
		ObjectTypeFamily = currentType.getObjectType()
		instNb = currentType.getAllDeviceTypeInstances().size()
		TypeToProccess = theXMLConfig.getTechnicalParametersMap(self.thePluginId + ":UNICOSTypesToProcess").get(typeName)
		if (TypeToProccess == "true" or genGlobalFilesForAllTypes) and (instNb <> 0):
			if ObjectTypeFamily == "IOObjectFamily":
				RepresentationName = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", typeName)
				IOObjectVector.append(RepresentationName)
			if ObjectTypeFamily == "InterfaceObjectFamily":
				RepresentationName = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", typeName)
				if RepresentationName == "DPAR":
					DPARVector.append(RepresentationName)
				else:
					InterfaceObjectVector.append(RepresentationName)
			if ObjectTypeFamily == "FieldObjectFamily":
				RepresentationName = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", typeName)
				FieldObjectVector.append(RepresentationName)
			if ObjectTypeFamily == "ControlObjectFamily":
				RepresentationName = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", typeName)
				ControlObjectVector.append(RepresentationName)
				
	for IOObject in IOObjectVector :
		self.thePlugin.writeInstanceInfoglobal('''$IOObject$;''')
	
	for DPARObject in DPARVector :
		self.thePlugin.writeInstanceInfoglobal('''$DPARObject$;''')
	
	for InterfaceObject in InterfaceObjectVector :
		self.thePlugin.writeInstanceInfoglobal('''$InterfaceObject$;''')
		
	for FieldObject in FieldObjectVector :
		self.thePlugin.writeInstanceInfoglobal('''$FieldObject$;''')		
		
	for ControlObject in ControlObjectVector :
		self.thePlugin.writeInstanceInfoglobal('''$ControlObject$;''')						
				
	self.thePlugin.writeInstanceInfoglobal('''COMMUNICATION;''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for Compilation.")


   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for Compilation.")
		
