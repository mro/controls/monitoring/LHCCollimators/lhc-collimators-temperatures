# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class Local_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for LOCAL.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for LOCAL.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for LOCAL.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'

   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for LOCAL.")
	
	# General Steps for Non-Optimized objects:
	# 1. DB creation and initialization for each instance
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 
	# 3. Create all the needed Structures from the TCT
 	# 4. Create all the need DBs to store the required signal from the object
	# 5: Create the Function. This function writes the input signals on each instance DB.
	# 		  For each DB we execute the FB.DB 
	# 		  And finally we pass the Status information from this instance DB to the corresponding Status DB (e.g DB_bin_status_ANALOG)
	
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	# This method is called on every Instance of the current type by the Code Generation plug-in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	
	# Step 1: DB creation and initialization for each instance	
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Local DB Creation file: UNICOS application
''')

	for instance in instancesVector :
		if (instance is not None):
						
			Name = instance.getAttributeData("DeviceIdentification:Name")
			PAnim = instance.getAttributeData ("FEDeviceParameters:ParReg:Full/Empty Animation")
			PosAl = instance.getAttributeData ("FEDeviceParameters:ParReg:Position Alarm")	
			FeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On")
			FeedbackOff = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Off")
			if FeedbackOn == "":
				bit9='0'
			else:
				bit9='1'
			if FeedbackOff == "":
				bit10='0'
			else:
				bit10='1'
			if PAnim.strip() == "Full/Empty":
				bit14='1'
			else:
				bit14='0'
			if (PosAl.lower() == "false" or PosAl.strip() == ""):
				bit2='0'
			else:
				bit2='1'
			
			self.thePlugin.writeInstanceInfo('''
DATA_BLOCK '''+Name+''' CPC_FB_LOCAL
BEGIN
	PLocal.ParReg :=  2#00000$bit2$000$bit14$000$bit10$$bit9$0;
END_DATA_BLOCK
''')
			

	# Step 3. Create all the needed Structures from the TCT
	# UDT for DB_LOCAL_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE LOCAL_ManRequest
TITLE = LOCAL_ManRequest
//
// parameters of LOCAL Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				attributePrimitiveType = attribute.getPrimitiveType()
				attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
				
				self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the bin status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE LOCAL_bin_Status
TITLE = LOCAL_bin_Status
//
// parameters of LOCAL Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE LOCAL_event
TITLE = LOCAL_event
//
// parameters of AI Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# Step 4: Create all the need DBs to store the required signal from the object
	
	# DB_LOCAL_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_LOCAL_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_LOCAL_ManRequest
TITLE = DB_LOCAL_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    LOCAL_Requests : ARRAY [1..'''+instanceNb+'''] OF LOCAL_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')


	# Step 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 

	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_LOCAL Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_LOCAL
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type LOCAL
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_LOCAL : INT := '''+str(NbType)+''';
	LOCAL_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF LOCAL_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT
BEGIN
END_DATA_BLOCK

''')		
	
	self.thePlugin.writeInstanceInfo('''
	
(*binaries Status of the LOCAL OBJECTS************************************************)
DATA_BLOCK DB_bin_status_LOCAL
TITLE = 'DB_bin_status_LOCAL'
//
// Global binary status DB of LOCAL
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : LOCAL_bin_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK

(*old binaries Status of the LOCAL OBJECTS************************************************)
DATA_BLOCK DB_bin_status_LOCAL_old
TITLE = 'DB_bin_status_LOCAL_old'
//
// Old Global binary status DB of LOCAL
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
	''')
	
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : LOCAL_bin_Status;
''')

	# Step 5: Create the Function. This function writes the input signals on each instance DB.
	# For each DB we execute the FB.DB 
	# and finally we pass the Status information from this instance DB to the corresponding Status DB (e.g DB_bin_status_LOCAL)
	
	self.thePlugin.writeInstanceInfo('''
END_STRUCT
BEGIN

END_DATA_BLOCK

FUNCTION FC_LOCAL : VOID
TITLE = 'FC_LOCAL'
//
// LOCAL calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_LOC'
FAMILY: 'Local'
VAR_TEMP
	old_status1 : DWORD;
END_VAR
BEGIN

(*EXEC of LOCALS************************************************)
''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		FeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On")
		FeedbackOff = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Off")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
// ----------------------------------------------
// ---- Local <'''+ str(RecordNumber) +'''>: $Name$
// ----------------------------------------------
old_status1 := DB_bin_status_LOCAL.$Name$.StsReg01;
old_status1 := ROR(IN:=old_status1, N:=16);


$Name$.ManReg01:=DB_LOCAL_ManRequest.LOCAL_Requests['''+ str(RecordNumber) +'''].ManReg01;


''')

		if FeedbackOn <> "":
			s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HFOn:='''+s7db_id_result+FeedbackOn+'''.PosSt;''')	
			
		if FeedbackOff <> "":
			s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HFOff:='''+s7db_id_result+FeedbackOff+'''.PosSt;''')

#		if FeedbackOn <> "":
#			self.thePlugin.writeInstanceInfo('''
#$Name$.PHFOn:=1;''')

#		if FeedbackOff <> "":
#			self.thePlugin.writeInstanceInfo('''
#$Name$.PHFOff:=1;''')
			
		self.thePlugin.writeInstanceInfo('''
		
// IOError
''')
		if (FeedbackOn == "" and FeedbackOff == ""):
			self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOError
// No FeedbackOn and FeedbackOff configured
''')
		else:
			self.thePlugin.writeInstanceInfo(Name+'''.IoError := ''')
			NbIOError = 0
			if FeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOn+'''.IoErrorW''')
				NbIOError = NbIOError +1 
			
			if FeedbackOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOff+'''.IoErrorW''')
				NbIOError = NbIOError +1 
			
			self.thePlugin.writeInstanceInfo(''';
				
// IOSimu
''')

		if (FeedbackOn == "" and FeedbackOff == ""):
			self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOSimu
// No FeedbackOn and FeedbackOff configured
''')
		else:
			self.thePlugin.writeInstanceInfo(Name+'''.IoSimu := ''')
			NbIOSImu = 0
			if FeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOn + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOn+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if FeedbackOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOff, "DigitalInput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOff + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOff+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
		self.thePlugin.writeInstanceInfo(''';

// Calls the Baseline function			
CPC_FB_LOCAL.$Name$();''')
		
		self.thePlugin.writeInstanceInfo('''
		
DB_bin_status_LOCAL.$Name$.stsreg01:= $Name$.Stsreg01;
DB_Event_LOCAL.LOCAL_evstsreg['''+str(RecordNumber)+'''].evstsreg01 := old_status1 OR DB_bin_status_LOCAL.$Name$.stsreg01;
''')
	
	self.thePlugin.writeInstanceInfo('''
	
END_FUNCTION''')


   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for LOCAL.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for LOCAL.")
		
