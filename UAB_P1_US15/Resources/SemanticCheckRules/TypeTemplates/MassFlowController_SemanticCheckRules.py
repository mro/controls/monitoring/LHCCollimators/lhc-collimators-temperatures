# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

class MassFlowController_Template(IUnicosTemplate):
   theSemanticVerifier = 0
   thePlugin = 0
   isDataValid = 1
   theCurrentDeviceType = 0
   
   def initialize(self):
   	self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("check rules: initialize")

   def check(self):
	self.thePlugin.writeInUABLog("check rules: check")

   def begin(self):
	self.thePlugin.writeInUABLog("check rules: begin")
	
   def process(self, *params): 
	theCurrentDeviceTypeName = params[0]
	theCurrentDeviceTypeDefinition = params[1]
	theUnicosProject = self.thePlugin.getUnicosProject()
	theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)	
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	self.thePlugin.writeInUABLog(""+str(theCurrentDeviceTypeName)+" Specific Semantic Rules")
	
	# 1. checking the length of the names
   	for instance in instancesVector :
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		Remarks = instance.getAttributeData("DeviceDocumentation:Remarks")
		nameSize = len(Name)
		theManufacturer = self.thePlugin.getPlcManufacturer()
		if (theManufacturer.lower() == "siemens") and nameSize > 21:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 21")
			
		elif (theManufacturer.lower() == "schneider") and nameSize > 23:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 23")
		
		# FEDevice inputs verification
		# FeedbackOn = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On").strip()
		# FeedbackOff = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off").strip()
		# LocalDrive = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Drive").strip()
		# FeedbackAnalog = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Analog").strip()
		# HardwareAnalogOutput = instance.getAttributeData("FEDeviceEnvironmentInputs:Hardware Analog Output").strip()
		# if FeedbackOn <> "":
			# FeedbackOnExist = self.theSemanticVerifier.doesObjectExist(FeedbackOn, theUnicosProject)
			# if FeedbackOnExist is not True:
				# self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The FeedbackOn $FeedbackOn$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")


		# FEDevice outputs verification
		# ProcessOutput = instance.getAttributeData("FEDeviceOutputs:Process Output").strip()
		# if ProcessOutput <> "":
			# ProcessOutputExist = self.theSemanticVerifier.doesObjectExist(ProcessOutput, theUnicosProject)
			# if ProcessOutputExist is not True:
				# self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The ProcessOutput $ProcessOutput$ doesn't exist in the device "+str(theCurrentDeviceTypeName)+".")
		
			
   def end(self):
	self.thePlugin.writeInUABLog("check rules: end")

   def shutdown(self):
	self.thePlugin.writeInUABLog("check rules: shutdown")
		
