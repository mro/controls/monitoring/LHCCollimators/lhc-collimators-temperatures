# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# DeviceTypes Common check rules
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin
from research.ch.cern.unicos.utilities import DeviceTypeFactory
import re
import os

class AllDeviceTypes_Template(IUnicosTemplate):
   theSemanticVerifier = 0
   thePlugin = 0
   theCurrentDeviceType = 0
   
   def initialize(self):
   	self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("Device Types Common check rules: initialize")

   def check(self):
	self.thePlugin.writeInUABLog("Device Types Common check rules: check")

   def begin(self):
	self.thePlugin.writeInUABLog("Device Types Common check rules: begin")
	
   def process(self, *params): 
	theCurrentDeviceTypeName = params[0]
	theCurrentDeviceTypeDefinition = params[1]
	theUnicosProject = self.thePlugin.getUnicosProject()
   	self.theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)
	
   	# Common check: instances against type definition
   	self.theSemanticVerifier.areInstancesValidToTypeDefinition(self.theCurrentDeviceType, theCurrentDeviceTypeDefinition)   	
   	
   	# Name of the xml tag containing the master device name
   	theMasterDeviceTagName = str("LogicDeviceDefinitions:Master")

   	# Master object existance
   	self.theSemanticVerifier.doesMasterObjectExist(self.theCurrentDeviceType, theMasterDeviceTagName, theCurrentDeviceTypeDefinition, theUnicosProject)
   	
	# Name of the xml tag containing the device name
   	theDeviceIdentificationTagName = str("DeviceIdentification:Name")
	
	# Expert Name of the xml tag containing the device name
   	theDeviceIdentificationTagExpertName = str("DeviceIdentification:Expert Name")
   	   	
	# Determine if CustomLogicParameters exist
	deviceTypeDefinition = DeviceTypeFactory.getInstance().getDeviceType(theCurrentDeviceTypeName)
	CustomLogicParametersExist = deviceTypeDefinition.doesSpecificationAttributeExist("LogicDeviceDefinitions:CustomLogicParameters:Parameter1")
	#self.thePlugin.writeWarningInUABLog("Device type: $theCurrentDeviceTypeName$ : $CustomLogicParametersExist$")
	CustomLogicSectionsExist = 0
	CustomLogicSections = []
	deviceTypeFamilies = deviceTypeDefinition.getAttributeFamily()
	for deviceTypeFamily in deviceTypeFamilies:
		familyName = deviceTypeFamily.getAttributeFamilyName()
		#self.thePlugin.writeWarningInUABLog("familyName = $familyName$")
		if familyName == "LogicDeviceDefinitions":
			attributes = deviceTypeFamily.getAttribute()
			for attribute in attributes:
				attributeName = attribute.getAttributeName()
				#self.thePlugin.writeWarningInUABLog("attributeName = $attributeName$")
				if attributeName == "CustomLogicSections":
					CustomLogicSectionsExist = 1
					subattributes = attribute.getAttribute()
					for subattribute in subattributes:
						subattributeName = subattribute.getAttributeName()
						#self.thePlugin.writeWarningInUABLog("subattributeName = $subattributeName$")
						isSpecificationAttribute = subattribute.getIsSpecificationAttribute()
						if isSpecificationAttribute != None:
							nameRepresentation = isSpecificationAttribute.getNameRepresentation()
							CustomLogicSections.append(nameRepresentation)
							#self.thePlugin.writeWarningInUABLog("nameRepresentation = $nameRepresentation$")
	
	SMSCategoryExist = deviceTypeDefinition.doesSpecificationAttributeExist("SCADADeviceAlarms:Alarm Config:SMS Category")	
		
   	instancesVector = self.theCurrentDeviceType.getAllDeviceTypeInstances()
   	for instance in instancesVector:
   		self.theSemanticVerifier.isInstanceAliasUnique(self.theCurrentDeviceType, instance, theDeviceIdentificationTagName, theUnicosProject)

		# Common Name semantic rules
		Name = instance.getAttributeData("DeviceIdentification:Name")
		
		# Forbidden characters from PVSS and PLC
		if re.match( ur'.*[\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>|\], /\\\-\.].*', Name): #\u0024 is unicode for dollar sign
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in Name: $Name$. None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|],/\\-. or space or dollar sign")
		
		# Checking if there is double underscore for all platforms, per IEC-61131-3 section 2.1.2 Identifiers, see UCPC-1389
		if re.match( ur'.*__.*', Name): 
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Double underscore (e.g. '__') is not allowed in the object Name")

		ExpertName =  instance.getAttributeData("DeviceIdentification:Expert Name")
		if ExpertName.strip() <>"":
			self.theSemanticVerifier.isInstanceAliasUnique(self.theCurrentDeviceType, instance, theDeviceIdentificationTagExpertName, theUnicosProject)
		
		if ExpertName <> "":
			# Forbidden characters from PVSS
			if re.match( ur'.*[\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>|\], /\\].*', ExpertName): #\u0024 is unicode for dollar sign
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in Expert Name: $ExpertName$. None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|],/\\ or space or dollar sign")

		AccessControlDomain =  instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain")
		if AccessControlDomain <> "":
			# Forbidden characters from PVSS
			if re.match( ur'.*[\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>\]].*', AccessControlDomain): #\u0024 is unicode for dollar sign
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in Access Control Domain: $AccessControlDomain$. None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>] or dollar sign")

		Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
		if Synoptic <> "":
			# Forbidden characters from PVSS
			if re.match( ur'.*[\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>|\] \-].*', Synoptic): #\u0024 is unicode for dollar sign
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in Synoptic: $Synoptic$. None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|]- or space or dollar sign")

		Diagnostic = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
		if Diagnostic <> "":
			# Forbidden characters from PVSS
			if re.match( ur'.*[\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>|\] \-].*', Diagnostic): #\u0024 is unicode for dollar sign
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in Diagnostic: $Diagnostic$. None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|]- or space or dollar sign")

		Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
		if Domain <> "":
			# Forbidden characters from PVSS
			if re.match( ur'.*[\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>\] /\\].*', Domain): #\u0024 is unicode for dollar sign
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in Domain: $Domain$. None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>]/\\ or space or dollar sign")

		Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
		if Nature <> "":
			# Forbidden characters from PVSS
			if re.match( ur'.*[\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>|\] /\\].*', Nature): #\u0024 is unicode for dollar sign
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in Nature: $Nature$. None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|]/\\ or space or dollar sign")
		
		DeviceLinkList = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ","")
		if DeviceLinkList <> "":
			# Forbidden characters from PVSS
			if re.match( ur'.*[\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>|\]/\\].*', DeviceLinkList): #\u0024 is unicode for dollar sign
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in Device Links: $DeviceLinkList$. None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|]/\\ or dollar sign")
                        for deviceLink in DeviceLinkList.split(","):
                                if not self.theSemanticVerifier.doesObjectExist(deviceLink, theUnicosProject):
                                        self.thePlugin.writeWarningInUABLog(str(theCurrentDeviceTypeName)+" instance: $Name$. Linked device does not exists: " + deviceLink)
		
		BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
		if BooleanArchive <> "":
			# Forbidden characters from PVSS
			if re.match( ur'.*[\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>|\]\.\-, /\\].*', BooleanArchive): #\u0024 is unicode for dollar sign
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in Boolean Archive: $BooleanArchive$. None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|].-,/\\ or space or dollar sign")
		
		AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
		if AnalogArchive <> "":
			# Forbidden characters from PVSS
			if re.match( ur'.*[\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>|\]\.\-, /\\].*', AnalogArchive): #\u0024 is unicode for dollar sign
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in Analog Archive: $AnalogArchive$. None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|].-,/\\ or space or dollar sign")
		
		EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")	
		if EventArchive <> "":
			# Forbidden characters from PVSS
			if re.match( ur'.*[\[:"\'@`#\u0024%\^&\*\?!;=\+~\(\)\{\}<>|\]\.\-, /\\].*', EventArchive): #\u0024 is unicode for dollar sign
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in Event Archive: $EventArchive$. None of the following characters are allowed: [:\"'@`#%^&*?!;=+~(){}<>|].-,/\\ or space or dollar sign")

		if SMSCategoryExist:
			SMSCategory = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:SMS Category")	
			if SMSCategory <> "":
				# Forbidden characters from PVSS
				if re.match( ur'.*[^0-9a-zA-Z_].*', SMSCategory): #\u0024 is unicode for dollar sign
					self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in SMS Category: $SMSCategory$. Allowed characters: 0..9, A..Z, _, a..z")

		FIRST_LOGIC_PARAM_INDEX=1
		LAST_LOGIC_PARAM_INDEX=10
		if CustomLogicParametersExist:
			for param in range(FIRST_LOGIC_PARAM_INDEX,LAST_LOGIC_PARAM_INDEX + 1):
				Lparam = instance.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter$str(param)$")
				if Lparam <> "":
					# Forbidden characters for PLC template: $
					if re.match( ur'.*["\'\u0024].*', Lparam): #\u0024 is unicode for dollar sign
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Illegal character in CustomLogicParameters:Parameter$str(param)$: $Lparam$. None of the following characters are allowed: \" ' or dollar sign. ")

		if CustomLogicSectionsExist:
			for logicSection in CustomLogicSections:
				logicSectionFilePath = instance.getAttributeData("LogicDeviceDefinitions:CustomLogicSections:$logicSection$").strip()
				if logicSectionFilePath != "":
					logicSectionFileName = os.path.basename(logicSectionFilePath)
					firstDigit = logicSectionFileName[0]
					if firstDigit.isdigit():
						self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. $logicSection$ ($logicSectionFileName$) cannot start with a digit [0-9]")

		# Checking for the character ";" for the WinCCOA importation line
		description = instance.getAttributeData("DeviceDocumentation:Description")
		remarks = instance.getAttributeData("DeviceDocumentation:Remarks")
		semicolonDescription = description.find(";")
		semicolonRemarks = remarks.find(";")
		if semicolonDescription >= 0:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The character ';' is not allowed (reserved for WinCCOA importation file), column Description")
		if semicolonRemarks >= 0:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The character ';' is not allowed (reserved for WinCCOA importation file), column Remarks")
		
		
		
			
			
   def end(self):
	self.thePlugin.writeInUABLog("Device Types Common check rules: end")

   def shutdown(self):
	self.thePlugin.writeInUABLog("Device Types Common check rules: shutdown")
		
