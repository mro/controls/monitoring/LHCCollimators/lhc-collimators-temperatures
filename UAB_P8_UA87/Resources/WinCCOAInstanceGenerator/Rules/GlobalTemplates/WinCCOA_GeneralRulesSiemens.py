# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.utilities import XMLConfigMapper
from research.ch.cern.unicos.plugins.interfaces import APlugin
from research.ch.cern.unicos.cpc.utilities.siemens import SiemensPLCMemoryMapper
from time import strftime
from java.lang import System



class ApplicationGeneral_Template(IUnicosTemplate):
   thePlugin = 0
   isDataValid = 1
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("Application Generation rules: initialize")

   def check(self):
	self.thePlugin.writeInUABLog("Application Generation rules: check")

   def begin(self):
	self.thePlugin.writeInUABLog("Application Generation rules: begin")
	self.dateAndTime = strftime("%Y-%m-%d %H:%M:%S")
	self.theUserName = System.getProperty("user.name")
	self.theTargetLocation = self.thePlugin.getDBTargetLocation()
   	#Plugin Information
   	self.thePluginId = self.thePlugin.getId()
   	self.thePluginVersion = self.thePlugin.getVersionId()
   	#Application information
   	self.theUniqueId = self.thePlugin.getApplicationUniqueID()
   	#Gets the plugin version
   	self.theWinCCOAVersion = self.thePlugin.getVersionId()
	
	
   def process(self, *params):
   	self.thePlugin.writeInUABLog("Application Generation rules: processApplicationData")
	theXMLConfig = params[0]
	CRLF = System.getProperty("line.separator")

	#Gets the application name
	theApplicationName = str(theXMLConfig.getApplicationParameter("GeneralData:ApplicationName"))  
	theUserTag = str(theXMLConfig.getApplicationParameter("GeneralData:UserTag"))

	#Writes the plugin information in the output file
	self.thePlugin.writeDBHeader("#$CRLF$# $self.thePluginId$ Version: $self.thePluginVersion$  -  $self.theUserName$  -  $self.dateAndTime$")
	self.thePlugin.writeDBHeader("#$CRLF$# UniqueID: $self.theUniqueId$  UserTag: $theUserTag$")
	self.thePlugin.writeDBHeader("#$CRLF$# Target location: $self.theTargetLocation$")

	
  	#PLC declarations
   	thePlcDeclarations = theXMLConfig.getSiemensPLCDeclarations()	
	
	#Get copy of instances, in order to be able to extract resource package version
	theOverallInstances = self.thePlugin.getUnicosProject()
	specVersion = theOverallInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeDBHeader("#$CRLF$# Spec version used for this generation: $specVersion$")
	self.thePlugin.writeDBHeader("#$CRLF$# Generated Objects:")
			
	# Computing the DB_WINCCOA address
	DB_WINCCOA_Address = self.thePlugin.getResourceAddress("DB_WINCCOA")
	if DB_WINCCOA_Address is None:
		self.thePlugin.writeErrorInUABLog("The 'DB_WINCCOA' resource has no address")
		return
 
	CPC_DB_VERSION_Address = self.thePlugin.getResourceAddress("CPC_DB_VERSION")
	if CPC_DB_VERSION_Address is None:
		self.thePlugin.writeErrorInUABLog("The 'CPC_DB_VERSION' resource has no address")
		return

   	for thePlcDeclaration in thePlcDeclarations:
   		if (thePlcDeclaration is not None):
   			# Common PLC Parameters
   			thePlcName = str(thePlcDeclaration.getPLCName().getValue())
   			if (thePlcName==""):
   				thePlcName = str(thePlcDeclaration.getPLCName().getDefaultValue())
   			thePlcType = str(thePlcDeclaration.getPLCType().getValue())
			self.thePlugin.writeDebugInUABLog("thePlcType = $thePlcType$")
   			thePlcIPAddress = str(theXMLConfig.getPLCParameter("SiemensSpecificParameters:EthernetParameters:IpAddressPlc"))
			if thePlcType == "S7-300" or thePlcType == "S7-300 PN/DP":
				thePlcTypeScada = "S7-300"
			elif thePlcType == "S7-400" or thePlcType == "S7-400H":
				thePlcTypeScada = "S7-400"
			self.thePlugin.writeDebugInUABLog("thePlcTypeScada = $thePlcTypeScada$")
			
   			# Siemens Specific Parameters: PLCS7Connection
   			theLocalId = theXMLConfig.getPLCParameter("SiemensSpecificParameters:PLCS7Connection:LocalId")
   			theLocalRack = theXMLConfig.getPLCParameter("SiemensSpecificParameters:PLCS7Connection:LocalRack")
   			theLocalSlot = theXMLConfig.getPLCParameter("SiemensSpecificParameters:PLCS7Connection:LocalSlot")
   			theLocalConnResource = theXMLConfig.getPLCParameter("SiemensSpecificParameters:PLCS7Connection:LocalConnResource")
   			  			
   			# Siemens Specific Parameters: AddressConfig
   			thePartnerRack = theXMLConfig.getPLCParameter("SiemensSpecificParameters:AddressConfig:PartnerRack")
   			thePartnerSlot = theXMLConfig.getPLCParameter("SiemensSpecificParameters:AddressConfig:PartnerSlot")
   			thePartnerConnResource = theXMLConfig.getPLCParameter("SiemensSpecificParameters:AddressConfig:PartnerConnResource")
   			theConnectionTimeout = theXMLConfig.getPLCParameter("SiemensSpecificParameters:AddressConfig:Timeout")
   			
			# S7-400H Parameters : PLC Config Parameters
			theRedundantModeEnabled = "false"
			if thePlcType == "S7-400H":
				theRedundantModeEnabled = theXMLConfig.getPLCParameter("S7-400HParameters:PlcConfigParameters:RedundantModeEnabled")
			
			# S7-400H Parameters : Ethernet Parameters
			
			theRedundantPlcIPAddress = theXMLConfig.getPLCParameter("S7-400HParameters:EthernetParameters:RedundantIpAddressPlc")
			
			# S7-400H Parameters : PLC S7 Connection
			theRedundantLocalId = theXMLConfig.getPLCParameter("S7-400HParameters:PLCS7Connection:RedundantLocalId")
			theRedundantLocalRack = theXMLConfig.getPLCParameter("S7-400HParameters:PLCS7Connection:RedundantLocalRack")
			theRedundantLocalSlot = theXMLConfig.getPLCParameter("S7-400HParameters:PLCS7Connection:RedundantLocalSlot")
			theRedundantLocalConnResource = theXMLConfig.getPLCParameter("S7-400HParameters:PLCS7Connection:RedundantLocalConnResource")
			
			#S7-400H Parameters : AddressConfig
			theRedundantPartnerConnResource = theXMLConfig.getPLCParameter("S7-400HParameters:AddressConfig:RedundantPartnerConnResource")
			theRedundantConnectionTimeout = theXMLConfig.getPLCParameter("S7-400HParameters:AddressConfig:RedundantTimeout")
			theRedundantPartnerRack = theXMLConfig.getPLCParameter("S7-400HParameters:AddressConfig:RedundantPartnerRack")
			theRedundantPartnerSlot = theXMLConfig.getPLCParameter("S7-400HParameters:AddressConfig:RedundantPartnerSlot")
			
   			# Address from the DB_WINCCOA_Address
   			thePlcDsIpAddress = "DB"+str(DB_WINCCOA_Address)+".DBD0"
   			thePlcCounterAddress = "DB"+str(DB_WINCCOA_Address)+".DBW4"
   			thePlcCommandAddress = "DB"+str(DB_WINCCOA_Address)+".DBW6"
   			thePlcInfoAddress = "DB"+str(DB_WINCCOA_Address)+".DBW16"
   			#thePlcVersionAddress = "DB"+str(DB_WINCCOA_Address)+".DBD26F"
   			theApplicationVersionAddress = "DB"+str(DB_WINCCOA_Address)+".DBD48F"
			theResourcePackageVersionMajorAddress = "DB"+str(DB_WINCCOA_Address)+".DBW52"
			theResourcePackageVersionMinorAddress = "DB"+str(DB_WINCCOA_Address)+".DBW54"
			theResourcePackageVersionSmallAddress = "DB"+str(DB_WINCCOA_Address)+".DBW56"
			
   			# Address from the CPC_DB_VERSION_Address
   			theBaselineVersionAddress = "DB"+str(CPC_DB_VERSION_Address)+".DBD0F"
			
			# Build Redundant Parameters for the Config Line
   			if theRedundantModeEnabled == "true":
				theLocalId = theLocalId + "|" + theRedundantLocalId
				theLocalRack = theLocalRack + "|" + theRedundantLocalRack
				theLocalSlot = theLocalSlot + "|" + theRedundantLocalSlot
				theLocalConnResource = theLocalConnResource + "|" + theRedundantLocalConnResource
				thePartnerRack = thePartnerRack + "|" + theRedundantPartnerRack
				thePartnerSlot = thePartnerSlot + "|" + theRedundantPartnerSlot
				thePartnerConnResource = thePartnerConnResource + "|" + theRedundantPartnerConnResource
				theConnectionTimeout = theConnectionTimeout + "|" + theRedundantConnectionTimeout
				thePlcIPAddress = thePlcIPAddress + "|" + theRedundantPlcIPAddress
				
				
   			# Resource Package Version string
   			theResourcePackageVersionString = self.thePlugin.getResourcesVersion()
   			
			self.thePlugin.writeDeleteStatement("# If the user needs to delete the DB, the DELETE keyword can be used as follows:")
			self.thePlugin.writeDeleteStatement("# ")
			self.thePlugin.writeDeleteStatement("# uncomment the line below to delete all the devices and the front-end, $thePlcName$")
			self.thePlugin.writeDeleteStatement("#Delete;" + thePlcName + ";")
			self.thePlugin.writeDeleteStatement("# uncomment the line below to delete all the devices of the front-end, $thePlcName$, of front-end application, $theApplicationName$")
			self.thePlugin.writeDeleteStatement("#Delete;" + thePlcName + ";" + theApplicationName + ";")
			self.thePlugin.writeDeleteStatement("# uncomment the line below to delete all the devices of the front-end, $thePlcName$ of front-end application, $theApplicationName$, of type CPC_AnalogInput, for example.")
			self.thePlugin.writeDeleteStatement("#Delete;" + thePlcName + ";" + theApplicationName + ";CPC_AnalogInput;")
			self.thePlugin.writeDeleteStatement("# uncomment the line below to delete all the devices of the front-end, $thePlcName$ of front-end application, $theApplicationName$, of type CPC_AnalogInput and a given device number, with leading zeros, i.e. 00018 will delete device 18")
			self.thePlugin.writeDeleteStatement("#Delete;" + thePlcName + ";" + theApplicationName + ";CPC_AnalogInput;00018;")
			self.thePlugin.writeDeleteStatement("# ")
			self.thePlugin.writeDeleteStatement("# For more information, see documentation of DELETE here: https://edms.cern.ch/file/1176169/6.0.0/unicos-pvss-unCore-data-flow.pdf#page=25")
			self.thePlugin.writeDeleteStatement("# ")
   			
   			#const unsigned UN_CONFIG_S7_LENGTH_CPC6 = 22; // CPC6
			#const unsigned UN_CONFIG_S7_PLC_TYPE=1; // NEW: (PROTOCOL,PLCMODEL) where PROTOCOL=MODBUS or S7 and PLCMODEL=free text; OLD: PLC type = S7-400, S7-300
			#const unsigned UN_CONFIG_S7_PLC_NAME=2; // Name of PLC
			#const unsigned UN_CONFIG_S7_APPLICATION=3; // Name of the application
			#const unsigned UN_CONFIG_S7_LOCAL_ID=4; // Local_ID(hex)
			#const unsigned UN_CONFIG_S7_LOCAL_RACK=5; // Local_Rack
			#const unsigned UN_CONFIG_S7_LOCAL_SLOT=6; // Local_Slot
			#const unsigned UN_CONFIG_S7_LOCAL_CONNRESOURCE=7; // Local_ConnectionResource(hex)
			#const unsigned UN_CONFIG_S7_PARTNER_RACK=8; // Partner_Rack
			#const unsigned UN_CONFIG_S7_PARTNER_SLOT=9; // Partner_Slot
			#const unsigned UN_CONFIG_S7_PARTNER_CONNRESOURCE=10; // Partner_ConnectionResource(hex)
			#const unsigned UN_CONFIG_S7_TIMEOUT=11; // Conexion
			#const unsigned UN_CONFIG_S7_PLC_IP=12; // PLC IP number
			#const unsigned UN_CONFIG_S7_ADD_IP=13; // IP send address
			#const unsigned UN_CONFIG_S7_ADD_COUNTER=14; // Counter alive address
			#const unsigned UN_CONFIG_S7_ADD_COMMANDINTERFACE=15; // Syncro, RequestAll address
			#const unsigned UN_CONFIG_S7_ADD_PLCINFO=16; // Additional PLC info
			#const unsigned UN_CONFIG_S7_CPC6_PLC_BASELINE_ADDRESS = 17; // PLC baseline version
			#const unsigned UN_CONFIG_S7_CPC6_PLC_APPLICATION_ADDRESS = 18; // PLC application version
			#const unsigned UN_CONFIG_S7_CPC6_RESPACK_MAJOR_VERSION_ADDRESS = 19; // (S7 address word polling) WORD, Addressing to read the major version of the Resource Package
			#const unsigned UN_CONFIG_S7_CPC6_RESPACK_MINOR_VERSION_ADDRESS = 20; // (S7 address word polling) WORD, Addressing to read the minor version of the Resource Package
			#const unsigned UN_CONFIG_S7_CPC6_RESPACK_SMALL_VERSION_ADDRESS= 21; // (S7 address word polling) WORD, Addressing to read the small version of the Resource Package
			#const unsigned UN_CONFIG_S7_CPC6_RESPACK_VERSION = 22; // (String) : version of the Resource Package employed
			
   			self.thePlugin.writeComment("#Config Line : PLCCONFIG;S7,thePlcType;thePlcName;theApplicationName;theLocalId;theLocalRack;theLocalSlot;theLocalConnResource;thePartnerRack;thePartnerSlot;thePartnerConnResource;theConnectionTimeout;thePlcIPAddress;thePlcDsIpAddress;thePlcCounterAddress;thePlcCommandAddress;thePlcInfoAddress;theBaselineVersionAddress;theApplicationVersionAddress;theResourcePackageVersionMajorAddress;theResourcePackageVersionMinorAddress;theResourcePackageVersionSmallAddress;theResourcePackageVersionString;")
   			self.thePlugin.writePlcDeclaration("PLCCONFIG;S7,$thePlcType$;$thePlcName$;$theApplicationName$;$theLocalId$;$theLocalRack$;$theLocalSlot$;$theLocalConnResource$;$thePartnerRack$;$thePartnerSlot$;$thePartnerConnResource$;$theConnectionTimeout$;$thePlcIPAddress$;$thePlcDsIpAddress$;$thePlcCounterAddress$;$thePlcCommandAddress$;$thePlcInfoAddress$;$theBaselineVersionAddress$;$theApplicationVersionAddress$;$theResourcePackageVersionMajorAddress$;$theResourcePackageVersionMinorAddress$;$theResourcePackageVersionSmallAddress$;$theResourcePackageVersionString$;")
			
			# Recipes
			GenerateBuffers = theXMLConfig.getPLCParameter("RecipeParameters:GenerateBuffers").strip().lower()
			if GenerateBuffers == "true":
				DB_RECIPES_Address = str(self.thePlugin.getResourceAddress("DB_RECIPES_INTERFACE"))
				activationTimeout = ActivationTimeout = theXMLConfig.getPLCParameter("RecipeParameters:ActivationTimeout")
				HeaderBufferSize = theXMLConfig.getPLCParameter("RecipeParameters:HeaderBufferSize").strip()
				BufferSize = theXMLConfig.getPLCParameter("RecipeParameters:BufferSize").strip()
				BufferSizeCalculated = int(BufferSize)
				if (BufferSizeCalculated%120 != 0):
					BufferSizeCalculated = (int(((BufferSizeCalculated-1)/120))+1)*120
					if BufferSizeCalculated > 1000:
						BufferSizeCalculated = 1000
				
				if int(BufferSizeCalculated)>1000:
					self.thePlugin.writeErrorInUABLog("The maximun buffer size allowed is 1000. BufferSize = 1000 has been taken")
					BufferSizeCalculated = 1000
				if int(BufferSizeCalculated)<1:
					self.thePlugin.writeErrorInUABLog("The buffer size must be a positive integer. BufferSize = 120 has been taken")
					BufferSizeCalculated = 120
				
				addr_HeaderBuffer = "DB"+DB_RECIPES_Address+".DBW2"
				addr_StatusBuffer = "DB"+DB_RECIPES_Address+".DBW"+str(int(HeaderBufferSize)*2+2)
				addr_ManRegAddrBuffer = "DB"+DB_RECIPES_Address+".DBW"+str(int(HeaderBufferSize)*4+2)
				addr_ManRegValBuffer = "DB"+DB_RECIPES_Address+".DBW"+str(int(HeaderBufferSize)*4+int(BufferSizeCalculated)*2+2)
				addr_ReqAddrBuffer = "DB"+DB_RECIPES_Address+".DBW"+str(int(HeaderBufferSize)*4+int(BufferSizeCalculated)*4+2)
				addr_ReqValBuffer = "DB"+DB_RECIPES_Address+".DBD"+str(int(HeaderBufferSize)*4+int(BufferSizeCalculated)*6+2)+"F"
				activationFunction = ""
				
				self.thePlugin.writePlcDeclaration('''#
# Object: UnRcpBuffers
#
#Config Line : DeviceTypeName;deviceNumber;Alias;Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;addr_HeaderBuffer;addr_StatusBuffer;BufferSize;addr_ManRegAddrBuffer;addr_ManRegValBuffer;addr_ReqAddrBuffer;addr_ReqValBuffer;activationFunction;activationTimeout;
#
CPC_RcpBuffers;1;S7_PLC_$thePlcName$_CPC_RcpBuffers;$thePlcName$ Recipe Buffers;;;;;;RcpBuffersState;$addr_HeaderBuffer$;$addr_StatusBuffer$;$BufferSize$;$addr_ManRegAddrBuffer$;$addr_ManRegValBuffer$;$addr_ReqAddrBuffer$;$addr_ReqValBuffer$;$activationFunction$;$activationTimeout$;
#''')
			
   			
      
   def end(self):
	self.thePlugin.writeInUABLog("Application Generation rules: end")

   def shutdown(self):
	self.thePlugin.writeInUABLog("Application Generation rules: shutdown")
