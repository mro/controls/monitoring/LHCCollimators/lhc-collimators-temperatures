# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for DigitalInput Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin	#REQUIRED
from research.ch.cern.unicos.core import CoreManager #REQUIRED
from java.lang import System
import WinCCOA_CommonMethods
from research.ch.cern.unicos.plugins.interfaces import AGenerationPlugin
try:
	# Try to import the user privileges file
	import WinCCOA_Privileges
except:
	# If the previous import failed, try to import the privileges template file
	import WinCCOA_Privileges_Template

class DigitalInput_Template(IUnicosTemplate):
   thePlugin = 0
   theDeviceType = "DigitalInput"
   # Default name for the privileges file
   privilegesFileName = "WinCCOA_Privileges"
   
   def deviceFormat(self):
    return ["deviceType", "deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType", 
            "archiveMode", "smsCategory", "alarmMessage", "alarmAck", "normalPosition", "addressStsReg01", "addressEvStsReg01", "addressManReg01", "booleanArchive", 
            "analogArchive", "eventArchive", "maskEvent", "parameters", "master", "parents", "children", "type", "secondAlias"]
   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython $self.theDeviceType$ template")
	reload(WinCCOA_CommonMethods)
	try:
		# Try to reload the user privileges file
		reload(WinCCOA_Privileges)
	except:
		# If the reload failed, reload the privileges template file
		self.privilegesFileName = "WinCCOA_Privileges_Template"
		reload(WinCCOA_Privileges_Template)

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython $self.theDeviceType$ template")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython $self.theDeviceType$ template")
	
   def process(self, *params):
   	self.thePlugin.writeInUABLog("processInstances in Jython $self.theDeviceType$ template")
	theCurrentDeviceType = params[0]
	self.theUnicosProject = theRawInstances = self.thePlugin.getUnicosProject()
	strAllDeviceTypes = theRawInstances.getAllDeviceTypesString()
	theApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
   	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
   	instancesNumber = str(len(instancesVector))
   	DeviceTypeName = theCurrentDeviceType.getDeviceTypeName()
   	deviceNumber = 0
	CRLF = System.getProperty("line.separator")
	
	# Get the config (UnicosApplication.xml)
	config = CoreManager.getITechnicalParameters().getXMLConfigMapper()
	# Query a PLC parameter
	PLCType = config.getPLCDeclarations().get(0).getPLCType().getValue()
   	
   	self.thePlugin.writeDBHeader ("#$self.theDeviceType$: $instancesNumber$")
   	DeviceTypeFormat = "CPC_"+DeviceTypeName+";deviceNumber;Alias[,DeviceLinkList];Description - ElectricalDiagram;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;ArchiveMode;SMSCat;AlarmMessage;AlarmAck;NormalPosition;addr_StsReg01;addr_EvStsReg01;addr_ManReg01;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;Parameters;Master;Parents;children;Type;SecondAlias;"
   	self.thePlugin.writeComment("#$CRLF$# Object: " + DeviceTypeName + "$CRLF$#$CRLF$#Config Line : " + DeviceTypeFormat + "$CRLF$#")
   	
	#Set Privileges
	Privileges = eval(self.privilegesFileName).getPrivileges(DeviceTypeName)
	
   	for instance in instancesVector :
   		if (instance is not None):
   			
   			# Check if user pressed 'cancel' button:
   			if AGenerationPlugin.isGenerationInterrupted():
   				self.thePlugin.interruptGeneration()
   				self.thePlugin.writeErrorInUABLog("User cancelled!")
   				return
   			
   			deviceNumber = int(int(deviceNumber)+1)
   			deviceNumber = str(deviceNumber)
			
			#1. Common Unicos fields
			#2. Specific fields
			#3. SCADA Device Data Archiving
			#4. Data Treatment	
			#5. Addresses computation
			#6. write the instance information in the database file	
				
			#1. Common Unicos fields
			name = Name = Alias = instance.getAttributeData("DeviceIdentification:Name")
			expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
			Description = instance.getAttributeData("DeviceDocumentation:Description")
			Diagnostics = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
			WWWLink = instance.getAttributeData("SCADADeviceGraphics:WWW Link")
			Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
			WidgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
			Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
			AccessControlDomain = instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain").replace(" ","")	
			Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
			DeviceLinkListSpec = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ","")

			#2. Specific fields
			ElectricalDiagram = instance.getAttributeData("DeviceDocumentation:Electrical Diagram")
			SMSCat = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:SMS Category")
			AlarmAck = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Auto Acknowledge")
			AlarmMasked = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Masked")
			AlarmOnState = instance.getAttributeData("SCADADeviceAlarms:Binary State:Alarm On State")
			AlarmMessage = instance.getAttributeData("SCADADeviceAlarms:Message")
			MaskEvent = instance.getAttributeData("SCADADeviceFunctionals:Mask Event")

			#3. SCADA Device Data Archiving
			ArchiveMode = instance.getAttributeData("SCADADeviceDataArchiving:Archive Mode")
			BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
			AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
			EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")	
			
			#4. Data Treatment
			S_MaskEvent = MaskEvent.lower()
			S_AlarmMasked = AlarmMasked.lower()
			S_AlarmAck = AlarmAck.lower()
			
			S_Domain = Domain.replace(" ","")		
			S_Nature = Nature.replace(" ","") 
			S_SMSCat = SMSCat.replace(" ","")
			
			# Build DeviceLinkList and children from related objects
			DeviceLinkList = ""
			
			#Append Device Link list from Spec
			DeviceLinkList = WinCCOA_CommonMethods.appendDeviceLinkListFromSpec(DeviceLinkListSpec,DeviceLinkList,self.thePlugin,strAllDeviceTypes)
			
				
			#Default values if domain or nature empty
			if S_Domain == "":
				Domain = theApplicationName
			
			if AccessControlDomain != "":
				AccessControlDomain = AccessControlDomain + "|"
			
			
			if S_Nature == "":
				Nature = "DI"
			
			#Mask Value
			if S_MaskEvent == "true":
				MaskEvent = "0"
			else:
				MaskEvent = "1"
				
			#AlarmMasked
			if S_AlarmMasked == "":
				AlarmMasked = "false"
			
			#AlarmAck
			AlarmAck = WinCCOA_CommonMethods.getPvssAlarmAckDigital(S_AlarmAck, AlarmOnState)
			
			# Alarm Message
			if S_SMSCat != "" and AlarmMessage == "":
				AlarmMessage = "No message"
				self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. An Alarm message is required: forced it to ($AlarmMessage$).")
				
			#Boolean Normal Position
			if AlarmOnState == "":
				NormalPosition = "2"
			elif AlarmOnState.lower() == "true" and AlarmMasked.lower() == "false" and SMSCat=="":
				NormalPosition = "0"
			elif AlarmOnState.lower() == "false" and AlarmMasked.lower() == "false" and SMSCat=="":
				NormalPosition = "1"
			elif AlarmOnState.lower() == "true" and AlarmMasked.lower() == "true" and SMSCat=="":
				NormalPosition = "3"
			elif AlarmOnState.lower() == "false" and AlarmMasked.lower() == "true" and SMSCat=="":
				NormalPosition = "4"
			elif AlarmOnState.lower() == "true" and AlarmMasked.lower() == "false" and SMSCat!="":
				NormalPosition = "100"
			elif AlarmOnState.lower() == "false" and AlarmMasked.lower() == "false" and SMSCat!="":
				NormalPosition = "101"
			elif AlarmOnState.lower() == "true" and AlarmMasked.lower() == "true" and SMSCat!="":
				NormalPosition = "103"
			elif AlarmOnState.lower() == "false" and AlarmMasked.lower() == "true" and SMSCat!="":
				NormalPosition = "104"
			else:
				NormalPosition = "2"
	
			#Archive Mode
			if (ArchiveMode.lower()=="no"):
				ArchiveMode = str("N")			
			elif (ArchiveMode.lower()=="old/new comparison"):
				ArchiveMode = str("Y")
			else:
				ArchiveMode = str("N")

			#5. Addresses computation
			addr_StsReg01	= self.getAddressSCADA("$Alias$_StsReg01",PLCType)
			addr_EvStsReg01	=self.getAddressSCADA("$Alias$_EvStsReg01",PLCType)
			addr_ManReg01	= self.getAddressSCADA("$Alias$_ManReg01",PLCType)
			
			#6. New relationship information in all objects:
			parents = []
			# OnOff parents
			FOn_OnOffParents = self.theUnicosProject.findMatchingInstances ("OnOff","'#FEDeviceEnvironmentInputs:Feedback On#'='$Name$'")
			for FOn_OnOffParent in FOn_OnOffParents:
				FOn_OnOffParent_Name = WinCCOA_CommonMethods.getExpertName(FOn_OnOffParent)
				parents.append(FOn_OnOffParent_Name)			
			FOff_OnOffParents = self.theUnicosProject.findMatchingInstances ("OnOff","'#FEDeviceEnvironmentInputs:Feedback Off#'='$Name$'")
			for FOff_OnOffParent in FOff_OnOffParents:
				FOff_OnOffParent_Name = WinCCOA_CommonMethods.getExpertName(FOff_OnOffParent)
				parents.append(FOff_OnOffParent_Name)
			LD_OnOffParents = self.theUnicosProject.findMatchingInstances ("OnOff","'#FEDeviceEnvironmentInputs:Local Drive#'='$Name$'")
			for LD_OnOffParent in LD_OnOffParents:
				LD_OnOffParent_Name = WinCCOA_CommonMethods.getExpertName(LD_OnOffParent)
				parents.append(LD_OnOffParent_Name)
			LOn_OnOffParents = self.theUnicosProject.findMatchingInstances ("OnOff","'#FEDeviceEnvironmentInputs:Local On#'='$Name$'")
			for LOn_OnOffParent in LOn_OnOffParents:
				LOn_OnOffParent_Name = WinCCOA_CommonMethods.getExpertName(LOn_OnOffParent)
				parents.append(LOn_OnOffParent_Name)	
			LOff_OnOffParents = self.theUnicosProject.findMatchingInstances ("OnOff","'#FEDeviceEnvironmentInputs:Local Off#'='$Name$'")
			for LOff_OnOffParent in LOff_OnOffParents:
				LOff_OnOffParent_Name = WinCCOA_CommonMethods.getExpertName(LOff_OnOffParent)
				parents.append(LOff_OnOffParent_Name)
			
			# Analog parents
			FOn_AnalogParents = self.theUnicosProject.findMatchingInstances ("Analog","'#FEDeviceEnvironmentInputs:Feedback On#'='$Name$'")
			for FOn_AnalogParent in FOn_AnalogParents:
				FOn_AnalogParent_Name = WinCCOA_CommonMethods.getExpertName(FOn_AnalogParent)
				parents.append(FOn_AnalogParent_Name)	
			FOff_AnalogParents = self.theUnicosProject.findMatchingInstances ("Analog","'#FEDeviceEnvironmentInputs:Feedback Off#'='$Name$'")
			for FOff_AnalogParent in FOff_AnalogParents:
				FOff_AnalogParent_Name = WinCCOA_CommonMethods.getExpertName(FOff_AnalogParent)
				parents.append(FOff_AnalogParent_Name)
			LD_AnalogParents = self.theUnicosProject.findMatchingInstances ("Analog","'#FEDeviceEnvironmentInputs:Local Drive#'='$Name$'")
			for LD_AnalogParent in LD_AnalogParents:
				LD_AnalogParent_Name = WinCCOA_CommonMethods.getExpertName(LD_AnalogParent)
				parents.append(LD_AnalogParent_Name)
			
			# AnaDO parents
			FOn_AnaDOParents = self.theUnicosProject.findMatchingInstances ("AnaDO","'#FEDeviceEnvironmentInputs:Feedback On#'='$Name$'")
			for FOn_AnaDOParent in FOn_AnaDOParents:
				FOn_AnaDOParent_Name = WinCCOA_CommonMethods.getExpertName(FOn_AnaDOParent)
				parents.append(FOn_AnaDOParent_Name)	
			LD_AnaDOParents = self.theUnicosProject.findMatchingInstances ("AnaDO","'#FEDeviceEnvironmentInputs:Local Drive#'='$Name$'")
			for LD_AnaDOParent in LD_AnaDOParents:
				LD_AnaDOParent_Name = WinCCOA_CommonMethods.getExpertName(LD_AnaDOParent)
				parents.append(LD_AnaDOParent_Name)
			LOn_AnaDOParents = self.theUnicosProject.findMatchingInstances ("AnaDO","'#FEDeviceEnvironmentInputs:Local On#'='$Name$'")
			for LOn_AnaDOParent in LOn_AnaDOParents:
				LOn_AnaDOParent_Name = WinCCOA_CommonMethods.getExpertName(LOn_AnaDOParent)
				parents.append(LOn_AnaDOParent_Name)	
			LOff_AnaDOParents = self.theUnicosProject.findMatchingInstances ("AnaDO","'#FEDeviceEnvironmentInputs:Local Off#'='$Name$'")
			for LOff_AnaDOParent in LOff_AnaDOParents:
				LOff_AnaDOParent_Name = WinCCOA_CommonMethods.getExpertName(LOff_AnaDOParent)
				parents.append(LOff_AnaDOParent_Name)
			
			# AnalogDigital parents
			FOn_AnalogDigitalParents = self.theUnicosProject.findMatchingInstances ("AnalogDigital","'#FEDeviceEnvironmentInputs:Feedback On#'='$Name$'")
			for FOn_AnalogDigitalParent in FOn_AnalogDigitalParents:
				FOn_AnalogDigitalParent_Name = WinCCOA_CommonMethods.getExpertName(FOn_AnalogDigitalParent)
				parents.append(FOn_AnalogDigitalParent_Name)	
			FOff_AnalogDigitalParents = self.theUnicosProject.findMatchingInstances ("AnalogDigital","'#FEDeviceEnvironmentInputs:Feedback Off#'='$Name$'")
			for FOff_AnalogDigitalParent in FOff_AnalogDigitalParents:
				FOff_AnalogDigitalParent_Name = WinCCOA_CommonMethods.getExpertName(FOff_AnalogDigitalParent)
				parents.append(FOff_AnalogDigitalParent_Name)
			LD_AnalogDigitalParents = self.theUnicosProject.findMatchingInstances ("AnalogDigital","'#FEDeviceEnvironmentInputs:Local Drive#'='$Name$'")
			for LD_AnalogDigitalParent in LD_AnalogDigitalParents:
				LD_AnalogDigitalParent_Name = WinCCOA_CommonMethods.getExpertName(LD_AnalogDigitalParent)
				parents.append(LD_AnalogDigitalParent_Name)
			
			# Local parents
			FOn_LocalParents = self.theUnicosProject.findMatchingInstances ("Local","'#FEDeviceEnvironmentInputs:Feedback On#'='$Name$'")
			for FOn_LocalParent in FOn_LocalParents:
				FOn_LocalParent_Name = WinCCOA_CommonMethods.getExpertName(FOn_LocalParent)
				parents.append(FOn_LocalParent_Name)
			FOff_LocalParents = self.theUnicosProject.findMatchingInstances ("Local","'#FEDeviceEnvironmentInputs:Feedback Off#'='$Name$'")
			for FOff_LocalParent in FOff_LocalParents:
				FOff_LocalParent_Name = WinCCOA_CommonMethods.getExpertName(FOff_LocalParent)
				parents.append(FOff_LocalParent_Name)
				
			
			# Digital Alarm parents
			I_DAParents = self.theUnicosProject.findMatchingInstances ("DigitalAlarm","'#FEDeviceEnvironmentInputs:Input#' contains '$Name$'")
			for I_DAParent in I_DAParents:
				if I_DAParent.getAttributeData("FEDeviceEnvironmentInputs:Input").strip() == "$Name$" or I_DAParent.getAttributeData("FEDeviceEnvironmentInputs:Input").strip().lower() == "not $Name$".lower():
					I_DAParent_Name = WinCCOA_CommonMethods.getExpertName(I_DAParent)
					parents.append(I_DAParent_Name)
		
			uniqueList = []
			for parent in parents:
				if parent not in uniqueList:
					uniqueList.append(parent)
			parents = uniqueList
			stringParents = ",".join(parents)
			type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
			children = ""
			master = ""
			
			# Expert Name Logic
			S_Name = name.strip()
			S_ExpertName = expertName.strip()
			if S_ExpertName == "":
				secondAlias = Alias
			else:
				secondAlias = Alias
				Alias = S_ExpertName
				
			#7. Parameters field for all objects
			Parameters = ""
			
   			#8. write the instance information in the database file	
  			self.thePlugin.writeInstanceInfo("CPC_$DeviceTypeName$;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$ - $ElectricalDiagram$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;$ArchiveMode$;$SMSCat$;$AlarmMessage$;$AlarmAck$;$NormalPosition$;$addr_StsReg01$;$addr_EvStsReg01$;$addr_ManReg01$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$MaskEvent$;$Parameters$;$master$;$stringParents$;$children$;$type$;$secondAlias$;")
   			 			

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython $self.theDeviceType$ template")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython $self.theDeviceType$ template")

   def getAddressSCADA(self,DPName,PLCType):
	if PLCType.lower() == "quantum":
		address = str(int(self.thePlugin.computeAddress(DPName)) - 1)
	else:
		address =  str(self.thePlugin.computeAddress(DPName))
	
	return address
   
   
   
   
   
   
   
   
   
   
		
