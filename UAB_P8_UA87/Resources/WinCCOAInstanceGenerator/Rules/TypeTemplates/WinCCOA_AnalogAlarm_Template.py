# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogAlarm Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin	#REQUIRED
from research.ch.cern.unicos.core import CoreManager #REQUIRED
from java.lang import System
import WinCCOA_CommonMethods
from research.ch.cern.unicos.plugins.interfaces import AGenerationPlugin
try:
	# Try to import the user privileges file
	import WinCCOA_Privileges
except:
	# If the previous import failed, try to import the privileges template file
	import WinCCOA_Privileges_Template

class AnalogAlarm_Template(IUnicosTemplate):
   thePlugin = 0
   theDeviceType = "AnalogAlarm"
   # Default name for the privileges file
   privilegesFileName = "WinCCOA_Privileges"
   
   def deviceFormat(self):
    return ["deviceType", "deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType", "unit", 
            "format", "archiveModeI", "archiveModeT", "timeFilterT", "smsCategory", "alarmMessage", "alarmAck", "level", "normalPosition", "addressStsReg01", 
            "addressStsReg02", "addressEvStsReg01", "addressEvStsReg02", "addressPosSt", "addressHHSt", "addressHSt", "addressLSt", "addressLLSt", "addressManReg01", 
            "addressHH", "addressH", "addressL", "addressLL", "booleanArchive", "analogArchive", "eventArchive", "maskEvent", "parameters", "master", "parents", 
            "children", "type", "secondAlias"]

   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython $self.theDeviceType$ template")
	reload(WinCCOA_CommonMethods)
	try:
		# Try to reload the user privileges file
		reload(WinCCOA_Privileges)
	except:
		# If the reload failed, reload the privileges template file
		self.privilegesFileName = "WinCCOA_Privileges_Template"
		reload(WinCCOA_Privileges_Template)

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython $self.theDeviceType$ template")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython $self.theDeviceType$ template")
	
   def process(self, *params):
   	self.thePlugin.writeInUABLog("processInstances in Jython $self.theDeviceType$ template")
	theCurrentDeviceType = params[0]
	self.theUnicosProject = theRawInstances = self.thePlugin.getUnicosProject()
	strAllDeviceTypes = theRawInstances.getAllDeviceTypesString()
	theApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
   	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
   	instancesNumber = str(len(instancesVector))
   	DeviceTypeName = theCurrentDeviceType.getDeviceTypeName()
   	deviceNumber = 0
	CRLF = System.getProperty("line.separator")
	
	# Get the config (UnicosApplication.xml)
	config = CoreManager.getITechnicalParameters().getXMLConfigMapper()
	# Query a PLC parameter
	PLCType = config.getPLCDeclarations().get(0).getPLCType().getValue()
	theManufacturer = self.thePlugin.getPlcManufacturer()
	
   	self.thePlugin.writeDBHeader ("#$self.theDeviceType$: $instancesNumber$")
   	DeviceTypeFormat = "CPC_AnalogAlarm;deviceNumber;Alias[,DeviceLinkList];Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;Unit;Format;ArchiveModeI;ArchiveModeT;TimeFilterT;SMSCat;AlarmMessage;AlarmAck;Level;NormalPosition;addr_StsReg01;addr_StsReg02;addr_EvStsReg01;addr_EvStsReg02;addr_PosSt;addr_HHSt;addr_HSt;addr_LSt;addr_LLSt;addr_ManReg01;addr_HH;addr_H;addr_L;addr_LL;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;Parameters;Master;Parents;children;Type;SecondAlias;"
	self.thePlugin.writeComment("#$CRLF$# Object: " + DeviceTypeName + "$CRLF$#$CRLF$#Config Line : " + DeviceTypeFormat + "$CRLF$#")
   	
	#Set Privileges
	Privileges = eval(self.privilegesFileName).getPrivileges(DeviceTypeName)
   	  	
   	for instance in instancesVector :
   		if (instance is not None):
   			
   			# Check if user pressed 'cancel' button:
   			if AGenerationPlugin.isGenerationInterrupted():
   				self.thePlugin.interruptGeneration()
   				self.thePlugin.writeErrorInUABLog("User cancelled!")
   				return
   			
   			deviceNumber = int(int(deviceNumber)+1)
   			deviceNumber = str(deviceNumber)
			
			#1. Common Unicos fields
			#2. Specific fields
			#3. SCADA Device Data Archiving
			#4. Data Treatment	
			#5. Addresses computation
			#6. New relationship information in all objects
			#7. Parameters field
			#8. write the instance information in the database file	

			#1. Common Unicos fields
			name = Name = Alias = instance.getAttributeData("DeviceIdentification:Name")
			expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
			Description = instance.getAttributeData("DeviceDocumentation:Description")
			Diagnostics = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
			WWWLink = instance.getAttributeData("SCADADeviceGraphics:WWW Link")
			Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
			WidgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
			Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
			AccessControlDomain = instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain").replace(" ","")
			Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
			DeviceLinkListSpec = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ","")
			master = MasterDevice	= instance.getAttributeData("LogicDeviceDefinitions:Master")

			#2. Specific fields
			SMSCat = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:SMS Category")
			AlarmAck = instance.getAttributeData("FEDeviceAlarm:Auto Acknowledge")
			AlarmMasked = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Masked")
			AlarmMessage = instance.getAttributeData("SCADADeviceAlarms:Message")
			MaskEvent = instance.getAttributeData("SCADADeviceFunctionals:Mask Event")
			HH	= instance.getAttributeData ("FEDeviceManualRequests:HH Alarm").strip()
			LL	= instance.getAttributeData ("FEDeviceManualRequests:LL Alarm").strip()
			H	= instance.getAttributeData ("FEDeviceManualRequests:H Warning").strip()
			L	= instance.getAttributeData ("FEDeviceManualRequests:L Warning").strip()
			Input = I = instance.getAttributeData ("FEDeviceEnvironmentInputs:Input").strip()
			Delay = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)").strip()


			#3. SCADA Device Data Archiving
			ArchiveModeI = instance.getAttributeData("SCADADeviceDataArchiving:Interlock Archiving:Archive Mode").lower()
			ArchiveModeT = "old/new comparison or time" #instance.getAttributeData("SCADADeviceDataArchiving:Threshold Archiving:Archive Mode").lower()
			TimeFilterT  = "10.0" #self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceDataArchiving:Threshold Archiving:Time Filter (s)"))
			BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
			AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
			EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")	
			

			# default value
			Unit = ""
			Format = "###.###"

			#4. Data Treatment
			S_HH = HH.lower().strip()	
			S_LL = LL.lower().strip()	
			S_L = L.lower().strip()	
			S_H = H.lower().strip()	
			S_I = I.lower().strip()	
			S_Delay = Delay.lower().strip()	
			
			S_ArchiveModeI = ArchiveModeI.lower()	
			S_AlarmMasked = AlarmMasked.lower()		

			S_MaskEvent = MaskEvent.lower()
			S_AlarmMasked = AlarmMasked.lower()
			S_AlarmAck = AlarmAck.lower()
			S_SMSCat = SMSCat.replace(" ","")

			
			# Build DeviceLinkList and children from related objects
			DeviceLinkList = ""
			#MasterDevice
			if (MasterDevice!=""):	
				MasterList = []
				MasterDevice = MasterDevice.strip().replace(",", " ")
				MasterList = MasterDevice.split()
				MasterListExpert = MasterList
				i=0
				for Master in MasterList:	
					MasterDeviceWinCCOAName = self.thePlugin.getLinkedExpertName(Master,"ProcessControlObject, OnOff, Analog, AnalogDigital, AnaDO, MassFlowController")
					MasterListExpert[i] = (MasterDeviceWinCCOAName)
					DeviceLinkList = self.thePlugin.appendLinkedDevice (DeviceLinkList, MasterDeviceWinCCOAName)
					i=i+1
			
			children = []
			#I : Extract Unit, Format			
			I_Unit = ""
			I_Format = ""
			
			if (S_I!=""): 
				inst_I = theRawInstances.findMatchingInstances ("AnalogInput, AnalogInputReal, Analog, AnalogStatus","'#DeviceIdentification:Name#'='$I$'")
				IWinCCOAName = self.thePlugin.getLinkedExpertName(I,"AnalogInput ,AnalogInputReal, AnalogStatus")
				DeviceLinkList = self.thePlugin.appendLinkedDevice (DeviceLinkList, IWinCCOAName)
				children.append(IWinCCOAName)
				for instance_I in inst_I:
					I_Unit 			= instance_I.getAttributeData("SCADADeviceGraphics:Unit")
					I_Format 		= instance_I.getAttributeData("SCADADeviceGraphics:Format")
					ArchiveModeT 	= instance_I.getAttributeData("SCADADeviceDataArchiving:Archive Mode").lower()
					TimeFilterT  	= self.thePlugin.formatNumberPLC(instance_I.getAttributeData("SCADADeviceDataArchiving:Time Filter (s)"))
					DeadbandTypeT 	= instance_I.getAttributeData("SCADADeviceDataArchiving:Deadband Type").lower()
					DeadbandValueT = self.thePlugin.formatNumberPLC(instance_I.getAttributeData("SCADADeviceDataArchiving:Deadband Value"))			

			#HH, H, L, LL
			if self.thePlugin.isString(S_HH) and S_HH <> "logic" :
				if (S_HH!=""): 
					HHWinCCOAName = self.thePlugin.getLinkedExpertName(HH,"AnalogParameter, WordParameter, AnalogStatus")
					DeviceLinkList = self.thePlugin.appendLinkedDevice (DeviceLinkList, HHWinCCOAName)
					children.append(HHWinCCOAName)

			if self.thePlugin.isString(S_H) and S_H <> "logic" :
				if (S_H!=""): 
					HWinCCOAName = self.thePlugin.getLinkedExpertName(H,"AnalogParameter, WordParameter, AnalogStatus")
					DeviceLinkList = self.thePlugin.appendLinkedDevice (DeviceLinkList, HWinCCOAName)
					children.append(HWinCCOAName)

			if self.thePlugin.isString(S_L) and S_L <> "logic" :
				if (S_L!=""): 
					LWinCCOAName = self.thePlugin.getLinkedExpertName(L,"AnalogParameter, WordParameter, AnalogStatus")
					DeviceLinkList = self.thePlugin.appendLinkedDevice (DeviceLinkList, LWinCCOAName)
					children.append(LWinCCOAName)
					
			if self.thePlugin.isString(S_LL) and S_LL <> "logic" :
				if (S_LL!=""): 
					LLWinCCOAName = self.thePlugin.getLinkedExpertName(LL,"AnalogParameter, WordParameter, AnalogStatus")
					DeviceLinkList = self.thePlugin.appendLinkedDevice (DeviceLinkList, LLWinCCOAName)
					children.append(LLWinCCOAName)

			# Delay
			if self.thePlugin.isString(S_Delay) and S_Delay <> "logic" :
				if (S_Delay!=""): 
					DelayWinCCOAName = self.thePlugin.getLinkedExpertName(Delay,"AnalogParameter, AnalogStatus")
					DeviceLinkList = self.thePlugin.appendLinkedDevice (DeviceLinkList, DelayWinCCOAName)
					children.append(DelayWinCCOAName)
			
			#Append Device Link list from Spec
			DeviceLinkList = WinCCOA_CommonMethods.appendDeviceLinkListFromSpec(DeviceLinkListSpec,DeviceLinkList,self.thePlugin,strAllDeviceTypes)
				
			#Avoid duplications on the deviceLinks
			if DeviceLinkList.strip() <> "":
				DeviceLinkListVector = DeviceLinkList.split(",")
				uniqueListDeviceLinkList = []
				for DeviceLink in DeviceLinkListVector:
					if DeviceLink not in uniqueListDeviceLinkList:
						uniqueListDeviceLinkList.append(DeviceLink)
				DeviceLinkList = ",".join(uniqueListDeviceLinkList)

			#Default values if domain or nature empty
			if Domain == "":
				Domain = theApplicationName
			
			if AccessControlDomain != "":
				AccessControlDomain = AccessControlDomain + "|"
			
			if Nature == "":
				Nature = "AA"
			
			#Mask Value
			if S_MaskEvent == "true":
				MaskEvent = "0"
			else:
				MaskEvent = "1"
				
			#AlarmMasked
			if S_AlarmMasked == "":
				S_AlarmMasked = "false"
			
			#AlarmAck
			if S_AlarmAck == "":
				AlarmAck = "true"
			elif S_AlarmAck.strip() == "false":
				AlarmAck = "true"
			elif S_AlarmAck.strip() == "true":
				AlarmAck = "false"
			
			# Alarm Message
			if S_SMSCat != "":
				if AlarmMessage == "":
					AlarmMessage = Description
				if AlarmMessage == "":			# Alarm Message and Description field are empty.	
					AlarmMessage = "No message"
					self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. An Alarm message or a description are required: forced it to ($AlarmMessage$).")
				
				
			#Selection of the Unit and format. 
		
			if (I_Unit !=""):				# unit of instance connected to I is prioritarily
				Unit = I_Unit
				
			if (I_Format !=""):				# Format of instance connected to I is prioritarily
				Format = I_Format
		
			#Boolean Normal Position
			if S_AlarmMasked == "false" and S_SMSCat=="":
				NormalPosition = "0"
			elif S_AlarmMasked  == "true" and S_SMSCat=="":
				NormalPosition = "3"
			elif S_AlarmMasked  == "false" and S_SMSCat!="":
				NormalPosition = "100"
			elif S_AlarmMasked  == "true" and S_SMSCat!="":
				NormalPosition = "103"
			else:
				self.thePlugin.writeErrorInUABLog("$self.theDeviceType$ instance: $Name$. Unexpected normal position")
				
	
			#Interlock Archive Mode
			if (S_ArchiveModeI=="no"):
				S_ArchiveModeI = str("N")			
			elif (S_ArchiveModeI=="old/new comparison"):
				S_ArchiveModeI = str("Y")
			else:
				S_ArchiveModeI = str("N")

			#PosSt Archive Mode	
			if (ArchiveModeT=="no"):
				ArchiveModeT = str("N")			
			elif (ArchiveModeT=="deadband" and DeadbandTypeT=="absolute"):
				ArchiveModeT = str("VA,$DeadbandValueT$,N")	
			elif (ArchiveModeT=="deadband" and DeadbandTypeT=="relative"):
				ArchiveModeT = str("VR,$DeadbandValueT$,N")	
			elif (ArchiveModeT=="time"):
				ArchiveModeT = str("Y")
			elif (ArchiveModeT=="deadband and time" and DeadbandTypeT=="absolute"):
				ArchiveModeT = str("VA,$DeadbandValueT$,A")	
			elif (ArchiveModeT=="deadband and time" and DeadbandTypeT=="relative"):
				ArchiveModeT = str("VR,$DeadbandValueT$,A")	
			elif (ArchiveModeT=="deadband or time" and DeadbandTypeT=="absolute"):
				ArchiveModeT = str("VA,$DeadbandValueT$,O")	
			elif (ArchiveModeT=="deadband or time" and DeadbandTypeT=="relative"):
				ArchiveModeT = str("VR,$DeadbandValueT$,O")	
			elif (ArchiveModeT=="old/new comparison"):
				ArchiveModeT = str("O")
				TimeFilterT = "0"
			elif (ArchiveModeT=="old/new comparison and time"):
				ArchiveModeT = str("A")
			elif (ArchiveModeT=="old/new comparison or time"):
				ArchiveModeT = str("O")
			else:
				ArchiveModeT = str("N")
				

			#5. Addresses computation
			addr_StsReg01	= self.getAddressSCADA("$Alias$_StsReg01",PLCType)
			addr_EvStsReg01 = self.getAddressSCADA("$Alias$_EvStsReg01",PLCType)
			addr_StsReg02	= self.getAddressSCADA("$Alias$_StsReg02",PLCType)
			addr_EvStsReg02 = self.getAddressSCADA("$Alias$_EvStsReg02",PLCType)
			addr_PosSt = self.getAddressSCADA("$Alias$_PosSt",PLCType)
			addr_HHSt = self.getAddressSCADA("$Alias$_HHSt",PLCType)
			addr_HSt = self.getAddressSCADA("$Alias$_HSt",PLCType)
			addr_LSt = self.getAddressSCADA("$Alias$_LSt",PLCType)
			addr_LLSt = self.getAddressSCADA("$Alias$_LLSt",PLCType)
			addr_ManReg01 = self.getAddressSCADA("$Alias$_ManReg01",PLCType)
			addr_HH = self.getAddressSCADA("$Alias$_HH",PLCType)
			addr_H = self.getAddressSCADA("$Alias$_H",PLCType)
			addr_L = self.getAddressSCADA("$Alias$_L",PLCType)
			addr_LL = self.getAddressSCADA("$Alias$_LL",PLCType)
			
						
			#6. New relationship information in all objects:
			type = instance.getAttributeData("FEDeviceAlarm:Type")
			parents = instance.getAttributeData("LogicDeviceDefinitions:Master")
			master = ""
			
			if type == "Multiple":
				type = ""
				typeList = instance.getAttributeData("FEDeviceAlarm:Multiple Types").strip().replace(",", " ").split()
				i=0
				for typeInst in typeList:
					type = type+ "," + typeInst + ":" + MasterListExpert[i]
					i = i+1
				type = type[1:]
				parentsList = []
				parentDevice = parents.strip().replace(",", " ")
				parentList = parentDevice.split()
				parentListExpert = []
				for parent in parentList:	
					parentDeviceWinCCOAName = self.thePlugin.getLinkedExpertName(parent,"ProcessControlObject, OnOff, Analog, AnalogDigital, AnaDO, MassFlowController")
					parentListExpert.append(parentDeviceWinCCOAName)
					
				stringparents = ",".join(parentListExpert)
			
			elif type <> "" and MasterDevice <> "":
				type = type + ":" + MasterListExpert[0]
				stringparents = self.thePlugin.getLinkedExpertName(parents, "ProcessControlObject, OnOff, Analog, AnalogDigital, AnaDO, MassFlowController")
			
			elif type <> "" and MasterDevice == "":
				stringparents = ""
				
			else:
				type = ""
				stringparents = ""			
		
			uniqueList = []
			for child in children:
				if child not in uniqueList:
					uniqueList.append(child)
			children = uniqueList
			stringchildren = ",".join(children)
			
			# Expert Name Logic
			S_Name = name.strip()
			S_ExpertName = expertName.strip()
			if S_ExpertName == "":
				secondAlias = Alias
			else:
				secondAlias = Alias
				Alias = S_ExpertName
				
			# Level logic
			LevelAlarm = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Level")
			if LevelAlarm.strip() == "":
				LevelAlarm = "Alarm"
			if LevelAlarm.lower() == "information":
				Level = "0"
			elif LevelAlarm.lower() == "warning":
				Level = "1"
			elif LevelAlarm.lower() == "alarm":
				Level = "2"
			elif LevelAlarm.lower() == "safety alarm":
				Level = "3"
			
			
			#7. Parameters field
			Parameters = ""
			if Delay == "":
				Delay = "0"
			elif not self.thePlugin.isString(Delay) and (theManufacturer.lower() == "siemens"): #Delay.isnumeric():
				Delay = int(round(float(Delay)))
			elif not self.thePlugin.isString(Delay):
				Delay = Delay #do not round delay for Schneider and Codesys (UCPC-1387)
			elif Delay.lower() == "logic":
				Delay = "logic"
			else:
				Delay = self.thePlugin.getLinkedExpertName(Delay,"AnalogParameter, AnalogStatus")
			ParamPAlDt = "PAlDt="+str(Delay)
			
			Parameters = ParamPAlDt
			
			
  			#8. write the instance information in the database file	
			self.thePlugin.writeInstanceInfo("CPC_AnalogAlarm;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;$Unit$;$Format$;$S_ArchiveModeI$;$ArchiveModeT$;$TimeFilterT$;$SMSCat$;$AlarmMessage$;$AlarmAck$;$Level$;$NormalPosition$;$addr_StsReg01$;$addr_StsReg02$;$addr_EvStsReg01$;$addr_EvStsReg02$;$addr_PosSt$;$addr_HHSt$;$addr_HSt$;$addr_LSt$;$addr_LLSt$;$addr_ManReg01$;$addr_HH$;$addr_H$;$addr_L$;$addr_LL$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$MaskEvent$;$Parameters$;$master$;$stringparents$;$stringchildren$;$type$;$secondAlias$;")

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython $self.theDeviceType$ template")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython $self.theDeviceType$ template")

   def getAddressSCADA(self,DPName,PLCType):
	if PLCType.lower() == "quantum":
		address = str(int(self.thePlugin.computeAddress(DPName)) - 1)
	else:
		address =  str(self.thePlugin.computeAddress(DPName))
	
	return address
   
