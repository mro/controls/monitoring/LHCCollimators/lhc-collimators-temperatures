# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
import Reverse_Generic_Template
reload(Reverse_Generic_Template)
    
class AnalogAlarm_Template(Reverse_Generic_Template.Generic_Template): 
    theDeviceType = "AnalogAlarm"
    
    def processInstance(self, instance, generatedInstance):        
        
        key,value = self.getDescriptionWithoutElectricalDiagram(instance) #overwrite result from generic function
        generatedInstance.put(key,value)
        
        key,value = self.getArchiveMode(instance, path = "SCADADeviceDataArchiving.InterlockArchiving", field = "archiveModeI")
        generatedInstance.put(key,value)
        
        key,value = self.getSMSCategory(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getMaskEvent(instance)
        generatedInstance.put(key,value)
    
        key,value = self.getAlarmMessage(instance)
        generatedInstance.put(key,value)
    
        key,value = self.getAlarmAcknowledge(instance)
        key = "FEDeviceAlarm.AutoAcknowledge" #Need name of the attribute is different than in generic function
        generatedInstance.put(key,value)
        
        key,value = self.getAlarmDelay(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getAlarmMaster(instance)
        generatedInstance.put(key,value)
        
        keyType, valueType, keyMultiple, valueMultiple = self.getAlarmType(instance)
        generatedInstance.put(keyType, valueType)
        generatedInstance.put(keyMultiple, valueMultiple)
        
        key,value = self.getLevel(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getMasked(instance)
        generatedInstance.put(key,value)