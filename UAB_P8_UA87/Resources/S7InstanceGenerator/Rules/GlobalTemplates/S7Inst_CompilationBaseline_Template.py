# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class CompilationBaseline_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0  
   thePluginId = 0
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
   	self.thePluginId = self.thePlugin.getId()
	self.thePlugin.writeInUABLog("initialize in Jython for CompilationBaseline.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for CompilationBaseline.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for CompilationBaseline.")
  
   def process(self, *params):
	self.thePlugin.writeInUABLog("processInstances in Jython for CompilationBaseline.")
	theOverallInstances = params[0]
	theXMLConfig = params[1]
	genGlobalFilesForAllTypes = params[2].booleanValue() # Comes from "Global files scope" dropdown on Wizard. true = All types. false = Selected types.
	
	# General Steps for the Baseline compilation file:
	# 1. Launch the global SCL files from the Baseline
	# 2. Launch the DeviceType SCL files from the Baseline

	# Step 1: Launch the global SCL files from the Baseline
	
	self.thePlugin.writeInstanceInfoglobal('''
(*Launch of the SCL files********************************************************)
//Data types and functions''')
	GenerateBuffers = theXMLConfig.getPLCParameter("RecipeParameters:GenerateBuffers").strip().lower();
	if GenerateBuffers == "true":
		self.thePlugin.writeInstanceInfoglobal('''
Recipes;''')
	self.thePlugin.writeInstanceInfoglobal('''
CPC_BASE_Unicos;
CPC_TSPP_Unicos;

(*FBs**************************)''')

	# Step 2: Launch the DeviceType SCL files from the Baseline

	typesVector = theOverallInstances.getAllDeviceTypes()
	for currentType in typesVector :
		typeName = currentType.getDeviceTypeName()
		TypeToProccess = theXMLConfig.getTechnicalParametersMap(self.thePluginId + ":UNICOSTypesToProcess").get(typeName)
		instNb = currentType.getAllDeviceTypeInstances().size()
		if (TypeToProccess == "true" or genGlobalFilesForAllTypes) and (instNb <> 0):
			RepresentationName = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", typeName)
			self.thePlugin.writeInstanceInfoglobal('''CPC_FB_$RepresentationName$;''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for CompilationBaseline.")


   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for CompilationBaseline.")
		
