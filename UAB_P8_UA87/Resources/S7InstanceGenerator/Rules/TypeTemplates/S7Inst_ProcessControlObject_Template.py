# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class ProcessControlObject_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for PCO.")
	
   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for PCO.")

   def begin(self):
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'
	self.thePlugin.writeInUABLog("begin in Jython for PCO.")

   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for PCO.")
	
	# General Steps for Non-Optimized objects:
	# 1. DB creation and initialization for each instance
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 
	# 3. Create all the needed Structures from the TCT
	# 4. Create all the need DBs to store the required signal from the object
	
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	# This method is called on every Instance of the current type by the Code Generation plug-in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	
	# Step 1: DB creation and initialization for each instance
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//PCO DB Creation file: UNICOS application
''')

	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			Name = instance.getAttributeData("DeviceIdentification:Name")
			Mode_1_Allowance = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 1 Allowance").strip()
			if Mode_1_Allowance == "":
				Mode_1_Allowance = '00000000'
			Mode_1_AllowanceHex = str(hex(int(Mode_1_Allowance, 2)))
			IndexI1 = Mode_1_AllowanceHex.index('x')+1
			Mode_1_AllowanceHex = Mode_1_AllowanceHex[IndexI1:]
			
			Mode_2_Allowance = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 2 Allowance").strip()
			if Mode_2_Allowance == "":
				Mode_2_Allowance = '00000000'
			Mode_2_AllowanceHex = str(hex(int(Mode_2_Allowance, 2)))
			IndexI2 = Mode_2_AllowanceHex.index('x')+1
			Mode_2_AllowanceHex = Mode_2_AllowanceHex[IndexI2:]
			
			Mode_3_Allowance = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 3 Allowance").strip()
			if Mode_3_Allowance == "":
				Mode_3_Allowance = '00000000'
			Mode_3_AllowanceHex = str(hex(int(Mode_3_Allowance, 2)))
			IndexI3 = Mode_3_AllowanceHex.index('x')+1
			Mode_3_AllowanceHex = Mode_3_AllowanceHex[IndexI3:]
			
			Mode_4_Allowance = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 4 Allowance").strip()
			if Mode_4_Allowance == "":
				Mode_4_Allowance = '00000000'
			Mode_4_AllowanceHex = str(hex(int(Mode_4_Allowance, 2)))
			IndexI4 = Mode_4_AllowanceHex.index('x')+1
			Mode_4_AllowanceHex = Mode_4_AllowanceHex[IndexI4:]
			
			Mode_5_Allowance = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 5 Allowance").strip()
			if Mode_5_Allowance == "":
				Mode_5_Allowance = '00000000'
			Mode_5_AllowanceHex = str(hex(int(Mode_5_Allowance, 2)))
			IndexI5 = Mode_5_AllowanceHex.index('x')+1
			Mode_5_AllowanceHex = Mode_5_AllowanceHex[IndexI5:]
			
			Mode_6_Allowance = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 6 Allowance").strip()
			if Mode_6_Allowance == "":
				Mode_6_Allowance = '00000000'
			Mode_6_AllowanceHex = str(hex(int(Mode_6_Allowance, 2)))
			IndexI6 = Mode_6_AllowanceHex.index('x')+1
			Mode_6_AllowanceHex = Mode_6_AllowanceHex[IndexI6:]
			
			Mode_7_Allowance = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 7 Allowance").strip()
			if Mode_7_Allowance == "":
				Mode_7_Allowance = '00000000'
			Mode_7_AllowanceHex = str(hex(int(Mode_7_Allowance, 2)))
			IndexI7 = Mode_7_AllowanceHex.index('x')+1
			Mode_7_AllowanceHex = Mode_7_AllowanceHex[IndexI7:]
			
			Mode_8_Allowance = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 8 Allowance").strip()
			if Mode_8_Allowance == "":
				Mode_8_Allowance = '00000000'
			Mode_8_AllowanceHex = str(hex(int(Mode_8_Allowance, 2)))
			IndexI8 = Mode_8_AllowanceHex.index('x')+1
			Mode_8_AllowanceHex = Mode_8_AllowanceHex[IndexI8:]
			
			PEnRstart = instance.getAttributeData	("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip()
			
			if PEnRstart.lower() == "false":
				bit0='0'
				bit1='0'				
			elif (PEnRstart.lower()=="true only if full stop disappeared"):
				bit0='1'
				bit1='0'
			else:
				bit0='1'
				bit1='1'
			
			
			self.thePlugin.writeInstanceInfo('''
DATA_BLOCK '''+Name+''' CPC_FB_PCO
BEGIN
	// new: ParReg for PCO
	PPCO.PArReg :=  2#00000$bit1$$bit0$00000000;

	POpMoTa[0] := B#16#$Mode_1_AllowanceHex$;
	POpMoTa[1] := B#16#$Mode_2_AllowanceHex$;
	POpMoTa[2] := B#16#$Mode_3_AllowanceHex$;
	POpMoTa[3] := B#16#$Mode_4_AllowanceHex$;
	POpMoTa[4] := B#16#$Mode_5_AllowanceHex$;
	POpMoTa[5] := B#16#$Mode_6_AllowanceHex$;
	POpMoTa[6] := B#16#$Mode_7_AllowanceHex$;
	POpMoTa[7] := B#16#$Mode_8_AllowanceHex$;

END_DATA_BLOCK
''')

	# Step 3: Create all the needed Structures from the TCT
	
	# UDT for DB_PCO_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE PCO_ManRequest
TITLE = PCO_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isCommVerification = attribute.getIsCommunicated()
				if isCommVerification:
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE PCO_bin_Status
TITLE = PCO_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE

''')

	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE PCO_event
TITLE = PCO_event
//
// parameters of AI Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				# isEventAttribute = attribute.getIsEventAttribute()
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE PCO_ana_Status
TITLE = PCO_ana_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributeName = attribute.getAttributeName()
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_PCO_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_PCO_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_PCO_ManRequest
TITLE = DB_PCO_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    PCO_Requests : ARRAY [1..'''+instanceNb+'''] OF PCO_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')
	
	# Step 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 

	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_PCO Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_PCO
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type PCO
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_PCO : INT := '''+str(NbType)+''';
	PCO_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF PCO_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')

	self.thePlugin.writeInstanceInfo('''
	
(*binaries Status of the PCO************************************************)
DATA_BLOCK DB_bin_status_PCO
TITLE = 'DB_bin_status_PCO'
//
// Global binary status DB of PCO
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : PCO_bin_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN
END_DATA_BLOCK
(*old binaries Status of the PCO************************************************)
DATA_BLOCK DB_bin_status_PCO_old
TITLE = 'DB_bin_status_PCO_old'
//
// Old Global binary status DB of PCO
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : PCO_bin_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN
END_DATA_BLOCK
(*Analogical Status of the PCO ************************************************)
DATA_BLOCK DB_ana_status_PCO
TITLE = 'DB_ana_status_PCO'
//
// Global analogic status DB of PCO
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : PCO_ana_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN
END_DATA_BLOCK
(*old Analogical Status of the PCO ************************************************)
DATA_BLOCK DB_ana_status_PCO_old
TITLE = 'DB_ana_status_PCO_old'
//
// Old Global analogic status DB of PCO
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : PCO_ana_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN
END_DATA_BLOCK
''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for PCO.")
	
   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for PCO.")
		
