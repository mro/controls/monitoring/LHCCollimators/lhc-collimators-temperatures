# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class WordStatus_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for WS.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for WS.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for WS.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'

   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for WS.")
	
	# General Steps for Optimized objects:
	# 1. Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances) It's called DB_Type
	# 2. Create all the needed Structures from DeviceTypeDefinition.xml created by TCT
	# 3. Create all the need DBs to store the required signal from the object
	# 4. Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 5. Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#	 We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	# This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
    
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	
	# Step 1: Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances)
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Word Status DB Creation file: UNICOS application
''')
	self.thePlugin.writeInstanceInfo('''
DATA_BLOCK DB_WS CPC_FB_WS
BEGIN
END_DATA_BLOCK
''')

	# Step 2: Create all the needed Structures from the TCT	
	
	# UDT for the ana status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE WS_ana_Status
TITLE = WS_ana_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributeName = attribute.getAttributeName()
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# Step 3: Create all the need DBs to store the required signal from the object
	
	self.thePlugin.writeInstanceInfo('''
(*  analog status of the WSs ************************************************)
DATA_BLOCK DB_ana_status_WS
TITLE = 'DB_ana_status_WS'
//
// Global Analog status DB of WS
//
//  List of variables:''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
	//  ['''+str(RecordNumber)+''']    $Name$      (WS_ana_Status)
''')	

		
	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF WS_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK

   
(*  old analog status of the WSs ************************************************)
DATA_BLOCK DB_ana_Status_WS_old
TITLE = 'DB_ana_Status_WS_old'
//
// old Global Analog  status DB of WS
//
// List of variables:
''')
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (WS_ana_Status)
''')		

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF WS_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
     ''')


	# Step 4: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	 
	self.thePlugin.writeInstanceInfo('''
  
(*WS execution ********************************************)
FUNCTION_BLOCK FB_WS_all
TITLE = 'FB_WS_all'
//
// Call the WS treatment
//
AUTHOR: 'UNICOS'
NAME: 'Call_WS'
FAMILY: 'WSTAT'
VAR
  // Static Variables
  WS_SET: STRUCT

''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
   $Name$       : CPC_DB_WS;
''')		

	self.thePlugin.writeInstanceInfo('''
  END_STRUCT;  
  
  // Different variable view declaration
  WS AT WS_SET: ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_DB_WS;
  
  // Support variables
  I: INT;
END_VAR
  FOR I:=1 TO '''+str(RecordNumber)+''' DO
  
	// call the IO_ACCESS_WS
	IF (WS[I].FEType <> 0) THEN
		IO_ACCESS_WS(PIWDef := WS[I].PIWDef,
			ReadByte := WS[I].ReadByte,
			FEType := WS[I].FEType,
			InterfaceParam1 := WS[I].InterfaceParam1, 
			InterfaceParam2 := WS[I].InterfaceParam1,
			AuPosR := WS[I].AuPosR);
	END_IF;
	
	// Calls the Baseline function
    CPC_FB_WS.DB_WS(
		Perst := WS[I]
	); 
    
    // Update Analog Status
    DB_ana_status_WS.status[I].PosSt:= WS[I].PosSt;  
  END_FOR;
END_FUNCTION_BLOCK
      ''')
	 
	# Step 5: Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.

	self.thePlugin.writeInstanceInfo('''
(* All WS devices instance DB  **************************************)     
DATA_BLOCK DB_WS_all  FB_WS_all
//
// Instance DB for the whole APAR devices initialization
//
BEGIN
''')
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		InterfaceParam1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
		InterfaceParam2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
		FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
		if (FEType ==""):
			FEType = "0"
			#self.thePlugin.writeWarningInUABLog("WS instance: $Name$. Undefined Type, FEEncondType = 0 is taken.")
		self.thePlugin.writeInstanceInfo('''
WS_SET.$Name$.index := '''+str(RecordNumber)+''';
WS_SET.$Name$.FEType:=$FEType$;''')
		#if (FEType =="0"):
		#	self.thePlugin.writeWarningInUABLog("WS instance: $Name$. FEEncondType = 0 No address configured.")
		if (FEType =="1"):
			Init = InterfaceParam1[0]
			if Init == "p":
				Fin = InterfaceParam1[3:]
				PIWDef = "true"
			else:
				Fin = InterfaceParam1[2:]
				PIWDef = "false"
			if (InterfaceParam1.startswith('pib') or InterfaceParam1.startswith('ib')):
				ReadByte = "true"
			else:
				ReadByte = "false"
			self.thePlugin.writeInstanceInfo('''
WS_SET.$Name$.InterfaceParam1:= $Fin$;
WS_SET.$Name$.PIWDef:= $PIWDef$;
WS_SET.$Name$.ReadByte:= $ReadByte$;
''')
		if (FEType == "101"):
			if InterfaceParam1[0:2] == "db":
				InterfaceParam1 = InterfaceParam1[2:]
			if InterfaceParam2.startswith('dbb'):
				ReadByte = "true"
			else:
				ReadByte = "false"
			if InterfaceParam2[0:2] ==  "db":
				InterfaceParam2 = InterfaceParam2[3:]
			self.thePlugin.writeInstanceInfo('''
WS_SET.$Name$.InterfaceParam1:=$InterfaceParam1$;
WS_SET.$Name$.InterfaceParam2:=$InterfaceParam2$;
WS_SET.$Name$.ReadByte:= $ReadByte$;
''')
		if (FEType == "100"):
			if InterfaceParam1.startswith('mb'):
				ReadByte = "true"
			else:
				ReadByte = "false"
			if InterfaceParam1[0] == "m":
				InterfaceParam1 = InterfaceParam1[2:]
			self.thePlugin.writeInstanceInfo('''
WS_SET.$Name$.InterfaceParam1:=$InterfaceParam1$;
WS_SET.$Name$.ReadByte:= $ReadByte$;
''')
	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK
''')

	#Creation of a FC even for the optimized objects in order to call it in the OB1
	#This avoid the too many symbols problem of the OB1
	#The code is ready for all the object in case we need to split in several DBs one day. Note that for the moment,
	#this has sense only for the AIR and the DI where the split is working.
	NumberOfInstances = instancesVector.size()
	LimitSizeOfInstances = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", theCurrentDeviceType.getDeviceTypeName()))
	nameRepresentation = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", theCurrentDeviceType.getDeviceTypeName())
	self.thePlugin.writeInstanceInfo('''
FUNCTION FC_$nameRepresentation$ : VOID
    FB_$nameRepresentation$_all.DB_$nameRepresentation$_all();''')
	if (NumberOfInstances > LimitSizeOfInstances):
		self.thePlugin.writeInstanceInfo('''
    FB_$nameRepresentation$_all2.DB_$nameRepresentation$_all2();    // Too many '''+str(NumberOfInstances)+''' Need a second FB_$nameRepresentation$_All2''')
	self.thePlugin.writeInstanceInfo('''
END_FUNCTION
''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for WS.")
	
   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for WS.")
		
