# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class DigitalAlarm_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for DA.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for DA.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for DA.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'
	
   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for DA.")
		
	# General Steps for Optimized objects:
	# 1. Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances) It's called DB_Type
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from DeviceTypeDefinition.xml created by TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5. Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 6. Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#	 We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	# This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	
	# Step 1: Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances)

	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Digital Alarm DB Creation file: UNICOS application
''')
	self.thePlugin.writeInstanceInfo('''

DATA_BLOCK DB_DA CPC_FB_DA
BEGIN
END_DATA_BLOCK
''')


	# Step 3: Create all the needed Structures from the TCT	
	
	# UDT for DB_DA_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE DA_ManRequest
TITLE = DA_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isCommVerification = attribute.getIsCommunicated()
				if isCommVerification:
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE DA_bin_Status
TITLE = DA_bin_Status
//
// parameters of DA Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE DA_event
TITLE = DA_event
//
// parameters of DA Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')



	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_DA_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_DA_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_DA_ManRequest
TITLE = DB_DA_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    DA_Requests : ARRAY [1..'''+instanceNb+'''] OF DA_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')

	
	# Step 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 
	
	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_DA Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_DA
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type DA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_DA : INT := '''+str(NbType)+''';
	DA_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF DA_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')
	
	self.thePlugin.writeInstanceInfo('''
(*Status of the DAs************************************************)
DATA_BLOCK DB_bin_status_DA
TITLE = 'DB_bin_status_DA'
//
// Global binary status DB of DA
//
// List of variables:

''')

	RecordNumber = 0
	for instance in instancesVector:
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
// ['''+str(RecordNumber)+'''] $Name$      (DA_bin_Status)
''')

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
  
STRUCT
StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF DA_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(*old Status of the DAs************************************************)
DATA_BLOCK DB_bin_status_DA_old
TITLE = 'DB_bin_status_DA_old'
//
// Old Global binary status DB of DA
//
// List of variables:
	''')
	
	RecordNumber = 0
	for instance in instancesVector:
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
// ['''+str(RecordNumber)+'''] $Name$      (DA_bin_Status)
''')


	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
	StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF DA_bin_Status;
END_STRUCT
  
BEGIN

END_DATA_BLOCK
	''')

	# Step 5: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	
	self.thePlugin.writeInstanceInfo('''
// DA EXEC: Inputs of the DA will be set by the FC logic blocks
FUNCTION_BLOCK FB_DA_ALL
TITLE = 'FB_DA_all'
//
// DAs grouped in a single DB
//
AUTHOR: 'UNICOS'
NAME: 'Call DA'
FAMILY: 'DA'
VAR
 DA_SET: STRUCT
	''')
	
	RecordNumber = 0
	for instance in instancesVector:
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
	$Name$      : CPC_DB_DA;
''')


	self.thePlugin.writeInstanceInfo('''
		END_STRUCT;
  // Different variable view declaration
  DA AT DA_SET: ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_DB_DA;
  
  // Support variables
  old_status : DWORD;
  I: INT;
END_VAR
    FOR I:=1 TO '''+str(RecordNumber)+''' DO
       
       old_status := DB_bin_status_DA.StsReg01[I].StsReg01;
       old_status := ROR(IN:=old_status, N:=16);
       
       // Call (Inputs will be set in the FC logic)
        CPC_FB_DA.DB_DA(
					   Manreg01 := DB_DA_ManRequest.DA_Requests[I].Manreg01
					   ,StsReg01 := DB_bin_status_DA.StsReg01[I].Stsreg01
					   ,Perst := DA[I]
						);
	   
		// Events
        DB_Event_DA.DA_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_DA.StsReg01[I].StsReg01;
          
    END_FOR;   
END_FUNCTION_BLOCK
   ''')
   
   
	# Step 6: Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	self.thePlugin.writeInstanceInfo('''
// DA DB: all instance data
DATA_BLOCK DB_DA_ALL FB_DA_ALL
BEGIN
''')

	
	RecordNumber = 0
	for instance in instancesVector:
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Delay = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
		AutoAlarmAck = instance.getAttributeData("FEDeviceAlarm:Auto Acknowledge").strip().lower()
		if AutoAlarmAck == "true":
			PAuAckAl = "TRUE"
		else:
			PAuAckAl = "FALSE"	
		self.thePlugin.writeInstanceInfo('''
	DA_SET.$Name$.PAuAckAl:='''+PAuAckAl+''';
''')
		if Delay.strip() == "":
			self.thePlugin.writeInstanceInfo('''
	DA_SET.$Name$.PAlDt:= 0;
''')	
		elif self.thePlugin.isString(Delay):
			self.thePlugin.writeInstanceInfo('''
	// The Alarm Delay is defined in the logic
''')	
		else:
			Delay = int(round(float(Delay)))
			self.thePlugin.writeInstanceInfo('''
	DA_SET.$Name$.PAlDt:='''+str(Delay)+''';
''')	
					
	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK
''')

	#Creation of a FC even for the optimized objects in order to call it in the OB1
	#This avoid the too many symbols problem of the OB1
	#The code is ready for all the object in case we need to split in several DBs one day. Note that for the moment,
	#this has sense only for the AIR and the DI where the split is working.
	NumberOfInstances = instancesVector.size()
	LimitSizeOfInstances = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", theCurrentDeviceType.getDeviceTypeName()))
	nameRepresentation = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", theCurrentDeviceType.getDeviceTypeName())
	self.thePlugin.writeInstanceInfo('''
FUNCTION FC_$nameRepresentation$ : VOID
    FB_$nameRepresentation$_all.DB_$nameRepresentation$_all();''')
	if (NumberOfInstances > LimitSizeOfInstances):
		self.thePlugin.writeInstanceInfo('''
    FB_$nameRepresentation$_all2.DB_$nameRepresentation$_all2();    // Too many '''+str(NumberOfInstances)+''' Need a second FB_$nameRepresentation$_All2''')
	self.thePlugin.writeInstanceInfo('''
END_FUNCTION
''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for DA.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for DA.")
		
