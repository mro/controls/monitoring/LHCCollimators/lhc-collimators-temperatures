FUNCTION_BLOCK TSPP_Unicos_Manager  // ### FBxxx  The symbol must be declared first

TITLE = 'TSPP_Unicos_Manager'
//
// Commentaire du bloc ...
//
VERSION : '4.3'  //optimization of the redundant implementation
AUTHOR  : 'UNICOS'
NAME    : 'COMM'
FAMILY  : 'COMMS'

CONST 
(* ############################################################################
These parameters MUST be adjusted to fullfill the user requirements *)   
MaxNbOfTSEvent      := 100; // ### Const1:To be set by the user
                            // Nb of event wich will trig the send to WinCCOA
SpareEventNumber    := 50;  // ### Const2:To be set by the user
                            // spare places to be able to continue to ../..
                            // record event when the send function is buzy                            
Word_in_one_TSEvent := 2;   // ### Const3:To be set by the user
                            // Number of data word in one Event
MaxStatusTable      := 100; // ### Const4:To be set by the user
                            // Max number of status tables (the length of one 
                            // table is 200 bytes without the header)
MaxTableInOneSend  := 100;  // ### Const5:To be set by the user 
                            //(MAX value S7-400:250  S7-300:100)
                            // Max nb of status tables in one TSPP message 
                            // Must lower or equal to MaxStatusTable
Redundant_PLC := TRUE;      // ### Const6:To be set by the user
                            // Using a S7-400H PLC in Redundance mode                                                     
ListEventSize := 1000;      // ### Const7:To be set by the user
                            // Size of the Event List                                                                            
(*###########################################################################*)

ExtendedNbOfEvent   := MaxNbOfTSEvent + SpareEventNumber; // Full size of event buffer 
TSEventHeaderSize   := 6;   // TimeStamp + DB number + Address
TSEventWordSize     := Word_in_one_TSEvent + TSEventHeaderSize;
TSEventByteSize     := TSEventWordSize*2;
EventByteDataSize   := Word_in_one_TSEvent *2;
MaxStatusReqNb      := MaxStatusTable;
StatusReqListSize   := MaxStatusReqNb +1;
StatusWordSize      := 100;  // Size of status table
StatusByteSize      := StatusWordSize * 2;
MaxReqNumber        := 3;
ReqListSize         := MaxReqNumber +1;
MaxBufferSize_Event := 6 + (TSEventByteSize*ExtendedNbOfEvent);
MaxBufferSize_Status:= 6 + ((StatusByteSize + 12)*MaxTableInOneSend);
MaxBufferSize := MaxBufferSize_Status+(MaxBufferSize_Event-MaxBufferSize_Status)
                 *(1/(1+MaxBufferSize_Status/(MaxBufferSize_Event+1)));  
EventExtension := (MaxBufferSize_Event-MaxBufferSize_Status)
                 *(1 DIV (1+(MaxBufferSize_Status DIV MaxBufferSize_Event)));                 
                 
// List of error codes
EventBufferFull     := w#16#F001;   // The number of event is exceeded
WrongDBNumber       := w#16#F002;   // The DB number is 0
WrongEventSize      := w#16#F003;   // In Event size <> Event size in buffer
MainQueueFull       := w#16#F004;   // Number of req in the global queue exceeded
NbTableExceeded     := w#16#F005;   // Number of tables exceeded
NoAccessToStatus    := w#16#F007;   // Error during access to one or more status table
EventAccessError    := w#16#F008;   // Error during access of the  event (in application part)
TimeStampError      := w#16#F009;   // READ_CLK error 
SendTimeOutError    := w#16#F00A;   // No reaction from network after sendind TSPP frame
Transmission_Error  := w#16#F00B;   // The BSEND error code is added to this value

END_CONST

VAR_TEMP
  //Variables temporaires
  Er_Code       : INT;
  Result        : INT;
  TempAdr       : DWORD;
  CurrentTime   : TIME;
  InEventByteDataSize   : INT;
  NumberOfEvents    : INT;
  CurrentInEvent: INT;
  EventListElemPtr  : ANY;
  EventListElement AT EventListElemPtr : STRUCT
      IDAndType     : WORD;
      DataAndDBNb   : DWORD;
      Address       : DWORD;
  END_STRUCT;    
  EventPtr      : ANY;
  Event AT EventPtr : STRUCT
      S7_ID     : BYTE;
      DataType  : BYTE;
      NbOfData  : INT;
      DBNumber  : INT;
      Address   : DWORD;
  END_STRUCT;
  EventListAdr  : INT;
  Index         : INT;
  Index2        : INT;
  Index3        : INT; 
  First         : INT;
  Last          : INT; 
  Src           : ANY;
  ATSrc AT Src  : STRUCT
      S7_ID     : BYTE;
      DataType  : BYTE;
      NbOfData  : INT;
      DBNumber  : INT;
      Address   : DWORD;
  END_STRUCT;  
  DWNumber      : INT;
  TempCounter   : INT;
  ActualTable   : ANY;
  CreateActual AT ActualTable : STRUCT
      S7_ID     : BYTE;
      DataType  : BYTE;
      NbOfData  : INT;
      DBNumber  : INT;
      Address   : DWORD;
  END_STRUCT; 
  OldTable      : ANY;
  CreateOLd AT OldTable : STRUCT
      S7_ID     : BYTE;
      DataType  : BYTE;
      NbOfData  : INT;
      DBNumber  : INT;
      Address   : DWORD;
  END_STRUCT; 
  TSPPTableIndex: INT;
  NumberOfTableDB   : INT;
  TablesInDB    : INT;
  LastTableSize : INT;
  ChangeInTable : BOOL;
//  LocPtr    : ANY;
//  Local  AT LocPtr : ARRAY[1..StatusWordSize] OF WORD;
END_VAR

VAR_INPUT
  // Input Variables
Init                    : BOOL;     // Initialisation of the function
SendID0                 : WORD;     // ID0 of the S7 link with WinCCOA (CPU0 if redundant)
SendID1                 : WORD;     // ID1 of the S7 link with WinCCOA (CPU1 if redundant)
SendEventPeriod         : TIME;     // Period for sending event buffer
NewEvent                : BOOL;     // New event indication
EventTSIncluded         : BOOL;     // Event(s) with(out)time stamp indication
MultipleEvent           : BOOL;     // EventData point to a list of events
EventData               : ANY;      // Pointer to event
IN_Event AT EventData   : STRUCT    
      S7_ID     : BYTE;             
      DataType  : BYTE;
      NbOfData  : INT;
      DBNumber  : INT;
      Address   : DWORD;
  END_STRUCT;
EventListDB             : BLOCK_DB; // For  
ListOfStatusTable       : BLOCK_DB; // DB containing the list of status tables
SendAllStatus   : BOOL;
WatchDog        : ANY;
IN_WatchDog AT WatchDog : STRUCT
      S7_ID     : BYTE;             
      DataType  : BYTE;
      NbOfData  : INT;
      DBNumber  : WORD;
      Address   : DWORD;
  END_STRUCT;    
END_VAR

VAR_OUTPUT
   Error                : BOOL;     // Function error indicator
   Error_code           : WORD;     // Function error code  
   Special_code         : WORD;     // Error code of internaly used functions
END_VAR

VAR
(*%%%START Redundancy Code%%%*)
Connection_to_use   : BOOL; //FALSE=Connection 0 , TRUE= Connection 1
Connection_old_state: BOOL; //FALSE=Connection 0 , TRUE= Connection 1 
Connection_changed  : BOOL; //Error - Connection lost-try to connect to the other CPU
Connection_Problem0 : BOOL; //for CPU0
Connection_Problem1 : BOOL; //for CPU1
Connection_Problem  : BOOL; //for all redundant system (CPU0 and CPU1)
BSENDcpu0_ERROR     : BOOL;
BSENDcpu1_ERROR     : BOOL;
BSENDcpu0_Status    : WORD;
BSENDcpu1_Status    : WORD;
//PLC_master          : BOOL; //FALSE=CPU0 , TRUE=CPU1
(*%%%END Redundancy Code%%%*)

ID_NewEvent         : INT;
ID_EventSent        : INT;
CurrentEvent        : INT := 0;
EventBufferSize          : INT := 6;
Bsend_Done          : BOOL;
Bsend_Error         : BOOL;
TimerFlag           : BOOL;
TimeOut             : BOOL;
SendReq             : BOOL;
BufferFullSendReq   : BOOL;
ExtendedBufFull     : BOOL := FALSE;
SendPending         : BOOL := FALSE;
SendProblem         : BOOL := FALSE;
InitDone            : BOOL := FALSE;
Dummy               : BOOL := TRUE;
RemoveFromList      : BOOL := FALSE;
BufferToSend        : BOOL := FALSE;
WaitDelay           : BOOL;
SendAllStatusReq    : BOOL := FALSE;
LinkNotEstablish    : BOOL;
WatchDogInList      : BOOL;
StatusInList        : BOOL;
IPReceived          : BOOL;
SendStatusCommand   : BOOL;
EventInList         : BOOL;
TSPPTableFull       : BOOL := FALSE;
WatchDogMissing     : BOOL;
WatchDogTimeOut     : BOOL;
WinCCOANotAlive     : BOOL;
OldSendAllStatus    : BOOL;
Bsend_Status        : WORD;
NbOfTables          : INT;
NBOfGroupOfTable    : INT := 1;
WorkingGroup        : INT := 1;
SizeOfLastGroup     : INT;
NbOfStatusReq       : INT;
NbOfRequest         : INT := 0;
BsendLen            : WORD;
TSComm_Alive        : STRUCT
    TSPP_ID1            : CHAR := 'T';
    TSPP_ID2            : CHAR := 'S';
    TSPP_ID3            : CHAR := 'P';
    Nb_of_TSWord        : BYTE := 1;
    TS_Data_Length      : INT := 7;
    TimeStamp           : DATE_AND_TIME;
    DBNumber            : WORD;
    Address             : INT;
    CommAliveCounter    : INT;
END_STRUCT;  
LocalActual         : ARRAY[1..StatusWordSize] OF WORD;
LocalOld            : ARRAY[1..StatusWordSize] OF WORD;
ReqList             : ARRAY[1..MaxReqNumber+1] OF INT := ReqListSize(0);
StatusReqList       : ARRAY[1..MaxStatusReqNb+1] OF INT := StatusReqListSize(0);
AlreadyInReqList    : ARRAY[1..MaxStatusTable] OF BOOL := MaxStatusTable(FALSE);
ActualTableList     : ARRAY[1..MaxStatusTable] OF   STRUCT
      S7_ID     : BYTE;
      DataType  : BYTE;
      NbOfData  : INT;
      DBNumber  : INT;
      Address   : DWORD;
END_STRUCT;
OldTableList        : ARRAY[1..MaxStatusTable] OF   STRUCT
      S7_ID     : BYTE;
      DataType  : BYTE;
      NbOfData  : INT;
      DBNumber  : INT;
      Address   : DWORD;
END_STRUCT; 
TS_EventBuffer      : STRUCT
    TSPP_ID1        : CHAR := 'T';
    TSPP_ID2        : CHAR := 'S';
    TSPP_ID3        : CHAR := 'P';
    Nb_of_TSWord    : BYTE := Word_in_one_TSEvent;
    TS_Data_Length  : INT;
    Data            : ARRAY[1..ExtendedNbOfEvent] OF
                   STRUCT
                          TimeStamp     : DATE_AND_TIME;
                          DBNumber      : INT;
                          Address       : INT;
                          Data          : ARRAY[1..Word_in_one_TSEvent] OF WORD;
                   END_STRUCT;
END_STRUCT;
BSendBuffer       : STRUCT  // Also used directly as StatusBuffer
    TSPP_ID1            : CHAR := 'T';
    TSPP_ID2            : CHAR := 'S';
    TSPP_ID3            : CHAR := 'P';
    Nb_of_TSWord        : BYTE;
    TS_Data_Length      : INT;
    TableList           : ARRAY[1..MaxTableInOneSend] OF 
                STRUCT
                    TimeStamp       : DATE_AND_TIME;
                    DBNumber        : INT;
                    Address         : INT;
                    Table           : ARRAY[1..StatusWordSize] OF WORD;
                END_STRUCT;           
    ExtendedData : ARRAY[0..EventExtension] OF BYTE;
END_STRUCT;  
SEND                : BSEND; // Dynamic call to the right BSEND depending of the PLC type. In this case, PLCType = S7-400H
(*%%%START Redundancy Code%%%*)
SEND1               : BSEND; // Dynamic call to the right BSEND depending of the PLC type. In this case, PLCType = S7-400H
SENDcpu0            : BSEND; // Dynamic call to the right BSEND depending of the PLC type. In this case, PLCType = S7-400H
SENDcpu1            : BSEND; // Dynamic call to the right BSEND depending of the PLC type. In this case, PLCType = S7-400H
(*%%%END Redundancy Code%%%*)
SendTimeOutTimer    : TON;
SendEventTimer      : TON;
WatchDogTimer     : TON;
NbOfRetry      : INT := 0;
END_VAR
  
//Program 

// Timer for sending events
SendEventTimer(IN :=  NOT(TimerFlag)
          ,PT :=  SendEventPeriod
          ); 
TimerFlag := SendEventTimer.Q;
CurrentTime := SendEventTimer.ET;
                  
// Timer for communication time out and retry 
SendTimeOutTimer(IN :=  SendPending
          ,PT :=  t#10s
          ); 
TimeOut := SendTimeOutTimer.Q;
CurrentTime := SendTimeOutTimer.ET; 

// Timer for watch dog
WatchDogTimer(IN :=  WatchDogMissing
          ,PT :=  t#10s
          ); 
WatchDogTimeOut := WatchDogTimer.Q;
CurrentTime := WatchDogTimer.ET;
                                            
Error := FALSE;
Error_code := 0;
Special_code := 0;
                       
  // Initialisation                       
IF Init OR NOT InitDone THEN
    
  ID_NewEvent  := 0; //changed TNR
  ID_EventSent := 0; //changed TNR 
  (*%%%START Redundancy Code%%%*)
  Connection_to_use := FALSE; // Redundancy, it starts with Connection 1 and in the next cycle it updates if necessary
  (*%%%END Redundancy Code%%%*)
  Error_code := 0;
  Special_code := 0;
  InitDone := TRUE;
  Init := FALSE;
  CurrentEvent := 0;
  EventBufferSize := 6;                        //Taille du header
  BufferFullSendReq:= FALSE;
  ExtendedBufFull := FALSE;
  NbOfTables := 0;
  NbOfStatusReq := 0;
  SendProblem := FALSE;
  RemoveFromList := FALSE;
  BufferToSend := FALSE;
  WaitDelay := FALSE;
  SendAllStatusReq := FALSE;
  SendStatusCommand := FALSE;
  WatchDogInList := FALSE;
  StatusInList := FALSE;
  EventInList := FALSE;
  TSPPTableFull := FALSE;
  WorkingGroup := 1;
  TS_EventBuffer.TS_Data_Length := 0;
  NbOfRequest := 0;
  FOR Index := 1 TO MaxReqNumber+1 BY 1 DO
    ReqList[Index] := 0;
  END_FOR;
  FOR Index := 1 TO MaxStatusReqNb +1 BY 1 DO
    StatusReqList[Index] := 0;
  END_FOR;
  FOR Index := 1 TO MaxStatusTable BY 1 DO
    AlreadyInReqList[Index] := FALSE;
  END_FOR;
  
  SendPending := FALSE;
  SendReq := FALSE;
  
 // Create table list
  NumberOfTableDB := WORD_TO_INT(ListOfStatusTable.dw0);
  Index3 := 0;
  FOR Index := 1 TO NumberOfTableDB BY 1 DO  // scan of DBs containing tables
    TablesInDB := WORD_TO_INT(ListOfStatusTable.dw[6+((Index-1)*6)]) DIV StatusByteSize;
    LastTableSize := WORD_TO_INT(ListOfStatusTable.dw[6+((Index-1)*6)]) MOD StatusByteSize;
    IF LastTableSize <> 0 THEN TablesInDB := TablesInDB +1; 
    ELSE LastTableSize := StatusByteSize;
    END_IF;
    FOR Index2 := 1 TO TablesInDB BY 1 DO  // scan of tables inside each DB
       Index3 := Index3+1;
       IF Index3 > MaxStatusTable THEN
         Error := TRUE;
         Error_code := NbTableExceeded;
         Special_code := 0;
         InitDone := FALSE;
       ELSE
         ActualTableList[Index3].S7_ID := b#16#10;
         ActualTableList[Index3].DataType := 2;
         OldTableList[Index3].S7_ID := b#16#10;
         OldTableList[Index3].DataType := 2;
         IF Index2 = TablesInDB THEN // Last table in the DB (can be shorter)
           ActualTableList[Index3].NbOfData := LastTableSize; 
           OldTableList[Index3].NbOfData := LastTableSize; 
         ELSE
           ActualTableList[Index3].NbOfData := StatusByteSize;
           OLdTableList[Index3].NbOfData := StatusByteSize;
         END_IF;    
         ActualTableList[Index3].DBNumber := WORD_TO_INT(ListOfStatusTable.dw[2+((Index-1)*6)]);
         OldTableList[Index3].DBNumber := WORD_TO_INT(ListOfStatusTable.dw[4+((Index-1)*6)]);
         ActualTableList[Index3].Address := DINT_TO_DWORD((INT_TO_DINT(Index2)-1) * StatusByteSize * 8)
                                            OR dw#16#84000000;
         OldTableList[Index3].Address := DINT_TO_DWORD((INT_TO_DINT(Index2)-1) * StatusByteSize * 8)
                                            OR dw#16#84000000;      
       END_IF;
    END_FOR;
  END_FOR;
  SendAllStatusReq := TRUE;
  NbOfTables := Index3;  // Total number of tables
  NbOfGroupOfTable := NbOfTables DIV MaxTableInOneSend;
  SizeOfLastGroup := NbOfTables MOD MaxTableInOneSend;  
  IF SizeOfLastGroup = 0 THEN
    SizeOfLastGroup := MaxTableInOneSend;
  ELSE
    NbOfGroupOfTable := NbOfGroupOfTable +1;    
  END_IF;
   
  // End create table list 
  
ELSE 
// End of initialisation

// Send all table
  // Copy Tables to OldTables and Put to SendTable queue in order to ..
  // send all tables...
  // 1 - once at the beginning,   
  // 2 - on request,
  // 3 - after communication lost and re-establismnet
  // For cases 1 and 3: wait enable from WinCCOA by IPReceived, that is to guaranty
  // that WinCCOA is ready to receive the tables
  // For case 2: send on the rising edge of SendAllStatus 
 IF (SendAllStatus AND NOT OldSendAllStatus) 
        OR (SendAllStatusReq AND IPReceived) THEN
    SendStatusCommand := TRUE;
    SendAllStatusReq := FALSE;
 END_IF;
 OldSendAllStatus := SendAllStatus;
 IF SendStatusCommand THEN
  FOR Index := 1 TO NbOfTables BY 1 DO
    CreateActual := ActualTableList[Index];
    CreateOld := OldTableList[Index];
    Result := BLKMOV(SRCBLK :=  ActualTable
       ,DSTBLK := OldTable
       );
    IF Result <> 0 THEN
      Error := TRUE;
      Error_code := NoAccessToStatus; 
      Special_code := INT_TO_WORD(Result);
    ELSE
      IF NOT AlreadyInReqList[Index] THEN
        NbOfStatusReq := NbOfStatusReq + 1;
        StatusReqList[NbOfStatusReq] := Index;
        AlreadyInReqList[Index] := TRUE;
      END_IF;
    END_IF;   
  END_FOR;
  SendStatusCommand := FALSE;
 END_IF;

// Events treatment
  IF NewEvent THEN
    IF MultipleEvent THEN 
      //EventListAdr := 0;
      //NumberOfEvents := WORD_TO_INT(EventListDB.DW[EventListAdr]);
      NumberOfEvents := ID_NewEvent - ID_EventSent;
      IF NumberOfEvents < 0 THEN
        NumberOfEvents := NumberOfEvents + ListEventSize;
      END_IF;
      EventListAdr := 2 + ID_EventSent * 10; 
      EventListElement.IDAndType := EventListDB.DW[EventListAdr];
      EventListElement.DataAndDBNb := EventListDB.DD[EventListAdr+2];
      EventListElement.Address := EventListDB.DD[EventListAdr+6];
      EventPtr := EventListElemPtr;
    ELSE   
       NumberOfEvents := 0; 
       EventPtr := EventData;
    END_IF;
       CurrentInEvent := 1;
    REPEAT
      CASE CHAR_TO_INT(BYTE_TO_CHAR(Event.DataType)) OF
        2 :
          InEventByteDataSize := Event.NbOfData;
        4..5 :
          InEventByteDataSize := Event.NbOfData *2;
        6..8 :
          InEventByteDataSize := Event.NbOfData *4;
      ELSE:
        ;
      END_CASE;
      IF EventTSIncluded THEN
        InEventByteDataSize := InEventByteDataSize - 8; //data size without time stamp   
      END_IF;
      IF InEventByteDataSize <> EventByteDataSize THEN  
          Error := TRUE;
          Error_code := WrongEventSize;
          Special_code := 0;
        ELSIF Event.DBNumber = 0 THEN
          Error := TRUE;
          Error_code := WrongDBNumber;    
          Special_code := 0;
        ELSIF CurrentEvent >= ExtendedNbOfEvent THEN
          Error := TRUE;
          Error_code := EventBufferFull;
          Special_code := 0;
          ExtendedBufFull := TRUE;
        ELSE    // Record new event
          CurrentEvent := CurrentEvent + 1;
          TS_EventBuffer.Data[CurrentEvent].DBNumber := Event.DBNumber;
          TempAdr := SHR(IN:=Event.Address,N:=3);
          IF EventTSIncluded THEN
             TS_EventBuffer.Data[CurrentEvent].Address := DWORD_TO_INT(TempAdr)+8;// Address of data  
             Event.DataType := 2;
             Event.NbOfData := 8;
            Result := BLKMOV(SRCBLK :=  EventData
                   ,DSTBLK := TS_EventBuffer.Data[CurrentEvent].TimeStamp
                   ); // Transfer associated timestamp
             IF Result <> 0 THEN
              Error := TRUE;
              Error_code := EventAccessError; 
              Special_code := INT_TO_WORD(Result);
             END_IF;
             Event.NBOfData := InEventByteDataSize;  //Size of data only
             Event.Address := DINT_TO_DWORD(DWORD_TO_DINT(Event.Address) + 64);//8 bytes more     
          ELSE 
             TS_EventBuffer.Data[CurrentEvent].Address := DWORD_TO_INT(TempAdr);  
             Er_code := READ_CLK(CDT :=  TS_EventBuffer.Data[CurrentEvent].TimeStamp);             
          END_IF;
          Result := BLKMOV(SRCBLK := EventPtr// IN: ANY
                ,DSTBLK := TS_EventBuffer.Data[CurrentEvent].Data // OUT: ANY
                ); // INT
          IF Result = 0 THEN
              TS_EventBuffer.TS_Data_Length := TS_EventBuffer.TS_Data_Length + TSEventWordSize;
              EventBufferSize := EventBufferSize + TSEventByteSize;
          ELSE
              CurrentEvent := CurrentEvent - 1;
              Error := TRUE;
              Error_code := EventAccessError;
              Special_code := INT_TO_WORD(Result);        
          END_IF;
          IF CurrentEvent > MaxNbOfTSEvent THEN 
              BufferFullSendReq:= TRUE; 
          END_IF;
        END_IF;
        IF MultipleEvent THEN 
			ID_EventSent  := ID_EventSent + 1; //changed tnr

			IF ID_EventSent > ListEventSize - 1 THEN
				ID_EventSent := 0;
				EventListAdr := 2 + ID_EventSent * 10; 
			ELSE
				EventListAdr := EventListAdr + 10;
			END_IF;

          EventListElement.IDAndType := EventListDB.DW[EventListAdr];
          EventListElement.DataAndDBNb := EventListDB.DD[EventListAdr+2];
          EventListElement.Address := EventListDB.DD[EventListAdr+6];
          EventPtr := EventListElemPtr;
          CurrentInEvent := CurrentInEvent +1;
        END_IF;
     UNTIL CurrentInEvent > NumberOfEvents OR Error
    END_REPEAT;
    //DB_EventData.ID_NewEvent := DB_EventData.ID_NewEvent + NumberOfEvents;//changed tnr
  END_IF;                   
  IF (TimerFlag AND CurrentEvent <> 0) OR BufferFullSendReq THEN
      IF NbOfRequest < MaxReqNumber AND NOT EventInlist THEN
        NbOfRequest := NbOfRequest +1; // One more event in send queue
        Reqlist[NbOfRequest] := 1; // Code 1 = send Event Buffer
        EventInList := TRUE;      
      END_IF;
  END_IF;                        
// END manage event


// Status table treatment
     // Scan all tables, and in case of change :
         // copy Actual to Old 
         // put the table number in the send waiting list 
  WorkingGroup := WorkingGroup +1; 
  IF WorkingGroup > NbOfGroupOfTable THEN WorkingGroup := 1; END_IF;
  First := 1 + ((WorkingGroup -1)* MaxTableInOneSend);
  IF WorkingGroup = NbOfGroupOfTable THEN
      Last := First + SizeOfLastGroup -1;
  ELSE
      Last := First + MaxTableInOneSend -1;
  END_IF;
  FOR Index := First TO Last BY 1 DO
    IF NOT AlreadyInReqList[Index] THEN
      CreateActual := ActualTableList[Index];
      CreateOld := OldTableList[Index];
      Result := BLKMOV(SRCBLK :=  ActualTable// IN: ANY
         ,DSTBLK := LocalActual              // OUT: ANY
         );                                  // INT
      IF Result <> 0 THEN
        Error := TRUE;
        Error_code := NoAccessToStatus; 
        Special_code := INT_TO_WORD(Result);
        InitDone := FALSE; 
      END_IF;
      Result := BLKMOV(SRCBLK :=  OldTable// IN: ANY
         ,DSTBLK := LocalOld              // OUT: ANY
         );                               // INT     
      IF Result <> 0 THEN
        Error := TRUE;
        Error_code := NoAccessToStatus; 
        Special_code := INT_TO_WORD(Result);
        InitDone := FALSE; 
      END_IF;             
          // Check for change in table    
      ChangeInTable := FALSE;
      FOR Index2 := 1  TO CreateActual.NbOfData/2  BY 1 DO
        IF LocalActual[Index2] <> LocalOld[Index2] THEN
          ChangeInTable := TRUE; 
        END_IF;
      END_FOR;

      IF ChangeInTable THEN      
        NbOfStatusReq := NbOfStatusReq +1;  
        StatusReqList[NbOfStatusReq] := Index; // Table number to send in waiting list
        AlreadyInReqList[Index] := TRUE;   
      END_IF;
    END_IF;  // IF NOT AlreadyInReqList
  END_FOR;
   // Put TS_Table in the global waiting list
  IF StatusReqList[1] <> 0 AND NOT StatusInList AND (NbOfRequest < MaxReqNumber) THEN 
    NbOfRequest := NbOfRequest +1; // One more event in send queue
    Reqlist[NbOfRequest] := 2; // Code 2= send Status Table 
    StatusInList := TRUE;       
  END_IF;
// END Status table treatment
    
// Communication watch-dog treatment
  IF DWORD_TO_DINT(WORD_TO_BLOCK_DB(IN_WatchDog.DBNumber).DD[0]) <> -1 THEN // new IP received
    WORD_TO_BLOCK_DB(IN_WatchDog.DBNumber).DD[0] := DINT_TO_DWORD(-1); 
    IPReceived := TRUE;    
    WatchDogMissing := FALSE;
    IF WinCCOANotAlive THEN        // Si WinCCOA �tait inactif auparavant
      SendAllStatusReq := TRUE; // On renvoie toutes les tables
    END_IF;
    WinCCOANotAlive := FALSE;
    TempAdr := SHR(IN:=IN_WatchDog.Address,N:=3);
    DWNumber := 4; // Counter address in watch dog DB 
    TempCounter := WORD_TO_INT(WORD_TO_BLOCK_DB(IN_WatchDog.DBNumber).DW[DWNumber]);
    TempCounter := TempCounter +1;
    IF TempCounter < 0 THEN TempCounter := 0; END_IF;
    WORD_TO_BLOCK_DB(IN_WatchDog.DBNumber).DW[DWNumber]:= INT_TO_WORD(TempCounter);  
    Er_code := READ_CLK(CDT :=  TSComm_Alive.TimeStamp);    
    TSComm_Alive.DBNumber := IN_WatchDog.DBNumber;
    TSComm_Alive.Address := DWORD_TO_INT(TempAdr);   
    TSComm_Alive.CommAliveCounter := TempCounter;  
    IF NOT WatchDogInList THEN
      IF NbOfRequest < MaxReqNumber  THEN
        NbOfRequest := NbOfRequest +1; // One more event in send queue
        Reqlist[NbOfRequest] := 3; // Code 3= send comm alive Buffer
        WatchDogInList := TRUE;
      ELSE
        Error := TRUE;
        Error_code := MainQueueFull;
        Special_code := 0;          
      END_IF;            
    END_IF;
  ELSE
    IPReceived := FALSE; 
    WatchDogMissing := TRUE;      
  END_IF;
  IF WatchDogTimeOut THEN 
    WinCCOANotAlive := TRUE;
  END_IF;
// END comm alive treatment

(*%%%START Redundancy Code%%%*)
//S7-400H PLC REDUNDANCY
//Select which connection to use
//Criteria: -Active connection
//          -Master PLC (not being used)

//Check connection status
  IF Redundant_PLC AND BufferToSend AND Connection_Problem THEN
      
    SENDcpu0(REQ :=  FALSE     // IN: BOOL
          ,ID :=  SendID0      // IN: WORD
          ,SD_1 :=  BSendBuffer// INOUT: ANY
          ,R_ID := W#16#3      // IN: DWORD
          ,LEN :=  BSendLen    // INOUT: WORD
          ); 
    BSENDcpu0_ERROR := SENDcpu0.ERROR;   // OUT: BOOL
    BSENDcpu0_Status := SENDcpu0.STATUS; // OUT: WORD 

    SENDcpu1(REQ :=  FALSE      // IN: BOOL
          ,ID :=  SendID1       // IN: WORD
          ,R_ID := W#16#4       // IN: DWORD
          ,SD_1 :=  BSendBuffer // INOUT: ANY
          ,LEN :=  BSendLen     // INOUT: WORD
          ); 
    BSENDcpu1_ERROR := SENDcpu1.ERROR;    // OUT: BOOL
    BSENDcpu1_Status := SENDcpu1.STATUS; // OUT: WORD  

    //Define Connection Problems Flags
    IF BSENDcpu0_ERROR AND BSENDcpu0_Status = 1 THEN
        Connection_Problem0 := TRUE;
    ELSE
        Connection_Problem0 := FALSE;
    END_IF;
    IF BSENDcpu1_ERROR AND BSENDcpu1_Status = 1 THEN
        Connection_Problem1 := TRUE;
    ELSE
        Connection_Problem1 := FALSE;
    END_IF;
    
    //Choose the connection to use
    
    //Save old_State
    Connection_old_state := Connection_to_use;
    //Variable: Connection_to_use, FALSE»Connection1 TRUE»Connection2
    IF NOT Connection_Problem0 AND Connection_Problem1 THEN //Connection 1 is active and Connection 2 is not
        Connection_to_use := FALSE;
    ELSIF Connection_Problem0 AND NOT Connection_Problem1 THEN //Connection 2 is active and Connection 1 is not
        Connection_to_use := TRUE;
    END_IF;
    
    //Compare Old and current Redundant Connection state (for error purposes)
    IF Connection_old_state <> Connection_to_use THEN
       Connection_changed:= TRUE;
    END_IF;
    
END_IF;
(*%%%END Redundancy Code%%%*)


// Check if last message sent 
  IF BufferToSend THEN
        (*%%%START Redundancy Code%%%*)
        IF (NOT Connection_to_use AND Redundant_PLC) OR NOT Redundant_PLC THEN
        (*%%%END Redundancy Code%%%*)    
            SEND(REQ :=  FALSE            // IN: BOOL
                  ,ID :=  SendID0         // IN: WORD
                  ,SD_1 :=  BSendBuffer   // INOUT: ANY
                  ,R_ID := W#16#1         // IN: DWORD
                  ,LEN :=  BSendLen       // INOUT: WORD
                  ); 
            Bsend_Done   := SEND.DONE;    // OUT: BOOL
            Bsend_Error  := SEND.ERROR;   // OUT: BOOL
            Bsend_Status := SEND.STATUS;  // OUT: WORD  
            
    (*%%%START Redundancy Code%%%*)
    ELSIF (Connection_to_use AND Redundant_PLC) THEN
    
             SEND1(REQ :=  FALSE           // IN: BOOL
                  ,ID  :=  SendID1         // IN: WORD
                  ,SD_1 :=  BSendBuffer    // INOUT: ANY
                  ,R_ID := W#16#2          // IN: DWORD
                  ,LEN :=  BSendLen        // INOUT: WORD
                  ); 
            Bsend_Done   := SEND1.DONE;    // OUT: BOOL
            Bsend_Error  := SEND1.ERROR;   // OUT: BOOL
            Bsend_Status := SEND1.STATUS;  // OUT: WORD  
    
    END_IF;
    (*%%%END Redundancy Code%%%*)
    
    IF Bsend_Done THEN // Transmission of data finished
      SendPending  := FALSE;
      BufferToSend := FALSE;
      WaitDelay    := FALSE;
      IF LinkNotEstablish THEN 
          LinkNotEstablish := FALSE;
          SendAllStatusReq := TRUE;
      END_IF;
       
       (*%%%START Redundancy Code%%%*)
       //Reset Flag - Redundancy
      IF Connection_Problem AND Redundant_PLC THEN 
          Connection_Problem  := FALSE;
          SendAllStatusReq    := TRUE;
      END_IF;
     (*%%%END Redundancy Code%%%*)
     
    END_IF;
  END_IF;
  
// Check if buffer free and if there is data to send   
  IF NOT BufferToSend THEN 
      CASE ReqList[1] OF
        1 :           // It's an event
          RemoveFromList := TRUE; 
          EventInList    := FALSE;
          Src := TS_EventBuffer;
          ATSrc.NbOfData := EventBufferSize;
          Result := BLKMOV(SRCBLK :=  Src// IN: ANY
            ,DSTBLK := BSendBuffer       // OUT: ANY
            );                           // INT 
          BSendLen := INT_TO_WORD(EventBufferSize);     
          CurrentEvent := 0;
          EventBufferSize := 6;                        //Taille du header
          TS_EventBuffer.TS_Data_Length := 0; 
          BufferFullSendReq := FALSE; 
          ExtendedBufFull := FALSE;
        2 :           // It's a status table
          RemoveFromList := TRUE; 
          StatusInList := FALSE;
          // Put the status tables from the status queue directly to the BSend buffer  
          TSPPTableIndex := 1; 
          BSendBuffer.TS_Data_Length := 0;
          TSPPTableFull := FALSE;  
          BSendBuffer.Nb_of_TSWord := StatusWordSize;        
          WHILE StatusReqList[1] <> 0 AND NOT TSPPTableFull DO  // There is table to send
            CreateActual := ActualTableList[StatusReqList[1]];
            CreateOld := OldTableList[StatusReqList[1]];
            Result :=  BLKMOV(SRCBLK :=  ActualTable// Copy Actual table in BSend buffer
                     ,DSTBLK := BSendBuffer.TableList[TSPPTableIndex].Table // OUT: ANY
                     ); // INT
            Result :=  BLKMOV(SRCBLK :=  ActualTable// Copy actual table in old table
                     ,DSTBLK := OldTable // OUT: ANY
                     ); // INT         
            Er_code := READ_CLK(CDT:= BSendBuffer.TableList[TSPPTableIndex].TimeStamp);
            BSendBuffer.TableList[TSPPTableIndex].DBNumber := CreateActual.DBNumber;
            TempAdr := SHR(IN:=CreateActual.Address,N:=3);      
            BSendBuffer.TableList[TSPPTableIndex].Address := WORD_TO_INT(DWORD_TO_WORD(TempAdr));
            BSendBuffer.TS_Data_Length := BSendBuffer.TS_Data_Length + StatusWordSize + 6;
            // Remove table from table waiting list
            AlreadyInReqList[StatusReqList[1]]:= FALSE; // The current table is nomore in list  
            FOR Index := 1 TO NBOfTables BY 1 DO
                StatusReqList[Index] := StatusReqList[Index+1];
            END_FOR;
            NbOfStatusReq := NbOfStatusReq-1;
            TSPPTableIndex := TSPPTableIndex +1;
            IF TSPPTableIndex > MaxTableInOneSend THEN TSPPTableFull := TRUE; END_IF; 
          END_WHILE;          
          BSendLen := INT_TO_WORD((BSendBuffer.TS_Data_Length * 2) +6);      
        3 :           // It's a watch dog event
          WatchDogInList := FALSE;
          RemoveFromList := TRUE; 
          Result := BLKMOV(SRCBLK :=  TSComm_Alive// IN: ANY
              ,DSTBLK := BSendBuffer              // OUT: ANY
              );                                  // INT
          BSendLen := 20;
      ELSE:
        ;
      END_CASE;

    IF RemoveFromList THEN    
      FOR Index := 1 TO NbOfRequest BY 1 DO
        ReqList[Index] := ReqList[Index+1];
      END_FOR;
      NbOfRequest := NbOfRequest-1;
      RemoveFromList := FALSE;
      BufferToSend := TRUE;
      SendReq := TRUE;
      WaitDelay := FALSE;
      SendPending := FALSE;
    END_IF;
  END_IF;
  
// Check if there is data in buffer fand try to send them  
  IF BufferToSend THEN 
    (*%%%START Redundancy Code%%%*)
    IF (NOT Connection_to_use AND Redundant_PLC) OR (Connection_to_use AND Redundant_PLC AND ReqList[1] = 3 AND NOT Connection_Problem) OR NOT Redundant_PLC THEN
    (*%%%END Redundancy Code%%%*)
              SEND(REQ :=  SendReq     // IN: BOOL
              ,ID :=  SendID0          // IN: WORD
              ,SD_1 :=  BSendBuffer    // INOUT: ANY
              ,R_ID := W#16#1
              ,LEN :=  BSendLen        // INOUT: WORD
              ); 
(*%%%START Redundancy Code%%%*)
		IF  (NOT Connection_to_use AND Redundant_PLC) OR NOT Redundant_PLC THEN   
(*%%%END Redundancy Code%%%*)		
			Bsend_Done := SEND.DONE;       // OUT: BOOL
			Bsend_Error := SEND.ERROR;     // OUT: BOOL
			Bsend_Status := SEND.STATUS;   // OUT: WORD  
(*%%%START Redundancy Code%%%*)
		END_IF; 
	END_IF;
(*%%%END Redundancy Code%%%*)
   
    (*%%%START Redundancy Code%%%*)
    IF (Connection_to_use AND Redundant_PLC) OR (NOT Connection_to_use AND Redundant_PLC AND ReqList[1] = 3 AND NOT Connection_Problem) THEN
        
        SEND1(REQ :=  SendReq          // IN: BOOL
              ,ID :=  SendID1          // IN: WORD
              ,SD_1 :=  BSendBuffer    // INOUT: ANY
              ,R_ID := W#16#2          // IN: DWORD
              ,LEN :=  BSendLen        // INOUT: WORD
              ); 
            
		IF (Connection_to_use AND Redundant_PLC) THEN
			Bsend_Done := SEND1.DONE;      // OUT: BOOL
			Bsend_Error := SEND1.ERROR;    // OUT: BOOL
			Bsend_Status := SEND1.STATUS;  // OUT: WORD 
		END_IF;
    END_IF;
    (*%%%END Redundancy Code%%%*)      
   
    IF SendReq THEN
      SendPending := TRUE;
      SendReq := FALSE;
    END_IF; 

    IF Bsend_Done THEN // Transmission of data finished
      SendPending := FALSE;
      BufferToSend := FALSE;
      WaitDelay := FALSE;
      IF LinkNotEstablish THEN 
          LinkNotEstablish := FALSE;
          SendAllStatusReq := TRUE;
      END_IF;
    ELSIF Bsend_Error THEN
        Error := TRUE;
        Error_code := Transmission_Error;
        Special_code := Bsend_Status;
        WaitDelay := TRUE;    
        IF Bsend_Status = 1 THEN 
            LinkNotEstablish := TRUE;
            (*%%%START Redundancy Code%%%*)
            Connection_Problem := TRUE;
            (*%%%END Redundancy Code%%%*)
        END_IF;
    
    (*%%%START Redundancy Code%%%*)    
    ELSIF Connection_changed THEN //One connection was lost
        Error := TRUE;
        Connection_changed := FALSE;
        SendAllStatusReq := TRUE;    
     (*%%%END Redundancy Code%%%*)   
    ELSIF WaitDelay THEN 
        Error := TRUE;
        Error_code := SendTimeOutError;
        Special_code := Bsend_Status;   
    END_IF;
    
    IF TimeOut THEN  // No answer from peer or retry delay riched      
      IF Bsend_Error THEN
        Error := TRUE;
        Error_code := Transmission_Error;
        Special_code := Bsend_Status;
      ELSE     
        Error := TRUE;
        Error_code := SendTimeOutError;
        Special_code := 0;
        WaitDelay := TRUE;
      END_IF;
      SendPending := FALSE;    
      SendReq := TRUE;  // Retry
      NbOfRetry := NbOfRetry + 1;
    END_IF;
  END_IF;   // End send buffer
  IF ExtendedBufFull THEN // BufferFull information of higher priority ...
     Error := TRUE;       // it must inform the user that he has ...
     Error_Code := EventBufferFull; // to retry to send the event
  END_IF;
END_IF; // Else Init   
                      
END_FUNCTION_BLOCK

DATA_BLOCK TSPP_Unicos_DB TSPP_Unicos_Manager //### DBxxx FBxxx The symbols must be declared first
BEGIN
END_DATA_BLOCK

