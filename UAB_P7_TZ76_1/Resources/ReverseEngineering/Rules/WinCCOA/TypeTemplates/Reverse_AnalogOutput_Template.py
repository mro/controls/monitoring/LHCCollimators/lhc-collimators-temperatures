# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
import Reverse_Generic_Template
reload(Reverse_Generic_Template)
import Reverse_GlobalFunctions_Template
reload(Reverse_GlobalFunctions_Template)
    
class AnalogOutput_Template(Reverse_Generic_Template.Generic_Template): 
    theDeviceType = "AnalogOutput"
    
    def processInstance(self, instance, generatedInstance):
        if int(instance["deviceNumber"]) >= 50000:
            generatedInstance.put("deviceType","AnalogOutputReal")
        else:
            generatedInstance.put("deviceType","AnalogOutput")
        
        key,value = self.getElectricalDiagram(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getElectricalDiagram(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getFEType(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getMaskEvent(instance)
        generatedInstance.put(key,value)

        key,value = self.getUnit(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getFormat(instance)
        generatedInstance.put(key,value)        
        
        key,value = self.getRangeMax(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getRangeMin(instance)
        generatedInstance.put(key,value)  
        
        keyArchive,valueArchive,keyDbType,valueDbType,keyDbValue,valueDbValue = self.getArchiveModeDeadbandTypeDeadbandValueWithTimeFilterWithDeadBand(instance)
        generatedInstance.put(keyArchive,valueArchive)
        generatedInstance.put(keyDbType,valueDbType)
        generatedInstance.put(keyDbValue,valueDbValue)
        
        key,value = self.getTimeFilter(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getDriverDeadbandType(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getDriverDeadbandValue(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getSMSCategory(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getAlarmAcknowledge(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getAlarmMessage(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getMaskedFromAlarmActive(instance)
        generatedInstance.put(key,value)
        
        limits = {}
        limits["hhLimit"] = "HHAlarm"
        limits["hLimit"] = "HWarning"
        limits["lLimit"] = "LWarning"
        limits["llLimit"] = "LLAlarm"
        for limit in limits:
            value = instance[limit]
            generatedInstance.put("SCADADeviceAlarms.AnalogThresholds." + limits[limit], value) 
