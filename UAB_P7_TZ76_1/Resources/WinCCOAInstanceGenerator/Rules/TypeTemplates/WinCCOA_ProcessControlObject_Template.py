# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for PCO Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin	#REQUIRED
from research.ch.cern.unicos.core import CoreManager #REQUIRED
from java.lang import System
import WinCCOA_CommonMethods
from research.ch.cern.unicos.plugins.interfaces import AGenerationPlugin
try:
	# Try to import the user privileges file
	import WinCCOA_Privileges
except:
	# If the previous import failed, try to import the privileges template file
	import WinCCOA_Privileges_Template

class ProcessControlObject_Template(IUnicosTemplate):
   thePlugin = 0
   theDeviceType = "ProcessControlObject"
   # Default name for the privileges file
   privilegesFileName = "WinCCOA_Privileges"
   
   def deviceFormat(self):
    return ["deviceType", "deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType", "reserved", 
            "normalPosition", "addressStsReg01", "addressStsReg02", "addressEvStsReg01", "addressEvStsReg02", "addressAuOpMoSt", "addressOpMoSt", "addressManReg01", 
            "addressMOpMoR", "modeName1", "modeName2", "modeName3", "modeName4", "modeName5", "modeName6", "modeName7", "modeName8", "modeAllowance1", "modeAllowance2", 
            "modeAllowance3", "modeAllowance4", "modeAllowance5", "modeAllowance6", "modeAllowance7", "modeAllowance8", "booleanArchive", "analogArchive", 
            "eventArchive", "maskEvent", "parameters", "master", "parents", "children", "type", "secondAlias"]
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython $self.theDeviceType$ template")
	reload(WinCCOA_CommonMethods)
	try:
		# Try to reload the user privileges file
		reload(WinCCOA_Privileges)
	except:
		# If the reload failed, reload the privileges template file
		self.privilegesFileName = "WinCCOA_Privileges_Template"
		reload(WinCCOA_Privileges_Template)

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython $self.theDeviceType$ template")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython $self.theDeviceType$ template")
	
   def process(self, *params):
   	self.thePlugin.writeInUABLog("processInstances in Jython $self.theDeviceType$ template")
	theCurrentDeviceType = params[0]
	self.theUnicosProject = theRawInstances = self.thePlugin.getUnicosProject()
	strAllDeviceTypes = theRawInstances.getAllDeviceTypesString()
	theApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
   	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
   	instancesNumber = str(len(instancesVector))
   	DeviceTypeName = theCurrentDeviceType.getDeviceTypeName()
   	deviceNumber = 0
	CRLF = System.getProperty("line.separator")
	
	# Get the config (UnicosApplication.xml)
	config = CoreManager.getITechnicalParameters().getXMLConfigMapper()
	# Query a PLC parameter
	PLCType = config.getPLCDeclarations().get(0).getPLCType().getValue()
   	
   	self.thePlugin.writeDBHeader ("#$self.theDeviceType$: $instancesNumber$")   	
   	DeviceTypeFormat = "CPC_"+DeviceTypeName+";deviceNumber;Alias[,DeviceLinkList];Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;reserved_empty;NormalPosition;addr_StsReg01;addr_StsReg02;addr_EvStsReg01;addr_EvStsReg02;addr_AuOpMoSt;addr_OpMoSt;addr_ManReg01;addr_MOpMoR;ModeName1;ModeName2;ModeName3;ModeName4;ModeName5;ModeName6;ModeName7;ModeName8;ModeAllowance1;ModeAllowance2;ModeAllowance3;ModeAllowance4;ModeAllowance5;ModeAllowance6;ModeAllowance7;ModeAllowance8;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;Parameters;Master;Parents;children;Type;SecondAlias;"
   	self.thePlugin.writeComment("#$CRLF$# Object: " + DeviceTypeName + "$CRLF$#$CRLF$#Config Line : " + DeviceTypeFormat + "$CRLF$#")
   	
	#Set Privileges
	Privileges = eval(self.privilegesFileName).getPrivileges(DeviceTypeName)
	
   	for instance in instancesVector :
   		if (instance is not None):
   			
   			# Check if user pressed 'cancel' button:
   			if AGenerationPlugin.isGenerationInterrupted():
   				self.thePlugin.interruptGeneration()
   				self.thePlugin.writeErrorInUABLog("User cancelled!")
   				return
   			
   			deviceNumber = int(int(deviceNumber)+1)
   			deviceNumber = str(deviceNumber)
			
			#1. Common Unicos fields
			#2. Specific fields
			#3. SCADA Device Data Archiving
			#4. Data Treatment	
			#5. Addresses computation
			#6. write the instance information in the database file
			#7. fill the parameters field			

			#1. Common Unicos fields
			name = Name = Alias = instance.getAttributeData("DeviceIdentification:Name")
			expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
			Description = instance.getAttributeData("DeviceDocumentation:Description")
			Diagnostics = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
			WWWLink = instance.getAttributeData("SCADADeviceGraphics:WWW Link")
			Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
			WidgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
			Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
			AccessControlDomain = instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain").replace(" ","")
			Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
			DeviceLinkListSpec = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ","")
			
			#2. Specific fields
			PCOName = instance.getAttributeData("SCADADeviceGraphics:Display Name")
			ModeName1 = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 1 Label")
			ModeName2 = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 2 Label")
			ModeName3 = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 3 Label")
			ModeName4 = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 4 Label")
			ModeName5 = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 5 Label")
			ModeName6 = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 6 Label")
			ModeName7 = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 7 Label")
			ModeName8 = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 8 Label")
			ModeAllowance1 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 1 Allowance")
			ModeAllowance2 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 2 Allowance")
			ModeAllowance3 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 3 Allowance")
			ModeAllowance4 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 4 Allowance")
			ModeAllowance5 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 5 Allowance")
			ModeAllowance6 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 6 Allowance")
			ModeAllowance7 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 7 Allowance")
			ModeAllowance8 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 8 Allowance")
			Master = instance.getAttributeData("LogicDeviceDefinitions:Master")
			MaskEvent = instance.getAttributeData("SCADADeviceFunctionals:Mask Event")
			NormalPosition = "0"

			#3. SCADA Device Data Archiving
			BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
			AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
			EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")

			#4. Data Treatment	
			# Build DeviceLinkList and children from related objects
			DeviceLinkList = ""
			
			#Append Device Link list from Spec
			DeviceLinkList = WinCCOA_CommonMethods.appendDeviceLinkListFromSpec(DeviceLinkListSpec,DeviceLinkList,self.thePlugin,strAllDeviceTypes)

			
			#Default values if domain or nature empty
			if Domain == "":
				Domain = theApplicationName
			
			if AccessControlDomain != "":
				AccessControlDomain = AccessControlDomain + "|"
			
			if Nature == "":
				Nature = "PCO"
			
			#Mask Value
			if (MaskEvent.lower()=="true"):
				MaskEvent = "0"
			else:
				MaskEvent = "1"
				
			#5. Addresses computation
			addr_StsReg01   = self.getAddressSCADA("$Alias$_StsReg01",PLCType)
			addr_StsReg02   = self.getAddressSCADA("$Alias$_StsReg02",PLCType)
			addr_EvStsReg01 = self.getAddressSCADA("$Alias$_EvStsReg01",PLCType)
			addr_EvStsReg02 = self.getAddressSCADA("$Alias$_EvStsReg02",PLCType)
			addr_ManReg01   = self.getAddressSCADA("$Alias$_ManReg01",PLCType)
			addr_AuOpMoSt    = self.getAddressSCADA("$Alias$_AuOpMoSt",PLCType)
			addr_OpMoSt    = self.getAddressSCADA("$Alias$_OpMoSt",PLCType)
			addr_MOpMoR   = self.getAddressSCADA("$Alias$_MOpMoR",PLCType)
											
			#6. New relationship information in all objects:
			parents = ""
			PEnRstart = instance.getAttributeData	("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip()
			type = ""
			
			# OnOff, Analog, AnaDO, AnalogDigital, Controller, MassFlowController, ProcessControlObject, AnalogAlarm, DigitalAlarm children
			children = []
			ObjectChildren = self.theUnicosProject.findMatchingInstances ("OnOff, Analog, AnaDO, AnalogDigital, Controller, MassFlowController, ProcessControlObject, AnalogAlarm, DigitalAlarm","$Name$","")
			for ObjectChild in ObjectChildren:
				ObjectChildName = WinCCOA_CommonMethods.getExpertName(ObjectChild)
				children.append(ObjectChildName)	
			
			
			uniqueList = []
			for child in children:
				if child not in uniqueList:
					uniqueList.append(child)
			children = uniqueList
			stringchildren = ",".join(children)
			
			# Expert Name Logic
			S_Name = name.strip()
			S_ExpertName = expertName.strip()
			if S_ExpertName == "":
				secondAlias = Alias
			else:
				secondAlias = Alias
				Alias = S_ExpertName	

			#7. Parameters field
			Parameters = ""
			if PEnRstart.lower() == "false":
				ParamPEnRstart = "PEnRstart=FALSE"
				ParamPRstartFS = "PRstartFS=FALSE"
			elif (PEnRstart.lower()=="true only if full stop disappeared"):
				ParamPEnRstart = "PEnRstart=TRUE"
				ParamPRstartFS = "PRstartFS=FALSE"
			else:
				ParamPEnRstart = "PEnRstart=TRUE"
				ParamPRstartFS = "PRstartFS=TRUE"
			Parameters = ParamPEnRstart+","+ParamPRstartFS
			if PCOName != "":
				Parameters = Parameters + ",PCO_NAME=$PCOName$"
			
  			#8. write the instance information in the database file
   			self.thePlugin.writeInstanceInfo("CPC_$DeviceTypeName$;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;;$NormalPosition$;$addr_StsReg01$;$addr_StsReg02$;$addr_EvStsReg01$;$addr_EvStsReg02$;$addr_AuOpMoSt$;$addr_OpMoSt$;$addr_ManReg01$;$addr_MOpMoR$;$ModeName1$;$ModeName2$;$ModeName3$;$ModeName4$;$ModeName5$;$ModeName6$;$ModeName7$;$ModeName8$;$ModeAllowance1$;$ModeAllowance2$;$ModeAllowance3$;$ModeAllowance4$;$ModeAllowance5$;$ModeAllowance6$;$ModeAllowance7$;$ModeAllowance8$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$MaskEvent$;$Parameters$;$Master$;$parents$;$stringchildren$;$type$;$secondAlias$;")

   			

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython $self.theDeviceType$ template")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython $self.theDeviceType$ template")

   def getAddressSCADA(self,DPName,PLCType):
	if PLCType.lower() == "quantum":
		address = str(int(self.thePlugin.computeAddress(DPName)) - 1)
	else:
		address =  str(self.thePlugin.computeAddress(DPName))
	
	return address
