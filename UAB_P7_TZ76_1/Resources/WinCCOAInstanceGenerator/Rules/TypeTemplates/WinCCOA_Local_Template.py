# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Local Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin	#REQUIRED
from research.ch.cern.unicos.core import CoreManager #REQUIRED
from java.lang import System
import WinCCOA_CommonMethods
from research.ch.cern.unicos.plugins.interfaces import AGenerationPlugin
try:
	# Try to import the user privileges file
	import WinCCOA_Privileges
except:
	# If the previous import failed, try to import the privileges template file
	import WinCCOA_Privileges_Template

class Local_Template(IUnicosTemplate):
   thePlugin = 0
   theDeviceType = "Local"
   # Default name for the privileges file
   privilegesFileName = "WinCCOA_Privileges"
   
   def deviceFormat(self):
    return ["deviceType","deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType", "archiveMode", 
            "smsCategory", "alarmMessage", "alarmAck", "normalPosition", "addressStsReg01", "addressEvStsReg01", "addressManReg01", "booleanArchive", "analogArchive", 
            "eventArchive", "maskEvent", "parameters", "master", "parents", "children", "type", "secondAlias"]
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython $self.theDeviceType$ template")
	reload(WinCCOA_CommonMethods)
	try:
		# Try to reload the user privileges file
		reload(WinCCOA_Privileges)
	except:
		# If the reload failed, reload the privileges template file
		self.privilegesFileName = "WinCCOA_Privileges_Template"
		reload(WinCCOA_Privileges_Template)

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython $self.theDeviceType$ template")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython $self.theDeviceType$ template")
	
   def process(self, *params):
   	self.thePlugin.writeInUABLog("processInstances in Jython $self.theDeviceType$ template")
	theCurrentDeviceType = params[0]
	self.theUnicosProject = theRawInstances = self.thePlugin.getUnicosProject()
	strAllDeviceTypes = theRawInstances.getAllDeviceTypesString()
	theApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
   	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
   	instancesNumber = str(len(instancesVector))
   	DeviceTypeName = theCurrentDeviceType.getDeviceTypeName()
   	deviceNumber = 0
   	CRLF = System.getProperty("line.separator")
	
	# Get the config (UnicosApplication.xml)
	config = CoreManager.getITechnicalParameters().getXMLConfigMapper()
	# Query a PLC parameter
	PLCType = config.getPLCDeclarations().get(0).getPLCType().getValue()
	
   	self.thePlugin.writeDBHeader ("#$self.theDeviceType$: $instancesNumber$")   	
   	DeviceTypeFormat = "CPC_"+DeviceTypeName+";deviceNumber;Alias[,DeviceLinkList];Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;ArchiveMode;SMSCat;AlarmMessage;AlarmAck;NormalPosition;addr_StsReg01;addr_EvStsReg01;addr_ManReg01;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;Parameters;Master;Parents;children;Type;SecondAlias;"
   	self.thePlugin.writeComment("#$CRLF$# Object: " + DeviceTypeName + "$CRLF$#$CRLF$#Config Line : " + DeviceTypeFormat + "$CRLF$#")
   	
	#Set Privileges
	Privileges = eval(self.privilegesFileName).getPrivileges(DeviceTypeName)
	
   	for instance in instancesVector :
   		if (instance is not None):
   			
   			# Check if user pressed 'cancel' button:
   			if AGenerationPlugin.isGenerationInterrupted():
   				self.thePlugin.interruptGeneration()
   				self.thePlugin.writeErrorInUABLog("User cancelled!")
   				return
   			
   			deviceNumber = int(int(deviceNumber)+1)
   			deviceNumber = str(deviceNumber)
			
			#1. Common Unicos fields
			#2. Specific fields
			#3. SCADA Device Data Archiving
			#4. Data Treatment	
			#5. Addresses computation
			#6. write the instance information in the database file	
			#7. fill the parameters field
		
			#1. Common Unicos fields
			name = Name = Alias = instance.getAttributeData("DeviceIdentification:Name")
			expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
			Description = instance.getAttributeData("DeviceDocumentation:Description")
			Diagnostics = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
			WWWLink = instance.getAttributeData("SCADADeviceGraphics:WWW Link")
			Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
			WidgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
			Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
			AccessControlDomain = instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain").replace(" ","")
			Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
			DeviceLinkListSpec = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ","")
			
			#2. Specific fields
			PHFOn = FeedbackOn 		= instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On")
			PHFOff = FeedbackOff 	= instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off")
			SMSCat = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:SMS Category")
			AlarmAck = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Auto Acknowledge")
			AlarmMasked = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Masked")
			AlarmMessage = instance.getAttributeData("SCADADeviceAlarms:Message")
			MaskEvent = instance.getAttributeData("SCADADeviceFunctionals:Mask Event")

			#3. SCADA Device Data Archiving
			ArchiveMode = instance.getAttributeData("SCADADeviceDataArchiving:Archive Mode")
			BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
			AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
			EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")

			#4. Data Treatment	
			# Build DeviceLinkList and children from related objects
			DeviceLinkList = ""

			children = []
			#feedback On
			if (FeedbackOn!=""): 
				FeedbackOnWinCCOAName = self.thePlugin.getLinkedExpertName(FeedbackOn,"DigitalInput")
				DeviceLinkList = self.thePlugin.appendLinkedDevice (DeviceLinkList, FeedbackOnWinCCOAName)
				children.append(FeedbackOnWinCCOAName)

			#feedback Off
			if (FeedbackOff!=""): 
				FeedbackOffWinCCOAName = self.thePlugin.getLinkedExpertName(FeedbackOff,"DigitalInput")
				DeviceLinkList = self.thePlugin.appendLinkedDevice (DeviceLinkList, FeedbackOffWinCCOAName)
				children.append(FeedbackOffWinCCOAName)
                            
			#Append Device Link list from Spec
			DeviceLinkList = WinCCOA_CommonMethods.appendDeviceLinkListFromSpec(DeviceLinkListSpec,DeviceLinkList,self.thePlugin,strAllDeviceTypes)
				
			#Avoid duplications on the deviceLinks
			if DeviceLinkList.strip() <> "":
				DeviceLinkListVector = DeviceLinkList.split(",")
				uniqueListDeviceLinkList = []
				for DeviceLink in DeviceLinkListVector:
					if DeviceLink not in uniqueListDeviceLinkList:
						uniqueListDeviceLinkList.append(DeviceLink)
				DeviceLinkList = ",".join(uniqueListDeviceLinkList)


			#Default values if domain or nature empty
			if Domain == "":
				Domain = theApplicationName
			
			if AccessControlDomain != "":
				AccessControlDomain = AccessControlDomain + "|"
				
			if Nature == "":
				Nature = DeviceTypeName
			
			#AlarmMasked
			if AlarmMasked == "":
				AlarmMasked = "false"
				
			
			#AlarmAck
			if AlarmAck.strip() == "":
				AlarmAck = "true"
			elif AlarmAck.lower().strip() == "false":
				AlarmAck = "true"
			elif AlarmAck.lower().strip() == "true":
				AlarmAck = "false"
				
			
			#Mask Value
			if (MaskEvent.lower()=="true"):
				MaskEvent = "0"
			else:
				MaskEvent = "1"
				
			if AlarmMasked.lower() == "true":
				NormalPosition = "3"
			else:
				NormalPosition = "0"
				
				
			#Archive Mode
			if (ArchiveMode.lower()=="no"):
				ArchiveMode = str("N")			
			elif (ArchiveMode.lower()=="old/new comparison"):
				ArchiveMode = str("Y")
			else:
				ArchiveMode = str("N")

			#5. Addresses computation
			addr_StsReg01	= self.getAddressSCADA("$Alias$_StsReg01",PLCType)
			addr_EvStsReg01 = self.getAddressSCADA("$Alias$_EvStsReg01",PLCType)
			addr_ManReg01	= self.getAddressSCADA("$Alias$_ManReg01",PLCType)
						
						
			#6. New relationship information in all objects:
			master = ""
			parents = ""
			type = ""
			
			uniqueList = []
			for child in children:
				if child not in uniqueList:
					uniqueList.append(child)
					
			children = uniqueList
			stringchildren = ",".join(children)
			
			# Expert Name Logic
			S_Name = name.strip()
			S_ExpertName = expertName.strip()
			if S_ExpertName == "":
				secondAlias = Alias
			else:
				secondAlias = Alias
				Alias = S_ExpertName
				
						
			#7. Parameters field 
			Parameters = ""
			if PHFOn.strip() == "":
				ParamPHFOn = "PHFOn=FALSE"
			else:
				ParamPHFOn = "PHFOn=TRUE"
			if PHFOff.strip() == "":
				ParamPHFOff = "PHFOff=FALSE"
			else:
				ParamPHFOff = "PHFOff=TRUE"
			
			Parameters = ParamPHFOn+","+ParamPHFOff
			
  			#8. write the instance information in the database file	
   			self.thePlugin.writeInstanceInfo("CPC_$DeviceTypeName$;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;$ArchiveMode$;$SMSCat$;$AlarmMessage$;$AlarmAck$;$NormalPosition$;$addr_StsReg01$;$addr_EvStsReg01$;$addr_ManReg01$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$MaskEvent$;$Parameters$;$master$;$parents$;$stringchildren$;$type$;$secondAlias$;")
   
   def end(self):
	self.thePlugin.writeInUABLog("end in Jython $self.theDeviceType$ template")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython $self.theDeviceType$ template")

   def getAddressSCADA(self,DPName,PLCType):
	if PLCType.lower() == "quantum":
		address = str(int(self.thePlugin.computeAddress(DPName)) - 1)
	else:
		address =  str(self.thePlugin.computeAddress(DPName))
	
	return address
