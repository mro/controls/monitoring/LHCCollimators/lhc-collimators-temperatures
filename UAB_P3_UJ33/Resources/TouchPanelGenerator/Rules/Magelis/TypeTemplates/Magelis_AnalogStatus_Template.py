# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogStatus Objects.

import Magelis_Generic_Template
reload(Magelis_Generic_Template)

class AnalogStatus_Template(Magelis_Generic_Template.Generic_Template):
    theDeviceType = "AnalogStatus"
    
    def processInstance(self, instance, params):
        self.writeVariable(instance, "PosSt", "REAL")
        self.writeVariable(instance, "Unit", "STRING")
        self.writeVariable(instance, "Description", "STRING")
