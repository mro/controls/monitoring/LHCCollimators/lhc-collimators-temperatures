# Jython source file to create DIP Configs and DIP publications
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED


class SelectInstances_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.dipService = self.thePlugin.getServiceInstance("Dip")
        self.thePlugin.writeInUABLog("initialize in Jython for SelectInstances.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for SelectInstances.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for SelectInstances.")

    def process(self):
        self.thePlugin.writeInUABLog("processInstances in Jython for SelectInstances.")

        configName = "DIPConfig_1"
        timeout = 15
        publicationPrefix = "dip/"
        managerNumber = "20"

        # Creates a DIP config. The parameters of the createDipConfig() method are:
        #   - DIP Config name.
        #   - DIP Query timeout
        #   - DIP Publication prefix
        self.dipService.createDipConfig(configName, timeout, publicationPrefix, managerNumber)

        # Selects the instances from the SPECS file using the findMatchingInstances methods.
        # di= self.theUnicosProject.findMatchingInstances("DigitalInput", "'#DeviceDocumentation:Description#' contains 'Security Switch'")
        # di= self.theUnicosProject.findMatchingInstances("DigitalInput", "'#DeviceDocumentation:Description#' not contains 'Security Switch'")
        # di= self.theUnicosProject.findMatchingInstances("DigitalInput", "'#DeviceDocumentation:Description#' startsWidth 'Security Switch'")
        # di= self.theUnicosProject.findMatchingInstances("DigitalInput", "'#DeviceDocumentation:Description#' endsWidth 'Security Switch'")
        di = self.theUnicosProject.findMatchingInstances("DigitalInput", "'#DeviceIdentification:Name#'!=''")
        ai = self.theUnicosProject.findMatchingInstances("AnalogInput", "'#DeviceIdentification:Name#'!=''")

        # Adds the DIP Publications to the DIP Config.
        # The parameters of the addDipPublications method() are:
        #   - DIP Config name.
        #   - Name of the element to publish (if not provided, the default element will be published)
        #   - Vector<IDeviceInstance> containing the instances to publish (returned by the findMatchingInstances() method)
        #   - Publication name.
        #
        self.dipService.addPublications("DIPConfig_1", "PosSt", di, "module/submodule")
        self.dipService.addPublications("DIPConfig_1", ai, "module/submodule")
        for i in range(0, 20):
            # Adds the DIP Free Publication to the DIP Config.
            # The parameters of the addFreePublications method() are:
            #   - Alias name
            #   - Dip Config name
            #   - Name of the element to publish
            #   - Tag name
            #   - Publication name.
            #   - Buffer value
            #   - Transformation type
            #
            self.dipService.addFreePublication("freeAlias_" + str(i), configName, "module/submodule/freePublication",
                                               "elementToPublish", "freeTag" + str(i), "1000", "double")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for SelectInstances.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for SelectInstances.")
