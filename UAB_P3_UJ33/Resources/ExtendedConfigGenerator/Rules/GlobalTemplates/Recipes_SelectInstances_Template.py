# Jython source file to create DIP Configs and DIP publications
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED


class SelectInstances_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.rcpService = self.thePlugin.getServiceInstance("Recipes")
        self.thePlugin.writeInUABLog("initialize in Jython for SelectInstances.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for SelectInstances.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for SelectInstances.")

    def process(self):
        self.thePlugin.writeInUABLog("processInstances in Jython for SelectInstances.")

        # Selects the instances from the SPECS file using the findMatchingInstances methods.
        di = self.theUnicosProject.findMatchingInstances("DigitalInput", "'#DeviceIdentification:Name#'!=''")
        ai = self.theUnicosProject.findMatchingInstances("AnalogInput", "'#DeviceIdentification:Name#'!=''")
        ai2 = self.theUnicosProject.findMatchingInstances("AnalogInput", "'#DeviceIdentification:Name#'!=''")
        wp = self.theUnicosProject.findMatchingInstances("WordParameter", "'#DeviceIdentification:Name#'!=''")
        rcpClass = self.rcpService.createRcpClass("JythonRecipeClass", "UnRcpType", "JythonTestClass",
                                                  "LHBGEM_Gs_GsPCO")
        rcpClass.setIBtnCancel("admin")

        self.rcpService.addDevices("JythonRecipeClass", wp)

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for SelectInstances.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for SelectInstances.")
