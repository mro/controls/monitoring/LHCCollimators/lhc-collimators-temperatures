# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Template to generate the IOCommissioning file
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin
from java.util import Vector
from java.util import ArrayList
import ucpc_library.shared_usage_finder
reload(ucpc_library.shared_usage_finder)
import ucpc_library.shared_generic_functions
reload(ucpc_library.shared_generic_functions)

class IOCommissioning_Template(IUnicosTemplate, ucpc_library.shared_usage_finder.Finder, ucpc_library.shared_generic_functions.SharedGenericFunctions):
    thePlugin = 0
    theUnicosProject = 0
    objectTypeShortNameDict = {
         'DigitalInput' : 'DI',
         'AnalogInput' : 'AI',
         'AnalogInputReal' : 'AIR',
         'DigitalOutput' : 'DO',
         'AnalogOutput' : 'AO',
         'AnalogOutputReal' : 'AOR',
         'DigitalParameter' : 'DPAR',
         'WordParameter' : 'WPAR',
         'AnalogParameter' : 'APAR',
         'WordStatus' : 'WS',
         'AnalogStatus' : 'AS',
         'Local' : 'Local',
         'OnOff' : 'OnOff',
         'Analog' : 'Analog',
         'AnalogDigital' : 'AnaDig',
         'AnaDO' : 'AnaDO',
         'MassFlowController' : 'MFC',
         'Controller' : 'PID',
         'ProcessControlObject' : 'Unit',
         'DigitalAlarm' : 'DA',
         'AnalogAlarm' : 'AA'
     }
     
    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("IO Commissioning: initialize")


    def check(self):
        self.thePlugin.writeInUABLog("IO Commissioning: check")


    def begin(self):
        self.thePlugin.writeInUABLog("IO Commissioning: begin")


    def process(self, *params):
        self.thePlugin.writeInUABLog("IO Commissioning: processInstances")
        self.theUnicosProject = params[0]

        #Writes the header of the excel/xml file
        self.thePlugin.writeCommissioningInfo('''
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>iprietob</Author>
  <LastAuthor>iprietob</LastAuthor>
  <Created>2010-11-25T11:00:08Z</Created>
  <LastSaved>2010-11-25T11:22:31Z</LastSaved>
  <Company>CERN</Company>
  <Version>1.0</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12660</WindowHeight>
  <WindowWidth>19020</WindowWidth>
  <WindowTopX>120</WindowTopX>
  <WindowTopY>105</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
  <Styles>
   <Style  xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="Default" >
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior />
   <NumberFormat ss:Format="@" />
   <Protection/>
  </Style>
<Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLEHIDDEN">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#FFFFFF"/>
  </Style>
  <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLE1">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#FFCC99" ss:Pattern="Solid"/>
  </Style>
  <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLE2">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
  </Style>
  <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLE3">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLE4">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#CCFFFF" ss:Pattern="Solid"/>
  </Style>
    <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="m160910728UABSTYLE">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#CCFFFF" ss:Pattern="Solid"/>
  </Style>
    <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLE5">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
  </Style>
<Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="s30UABSTYLE">
   <Font x:CharSet="204" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="allBorders">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat ss:Format="@"/>
   <Protection/>
  </Style>
  </Styles>
   ''')


        #SUMMARY OF COMMISSIONING
        self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="SUMMARY">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>

    <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">Project</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">EDMS No</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">Status</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">DRAFT</Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">ICE Responsible</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">CV-CL Responsible</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">CV-OP Responsible</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">AF SVN Revision</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">PLC SVN Revision</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0" ss:Index="10">
    <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="4" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Unit</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">I/O</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Sequencing</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Regulation</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Unit Interlocks</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Actuator Interlocks</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
        ''')

        PCODevice               = self.theUnicosProject.getDeviceType("ProcessControlObject")
        if PCODevice is not None:
            PCOInstances = PCODevice.getAllDeviceTypeInstances()
            for instance in PCOInstances:
                self.processPCOSummary(instance)

        #Write Footer of alarms
        self.writeXmlWorksheetFooter()

        #Writes the header of the DI worksheet
        self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="DI">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Address</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Connection</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Conversion</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>
   </Row>

        ''')

        digitalInputDevice              = self.theUnicosProject.getDeviceType("DigitalInput")
        digitalOutputDevice             = self.theUnicosProject.getDeviceType("DigitalOutput")
        analogInputDevice               = self.theUnicosProject.getDeviceType("AnalogInput")
        analogOutputDevice              = self.theUnicosProject.getDeviceType("AnalogOutput")
        digitalParameterDevice              = self.theUnicosProject.getDeviceType("DigitalParameter")
        wordParameterDevice              = self.theUnicosProject.getDeviceType("WordParameter")
        analogParameterDevice              = self.theUnicosProject.getDeviceType("AnalogParameter")
        OnOffDevice             = self.theUnicosProject.getDeviceType("OnOff")
        AnalogDevice            = self.theUnicosProject.getDeviceType("Analog")
        AnadigDevice            = self.theUnicosProject.getDeviceType("AnalogDigital")
        AnaDODevice             = self.theUnicosProject.getDeviceType("AnaDO")
        MFCDevice               = self.theUnicosProject.getDeviceType("MassFlowController")
        PCODevice               = self.theUnicosProject.getDeviceType("ProcessControlObject")
        PIDDevice               = self.theUnicosProject.getDeviceType("Controller")
        DADevice                = self.theUnicosProject.getDeviceType("DigitalAlarm")
        AADevice                = self.theUnicosProject.getDeviceType("AnalogAlarm")

        #PROCESS DI objects

        if digitalInputDevice is not None:
            diInstances = digitalInputDevice.getAllDeviceTypeInstances()
            for instance in diInstances:
                self.processIndividualDigitalIO(instance, "DigitalInput")


        #Write Footer of DI
        self.writeXmlWorksheetFooter()

        #Writes the header of the DO worksheet
        self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="DO">
 <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Address</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Connection</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Conversion</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>
   </Row>
        ''')

        #PROCESS DO objects
        if digitalOutputDevice is not None:
            doInstances = digitalOutputDevice.getAllDeviceTypeInstances()
            for instance in doInstances:
                self.processIndividualDigitalIO(instance, "DigitalOutput")


        #Write Footer of DO
        self.writeXmlWorksheetFooter()

        #Writes the header of the AI worksheet
        self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="AI">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="3" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Address</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Range</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Connection</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Conversion</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>
   </Row>
   ''')

        # Process the individual analog inputs
        if analogInputDevice is not None:
            analogInputInstances = analogInputDevice.getAllDeviceTypeInstances()
            for instance in analogInputInstances:
                self.processIndividualAnalogIO(instance, "AnalogInput")

        #Write Footer of AI
        self.writeXmlWorksheetFooter()


        #Writes the header of the AO worksheet
        self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="AO">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="3" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Address</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Range</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Connection</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Conversion</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>
   </Row>
        ''')

        # Process the individual analog outputs
        if analogOutputDevice is not None:
            analogOutputInstances = analogOutputDevice.getAllDeviceTypeInstances()
            for instance in analogOutputInstances:
                self.processIndividualAnalogIO(instance, "AnalogOutput")

        #Write Footer of AO
        self.writeXmlWorksheetFooter()


        #Writes the header of the Actuator worksheet
        self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="Actuator">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="3" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="4" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Behaviours</Data></Cell>
        <Cell ss:MergeAcross="0" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Type</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Feedbacks</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Fail Safe</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Limit On (full animation)</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Limit Off(Empty animation)</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Increase Speed (unit/s)</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Decrease Speed (unit/s)</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Test</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>
   </Row>
        ''')

        # Process the individual actuators
        if OnOffDevice is not None:
            for instance in OnOffDevice.getAllDeviceTypeInstances():
                self.processIndividualActuator(instance)

        if AnalogDevice is not None:
            for instance in AnalogDevice.getAllDeviceTypeInstances():
                self.processIndividualActuator(instance)

        if AnadigDevice is not None:
            for instance in AnadigDevice.getAllDeviceTypeInstances():
                self.processIndividualActuator(instance)

        if AnaDODevice is not None:
            for instance in AnaDODevice.getAllDeviceTypeInstances():
                self.processIndividualActuator(instance)

        if MFCDevice is not None:
            for instance in MFCDevice.getAllDeviceTypeInstances():
                self.processIndividualActuator(instance)

        #Write Footer of Actuators
        self.writeXmlWorksheetFooter()

        #Writes the header of the Parameter worksheet
        self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="Parameters">
    <Table x:FullColumns="1" x:FullRows="1">
     <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
     <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="7" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
     </Row>
     <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Param Name</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Default Value</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Unit</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Range</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Recipe Type</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Master</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Activation</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Commissioning Value</Data></Cell>

    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
     <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

     </Row>
        ''')

        # load the sources to RAM 
        self.thePlugin.writeInUABLog("load sources to RAM")
        if not self.loadSources():
            pass
        
        # Process the individual parameters
        parameterInstances = self.java_vector_to_list(digitalParameterDevice.getAllDeviceTypeInstances()) + \
                             self.java_vector_to_list(wordParameterDevice.getAllDeviceTypeInstances()) + \
                             self.java_vector_to_list(analogParameterDevice.getAllDeviceTypeInstances())

        # retrieve inforamtion where parameteres are used
        parametersUsedIn = self.processParametersUsedIn(parameterInstances)

        if digitalParameterDevice is not None:
            for instance in digitalParameterDevice.getAllDeviceTypeInstances():
                self.processIndividualParameter(instance, "DPAR", parametersUsedIn)

        if wordParameterDevice is not None:
            for instance in wordParameterDevice.getAllDeviceTypeInstances():
                self.processIndividualParameter(instance, "WPAR", parametersUsedIn)

        if analogParameterDevice is not None:
            for instance in analogParameterDevice.getAllDeviceTypeInstances():
                self.processIndividualParameter(instance, "APAR", parametersUsedIn)

        #Write Footer of Parameters
        self.writeXmlWorksheetFooter()

        #Generate one CCC alarm sheet per project
        self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="CCC">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="4" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Master</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Nb of copies</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Alarm copies</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Alarm activation</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">LASER activation</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
        ''')

        if DADevice is not None:
            DAInstances = DADevice.getAllDeviceTypeInstances()
            for instance in DAInstances:
                self.processCCCAlarm(instance, DAInstances)

        #Write Footer of alarms
        self.writeXmlWorksheetFooter()

        #Generate one Regulation sheet per project
        self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="Regulations">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="7" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="3" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Master</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Measured Value</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Controlled Devices</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Reverse Action</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">State/Step</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Activation</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Mode</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">SetPoint</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">controlled Devices/PID</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
        ''')

        #Write PID
        if PIDDevice is not None:
            PIDInstances = PIDDevice.getAllDeviceTypeInstances()
            for instance in PIDInstances:
                self.processLogicPID(instance)

        #Write Footer of regulation sheet
        self.writeXmlWorksheetFooter()

#########################################################################################

        #Pre-processing logic files to find inputs/etc

        #Process Alarm Inputs
        alarmInstances = self.java_vector_to_list(DADevice.getAllDeviceTypeInstances()) + \
                         self.java_vector_to_list(AADevice.getAllDeviceTypeInstances())

        # retrieve inputs of alarms
        alarmInputs = self.processObjectInputs(alarmInstances,"I")
        #self.thePlugin.writeDebugInUABLog(str(alarmInputs))

        #Process Actuator AuOnR/AuOffR
        actuatorInstances =  self.java_vector_to_list(OnOffDevice.getAllDeviceTypeInstances()) + \
                             self.java_vector_to_list(AnalogDevice.getAllDeviceTypeInstances()) + \
                             self.java_vector_to_list(AnadigDevice.getAllDeviceTypeInstances()) + \
                             self.java_vector_to_list(AnaDODevice.getAllDeviceTypeInstances()) + \
                             self.java_vector_to_list(MFCDevice.getAllDeviceTypeInstances()) + \
                             self.java_vector_to_list(PCODevice.getAllDeviceTypeInstances())
        
        # retrieve AuOnR and AuOffR inputs of actuators
        actuatorAuOnRlogic  = self.processObjectInputs(actuatorInstances,"AuOnR")
        #self.thePlugin.writeDebugInUABLog("actuatorAuOnRlogic = " + str(actuatorAuOnRlogic))
        actuatorAuOffRlogic = self.processObjectInputs(actuatorInstances,"AuOffR")
        #self.thePlugin.writeDebugInUABLog("actuatorAuOffRlogic = " + str(actuatorAuOffRlogic))

        # Process the individual onoff commands
        cmdeInstances = self.java_vector_to_list(OnOffDevice.getAllDeviceTypeInstances())
        cmdeUsedIn = self.processCmdeUsedIn(cmdeInstances)

###############################LOGIC SHEETS##############################################

        #Generate 3 sheets per PCO:
        # 1. Process Alarms: $PCO$_AL
        # 2. FSM: $PCO$_FSM
        # 3. Actuator: $PCO$_Actu
        # 4. Parameters: $PCO$_Param
        # 5. Commands: $PCO$_Cmde
        PCOInstances = PCODevice.getAllDeviceTypeInstances()

        for instance in PCOInstances:
            PCOName                         = instance.getAttributeData("DeviceIdentification:Name")
            PCODescription          = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")

            if PCODescription.lower() <> "spare":

                #1. Generate one Process alarms sheet
                self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="$PCOName$_AL">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
        <Cell ss:MergeAcross="5" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Condition</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Type</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Master</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Alarm activation</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Alarm Action</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
        ''')

                #List all masters in this PCO
                masterList = []
                masterList.append(PCOName)
                for child in self.theUnicosProject.findMatchingInstances("OnOff, Analog, AnalogDigital, AnaDO, MassFlowController", PCOName, ""):
                    masterList.append(child.getAttributeData("DeviceIdentification:Name"))

                #Write DA/AA depending on this PCO and its dependent field objects, in order
                for childFieldObjectName in masterList:
                    listAlarms = self.theUnicosProject.findMatchingInstances("DigitalAlarm, AnalogAlarm", childFieldObjectName, "")
                    if listAlarms:
                        # write separator row before each set of alarms per PCO/field object
                        self.writeSeparatorRow(11,childFieldObjectName)
                        for alarm in listAlarms:
                            self.processLogicAlarm(alarm, alarmInputs)

                #Write Footer of alarms
                self.writeXmlWorksheetFooter()


            #2. Generate one FSM sheet
                self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="$PCOName$_FSM">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
        <Cell ss:MergeAcross="3" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Start</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">End</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Transition Type</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Transition</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Sequence</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>''')

                self.writeXmlRow(["","","","","","","","",""])

                self.writeXmlWorksheetFooter()

            #3. Generate one actuator sheet
                self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="$PCOName$_Actu">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
        <Cell ss:MergeAcross="6" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Type</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">AuOnR logic</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">AuOffR logic</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">State/Step</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Activation</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Value</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
        ''')

                #Write Actuators depending of this PCO
                for child in self.theUnicosProject.findMatchingInstances("ProcessControlObject, OnOff, Analog, AnalogDigital, AnaDO, MassFlowController", PCOName, ""):
                    self.processLogicActuator(child,actuatorAuOnRlogic,actuatorAuOffRlogic)

                #Write Footer of logic actuator
                self.writeXmlWorksheetFooter()

            #3. Generate one Param sheet per PCO
                self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="$PCOName$_Param">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
        <Cell ss:MergeAcross="7" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Param Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Description</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Default Value</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Unit</Data></Cell>
<Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Range</Data></Cell>
<Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Recipe Type</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Master</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Activation</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Commissioning Value</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
        ''')

                #Write Parameters depending on this PCO
                if digitalParameterDevice is not None:
                    for instance in digitalParameterDevice.getAllDeviceTypeInstances():
                        self.processLogicParameter(instance, PCOName, "DPAR", parametersUsedIn)

                if wordParameterDevice is not None:
                    for instance in wordParameterDevice.getAllDeviceTypeInstances():
                        self.processLogicParameter(instance, PCOName, "WPAR", parametersUsedIn)

                if analogParameterDevice is not None:
                    for instance in analogParameterDevice.getAllDeviceTypeInstances():
                        self.processLogicParameter(instance, PCOName, "APAR", parametersUsedIn)

                self.writeXmlWorksheetFooter()

            #4. Generate one Cmde sheet per PCO
                self.thePlugin.writeCommissioningInfo('''
<Worksheet ss:Name="$PCOName$_cmde">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
        <Cell ss:MergeAcross="3" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Description</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Master Type</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Availability</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Action</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
   ''')

                #Write OnOff Commands depending on this PCO
                if OnOffDevice is not None:
                    for instance in OnOffDevice.getAllDeviceTypeInstances():
                        self.processLogicCmde(instance, PCOName, cmdeUsedIn)

                self.writeXmlWorksheetFooter()


        #Writes the footer of the workbook
        self.thePlugin.writeCommissioningInfo('''
        </Workbook>''')


    def processIndividualDigitalIO(self, instance, deviceType):

        instanceName    = instance.getAttributeData("DeviceIdentification:Name")
        expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
        description     = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        diagram         = instance.getAttributeData("DeviceDocumentation:Electrical Diagram").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")

        if expertName <> "":
            instanceName = expertName

        # Writes the information in the buffer
        self.writeXmlRow([instanceName,description,diagram,"","","","",""])

    def processIndividualAnalogIO(self, instance, deviceType):

        instanceName    = instance.getAttributeData("DeviceIdentification:Name")
        expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
        description     = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        diagram         = instance.getAttributeData("DeviceDocumentation:Electrical Diagram").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        rangeMin        = instance.getAttributeData("FEDeviceParameters:Range Min")
        rangeMax        = instance.getAttributeData("FEDeviceParameters:Range Max")
        rangeUnit       = instance.getAttributeData("SCADADeviceGraphics:Unit")
        rangeStr = "[ " + rangeMin + " - " + rangeMax + " ] " + rangeUnit

        if expertName <> "":
            instanceName = expertName

        # Writes the information in the buffer
        self.writeXmlRow([instanceName,description,diagram,rangeStr,"","","","",""])

    def processIndividualActuator(self, instance):

        instanceName    = instance.getAttributeData("DeviceIdentification:Name")
        description     = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        deviceType = self.objectTypeShortNameDict[instance.getDeviceTypeName()]
        if deviceType == "MFC":
            FOn                     = ""
        else:
            FOn                     = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On")
        FailSafe                                = instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe")
        Nature                  = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature").lower()

        if deviceType in ["AnaDO","MFC"]:
            FOff                    = ""
        else:
            FOff                    = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off")

        if deviceType == "OnOff":
            FAnalog = ""
            LimitOn = ""
            LimitOff = ""
            InSpd= ""
            DeSpd= ""
        elif deviceType == "MFC":
            FAnalog = ""
            LimitOn = ""
            LimitOff = ""
            InSpd                           = instance.getAttributeData("FEDeviceParameters:Manual Increase Speed (Unit/s)")
            DeSpd                           = instance.getAttributeData("FEDeviceParameters:Manual Decrease Speed (Unit/s)")
        else:
            FAnalog                         = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Analog")
            LimitOn                         = instance.getAttributeData("FEDeviceManualRequests:Parameter Limit On/Open")
            LimitOff                                = instance.getAttributeData("FEDeviceManualRequests:Parameter Limit Off/Closed")
            InSpd                           = instance.getAttributeData("FEDeviceParameters:Manual Increase Speed (Unit/s)")
            DeSpd                           = instance.getAttributeData("FEDeviceParameters:Manual Decrease Speed (Unit/s)")

        Feedbacks = ""
        if FOn != "":
            Feedbacks = FOn + ", "

        if FOff != "":
            Feedbacks =Feedbacks + FOff + ", "

        if FAnalog != "":
            Feedbacks = Feedbacks + FAnalog


        # Writes the information in the buffer
        if description.lower() <> "spare" and (Nature <> "bouton" or Nature <> "button"):
            self.writeXmlRow([instanceName,deviceType,description,Feedbacks,FailSafe,LimitOn,LimitOff,InSpd,DeSpd,"","","",""])

    def processParametersUsedIn(self, parameterInstances):
        parametersUsedIn = {}

        listOfNotSpares = []
        for instance in parameterInstances:
            instanceName    = instance.getAttributeData("DeviceIdentification:Name")
            description     = instance.getAttributeData("DeviceDocumentation:Description").strip()
            if description.lower() <> "spare" :
                listOfNotSpares.append(instanceName)    # add parameters to check
            else:
                parametersUsedIn[instanceName] = [] 	# spares are ignored, add them at once
        
        parametersUsedIn.update(self.multiFindUsedIn(listOfNotSpares)) # look for notSPARE parameters in logic
        return parametersUsedIn

    def processIndividualParameter(self, instance, deviceType, parametersUsedIn):

        instanceName    = instance.getAttributeData("DeviceIdentification:Name")
        description     = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        if description.strip().lower() <> "spare" :
            defaultValue    = instance.getAttributeData("FEDeviceParameters:Default Value")
            pcoNames = ",".join(parametersUsedIn[instanceName])
            if deviceType != "DPAR":
                unit            = instance.getAttributeData("SCADADeviceGraphics:Unit")
                rangeMin        = instance.getAttributeData("FEDeviceParameters:Range Min")
                rangeMax        = instance.getAttributeData("FEDeviceParameters:Range Max")
                rangeStr = "[" + rangeMin + " : " + rangeMax + "]"
            else:
                unit = ""
                rangeStr = ""

            # Writes the information in the buffer
            self.writeXmlRow([instanceName,description,defaultValue,unit,rangeStr,"",pcoNames,"","","","","",""])

    def processCmdeUsedIn(self, cmdeInstances):
        cmdeUsedIn = {}
        
        listOfNotSpares = []
        for instance in cmdeInstances:
            instanceName  = instance.getAttributeData("DeviceIdentification:Name")
            description     = instance.getAttributeData("DeviceDocumentation:Description").strip()
            master     = instance.getAttributeData("LogicDeviceDefinitions:Master").strip()
            if description.lower() <> "spare" and master == "":
                listOfNotSpares.append(instanceName)    # collect all the instances to analyse
            else:
                cmdeUsedIn[instanceName] = []           # skip spares, assign empty list
            
        cmdeUsedIn.update(self.multiFindUsedIn(listOfNotSpares))
        # make sure does not include instanceName
        for instance in cmdeInstances:
            instanceName  = instance.getAttributeData("DeviceIdentification:Name")
            if instanceName in cmdeUsedIn[instanceName]: cmdeUsedIn[instanceName].remove(instanceName)

        return cmdeUsedIn

    def processLogicAlarm(self, instance, alarmInputs):
        instanceName    = instance.getAttributeData("DeviceIdentification:Name")
        description     = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        Type                    = instance.getAttributeData("FEDeviceAlarm:Type")
        MultipleType    = instance.getAttributeData("FEDeviceAlarm:Multiple Types")
        AuAck                   = instance.getAttributeData("FEDeviceAlarm:Auto Acknowledge")
        Master                  = instance.getAttributeData("LogicDeviceDefinitions:Master")
        alarmInputString = alarmInputs[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        if Type.lower() == "multiple":
            Type = Type + ": " + MultipleType
        if Master <> "":
            self.writeXmlRow([instanceName,description,alarmInputString,Type,Master,"","","","","",""])

    def processObjectInputs(self, objectInstances, inputPin):
        objectInput = {}
            
        listOfNotSpares = []
        for instance in objectInstances:
            instanceName    = instance.getAttributeData("DeviceIdentification:Name")
            description     = instance.getAttributeData("DeviceDocumentation:Description").strip()
            if description.lower() <> "spare" :
                listOfNotSpares.append(instanceName)	# add objects to check list
            else:
                objectInput[instanceName] = ""			# assign empty lists for spares 

        objectInput.update(self.multiFindGivenInput(listOfNotSpares, inputPin))
        
        return objectInput

    def processPCOSummary(self, instance):
        instanceName            = instance.getAttributeData("DeviceIdentification:Name")
        description             = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")

        # Writes the information in the buffer
        if description.lower() <> "spare":
            self.writeXmlRow([instanceName,description,"","","","","","","",""])

    def processCCCAlarm(self, instance, DAInstances):
        instanceName    = instance.getAttributeData("DeviceIdentification:Name")
        description     = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        Master                  = instance.getAttributeData("LogicDeviceDefinitions:Master")
        Param1                  = instance.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter1")


        # Writes the information in the buffer
        if Param1 == "CCC":
            #count the number of copies plugged on this CCC alarm
            NbCopies = 0
            copyList = []
            for DAInst in DAInstances:
                Param1  = DAInst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter1")
                Param2  = DAInst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")
                copyName = DAInst.getAttributeData("DeviceIdentification:Name")
                copyDesc = DAInst.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
                if Param1 == "copy" and Param2 == instanceName:
                    NbCopies = NbCopies+1;
                    copyList.append(copyName + " : " + copyDesc)

            self.writeXmlRow([instanceName,description,Master,str(NbCopies),"","","","","","",""])
            for copy in copyList:
                self.writeXmlRow(["","","",copy,"","","","","","",""])


    def processLogicPID(self, instance):
        instanceName            = instance.getAttributeData("DeviceIdentification:Name")
        description             = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        Master                          = instance.getAttributeData("LogicDeviceDefinitions:Master")
        MV                                      = instance.getAttributeData("FEDeviceEnvironmentInputs:Measured Value")
        ControlledDevice        = instance.getAttributeData("FEDeviceOutputs:Controlled Objects")
        RA                                      = instance.getAttributeData("FEDeviceParameters:Controller Parameters:RA")

        # Writes the information in the buffer
        if description.lower() <> "spare":
            self.writeXmlRow([instanceName,description,Master,MV,ControlledDevice,RA,"","","","","","","","",""])

    def processLogicActuator(self, instance, auOnRlogic, auOffRlogic):
        instanceName    = instance.getAttributeData("DeviceIdentification:Name")
        description     = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        nature                  = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature").lower()
        deviceType = self.objectTypeShortNameDict[instance.getDeviceTypeName()]
        auOnRlogicString = auOnRlogic[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        auOffRlogicString = auOffRlogic[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        # Writes the information in the buffer
        if (nature <> "bouton" or nature <> "button"):
            self.writeXmlRow([instanceName,description,deviceType,"AuOnR := " + auOnRlogicString,"AuOffR := " + auOffRlogicString,"","","","","","",""])

    def processLogicParameter(self, instance, PCOName, deviceType, parametersUsedIn):
        instanceName    = instance.getAttributeData("DeviceIdentification:Name")
        description     = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        pcoNames = parametersUsedIn[instanceName]
        if description.strip().lower() <> "spare" and PCOName in pcoNames:
            defaultValue    = instance.getAttributeData("FEDeviceParameters:Default Value")
            if deviceType != "DPAR":
                unit            = instance.getAttributeData("SCADADeviceGraphics:Unit")
                rangeMin        = instance.getAttributeData("FEDeviceParameters:Range Min")
                rangeMax        = instance.getAttributeData("FEDeviceParameters:Range Max")
                rangeStr = "[" + rangeMin + " : " + rangeMax + "]"
            else:
                unit = ""
                rangeStr = ""
            #pcoNames        = self.findUsedIn(instance)
            pcoNamesString  = ",".join(pcoNames)
            # Writes the information in the buffer
            self.writeXmlRow([instanceName,description,defaultValue,unit,rangeStr,"",pcoNamesString,"","","","","",""])

    def processLogicCmde(self, instance, PCOName, cmdeUsedIn):
        instanceName    = instance.getAttributeData("DeviceIdentification:Name")   
        description     = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        pcoNames = cmdeUsedIn[instanceName]
        if description.strip().lower() <> "spare" and PCOName in pcoNames:
            pcoNamesString  = ",".join(pcoNames)
            # Writes the information in the buffer
            self.writeXmlRow([instanceName,description,pcoNamesString,"","","","","",""])

    def writeSeparatorRow(self,n=1,text=""):
        self.thePlugin.writeCommissioningInfo('<Row ss:AutoFitHeight="0">' + \
          '<Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">' + text + '</Data></Cell>' + \
          '<Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String"></Data></Cell>' * (n-1) + '</Row>')

    def writeXmlRow(self,dataList):
        xmlString = '''<Row>'''
        for cellData in dataList:
            xmlString += '''<Cell ss:StyleID="allBorders"><Data ss:Type="String">''' + cellData + '''</Data></Cell>'''
        xmlString += '''</Row>'''
        self.thePlugin.writeCommissioningInfo(xmlString)

    def writeXmlWorksheetFooter(self):
        xmlString = '''
          </Table>
           <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
                   <Print>
                        <ValidPrinterInfo/>
                        <PaperSizeIndex>9</PaperSizeIndex>
                        <HorizontalResolution>600</HorizontalResolution>
                        <VerticalResolution>600</VerticalResolution>
                   </Print>
                   <TabColorIndex>40</TabColorIndex>
                   <Zoom>75</Zoom>
                   <Selected/>
                   <Panes>
                        <Pane>
                         <Number>3</Number>
                         <ActiveRow>22</ActiveRow>
                         <ActiveCol>5</ActiveCol>
                        </Pane>
                   </Panes>
                   <ProtectObjects>False</ProtectObjects>
                   <ProtectScenarios>False</ProtectScenarios>
                </WorksheetOptions>
                 </Worksheet>
        '''
        self.thePlugin.writeCommissioningInfo(xmlString)

    def end(self):
        self.thePlugin.writeInUABLog("IO Commissioning: end")
        

    def shutdown(self):
        self.thePlugin.writeInUABLog("IO Commissioning: shutdown")
