# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
import Reverse_Generic_Template
reload(Reverse_Generic_Template)
import Reverse_GlobalFunctions_Template
reload(Reverse_GlobalFunctions_Template)
    
class DigitalParameter_Template(Reverse_Generic_Template.Generic_Template): 
    theDeviceType = "DigitalParameter"
    
    def processInstance(self, instance, generatedInstance):
        
        key,value = self.getDescriptionWithoutElectricalDiagram(instance) #overwrite result from generic function
        generatedInstance.put(key,value)
        
        key,value = self.getMaskEvent(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getArchiveMode(instance)
        generatedInstance.put(key,value)
        
        key,value = self.getDefaultValue(instance)
        generatedInstance.put(key,value)
