# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class AnaDO_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for AnaDO.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for AnaDO.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for AnaDO.")
  
   def process(self, *params):
	self.thePlugin.writeInUABLog("processInstances in Jython for AnaDO.")
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	
	# General Steps for Non-Optimized objects:
	# 1. DB creation and initialization for each instance
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from the TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5: Create the Function. This function writes the input signals on each instance DB.
	# 		  For each DB we execute the FB.DB 
	# 		  And finally we pass the Status information from this instance DB to the corresponding Status DB (e.g DB_bin_status_AnaDO)
	
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	# This method is called on every Instance of the current type by the Code Generation plug-in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	
	# Step 1: DB creation and initialization for each instance
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//AnaDO DB Creation file: UNICOS application
''')
	for instance in instancesVector :
		if (instance is not None):
											
			Name = instance.getAttributeData("DeviceIdentification:Name")
			Description = instance.getAttributeData("DeviceDocumentation:Description")
			Deadband = self.thePlugin.formatNumberPLC(instance.getAttributeData ("FEDeviceParameters:Warning Deadband Value (Unit)"))
			HardwareAnalogOutput = instance.getAttributeData ("FEDeviceEnvironmentInputs:Hardware Analog Output")
			LocalON = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local On").strip()
			LocalOFF = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Off").strip()
			LocalDrive = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Drive")
			FeedbackAnalog = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Analog")
			FeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On")
			FailSafeOpen = instance.getAttributeData	("FEDeviceParameters:ParReg:Fail-Safe")
			PEnRstart = instance.getAttributeData	("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip()
			AnalogProcessOutput = instance.getAttributeData("FEDeviceOutputs:Analog Process Output")
			DigitalProcessOutput = instance.getAttributeData("FEDeviceOutputs:Digital Process Output")
			inst_AnalogProcessOutput_Vec = theRawInstances.findMatchingInstances ("AnalogOutput, AnalogOutputReal","'#DeviceIdentification:Name#'='$AnalogProcessOutput$'")
			inst_DigitalProcessOutput_Vec = theRawInstances.findMatchingInstances ("DigitalOutput","'#DeviceIdentification:Name#'='$DigitalProcessOutput$'")
			StepInc = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Manual Increase Step (Unit)"))
			StepDec = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Manual Decrease Step (Unit)"))
			ManualIncSpeed = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Manual Increase Speed (Unit/s)"))
			ManualDecSpeed = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Manual Decrease Speed (Unit/s)"))
			LimitOff = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceManualRequests:Parameter Limit Off/Closed"))
			LimitOn = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceManualRequests:Parameter Limit On/Open"))
			WarningDelay = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceParameters:Warning Time Delay (s)"))
			
			RangeMax = "100.0"
			RangeMin = "0.0"
			
			if (AnalogProcessOutput <> ""):
				for inst_AnalogProcessOutput in inst_AnalogProcessOutput_Vec:
					RangeMin = self.thePlugin.formatNumberPLC(inst_AnalogProcessOutput.getAttributeData("FEDeviceParameters:Range Min"))
					RangeMax = self.thePlugin.formatNumberPLC(inst_AnalogProcessOutput.getAttributeData("FEDeviceParameters:Range Max"))
			# Default values
			if WarningDelay.strip() == "":
				WarningDelay = "5.0"
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined WarningDelay, WarningDelay = $WarningDelay$ is taken.")
			if Deadband.strip() == "":
				Deadband = str((float(RangeMax)-float(RangeMin))/100)
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined Deadband, Deadband = $Deadband$ is taken.")
			if ManualIncSpeed.strip() == "":
				ManualIncSpeed = str((float(RangeMax)-float(RangeMin))/10)
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined ManualIncSpeed, ManualIncSpeed = $ManualIncSpeed$ is taken.")
			if ManualDecSpeed.strip() == "":
				ManualDecSpeed = str((float(RangeMax)-float(RangeMin))/10)
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined ManualDecSpeed, ManualDecSpeed = $ManualDecSpeed$ is taken.")
			if StepInc.strip() == "":
				StepInc = str((float(RangeMax)-float(RangeMin))/100)
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined StepInc, StepInc = $StepInc$ is taken.")
			if StepDec.strip() == "":
				StepDec = str((float(RangeMax)-float(RangeMin))/100)
				self.thePlugin.writeWarningInUABLog("ANALOG instance: $Name$. Undefined StepDec, StepDec = $StepDec$ is taken.")

			# PLiOn/Off default :  +/- 10% PMaxRan/PMinRan
			if LimitOn.strip() == "":
				LimitOn = str(float(RangeMax) - 0.1*(float(RangeMax)-float(RangeMin)))
				self.thePlugin.writeWarningInUABLog("ANADO instance: $Name$. Undefined LimitOn, LimitOn = $LimitOn$ is taken.")
			if LimitOff.strip()  == "":
				LimitOff = str(float(RangeMin) + 0.1*(float(RangeMax)-float(RangeMin)))
				self.thePlugin.writeWarningInUABLog("ANADO instance: $Name$. Undefined LimitOff, LimitOff = $LimitOff$ is taken.")
			
			
			# ParReg Logic				
			if (FailSafeOpen.lower()=="off/close"):
				bit8 = "0"
			elif (FailSafeOpen.lower()=="on/open"):
				bit8 = "1"
			else:
				bit8 = "0"
				
			if FeedbackOn == "":
				bit9='0'
			else:
				bit9='1'
			
			if FeedbackAnalog == "":
				bit11='0'
			else:
				bit11='1'
			
			if LocalDrive == "":
				bit12='0'
			else:
				bit12='1'
				
			if HardwareAnalogOutput == "":
				bit13='0'
			else:
				bit13='1'
			
			if PEnRstart.lower() == "false":
				bit0='0'
				bit1='0'				
			elif (PEnRstart.lower()=="true only if full stop disappeared"):
				bit0='1'
				bit1='0'
			else:
				bit0='1'
				bit1='1'
				
		
			self.thePlugin.writeInstanceInfo('''
DATA_BLOCK '''+Name+''' CPC_FB_AnaDO
BEGIN
	
	PAnalog.PArReg :=  2#00000$bit1$$bit0$00$bit13$$bit12$$bit11$0$bit9$$bit8$;
	PAnalog.PWDt := T#$WarningDelay$s;
	PAnalog.PWDb := $Deadband$;
	PAnalog.PMinSpd := $ManualIncSpeed$;
	PAnalog.PMDeSpd := $ManualDecSpeed$;
	PAnalog.PMStpInV := $StepInc$;
	PAnalog.PMStpDeV := $StepDec$;
	PAnalog.PMaxRan := $RangeMax$;
	PAnalog.PMinRan := $RangeMin$;
	
END_DATA_BLOCK
''')


	
	# Step 2: Create all the needed Structures from the TCT
	
	# UDT for DB_AnaDO_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE AnaDO_ManRequest
TITLE = AnaDO_ManRequest
//
// parameters of AnaDO Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				attributePrimitiveType = attribute.getPrimitiveType()
				attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
				
				self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')



	# UDT for all the bin status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE AnaDO_bin_Status
TITLE = AnaDO_bin_Status
//
// parameters of AnaDO Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification:  
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE AnaDO_event
TITLE = AnaDO_event
//
// parameters of PID Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# UDT for all the ana status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE AnaDO_ana_Status
TITLE = AnaDO_ana_Status
//
// parameters of AnaDO Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')
	
					
	# Step 3. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 
	
	# DB_EVENT
	
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_AnaDO Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_AnaDO
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type AnaDO
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_AnaDO : INT := '''+str(NbType)+''';
	AnaDO_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF AnaDO_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT
BEGIN
END_DATA_BLOCK

''')


	# Step 4: Create all the need DBs to store the required signal from the object
	
	# DB_AnaDO_ManRequest
	
	self.thePlugin.writeInstanceInfo('''
// DB_AnaDO_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_AnaDO_ManRequest
TITLE = DB_AnaDO_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    AnaDO_Requests : ARRAY [1..'''+instanceNb+'''] OF AnaDO_ManRequest;
END_STRUCT;
BEGIN
''')

	RecordNumber = 0
	for instance in instancesVector :								
		RecordNumber = RecordNumber + 1		
		Name = instance.getAttributeData("DeviceIdentification:Name")
		LimitOff = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceManualRequests:Parameter Limit Off/Closed"))
		LimitOn = self.thePlugin.formatNumberPLC(instance.getAttributeData	("FEDeviceManualRequests:Parameter Limit On/Open"))
		
		# PLiOn/Off default :  +/- 10% PMaxRan/PMinRan
		if LimitOn.strip() == "":
			LimitOn = str(float(RangeMax) - 0.1*(float(RangeMax)-float(RangeMin)))
			self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined LimitOn, LimitOn = $LimitOn$ is taken.")
		if LimitOff.strip()  == "":
			LimitOff = str(float(RangeMin) + 0.1*(float(RangeMax)-float(RangeMin)))
			self.thePlugin.writeWarningInUABLog("ANADIG instance: $Name$. Undefined LimitOff, LimitOff = $LimitOff$ is taken.")
				
		self.thePlugin.writeInstanceInfo('''
	AnaDO_Requests['''+str(RecordNumber)+'''].PliOff:= $LimitOff$;
	AnaDO_Requests['''+str(RecordNumber)+'''].PliOn:= $LimitOn$;
''')



	self.thePlugin.writeInstanceInfo('''  
END_DATA_BLOCK

''')
	
	self.thePlugin.writeInstanceInfo('''
	
(*binaries Status of the AnaDO OBJECTS************************************************)
DATA_BLOCK DB_bin_status_AnaDO
TITLE = 'DB_bin_status_AnaDO'
//
// Global binary status DB of AnaDO
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : AnaDO_bin_Status;
''')


	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK

(*old binaries Status of the AnaDO OBJECTS************************************************)
DATA_BLOCK DB_bin_status_AnaDO_old
TITLE = 'DB_bin_status_AnaDO_old'
//
// Old Global binary status DB of AnaDO
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
	''')
	
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : AnaDO_bin_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK

(*analogic Status of the AnaDO OBJECTS************************************************)

DATA_BLOCK DB_ana_status_AnaDO
TITLE = 'DB_ana_status_AnaDO'
//
// Global analogic status DB of AnaDO
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : AnaDO_ana_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK


(*old analogic Status of the AnaDO OBJECTS************************************************)

DATA_BLOCK DB_ana_status_AnaDO_old
TITLE = 'DB_ana_status_AnaDO_old'
//
// Old Global analogic status DB of AnaDO
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : AnaDO_ana_Status;
''')


	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN

END_DATA_BLOCK
''')


	
	# Step 5: Create the Function. This function writes the input signals on each instance DB.
	# For each DB we execute the FB.DB 
	# and finally we pass the Status information from this instance DB to the corresponding Status DB (e.g DB_bin_status_AnaDO)
	
	self.thePlugin.writeInstanceInfo('''
	
	
(*EXEC of AnaDO************************************************)
FUNCTION FC_AnaDO : VOID
TITLE = 'FC_AnaDO'
//
// AnaDO calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_ANA'
FAMILY: 'AnaDO'
VAR_TEMP
	old_status1 : DWORD;
	old_status2 : DWORD;
END_VAR
BEGIN
''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		HardwareAnalogOutput = instance.getAttributeData ("FEDeviceEnvironmentInputs:Hardware Analog Output")
		LocalON = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local On").strip()
		LocalOFF = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Off").strip()
		LocalDrive = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Drive")
		FeedbackAnalog = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Analog")
		FeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On")
		FailSafeOpen = instance.getAttributeData	("FEDeviceParameters:ParReg:Fail-Safe")
		AnalogProcessOutput = instance.getAttributeData("FEDeviceOutputs:Analog Process Output")
		DigitalProcessOutput = instance.getAttributeData("FEDeviceOutputs:Digital Process Output")
		
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
// ----------------------------------------------
// ---- AnaDO <'''+ str(RecordNumber) +'''>: $Name$
// ----------------------------------------------
old_status1 := DB_bin_status_AnaDO.$Name$.stsreg01;
old_status2 := DB_bin_status_AnaDO.$Name$.stsreg02;
old_status1 := ROR(IN:=old_status1, N:=16);
old_status2 := ROR(IN:=old_status2, N:=16);

$Name$.ManReg01:=DB_AnaDO_ManRequest.AnaDO_Requests['''+ str(RecordNumber) +'''].ManReg01;
$Name$.MPosR:=DB_AnaDO_ManRequest.AnaDO_Requests['''+ str(RecordNumber) +'''].MPosR;
$Name$.PLiOn:=DB_AnaDO_ManRequest.AnaDO_Requests['''+ str(RecordNumber) +'''].PLiOn;
$Name$.PLiOff:=DB_AnaDO_ManRequest.AnaDO_Requests['''+ str(RecordNumber) +'''].PLiOff;

''')
	
		if FeedbackOn <> "":
			s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HFOn:='''+s7db_id_result+FeedbackOn+'''.PosSt;''')
			

		if FeedbackAnalog <> "":
			s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal, AnalogStatus, WordStatus", True)#return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications
			self.thePlugin.writeInstanceInfo('''
$Name$.HFPos:='''+s7db_id_result+FeedbackAnalog+'''.PosSt;''')
			
			
		if LocalDrive <> "":
			NotLocal = LocalDrive[0:4].lower()
			if NotLocal == "not ":
				LocalDrive = LocalDrive[4:]
			else:
				NotLocal = ""
				LocalDrive = LocalDrive
				
			s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HLD:='''+NotLocal+s7db_id_result+LocalDrive+'''.PosSt;''')

		if LocalON <> "":
			s7db_id_result=self.thePlugin.s7db_id(LocalON, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HOnR:='''+s7db_id_result+LocalON+'''.PosSt;''')
			
		if LocalOFF <> "":
			s7db_id_result=self.thePlugin.s7db_id(LocalOFF, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HOffR:='''+s7db_id_result+LocalOFF+'''.PosSt;''')
			
		self.thePlugin.writeInstanceInfo('''
		
// IO Error
''')
		
		if (FeedbackOn == "" and FeedbackAnalog == "" and LocalDrive == "" and LocalON == "" and LocalOFF == "" and AnalogProcessOutput == "" and DigitalProcessOutput == ""):
			# Do nothing
			message = "there are not any Feedback"
		else:
			self.thePlugin.writeInstanceInfo(Name+'''.IoError := ''')
			NbIOError = 0
			if FeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackOn+'''.IoErrorW''')
				NbIOError = NbIOError +1 

			if FeedbackAnalog <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal", True)#return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications
				if s7db_id_result <> "":
					if NbIOError <> 0:
						self.thePlugin.writeInstanceInfo('''
	OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result+FeedbackAnalog+'''.IoErrorW''')
					NbIOError = NbIOError +1
				
			if LocalDrive <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalDrive+'''.IoErrorW''')
				NbIOError = NbIOError +1
				
			if LocalON <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalON, "DigitalInput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalON+'''.IoErrorW''')
				NbIOError = NbIOError +1 
				
			if LocalOFF <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalOFF, "DigitalInput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalOFF+'''.IoErrorW''')
				NbIOError = NbIOError +1 
				
			if AnalogProcessOutput <> "":
				s7db_id_result=self.thePlugin.s7db_id(AnalogProcessOutput, "AnalogOutput, AnalogOutputReal")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+AnalogProcessOutput+'''.IoErrorW''')
				NbIOError = NbIOError +1
				
			if DigitalProcessOutput <> "":
				s7db_id_result=self.thePlugin.s7db_id(DigitalProcessOutput, "DigitalOutput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+DigitalProcessOutput+'''.IoErrorW''')
				NbIOError = NbIOError +1
			
			self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoError;
		
				
// IOSimu
''')
		if (FeedbackOn == "" and FeedbackAnalog == "" and LocalDrive == "" and LocalON == "" and LocalOFF == "" and AnalogProcessOutput == "" and DigitalProcessOutput == ""):
			# Do nothing
			message = "there are not any Feedback"
		else:
			self.thePlugin.writeInstanceInfo(Name+'''.IoSimu := ''')
			NbIOSImu = 0
			if FeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackOn + '''.IoSimuW OR ''' + s7db_id_result+FeedbackOn+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
	
			if FeedbackAnalog <> "":
				s7db_id_result=self.thePlugin.s7db_id(FeedbackAnalog, "AnalogInput, AnalogInputReal", True)#return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications
				if s7db_id_result <> "":
					if NbIOSImu <> 0:
						self.thePlugin.writeInstanceInfo('''
	OR ''')
					self.thePlugin.writeInstanceInfo(s7db_id_result + FeedbackAnalog + '''.IoSimuW OR ''' + s7db_id_result+FeedbackAnalog+'''.FoMoSt''')
					NbIOSImu = NbIOSImu +1 
			
			if LocalDrive <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result + LocalDrive + '''.IoSimuW OR ''' + s7db_id_result+LocalDrive+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if LocalON <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalON, "DigitalInput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalON+'''.IoSimuW OR ''' + s7db_id_result+LocalON+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if LocalOFF <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalOFF, "DigitalInput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalOFF+'''.IoSimuW OR ''' + s7db_id_result+LocalOFF+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1
				
			if AnalogProcessOutput <> "":
				s7db_id_result=self.thePlugin.s7db_id(AnalogProcessOutput, "AnalogOutput, AnalogOutputReal", True)#return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result + AnalogProcessOutput + '''.IoSimuW OR ''' + s7db_id_result+AnalogProcessOutput+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
			
			if DigitalProcessOutput <> "":
				s7db_id_result=self.thePlugin.s7db_id(DigitalProcessOutput, "DigitalOutput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result + DigitalProcessOutput + '''.IoSimuW OR ''' + s7db_id_result+DigitalProcessOutput+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 

			self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoSimu;
''')

		self.thePlugin.writeInstanceInfo(''';

// Calls the Baseline function			
CPC_FB_AnaDO.$Name$();
''')
		
		      
		 
		if AnalogProcessOutput <> "":
			s7db_id_result=self.thePlugin.s7db_id(AnalogProcessOutput, "AnalogOutput, AnalogOutputReal")
			self.thePlugin.writeInstanceInfo(s7db_id_result+AnalogProcessOutput+'''.AuposR := '''+Name+'''.OutOV;
''')

		if DigitalProcessOutput <> "":
			s7db_id_result=self.thePlugin.s7db_id(DigitalProcessOutput, "DigitalOutput")
			self.thePlugin.writeInstanceInfo(s7db_id_result+DigitalProcessOutput+'''.AuposR := '''+Name+'''.OutOnOV;
''')
		self.thePlugin.writeInstanceInfo('''
//Reset AuAuMoR and AuAlAck
$Name$.AuAuMoR := FALSE;	
$Name$.AuAlAck := FALSE;	

//Recopy new status
DB_bin_status_AnaDO.$Name$.stsreg01:= $Name$.Stsreg01;
DB_bin_status_AnaDO.$Name$.stsreg02:= $Name$.Stsreg02;
DB_ana_status_AnaDO.$Name$.PosSt:= $Name$.PosSt;
DB_ana_status_AnaDO.$Name$.AuPosRSt:= $Name$.AuPosRSt;
DB_ana_status_AnaDO.$Name$.MPosRSt:= $Name$.MPosRSt;
DB_ana_status_AnaDO.$Name$.PosRSt:= $Name$.PosRSt;
DB_Event_AnaDO.AnaDO_evstsreg['''+str(RecordNumber)+'''].evstsreg01 := old_status1 OR DB_bin_status_AnaDO.$Name$.stsreg01;
DB_Event_AnaDO.AnaDO_evstsreg['''+str(RecordNumber)+'''].evstsreg02 := old_status2 OR DB_bin_status_AnaDO.$Name$.stsreg02;
''')
	LimitNumber = self.thePlugin.getTargetDeviceInformationParam("LimitSize","AnaDO")
	if RecordNumber >= LimitNumber:
		self.thePlugin.writeInstanceInfo(''' 
END_FUNCTION
		    
(***** EXEC of AnaDO 2************************************************)
// Large number of AnaDO calls . Code is split in two FC_AnaDO and FC_AnaDO2
   
FUNCTION FC_AnaDO2 : VOID
TITLE = 'FC_AnaDO2'
//
// AnaDO calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_AN2'
FAMILY: 'AnaDO'
VAR_TEMP
	old_status1 : DWORD;
	old_status2 : DWORD;
END_VAR
BEGIN
		''')
	
	
	self.thePlugin.writeInstanceInfo('''
	
END_FUNCTION''')


   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for AnaDO.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for AnaDO.")
		
