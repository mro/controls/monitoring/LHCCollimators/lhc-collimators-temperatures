# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for OnOff Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class OnOff_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for ONOFF.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for ONOFF.")

   def begin(self):
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'
	self.thePlugin.writeInUABLog("begin in Jython for ONOFF.")

   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for ONOFF.")
	
	# General Steps for Non-Optimized objects:
	# 1. DB creation and initialization for each instance
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects  
	# 3. Create all the needed Structures from the TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5: Create the Function. This function writes the input signals on each instance DB.
	#    For each DB we execute the FB.DB 
	# 	 And finally we pass the Status information from this instance DB to the corresponding Status DB (e.g DB_bin_status_ONFF)
	
	#This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	
	# Step 1: DB creation and initialization for each instance
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//OnOff DB Creation file: UNICOS application
''')

	for instance in instancesVector :
		if (instance is not None):
			ProcessOutput 	= instance.getAttributeData ("FEDeviceOutputs:Process Output")	
			ProcessOutputOff = instance.getAttributeData	("FEDeviceOutputs:Process Output Off")
			Name = instance.getAttributeData("DeviceIdentification:Name").strip()
			Description = instance.getAttributeData("DeviceDocumentation:Description").strip()
			FailSafeOpen = instance.getAttributeData	("FEDeviceParameters:ParReg:Fail-Safe").strip()
			PAnim = instance.getAttributeData	("FEDeviceParameters:ParReg:Full/Empty Animation").strip()
			PEnRstart = instance.getAttributeData	("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip()
			PPulseCste = instance.getAttributeData ("FEDeviceParameters:ParReg:Constant Time Pulse")
			PulseLength = instance.getAttributeData	("FEDeviceParameters:Pulse Duration (s)").strip()
			WarningDelay = instance.getAttributeData	("FEDeviceParameters:Warning Time Delay (s)").strip()			
			HardwareFeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On").strip()
			HardwareFeedbackOff = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Off").strip()
			LocalDrive = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Drive").strip()
			LocalON = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local On").strip()
			LocalOFF = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Off").strip()
			
			# ParReg Logic
			if FailSafeOpen.lower() == "on/open":
				bit8='1'
				bit2='0'
			elif FailSafeOpen.lower() == "off/close":
				bit8='0'
				bit2='0'
			elif FailSafeOpen.lower() == "2 do off":
				bit8='0'
				bit2='1'
			elif FailSafeOpen.lower() == "2 do on":
				bit8='1'
				bit2='1'
		
			if HardwareFeedbackOn == "":
				bit9='0'
			else:
				bit9='1'
			
			if HardwareFeedbackOff == "":
				bit10='0'
			else:
				bit10='1'
			
			if PulseLength == "" or PulseLength == "0":
				bit11='0'
			else:
				bit11='1'
				
			if LocalDrive == "":
				bit12='0'
			else:
				bit12='1'
			
			if LocalOFF == "" and LocalON == "":
				bit13='0'
			else:
				bit13='1'

			if PAnim == "Full/Empty":
				bit14='1'
			else:
				bit14='0'
	
			if ProcessOutputOff == "":
				bit15='0'
			else:
				bit15='1'
			
			if PEnRstart.lower() == "false":
				bit0='0'
			else:
				bit0='1'
			
			if PulseLength == "":
				PulseLength='0'
			
			if WarningDelay == "":
				WarningDelay='5'
			
			if PEnRstart.lower() == "false":
				bit0='0'
				bit1='0'				
			elif (PEnRstart.lower()=="true only if full stop disappeared"):
				bit0='1'
				bit1='0'
			else:
				bit0='1'
				bit1='1'

			if PPulseCste.lower() in ["", "false"]:
				bit3='0'
			elif PPulseCste.lower() == "true":
				bit3='1'
			else:
				bit3='0'
			
			self.thePlugin.writeInstanceInfo('''
DATA_BLOCK '''+Name+''' CPC_FB_ONOFF
BEGIN

	POnOff.ParReg :=  2#000$bit3$$bit2$$bit1$$bit0$$bit15$$bit14$$bit13$$bit12$$bit11$$bit10$$bit9$$bit8$;
	POnOff.PPulseLe := T#$PulseLength$s;
	POnOff.PWDt := T#$WarningDelay$s;
	
END_DATA_BLOCK
''')
	
	# Step 3: Create all the needed Structures from the TCT
	
	# UDT for DB_ONOFF_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE ONOFF_ManRequest
TITLE = ONOFF_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				attributePrimitiveType = attribute.getPrimitiveType()
				attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
				self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE ONOFF_bin_Status
TITLE = ONOFF_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE

''')
		
		
	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE ONOFF_event
TITLE = ONOFF_event
//
// parameters of ONOFF Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				# isEventAttribute = attribute.getIsEventAttribute()
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	
	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_ONOFF_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_ONOFF_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_ONOFF_ManRequest
TITLE = DB_ONOFF_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    ONOFF_Requests : ARRAY [1..'''+instanceNb+'''] OF ONOFF_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')	

	# Step 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects .
	
	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_ONOFF Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_ONOFF
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type ONOFF
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_ONOFF : INT := '''+str(NbType)+''';
	ONOFF_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF ONOFF_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT
BEGIN
END_DATA_BLOCK

''')
	
	self.thePlugin.writeInstanceInfo('''
	
(*Status of the ONOFF OBJECTS************************************************)
DATA_BLOCK DB_bin_status_ONOFF
TITLE = 'DB_bin_status_ONOFF'
//
// Global binary status DB of ONOFF
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')
	
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : ONOFF_bin_Status;
''')
			
	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN
END_DATA_BLOCK

(*old Status of the ONOFF OBJECTS************************************************)
DATA_BLOCK DB_bin_status_ONOFF_old
TITLE = 'DB_bin_status_ONOFF_old'
//
// Old Global binary status DB of ONOFF
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : ONOFF_bin_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN
END_DATA_BLOCK
''')

	# Step 5: Create the Function. This function writes the input signals on each instance DB.
	# For each DB we execute the FB.DB 
	# and finally we pass the Status information from this instance DB to the corresponding Status DB (e.g DB_bin_status_ONOFF)
		
	self.thePlugin.writeInstanceInfo('''

(*EXEC of ONOFF************************************************)
FUNCTION FC_ONOFF : VOID
TITLE = 'FC_ONOFF'
//
// ONOFF calls
//
AUTHOR: 'UNICOS'
NAME: 'Call_OO'
FAMILY: 'OnOff'
VAR_TEMP
    old_status1 : DWORD;
    old_status2 : DWORD;
END_VAR
BEGIN
''')		

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		Description = instance.getAttributeData("DeviceDocumentation:Description")
		FailSafeOpen = instance.getAttributeData	("FEDeviceParameters:ParReg:Fail-Safe")
		PAnim = instance.getAttributeData	("FEDeviceParameters:ParReg:Full/Empty Animation")
		PulseLength = instance.getAttributeData	("FEDeviceParameters:Pulse Duration (s)")
		WarningDelay = instance.getAttributeData	("FEDeviceParameters:Warning Time Delay (s)")
		HardwareFeedbackOn = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback On")
		HardwareFeedbackOff = instance.getAttributeData	("FEDeviceEnvironmentInputs:Feedback Off")
		LocalDrive = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Drive")
		LocalON = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local On")
		LocalOFF = instance.getAttributeData	("FEDeviceEnvironmentInputs:Local Off")
		ProcessOutput = instance.getAttributeData	("FEDeviceOutputs:Process Output")
		ProcessOutputOff = instance.getAttributeData	("FEDeviceOutputs:Process Output Off")
		
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
// ----------------------------------------------
// ---- OnOff <'''+ str(RecordNumber) +'''>: $Name$
// ----------------------------------------------
old_status1 := DB_bin_status_ONOFF.$Name$.stsreg01;
old_status2 := DB_bin_status_ONOFF.$Name$.stsreg02;
old_status1 := ROR(IN:=old_status1, N:=16);
old_status2 := ROR(IN:=old_status2, N:=16);

$Name$.ManReg01:=DB_ONOFF_ManRequest.ONOFF_Requests['''+ str(RecordNumber) +'''].ManReg01;

''')

		if HardwareFeedbackOn <> "":
			s7db_id_result=self.thePlugin.s7db_id(HardwareFeedbackOn, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HFOn:='''+s7db_id_result+HardwareFeedbackOn+'''.PosSt;''')	
			
		if HardwareFeedbackOff <> "":
			s7db_id_result=self.thePlugin.s7db_id(HardwareFeedbackOff, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HFOff:='''+s7db_id_result+HardwareFeedbackOff+'''.PosSt;''')
				
		if LocalDrive <> "":
			NotLocal = LocalDrive[0:4].lower()
			if NotLocal == "not ":
				LocalDrive = LocalDrive[4:]
			else:
				NotLocal = ""
				LocalDrive = LocalDrive
				
			s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HLD:='''+NotLocal+s7db_id_result+LocalDrive+'''.PosSt;''')
			
		if LocalON <> "":
			s7db_id_result=self.thePlugin.s7db_id(LocalON, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HOnR:='''+s7db_id_result+LocalON+'''.PosSt;''')
			
		if LocalOFF <> "":
			s7db_id_result=self.thePlugin.s7db_id(LocalOFF, "DigitalInput")
			self.thePlugin.writeInstanceInfo('''
$Name$.HOffR:='''+s7db_id_result+LocalOFF+'''.PosSt;''')
				
		self.thePlugin.writeInstanceInfo('''
		
// IOError
''')

		if (HardwareFeedbackOn == "" and HardwareFeedbackOff == "" and LocalDrive == "" and LocalON == "" and LocalOFF == "" and ProcessOutput == "" and ProcessOutputOff == ""):
			self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOError
// No FeedbackOn, FeedbackOff, LocalDrive, LocalON and LocalOFF, ProcessOutput configured
''')
		else:
			self.thePlugin.writeInstanceInfo(Name+'''.IoError := ''')
			NbIOError = 0
			if HardwareFeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(HardwareFeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo(s7db_id_result+HardwareFeedbackOn+'''.IoErrorW''')
				NbIOError = NbIOError +1 
			
			if HardwareFeedbackOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(HardwareFeedbackOff, "DigitalInput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+HardwareFeedbackOff+'''.IoErrorW''')
				NbIOError = NbIOError +1 
				
			if LocalDrive <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalDrive+'''.IoErrorW''')
				NbIOError = NbIOError +1 
				
			if LocalON <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalON, "DigitalInput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalON+'''.IoErrorW''')
				NbIOError = NbIOError +1 
				
			if LocalOFF <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalOFF, "DigitalInput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalOFF+'''.IoErrorW''')
				NbIOError = NbIOError +1 
				
			if ProcessOutput <> "":
				s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "DigitalOutput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutput+'''.IoErrorW''')
				NbIOError = NbIOError +1 

			if ProcessOutputOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(ProcessOutputOff, "DigitalOutput")
				if NbIOError <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutputOff+'''.IoErrorW''')
				NbIOError = NbIOError +1 
				
			self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoError;
			
// IOSimu
''')


		if (HardwareFeedbackOn == "" and HardwareFeedbackOff == "" and LocalDrive == "" and LocalON == "" and LocalOFF == "" and ProcessOutput == "" and ProcessOutputOff == ""):
			self.thePlugin.writeInstanceInfo('''// Nothing to add to the IOSimu
// No FeedbackOn, FeedbackOff, LocalDrive, LocalON and LocalOFF, ProcessOutput configured
''')
		else:
			self.thePlugin.writeInstanceInfo(Name+'''.IoSimu := ''')
			NbIOSImu = 0
			if HardwareFeedbackOn <> "":
				s7db_id_result=self.thePlugin.s7db_id(HardwareFeedbackOn, "DigitalInput")
				self.thePlugin.writeInstanceInfo(s7db_id_result+HardwareFeedbackOn+'''.IoSimuW OR ''' + s7db_id_result+HardwareFeedbackOn+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if HardwareFeedbackOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(HardwareFeedbackOff, "DigitalInput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+HardwareFeedbackOff+'''.IoSimuW OR ''' + s7db_id_result+HardwareFeedbackOff+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if LocalDrive <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalDrive, "DigitalInput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalDrive+'''.IoSimuW OR ''' + s7db_id_result+LocalDrive+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if LocalON <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalON, "DigitalInput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalON+'''.IoSimuW OR ''' + s7db_id_result+LocalON+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if LocalOFF <> "":
				s7db_id_result=self.thePlugin.s7db_id(LocalOFF, "DigitalInput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+LocalOFF+'''.IoSimuW OR ''' + s7db_id_result+LocalOFF+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
				
			if ProcessOutput <> "":
				s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "DigitalOutput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutput+'''.IoSimuW OR ''' + s7db_id_result+ProcessOutput+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 

			if ProcessOutputOff <> "":
				s7db_id_result=self.thePlugin.s7db_id(ProcessOutputOff, "DigitalOutput")
				if NbIOSImu <> 0:
					self.thePlugin.writeInstanceInfo('''
OR ''')
				self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutputOff+'''.IoSimuW OR ''' + s7db_id_result+ProcessOutputOff+'''.FoMoSt''')
				NbIOSImu = NbIOSImu +1 
			
			self.thePlugin.writeInstanceInfo('''
OR '''+Name+'''.IoSimu;
''')

		self.thePlugin.writeInstanceInfo(''';

// Calls the Baseline function
CPC_FB_ONOFF.$Name$();
''')

		if ProcessOutput <> "":
			s7db_id_result=self.thePlugin.s7db_id(ProcessOutput, "DigitalOutput")
			self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutput+'''.AuposR := '''+Name+'''.OutOnOV;
''')

		if ProcessOutputOff <> "":
			s7db_id_result=self.thePlugin.s7db_id(ProcessOutputOff, "DigitalOutput")
			self.thePlugin.writeInstanceInfo(s7db_id_result+ProcessOutputOff+'''.AuposR := '''+Name+'''.OutOffOV;
''')
			
		self.thePlugin.writeInstanceInfo('''
//Reset AuAuMoR and AuAlAck
$Name$.AuAuMoR := FALSE;	
$Name$.AuAlAck := FALSE;	

//Recopy new status		
DB_bin_status_ONOFF.$Name$.stsreg01:= $Name$.Stsreg01;
DB_bin_status_ONOFF.$Name$.stsreg02:= $Name$.Stsreg02;
DB_Event_ONOFF.ONOFF_evstsreg['''+str(RecordNumber)+'''].evstsreg01 := old_status1 OR DB_bin_status_ONOFF.$Name$.stsreg01;
DB_Event_ONOFF.ONOFF_evstsreg['''+str(RecordNumber)+'''].evstsreg02 := old_status2 OR DB_bin_status_ONOFF.$Name$.stsreg02;
''')
		
	self.thePlugin.writeInstanceInfo('''
	
END_FUNCTION''')
	
	
   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for ONOFF.")


   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for ONOFF.")
