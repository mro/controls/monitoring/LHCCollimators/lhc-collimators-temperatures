# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
#from research.ch.cern.unicos.types.ai import Instance                 	#REQUIRED
from time import strftime

class Controller_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for PID.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for PID.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for PID.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'

   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for PID.")
	
	# General Steps for Non-Optimized objects:
	# 1. DB creation and initialization for each instance
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from the TCT
	# 4. Create all the need DBs to store the required signal from the object
		
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	# This method is called on every Instance of the current type by the Code Generation plug-in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	
	# Step 1: DB creation and initialization for each instance
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//PID DB Creation file: UNICOS application
''')
	instancesVectorNb = instancesVector.size()
	ScaMethodVector = []
	i = 0
	for instance in instancesVector :
		ScaMethod = instance.getAttributeData("FEDeviceParameters:Controller Parameters:Scaling Method")
		ScaMethodVector.append(ScaMethod)
		if i > 0:
			if ScaMethodVector[i] <> ScaMethodVector[i-1]:
				self.thePlugin.writeWarningInUABLog('''PID instances: All the Controller instances don't have the same Scaling Method''')
		i = i+1
		
	for instance in instancesVector :
		if (instance is not None):
			
			Name = instance.getAttributeData("DeviceIdentification:Name")
			PMaxRan = '100.0'
			PMinRan = '0.0'
			POutMaxRan = '100.0'
			POutMinRan = '0.0'
			Kc = '0'	
			Ti = '0'	
			Td = '0'	
			Tds = '0'
			SPHighLimit = '0'
			SPLowLimit = '0'	
			OutHighLimit = '0'	
			OutLowLimit = '0'

			MeasuredValue = instance.getAttributeData("FEDeviceEnvironmentInputs:Measured Value")
			MeasuredValueVector = theRawInstances.findMatchingInstances("AnalogInput, AnalogInputReal, AnalogStatus", "'#DeviceIdentification:Name#'='$MeasuredValue$'")
			for MeasuredValueInst in MeasuredValueVector:
				PMaxRan = self.thePlugin.formatNumberPLC(MeasuredValueInst.getAttributeData("FEDeviceParameters:Range Max"))
				PMinRan = self.thePlugin.formatNumberPLC(MeasuredValueInst.getAttributeData("FEDeviceParameters:Range Min"))
			
			#Set POutMinRan/POutMaxRan
			ControlledObjectNames = instance.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ")
			ControlledObjectList = ControlledObjectNames.split() 
			ControlledObjectNumber = len(ControlledObjectList)
			POutMinRan		= self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Controller Parameters:Output Range Min"))
			POutMaxRan		= self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Controller Parameters:Output Range Max"))
			if (POutMaxRan == "" and ControlledObjectNumber == 0):
				POutMaxRan = "100.0"
				POutMinRan = "0.0"
				self.thePlugin.writeWarningInUABLog("PID instance: $Name$. The absence of ControlledObjects forces the POutMaxRan to ($POutMaxRan$) and the POutMinRan to ($POutMinRan$).")
			elif POutMaxRan == "" and ControlledObjectNumber <> 0:
				ControlledObject1AnalogVector = theRawInstances.findMatchingInstances("Analog","'#DeviceIdentification:Name#'='$ControlledObjectList[0]$'")
				ControlledObject1AnalogDigitalVector = theRawInstances.findMatchingInstances("AnalogDigital", "'#DeviceIdentification:Name#'='$ControlledObjectList[0]$'")
				ControlledObject1AnaDOVector = theRawInstances.findMatchingInstances("AnaDO", "'#DeviceIdentification:Name#'='$ControlledObjectList[0]$'")
				if ControlledObject1AnalogVector.size() >  0:
					for ControlledObject1AnalogInst in ControlledObject1AnalogVector:
						POutMaxRan = "100.0"
						POutMinRan = "0.0"
						processOutput= ControlledObject1AnalogInst.getAttributeData("FEDeviceOutputs:Process Output")
						processOutputVector = theRawInstances.findMatchingInstances("AnalogOutput, AnalogOutputReal", "'#DeviceIdentification:Name#'='$processOutput$'")
						for processOutputInst in processOutputVector:
							POutMaxRan = self.thePlugin.formatNumberPLC(processOutputInst.getAttributeData("FEDeviceParameters:Range Max"))
							POutMinRan = self.thePlugin.formatNumberPLC(processOutputInst.getAttributeData("FEDeviceParameters:Range Min"))
				elif ControlledObject1AnaDOVector.size() >  0:
					for ControlledObject1AnaDOInst in ControlledObject1AnaDOVector:
						POutMaxRan = "100.0"
						POutMinRan = "0.0"
						processOutput= ControlledObject1AnaDOInst.getAttributeData("FEDeviceOutputs:Analog Process Output")
						processOutputVector = theRawInstances.findMatchingInstances("AnalogOutput, AnalogOutputReal", "'#DeviceIdentification:Name#'='$processOutput$'")
						for processOutputInst in processOutputVector:
							POutMaxRan = self.thePlugin.formatNumberPLC(processOutputInst.getAttributeData("FEDeviceParameters:Range Max"))
							POutMinRan = self.thePlugin.formatNumberPLC(processOutputInst.getAttributeData("FEDeviceParameters:Range Min"))
				elif ControlledObject1AnalogDigitalVector.size() > 0:
					for ControlledObject1AnalogDigitalInst in ControlledObject1AnalogDigitalVector:
						POutMaxRan = self.thePlugin.formatNumberPLC(ControlledObject1AnalogDigitalInst.getAttributeData("FEDeviceParameters:Range Max"))
						POutMinRan = self.thePlugin.formatNumberPLC(ControlledObject1AnalogDigitalInst.getAttributeData("FEDeviceParameters:Range Min"))
				else:
					ControlledObject1ControllerVector = theRawInstances.findMatchingInstances("Controller","'#DeviceIdentification:Name#'='$ControlledObjectList[0]$'")
					if ControlledObject1ControllerVector.size() >  0:
						for ControlledObject1ControllerInst in ControlledObject1ControllerVector:
							MeasuredValue = ControlledObject1ControllerInst.getAttributeData("FEDeviceEnvironmentInputs:Measured Value")
							MeasuredValueVector = theRawInstances.findMatchingInstances("AnalogInput, AnalogInputReal, AnalogStatus", "'#DeviceIdentification:Name#'='$MeasuredValue$'")
							if MeasuredValueVector.size() > 0:
								for MeasuredValueInst in MeasuredValueVector:
									POutMaxRan = self.thePlugin.formatNumberPLC(MeasuredValueInst.getAttributeData("FEDeviceParameters:Range Max"))
									POutMinRan = self.thePlugin.formatNumberPLC(MeasuredValueInst.getAttributeData("FEDeviceParameters:Range Min"))
							else:
								POutMaxRan = "100.0"
								POutMinRan = "0.0"
								self.thePlugin.writeWarningInUABLog("PID instance: $Name$. The ControlledObject $ControlledObjectList[0]$ cannot be a controlled object. POutMaxRan has been forced to ($POutMaxRan$) and POutMinRan has been forced to ($POutMinRan$)")
			
			MVFiltTime = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Controller Parameters:MV Filter Time (s)"))
			PIDCycle = instance.getAttributeData("FEDeviceParameters:Controller Parameters:PID Cycle (s)")
			ScaMethod = instance.getAttributeData("FEDeviceParameters:Controller Parameters:Scaling Method")
			if ScaMethod == "Input Scaling":
				ScaMethod = '1'
			if ScaMethod == "Input/Output Scaling":
				ScaMethod = '2'
			if ScaMethod == "No Scaling":
				ScaMethod = '3'
			RA = instance.getAttributeData("FEDeviceParameters:Controller Parameters:RA")
			InSpd = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceAutoRequests:Default Setpoint Speed:Increase Speed"))
			DeSpd = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceAutoRequests:Default Setpoint Speed:Decrease Speed"))
			
			Kc = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Kc"))
			if Kc == "":
				Kc = '1.0'	
			Ti = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Ti"))	
			if Ti == "":
				Ti = '100.0'	
			Td = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Td"))
			if Td == "":
				Td = '0.0'	
			Tds = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Tds"))
			if Tds == "":
				Tds = '0.0'	
			SP = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Setpoint"))
			if SP == "":
				SP = '0.0'	
			SPHighLimit = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:SP High Limit"))
			if SPHighLimit == "":
				SPHighLimit = PMaxRan	
			SPLowLimit = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:SP Low Limit"))
			if SPLowLimit == "":
				SPLowLimit = PMinRan	
			OutHighLimit = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Out High Limit"))
			if OutHighLimit == "":
				OutHighLimit = POutMaxRan	
			OutLowLimit = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Out Low Limit"))
			if OutLowLimit == "":
				OutLowLimit = POutMinRan
			
			self.thePlugin.writeInstanceInfo('''
DATA_BLOCK '''+Name+''' CPC_FB_PID
BEGIN

	PControl.PMaxRan := $PMaxRan$;
	PControl.PMinRan := $PMinRan$;
	PControl.POutMaxRan := $POutMaxRan$;
	PControl.POutMinRan := $POutMinRan$;
	PControl.MVFiltTime := T#$MVFiltTime$s;
	PControl.PIDCycle := T#$PIDCycle$s;
	PControl.ScaMethod := $ScaMethod$;
	PControl.RA := $RA$;

	AuSPSpd.InSpd := $InSpd$;
	AuSPSpd.DeSpd := $DeSpd$;


	DefKc := $Kc$;
	DefTi := $Ti$;
	DefTd := $Td$;
	DefTds := $Tds$;
	DefSP := $SP$; 
	DefSPH := $SPHighLimit$;
	DefSPL := $SPLowLimit$;
	DefOutH := $OutHighLimit$;
	DefOutL := $OutLowLimit$; 




END_DATA_BLOCK
''')	


	# Step 3: Create all the needed Structures from the TCT
	
	# UDT for DB_PID_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE PID_ManRequest
TITLE = PID_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				attributePrimitiveType = attribute.getPrimitiveType()
				attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
				
				self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the bin status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE PID_bin_Status
TITLE = PID_bin_Status
//
// parameters of PID Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				# isEventAttribute = attribute.getIsEventAttribute()
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE PID_event
TITLE = PID_event
//
// parameters of PID Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# UDT for all the ana status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE PID_ana_Status
TITLE = PID_ana_Status
//
// parameters of PID Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# Step 3. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 

	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_PID Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_PID
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type PID
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_PID : INT := '''+str(NbType)+''';
	PID_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF PID_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT
BEGIN
END_DATA_BLOCK

''')
	


	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_PID_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_PID_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_PID_ManRequest
TITLE = DB_PID_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    PID_Requests : ARRAY [1..'''+instanceNb+'''] OF PID_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')

	self.thePlugin.writeInstanceInfo('''
(*binaries Status of the PID OBJECTS ************************************************)
DATA_BLOCK DB_bin_status_PID
TITLE = 'DB_bin_status_PID'
//
// Global binaries status DB of PID
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : PID_bin_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN
END_DATA_BLOCK
(*old binaries Status of the CONTROLLER OBJECTS************************************************)
DATA_BLOCK DB_bin_status_PID_old
TITLE = 'DB_bin_status_PID_old'
//
// Old Global binaries status DB of PID
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : PID_bin_Status;
''')


	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN
END_DATA_BLOCK
(*analogic Status of the PID OBJECTS************************************************)
DATA_BLOCK DB_ana_status_PID
TITLE = 'DB_ana_status_PID'
//
// Global analogic status DB of PID
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : PID_ana_Status;
''')

	self.thePlugin.writeInstanceInfo('''END_STRUCT
BEGIN
END_DATA_BLOCK
(*old analogic Status of the PID OBJECTS************************************************)
DATA_BLOCK DB_ana_status_PID_old
TITLE = 'DB_ana_status_PID_old'
//
// old Global analogic status DB of PID
//
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
''')

	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''    $Name$   : PID_ana_Status;
''')	

	GroupMaxNb = self.thePlugin.getGroupMaxNb()
	self.thePlugin.writeInstanceInfo('''
	END_STRUCT
BEGIN
END_DATA_BLOCK
// INIT OF PARAMETERS DONE in the DB creation
        
        
// DB scheduler to manage the PID sampling times
   
DATA_BLOCK DB_SCHED
  STRUCT
   GLP_NBR : INT; //greatest loop number
   ALP_NBR : INT;  //actual loop number
   LOOP_DAT : ARRAY  [1 .. '''+str(GroupMaxNb)+'''] OF STRUCT    
        MAN_CYC : TIME ;    //loop data: manual sample time
        MAN_DIS : BOOL ;    //loop data: manual loop disable
        MAN_CRST : BOOL ;   //loop data: manual complete restart
        ENABLE : BOOL ; //loop data: enable loop
        COM_RST : BOOL ; //loop data: complete restart
        ILP_COU : INT ; //loop data: internal loop counter
        CYCLE : TIME  ;  //loop data: sample time
       END_STRUCT ; 
  END_STRUCT ;  
BEGIN
   GLP_NBR := '''+str(GroupMaxNb)+'''; 
   ALP_NBR := 0;
''')

	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	RecordNumber = 0
	GroupMaxComponentsNb = self.thePlugin.getGroupComponentsNb()
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		Name = instance.getAttributeData("DeviceIdentification:Name")
		RecordNumber = RecordNumber + 1
		Def_Tsamp = instance.getAttributeData("FEDeviceParameters:Controller Parameters:PID Cycle (s)")
		if Def_Tsamp == "":
			Def_Tsamp = '0'
			
	
	for i in range(1, GroupMaxNb+1):
		group = i
		Def_Tsamp = str(self.thePlugin.getTsamp(group))
		if Def_Tsamp == "":
			Def_Tsamp = '0'
		self.thePlugin.writeInstanceInfo('''     LOOP_DAT['''+str(group)+'''].MAN_CYC :=T#'''+Def_Tsamp+'''s;        // Group: '''+str(group)+'''; Components: '''+str(GroupMaxComponentsNb)+''' PIDs
''')	
	
	self.thePlugin.writeInstanceInfo('''

END_DATA_BLOCK''')

	
	
   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for PID.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for PID.")
		
