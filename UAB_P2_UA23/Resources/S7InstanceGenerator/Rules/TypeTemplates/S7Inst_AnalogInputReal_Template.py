# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
#from research.ch.cern.unicos.types.ai import Instance                 	#REQUIRED
from time import strftime

class AnalogInputReal_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for AIR.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for AIR.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for AIR.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'
	
   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for AIR.")
			
	# General Steps for Optimized objects:
	# 1. Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances) It's called DB_Type
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from DeviceTypeDefinition.xml created by TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5. Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 6. Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#	 We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	# 7. Create another FB to optimize the Object (FB_Type_all2) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 8. Create a instance DB_Type_all2 from FB_Type_all2 where we can write config and parameters for the Front-End
	#	 We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	# This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	Diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	


	# Step 1: Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances)
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()

	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Analog Input Real DB Creation file: UNICOS application
''')
	self.thePlugin.writeInstanceInfo('''
DATA_BLOCK DB_AIR CPC_FB_AIR
BEGIN
END_DATA_BLOCK
''')


	# Step 3: Create all the needed Structures from the TCT	

	# UDT for DB_AIR_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE AIR_ManRequest
TITLE = AIR_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isCommVerification = attribute.getIsCommunicated()
				if isCommVerification:
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE AIR_bin_Status
TITLE = AIR_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE AIR_event
TITLE = AIR_event
//
// parameters of ANADIG Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE AIR_ana_Status
TITLE = AIR_ana_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributeName = attribute.getAttributeName()
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')


	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_AIR_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_AIR_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_AIR_ManRequest
TITLE = DB_AIR_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = instancesVector.size()
	self.thePlugin.writeInstanceInfo('''    AIR_Requests : ARRAY [1..'''+str(instanceNb)+'''] OF AIR_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')
	
	
	# Step 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 
	
	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_AIR Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_AIR
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type AIR
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_AIR : INT := '''+str(NbType)+''';
	AIR_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF AIR_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')

	self.thePlugin.writeInstanceInfo('''
		
(******* Status of the AIRs *************************)
DATA_BLOCK DB_bin_status_AIR
TITLE = 'DB_bin_status_AIR'
//
// Global binary status DB of AIR
//

  // List of variables:
''')		  
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AIR_bin_Status)
''')
		
	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF AIR_bin_Status;
END_STRUCT
BEGIN

END_DATA_BLOCK

(*******old Status of the AIRs *************************)
DATA_BLOCK DB_bin_status_AIR_old
TITLE = 'DB_bin_status_AIR_old'
//
// old Global binary status DB of AI
//
	//  List of variables:
''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AIR_bin_Status)
''')	

		
	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF AIR_bin_Status;
END_STRUCT
BEGIN

END_DATA_BLOCK

(******* analog status of the AIRs *************************)
DATA_BLOCK DB_ana_Status_AIR
TITLE = 'DB_ana_status_AIR'
//
// Global analogic status DB of AIR
//
	//  List of variables:
''')
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AIR_ana_Status)
''')		

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF AIR_ana_Status;
END_STRUCT
BEGIN

END_DATA_BLOCK


(******* old analog status of the AIRs *************************)
DATA_BLOCK DB_ana_Status_AIR_old
TITLE = 'DB_ana_status_AIR_old'
//
// old Global analogic status DB of AI
//
	// List of variables:
''')
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AIR_ana_Status)
''')	

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF AIR_ana_Status;
END_STRUCT
BEGIN

END_DATA_BLOCK
''')


	LimitSize = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "AnalogInputReal"))
	sIsLargeApplication = self.thePlugin.getXMLConfig().getPLCParameter("GeneralConfiguration:LargeApplication", False)
	if sIsLargeApplication == None:
		bIsLargeApplication = False
	else:
		bIsLargeApplication = (sIsLargeApplication.lower() == 'true')

	if (bIsLargeApplication): #UCPC-1670
		self.thePlugin.writeInstanceInfo('''
(* SIMPLIFIED ANALOG INPUT REAL OBJECT DATA STRUCTURE *****************)

TYPE CPC_DB_AIR_S
TITLE ='CPC_DB_AIR_S'
AUTHOR: 'EN/ICE'
VERSION: 6.0

STRUCT     
   PosSt:                   REAL;          
   FoMoSt:                  BOOL;    
   IOErrorW:                BOOL;   
   IOSimuW:                 BOOL;   
    
END_STRUCT     
END_TYPE

(* SIMPLIFIED DB instance AIR ******************************************)
DATA_BLOCK DB_AIR_ALL_S
//
// SIMPLIFIED DB instance AIR
//
    STRUCT
        AIR_SET : STRUCT
''')
		RecordNumber = 0
		for instance in instancesVector :
			# Now looping on each device instance.
			# 1 Building shortcuts
			# 2 Write code to output
			RecordNumber = RecordNumber + 1
			Name = instance.getAttributeData("DeviceIdentification:Name")
			self.thePlugin.writeInstanceInfo('''
            $Name$       : CPC_DB_AIR_S;	// AIR number <'''+str(RecordNumber)+'''>
''')
		self.thePlugin.writeInstanceInfo('''
        END_STRUCT;
    END_STRUCT
BEGIN

END_DATA_BLOCK
''')
    

	# Step 5: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	
	self.thePlugin.writeInstanceInfo('''
(******* AIR EXEC *************************)
FUNCTION_BLOCK FB_AIR_all
TITLE = 'FB_AIR_all'
//
// Call the AIR treatment
//
AUTHOR: 'UNICOS'
NAME: 'CallAIR'
FAMILY: 'AIR'

CONST
	// Constants for the CPC_DB_AIR_S ENS-7060
	SIZE_DB_AIR_S_IN_BYTES:=6; //one FLOAT + one WORD
	OFFSET_FOMOST_IN_BYTES:=4; //one FLOAT
	OFFSET_IOERRORW_IN_BYTES:=4; //one FLOAT
	OFFSET_IOSIMUW_IN_BYTES:=4; //one FLOAT
	BIT_FOMOST:=0;
	BIT_IOERRORW:=1;
	BIT_IOSIMUW:=2;
END_CONST

VAR
  // Static Variables
  AIR_SET: STRUCT
''')

	RecordNumber = 0
	self.thePlugin.writeDebugInUABLog("LimitSize = "+str(LimitSize))
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (RecordNumber <= LimitSize):
			self.thePlugin.writeInstanceInfo('''
   $Name$       : CPC_DB_AIR;	// AIR number <'''+str(RecordNumber)+'''>
''')	
			
	MinNb = RecordNumber
	if (RecordNumber >= LimitSize):
		MinNb = LimitSize
	self.thePlugin.writeInstanceInfo('''

END_STRUCT;
		
  // Different variable view declaration
  AIR AT AIR_SET: ARRAY[1..'''+str(MinNb)+'''] OF CPC_DB_AIR;
  
  // Support variables
  old_status : DWORD;
  I: INT; 
  Signal_Error: BOOL; 
  // PA Status
  Signal_ReadBack_status: BYTE;
  Signal_Pos_D: BYTE;
  Signal_Pos_D_status: BYTE;
  Signal_CheckBack: ARRAY [1..3] OF BYTE;
  
END_VAR  
  FOR I:=1 TO '''+str(MinNb)+''' DO
  
	IF (AIR[I].FEType <> 0) THEN
		IO_ACCESS_AIR(PIWDef := AIR[I].PiwDef,
			  FEType := AIR[I].FEType,
			  InterfaceParam1 := AIR[I].DBNum, 
			  InterfaceParam2 := AIR[I].DBpos, 
			  InterfaceParam3 := AIR[I].DBnumIOerror, 
			  InterfaceParam4 := AIR[I].DBposIOerror,
			  InterfaceParam5 := AIR[I].DBbitIOerror,
			  HFPos := AIR[I].HFPos,
			  IOError := AIR[I].IoError);
	END_IF;
	

	old_status := DB_bin_status_AIR.StsReg01[I].StsReg01;
	old_status := ROR(IN:=old_status, N:=16);
	
	// Call the Baseline function
	CPC_FB_AIR.DB_AIR(
			Manreg01 :=  DB_AIR_ManRequest.AIR_Requests[I].Manreg01    // set by WinCCOA in the DB_AIR
            ,MposR :=  DB_AIR_ManRequest.AIR_Requests[I].MposR          // set by WinCCOA in the DB_AIR            

			,StsReg01 := DB_bin_status_AIR.StsReg01[I].StsReg01 
			,Perst := AIR[I]
	);
''')    
	if (bIsLargeApplication):
		self.thePlugin.writeInstanceInfo('''    
    DB_AIR_All_S.DD [(I-1)*SIZE_DB_AIR_S_IN_BYTES]:= REAL_TO_DWORD (AIR[I].PosSt);
    DB_AIR_All_S.DX [((I-1)*SIZE_DB_AIR_S_IN_BYTES)+OFFSET_FOMOST_IN_BYTES,BIT_FOMOST]:= AIR[I].FoMoSt;
    DB_AIR_All_S.DX [((I-1)*SIZE_DB_AIR_S_IN_BYTES)+OFFSET_IOERRORW_IN_BYTES,BIT_IOERRORW]:= AIR[I].IOErrorW;
    DB_AIR_All_S.DX [((I-1)*SIZE_DB_AIR_S_IN_BYTES)+OFFSET_IOSIMUW_IN_BYTES,BIT_IOSIMUW]:= AIR[I].IOSimuW;  
''')    

	self.thePlugin.writeInstanceInfo('''  
	DB_ana_status_AIR.status[I].PosSt:= AIR[I].PosSt; 
	DB_ana_status_AIR.status[I].HFSt:= AIR[I].HFSt; 

	// Event
	DB_Event_AIR.AIR_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_AIR.StsReg01[I].StsReg01;
	
  END_FOR;
END_FUNCTION_BLOCK
	''')
	


	# Step 6: Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
		
	self.thePlugin.writeInstanceInfo('''
(******* All AIR devices instance *************************)   
DATA_BLOCK DB_AIR_all  FB_AIR_all
//
// Instance DB for the whole AIR devices
//
BEGIN

''')

	RecordNumber = instancesVector.size()
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		if (RecordNumber <= LimitSize):
			Name = instance.getAttributeData("DeviceIdentification:Name")
			RangeMax = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max"))
			RangeMin = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min"))
			RawMax = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Max"))
			RawMin = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Min"))
			Deadband = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Deadband (%)"))
			FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
			if (FEType.strip() == ""):
				FEType = "0"
			DBnum = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
			DBpos = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
			DBnumIOerror = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam3").strip().lower()
			DBposIOerror = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam4").strip().lower()
			DBbitIOerror = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5").strip().lower()
			if DBnum.strip()<>"":
				if DBnum[0] == 'p':
					PIWDef = "TRUE"
				else:
					PIWDef = "FALSE"
			else:
				PIWDef = "FALSE"
			self.thePlugin.writeInstanceInfo(''' 
// AIR number <'''+str(RecordNumber)+'''>
AIR_SET.$Name$.PMaxRan := $RangeMax$; 
AIR_SET.$Name$.PMinRan := $RangeMin$;
AIR_SET.$Name$.PMaxRaw := $RawMax$; 
AIR_SET.$Name$.PMinRaw := $RawMin$;
AIR_SET.$Name$.PDb := $Deadband$;
AIR_SET.$Name$.index := '''+str(RecordNumber)+''';
AIR_SET.$Name$.FEType:=$FEType$;
AIR_SET.$Name$.PIWDef:=$PIWDef$;''')
			#if (FEType == "0"):
			#	self.thePlugin.writeWarningInUABLog("AIR instance: $Name$. Undefined Type, FEType = 0 is taken.")
			if (FEType == "101"):
				if DBnum[0:2] == "db":
					DBnum = DBnum[2:]
				if DBpos[0:2] ==  "db":
					DBpos = DBpos[3:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$;
AIR_SET.$Name$.DBpos:=$DBpos$;''')
			if ((FEType == "102") or (FEType == "103")):
				if DBnum[0:2] == "db":
					DBnum = DBnum[2:]
				if DBpos[0:2] ==  "db":
					DBpos = DBpos[3:]
				if DBnumIOerror[0:2] == "db":
					DBnumIOerror = DBnumIOerror[2:]
				if DBposIOerror[0:2] ==  "db":
					DBposIOerror = DBposIOerror[3:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$;
AIR_SET.$Name$.DBpos:=$DBpos$;
AIR_SET.$Name$.DBnumIOerror:=$DBnumIOerror$;
AIR_SET.$Name$.DBposIOerror:=$DBposIOerror$;
AIR_SET.$Name$.DBbitIOerror:=$DBbitIOerror$;''')
			
			if (FEType == "201"):
				if DBnum[0:2] == "pi":
					DBnum = DBnum[3:]
				else:
					DBnum = DBnum[2:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$; // PA valve address IW$DBnum$''')
			
			if (FEType == "202"):
				if DBnum[0:2] == "pi":
					DBnum = DBnum[3:]
				else:
					DBnum = DBnum[2:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$; // PA address (5 bytes) PID$DBnum$''')
			
			if (FEType == "203"):
				if DBnum[0:2] == "pi":
					DBnum = DBnum[3:]
				else:
					DBnum = DBnum[2:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$; // DP address (5 bytes)''')
			
			if (FEType == "204"):
				if DBnum[0:2] == "pi":
					DBnum = DBnum[3:]
				else:
					DBnum = DBnum[2:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$; // non-standard DP address (4 bytes for value only, no IO error)''')
			
			if (FEType == "205"):
				if DBnum[0:2] == "pi":
					DBnum = DBnum[3:]
				else:
					DBnum = DBnum[2:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$; // Read Profibus DP device with 4 bytes consistency check (4 bytes for value only, IO error evaluation)''')
			

			# add blank line after each AIR
			self.thePlugin.writeInstanceInfo('''
''')
			
			
	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK
''')


	# Step 7: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	
	RecordNumber = instancesVector.size()
	LimitSize = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "AnalogInputReal"))
	if (RecordNumber > LimitSize):
		self.thePlugin.writeInstanceInfo('''
(******* AIR EXEC *************************)
FUNCTION_BLOCK FB_AIR_all2
TITLE = 'CallAIR2'
//
// Call the AIR2 treatment
//
AUTHOR: 'UNICOS'
NAME: 'CallAIR2'
FAMILY: 'AIR2'

CONST
	// Constants for the CPC_DB_AIR_S ENS-7060
	SIZE_DB_AIR_S_IN_BYTES:=6; //one FLOAT + one WORD
	OFFSET_FOMOST_IN_BYTES:=4; //one FLOAT
	OFFSET_IOERRORW_IN_BYTES:=4; //one FLOAT
	OFFSET_IOSIMUW_IN_BYTES:=4; //one FLOAT
	BIT_FOMOST:=0;
	BIT_IOERRORW:=1;
	BIT_IOSIMUW:=2;
END_CONST

VAR
  // Static Variables
  AIR_SET: STRUCT
		''')
		
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		if (RecordNumber > LimitSize):
			Name = instance.getAttributeData("DeviceIdentification:Name")
			self.thePlugin.writeInstanceInfo('''
   $Name$       : CPC_DB_AIR;	// AIR number <'''+str(RecordNumber)+'''>
   ''')
   
	if (RecordNumber > LimitSize):
		self.thePlugin.writeInstanceInfo('''

 END_STRUCT;
		
// Different variable view declaration
AIR AT AIR_SET: ARRAY['''+str(LimitSize+1)+'''..'''+str(RecordNumber)+'''] OF CPC_DB_AIR;

// Support variables
old_status : DWORD;
I: INT;  
Signal_Error: BOOL;   

// PA Status
Signal_ReadBack_status: BYTE;
Signal_Pos_D: BYTE;
Signal_Pos_D_status: BYTE;
Signal_CheckBack: ARRAY [1..3] OF BYTE;

END_VAR  
  FOR I:='''+str(LimitSize+1)+''' TO '''+str(RecordNumber)+''' DO
  
   
	IF (AIR[I].FEType <> 0) THEN
		IO_ACCESS_AIR(PIWDef := AIR[I].PiwDef,
			  FEType := AIR[I].FEType,
			  InterfaceParam1 := AIR[I].DBNum, 
			  InterfaceParam2 := AIR[I].DBpos, 
			  InterfaceParam3 := AIR[I].DBnumIOerror, 
			  InterfaceParam4 := AIR[I].DBposIOerror,
			  InterfaceParam5 := AIR[I].DBbitIOerror,
			  HFPos := AIR[I].HFPos,
			  IOError := AIR[I].IoError);
	END_IF;
	
  
    // Recovery static variables
    old_status := DB_bin_status_AIR.StsReg01[I].StsReg01;
    old_status := ROR(IN:=old_status, N:=16);

''')

	if (RecordNumber > LimitSize):
		self.thePlugin.writeInstanceInfo('''    
	// Call the Baseline function        
    CPC_FB_AIR.DB_AIR(
            Manreg01 :=  DB_AIR_ManRequest.AIR_Requests[I].Manreg01    // set by WinCCOA in the DB_AIR
            ,MposR :=  DB_AIR_ManRequest.AIR_Requests[I].MposR          // set by WinCCOA in the DB_AIR     
            ,StsReg01 := DB_bin_status_AIR.StsReg01[I].StsReg01 
            ,Perst := AIR[I]); 
''')
		if (bIsLargeApplication):
			self.thePlugin.writeInstanceInfo('''           
    DB_AIR_All_S.DD [(I-1)*SIZE_DB_AIR_S_IN_BYTES]:= REAL_TO_DWORD (AIR[I].PosSt);
    DB_AIR_All_S.DX [((I-1)*SIZE_DB_AIR_S_IN_BYTES)+OFFSET_FOMOST_IN_BYTES,BIT_FOMOST]:= AIR[I].FoMoSt;
    DB_AIR_All_S.DX [((I-1)*SIZE_DB_AIR_S_IN_BYTES)+OFFSET_IOERRORW_IN_BYTES,BIT_IOERRORW]:= AIR[I].IOErrorW;
    DB_AIR_All_S.DX [((I-1)*SIZE_DB_AIR_S_IN_BYTES)+OFFSET_IOSIMUW_IN_BYTES,BIT_IOSIMUW]:= AIR[I].IOSimuW;  
''')

		self.thePlugin.writeInstanceInfo('''                       
    DB_ana_status_AIR.status[I].PosSt:= AIR[I].PosSt; 
    DB_ana_status_AIR.status[I].HFSt:= AIR[I].HFSt; 
    
// Event
DB_Event_AIR.AIR_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_AIR.StsReg01[I].StsReg01;

	END_FOR;
END_FUNCTION_BLOCK
''')


	# Step 8: Create a instance DB_Type_all2 from FB_Type_all2 where we can write config and parameters for the Front-End
	#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.

	if (RecordNumber > LimitSize):
		self.thePlugin.writeInstanceInfo('''
(******* All AIR devices instance *************************)   
DATA_BLOCK DB_AIR_all2  FB_AIR_all2
//
// Instance DB for the whole AIR devices
//
BEGIN
''')
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		if (RecordNumber > LimitSize):
			Name = instance.getAttributeData("DeviceIdentification:Name")
			RangeMax = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max"))
			RangeMin = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min"))
			RawMax = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Max"))
			RawMin = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Min"))
			Deadband = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Deadband (%)"))
			FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
			if (FEType.strip() == ""):
				FEType = "0"
			DBnum = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()	
			DBpos = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
			DBnumIOerror = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam3").strip().lower()
			DBposIOerror = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam4").strip().lower()
			DBbitIOerror = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5").strip().lower()
			if DBnum <>"":
				if (DBnum[0])== 'p':
					PIWDef = "TRUE"
				else:
					PIWDef = "FALSE"
			else:
				PIWDef = "FALSE"
			self.thePlugin.writeInstanceInfo(''' 
// AIR number <'''+str(RecordNumber)+'''>
AIR_SET.$Name$.PMaxRan := $RangeMax$; 
AIR_SET.$Name$.PMinRan := $RangeMin$;
AIR_SET.$Name$.PMaxRaw := $RawMax$; 
AIR_SET.$Name$.PMinRaw := $RawMin$;
AIR_SET.$Name$.PDb := $Deadband$;
AIR_SET.$Name$.index := '''+str(RecordNumber)+''';
AIR_SET.$Name$.FEType:=$FEType$;
AIR_SET.$Name$.PIWDef:=$PIWDef$;''')
			#if (FEType == "0"):
			#	self.thePlugin.writeWarningInUABLog("AIR instance: $Name$. Undefined Type, FEType = 0 is taken.")
			if (FEType == "101"):
				if DBnum[0:2] == "db":
					DBnum = DBnum[2:]
				if DBpos[0:2] ==  "db":
					DBpos = DBpos[3:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$;
AIR_SET.$Name$.DBpos:=$DBpos$;''')
			if ((FEType == "102") or (FEType == "103")):
				if DBnum[0:2] == "db":
					DBnum = DBnum[2:]
				if DBpos[0:2] ==  "db":
					DBpos = DBpos[3:]
				if DBnumIOerror[0:2] == "db":
					DBnumIOerror = DBnumIOerror[2:]
				if DBposIOerror[0:2] ==  "db":
					DBposIOerror = DBposIOerror[3:]
					
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$;
AIR_SET.$Name$.DBpos:=$DBpos$;
AIR_SET.$Name$.DBnumIOerror:=$DBnumIOerror$;
AIR_SET.$Name$.DBposIOerror:=$DBposIOerror$;
AIR_SET.$Name$.DBbitIOerror:=$DBbitIOerror$;''')
			
			if (FEType == "201"):
				if DBnum[0:2] == "pi":
					DBnum = DBnum[3:]
				else:
					DBnum = DBnum[2:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$; // PA valve address IW$DBnum$''')
			
			if (FEType == "202"):
				if DBnum[0:2] == "pi":
					DBnum = DBnum[3:]
				else:
					DBnum = DBnum[2:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$; // PA address (5 bytes) PID$DBnum$''')
			
			if (FEType == "203"):
				if DBnum[0:2] == "pi":
					DBnum = DBnum[3:]
				else:
					DBnum = DBnum[2:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$; // DP address (5 bytes)''')
			
			if (FEType == "204"):
				if DBnum[0:2] == "pi":
					DBnum = DBnum[3:]
				else:
					DBnum = DBnum[2:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$; // non-standard DP address (4 bytes for value only, no IO error)''')
			
			if (FEType == "205"):
				if DBnum[0:2] == "pi":
					DBnum = DBnum[3:]
				else:
					DBnum = DBnum[2:]
				self.thePlugin.writeInstanceInfo('''
AIR_SET.$Name$.DBnum:=$DBnum$; // Bronkhorst MFC address''')
			
			# add blank line after each AIR
			self.thePlugin.writeInstanceInfo('''
''')

	if (RecordNumber > LimitSize):
		self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK''')

	#Creation of a FC even for the optimized objects in order to call it in the OB1
	#This avoid the too many symbols problem of the OB1
	#The code is ready for all the object in case we need to split in several DBs one day. Note that for the moment,
	#this has sense only for the AIR and the DI where the split is working.
	NumberOfInstances = instancesVector.size()
	LimitSizeOfInstances = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", theCurrentDeviceType.getDeviceTypeName()))
	nameRepresentation = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", theCurrentDeviceType.getDeviceTypeName())
	self.thePlugin.writeInstanceInfo('''
FUNCTION FC_$nameRepresentation$ : VOID
    FB_$nameRepresentation$_all.DB_$nameRepresentation$_all();''')
	if (NumberOfInstances > LimitSizeOfInstances):
		self.thePlugin.writeInstanceInfo('''
    FB_$nameRepresentation$_all2.DB_$nameRepresentation$_all2();    // Too many '''+str(NumberOfInstances)+''' Need a second FB_$nameRepresentation$_All2''')
	self.thePlugin.writeInstanceInfo('''
END_FUNCTION
''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for AIR.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for AIR.")
		
