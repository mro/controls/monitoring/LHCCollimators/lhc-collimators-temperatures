# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from java.util import Vector
from time import strftime
import S7Logic_CDOL_Standard_Template
import S7Logic_DefaultAlarms_Template
import sys

class CDOL_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0   

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for cdol.")
        reload(S7Logic_CDOL_Standard_Template)
        reload(S7Logic_DefaultAlarms_Template)
        reload(sys)
        sys.setdefaultencoding("utf-8")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for cdol.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for cdol.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'

    def process(self, *params):
        theCurrentPco = params[0]
        theCurrentSection = params[1]
        callUserTemplate  = str(params[2])
        userTemplateName  = str(params[3])
        Name = name = theCurrentPco.getDeviceName()
        self.thePlugin.writeInUABLog("processInstances in Jython for CDOL for PCO $name$.")

        # General Steps for PCO section templates:
        # 1. Create the FUNCTION called DeviceName_DL. Inside this function we must include the next steps:
        #	1.1: Set the Auto Mode Request. For each Dependent Device of the current PCO with RE_RunOSt or FE_RunOSt or RE_CStopOSt or RE_AuDepOSt. 
        #	1.2: Set the Auto Dependent Request for PCO only. For each Dependent Device of the current PCO with RE_RunOSt or FE_RunOSt or RE_CStopOSt or RE_AuDepOSt. 
        #	1.3: Set the Auto Alarm Acknowledge. For ANADIG, ANALOG and ONOFF Objects only. Pass the RE_AlUnAck from the current PCO to the AuAlAck of their dependent devices
        # 	1.4: IOError IOSimu


        theRawInstances = self.thePlugin.getUnicosProject()
        master = Master = theCurrentPco.getMasterDevice().getDeviceName()
        ApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instVector = theRawInstances.findMatchingInstances("ProcessControlObject", "'#DeviceIdentification:Name#'='$name$'")
        if (instVector.size()==0):
            self.thePlugin.writeErrorInUABLog("The instance $name$ does not exist in the specification file.")
            self.thePlugin.writeInfoInUABLog("Reload the specification file by clicking on the Reload Specs. button in the wizard.")
            return
        description = theRawInstances.createSectionText(instVector, 1, 1, '''#DeviceDocumentation:Description#''').strip() 
        for inst in instVector:
            LparamVector = S7Logic_DefaultAlarms_Template.getLparameters(inst)
            self.thePlugin.writeSiemensLogic('''(**** Common Dependant Object Logic: $name$ ($description$)*** Application: $ApplicationName$ *****)
''')

        #call the standard template or the custom one
        if callUserTemplate=="true":
            eval(userTemplateName).CDOLLogic(self.thePlugin, theRawInstances, master, name, LparamVector, theCurrentPco)		
        else:
           S7Logic_CDOL_Standard_Template.CDOLLogic(self.thePlugin, theRawInstances, master, name, LparamVector, theCurrentPco)


        self.thePlugin.writeSiemensLogic('''
END_FUNCTION
''')
	
    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for cdol.")


    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for cdol.")
