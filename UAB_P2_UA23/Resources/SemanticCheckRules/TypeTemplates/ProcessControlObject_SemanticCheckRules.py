# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

class ProcessControlObject_Template(IUnicosTemplate):
   theSemanticVerifier = 0
   thePlugin = 0
   isDataValid = 1
   theCurrentDeviceType = 0
   
   def initialize(self):
   	self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("check rules: initialize")

   def check(self):
	self.thePlugin.writeInUABLog("check rules: check")

   def begin(self):
	self.thePlugin.writeInUABLog("check rules: begin")
	
   def process(self, *params): 
	theCurrentDeviceTypeName = params[0]
	theCurrentDeviceTypeDefinition = params[1]
	theUnicosProject = self.thePlugin.getUnicosProject()
   	theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)	
   	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	self.thePlugin.writeInUABLog(""+str(theCurrentDeviceTypeName)+" Specific Semantic Rules")
	
	# 1. checking the length of the names
   	for instance in instancesVector :
		Name = instance.getAttributeData("DeviceIdentification:Name")
		OptionMode1Label = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 1 Label").strip()
		OptionMode2Label = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 2 Label").strip()
		OptionMode3Label = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 3 Label").strip()
		OptionMode4Label = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 4 Label").strip()
		OptionMode5Label = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 5 Label").strip()
		OptionMode6Label = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 6 Label").strip()
		OptionMode7Label = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 7 Label").strip()
		OptionMode8Label = instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode 8 Label").strip()
		OptionMode1 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 1 Allowance").strip()
		OptionMode2 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 2 Allowance").strip()
		OptionMode3 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 3 Allowance").strip()
		OptionMode4 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 4 Allowance").strip()
		OptionMode5 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 5 Allowance").strip()
		OptionMode6 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 6 Allowance").strip()
		OptionMode7 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 7 Allowance").strip()
		OptionMode8 = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode 8 Allowance").strip()
		
		# Check if the Option Mode Label is defined The Option Mode Allowance is mandatory
		if OptionMode1Label <> "" and OptionMode1 == "":
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The Option Mode Allowance is mandatory if the Option Mode Label is defined")
		
		if OptionMode2Label <> "" and OptionMode2 == "":
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The Option Mode Allowance is mandatory if the Option Mode Label is defined")	
		
		if OptionMode3Label <> "" and OptionMode3 == "":
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The Option Mode Allowance is mandatory if the Option Mode Label is defined")
			
		if OptionMode4Label <> "" and OptionMode4 == "":
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The Option Mode Allowance is mandatory if the Option Mode Label is defined")
			
		if OptionMode5Label <> "" and OptionMode5 == "":
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The Option Mode Allowance is mandatory if the Option Mode Label is defined")
			
		if OptionMode6Label <> "" and OptionMode6 == "":
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The Option Mode Allowance is mandatory if the Option Mode Label is defined")
			
		if OptionMode7Label <> "" and OptionMode7 == "":
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The Option Mode Allowance is mandatory if the Option Mode Label is defined")
			
		if OptionMode8Label <> "" and OptionMode8 == "":
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The Option Mode Allowance is mandatory if the Option Mode Label is defined")
		
		# Name size verification		
		nameSize = len(Name)
		theManufacturer = self.thePlugin.getPlcManufacturer()
		if (theManufacturer.lower() == "siemens") and nameSize > 19:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 19")
			
		elif (theManufacturer.lower() == "schneider") and nameSize > 23:
			self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. Max number of letters exceeded in the device type Name: current length = "+str(nameSize)+". Max length allowed = 23")
		
		# Option mode verification
		if OptionMode1Label <> "":
			OptionMode1String = self.thePlugin.isString(OptionMode1)
			OptionMode1Size = len(OptionMode1)
			if (OptionMode1String or OptionMode1Size > 8) and OptionMode1 <> "":
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The OptionMode1 is not correct")
		
		if OptionMode2Label <> "":		
			OptionMode2String = self.thePlugin.isString(OptionMode2)
			OptionMode2Size = len(OptionMode2)
			if (OptionMode2String or OptionMode2Size > 8) and OptionMode2 <> "":
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The OptionMode2 is not correct")
				
		if OptionMode3Label <> "":				
			OptionMode3String = self.thePlugin.isString(OptionMode3)
			OptionMode3Size = len(OptionMode3)
			if (OptionMode3String or OptionMode3Size > 8) and OptionMode3 <> "":
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The OptionMode3 is not correct")
			
		if OptionMode4Label <> "":			
			OptionMode4String = self.thePlugin.isString(OptionMode4)
			OptionMode4Size = len(OptionMode4)
			if (OptionMode4String or OptionMode4Size > 8) and OptionMode4 <> "":
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The OptionMode4 is not correct")
		
		if OptionMode5Label <> "":			
			OptionMode5String = self.thePlugin.isString(OptionMode5)
			OptionMode5Size = len(OptionMode5)
			if (OptionMode5String or OptionMode5Size > 8) and OptionMode5 <> "":
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The OptionMode5 is not correct")
				
		if OptionMode6Label <> "":					
			OptionMode6String = self.thePlugin.isString(OptionMode6)
			OptionMode6Size = len(OptionMode6)
			if (OptionMode6String or OptionMode6Size > 8) and OptionMode6 <> "":
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The OptionMode6 is not correct")
				
		if OptionMode7Label <> "":					
			OptionMode7String = self.thePlugin.isString(OptionMode7)
			OptionMode7Size = len(OptionMode7)
			if (OptionMode7String or OptionMode7Size > 8) and OptionMode7 <> "":
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The OptionMode7 is not correct")
				
		if OptionMode8Label <> "":					
			OptionMode8String = self.thePlugin.isString(OptionMode8)
			OptionMode8Size = len(OptionMode8)
			if (OptionMode8String or OptionMode8Size > 8) and OptionMode8 <> "":
				self.thePlugin.writeErrorInUABLog(""+str(theCurrentDeviceTypeName)+" instance: $Name$. The OptionMode8 is not correct")
		
			
   def end(self):
	self.thePlugin.writeInUABLog("check rules: end")

   def shutdown(self):
	self.thePlugin.writeInUABLog("check rules: shutdown")
		
