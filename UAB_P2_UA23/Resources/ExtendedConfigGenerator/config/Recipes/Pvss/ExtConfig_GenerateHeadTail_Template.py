# Jython source file to create DIP Configs and DIP publications
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from research.ch.cern.unicos.utilities import AbsolutePathBuilder	#REQUIRED
from java.util import Vector
from datetime import datetime
from time import strftime
from java.lang import System
import getpass
import os
import platform
import sys


class GenerateHeadTail_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   

   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
   	self.theUnicosProject = self.thePlugin.getUnicosProject()
	self.thePlugin.writeInUABLog("Initialize in Jython for GenerateHeadTail.")


   def check(self):
	self.thePlugin.writeInUABLog("Check in Jython for GenerateHeadTail.")


   def begin(self):
	self.thePlugin.writeInUABLog("Begin in Jython for GenerateHeadTail.")
	self.dateAndTime = strftime("%Y-%m-%d %H:%M:%S")
	self.theUserName = System.getProperty("user.name")
	#self.theTargetLocation = self.thePlugin.getDBTargetLocation()
   	#Plugin Information
   	self.thePluginId = self.thePlugin.getId()
   	self.thePluginVersion = self.thePlugin.getVersionId()
   	CRLF = System.getProperty("line.separator")
	
	
	#projectName 	= self.thePlugin.getConfigParameter("name")
	#applicationName = self.thePlugin.getApplicationParameter("GeneralData:ApplicationName")
	#pluginId 	= self.thePlugin.getId()
	#pluginVersion	= self.thePlugin.getVersionId()
	#userName	= getpass.getuser()
	#computerName	= os.getenv('COMPUTERNAME')
	#currentDate 	= datetime.now()
	#generationDate	= str(currentDate.day) + "/" + str(currentDate.month) + "/" + str(currentDate.year) +" " + str(currentDate.hour) + ":" + str(currentDate.minute) + ":" + str(currentDate.second)

	headFilePath = self.thePlugin.getPluginParameter("Services:Recipes:OutputFormats:Pvss:HeadFile")
	headFilePath = AbsolutePathBuilder.getAbsolutePath(headFilePath)
	
	#tailFilePath = self.thePlugin.getPluginParameter("Services:Dip:OutputFormats:HTMLPublications:TailFile")
	#tailFilePath = AbsolutePathBuilder.getAbsolutePath(tailFilePath)
	

	headFileContent = '''#$CRLF$# $self.thePluginId$ Version: $self.thePluginVersion$ - $self.theUserName$-$self.dateAndTime$$CRLF$'''
	headFileContent +='''#$CRLF$# Device type: SOFT_FE $CRLF$#$CRLF$# Delete;SOFT_FE;$CRLF$#$CRLF$'''

	file = open(headFilePath, 'w')
	file.write(str(headFileContent))
	file.close()
	
		
   def process(self, theXMLConfig):		
	self.thePlugin.writeInUABLog("ProcessInstances in Jython for GenerateHeadTail.")
		

   def end(self):
	self.thePlugin.writeInUABLog("End in Jython for GenerateHeadTail.")


   def shutdown(self):
	self.thePlugin.writeInUABLog("Shutdown in Jython for GenerateHeadTail.")
		
