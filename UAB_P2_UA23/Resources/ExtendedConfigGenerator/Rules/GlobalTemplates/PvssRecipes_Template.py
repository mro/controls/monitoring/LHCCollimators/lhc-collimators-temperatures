# Jython source file to create Recipes file to import in PVSS
from java.lang import System
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from time import strftime


class PvssRecipes_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0
    recipeService = 0
    # Maximum number of recipe values
    maxRecipeValues = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize in Jython for SelectInstances.")
        self.recipeService = self.thePlugin.getServiceInstance("Recipes")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for SelectInstances.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for SelectInstances.")

    def process(self, *params):
        self.thePlugin.writeInUABLog("processInstances in Jython for SelectInstances.")

        # Get the input parameters
        rcpClasses = params[0]
        rcpInstances = params[1]

        self.writeHeader()
        self.processRecipeClasses(rcpClasses)
        self.recipeService.write("")
        self.processRecipeInstances(rcpInstances)

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for SelectInstances.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for SelectInstances.")
        self.thePlugin.writeInfoInUABLog("The maximum number of recipe values is: " + str(self.maxRecipeValues))

    def processRecipeClasses(self, rcpClasses):

        self.writeClassConfigLine()

        deviceNumber = 1
        for rcpClass in rcpClasses:
            if (False == rcpClass.isDefinitionValid()):
                continue

            className = self.getString(rcpClass.getClassName())
            typeName = self.getString(rcpClass.getTypeName())
            description = self.getString(rcpClass.getClassDesc())
            deviceLink = self.getString(rcpClass.getDeviceLink())
            deviceList = self.getString(rcpClass.getAllDeviceDpeValueString())

            domain = ""
            if (rcpClass.hasClassDomainDefinition()):
                domain = self.getString(rcpClass.getClassACDomainString()) + "|" + self.getString(
                    rcpClass.getClassDomainString())

            nature = ""
            if (rcpClass.hasClassNatureDefinition() or rcpClass.hasClassActionDefinition()):
                nature = self.getString(rcpClass.getCNature())
                operatorAction = self.getString(rcpClass.getOperatorClassActionString())
                expertAction = self.getString(rcpClass.getExpertClassActionString())
                adminAction = self.getString(rcpClass.getAdminClassActionString())
                nature = operatorAction + "|" + expertAction + "|" + adminAction + "|" + nature

            self.recipeService.write("UnRcpClass;" + str(
                deviceNumber) + ";$className$,$deviceLink$;$description$;;;;$domain$;$nature$;UnRcpClassState;$typeName$;$deviceList$;")
            # Update the maximum number of recipe values
            if rcpClass.getNumValues() > self.maxRecipeValues:
                self.maxRecipeValues = rcpClass.getNumValues()
            deviceNumber += 1

    def processRecipeInstances(self, rcpInstances):

        self.writeInstanceConfigLine()
        deviceNumber = 1
        for rcpInstance in rcpInstances:
            if (False == rcpInstance.isDefinitionValid()):
                continue

            rcpClass = self.getString(rcpInstance.getRecipeClass())
            className = self.getString(rcpInstance.getClassName())
            instanceName = self.getString(rcpInstance.getInstanceName())
            description = self.getString(rcpInstance.getDescription())
            initialRecipe = self.getString(str(rcpInstance.isInitialRecipe()).lower())
            values = self.getString(rcpInstance.getRecipeDpeValueString())

            domain = ""
            operatorAction = ""
            expertAction = ""
            adminAction = ""
            nature = ""

            if (rcpClass.hasInstanceDomainDefinition()):
                domain = self.getString(rcpClass.getInstanceACDomainString()) + "|" + self.getString(
                    rcpClass.getInstanceDomainString())

            if (rcpClass.hasInstanceNatureDefinition() or rcpClass.hasInstanceActionDefinition()):
                nature = self.getString(rcpClass.getINature())
                operatorAction = self.getString(rcpClass.getOperatorInstanceActionString())
                expertAction = self.getString(rcpClass.getExpertInstanceActionString())
                adminAction = self.getString(rcpClass.getAdminInstanceActionString())

            self.recipeService.write("UnRcpInstance;" + str(
                deviceNumber) + ";$className$/$instanceName$;$description$;;;;$domain$;$operatorAction$|$expertAction$|$adminAction$|$nature$;UnRcpInstanceState;$className$;$initialRecipe$;$values$;")
            deviceNumber += 1

    def writeHeader(self):
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")
        applicationName = self.thePlugin.getApplicationParameter("GeneralData:ApplicationName")
        userName = System.getProperty("user.name")

        self.recipeService.write("#")
        self.recipeService.write(
            "# " + self.thePlugin.getId() + " Version: " + self.thePlugin.getVersionId() + " - $userName$ - $dateAndTime$")
        self.recipeService.write("#")
        self.recipeService.write("# Front-End: SOFT_FE")
        self.recipeService.write("#")
        self.recipeService.write("# WARNING: Please don't modify the recipe SOFT_FE name, it should be always 'RCP_FE'")
        self.recipeService.write("#")
        self.recipeService.write("# Delete;RCP_FE;$applicationName$")
        self.recipeService.write("#")
        self.recipeService.write("")
        self.recipeService.write("PLCCONFIG;SOFT_FE;RCP_FE;$applicationName$;N;")
        self.recipeService.write("")

    def writeClassConfigLine(self):
        self.recipeService.write("#")
        self.recipeService.write("# Object: UnRcpClass")
        self.recipeService.write("#")
        self.recipeService.write(
            "# Config Line : DeviceTypeName;deviceNumber;Alias[,DeviceLinkList];Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;RecipeType;DeviceList;")
        self.recipeService.write("#")

    def writeInstanceConfigLine(self):
        self.recipeService.write("#")
        self.recipeService.write("# Object: UnRcpInstance")
        self.recipeService.write("#")
        self.recipeService.write(
            "# Config Line : DeviceTypeName;deviceNumber;Alias;Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;RecipeClass;InitialRecipe;RecipeValues;")
        self.recipeService.write("#")

    def getString(self, str):
        if str is None:
            return ""
        else:
            return str
