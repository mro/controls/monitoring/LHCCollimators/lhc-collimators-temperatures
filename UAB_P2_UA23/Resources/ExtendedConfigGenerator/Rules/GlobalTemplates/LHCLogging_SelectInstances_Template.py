# Jython source file to create LHCLogging file
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED


class SelectInstances_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.loggingService = self.thePlugin.getServiceInstance("LHCLogging")
        self.thePlugin.writeInUABLog("initialize in Jython for SelectInstances.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for SelectInstances.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for SelectInstances.")

    def process(self):
        self.thePlugin.writeInUABLog("processInstances in Jython for SelectInstances.")

        # Selects the instances from the SPECS file using the findMatchingInstances methods.
        di = self.theUnicosProject.findMatchingInstances("DigitalInput",
                                                         "'#DeviceDocumentation:Description#' not contains 'SPARE'")
        do = self.theUnicosProject.findMatchingInstances("DigitalOutput",
                                                         "'#DeviceDocumentation:Description#' not contains 'SPARE'")
        ai = self.theUnicosProject.findMatchingInstances("AnalogInput",
                                                         "'#DeviceDocumentation:Description#' not contains 'SPARE'")
        # diSpare = self.theUnicosProject.findMatchingInstances("DigitalInput", "'#DeviceIdentification:Description#' contains 'SPARE'")
        # doSpare = self.theUnicosProject.findMatchingInstances("DigitalOutput", "'#DeviceIdentification:Description#' contains 'SPARE'")
        # aiSpare = self.theUnicosProject.findMatchingInstances("AnalogInput", "'#DeviceIdentification:Description#' contains 'SPARE'")


        # Adds the selected instances to the LHCLogging data
        self.loggingService.addLHCLoggingData("PosSt", "pvss", di, "CRYO", "dist", "QSDN2_v3", "DI", "QSDN2")
        self.loggingService.addLHCLoggingData("StsReg01", "pvss", di, "CRYO", "dist", "QSDN2_v3", "DI", "QSDN2")
        self.loggingService.addLHCLoggingData("PosSt", "pvss", do, "CRYO", "dist", "QSDN2_v3", "DO", "QSDN2")
        self.loggingService.addLHCLoggingData("StsReg01", "pvss", do, "CRYO", "dist", "QSDN2_v3", "DO", "QSDN2")
        self.loggingService.addLHCLoggingData("PosSt", "pvss", ai, "CRYO", "dist", "QSDN2_v3", "AI", "QSDN2")
        self.loggingService.addLHCLoggingData("StsReg01", "pvss", ai, "CRYO", "dist", "QSDN2_v3", "AI", "QSDN2")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for SelectInstances.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for SelectInstances.")
