# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved

from research.ch.cern.unicos.utilities.constants import FileOutputFormat

import ucpc_library.shared_generic_functions
reload(ucpc_library.shared_generic_functions)

class GenericFunctions(ucpc_library.shared_generic_functions.SharedGenericFunctions):
    output = 0

    def isSpareInstance(self, instance):
        """ Return true for spare instance. """
        description = instance.getAttributeData("DeviceDocumentation:Description")
        return description.strip().lower() == "spare"

    def getFormatPrecision(self, format):
        format = format.split(".")
        if len(format) != 2:
            return 0
        else:
            precision = format[1].count("#")
            return precision if precision < 7 else 7

    def computeAddress(self, instance, pinName):
        """ Function calculates an address name based on instance name and pin name and calls thePlugin.computeAddress """
        fullName = instance.getAttributeData("DeviceIdentification:Name") + "_" + pinName
        return str(self.thePlugin.computeAddress(fullName))

    def getCommGroup(self):
        if self.thePlugin.getPlcManufacturer().lower().strip() == "schneider":
            return "EquipementModbus01"
        else :
            return "S7300400Series01"

    def writeVariable(self, instance, name, dataType, props = {}):
        if name == "Name":
            self.writeRawVariable(name, dataType, {"Source" : "Internal", "Sharing" : "Read/Write",
                "InitialValue" : instance.getAttributeData("DeviceIdentification:Name"), "NoOfBytes" : "40"})
        elif name == "VijReg01":
            format = self.getFormat(instance)
            VijReg01 = self.getFormatPrecision(format)
            if instance.getDeviceType().doesSpecificationAttributeExist("FEDeviceParameters:ParReg:Manual Restart after Full Stop"):
                enRstart = instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip()
                if enRstart.lower() == "false":
                    VijReg01 += 1024
            self.writeRawVariable(name, dataType, {"Source" : "Internal", "Sharing" : "Read/Write", 'InitialValue' : VijReg01})
        elif name == "Description":
            description = instance.getAttributeData("DeviceDocumentation:Description").replace("&","&amp;").replace("<","&lt;").replace(">","&gt;")
            self.writeRawVariable(name, dataType, {"Source" : "Internal", "Sharing" : "Read/Write", 'InitialValue' : description, 'NoOfBytes' : '100'})
        elif name == "DisplayName":
            displayName = instance.getAttributeData("SCADADeviceGraphics:Display Name")
            if displayName == "":
                displayName=="PCO"
            self.writeRawVariable(name, dataType, {"Source" : "Internal", "Sharing" : "Read/Write", 'InitialValue' : displayName, 'NoOfBytes' : '20'})
        elif name == "Unit":
            self.writeRawVariable(name, dataType, {"Source" : "Internal", "Sharing" : "Read/Write",
                'InitialValue' : self.getUnit(instance), 'NoOfBytes' : '10'})
        else:
            address = props['DeviceAddress'] if 'DeviceAddress' in props else self.computeAddress(instance, name)
            self.writeRawVariable(name, dataType, {"Source" : "External", "Sharing" : "Read/Write", 
                "ScanGroup" : self.getCommGroup(), "DeviceAddress" : address})

    def writeRawVariable(self, name, type, props):
        self.writeInstanceInfo(2, "<Variable name=\"$name$\" type=\"$type$\">")
        for key in props:
            value = props[key]
            self.writeInstanceInfo(2, "  <$key$>$value$</$key$>")
        self.writeInstanceInfo(2, "</Variable>")

    def writeInstanceInfo(self, buffer, data):
        if self.output == 0:
            self.output = self.thePlugin.getPluginParameter("OutputParameters:Magelis:OutputMappedVariablesFileName")
            self.thePlugin.setOutputFormat(self.output, FileOutputFormat.XML, "UTF-8")
        self.thePlugin.writeInstanceInfo(self.output, buffer, data)
