# -*- coding: utf-8 -*-
from research.ch.cern.unicos.utilities.constants import FileOutputFormat

import ucpc_library.shared_generic_functions
reload(ucpc_library.shared_generic_functions)

NO_VALUE = "<No Value>"

class GenericFunctions(ucpc_library.shared_generic_functions.SharedGenericFunctions):
    def writeWarning(self, instance, text):
        if instance == None:
            self.thePlugin.writeWarningInUABLog(text)
        else:
            alias = instance.getAttributeData("DeviceIdentification:Name")
            self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $alias$. " + text)

    def writeError(self, instance, text):
        if instance == None:
            self.thePlugin.writeErrorInUABLog(text)
        else:
            alias = instance.getAttributeData("DeviceIdentification:Name")
            self.thePlugin.writeErrorInUABLog("$self.theDeviceType$ instance: $alias$. " + text)

    def isSpareInstance(self, instance):
        """ Return true for spare instance. """
        description = instance.getAttributeData("DeviceDocumentation:Description")
        return description.strip().lower() == "spare"

    def computeAddress(self, instance, pinName):
        """ Function calculates an address name based on instance name and pin name and calls thePlugin.computeAddress """
        fullName = instance.getAttributeData("DeviceIdentification:Name") + "_" + pinName
        return str(self.thePlugin.computeAddress(fullName))

    def writeScriptTag(self, instance, tag, dataType, props = {}):
        """ Calculate tag properties and write tag to the output file.
        If tag starts with capital than tag's address will be calculated.
        Any property could be overwritten using props attribute"""
        if tag != "" and tag[0].isupper():
            address = self.computeAddress(instance, tag)
        else:
            address = NO_VALUE
        defaultValues = {
            'address' : address,
            'dataType' : dataType,
            'plcTag' : NO_VALUE,
            'indirectAddressing' : 'False',
            'indexTag' : NO_VALUE,
            'startValue' : NO_VALUE,
            'idTag' : '0',
            'displayName' : NO_VALUE,
            'comment' : NO_VALUE,
            'acquisitionMode' : 'Cyclic in operation',
            'acquisitionCycle' : '1 s',
            'rangeMaximumType' : 'None',
            'rangeMaximum' : NO_VALUE,
            'rangeMinimumType' : 'None',
            'rangeMinimum' : NO_VALUE,
            'linearScaling' : 'False',
            'endValuePLC' : '10',
            'startValuePLC' : '0',
            'endValueHMI' : '100',
            'startValueHMI' : '0',
            'gmpRelevant' : 'False',
            'confirmationType' : 'None',
            'mandatoryCommenting' : 'False'
        }
        values = dict(defaultValues.items() + props.items())

        if 'name' not in values:
            if instance.getAttributeData("DeviceIdentification:Expert Name"):
                nameAlias = instance.getAttributeData("DeviceIdentification:Expert Name")
            else:
                nameAlias = instance.getAttributeData("DeviceIdentification:Name")
            values['name'] = nameAlias + "." + tag
        if 'path' not in values:
            values['path'] = instance.getDeviceTypeName()
        if 'connection' not in values:
            values['connection'] = NO_VALUE if values['address'] == NO_VALUE else self.getPLCName()
        if 'accessMethod' not in values:
            values['accessMethod'] = NO_VALUE if values['address'] == NO_VALUE else 'Absolute access'
        if 'length' not in values:
            values['length'] = self.getDataLength(instance, tag, values['dataType'])
        if 'coding' not in values:
            values['coding'] = 'IEEE754' if values['dataType'] == 'Real' or values['dataType'] == 'Float' else 'Binary'

        self.printScriptTag(values)

    def printScriptTag(self, values):
        """ Write down a tag following format. """
        format = ['name', 'path', 'connection', 'plcTag', 'dataType', 'length', 'address', 'accessMethod', 'indirectAddressing', 'indexTag', 'startValue', 'idTag',
                  'coding', 'displayName', 'comment', 'acquisitionMode', 'acquisitionCycle', 'rangeMaximumType', 'rangeMaximum', 'rangeMinimumType',
                  'rangeMinimum', 'linearScaling', 'endValuePLC', 'startValuePLC', 'endValueHMI', 'startValueHMI', 'gmpRelevant', 'confirmationType', 'mandatoryCommenting']
        output = []
        for key in format:
            if key in values:
                output.append(values[key])
            else:
                self.writeWarning(None, "No value defined for key: " + key)
                output.addend(NO_VALUE)

        self.writeTagInstance("Hmi Tags", ",".join(output))

    def getDataLength(self, instance, name, dataType):
        """ Calculate data length for given data type. """
        fixedTypes = {'Bool' : '1', 'UInt' : '2', 'Word' : '2', 'Int' : '2', 'Float' : '4', 'Real' : '4'}
        stringTypes = {'userName' : '10', 'unit' : '10', 'objName' : '40', 'description' : '100', 'displayName': '8'}
        if dataType in fixedTypes:
            return fixedTypes[dataType]
        elif dataType == 'WString' and name in stringTypes:
            return stringTypes[name]
        else:
            self.writeError(instance, "The data type is not supported: " + dataType)
            return ""

    def writeSmartTagName(self, instance):
        """ Write down smart tag for name. """
        if instance.getAttributeData("DeviceIdentification:Expert Name"):
            name = instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            name = instance.getAttributeData("DeviceIdentification:Name")
        self.writeScriptInstance("SmartTags(\"$name$.objName\") = \"$name$\"")
        
    def writeSmartTagDisplayName(self, instance):
        """ Write down smart tag for display name. """
        if instance.getAttributeData("DeviceIdentification:Expert Name"):
            name = instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            name = instance.getAttributeData("DeviceIdentification:Name")
        if instance.getAttributeData("SCADADeviceGraphics:Display Name"):
            displayName = instance.getAttributeData("SCADADeviceGraphics:Display Name")
        else:
            displayName = instance.getAttributeData("DeviceIdentification:Name")
        self.writeScriptInstance("SmartTags(\"$name$.displayName\") = \"$displayName$\"")

    def writeSmartTagUnit(self, instance):
        """ Write down smart tag for unit. """
        if instance.getAttributeData("DeviceIdentification:Expert Name"):
            name = instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            name = instance.getAttributeData("DeviceIdentification:Name")
        unit = instance.getAttributeData("SCADADeviceGraphics:Unit")
        unit = unit.replace(" ","").replace("\"","\"\"")
        self.writeScriptInstance("SmartTags(\"$name$.unit\") = \"$unit$\"")

    def writeSmartTagDescription(self, instance):
        """ Write down smart tag for description. """
        if instance.getAttributeData("DeviceIdentification:Expert Name"):
            name = instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            name = instance.getAttributeData("DeviceIdentification:Name")
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("\"","\"\"")
        self.writeScriptInstance("SmartTags(\"$name$.description\") = \"$description$\"")

    def writeAlarmInfo(self, instance):
        """ Retrieve and write down alarm info. """
        if instance.getAttributeData("DeviceIdentification:Expert Name"):
            name = instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            name = instance.getAttributeData("DeviceIdentification:Name")
        description = instance.getAttributeData("DeviceDocumentation:Description").replace(",","\,")
        deviceNumber = int(instance.getInstanceNumber())
        if instance.getDeviceTypeName() == "AnalogAlarm":
            deviceNumber += int(self.thePlugin.getUnicosProject().getDeviceType("DigitalAlarm").getAllDeviceTypeInstances().size())
        deviceNumber = str(deviceNumber)
        self.writeAlarmInstance("DiscreteAlarms", "$deviceNumber$,Discrete_alarm_$deviceNumber$,$description$,,Errors,\"$name$.StsReg01\",0,<No value>,0,<No value>,0,<No value>,,<No value>")
    
    def writeTextListPattern(self, instance):
        """ Retrieve and write down WS and WPAR pattern. """
        if instance.getAttributeData("DeviceIdentification:Expert Name"):
            name = instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            name = instance.getAttributeData("DeviceIdentification:Name")
        pattern = instance.getAttributeData("SCADADeviceGraphics:Pattern")
        widgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
        if "Bit" in widgetType:
            self.writeWarning(instance, "skip text list generation for WSBit type.")
            return
        if pattern in ["", "-"]:
            return
        self.writeTextlistInstance("TextList", "$name$,Decimal,<No value>")
        idx = 1
        for pair in pattern.split(","):
            key, value = pair.split("=")
            self.writeTextlistInstance("TextListEntry", "Text_list_entry_"+str(idx)+",$name$,,"+key+","+value+",")
            idx+=1

    tag_filename = 0
    def writeTagInstance(self, section, data):
        if self.tag_filename == 0:
            self.tag_filename = self.thePlugin.getPluginParameter("OutputParameters:TIAPortal:TagsFileName")
            self.thePlugin.setOutputFormat(self.tag_filename, FileOutputFormat.XLSX)
            self.thePlugin.setExcelCustomProperty(self.tag_filename, "TIA_Version", "1.1")
        self.thePlugin.writeInstanceInfo(self.tag_filename, section, data)

    script_filename = 0
    def writeScriptInstance(self, data):
        if self.script_filename == 0:
            self.script_filename = self.thePlugin.getPluginParameter("OutputParameters:TIAPortal:ScriptFileName")
        self.thePlugin.writeInstanceInfo(self.script_filename, data)

    alarm_filename = 0
    def writeAlarmInstance(self, section, data):
        if self.alarm_filename == 0:
            self.alarm_filename = self.thePlugin.getPluginParameter("OutputParameters:TIAPortal:AlarmFileName")
            self.thePlugin.setOutputFormat(self.alarm_filename, FileOutputFormat.XLSX)
            self.thePlugin.setExcelCustomProperty(self.alarm_filename, "TIA_Version", "1.1")
        self.thePlugin.writeInstanceInfo(self.alarm_filename, section, data)

    textlist_filename = 0
    def writeTextlistInstance(self, section, data):
        if self.textlist_filename == 0:
            self.textlist_filename = self.thePlugin.getPluginParameter("OutputParameters:TIAPortal:TextListsFileName")
            self.thePlugin.setOutputFormat(self.textlist_filename, FileOutputFormat.XLSX)
            self.thePlugin.setExcelCustomProperty(self.textlist_filename, "TIA_Version", "1.1")
        self.thePlugin.writeInstanceInfo(self.textlist_filename, section, data)

    security_settings_filename = 0
    def writeSecuritySettingsInstance(self, data):
        if self.security_settings_filename == 0:
            self.security_settings_filename = self.thePlugin.getPluginParameter("OutputParameters:TIAPortal:SecuritySettingsScriptFileName")
        self.thePlugin.writeInstanceInfo(self.security_settings_filename, data)

