# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from time import strftime

class DigitalParameter_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for DPAR.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for DPAR.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for DPAR.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'

   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for DPAR.")
	
	# General Steps for Optimized objects:
	# 1. Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances) It's called DB_Type
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from DeviceTypeDefinition.xml created by TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5. Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 6. Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#	 We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	# This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	
	# Step 1: Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances)

	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Digital Parameter DB Creation file: UNICOS application
''')
	self.thePlugin.writeInstanceInfo('''
DATA_BLOCK DB_DPAR CPC_FB_DPAR
BEGIN
END_DATA_BLOCK
''')


	
	# Step 3: Create all the needed Structures from the TCT	
	
	# UDT for DB_DPAR_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE DPAR_ManRequest
TITLE = DPAR_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isCommVerification = attribute.getIsCommunicated()
				if isCommVerification:
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')
	
	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE DPAR_bin_Status
TITLE = DPAR_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE DPAR_event
TITLE = DPAR_event
//
// parameters of ANADIG Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				# isEventAttribute = attribute.getIsEventAttribute()
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')
	

	# Step 2: Create all the needed Structures from the TCT	
		
	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_DPAR Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_DPAR
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type DPAR
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_DPAR : INT := '''+str(NbType)+''';
	DPAR_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF DPAR_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')
	
	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_DPAR_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_DPAR_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_DPAR_ManRequest
TITLE = DB_DPAR_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    DPAR_Requests : ARRAY [1..'''+instanceNb+'''] OF DPAR_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')

	self.thePlugin.writeInstanceInfo('''
(************************* binary Status of the DPARs ************************)
DATA_BLOCK DB_bin_status_DPAR
TITLE = 'DB_bin_status_DPAR'
//
// Global binary status DB of DPAR
//
// List of variables:''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (DPAR_bin_Status)
''')

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF DPAR_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(********************* old binary Status of the DPARs ****************************)
DATA_BLOCK DB_bin_status_DPAR_old
TITLE = 'DB_bin_status_DPAR_old'
//
// Old Global binary status DB of DPAR
//
''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (DPAR_bin_Status)
''')
		
	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF DPAR_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
''')


	# Step 5: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	 
	self.thePlugin.writeInstanceInfo('''
  
(*DPAR execution ********************************************)
FUNCTION_BLOCK FB_DPAR_all
TITLE = 'FB_DPAR_all'
//
// Call the DPAR treatment
//
AUTHOR: 'UNICOS'
NAME: 'CallDPAR'
FAMILY: 'DPAR'
VAR
  // Static Variables
  DPAR_SET: STRUCT

''')


	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
   $Name$       : CPC_DB_DPAR;
''')		

		
	self.thePlugin.writeInstanceInfo('''
    END_STRUCT;  
  
  // Different variable view declaration
  DPAR AT DPAR_SET: ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_DB_DPAR;
  
  // Support variables
  old_status : DWORD;
  I: INT;
END_VAR
  FOR I:=1 TO '''+str(RecordNumber)+''' DO
  
    // Recovery static variables
    old_status := DB_bin_status_DPAR.StsReg01[I].StsReg01;
    old_status := ROR(IN:=old_status, N:=16);
     
	// Calls the Baseline function
    CPC_FB_DPAR.DB_DPAR(    
        ManReg01 :=  DB_DPAR_ManRequest.DPAR_Requests[I].ManReg01   // set by WinCCOA in the DB_DPAR_ManRequest   
		,StsReg01 := DB_bin_status_DPAR.StsReg01[I].StsReg01
		,Perst:= DPAR[I]
    );
    
    // Event
    DB_Event_DPAR.DPAR_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_DPAR.StsReg01[I].StsReg01;
    
  END_FOR;
END_FUNCTION_BLOCK
     ''')

	 
	# Step 6: Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
	
	self.thePlugin.writeInstanceInfo('''
(* All DPAR devices instance DB  **************************************)     
DATA_BLOCK DB_DPAR_all  FB_DPAR_all
//
// Instance DB for the whole DPAR devices initialization
//
BEGIN

''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		# Latch = instance.getAttributeData("FEDeviceAutoRequests:Latch")
		DefaultValue = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Default Value")).lower()
		if DefaultValue == "false":
			DefaultValue = "W#16#0"
		else:
			DefaultValue = "W#16#1"
		self.thePlugin.writeInstanceInfo('''
DPAR_SET.$Name$.PosSt := $DefaultValue$;		
''')	
		
	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK
''')

	#Creation of a FC even for the optimized objects in order to call it in the OB1
	#This avoid the too many symbols problem of the OB1
	#The code is ready for all the object in case we need to split in several DBs one day. Note that for the moment,
	#this has sense only for the AIR and the DI where the split is working.
	NumberOfInstances = instancesVector.size()
	LimitSizeOfInstances = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", theCurrentDeviceType.getDeviceTypeName()))
	nameRepresentation = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", theCurrentDeviceType.getDeviceTypeName())
	self.thePlugin.writeInstanceInfo('''
FUNCTION FC_$nameRepresentation$ : VOID
    FB_$nameRepresentation$_all.DB_$nameRepresentation$_all();''')
	if (NumberOfInstances > LimitSizeOfInstances):
		self.thePlugin.writeInstanceInfo('''
    FB_$nameRepresentation$_all2.DB_$nameRepresentation$_all2();    // Too many '''+str(NumberOfInstances)+''' Need a second FB_$nameRepresentation$_All2''')
	self.thePlugin.writeInstanceInfo('''
END_FUNCTION
''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for DPAR.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for DPAR.")
		
