# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from java.util import Vector
from time import strftime
import S7Logic_Controller_DL_Standard_Template
import S7Logic_DefaultAlarms_Template
import sys

class Controller_DL_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0   

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for CONTROLLER.")
        reload(S7Logic_Controller_DL_Standard_Template)
        reload(S7Logic_DefaultAlarms_Template)
        reload(sys)
        sys.setdefaultencoding("utf-8")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for CONTROLLER.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for CONTROLLER.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'
  
    def process(self, *params):
        theCurrentPco = params[0]
        theCurrentDevice = params[1]
        callUserTemplate  = str(params[2])
        userTemplateName  = str(params[3])
        name = theCurrentDevice.getDeviceName()
        self.thePlugin.writeInUABLog("processInstances in Jython for Controller for the $name$.")
	
        # General Steps for Dependent Logic templates:
        # 1. Create the FUNCTION called DeviceName_DL. Inside this function we must include the next steps:
        #	1.1. Create all the variable that we're using on this Function
        #	1.2. Setting the "Auto Parameter Restore" (AuPRest). Set the RE_RunOSt or the FE_RunOSt of his master into the AuPRest of the current control Object
        #	1.3. Setpoint management
        #	1.4. Driving the controller. HMV (Hardware Measured value. Feedback Measured Value of the controlled process.)
        #	1.5. Tracking the controller. HOutO (Hardware Output Order.Feedback position of controlled object.)
        #   1.6. IoSimu and IoError
        # 	1.7. Run the CPC_FB_PID Function Block from the Baseline
        # 	1.8. Copy the status of the current Controller to global status DB
        # 	1.9. No master 	
	
        # Step 1: Create the FUNCTION called DeviceName_DL. 
        master = theCurrentPco.getDeviceName()
        theRawInstances = self.thePlugin.getUnicosProject()
	
        recordNumber = 0
        currentRecordNumber = 0
        
        theControllerInstances = theRawInstances.getDeviceType("Controller").getAllDeviceTypeInstances()
        for PIDInst in theControllerInstances:
            currentName = PIDInst.getAttributeData("DeviceIdentification:Name")
            recordNumber = recordNumber + 1
            if currentName == name:
                currentRecordNumber = recordNumber

        ApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instVector = theRawInstances.findMatchingInstances ("Controller","'#DeviceIdentification:Name#'='$name$'")
        if (instVector.size()==0):
            self.thePlugin.writeErrorInUABLog("The instance $name$ does not exist in the specification file.")
            self.thePlugin.writeInfoInUABLog("Reload the specification file by clicking on the Reload Specs. button in the wizard.")
            return
        for inst in instVector:
            Name = name = inst.getAttributeData("DeviceIdentification:Name")
            LparamVector = S7Logic_DefaultAlarms_Template.getLparameters(inst)
            IncreaseSpeed = inst.getAttributeData("FEDeviceAutoRequests:Default Setpoint Speed:Increase Speed")
            DecreaseSpeed = inst.getAttributeData("FEDeviceAutoRequests:Default Setpoint Speed:Decrease Speed")
            MeasuredValue = inst.getAttributeData("FEDeviceEnvironmentInputs:Measured Value")
            ScalingMethod = inst.getAttributeData("FEDeviceParameters:Controller Parameters:Scaling Method")
            PIDCycle = inst.getAttributeData("FEDeviceParameters:Controller Parameters:PID Cycle (s)")
            PIDCycle = str(float(PIDCycle)*1000)
            description = inst.getAttributeData("DeviceDocumentation:Description")
	

            #Count number of controlled Objects
            ControlledObjectNames = inst.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ")
            ControlledObjectList = ControlledObjectNames.split() 
            ControlledObjectNumber = len(ControlledObjectList)

            #check if 1st controlled objet is a PID
            ControlledObject1IsPID = 0
            if ControlledObjectNumber > 0:
                instPID = theRawInstances.findMatchingInstances("Controller","'#DeviceIdentification:Name#'='$ControlledObjectList[0]$'")
                for inst in instPID:
                    ControlledObject1IsPID = 1

            #check if the current controller in controlled by another controller(cascade)
            MasterControllerName = ""
            AllControllers = theRawInstances.findMatchingInstances("Controller","'#FEDeviceOutputs:Controlled Objects#' != ''")
            for PID in AllControllers:
                PIDName = PID.getAttributeData ("DeviceIdentification:Name").strip()
                ControlledObjectNames2 =  PID.getAttributeData ("FEDeviceOutputs:Controlled Objects").replace(",", " ")
                ControlledObjectList2 = ControlledObjectNames2.split()
                for ControlledObjectName2 in ControlledObjectList2:
                    if (ControlledObjectName2 == Name):
                        MasterControllerName = PIDName

        self.thePlugin.writeSiemensLogic('''(*Device Logic of $name$ ($description$) ********** Application: $ApplicationName$ *****)
''')

        #call the standard template or the custom one
        if callUserTemplate=="true":
           eval(userTemplateName).ControllerLogic(self.thePlugin, theRawInstances, master, name, LparamVector, MasterControllerName, IncreaseSpeed, DecreaseSpeed, MeasuredValue)		
        else:
           S7Logic_Controller_DL_Standard_Template.ControllerLogic(self.thePlugin, theRawInstances, master, name, LparamVector, MasterControllerName, IncreaseSpeed, DecreaseSpeed, MeasuredValue)

        # Step 1.5: Tracking the controller. HOutO (Hardware Output Order.Feedback position of controlled object.)
        self.thePlugin.writeSiemensLogic('''
(*Tracking***********)
''')

        # Discover which object (ANALOG or ANADIG for example) has been controlled by this PID. With this info set the Input variable 
        # in order to run the inverse scaling funtion

        self.thePlugin.writeSiemensLogic('''
// Scaling Function
// Values to change if SPlit Range 
''')

        #3.4 Compute HoutO coming from controlled Objects
        if ControlledObject1IsPID:
            ControlledOut = "$ControlledObjectList[0]$.ActSP"
            ControlledMaxRan = "$ControlledObjectList[0]$.PControl.PMaxRan"
            ControlledMinRan = "$ControlledObjectList[0]$.PControl.PMinRan"
        elif ControlledObjectNumber > 0:
            ControlledOut = "$ControlledObjectList[0]$.PosRSt"
            ControlledMaxRan = "$ControlledObjectList[0]$.Panalog.PMaxRan"
            ControlledMinRan = "$ControlledObjectList[0]$.Panalog.PMinRan"
        else:
            ControlledOut = "0.0"
            ControlledMaxRan = "100.0"
            ControlledMinRan = "0.0"

        if ScalingMethod == "Input Scaling" and ControlledObjectNumber == 1:
           self.thePlugin.writeSiemensLogic('''
IN := $ControlledOut$; 
In_Min := $ControlledMinRan$;
In_Max := $ControlledMaxRan$;
Out_Min	:= 0.0;
Out_Max := 100.0;

// Scaling for analog OUT := (IN-In_Min) * (Out_Max-Out_Min)/(In_Max-In_Min) + Out_Min; 
// Inverse Scaling function for Control Objects:
OUT := (IN - Out_Min) * (Out_Max-Out_Min)/(In_Max-In_Min) + Out_Min;  //scaling for PID

$name$.HOutO := OUT;

''')
        elif ((ScalingMethod == "Input/Output Scaling" or ScalingMethod == "No Scaling")  and ControlledObjectNumber == 1):
            self.thePlugin.writeSiemensLogic('''
$name$.HOutO := $ControlledOut$;
''')
        else:
            self.thePlugin.writeSiemensLogic('''
// No tracking
$name$.HOutO := 0.0;
''')


        # Set the AuTrR (The control logic requests to apply the tracking working state.)
        # The control logic requests to apply the tracking working state.
        self.thePlugin.writeSiemensLogic('''
$name$.AuTrR := ''')
        if ControlledObjectNumber == 1:
            self.thePlugin.writeSiemensLogic('''
$ControlledObjectList[0]$.FoMoSt OR $ControlledObjectList[0]$.MMoSt OR 
''')	
            if not ControlledObject1IsPID:	
                self.thePlugin.writeSiemensLogic('''
$ControlledObjectList[0]$.TStopISt OR $ControlledObjectList[0]$.FuStopISt OR 
''')
        self.thePlugin.writeSiemensLogic('''0;
''')


        # Step 1.6: IoSimu and IoError:
        # IO Error. Set the Controller IOError with the controlled objects IOErrors

        self.thePlugin.writeSiemensLogic('''
// IO Error
$name$.IoError := ''')
        if MeasuredValue <> "":
            s7db_id_result=self.thePlugin.s7db_id(MeasuredValue, "AnalogInput, AnalogInputReal", True) #return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications
            if s7db_id_result <> "":
                self.thePlugin.writeSiemensLogic(s7db_id_result+MeasuredValue+'''.IoErrorW OR
''')
        for ControlledObject in ControlledObjectList:
            self.thePlugin.writeSiemensLogic(ControlledObject+'''.IoErrorW OR
''')
        self.thePlugin.writeSiemensLogic('''
0;''')


        # IO Simu. Set the Controller IOSimu with the controlled objects IOSimus
        ioSimuList = ["0"]
        if MeasuredValue <> "":
            s7db_id_result=self.thePlugin.s7db_id(MeasuredValue, "AnalogInput, AnalogInputReal", True) #return DB_SIMPLE if it exists to avoid 'Too Many Symbols' error for large applications
            if s7db_id_result <> "":
                ioSimuList.append(s7db_id_result+MeasuredValue+".IoSimuW OR " + s7db_id_result+MeasuredValue+".FoMoSt")

        for controlledObject in ControlledObjectList:
            ioSimuList.append(controlledObject+".IoSimuW")
        ioSimuList.append("0")
        self.thePlugin.writeSiemensLogic('''
// IO Simu
$name$.IoSimu := ''' + " OR\n".join(ioSimuList) + ";")


        self.thePlugin.writeSiemensLogic('''    
old_status1 := DB_bin_status_PID.$name$.stsreg01;
old_status2 := DB_bin_status_PID.$name$.stsreg02;
old_status1 := ROR(IN:=old_status1, N:=16);
old_status2 := ROR(IN:=old_status2, N:=16);
''')

        # Step 1.7: Run the CPC_FB_PID Function Block from the Baseline
        group = self.thePlugin.getGroup(name)
        if group is None:
            return

        self.thePlugin.writeSiemensLogic('''
(*Execution ********)
// Controller: group='''+str(group)+'''; Sampling Time= $PIDCycle$ms;
$name$.pid_activation:= DB_SCHED.LOOP_DAT['''+str(group)+'''].ENABLE;
$name$.PControl.PIDCycle:=DB_SCHED.LOOP_DAT['''+str(group)+'''].cycle;

// Passing the ManualRequest from the DB_PID_ManRequest to DB $name$
''')

        self.thePlugin.writeSiemensLogic('''
CPC_FB_PID.$name$(

        Manreg01 :=  DB_PID_ManRequest.PID_Requests['''+str(currentRecordNumber)+'''].Manreg01,		// set by WinCCOA in the DB_PID_ManRequest
        ManReg02 :=  DB_PID_ManRequest.PID_Requests['''+str(currentRecordNumber)+'''].ManReg02,          // set by WinCCOA in the DB_PID_ManRequest
        MPosR :=  DB_PID_ManRequest.PID_Requests['''+str(currentRecordNumber)+'''].MPosR,		// set by WinCCOA in the DB_PID_ManRequest
        MSP :=  DB_PID_ManRequest.PID_Requests['''+str(currentRecordNumber)+'''].MSP,          // set by WinCCOA in the DB_PID_ManRequest
        MSPH :=  DB_PID_ManRequest.PID_Requests['''+str(currentRecordNumber)+'''].MSPH,		// set by WinCCOA in the DB_PID_ManRequest
        MSPL :=  DB_PID_ManRequest.PID_Requests['''+str(currentRecordNumber)+'''].MSPL,          // set by WinCCOA in the DB_PID_ManRequest
        MOutH :=  DB_PID_ManRequest.PID_Requests['''+str(currentRecordNumber)+'''].MOutH,		// set by WinCCOA in the DB_PID_ManRequest
        MOutL :=  DB_PID_ManRequest.PID_Requests['''+str(currentRecordNumber)+'''].MOutL,          // set by WinCCOA in the DB_PID_ManRequest
        MKc :=  DB_PID_ManRequest.PID_Requests['''+str(currentRecordNumber)+'''].MKc,		// set by WinCCOA in the DB_PID_ManRequest
        MTi :=  DB_PID_ManRequest.PID_Requests['''+str(currentRecordNumber)+'''].MTi,          // set by WinCCOA in the DB_PID_ManRequest
        MTd :=  DB_PID_ManRequest.PID_Requests['''+str(currentRecordNumber)+'''].MTd,		// set by WinCCOA in the DB_PID_ManRequest
        MTds :=  DB_PID_ManRequest.PID_Requests['''+str(currentRecordNumber)+'''].MTds          // set by WinCCOA in the DB_PID_ManRequest	

);

''')


        # Step 1.8: Copy the status of the current Controller to global status DB
        self.thePlugin.writeSiemensLogic('''
//Reset AuAuMoR
$Name$.AuAuMoR := FALSE;	


//Recopy new status
(*Copy to global status DB*************)
DB_bin_status_PID.$name$.stsreg01:= $name$.Stsreg01;
DB_bin_status_PID.$name$.stsreg02:= $name$.Stsreg02;
DB_ana_status_PID.$name$.MSPSt:= $name$.MSPSt;
DB_ana_status_PID.$name$.AuSPSt:= $name$.AuSPSt;
DB_ana_status_PID.$name$.ActSP:= $name$.ActSP;
DB_ana_status_PID.$name$.ActSPH:= $name$.ActSPH;
DB_ana_status_PID.$name$.ActSPL:= $name$.ActSPL;
DB_ana_status_PID.$name$.OutOV:= $name$.OutOV;
DB_ana_status_PID.$name$.ActOutH:= $name$.ActOutH;
DB_ana_status_PID.$name$.ActOutL:= $name$.ActOutL;
DB_ana_status_PID.$name$.AuPosRSt:= $name$.AuPosRSt;
DB_ana_status_PID.$name$.MPosRSt:= $name$.MPosRSt;
DB_ana_status_PID.$name$.MV:= $name$.MV;
DB_ana_status_PID.$name$.ActKc:= $name$.ActKc;
DB_ana_status_PID.$name$.ActTi:= $name$.ActTi;
DB_ana_status_PID.$name$.ActTd:= $name$.ActTd;
DB_ana_status_PID.$name$.ActTds:= $name$.ActTds;
    
DB_Event_PID.PID_evstsreg['''+str(currentRecordNumber)+'''].evstsreg01 := old_status1 OR DB_bin_status_PID.$name$.stsreg01;
DB_Event_PID.PID_evstsreg['''+str(currentRecordNumber)+'''].evstsreg02 := old_status2 OR DB_bin_status_PID.$name$.stsreg02;
''')

        self.thePlugin.writeSiemensLogic('''
END_FUNCTION

''')

    def end(self):
       self.thePlugin.writeInUABLog("end in Jython for CONTROLLER.")


    def shutdown(self):
       self.thePlugin.writeInUABLog("shutdown in Jython for CONTROLLER.")
		
