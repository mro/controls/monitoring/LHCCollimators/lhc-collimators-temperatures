# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
from java.util import ArrayList
from java.util import Vector
from time import strftime
import S7Logic_BL_Standard_Template
import S7Logic_DefaultAlarms_Template
import sys

class BL_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0   

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for BL.")
        reload(S7Logic_BL_Standard_Template)
        reload(S7Logic_DefaultAlarms_Template)
        reload(sys)
        sys.setdefaultencoding("utf-8")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for BL.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for BL.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'

    def process(self, *params):
        theCurrentPco = params[0]
        theCurrentSection = params[1]
        callUserTemplate  = str(params[2])
        userTemplateName  = str(params[3])
        name = theCurrentPco.getDeviceName()
        self.thePlugin.writeInUABLog("processInstances in Jython for BL for PCO $name$.")

        # General Steps for PCO section templates:
        # 1. Create the FUNCTION called DeviceName_BL. Inside this function we must include the next steps:
        #	1.1. IOError IOSimu
        #	1.2: Blocked Alarm warning

        theRawInstances = self.thePlugin.getUnicosProject()
        master = Master = theCurrentPco.getMasterDevice().getDeviceName()
        ApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instVector = theRawInstances.findMatchingInstances("ProcessControlObject", "'#DeviceIdentification:Name#'='$name$'")
        if (instVector.size()==0):
            self.thePlugin.writeErrorInUABLog("The instance $name$ does not exist in the specification file.")
            self.thePlugin.writeInfoInUABLog("Reload the specification file by clicking on the Reload Specs. button in the wizard.")
            return
        description = theRawInstances.createSectionText(instVector, 1, 1, '''#DeviceDocumentation:Description#''').strip() 
        # Gets all the Digital Alarms that are children of the current PCO object
        theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition = S7Logic_DefaultAlarms_Template.getDigitalAlarms(theRawInstances, name)

        # Gets all the Analog Alarms that are children of the current PCO object
        theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition = S7Logic_DefaultAlarms_Template.getAnalogAlarms(theRawInstances, name)

        for inst in instVector:
            LparamVector = S7Logic_DefaultAlarms_Template.getLparameters(inst)
        self.thePlugin.writeSiemensLogic('''(**** WARNING:   $name$ ($description$)********** Application: $ApplicationName$ **********)
''')

        #call the standard template or the custom one
        if callUserTemplate=="true":
            eval(userTemplateName).BLLogic(self.thePlugin, theRawInstances, master, name, LparamVector, theCurrentPco, allTheAnalogAlarms, allTheDigitalAlarms)		
        else:
            S7Logic_BL_Standard_Template.BLLogic(self.thePlugin, theRawInstances, master, name, LparamVector, theCurrentPco, allTheAnalogAlarms, allTheDigitalAlarms)

        self.thePlugin.writeSiemensLogic('''
END_FUNCTION
''')

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for BL.")


    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for BL.")

