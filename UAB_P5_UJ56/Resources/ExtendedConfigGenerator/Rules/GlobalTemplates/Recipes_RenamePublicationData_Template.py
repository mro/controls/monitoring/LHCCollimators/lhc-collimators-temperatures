# Jython template to modify the data contained in the DipPublications.xml file.
# The processInstances method gets the JXPathContext and uses it to select nodes 
# and modify the desired values.

from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED


class RenamePublicationData_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.rcpService = self.thePlugin.getServiceInstance("Recipes")
        self.thePlugin.writeInUABLog("initialize in Jython for RenameClassData.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for RenameClassData.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for RenameClassData.")

    def process(self):
        self.thePlugin.writeInUABLog("processInstances in Jython for RenameClassData.")

        # Gets the JXPath context of the DipPublications.xml file
        classContext = self.rcpService.getJXPathContext()

        # Example: Modify the CMW Config for the device instance: QSDN_4_24VPwOn
        # node = publicationsContext.selectSingleNode("cmwPublication[alias='QSDN_4_24VPwOn']")
        # node.setCmwConfig("MyNewConfig")

        # Example: Modify the Time of all the DigitalInput objects
        # nodes = publicationsContext.selectNodes("cmwPublication[deviceType='DigitalInput']")
        # for node in nodes:
        #   node.setTime("long")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for RenameClassData.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for RenameClassData.")
