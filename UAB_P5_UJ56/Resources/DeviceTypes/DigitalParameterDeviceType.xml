<?xml version='1.0' encoding='UTF-8'?>
<UNICOSMetaModel xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='..\unicos\UNICOSMetaModel.xsd'>
  <Information>
    <Name>DigitalParameter</Name>
    <ObjectTypeFamily>InterfaceObjectFamily</ObjectTypeFamily>
    <Description>Digital Parameter Device</Description>
    <Version>$LastChangedRevision: 117564 $</Version>
    <VersionComments/>
    <Status/>
  </Information>
  <AttributeFamily>
    <AttributeFamilyName>DeviceIdentification</AttributeFamilyName>
    <UserExpandable>false</UserExpandable>
    <Attribute>
      <AttributeName>Name</AttributeName>
      <Description>Name of the device. It must be unique.
Max length:
- Schneider: 23
- Siemens: Field objects, Controller and PCO: 19; Local: 21; otherwise: 24
Forbidden chars:  [: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]-., double underscore, and page break</Description>
      <PrimitiveType>STRING</PrimitiveType>
      <isSpecificationAttribute>
        <isValueRequired>true</isValueRequired>
        <Usage>Name displayed at the SCADA level if "Expert Name" is not specified.
This name will appear in the datapoints created in the SCADA layer.</Usage>
        <DependentAttributes>Device Links.
The name of the device(s) specified in Device Links *must* correspond to "Expert Name" if it is defined.
If "Expert Name" is not defined, the name of the device(s) specified in Device Links corresponds to "Name".</DependentAttributes>
        <Constraints>Max length:
- Schneider: 23
- Siemens: Field objects, Controller and PCO: 19; Local: 21; otherwise: 24
Forbidden chars:  [: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]-., double underscore, and page break
Name must be unique.</Constraints>
      </isSpecificationAttribute>
    </Attribute>
    <Attribute>
      <AttributeName>ExpertName</AttributeName>
      <Description>Name of the device displayed at the SCADA level. It must be unique.
Forbidden characters:  *[: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]</Description>
      <PrimitiveType>STRING</PrimitiveType>
      <isSpecificationAttribute>
        <NameRepresentation>Expert Name</NameRepresentation>
        <TypeRepresentation>STRING</TypeRepresentation>
        <isValueRequired>false</isValueRequired>
        <Usage>It does not affect to the datapoints names in the SCADA layer.</Usage>
        <DependentAttributes>Device Links.
The name of the device(s) specified in Device Links *must* correspond to "Expert Name" if it is defined.
If "Expert Name" is not defined, the name of the device(s) specified in Device Links corresponds to "Name".</DependentAttributes>
        <Constraints>In principle there is no limit to the number of characters used, however a long name may result in display issues at the SCADA level.
Forbidden characters:  *[: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]
Expert Name must be unique.</Constraints>
      </isSpecificationAttribute>
    </Attribute>
  </AttributeFamily>
  <AttributeFamily>
    <AttributeFamilyName>DeviceDocumentation</AttributeFamilyName>
    <UserExpandable>true</UserExpandable>
    <Attribute>
      <AttributeName>DeviceDescription</AttributeName>
      <Description>Description of the device.  </Description>
      <PrimitiveType>STRING</PrimitiveType>
      <isSpecificationAttribute>
        <NameRepresentation>Description</NameRepresentation>
        <isValueRequired>false</isValueRequired>
        <Usage>Used in the SCADA layer in the device faceplate</Usage>
        <DependentAttributes/>
        <Constraints>In principle there is no limit to the number of characters used, however a long description may result in display issues at the SCADA level.
Forbidden characters:  ;</Constraints>
      </isSpecificationAttribute>
    </Attribute>
    <Attribute>
      <AttributeName>Remarks</AttributeName>
      <Description>Field used to add relevant information about the device. </Description>
      <PrimitiveType>STRING</PrimitiveType>
      <isSpecificationAttribute>
        <isValueRequired>false</isValueRequired>
        <Usage>This information is not used in the generation process, it remains only at the specification level for documentation purposes.</Usage>
        <DependentAttributes/>
        <Constraints>Forbidden characters:  ;</Constraints>
      </isSpecificationAttribute>
    </Attribute>
  </AttributeFamily>
  <AttributeFamily>
    <AttributeFamilyName>FEDeviceParameters</AttributeFamilyName>
    <UserExpandable>true</UserExpandable>
    <Attribute>
      <AttributeName>DefaultValue</AttributeName>
      <Meaning>Default value</Meaning>
      <Description>Default value for the parameter </Description>
      <PrimitiveType>BOOLEAN</PrimitiveType>
      <isSpecificationAttribute>
        <NameRepresentation>Default Value</NameRepresentation>
        <TypeRepresentation>BOOLEAN</TypeRepresentation>
        <isValueRequired>true</isValueRequired>
        <Usage>This is the default parameter value assigned into the PLC variable and in the SCADA datapoint element. </Usage>
        <DependentAttributes/>
        <Constraints/>
      </isSpecificationAttribute>
    </Attribute>
  </AttributeFamily>
  <AttributeFamily>
    <AttributeFamilyName>FEDeviceManualRequests</AttributeFamilyName>
    <UserExpandable>true</UserExpandable>
    <Attribute>
      <AttributeName>ManReg01</AttributeName>
      <Meaning>Manual Register 1</Meaning>
      <Description>Manual Register 1</Description>
      <isCommunicated>true</isCommunicated>
      <PrimitiveType>WORD</PrimitiveType>
      <Attribute>
        <AttributeName>ArmRcp</AttributeName>
        <Meaning>Armed Recipe</Meaning>
        <Description>A Recipe is Armed : New values are available at the input</Description>
        <PrimitiveType>BIT1</PrimitiveType>
        <BitPosition>2</BitPosition>
      </Attribute>
      <Attribute>
        <AttributeName>ActRcp</AttributeName>
        <Meaning>Activate Recipe</Meaning>
        <Description>Activate Recipe : All new signals at the inputs are activated.</Description>
        <PrimitiveType>BIT1</PrimitiveType>
        <BitPosition>3</BitPosition>
      </Attribute>
      <Attribute>
        <AttributeName>MOnR</AttributeName>
        <Meaning>Manual On Request</Meaning>
        <Description>Manual On Request: The operator requests the On/Open position</Description>
        <PrimitiveType>BIT1</PrimitiveType>
        <BitPosition>4</BitPosition>
      </Attribute>
      <Attribute>
        <AttributeName>MOffR</AttributeName>
        <Meaning>Manual Off Request</Meaning>
        <Description>Manual Off Request: The operator requests the Off/Close position</Description>
        <PrimitiveType>BIT1</PrimitiveType>
        <BitPosition>5</BitPosition>
      </Attribute>
    </Attribute>
  </AttributeFamily>
  <AttributeFamily>
    <AttributeFamilyName>FEDeviceOutputs</AttributeFamilyName>
    <UserExpandable>true</UserExpandable>
    <Attribute>
      <AttributeName>StsReg01</AttributeName>
      <Meaning>Status Register 1</Meaning>
      <Description>Status Register 1</Description>
      <isEventAttribute>true</isEventAttribute>
      <isCommunicated>true</isCommunicated>
      <PrimitiveType>WORD</PrimitiveType>
      <Attribute>
        <AttributeName>PosSt</AttributeName>
        <Meaning>Position Status</Meaning>
        <Description>Digital Position Status</Description>
        <isArchived>true</isArchived>
        <PrimitiveType>BIT1</PrimitiveType>
        <BitPosition>0</BitPosition>
      </Attribute>
      <Attribute>
        <AttributeName>ArmRcpSt</AttributeName>
        <Meaning>Armed Recipe Status</Meaning>
        <Description>A Recipe is Armed : New values are available at the input</Description>
        <PrimitiveType>BIT1</PrimitiveType>
        <BitPosition>3</BitPosition>
      </Attribute>
      <Attribute>
        <AttributeName>MOnRSt</AttributeName>
        <Meaning>Manual On Request Status</Meaning>
        <Description>Manual On Request Status</Description>
        <PrimitiveType>BIT1</PrimitiveType>
        <BitPosition>4</BitPosition>
      </Attribute>
      <Attribute>
        <AttributeName>MOffRSt</AttributeName>
        <Meaning>Manual Off Request Status</Meaning>
        <Description>Manual Off Request Status</Description>
        <PrimitiveType>BIT1</PrimitiveType>
        <BitPosition>5</BitPosition>
      </Attribute>
    </Attribute>
    <Attribute>
      <AttributeName>PosSt</AttributeName>
      <Meaning>Position status</Meaning>
      <Description>Position Status</Description>
      <PrimitiveType>BOOLEAN</PrimitiveType>
    </Attribute>
  </AttributeFamily>
  <AttributeFamily>
    <AttributeFamilyName>SCADADeviceGraphics</AttributeFamilyName>
    <UserExpandable>true</UserExpandable>
    <Attribute>
      <AttributeName>WidgetType</AttributeName>
      <Description>Define the widget type to display in the SCADA device tree overview only. 
The widget displayed in the process panel will be selected when the user creates the panel.</Description>
      <PrimitiveType>STRING</PrimitiveType>
      <isSpecificationAttribute>
        <NameRepresentation>Widget Type</NameRepresentation>
        <isValueRequired>true</isValueRequired>
        <isCaseSensitive>true</isCaseSensitive>
        <PermittedValue>DigitalParameter</PermittedValue>
        <Usage/>
        <DependentAttributes/>
        <Constraints/>
      </isSpecificationAttribute>
    </Attribute>
    <Attribute>
      <AttributeName>Synoptic</AttributeName>
      <Description>Define link between the device and an existing synoptic where it appears. The synoptic specified here can be accessed from the device right-click menu item "Synoptic".</Description>
      <PrimitiveType>STRING</PrimitiveType>
      <isSpecificationAttribute>
        <isValueRequired>false</isValueRequired>
        <Usage>Specify the path of the .pnl file under the "\panel" directory of the PVSS project.</Usage>
        <DependentAttributes/>
        <Constraints/>
      </isSpecificationAttribute>
    </Attribute>
    <Attribute>
      <AttributeName>DiagnosticPanel</AttributeName>
      <Description>Define link between the device and an existing diagnostic panel for the device. The panel specified here can be accessed from the device right-click menu item "Diagnostic" as well as from the "Diagnostic" button on the object faceplate.</Description>
      <PrimitiveType>STRING</PrimitiveType>
      <isSpecificationAttribute>
        <NameRepresentation>Diagnostic</NameRepresentation>
        <isValueRequired>false</isValueRequired>
        <Usage>Specify the path of the .pnl file under the "\panel" directory of the PVSS project </Usage>
        <DependentAttributes/>
        <Constraints/>
      </isSpecificationAttribute>
    </Attribute>
    <Attribute>
      <AttributeName>WWWLink</AttributeName>
      <Description>Define link between the device and an existing web page (or pdf file, or other file which can be opened with IE).  The link can be accessed from the device right-click menu item "Info" as well as from the "Info" button on the object faceplate.</Description>
      <PrimitiveType>STRING</PrimitiveType>
      <isSpecificationAttribute>
        <NameRepresentation>WWW Link</NameRepresentation>
        <isValueRequired>false</isValueRequired>
        <Usage/>
        <DependentAttributes/>
        <Constraints/>
      </isSpecificationAttribute>
    </Attribute>
  </AttributeFamily>
  <AttributeFamily>
    <AttributeFamilyName>SCADADeviceFunctionals</AttributeFamilyName>
    <UserExpandable>false</UserExpandable>
    <Attribute>
      <AttributeName>MaskEvent</AttributeName>
      <Description>If TRUE: the events of the device will be masked in SCADA and not diplayed or archived in the Event List.
An 'event' is defined as a bit change in StsReg01 or StsReg02</Description>
      <PrimitiveType>BOOLEAN</PrimitiveType>
      <isSpecificationAttribute>
        <NameRepresentation>Mask Event</NameRepresentation>
        <isValueRequired>false</isValueRequired>
        <Usage/>
        <DependentAttributes/>
        <Constraints/>
      </isSpecificationAttribute>
    </Attribute>
    <Attribute>
      <AttributeName>AccessControlDomain</AttributeName>
      <Description>Define Access Control on the device to an existing SCADA Domain
Forbidden characters:  *[: "'@`#$%^&amp;*?!;=+~(){}&lt;&gt;|]</Description>
      <PrimitiveType>STRING</PrimitiveType>
      <isSpecificationAttribute>
        <NameRepresentation>Access Control Domain</NameRepresentation>
        <isValueRequired>false</isValueRequired>
        <Usage>This domain is used to grant access to this specific device. The domain specified for this object will allow access to the object only to registered users on that domain</Usage>
        <DependentAttributes/>
        <Constraints>Forbidden characters:  *[: "'@`#$%^&amp;*?!;=+~(){}&lt;&gt;|]</Constraints>
      </isSpecificationAttribute>
    </Attribute>
    <Attribute>
      <AttributeName>SCADADeviceClassificationTags</AttributeName>
      <Description>It defines the Domain, Nature and DeviceLinks for the SCADA visualization</Description>
      <PrimitiveType>STRUCT</PrimitiveType>
      <Attribute>
        <AttributeName>Domain</AttributeName>
        <Description>Domain of the device. If empty, the domain will be the name of the application
Forbidden characters:  *[: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]</Description>
        <PrimitiveType>STRING</PrimitiveType>
        <isSpecificationAttribute>
          <isValueRequired>false</isValueRequired>
          <Usage>Domain is used to filter the devices in the alarm list or in the device tree overview</Usage>
          <DependentAttributes/>
          <Constraints>Forbidden characters:  *[: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]</Constraints>
        </isSpecificationAttribute>
      </Attribute>
      <Attribute>
        <AttributeName>Nature</AttributeName>
        <Description>Nature of the device. If empty, the nature will be the type of the device
Forbidden characters:  *[: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]</Description>
        <PrimitiveType>STRING</PrimitiveType>
        <isSpecificationAttribute>
          <isValueRequired>false</isValueRequired>
          <Usage>Nature is used to filter the devices in the alarm list or in the device tree overview</Usage>
          <DependentAttributes/>
          <Constraints>Forbidden characters:  *[: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]</Constraints>
        </isSpecificationAttribute>
      </Attribute>
      <Attribute>
        <AttributeName>DeviceLinks</AttributeName>
        <Description>Define links to other devices (separate device names with commas). 
Note: it is not necessary to link to master, parents or children because these links are automatically created.
Forbidden characters:  *[: "'@`#$%^&amp;*?!;=+~(){}&lt;&gt;|]</Description>
        <PrimitiveType>STRING</PrimitiveType>
        <isSpecificationAttribute>
          <NameRepresentation>Device Links</NameRepresentation>
          <isValueRequired>false</isValueRequired>
          <Usage>Linked devices will be shown in the device right-click menu</Usage>
          <DependentAttributes>Expert Name or Name.
The name of the device(s) specified here *must* correspond to "Expert Name" if it is defined.
If "Expert Name" is not defined, the name of the device(s) specified here corresponds to "Name".</DependentAttributes>
          <Constraints>Forbidden characters:  *[: "'@`#$%^&amp;*?!;=+~(){}&lt;&gt;|]</Constraints>
        </isSpecificationAttribute>
      </Attribute>
    </Attribute>
  </AttributeFamily>
  <AttributeFamily>
    <AttributeFamilyName>SCADADeviceDataArchiving</AttributeFamilyName>
    <UserExpandable>true</UserExpandable>
    <Attribute>
      <AttributeName>ArchiveMode</AttributeName>
      <Description>Archive mode of the object engineering values. Archive if:
Old/New Comparison: value changes
Time: value changes after Time Filter
Deadband: value &lt; or &gt; deadband
AND: at least one of the conditions is fulfilled
OR: both conditions are fulfilled</Description>
      <PrimitiveType>STRING</PrimitiveType>
      <isSpecificationAttribute>
        <NameRepresentation>Archive Mode</NameRepresentation>
        <isValueRequired>true</isValueRequired>
        <PermittedValue>No</PermittedValue>
        <PermittedValue>Old/New Comparison</PermittedValue>
        <Usage>This archive mode is used to archive data in the PVSS database</Usage>
        <DependentAttributes>If "Time" is selected, "Time Filter (s)" must be filled 
If "Deadband"  is selected: "Deadband Type" and "Deadband Value" must be filled.</DependentAttributes>
        <Constraints/>
      </isSpecificationAttribute>
    </Attribute>
    <Attribute>
      <AttributeName>BooleanArch</AttributeName>
      <Description>Name of the Boolean archive
Forbidden characters:  *[: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]</Description>
      <PrimitiveType>STRING</PrimitiveType>
      <DefaultValue>boolean</DefaultValue>
      <isSpecificationAttribute>
        <NameRepresentation>Boolean Archive</NameRepresentation>
        <isValueRequired>false</isValueRequired>
        <Usage>The boolean values of the device will be archived in the specified PVSS database. The archive must be created in PVSS before importing the object.</Usage>
        <DependentAttributes/>
        <Constraints>Forbidden characters:  *[: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]</Constraints>
      </isSpecificationAttribute>
    </Attribute>
    <Attribute>
      <AttributeName>AnalogArch</AttributeName>
      <Description>Name of the analog archive
Forbidden characters:  *[: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]</Description>
      <PrimitiveType>STRING</PrimitiveType>
      <DefaultValue>analog</DefaultValue>
      <isSpecificationAttribute>
        <NameRepresentation>Analog Archive</NameRepresentation>
        <isValueRequired>false</isValueRequired>
        <Usage>The analog values of the device will be archived in the specified PVSS database. The archive must be created in PVSS before importing the object.</Usage>
        <DependentAttributes/>
        <Constraints>Forbidden characters:  *[: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]</Constraints>
      </isSpecificationAttribute>
    </Attribute>
    <Attribute>
      <AttributeName>EventArch</AttributeName>
      <Description>Name of the event archive
Forbidden characters:  *[: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]</Description>
      <PrimitiveType>STRING</PrimitiveType>
      <DefaultValue>event</DefaultValue>
      <isSpecificationAttribute>
        <NameRepresentation>Event Archive</NameRepresentation>
        <isValueRequired>false</isValueRequired>
        <Usage>The events generated by the device will be archived in the specified PVSS database. The archive must be created in PVSS before importing the object.</Usage>
        <DependentAttributes/>
        <Constraints>Forbidden characters:  *[: "'@`#$%^&amp;*?!,;=+~(){}&lt;&gt;|]</Constraints>
      </isSpecificationAttribute>
    </Attribute>
  </AttributeFamily>
  <AttributeFamily>
    <AttributeFamilyName>SCADADeviceParameters</AttributeFamilyName>
    <UserExpandable>true</UserExpandable>
    <Attribute>
      <AttributeName>RecipeType</AttributeName>
      <Description>Recipies familie name</Description>
      <PrimitiveType>STRING</PrimitiveType>
    </Attribute>
    <Attribute>
      <AttributeName>RecipeInitName</AttributeName>
      <Description>Name of the collection of defaults values associated to the RecipeType </Description>
      <PrimitiveType>STRING</PrimitiveType>
    </Attribute>
  </AttributeFamily>
  <AttributeFamily>
    <AttributeFamilyName>TargetDeviceInformation</AttributeFamilyName>
    <UserExpandable>true</UserExpandable>
    <Attribute>
      <AttributeName>Target</AttributeName>
      <Description>Identifies a target type (e.g. SIEMENS, SCHNEIDER...)</Description>
      <PrimitiveType>STRUCT</PrimitiveType>
      <DefaultValue>Siemens</DefaultValue>
      <Attribute>
        <AttributeName>RepresentationName</AttributeName>
        <Description>It's the name used ...</Description>
        <PrimitiveType>STRING</PrimitiveType>
        <DefaultValue>DPAR</DefaultValue>
      </Attribute>
      <Attribute>
        <AttributeName>Optimized</AttributeName>
        <Description>Is this object an optimized Object?</Description>
        <PrimitiveType>BOOLEAN</PrimitiveType>
        <DefaultValue>true</DefaultValue>
      </Attribute>
      <Attribute>
        <AttributeName>LimitSize</AttributeName>
        <Description>Maximun number of instances allowed</Description>
        <PrimitiveType>INT32</PrimitiveType>
        <DefaultValue>2000</DefaultValue>
      </Attribute>
    </Attribute>
  </AttributeFamily>
</UNICOSMetaModel>
