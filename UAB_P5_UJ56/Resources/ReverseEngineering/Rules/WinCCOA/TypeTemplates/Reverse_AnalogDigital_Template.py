# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
import Reverse_Generic_Template
reload(Reverse_Generic_Template)
import Reverse_GlobalFunctions_Template
reload(Reverse_GlobalFunctions_Template)
    
class AnalogDigital_Template(Reverse_Generic_Template.Generic_Template): 
    theDeviceType = "AnalogDigital"
    
    def processInstance(self, instance, generatedInstance):
        pass
    
