# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2015 all rights reserved
# Template to generate the IOCommissioning file
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

from java.lang import Runtime

from java.util import Vector
from java.util import ArrayList

import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)

import ucpc_library.shared_grafcet_parsing
reload(ucpc_library.shared_grafcet_parsing)

import ucpc_library.shared_usage_finder
reload(ucpc_library.shared_usage_finder)
import ucpc_library.shared_generic_functions
reload(ucpc_library.shared_generic_functions)
import xml.etree.ElementTree as ET

import os
import re
import sys
from java.lang import System
import textwrap
from research.ch.cern.unicos.utilities import AbsolutePathBuilder
from research.ch.cern.unicos.utilities import XMLInstancesFacade
from research.ch.cern.unicos.utilities.constants import FileOutputFormat

import unicodedata
import csv


def removeNonAscii(inputString):
    return str(unicodedata.normalize('NFKD', unicode(inputString.decode('ascii', 'ignore'))).encode('ascii', 'ignore'))


class IOCommissioning_Template(IUnicosTemplate, ucpc_library.shared_usage_finder.Finder, ucpc_library.shared_generic_functions.SharedGenericFunctions, ucpc_library.shared_decorator.ExpressionDecorator, ucpc_library.shared_grafcet_parsing.SharedGrafcetParsing):
    thePlugin = 0
    theUnicosProject = 0
    PVSSSystemName = "dist_101"
    PVSSAddTransitionLabels = False
    panelW = 1270
    panelH = 830
    outputFileContent = ""
    objectTypeShortNameDict = {
        'DigitalInput': 'DI',
        'AnalogInput': 'AI',
        'AnalogInputReal': 'AIR',
        'DigitalOutput': 'DO',
        'AnalogOutput': 'AO',
        'AnalogOutputReal': 'AOR',
        'DigitalParameter': 'DPAR',
        'WordParameter': 'WPAR',
        'AnalogParameter': 'APAR',
        'WordStatus': 'WS',
        'AnalogStatus': 'AS',
        'Local': 'Local',
        'OnOff': 'OnOff',
        'Analog': 'Analog',
        'AnalogDigital': 'AnaDig',
        'AnaDO': 'AnaDO',
        'MassFlowController': 'MFC',
        'Controller': 'PID',
        'ProcessControlObject': 'Unit',
        'DigitalAlarm': 'DA',
        'AnalogAlarm': 'AA'
    }

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("IO Commissioning: initialize")
        if self.PVSSSystemName:
            self.PVSSSystemName += ":"

    def check(self):
        self.thePlugin.writeInUABLog("IO Commissioning: check")

    def begin(self):
        self.thePlugin.writeInUABLog("IO Commissioning: begin")

    def process(self, *params):
        self.thePlugin.writeInUABLog("IO Commissioning: processInstances")
        self.theUnicosProject = params[0]

        # Look for sources in *exported_src directory underneath parent directory.
        # If does not exist, will use generated output directory by default
        if self.isSiemens():
            pathToSources = str(self.locateDir("*exported_src", AbsolutePathBuilder.getAbsolutePath("../"))) + "/"
        elif self.isSchneider():
            pathToSources = str(self.locateFile("s??", "*.xef", AbsolutePathBuilder.getAbsolutePath("../")))
        else:
            pathToSources = ""

        # use generated output
        if not os.path.exists(pathToSources):
            if self.isSiemens():
                self.thePlugin.writeWarningInUABLog("Cannot find *exported_src path (" + str(pathToSources) + ") in parent hierarchy. Using generated output directory by default.")
            elif self.isSchneider():
                self.thePlugin.writeWarningInUABLog("Cannot find *.xef file in s?? directory  (" + str(pathToSources) + ") in parent hierarchy. Using generated output directory by default.")
            ProjectSources = self.loadSources()
            self.thePlugin.writeInUABLog("Loaded sources.")

        # use exported directory
        else:
            self.thePlugin.writeWarningInUABLog("path=" + pathToSources)
            ProjectSources = self.loadSources(pathToSources)
            self.thePlugin.writeInUABLog("Loaded sources.")

        # in case of no sources..
        if not ProjectSources:
            self.thePlugin.writeInUABLog("Could not load sources.")
            pass

        # Writes the header of the excel/xml file

        self.outputFileContent += '''
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>iprietob</Author>
  <LastAuthor>iprietob</LastAuthor>
  <Created>2010-11-25T11:00:08Z</Created>
  <LastSaved>2010-11-25T11:22:31Z</LastSaved>
  <Company>CERN</Company>
  <Version>1.0</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12660</WindowHeight>
  <WindowWidth>19020</WindowWidth>
  <WindowTopX>120</WindowTopX>
  <WindowTopY>105</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
  <Styles>
   <Style  xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="Default" >
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior />
   <NumberFormat ss:Format="@" />
   <Protection/>
  </Style>
<Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLEHIDDEN">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#FFFFFF"/>
  </Style>
  <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLE1">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#FFCC99" ss:Pattern="Solid"/>
  </Style>
  <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLE2">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
  </Style>
  <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLE3">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLE4">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#CCFFFF" ss:Pattern="Solid"/>
  </Style>
    <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="m160910728UABSTYLE">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#CCFFFF" ss:Pattern="Solid"/>
  </Style>
    <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLE5">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
  </Style>
    <Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="UABSTYLE6">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:CharSet="204" x:Family="Swiss" ss:Bold="1"/>
   <Interior ss:Color="#CCC0DA" ss:Pattern="Solid"/>
  </Style>
<Style xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" ss:ID="s30UABSTYLE">
   <Font x:CharSet="204" x:Family="Swiss"/>
  </Style>
  <Style ss:ID="allBorders">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat ss:Format="@"/>
   <Protection/>
  </Style>
  <Style ss:ID="allBordersWrap">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat ss:Format="@"/>
   <Protection/>
  </Style>
  </Styles>
   '''

        # SUMMARY OF COMMISSIONING

        self.outputFileContent += '''
<Worksheet ss:Name="SUMMARY">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>

    <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">Project</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">EDMS No</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">Status</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">DRAFT</Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">ICE Responsible</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">CV-CL Responsible</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">CV-OP Responsible</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">AF SVN Revision</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0">
    <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String">PLC SVN Revision</Data></Cell>
        <Cell  ss:StyleID="UABSTYLE1"><Data ss:Type="String"></Data></Cell>
   </Row>

   <Row ss:AutoFitHeight="0" ss:Index="10">
    <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="4" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Unit</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">I/O</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Sequencing</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Regulation</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Unit Interlocks</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Actuator Interlocks</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
        '''

        PCODevice = self.theUnicosProject.getDeviceType("ProcessControlObject")
        if PCODevice is not None:
            PCOInstances = PCODevice.getAllDeviceTypeInstances()
            for instance in PCOInstances:
                self.processPCOSummary(instance)

        # Write Footer of alarms
        self.writeXmlWorksheetFooter()

        # Writes the header of the DI worksheet
        self.outputFileContent += '''
<Worksheet ss:Name="DI">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Address</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Connection</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Conversion</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>
   </Row>

        '''

        digitalInputDevice = self.theUnicosProject.getDeviceType("DigitalInput")
        digitalOutputDevice = self.theUnicosProject.getDeviceType("DigitalOutput")
        analogInputDevice = self.theUnicosProject.getDeviceType("AnalogInput")
        analogOutputDevice = self.theUnicosProject.getDeviceType("AnalogOutput")
        digitalParameterDevice = self.theUnicosProject.getDeviceType("DigitalParameter")
        wordParameterDevice = self.theUnicosProject.getDeviceType("WordParameter")
        analogParameterDevice = self.theUnicosProject.getDeviceType("AnalogParameter")
        OnOffDevice = self.theUnicosProject.getDeviceType("OnOff")
        AnalogDevice = self.theUnicosProject.getDeviceType("Analog")
        AnadigDevice = self.theUnicosProject.getDeviceType("AnalogDigital")
        AnaDODevice = self.theUnicosProject.getDeviceType("AnaDO")
        MFCDevice = self.theUnicosProject.getDeviceType("MassFlowController")
        PCODevice = self.theUnicosProject.getDeviceType("ProcessControlObject")
        PIDDevice = self.theUnicosProject.getDeviceType("Controller")
        DADevice = self.theUnicosProject.getDeviceType("DigitalAlarm")
        AADevice = self.theUnicosProject.getDeviceType("AnalogAlarm")

        # PROCESS DI objects

        if digitalInputDevice is not None:
            diInstances = digitalInputDevice.getAllDeviceTypeInstances()
            for instance in diInstances:
                self.processIndividualDigitalIO(instance, "DigitalInput")

        # Write Footer of DI
        self.writeXmlWorksheetFooter("Portrait")

        # Writes the header of the DO worksheet
        self.outputFileContent += '''
<Worksheet ss:Name="DO">
 <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Address</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Connection</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Conversion</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>
   </Row>
        '''

        # PROCESS DO objects
        if digitalOutputDevice is not None:
            doInstances = digitalOutputDevice.getAllDeviceTypeInstances()
            for instance in doInstances:
                self.processIndividualDigitalIO(instance, "DigitalOutput")

        # Write Footer of DO
        self.writeXmlWorksheetFooter("Portrait")

        # Writes the header of the AI worksheet
        self.outputFileContent += '''
<Worksheet ss:Name="AI">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="3" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Address</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Range</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Connection</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Conversion</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>
   </Row>
   '''

        # Process the individual analog inputs
        if analogInputDevice is not None:
            analogInputInstances = analogInputDevice.getAllDeviceTypeInstances()
            for instance in analogInputInstances:
                self.processIndividualAnalogIO(instance, "AnalogInput")

        # Write Footer of AI
        self.writeXmlWorksheetFooter("Portrait")

        # Writes the header of the AO worksheet
        self.outputFileContent += '''
<Worksheet ss:Name="AO">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="3" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Address</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Range</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Connection</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Conversion</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>
   </Row>
        '''

        # Process the individual analog outputs
        if analogOutputDevice is not None:
            analogOutputInstances = analogOutputDevice.getAllDeviceTypeInstances()
            for instance in analogOutputInstances:
                self.processIndividualAnalogIO(instance, "AnalogOutput")

        # Write Footer of AO
        self.writeXmlWorksheetFooter("Portrait")

        # Writes the header of the Actuator worksheet
        self.outputFileContent += '''
<Worksheet ss:Name="Actuator">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="3" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="4" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Behaviours</Data></Cell>
        <Cell ss:MergeAcross="0" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Type</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Feedbacks</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Fail Safe</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Limit On (full animation)</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Limit Off(Empty animation)</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Increase Speed (unit/s)</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Decrease Speed (unit/s)</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Test</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>
   </Row>
        '''

        # Process the individual actuators
        if OnOffDevice is not None:
            for instance in OnOffDevice.getAllDeviceTypeInstances():
                self.processIndividualActuator(instance)

        if AnalogDevice is not None:
            for instance in AnalogDevice.getAllDeviceTypeInstances():
                self.processIndividualActuator(instance)

        if AnadigDevice is not None:
            for instance in AnadigDevice.getAllDeviceTypeInstances():
                self.processIndividualActuator(instance)

        if AnaDODevice is not None:
            for instance in AnaDODevice.getAllDeviceTypeInstances():
                self.processIndividualActuator(instance)

        if MFCDevice is not None:
            for instance in MFCDevice.getAllDeviceTypeInstances():
                self.processIndividualActuator(instance)

        # Write Footer of Actuators
        self.writeXmlWorksheetFooter("Portrait")

        # Writes the header of the Parameter worksheet
        self.outputFileContent += '''
<Worksheet ss:Name="Parameters">
    <Table x:FullColumns="1" x:FullRows="1">
     <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
     <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="7" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
     </Row>
     <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Param Name</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Default Value</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Unit</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Range</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Recipe Type</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Master</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Activation</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Commissioning Value</Data></Cell>

    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
     <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

     </Row>
        '''

        # Process the individual parameters
        parameterInstances = self.getDeviceTypeInstancesSorted("DigitalParameter") + \
            self.getDeviceTypeInstancesSorted("WordParameter") + \
            self.getDeviceTypeInstancesSorted("AnalogParameter")

        # retrieve inforamtion where parameteres are used
        parametersUsedIn = self.processParametersUsedIn(parameterInstances, ProjectSources)

        inv = {}
        for k, v in parametersUsedIn.iteritems():
            for i in v:
                keys = inv.setdefault(i, set())
                keys.add(k)
        # for k, v in inv.iteritems():
        #  self.thePlugin.writeWarningInUABLog(str(k) + " -> " + str(v))

        for instance in parameterInstances:
            self.processIndividualParameter(instance, parametersUsedIn)

        # Write Footer of Parameters
        self.writeXmlWorksheetFooter("Portrait")

        # Generate one CCC alarm sheet per project
        self.outputFileContent += '''
<Worksheet ss:Name="CCC">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="3" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Nb of copies</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Alarm copies</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Alarm activation</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">LASER activation</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
        '''

        if DADevice is not None:
            DAInstances = DADevice.getAllDeviceTypeInstances()
            for instance in DAInstances:
                self.processCCCAlarm(instance, DAInstances)

        # Write Footer of alarms
        self.writeXmlWorksheetFooter("Portrait")

        # Generate one Regulation sheet per project
        self.outputFileContent += '''
<Worksheet ss:Name="Regulations">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
    <Cell ss:MergeAcross="7" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="3" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Master</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Measured Value</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Controlled Devices</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Reverse Action</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">State/Step</Data></Cell>
    <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Activation</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Mode</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">SetPoint</Data></Cell>
    <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">controlled Devices/PID</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
    <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
        '''

        # Write PID
        if PIDDevice is not None:
            PIDInstances = PIDDevice.getAllDeviceTypeInstances()
            for instance in PIDInstances:
                self.processLogicPID(instance)

        # Write Footer of regulation sheet
        self.writeXmlWorksheetFooter("Portrait")

#########################################################################################

        # Pre-processing logic files to find inputs/etc

        # Process Alarm Inputs
        alarmInstances = self.java_vector_to_list(DADevice.getAllDeviceTypeInstances()) + \
            self.java_vector_to_list(AADevice.getAllDeviceTypeInstances())
        AAinstances = self.java_vector_to_list(AADevice.getAllDeviceTypeInstances())

        # load csv with alarm data
        inputCSVFilePath = os.path.join(AbsolutePathBuilder.getTechnicalPathParameter("ExpertUserGenerator:OutputParameters:OutputFolder"), self.thePlugin.getApplicationName() + "_FAalarmListOut.csv")
        AFalarmInputs = {}
        AFalarmFolios = {}

        CSValarmInputs = {}
        CSValarmFolios = {}
        if os.path.isfile(inputCSVFilePath):
            inputFile = open(inputCSVFilePath)
            csvReader = csv.reader(inputFile)
            csvReader.next()    # skip header
            #[0] FA name,
            #[1] Source name, (expert name)
            #[2] FA condition,
            #[3] Source condition,
            #[4] FA type,
            #[5] Source type,
            #[6] FA description,
            #[7] Source description,
            #[8] FA section,
            #[9] FA page
            for row in csvReader:
                if row[0]:  # if there is a matched FA name
                    if row[1] not in CSValarmInputs:  # only for first occurrence of alarmExpertName (to avoid MultipleAlarm problem)
                        CSValarmInputs[row[1]] = removeNonAscii(row[2])
                        if len(row) > 8:  # new version with section #s
                            CSValarmFolios[row[1]] = row[8]
                        else:  # old version without section #s
                            CSValarmFolios[row[1]] = ""
            inputFile.close()
        else:
            self.thePlugin.writeWarningInUABLog("File: " + inputCSVFilePath + " doesn't exist.")

        for alarm in alarmInstances:
            alarmExpertName = self.getExpertName(alarm)
            if alarmExpertName in CSValarmInputs:
                AFalarmInputs[alarmExpertName] = CSValarmInputs[alarmExpertName]
                AFalarmFolios[alarmExpertName] = CSValarmFolios[alarmExpertName]
            else:
                AFalarmInputs[alarmExpertName] = ""  # better to put blank instead of "NOT FOUND IN AF"
                #self.thePlugin.writeWarningInUABLog("No alarm condition/folio found in AF for alarm: " + alarmExpertName)
                AFalarmFolios[alarmExpertName] = ""

        # retrieve inputs of alarms
        CodeAlarmInputs = self.processObjectInputs(alarmInstances, "I", ProjectSources)

        #self.thePlugin.writeDebugInUABLog("alarmInputs: " + str(alarmInputs))
        alarmHH = self.processObjectInputs(AAinstances, "HH", ProjectSources)
        alarmAuEHH = self.processObjectInputs(AAinstances, "AuEHH", ProjectSources)
        alarmH = self.processObjectInputs(AAinstances, "H", ProjectSources)
        alarmAuEH = self.processObjectInputs(AAinstances, "AuEH", ProjectSources)
        alarmL = self.processObjectInputs(AAinstances, "L", ProjectSources)
        alarmAuEL = self.processObjectInputs(AAinstances, "AuEL", ProjectSources)
        alarmLL = self.processObjectInputs(AAinstances, "LL", ProjectSources)
        alarmAuELL = self.processObjectInputs(AAinstances, "AuELL", ProjectSources)

        # Process Actuator AuOnR/AuOffR
        actuatorInstances =  self.getDeviceTypeInstances("OnOff") + \
            self.getDeviceTypeInstances("Analog") + \
            self.getDeviceTypeInstances("AnalogDigital") + \
            self.getDeviceTypeInstances("AnaDO") + \
            self.getDeviceTypeInstances("MassFlowController") + \
            self.getDeviceTypeInstances("ProcessControlObject")

        # retrieve AuOnR and AuOffR inputs of actuators
        actuatorAuOnRlogic = self.processObjectInputs(actuatorInstances, "AuOnR", ProjectSources)
        #self.thePlugin.writeDebugInUABLog("actuatorAuOnRlogic = " + str(actuatorAuOnRlogic))
        actuatorAuOffRlogic = self.processObjectInputs(actuatorInstances, "AuOffR", ProjectSources)
        #self.thePlugin.writeDebugInUABLog("actuatorAuOffRlogic = " + str(actuatorAuOffRlogic))

        # Process the individual onoff commands
        cmdeInstances = self.java_vector_to_list(OnOffDevice.getAllDeviceTypeInstances())
        cmdeUsedIn = self.processCmdeUsedIn(cmdeInstances, ProjectSources)

###############################LOGIC SHEETS##############################################

        # Generate 3 sheets per PCO:
        # 1. Process Alarms: $PCO$_AL
        # 2. FSM: $PCO$_FSM
        # 3. Actuator: $PCO$_Actu
        # 4. Parameters: $PCO$_Param
        # 5. Commands: $PCO$_Cmde
        PCOInstances = PCODevice.getAllDeviceTypeInstances()

        grafcetTransitions = {}
        grafcetSteps = {}

        for instance in PCOInstances:
            PCOName = instance.getAttributeData("DeviceIdentification:Name")
            PCODescription = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")

            if PCODescription.lower() not in ("spare", "reserve"):

                # 1. Generate one Process alarms sheet
                self.outputFileContent += '''
<Worksheet ss:Name="$PCOName$_AL">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:Index="3" ss:Width="528.75"/>
   <Column ss:Index="4" ss:Width="260.0"/>
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="252"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
        <Cell ss:MergeAcross="6" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE6"><Data ss:Type="String">Autres infos (for reference only)</Data></Cell>
        <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">AF Condition</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Code Condition (for reference only)</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Type</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Master</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE6"><Data ss:Type="String">Seuil</Data></Cell>
        <Cell ss:StyleID="UABSTYLE6"><Data ss:Type="String">Tempos</Data></Cell>
        <Cell ss:StyleID="UABSTYLE6"><Data ss:Type="String">CCC Alarm</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Alarm activation</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Alarm Action</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
        '''

                # List all masters in this PCO
                masterList = []
                masterList.append(PCOName)
                for child in self.theUnicosProject.findMatchingInstances("OnOff, Analog, AnalogDigital, AnaDO, MassFlowController", PCOName, ""):
                    masterList.append(child.getAttributeData("DeviceIdentification:Name"))

                # Write DA/AA depending on this PCO and its dependent field objects, in order
                for childFieldObjectName in masterList:
                    listAlarms = self.theUnicosProject.findMatchingInstances("AnalogAlarm, DigitalAlarm", childFieldObjectName, "")
                    if listAlarms:
                        # write separator row before each set of alarms per PCO/field object
                        self.writeSeparatorRow(15, childFieldObjectName)
                        types = ["FS", "TS", "SI", "AL"]

                        alarmsSorted = []
                        # consider each type
                        for type in types:
                            tmp = [alarm for alarm in listAlarms if alarm.getAttributeData("FEDeviceAlarm:Type").upper() == type]
                            alarmsSorted.extend(tmp)

                        # consider other
                        tmp = [alarm for alarm in listAlarms if alarm.getAttributeData("FEDeviceAlarm:Type").upper() not in types]
                        alarmsSorted.extend(tmp)

                        # print
                        for alarm in alarmsSorted:
                            alarmName = alarm.getAttributeData("DeviceIdentification:Name")
                            alarmExpertName = self.getExpertName(alarm)
                            self.processLogicAlarm(alarm, AFalarmInputs[alarmExpertName], CodeAlarmInputs[alarmName], alarmHH, alarmH, alarmL, alarmLL, alarmAuEHH, alarmAuEH, alarmAuEL, alarmAuELL, AFalarmFolios[alarmExpertName], self.findCCCtarget(alarmName, ProjectSources, CodeAlarmInputs))

                # Write Footer of alarms
                self.writeXmlWorksheetFooter()

                # Grafcet processing for FSM/actuator sheet
                # grafcetSteps is the dictionary of grafcetName:ListOfPairs(stepName, stepNumber)
                # grafcetTransitions is the dictionary of grafcetName:ListOfTriplets(fromStep, toStep, conditionLabel)

                grafcetName = ""       # grafcet name in case of Siemens/pco in case of Schneider

                # for Siemens:
                # 1. find _SL file for PCO and check if there is call to grafcet
                # 2. if so, extract the grafcet name
                # 3. find the grafcet name in user resources file and check its FB number
                # 4. find the file in sources that corresponds to the FB number
                if self.isSiemens():
                    if PCOName.lower() + "_sl.scl" not in ProjectSources or PCOName.lower() + "_tl.scl" not in ProjectSources:
                        self.thePlugin.writeWarningInUABLog("PCO $PCOName$_SL.SCL or $PCOName$_TL.SCL does not exist in ProjectSources.")
                    else:
                        pattern = re.compile("^(.+?)(?:\.)(.+?)(?:\(\);)", re.IGNORECASE | re.MULTILINE)  # patter looks for expression: GRAFCET_NAME.DB_GRAFCET();
                        matchResults = re.search(pattern, ProjectSources[PCOName.lower() + "_sl.scl"])  # check if sl file contains call to grafcet

                        if not matchResults:
                            self.thePlugin.writeInUABLog("PCO unit $PCOName$ has no call to grafcet in SL file.")
                        # if the call was found, we have grafcet name
                        else:
                            grafcetName = matchResults.group(1).strip()
                            grafcetDB = matchResults.group(2)
                            self.thePlugin.writeInUABLog("PCO unit $PCOName$ has call to grafcet $grafcetName$ in SL file: $grafcetDB$.")

                            # if grafcetName not in grafcetSteps:  # check if the grafcet of name grafcetName wasn't parsed before
                            if True:  # 2016-11-10 fix to force re-parsing grafcet for each PCO to ensure that if the same grafcet is used for multiple PCOs
                                #                it will return different grafcetTransitions[grafcetName] vector
                                # load user resources for grafcet processing

                                # this is based on UAB user resources
                                # theUserResourcesPath    = AbsolutePathBuilder.getApplicationPathParameter("SiemensSpecificParameters:UserResources:FixUserResourcesFile");
                                # theUserResources        = XMLInstancesFacade(theUserResourcesPath)
                                # theUserResourcesVector  = theUserResources.getDeviceType("UserResources").getAllDeviceTypeInstances()
                                # check what is the FB address of the found grafcet
                                # grafcetResource = find(lambda resource:resource.getAttributeData("UserResources:Symbol") == grafcetName, theUserResourcesVector)
                                # grafcetFB = grafcetResource.getAttributeData("UserResources:Address")

                                # this is based on UAB user resources

                                pattern = re.compile("^\"$grafcetName$\s*\",\"FB\s*(.+?)\s*\"", re.IGNORECASE | re.MULTILINE)        # patter looks for expression: GRAFCET_NAME.DB_GRAFCET();
                                symbolFile = self.find(lambda filename: filename.endswith(".sdf"), ProjectSources)

                                if not symbolFile:
                                    self.thePlugin.writeWarningInUABLog("No symbol (.sdf) file in ProjectSources folder, please export symbols to *exported_src folder")
                                else:
                                    #grafcetFB = re.search(pattern, ProjectSources[symbolFile]).group(1)
                                    grafcetFB = self.getFBofDBStepper(grafcetDB, ProjectSources[symbolFile])
                                    if grafcetFB == "":
                                        self.thePlugin.writeWarningInUABLog("No grafcetFB found in symbolFile for $grafcetDB$.")
                                    else:
                                        self.thePlugin.writeInUABLog("grafcet $grafcetName$ FB: $grafcetFB$")

                                        # find the source file that corresponds to the found FB number
                                        pattern = re.compile("^FUNCTION_BLOCK\s*(FB$grafcetFB$|\"$grafcetName$\")", re.IGNORECASE | re.MULTILINE)
                                        GrafcetSourceFileName = self.find(lambda fname: re.search(pattern, ProjectSources[fname]), ProjectSources)

                                        if GrafcetSourceFileName is None:
                                            self.thePlugin.writeWarningInUABLog("No source file found for grafcet $grafcetName$.")
                                        else:
                                            # parse grafcet
                                            (grafcetSteps[grafcetName], grafcetTransitions[grafcetName]) = self.parseGrafcet(grafcetName, ProjectSources[PCOName.lower() + "_tl.scl"], ProjectSources[GrafcetSourceFileName])

                            if grafcetName in grafcetSteps:  # check if the grafcet of name grafcetName was correctly parsed
                                graphvizPath = AbsolutePathBuilder.getTechnicalPathParameter(self.thePlugin.getId() + ":OutputParameters:OutputFolder") + "graphviz"
                                if not os.path.exists(graphvizPath):
                                    os.makedirs(graphvizPath)
                                    self.thePlugin.writeInUABLog("Created missing directory:" + graphvizPath)

                                graphfileBasename = os.path.join(graphvizPath, PCOName)

                                # create dotfile in a nice format
                                dotFile = self.exportGrafcetToGraphviz(grafcetSteps[grafcetName], grafcetTransitions[grafcetName])

                                # save .dot with nice graph
                                dotOK = 0
                                try:
                                    f = open(graphfileBasename + ".dot", "w")
                                    f.write(dotFile)
                                    f.close()
                                    dotOK = 1
                                except:
                                    self.thePlugin.writeWarningInUABLog("Error handling the file: " + graphfileBasename + ".dot")

                                # save .dot with layout for pvss .xml
                                simpleDotTmpOK = 0
                                if dotOK == 1:
                                    try:
                                        f = open(graphfileBasename + "_simple.dot.tmp", "w")
                                        f.write(dotFile.replace("shape=Mrecord", "shape=point, width=0"))
                                        f.close()
                                        simpleDotTmpOK = 1
                                    except:
                                        self.thePlugin.writeWarningInUABLog("Error handling the file: " + graphfileBasename + "_simple.dot.tmp")

                                # generate dot files
                                dotExecOK = 0
                                if simpleDotTmpOK == 1:
                                    try:
                                        # generate SVG
                                        process = Runtime.getRuntime().exec('dot -Tsvg "$graphfileBasename$.dot" -o "$graphfileBasename$.svg"')
                                        process.waitFor()   # wait until process is finished
                                        # generate simple dot with positions for pvss .xml
                                        process = Runtime.getRuntime().exec('dot -Tplain "$graphfileBasename$_simple.dot.tmp" -o "$graphfileBasename$_simple.dot"')
                                        process.waitFor()   # wait until process is finished
                                        os.remove(graphfileBasename + "_simple.dot.tmp")
                                        dotExecOK = 1
                                    except:
                                        self.thePlugin.writeWarningInUABLog("Graphviz .dot processing error:" + str(sys.exc_info()))

                                # generate xml based on .dot with layout
                                if dotExecOK == 1:
                                    try:
                                        f = open(graphfileBasename + "_simple.dot", "r")
                                        headerText = self.thePlugin.getApplicationName() + " " + PCOName + " Stepper"
                                        outputXmlFile = self.exportGrafcetToXml(f, PCOName, ProjectSources[PCOName.lower() + "_tl.scl"], grafcetDB, grafcetSteps[grafcetName], grafcetTransitions[grafcetName], self.PVSSSystemName, headerText, self.PVSSAddTransitionLabels, self.panelW, self.panelH)
                                        f.close()

                                        f = open(graphfileBasename + "_Stepper.xml", "w")
                                        f.write(outputXmlFile)
                                        f.close()
                                    except Exception, e:
                                        self.thePlugin.writeWarningInUABLog("Stepper to xml error: " + str(e))

                elif self.isSchneider():
                    grafcetName = instance.getAttributeData("DeviceIdentification:Name")
                    if grafcetName not in grafcetSteps:  # check if the grafcet of name grafcetName wasn't parsed before...
                        (grafcetSteps[grafcetName], grafcetTransitions[grafcetName]) = self.parseGrafcet(grafcetName)
                        graphvizPath = AbsolutePathBuilder.getTechnicalPathParameter(self.thePlugin.getId() + ":OutputParameters:OutputFolder") + "graphviz"
                        if not os.path.exists(graphvizPath):
                            os.makedirs(graphvizPath)

                        graphfileBasename = os.path.join(graphvizPath, PCOName)

                        if (grafcetName in grafcetSteps) and (len(grafcetSteps[grafcetName]) > 0):  # check if the grafcet of name grafcetName was correctly parsed
                            # create dotfile in a nice format
                            dotFile = self.exportGrafcetToGraphviz(grafcetSteps[grafcetName], grafcetTransitions[grafcetName])

                            # save .dot with nice graph
                            dotOK = 0
                            try:
                                f = open(graphfileBasename + ".dot", "w")
                                f.write(dotFile)
                                f.close()
                                dotOK = 1
                            except:
                                self.thePlugin.writeWarningInUABLog("Error handling the file: " + graphfileBasename + ".dot")

                            # save .dot with layout for pvss .xml
                            simpleDotTmpOK = 0
                            if dotOK == 1:
                                try:
                                    f = open(graphfileBasename + "_simple.dot.tmp", "w")
                                    f.write(dotFile.replace("shape=Mrecord", "shape=point, width=0"))
                                    f.close()
                                    simpleDotTmpOK = 1
                                except:
                                    self.thePlugin.writeWarningInUABLog("Error handling the file: " + graphfileBasename + "_simple.dot.tmp")

                            # generate dot files
                            dotExecOK = 0
                            if simpleDotTmpOK == 1:
                                try:
                                    # generate SVG
                                    process = Runtime.getRuntime().exec('dot -Tsvg "$graphfileBasename$.dot" -o "$graphfileBasename$.svg"')
                                    process.waitFor()   # wait until process is finished
                                    # generate simple dot with positions for pvss .xml
                                    process = Runtime.getRuntime().exec('dot -Tplain "$graphfileBasename$_simple.dot.tmp" -o "$graphfileBasename$_simple.dot"')
                                    process.waitFor()   # wait until process is finished
                                    os.remove(graphfileBasename + "_simple.dot.tmp")
                                    dotExecOK = 1
                                except:
                                    self.thePlugin.writeWarningInUABLog("Graphviz .dot processing error:" + str(sys.exc_info()))

                            # generate xml based on .dot with layout
                            if dotExecOK == 1:
                                try:
                                    f = open(graphfileBasename + "_simple.dot", "r")
                                    headerText = self.thePlugin.getApplicationName() + " " + PCOName + " Stepper"
                                    outputXmlFile = self.exportGrafcetToXml(f, PCOName, ProjectSources[PCOName + "_TL"], "", grafcetSteps[grafcetName], grafcetTransitions[grafcetName], self.PVSSSystemName, headerText, self.PVSSAddTransitionLabels, self.panelW, self.panelH)
                                    f.close()

                                    f = open(graphfileBasename + "_Stepper.xml", "w")
                                    f.write(outputXmlFile)
                                    f.close()
                                except Exception, e:
                                    self.thePlugin.writeWarningInUABLog("Stepper to xml error: " + str(e))

                # or even here, common for Siemens & schneider
                # 2. Generate one FSM sheet

                self.outputFileContent += '''
<Worksheet ss:Name="$PCOName$_FSM">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:Index="3" ss:Width="528.75"/>
   <Column ss:Index="4" ss:Width="260.0"/>
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
        <Cell ss:MergeAcross="4" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Start</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">End</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">AF Transition Condition</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Logic (for reference only)</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Transition</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Sequence</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>'''

                # add all the transitions to the FSM sheet
                if grafcetName in grafcetTransitions:

                    for (fromPosition, toPosition, conditionLabel, conditionLogic, jump, priority) in grafcetTransitions[grafcetName]:
                        # directory of stepName:stepNumber; just to ease use of data
                        stepsNumbers = dict(grafcetSteps[grafcetName])
                        self.outputFileContent += '''
   <Row ss:AutoFitHeight="0">
        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$fromPosition$ '''
                        if stepsNumbers[fromPosition.lower()]:
                            self.outputFileContent += '''($stepsNumbers[fromPosition.lower()]$)'''
                        self.outputFileContent += '''</Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$toPosition$ '''
                        if stepsNumbers[toPosition.lower()]:
                            self.outputFileContent += '''($stepsNumbers[toPosition.lower()]$)'''
                        self.outputFileContent += '''</Data></Cell>
        <Cell ss:StyleID="allBordersWrap"><Data ss:Type="String"></Data></Cell>
        <Cell ss:StyleID="allBordersWrap"><Data ss:Type="String">$conditionLogic$</Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>

        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>

        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>
         <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>

   </Row>'''

                self.writeXmlRow(["", "", "", "", "", "", "", "", "", ""])

                self.writeXmlWorksheetFooter()

            # 3. Generate one actuator sheet
                self.outputFileContent += '''
<Worksheet ss:Name="$PCOName$_Actu">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:Index="4" ss:Width="500"/>
   <Column ss:Index="5" ss:Width="250"/>
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
        <Cell ss:MergeAcross="6" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Description</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Type</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">AF Requirement</Data></Cell>
        <Cell ss:StyleID="UABSTYLE2"><Data ss:Type="String">Code (for reference only)</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">State/Step</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Activation</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Value</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
        '''

                # Write Actuators depending of this PCO
                if grafcetName in grafcetSteps:
                    for (position, number) in grafcetSteps[grafcetName]:
                        step = position
                        if (number):
                            step = step + " ($number$)"
                        self.writeSeparatorRow(12, step)
                        for child in self.theUnicosProject.findMatchingInstances("ProcessControlObject, OnOff, Analog, AnalogDigital, AnaDO, MassFlowController", PCOName, ""):
                            self.processLogicActuator(child, actuatorAuOnRlogic, actuatorAuOffRlogic, step)
                else:
                    for child in self.theUnicosProject.findMatchingInstances("ProcessControlObject, OnOff, Analog, AnalogDigital, AnaDO, MassFlowController", PCOName, ""):
                        self.processLogicActuator(child, actuatorAuOnRlogic, actuatorAuOffRlogic, "")
                # Write Footer of logic actuator
                self.writeXmlWorksheetFooter()

            # 3. Generate one Param sheet per PCO
                self.outputFileContent += '''
<Worksheet ss:Name="$PCOName$_Param">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
        <Cell ss:MergeAcross="7" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Param Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Description</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Default Value</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Unit</Data></Cell>
<Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Range</Data></Cell>
<Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Recipe Type</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Master</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Activation</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Commissioning Value</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
        '''

                # Write Parameters depending on this PCO
                for instance in parameterInstances:
                    self.processLogicParameter(instance, PCOName, parametersUsedIn, masterList)

                # self.writeSeparatorRow(10,"test new")

                # for i in sorted(set([inv[x] for x in masterList if inv.get(x) is not None])):
                #     self.writeSeparatorRow(10,str(i))
                # for x in masterList:
                #   #self.thePlugin.writeWarningInUABLog("x = " + str(x))
                #   self.writeSeparatorRow(10,str(x))
                #   if inv.get(x) is not None:
                #     for i in inv[x]:
                #       #self.thePlugin.writeWarningInUABLog("i = " + str(i))
                #       #self.processIndividualParameter(i, parametersUsedIn)
                #       self.writeSeparatorRow(10,str(i))

                self.writeXmlWorksheetFooter()

            # 4. Generate one Cmde sheet per PCO
                self.outputFileContent += '''
<Worksheet ss:Name="$PCOName$_cmde">
  <Table x:FullColumns="1" x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="90" ss:Span="255"/>
   <Row ss:AutoFitHeight="0" ss:Index="8">
        <Cell ss:MergeAcross="3" ss:StyleID="UABSTYLE2"><Data ss:Type="String">Information</Data></Cell>
        <Cell ss:MergeAcross="1" ss:StyleID="UABSTYLE3"><Data ss:Type="String">Results</Data></Cell>
        <Cell ss:MergeAcross="2" ss:StyleID="UABSTYLE4"><Data ss:Type="String">Info</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0">
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Name</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Description</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Master Type</Data></Cell>
        <Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">Folio</Data></Cell>

        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Availability</Data></Cell>
        <Cell ss:StyleID="UABSTYLE3"><Data ss:Type="String">Action</Data></Cell>

        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Date</Data></Cell>
        <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Responsible</Data></Cell>
         <Cell ss:StyleID="UABSTYLE4"><Data ss:Type="String">Remarks</Data></Cell>

   </Row>
   '''

                # Write OnOff Commands depending on this PCO
                if OnOffDevice is not None:
                    for instance in OnOffDevice.getAllDeviceTypeInstances():
                        self.processLogicCmde(instance, PCOName, cmdeUsedIn)

                self.writeXmlWorksheetFooter()

        # Writes the footer of the workbook
        self.outputFileContent += '''
        </Workbook>'''

        if self.isSchneider() or self.isCodesys():
            self.thePlugin.writeCommissioningInfo(self.outputFileContent)
        else:
            self.thePlugin.setOutputFormat("IOCommissioning.xml", FileOutputFormat.XML, "Windows-1252")
            self.thePlugin.writeInstanceInfo("IOCommissioning.xml", self.outputFileContent)

    def getDeviceTypeInstances(self, typeName):
        deviceType = self.theUnicosProject.getDeviceType(typeName)
        if deviceType is not None:
            return self.java_vector_to_list(deviceType.getAllDeviceTypeInstances())
        else:
            return []

    def getDeviceTypeInstancesSorted(self, typeName):
        deviceType = self.theUnicosProject.getDeviceType(typeName)
        if deviceType is not None:
            return sorted(self.java_vector_to_list(deviceType.getAllDeviceTypeInstances()), key=lambda instance: instance.getAttributeData("DeviceIdentification:Name"))
        else:
            return []

    def processIndividualDigitalIO(self, instance, deviceType):

        expertName = self.getExpertName(instance)
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        diagram = instance.getAttributeData("DeviceDocumentation:Electrical Diagram").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")

        # Writes the information in the buffer
        self.writeXmlRow([expertName, description, diagram, "", "", "", "", ""])

    def processIndividualAnalogIO(self, instance, deviceType):

        expertName = self.getExpertName(instance)
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        diagram = instance.getAttributeData("DeviceDocumentation:Electrical Diagram").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        rangeMin = instance.getAttributeData("FEDeviceParameters:Range Min")
        rangeMax = instance.getAttributeData("FEDeviceParameters:Range Max")
        rangeUnit = instance.getAttributeData("SCADADeviceGraphics:Unit")
        rangeStr = "[ " + rangeMin + " - " + rangeMax + " ] " + rangeUnit

        # Writes the information in the buffer
        self.writeXmlRow([expertName, description, diagram, rangeStr, "", "", "", "", ""])

    def processIndividualActuator(self, instance):

        expertName = self.getExpertName(instance)
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        deviceType = self.objectTypeShortNameDict[instance.getDeviceTypeName()]
        if deviceType == "MFC":
            FOn = ""
        else:
            FOn = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On")
        FailSafe = instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe")
        Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature").lower()

        if deviceType in ["AnaDO", "MFC"]:
            FOff = ""
        else:
            FOff = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off")

        if deviceType == "OnOff":
            FAnalog = ""
            LimitOn = ""
            LimitOff = ""
            InSpd = ""
            DeSpd = ""
        elif deviceType == "MFC":
            FAnalog = ""
            LimitOn = ""
            LimitOff = ""
            InSpd = instance.getAttributeData("FEDeviceParameters:Manual Increase Speed (Unit/s)")
            DeSpd = instance.getAttributeData("FEDeviceParameters:Manual Decrease Speed (Unit/s)")
        else:
            FAnalog = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Analog")
            LimitOn = instance.getAttributeData("FEDeviceManualRequests:Parameter Limit On/Open")
            LimitOff = instance.getAttributeData("FEDeviceManualRequests:Parameter Limit Off/Closed")
            InSpd = instance.getAttributeData("FEDeviceParameters:Manual Increase Speed (Unit/s)")
            DeSpd = instance.getAttributeData("FEDeviceParameters:Manual Decrease Speed (Unit/s)")

        Feedbacks = ""
        if FOn != "":
            Feedbacks = FOn + ", "

        if FOff != "":
            Feedbacks = Feedbacks + FOff + ", "

        if FAnalog != "":
            Feedbacks = Feedbacks + FAnalog

        # Writes the information in the buffer
        if description.lower() not in ("spare", "reserve") and (Nature <> "bouton" or Nature <> "button"):
            self.writeXmlRow([expertName, deviceType, description, Feedbacks, FailSafe, LimitOn, LimitOff, InSpd, DeSpd, "", "", "", ""])

    def processParametersUsedIn(self, parameterInstances, ProjectSources):
        parametersUsedIn = {}

        listOfNotSpares = []
        for instance in parameterInstances:
            instanceName = instance.getAttributeData("DeviceIdentification:Name")
            description = instance.getAttributeData("DeviceDocumentation:Description").strip()
            if description.lower() not in ("spare", "reserve"):
                listOfNotSpares.append(instanceName)    # add parameters to check
            else:
                parametersUsedIn[instanceName] = [] 	# spares are ignored, add them at once

        parametersUsedIn.update(self.multiFindUsedIn(listOfNotSpares, ProjectSources))  # look for notSPARE parameters in logic
        return parametersUsedIn

    def processIndividualParameter(self, instance, parametersUsedIn):

        instanceName = instance.getAttributeData("DeviceIdentification:Name")
        expertName = self.getExpertName(instance)
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        deviceType = self.objectTypeShortNameDict[instance.getDeviceTypeName()]
        if description.strip().lower() not in ("spare", "reserve"):
            defaultValue = instance.getAttributeData("FEDeviceParameters:Default Value")
            pcoNames = ",".join(sorted(parametersUsedIn[instanceName]))
            if deviceType != "DPAR":
                unit = instance.getAttributeData("SCADADeviceGraphics:Unit")
                rangeMin = instance.getAttributeData("FEDeviceParameters:Range Min")
                rangeMax = instance.getAttributeData("FEDeviceParameters:Range Max")
                rangeStr = "[" + rangeMin + " : " + rangeMax + "]"
            else:
                unit = ""
                rangeStr = ""

            # Writes the information in the buffer
            self.writeXmlRow([expertName, description, defaultValue, unit, rangeStr, "", pcoNames, "", "", "", "", "", ""])

    def processCmdeUsedIn(self, cmdeInstances, ProjectSources):
        cmdeUsedIn = {}

        listOfNotSpares = []
        for instance in cmdeInstances:
            instanceName = instance.getAttributeData("DeviceIdentification:Name")
            description = instance.getAttributeData("DeviceDocumentation:Description").strip()
            master = instance.getAttributeData("LogicDeviceDefinitions:Master").strip()
            if description.lower() not in ("spare", "reserve") and master == "":
                listOfNotSpares.append(instanceName)    # collect all the instances to analyse
            else:
                cmdeUsedIn[instanceName] = []           # skip spares, assign empty list

        cmdeUsedIn.update(self.multiFindUsedIn(listOfNotSpares, ProjectSources))
        # make sure does not include instanceName
        for instance in cmdeInstances:
            instanceName = instance.getAttributeData("DeviceIdentification:Name")
            if instanceName in cmdeUsedIn[instanceName]:
                cmdeUsedIn[instanceName].remove(instanceName)

        return cmdeUsedIn

    def processLogicAlarm(self, instance, AFalarmInput, CodeAlarmInput, alarmHH, alarmH, alarmL, alarmLL, alarmAuEHH, alarmAuEH, alarmAuEL, alarmAuELL, alarmFolio, alarmCCC):
        instanceName = instance.getAttributeData("DeviceIdentification:Name")
        expertName = self.getExpertName(instance)
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        Type = instance.getAttributeData("FEDeviceAlarm:Type")
        MultipleType = instance.getAttributeData("FEDeviceAlarm:Multiple Types")
        AuAck = instance.getAttributeData("FEDeviceAlarm:Auto Acknowledge")
        Master = instance.getAttributeData("LogicDeviceDefinitions:Master")
        Delay = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
        AFalarmInputString = self.unDecorateExpression(AFalarmInput.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))
        CodeAlarmInputString = self.unDecorateExpression(CodeAlarmInput.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))
        if alarmCCC:
            alarmCCC = self.getExpertNameFromName(alarmCCC)
        deviceTypeName = instance.getDeviceTypeName()
        if Type.lower() == "multiple":
            Type = Type + ": " + MultipleType

        if AuAck.lower().strip() == "true":
            AuAlAckString = "; (NoAck)"
        else:
            AuAlAckString = ""

        HHstring = ""
        Hstring = ""
        Lstring = ""
        LLstring = ""
        enableString = ""
        if deviceTypeName == "AnalogAlarm":
            HHAA = instance.getAttributeData("FEDeviceManualRequests:HH Alarm")
            HWAA = instance.getAttributeData("FEDeviceManualRequests:H Warning")
            LWAA = instance.getAttributeData("FEDeviceManualRequests:L Warning")
            LLAA = instance.getAttributeData("FEDeviceManualRequests:LL Alarm")
            if HHAA != "" and not self.thePlugin.isString(HHAA):  # numeric value
                HHstring = "; HH = " + HHAA
            elif HHAA != "" and self.thePlugin.isString(HHAA):  # logic
                HHstring = "; HH = " + self.unDecorateExpression(alarmHH[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))
            if HWAA != "" and not self.thePlugin.isString(HWAA):  # numeric value
                Hstring = "; H = " + HWAA
            elif HWAA != "" and self.thePlugin.isString(HWAA):  # logic
                Hstring = "; H = " + self.unDecorateExpression(alarmH[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))
            if LWAA != "" and not self.thePlugin.isString(LWAA):  # numeric value
                Lstring = "; L = " + LWAA
            elif LWAA != "" and self.thePlugin.isString(LWAA):  # logic
                Lstring = "; L = " + self.unDecorateExpression(alarmL[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))
            if LLAA != "" and not self.thePlugin.isString(LLAA):  # numeric value
                LLstring = "; LL = " + LLAA
            elif LLAA != "" and self.thePlugin.isString(LLAA):  # logic
                LLstring = "; LL = " + self.unDecorateExpression(alarmLL[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))

            # Check if enable logic is unique
            enableLogic = set()
            if alarmAuEHH[instanceName] != "":
                enableLogic.add(alarmAuEHH[instanceName])
            if alarmAuEH[instanceName] != "":
                enableLogic.add(alarmAuEH[instanceName])
            if alarmAuEL[instanceName] != "":
                enableLogic.add(alarmAuEL[instanceName])
            if alarmAuELL[instanceName] != "":
                enableLogic.add(alarmAuELL[instanceName])
            # if unique, write condition only once
            if len(enableLogic) == 1:
                enableString += "; AuE** = " + self.unDecorateExpression(enableLogic.pop().replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))
            else:
                if HHAA != "":
                    enableString += "; AuEHH = " + self.unDecorateExpression(alarmAuEHH[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))
                if HWAA != "":
                    enableString += "; AuEH = " + self.unDecorateExpression(alarmAuEH[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))
                if LWAA != "":
                    enableString += "; AuEL = " + self.unDecorateExpression(alarmAuEL[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))
                if LLAA != "":
                    enableString += "; AuELL = " + self.unDecorateExpression(alarmAuELL[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))

        if Master <> "":
            # self.writeXmlRow([instanceName,description,alarmInputString,Type,Master,"","","","","",""])
            self.outputFileContent += '''
   <Row>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$expertName$</Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$description$</Data></Cell>
        <Cell ss:StyleID="allBordersWrap"><Data ss:Type="String">$AFalarmInputString$</Data></Cell>
        <Cell ss:StyleID="allBordersWrap"><Data ss:Type="String">$CodeAlarmInputString$$HHstring$$Hstring$$Lstring$$LLstring$$enableString$</Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$Type$$AuAlAckString$</Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$Master$</Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$alarmFolio$</Data></Cell>

        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$HHstring$$Hstring$$Lstring$$LLstring$</Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$Delay$</Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$alarmCCC$</Data></Cell>

        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>

        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>

   </Row>'''

    def processObjectInputs(self, objectInstances, inputPin, ProjectSources):
        objectInput = {}

        listOfNotSpares = []
        for instance in objectInstances:
            instanceName = instance.getAttributeData("DeviceIdentification:Name")
            description = instance.getAttributeData("DeviceDocumentation:Description").strip()
            if description.lower() not in ("spare", "reserve"):
                listOfNotSpares.append(instanceName)  # add objects to check list
            else:
                objectInput[instanceName] = ""			# assign empty lists for spares

        objectInput.update(self.multiFindGivenInput(listOfNotSpares, inputPin, ProjectSources))

        return objectInput

    def processPCOSummary(self, instance):
        expertName = self.getExpertName(instance)
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")

        # Writes the information in the buffer
        if description.lower() not in ("spare", "reserve"):
            self.writeXmlRow([expertName, description, "", "", "", "", "", "", "", ""])

    def processCCCAlarm(self, instance, DAInstances):
        instanceName = instance.getAttributeData("DeviceIdentification:Name")
        expertName = self.getExpertName(instance)
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        Master = instance.getAttributeData("LogicDeviceDefinitions:Master")
        Param1 = instance.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter1")

        # Writes the information in the buffer
        if Param1 == "CCC":
            # count the number of copies plugged on this CCC alarm
            NbCopies = 0
            copyList = []
            for DAInst in DAInstances:
                Param1 = DAInst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter1")
                Param2 = DAInst.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter2")
                copyExpertName = self.getExpertName(DAInst)
                copyDesc = DAInst.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
                if Param1 == "copy" and Param2 == instanceName:
                    NbCopies = NbCopies + 1
                    copyList.append(copyExpertName + " : " + copyDesc)

            self.writeXmlRow([expertName, "Nb of copies: " + str(NbCopies) + "; " + "; ".join(copyList), description, "", "", "", "", "", "", ""])
            # for copy in copyList:
            #    self.writeXmlRow(["",copy,"","","","","","","",""])

    def processLogicPID(self, instance):
        expertName = self.getExpertName(instance)
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        Master = instance.getAttributeData("LogicDeviceDefinitions:Master")
        MV = instance.getAttributeData("FEDeviceEnvironmentInputs:Measured Value")
        ControlledDevice = instance.getAttributeData("FEDeviceOutputs:Controlled Objects")
        RA = instance.getAttributeData("FEDeviceParameters:Controller Parameters:RA")

        # Writes the information in the buffer
        if description.lower() not in ("spare", "reserve"):
            self.writeXmlRow([expertName, description, Master, MV, ControlledDevice, RA, "", "", "", "", "", "", "", "", ""])

    def processLogicActuator(self, instance, auOnRlogic, auOffRlogic, step):
        instanceName = instance.getAttributeData("DeviceIdentification:Name")
        expertName = self.getExpertName(instance)
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature").lower()
        deviceType = self.objectTypeShortNameDict[instance.getDeviceTypeName()]
        auOnRlogicString = self.unDecorateExpression(auOnRlogic[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))
        auOffRlogicString = self.unDecorateExpression(auOffRlogic[instanceName].replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;"))
        # Writes the information in the buffer
        if (nature <> "bouton" or nature <> "button"):
            #self.writeXmlRow([instanceName,description,deviceType,"AuOnR := " + auOnRlogicString,"AuOffR := " + auOffRlogicString,"","","","","","",""])
            self.outputFileContent += '''
   <Row>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$expertName$</Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$description$</Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$deviceType$</Data></Cell>
        <Cell ss:StyleID="allBordersWrap"><Data ss:Type="String"></Data></Cell>
        <Cell ss:StyleID="allBordersWrap"><Data ss:Type="String">AuOnR := $auOnRlogicString$; AuOffR := $auOffRlogicString$</Data></Cell>

        <Cell ss:StyleID="allBorders"><Data ss:Type="String">$step$</Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>
        <Cell ss:StyleID="allBorders"><Data ss:Type="String"></Data></Cell>

   </Row>'''

    def processLogicParameter(self, instance, PCOName, parametersUsedIn, masterList):
        instanceName = instance.getAttributeData("DeviceIdentification:Name")
        expertName = self.getExpertName(instance)
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        deviceType = self.objectTypeShortNameDict[instance.getDeviceTypeName()]
        pcoNames = parametersUsedIn[instanceName]
        if description.strip().lower() not in ("spare", "reserve") and (set(masterList) & set(pcoNames)):
            defaultValue = instance.getAttributeData("FEDeviceParameters:Default Value")
            if deviceType != "DPAR":
                unit = instance.getAttributeData("SCADADeviceGraphics:Unit")
                rangeMin = instance.getAttributeData("FEDeviceParameters:Range Min")
                rangeMax = instance.getAttributeData("FEDeviceParameters:Range Max")
                rangeStr = "[" + rangeMin + " : " + rangeMax + "]"
            else:
                unit = ""
                rangeStr = ""
            #pcoNames        = self.findUsedIn(instance)
            pcoNamesString = ",".join(sorted(pcoNames))
            # Writes the information in the buffer
            self.writeXmlRow([expertName, description, defaultValue, unit, rangeStr, "", pcoNamesString, "", "", "", "", "", ""])

    def processLogicCmde(self, instance, PCOName, cmdeUsedIn):
        instanceName = instance.getAttributeData("DeviceIdentification:Name")
        expertName = self.getExpertName(instance)
        description = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
        pcoNames = cmdeUsedIn[instanceName]
        if description.strip().lower() not in ("spare", "reserve") and PCOName in pcoNames:
            pcoNamesString = ",".join(sorted(pcoNames))
            # Writes the information in the buffer
            self.writeXmlRow([expertName, description, pcoNamesString, "", "", "", "", "", ""])

    def getExpertName(self, instance):
        """
        This function returns the expert name (if available) from a given instance.
        If the expert name is not defined, then will return the object name.

        The arguments are:
        @param instance: object instance

        This function returns:
        @return: name: the expert name (or name) of the object for use in WinCCOA db file
        """

        if instance.getAttributeData("DeviceIdentification:Expert Name") != "":
            return instance.getAttributeData("DeviceIdentification:Expert Name")
        else:
            return instance.getAttributeData("DeviceIdentification:Name")

    def writeSeparatorRow(self, n=1, text=""):
        self.outputFileContent += ('<Row ss:AutoFitHeight="0">' +
                                   '<Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String">' + text + '</Data></Cell>' +
                                   '<Cell ss:StyleID="UABSTYLE5"><Data ss:Type="String"></Data></Cell>' * (n - 1) + '</Row>')

    def writeXmlRow(self, dataList):
        xmlString = '''<Row>'''
        for cellData in dataList:
            xmlString += '''<Cell ss:StyleID="allBorders"><Data ss:Type="String">''' + cellData + '''</Data></Cell>'''
        xmlString += '''</Row>'''
        self.outputFileContent += xmlString

    def writeXmlWorksheetFooter(self, orientation="Landscape"):
        if orientation not in ("Portrait", "Landscape"):
            orientation = "Landscape"  # default to Landscape
        xmlString = '''
          </Table>
           <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
                   <PageSetup>
                     <Layout x:Orientation="''' + orientation + '''"/>
                   </PageSetup>
                   <FitToPage/>
                   <Print>
                        <FitHeight>50</FitHeight>
                        <ValidPrinterInfo/>
                        <PaperSizeIndex>9</PaperSizeIndex>
                        <HorizontalResolution>600</HorizontalResolution>
                        <VerticalResolution>600</VerticalResolution>
                   </Print>
                   <TabColorIndex>40</TabColorIndex>
                   <!--<ShowPageBreakZoom/>-->
                   <Zoom>75</Zoom>
                   <Selected/>
                   <Panes>
                        <Pane>
                         <Number>3</Number>
                         <ActiveRow>22</ActiveRow>
                         <ActiveCol>5</ActiveCol>
                        </Pane>
                   </Panes>
                   <ProtectObjects>False</ProtectObjects>
                   <ProtectScenarios>False</ProtectScenarios>
                </WorksheetOptions>
                 </Worksheet>
        '''
        self.outputFileContent += xmlString

    def end(self):
        self.thePlugin.writeInUABLog("IO Commissioning: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("IO Commissioning: shutdown")
