# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogParameter Objects.

import TIAPortal_Generic_Template
reload(TIAPortal_Generic_Template)


class AnalogParameter_Template(TIAPortal_Generic_Template.Generic_Template):
    theDeviceType = "AnalogParameter"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'objName', 'WString')
        self.writeScriptTag(instance, 'description', 'WString')
        self.writeScriptTag(instance, 'unit', 'WString')
        self.writeScriptTag(instance, 'setValue', 'Bool')
        self.writeScriptTag(instance, 'ManReg01', self.integer)
        self.writeScriptTag(instance, 'PosSt', self.real)
        self.writeScriptTag(instance, 'MPosR', self.real)
        self.writeScriptTag(instance, 'MPosRSt', self.real)

        self.writeSmartTagName(instance)
        self.writeSmartTagDescription(instance)
        self.writeSmartTagUnit(instance)
