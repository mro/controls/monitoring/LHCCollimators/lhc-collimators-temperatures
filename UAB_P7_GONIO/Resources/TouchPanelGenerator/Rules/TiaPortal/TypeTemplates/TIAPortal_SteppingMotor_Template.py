# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Stepping Motor Objects.

import TIAPortal_Generic_Template
reload(TIAPortal_Generic_Template)


class SteppingMotor_Template(TIAPortal_Generic_Template.Generic_Template):
    theDeviceType = "SteppingMotor"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'objName', 'WString')
        self.writeScriptTag(instance, 'unit', 'WString')
        self.writeScriptTag(instance, 'setValue', 'Bool')
        self.writeScriptTag(instance, 'setSpeed', 'Bool')
        self.writeScriptTag(instance, 'StsReg01', self.integer)
        self.writeScriptTag(instance, 'StsReg02', self.integer)
        self.writeScriptTag(instance, 'ManReg01', self.integer)
        self.writeScriptTag(instance, 'PosSt', self.real)
        self.writeScriptTag(instance, 'SpdSt', self.integer)
        self.writeScriptTag(instance, 'PosRSt', self.real)
        self.writeScriptTag(instance, 'MPosR', self.real)
        self.writeScriptTag(instance, 'MSpdR', self.integer)

        self.writeSmartTagName(instance)
        self.writeSmartTagUnit(instance)
