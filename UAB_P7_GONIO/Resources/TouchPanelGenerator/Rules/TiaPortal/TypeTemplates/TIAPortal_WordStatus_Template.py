# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for WordStatus Objects.

import TIAPortal_Generic_Template
reload(TIAPortal_Generic_Template)


class WordStatus_Template(TIAPortal_Generic_Template.Generic_Template):
    theDeviceType = "WordStatus"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'unit', 'WString')
        self.writeScriptTag(instance, 'PosSt', self.integer)

        self.writeSmartTagUnit(instance)
        self.writeTextListPattern(instance)
