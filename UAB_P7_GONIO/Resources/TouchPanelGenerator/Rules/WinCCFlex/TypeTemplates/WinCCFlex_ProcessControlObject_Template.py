# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for PCO Objects.

import WinCCFlex_Generic_Template
reload(WinCCFlex_Generic_Template)


class ProcessControlObject_Template(WinCCFlex_Generic_Template.Generic_Template):
    theDeviceType = "ProcessControlObject"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'name', 'String')
        self.writeScriptTag(instance, 'displayName', 'String')
        self.writeScriptTag(instance, 'setOpMo', 'Bool')
        self.writeScriptTag(instance, 'StsReg01', self.integer)
        self.writeScriptTag(instance, 'StsReg02', self.integer)
        self.writeScriptTag(instance, 'ManReg01', self.integer)
        self.writeScriptTag(instance, 'OpMoSt', self.real)
        self.writeScriptTag(instance, 'MOpMoR', self.real)
        self.writeScriptTag(instance, 'AuOpMoSt', self.real)

        self.writeSmartTagName(instance)
        self.writeSmartTagDisplayName(instance)
        self.writeTextListPCO(instance)

    def writeTextListPCO(self, instance):
        """ Retrieve and write down PCO modes info. """
        modes = map(lambda idx: instance.getAttributeData("SCADADeviceFunctionals:Mode Label:Option Mode " + str(idx) + " Label"), list(xrange(1, 9)))
        if all(mode == "" for mode in modes):
            return
        name = instance.getAttributeData("DeviceIdentification:Name")
        self.writeTextlistInstance(3, "$name$\tR\t\t\t")
        for i in list(xrange(8)):
            if modes[i] != "":
                self.writeTextlistInstance(3, "$name$\t\t\t" + str(i + 1) + "\ten-US=" + modes[i])
