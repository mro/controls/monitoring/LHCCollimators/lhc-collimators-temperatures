# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Analog Objects.

import Magelis_Generic_Template
reload(Magelis_Generic_Template)


class Analog_Template(Magelis_Generic_Template.Generic_Template):
    theDeviceType = "Analog"

    def processInstance(self, instance, params):
        self.writeVariable(instance, "ManReg01", "UINT")
        self.writeVariable(instance, "StsReg01", "UINT")
        self.writeVariable(instance, "StsReg02", "UINT")
        self.writeVariable(instance, "PosSt", "REAL")
        self.writeVariable(instance, "MPosR", "REAL")
        self.writeVariable(instance, "Name", "STRING")
        self.writeVariable(instance, "VijReg01", "UINT")
        self.writeVariable(instance, "Unit", "STRING")
        self.writeVariable(instance, "Description", "STRING")
