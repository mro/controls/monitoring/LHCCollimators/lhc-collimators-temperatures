# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2016 all rights reserved
# Jython source file for Encoder Objects.

import Magelis_Generic_Template
reload(Magelis_Generic_Template)


class Encoder_Template(Magelis_Generic_Template.Generic_Template):
    theDeviceType = "Encoder"

    def processInstance(self, instance, params):
        self.thePlugin.writeWarningInUABLog("the object Encoder has not been developed for the Magelis Platform.")
