# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogAlarm Objects.

import Magelis_Generic_Template
reload(Magelis_Generic_Template)


class AnalogAlarm_Template(Magelis_Generic_Template.Generic_Template):
    theDeviceType = "AnalogAlarm"

    def processInstance(self, instance, params):
        self.writeVariable(instance, "ManReg01", "UINT")
        self.writeVariable(instance, "StsReg01", "UINT")
        self.writeVariable(instance, "StsReg02", "UINT")
        self.writeVariable(instance, "PosSt", "REAL")

        self.writeVariable(instance, "Name", "STRING")
        self.writeVariable(instance, "VijReg01", "UINT")
        self.writeVariable(instance, "Description", "STRING")
        self.writeVariable(instance, "Unit", "STRING")

    def getUnit(self, instance):
        return self.getUnitAndFormat(instance)[0]

    def getFormat(self, instance):
        return self.getUnitAndFormat(instance)[1]

    def getUnitAndFormat(self, instance):
        input = instance.getAttributeData("FEDeviceEnvironmentInputs:Input").strip()
        unit = ""
        format = "###.###"
        if input.strip() != "":
            inputObjects = self.thePlugin.getUnicosProject().findMatchingInstances("AnalogInput, AnalogInputReal, Analog, AnalogStatus", "'#DeviceIdentification:Name#'='$input$'")
            for obj in inputObjects:
                unit = obj.getAttributeData("SCADADeviceGraphics:Unit")
                format = obj.getAttributeData("SCADADeviceGraphics:Format")
        return unit, format
