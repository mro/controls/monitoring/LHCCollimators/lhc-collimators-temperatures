# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for MassFlowController Objects.

import Magelis_Generic_Template
reload(Magelis_Generic_Template)


class MassFlowController_Template(Magelis_Generic_Template.Generic_Template):
    theDeviceType = "MassFlowController"

    def processInstance(self, instance, params):
        self.writeVariable(instance, "ManReg01", "UINT")
        self.writeVariable(instance, "StsReg01", "UINT")
        self.writeVariable(instance, "StsReg02", "UINT")
        self.writeVariable(instance, "ActVoTMo", "INT")
        self.writeVariable(instance, "AuVoTOfSt", "REAL")
        self.writeVariable(instance, "VoTSt", "REAL")
        self.writeVariable(instance, "PosSt", "REAL")

        self.writeVariable(instance, "Name", "STRING")
        self.writeVariable(instance, "Description", "STRING")
