# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for AnalogOutputReal Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin  # REQUIRED
from research.ch.cern.unicos.core import CoreManager  # REQUIRED
from java.lang import System
import WinCCOA_CommonMethods
from research.ch.cern.unicos.plugins.interfaces import AGenerationPlugin
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)
try:
    # Try to import the user privileges file
    import WinCCOA_Privileges
except:
    # If the previous import failed, try to import the privileges template file
    import WinCCOA_Privileges_Template


class AnalogOutputReal_Template(IUnicosTemplate):
    thePlugin = 0
    theDeviceType = "AnalogOutputReal"
    # Default name for the privileges file
    privilegesFileName = "WinCCOA_Privileges"

    def deviceFormat(self):
        return ["deviceType", "deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType",
                "unit", "format", "rangeMax", "rangeMin", "hhLimit", "hLimit", "lLimit", "llLimit", "alarmActive", "driverDeadbandValue", "driverDeadbandType",
                "archiveMode", "timeFilter", "smsCategory", "alarmMessage", "alarmAck", "addressStsReg01", "addressEvStsReg01", "addressPosSt", "addressAuPosRst",
                "addressManReg01", "addressMPosR", "booleanArchive", "analogArchive", "eventArchive", "maskEvent", "parameters", "master", "parents", "children", "type", "secondAlias"]

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython $self.theDeviceType$ template")
        reload(WinCCOA_CommonMethods)
        self.decorator = ucpc_library.shared_decorator.ExpressionDecorator()
        try:
            # Try to reload the user privileges file
            reload(WinCCOA_Privileges)
        except:
            # If the reload failed, reload the privileges template file
            self.privilegesFileName = "WinCCOA_Privileges_Template"
            reload(WinCCOA_Privileges_Template)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython $self.theDeviceType$ template")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython $self.theDeviceType$ template")

    def process(self, *params):
        self.thePlugin.writeInUABLog("processInstances in Jython $self.theDeviceType$ template")
        theCurrentDeviceType = params[0]
        self.theUnicosProject = theRawInstances = self.thePlugin.getUnicosProject()
        strAllDeviceTypes = theRawInstances.getAllDeviceTypesString()
        theApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        instancesNumber = str(len(instancesVector))
        DeviceTypeName = theCurrentDeviceType.getDeviceTypeName()
        deviceNumber = 50000
        CRLF = System.getProperty("line.separator")

        # Get the config (UnicosApplication.xml)
        config = CoreManager.getITechnicalParameters().getXMLConfigMapper()
        # Query a PLC parameter
        PLCType = config.getPLCDeclarations().get(0).getPLCType().getValue()

        self.thePlugin.writeDBHeader("#$self.theDeviceType$: $instancesNumber$")
        DeviceTypeFormat = "CPC_AnalogOutput;deviceNumber;Alias[,DeviceLinkList];Description - ElectricalDiagram;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;Unit;Format;RangeMax;RangeMin;HHLimit;HLimit;LLimit;LLLimit;AlarmActive;DriverDeadbandValue;DriverDeadbandType;ArchiveMode;TimeFilter;SMSCat;AlarmMessage;AlarmAck;addr_StsReg01;addr_EvStsReg01;addr_PosSt;addr_AuPosRst;addr_ManReg01;addr_MPosR;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;Parameters;Master;Parents;children;Type;SecondAlias;"
        self.thePlugin.writeComment("#$CRLF$# Object: " + DeviceTypeName + "$CRLF$#$CRLF$#Config Line : " + DeviceTypeFormat + "$CRLF$#")

        # Set Privileges
        Privileges = eval(self.privilegesFileName).getPrivileges(DeviceTypeName)

        for instance in instancesVector:
            if (instance is not None):

                # Check if user pressed 'cancel' button:
                if AGenerationPlugin.isGenerationInterrupted():
                    return

                deviceNumber = int(int(deviceNumber) + 1)
                deviceNumber = str(deviceNumber)

                # 1. Common Unicos fields
                # 2. Specific fields
                # 3. SCADA Device Data Archiving
                # 4. Data Treatment
                # 5. Addresses computation
                # 6. write the instance information in the database file

                # 1. common Unicos fields
                name = Name = Alias = instance.getAttributeData("DeviceIdentification:Name")
                expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
                Description = instance.getAttributeData("DeviceDocumentation:Description")
                Diagnostics = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
                WWWLink = instance.getAttributeData("SCADADeviceGraphics:WWW Link")
                Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
                WidgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
                Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
                AccessControlDomain = instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain").replace(" ", "")
                Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
                DeviceLinkListSpec = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ", "")

                # 2. Specific fields
                ElectricalDiagram = instance.getAttributeData("DeviceDocumentation:Electrical Diagram")
                Unit = instance.getAttributeData("SCADADeviceGraphics:Unit")
                Format = instance.getAttributeData("SCADADeviceGraphics:Format").replace(" ", "")
                RangeMax = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max"))
                RangeMin = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min"))
                HHLimit = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceAlarms:Analog Thresholds:HH Alarm"))
                HLimit = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceAlarms:Analog Thresholds:H Warning"))
                LLimit = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceAlarms:Analog Thresholds:L Warning"))
                LLLimit = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceAlarms:Analog Thresholds:LL Alarm"))
                DriverDeadbandType = instance.getAttributeData("SCADADriverDataSmoothing:Deadband Type")
                DriverDeadbandValue = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADriverDataSmoothing:Deadband Value"))
                SMSCat = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:SMS Category")
                AlarmAck = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Auto Acknowledge")
                AlarmMasked = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Masked")
                AlarmMessage = instance.getAttributeData("SCADADeviceAlarms:Message")
                MaskEvent = instance.getAttributeData("SCADADeviceFunctionals:Mask Event")

                # 3. SCADA Device Data Archiving
                ArchiveMode = instance.getAttributeData("SCADADeviceDataArchiving:Archive Mode")
                TimeFilter = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceDataArchiving:Time Filter (s)"))
                DeadbandType = instance.getAttributeData("SCADADeviceDataArchiving:Deadband Type")
                DeadbandValue = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceDataArchiving:Deadband Value"))
                BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
                AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
                EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")

                # 4. Data Treatment
                S_ArchiveMode = ArchiveMode.lower()
                S_DriverDeadbandType = DriverDeadbandType.lower()
                S_DeadbandType = DeadbandType.lower()
                S_MaskEvent = MaskEvent.lower()
                S_AlarmMasked = AlarmMasked.lower()
                S_AlarmAck = AlarmAck.lower()

                S_Domain = Domain.replace(" ", "")
                S_Nature = Nature.replace(" ", "")
                S_Unit = Unit.replace(" ", "")
                S_SMSCat = SMSCat.replace(" ", "")

                # Build DeviceLinkList and children from related objects
                DeviceLinkList = ""

                # Append Device Link list from Spec
                DeviceLinkList = WinCCOA_CommonMethods.appendDeviceLinkListFromSpec(DeviceLinkListSpec, DeviceLinkList, self.thePlugin, strAllDeviceTypes)

                # Avoid duplications on the deviceLinks
                DeviceLinkList = WinCCOA_CommonMethods.removeDuplicatesAndGivenObjects(DeviceLinkList, [Name, expertName])

                # Default values if domain or nature empty
                if S_Domain == "":
                    Domain = theApplicationName

                if AccessControlDomain != "":
                    AccessControlDomain = AccessControlDomain + "|"

                if S_Nature == "":
                    Nature = "AOR"

                if S_Unit == "":
                    self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. There is no Unit specified.")

                # Mask Value
                if S_MaskEvent == "true":
                    MaskEvent = "0"
                else:
                    MaskEvent = "1"

                # AlarmMasked --> NOT Alarm Active
                AlarmActive = WinCCOA_CommonMethods.getPvssAlarmActiveAnalog(S_AlarmMasked, HHLimit, HLimit, LLimit, LLLimit)

                # AlarmAck
                AlarmAck = WinCCOA_CommonMethods.getPvssAlarmAckAnalog(S_AlarmAck, HHLimit, LLLimit)

                # Alarm Message
                if S_SMSCat != "" and AlarmMessage == "":
                    AlarmMessage = "No message"
                    self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. An Alarm message is required: forced it to ($AlarmMessage$).")

                # Archive Time Filter
                if TimeFilter == "" and S_ArchiveMode != "no":
                    TimeFilter = "0.0"
                    if S_ArchiveMode == "time" or S_ArchiveMode == "deadband and time" or S_ArchiveMode == "deadband or time" or S_ArchiveMode == "old/new comparison and time" or S_ArchiveMode == "old/new comparison or time":
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Archive Time Filter is required by the Archive Mode selected: forced to ($TimeFilter$).")

                # Archive deadBand Value
                if not (S_ArchiveMode == "no" or S_ArchiveMode == "old/new comparison" or S_ArchiveMode == "time" or S_ArchiveMode == "old/new comparison and time" or S_ArchiveMode == "old/new comparison or time"):
                    if S_DeadbandType == "" or DeadbandValue == "":
                        S_DeadbandType = "relative"
                        DeadbandValue = "5.0"
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Archive Deadband Type and Value are required : forced to ($S_DeadbandType$ $DeadbandValue$ %).")

                # Driver Deadband Value
                if DriverDeadbandValue == "":
                    DriverDeadbandValue = "0.0"
                    if S_DriverDeadbandType == "absolute" or S_DriverDeadbandType == "relative":
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Driver Deadband Value is required : forced to ($DriverDeadbandValue$).")

                # Driver Deadband versus Archive Deadband
                if (S_DriverDeadbandType == "absolute" or S_DriverDeadbandType == "relative") and (S_ArchiveMode == "deadband" or S_ArchiveMode == "deadband and time" or S_ArchiveMode == "deadband or time"):
                    if S_DriverDeadbandType == S_DeadbandType:  # both are of the same type relative or absolute
                        if float(DriverDeadbandValue) > float(DeadbandValue):
                            self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Driver Deadband ($DriverDeadbandValue$) and Archive Deadband ($DeadbandValue$) are not consistent.")
                    else:
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Driver Deadband and Archive Deadband are not of the same type (relative or aboslute).")

                # Driver Smooting
                if (S_DriverDeadbandType == "no"):
                    DriverDeadbandType = str("0")
                elif (S_DriverDeadbandType == "absolute"):
                    DriverDeadbandType = str("1")
                elif (S_DriverDeadbandType == "relative"):
                    DriverDeadbandType = str("2")
                elif (S_DriverDeadbandType == "old/new"):
                    DriverDeadbandType = str("3")
                else:
                    DriverDeadbandType = str("0")

                # Archiving mode
                if (S_ArchiveMode == "no"):
                    ArchiveMode = str("N")
                elif (S_ArchiveMode == "deadband" and S_DeadbandType == "absolute"):
                    ArchiveMode = str("VA,$DeadbandValue$,N")
                elif (S_ArchiveMode == "deadband" and S_DeadbandType == "relative"):
                    ArchiveMode = str("VR,$DeadbandValue$,N")
                elif (S_ArchiveMode == "time"):
                    ArchiveMode = str("Y")
                elif (S_ArchiveMode == "deadband and time" and S_DeadbandType == "absolute"):
                    ArchiveMode = str("VA,$DeadbandValue$,A")
                elif (S_ArchiveMode == "deadband and time" and S_DeadbandType == "relative"):
                    ArchiveMode = str("VR,$DeadbandValue$,A")
                elif (S_ArchiveMode == "deadband or time" and S_DeadbandType == "absolute"):
                    ArchiveMode = str("VA,$DeadbandValue$,O")
                elif (S_ArchiveMode == "deadband or time" and S_DeadbandType == "relative"):
                    ArchiveMode = str("VR,$DeadbandValue$,O")
                elif (S_ArchiveMode == "old/new comparison"):
                    ArchiveMode = str("O")
                    TimeFilter = "0"
                elif (S_ArchiveMode == "old/new comparison and time"):
                    ArchiveMode = str("A")
                elif (S_ArchiveMode == "old/new comparison or time"):
                    ArchiveMode = str("O")
                else:
                    ArchiveMode = str("N")

                # 5. Addresses computation
                # how to define "communication type"?:
                addr_StsReg01 = self.getAddressSCADA("$Alias$_StsReg01", PLCType)
                addr_EvStsReg01 = self.getAddressSCADA("$Alias$_EvStsReg01", PLCType)
                addr_PosSt = self.getAddressSCADA("$Alias$_PosSt", PLCType)
                addr_AuPosRst = self.getAddressSCADA("$Alias$_AuPosRSt", PLCType)
                addr_ManReg01 = self.getAddressSCADA("$Alias$_ManReg01", PLCType)
                addr_MPosR = self.getAddressSCADA("$Alias$_MPosR", PLCType)

                # 6. New relationship information in all objects:
                parents = []

                # Analog parents
                parents += self.thePlugin.getRIndex().get(Name, "Analog", "FEDeviceOutputs:Process Output").split(',')

                # AnaDO parents
                parents += self.thePlugin.getRIndex().get(Name, "AnaDO", "FEDeviceOutputs:Analog Process Output").split(',')

                # Digital Alarm parents
                parents += self.thePlugin.getRIndex().get(Name, "DigitalAlarm", "FEDeviceEnvironmentInputs:Input").split(',')
                # Analog Alarm parents
                parents += self.thePlugin.getRIndex().get(Name, "AnalogAlarm",
                                                          ["FEDeviceEnvironmentInputs:Input",
                                                           "FEDeviceAlarm:Enable Condition",
                                                           "FEDeviceManualRequests:HH Alarm",
                                                           "FEDeviceManualRequests:H Warning",
                                                           "FEDeviceManualRequests:L Warning",
                                                           "FEDeviceManualRequests:LL Alarm"]).split(',')
                parents = [x for x in parents if x]  # remove empty strings

                uniqueList = []
                for parent in parents:
                    if parent not in uniqueList:
                        uniqueList.append(parent)
                parents = uniqueList
                stringParents = ",".join(parents)
                type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
                children = ""
                master = ""

                # Expert Name Logic
                S_Name = name.strip()
                S_ExpertName = expertName.strip()
                if S_ExpertName == "":
                    secondAlias = Alias
                else:
                    secondAlias = Alias
                    Alias = S_ExpertName

                # 7. Parameters field for all objects
                Parameters = ""

                # 8. write the instance information in the database file
                self.thePlugin.writeInstanceInfo("CPC_AnalogOutput;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$ - $ElectricalDiagram$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;$Unit$;$Format$;$RangeMax$;$RangeMin$;$HHLimit$;$HLimit$;$LLimit$;$LLLimit$;$AlarmActive$;$DriverDeadbandValue$;$DriverDeadbandType$;$ArchiveMode$;$TimeFilter$;$SMSCat$;$AlarmMessage$;$AlarmAck$;$addr_StsReg01$;$addr_EvStsReg01$;$addr_PosSt$;$addr_AuPosRst$;$addr_ManReg01$;$addr_MPosR$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$MaskEvent$;$Parameters$;$master$;$stringParents$;$children$;$type$;$secondAlias$;")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython $self.theDeviceType$ template")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython $self.theDeviceType$ template")

    def getAddressSCADA(self, DPName, PLCType):
        if PLCType.lower() == "quantum":
            address = str(int(self.thePlugin.computeAddress(DPName)) - 1)
        else:
            address = str(self.thePlugin.computeAddress(DPName))

        return address
