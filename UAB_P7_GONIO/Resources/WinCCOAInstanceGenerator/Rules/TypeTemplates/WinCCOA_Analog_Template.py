# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Analog Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin  # REQUIRED
from research.ch.cern.unicos.core import CoreManager  # REQUIRED
from java.util import ArrayList
from java.util import Vector
from java.lang import System
import WinCCOA_CommonMethods
from research.ch.cern.unicos.plugins.interfaces import AGenerationPlugin
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)
try:
    # Try to import the user privileges file
    import WinCCOA_Privileges
except:
    # If the previous import failed, try to import the privileges template file
    import WinCCOA_Privileges_Template


class Analog_Template(IUnicosTemplate):
    thePlugin = 0
    theDeviceType = "Analog"
    # Default name for the privileges file
    privilegesFileName = "WinCCOA_Privileges"

    def deviceFormat(self):
        return ["deviceType", "deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType", "unit",
                "format", "rangeMax", "rangeMin", "pLiOn", "pLiOff", "driverDeadbandValue", "driverDeadbandType", "archiveMode", "timeFilter", "normalPosition",
                "addressStsReg01", "addressStsReg02", "addressEvStsReg01", "addressEvStsReg02", "addressPosSt", "addressPosRSt", "addressAuPosRSt", "addressMPosRSt",
                "addressManReg01", "addressMPosR", "addressPLiOn", "addressPLiOff", "booleanArchive", "analogArchive", "eventArchive", "maskEvent", "parameters", "master",
                "parents", "children", "type", "secondAlias"]

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython $self.theDeviceType$ template")
        reload(WinCCOA_CommonMethods)
        self.decorator = ucpc_library.shared_decorator.ExpressionDecorator()
        try:
            # Try to reload the user privileges file
            reload(WinCCOA_Privileges)
        except:
            # If the reload failed, reload the privileges template file
            self.privilegesFileName = "WinCCOA_Privileges_Template"
            reload(WinCCOA_Privileges_Template)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython $self.theDeviceType$ template")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython $self.theDeviceType$ template")

    def process(self, *params):
        self.thePlugin.writeInUABLog("processInstances in Jython $self.theDeviceType$ template")
        theCurrentDeviceType = params[0]
        self.theUnicosProject = theRawInstances = self.thePlugin.getUnicosProject()
        strAllDeviceTypes = theRawInstances.getAllDeviceTypesString()
        theApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        instancesNumber = str(len(instancesVector))
        DeviceTypeName = theCurrentDeviceType.getDeviceTypeName()
        deviceNumber = 0
        CRLF = System.getProperty("line.separator")

        # Get the config (UnicosApplication.xml)
        config = CoreManager.getITechnicalParameters().getXMLConfigMapper()
        # Query a PLC parameter
        PLCType = config.getPLCDeclarations().get(0).getPLCType().getValue()

        self.thePlugin.writeDBHeader("#$self.theDeviceType$: $instancesNumber$")
        DeviceTypeFormat = "CPC_$DeviceTypeName$;deviceNumber;Alias[,DeviceLinkList];Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;Unit;Format;RangeMax;RangeMin;PLiOn;PLiOff;DriverDeadbandValue;DriverDeadbandType;ArchiveMode;TimeFilter;NormalPosition;addr_StsReg01;addr_StsReg02;addr_EvStsReg01;addr_EvStsReg02;addr_PosSt;addr_PosRSt;addr_AuPosRSt;addr_MPosRSt;addr_ManReg01;addr_MPosR;addr_PLiOn;addr_PLiOff;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;Parameters;master;stringparents;stringchildren;type;secondAlias;"
        self.thePlugin.writeComment("#$CRLF$# Object: " + DeviceTypeName + "$CRLF$#$CRLF$#Config Line : " + DeviceTypeFormat + "$CRLF$#")

        # Set Privileges
        Privileges = eval(self.privilegesFileName).getPrivileges(DeviceTypeName)

        for instance in instancesVector:
            if (instance is not None):

                # Check if user pressed 'cancel' button:
                if AGenerationPlugin.isGenerationInterrupted():
                    return

                deviceNumber = int(int(deviceNumber) + 1)
                deviceNumber = str(deviceNumber)

                # 1. Common Unicos fields
                # 2. Specific fields
                # 3. SCADA Device Data Archiving
                # 4. Data Treatment
                # 5. Addresses computation
                # 6. write the instance information in the database file
                # 7. fill the parameters field

                # 1. Common Unicos fields
                name = Name = Alias = instance.getAttributeData("DeviceIdentification:Name")
                expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
                Description = instance.getAttributeData("DeviceDocumentation:Description")
                Diagnostics = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
                WWWLink = instance.getAttributeData("SCADADeviceGraphics:WWW Link")
                Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
                WidgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
                Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
                AccessControlDomain = instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain").replace(" ", "")
                Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
                DeviceLinkListSpec = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ", "")

                # 2. Specific fields
                Unit = instance.getAttributeData("SCADADeviceGraphics:Unit")
                Format = instance.getAttributeData("SCADADeviceGraphics:Format")
                PLiOn = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceManualRequests:Parameter Limit On/Open"))
                PLiOff = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceManualRequests:Parameter Limit Off/Closed"))
                FeedbackOn = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On")
                FeedbackOff = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off")
                FeedbackAnalog = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Analog")
                HAnalogOut = instance.getAttributeData("FEDeviceEnvironmentInputs:Hardware Analog Output")
                LocalDrive = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Drive").strip()
                if LocalDrive <> "":
                    if LocalDrive[0:4].lower() == "not ":
                        LocalDrive = LocalDrive[4:]
                ProcessOutput = instance.getAttributeData("FEDeviceOutputs:Process Output")
                MasterDevice = instance.getAttributeData("LogicDeviceDefinitions:Master")
                ExternalMasterDevice = instance.getAttributeData("LogicDeviceDefinitions:External Master")
                MaskEvent = instance.getAttributeData("SCADADeviceFunctionals:Mask Event")
                DriverDeadbandType = instance.getAttributeData("SCADADriverDataSmoothing:Deadband Type")
                DriverDeadbandValue = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADriverDataSmoothing:Deadband Value"))
                NormalPosition = "0"
                WarningDelay = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Warning Time Delay (s)"))
                WarningDeadband = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Warning Deadband Value (Unit)"))

                # 3. SCADA Device Data Archiving
                ArchiveMode = instance.getAttributeData("SCADADeviceDataArchiving:Archive Mode")
                TimeFilter = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceDataArchiving:Time Filter (s)"))
                DeadbandType = instance.getAttributeData("SCADADeviceDataArchiving:Deadband Type")
                DeadbandValue = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceDataArchiving:Deadband Value"))
                BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
                AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
                EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")

                AO_ArchiveMode = "No"
                AO_TimeFilter = ""
                AO_DeadbandType = ""
                AO_DeadbandValue = ""

                # 4. Data Treatment
                S_DeadbandType = DeadbandType.lower()
                S_DriverDeadbandType = DriverDeadbandType.lower()
                S_ArchiveMode = ArchiveMode.lower()

                # Build DeviceLinkList and children from related objects
                DeviceLinkList = ""
                # MasterDevice
                if (MasterDevice != ""):
                    MasterDevice = MasterDeviceWinCCOAName = self.thePlugin.getLinkedExpertName(MasterDevice, "ProcessControlObject")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, MasterDeviceWinCCOAName)
                elif (ExternalMasterDevice != ""):
                    MasterDevice = MasterDeviceWinCCOAName = ExternalMasterDevice
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, MasterDeviceWinCCOAName)

                children = []
                # feedback On
                if (FeedbackOn != ""):
                    FeedbackOnWinCCOAName = self.thePlugin.getLinkedExpertName(FeedbackOn, "DigitalInput")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, FeedbackOnWinCCOAName)
                    children.append(FeedbackOnWinCCOAName)

                # feedback Off
                if (FeedbackOff != ""):
                    FeedbackOffWinCCOAName = self.thePlugin.getLinkedExpertName(FeedbackOff, "DigitalInput")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, FeedbackOffWinCCOAName)
                    children.append(FeedbackOffWinCCOAName)

                # feedback Analog
                if (FeedbackAnalog != ""):
                    FeedbackAnalogWinCCOAName = self.thePlugin.getLinkedExpertName(FeedbackAnalog, "AnalogInput, AnalogInputReal, AnalogStatus, WordStatus")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, FeedbackAnalogWinCCOAName)
                    children.append(FeedbackAnalogWinCCOAName)

                # Local Drive
                if (LocalDrive != ""):
                    LocalDriveWinCCOAName = self.thePlugin.getLinkedExpertName(LocalDrive, "DigitalInput")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, LocalDriveWinCCOAName)
                    children.append(LocalDriveWinCCOAName)

                # Hardware Analog Output
                if (HAnalogOut != ""):
                    HAnalogOutWinCCOAName = self.thePlugin.getLinkedExpertName(HAnalogOut, "AnalogInput, AnalogInputReal, AnalogStatus")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, HAnalogOutWinCCOAName)
                    children.append(HAnalogOutWinCCOAName)

                # Process Output
                # Connect Range Min and Range Max from AO to Analog
                if (ProcessOutput != ""):
                    inst_Out = theRawInstances.findMatchingInstances("AnalogOutput, AnalogOutputReal, AnalogStatus", "'#DeviceIdentification:Name#'='$ProcessOutput$'")
                    ProcessOutputWinCCOAName = self.thePlugin.getLinkedExpertName(ProcessOutput, "AnalogOutput, AnalogOutputReal, AnalogStatus")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, ProcessOutputWinCCOAName)
                    children.append(ProcessOutputWinCCOAName)
                    for instance_Out in inst_Out:
                        RangeMax = self.thePlugin.formatNumberPLC(instance_Out.getAttributeData("FEDeviceParameters:Range Max"))
                        RangeMin = self.thePlugin.formatNumberPLC(instance_Out.getAttributeData("FEDeviceParameters:Range Min"))
                        AO_ArchiveMode = instance_Out.getAttributeData("SCADADeviceDataArchiving:Archive Mode")
                        AO_TimeFilter = self.thePlugin.formatNumberPLC(instance_Out.getAttributeData("SCADADeviceDataArchiving:Time Filter (s)"))
                        AO_DeadbandType = instance_Out.getAttributeData("SCADADeviceDataArchiving:Deadband Type")
                        AO_DeadbandValue = self.thePlugin.formatNumberPLC(instance_Out.getAttributeData("SCADADeviceDataArchiving:Deadband Value"))
                else:
                    RangeMax = "100.0"
                    RangeMin = "0.0"

                # Gets all the controller objects that are child of the object with some properties
                AllControllers = theRawInstances.findMatchingInstances("Controller", "'#FEDeviceOutputs:Controlled Objects#' != ''")
                theControllerObjects = ArrayList()
                for PID in AllControllers:
                    PIDName = PID.getAttributeData("DeviceIdentification:Name").replace(",", " ")
                    ControlledObjectNames = PID.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ")
                    ControlledObjectList = ControlledObjectNames.split()
                    for ControlledObjectName in ControlledObjectList:
                        if (ControlledObjectName == Name):
                            theControllerObjects.add(PIDName)

                inst_PID = theControllerObjects
                for PID_Name in inst_PID:
                    PID_NameWinCCOAName = self.thePlugin.getLinkedExpertName(PID_Name, "Controller")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, PID_NameWinCCOAName)

                # UCPC-1400 Add DeviceLink list and children for new object links
                if self.thePlugin.isString(WarningDelay) and WarningDelay <> "":
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(WarningDelay, DeviceLinkList, children, self.thePlugin, self.decorator)
                if self.thePlugin.isString(WarningDeadband) and WarningDeadband <> "":
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(WarningDeadband, DeviceLinkList, children, self.thePlugin, self.decorator)

                # Append Device Link list from Spec
                DeviceLinkList = WinCCOA_CommonMethods.appendDeviceLinkListFromSpec(DeviceLinkListSpec, DeviceLinkList, self.thePlugin, strAllDeviceTypes)

                # Avoid duplications on the deviceLinks
                DeviceLinkList = WinCCOA_CommonMethods.removeDuplicatesAndGivenObjects(DeviceLinkList, [Name, expertName])

                # If no Archive mode, Deadband or TimeFilter defined, take archive mode of AO
                if (ArchiveMode == ""):
                    ArchiveMode = AO_ArchiveMode
                if (DeadbandType == ""):
                    DeadbandType = AO_DeadbandType
                if (DeadbandValue == ""):
                    DeadbandValue = AO_DeadbandValue
                if (TimeFilter == ""):
                    TimeFilter = AO_TimeFilter

                S_DeadbandType = DeadbandType.lower()
                S_DriverDeadbandType = DriverDeadbandType.lower()
                S_ArchiveMode = ArchiveMode.lower()

                # PLiOn/Off default :  +/- 10% PMaxRan/PMinRan
                if PLiOn == "":
                    PLiOn = str(float(RangeMax) - 0.1 * (float(RangeMax) - float(RangeMin)))
                if PLiOff == "":
                    PLiOff = str(float(RangeMin) + 0.1 * (float(RangeMax) - float(RangeMin)))

                # Default values if domain or nature empty
                if Domain == "":
                    Domain = theApplicationName

                if AccessControlDomain != "":
                    AccessControlDomain = AccessControlDomain + "|"

                if Nature == "":
                    Nature = DeviceTypeName

                # Mask Value
                if (MaskEvent.lower() == "true"):
                    MaskEvent = "0"
                else:
                    MaskEvent = "1"

                # Archive Time Filter
                if TimeFilter == "" and S_ArchiveMode != "no":
                    TimeFilter = "0.0"
                    if S_ArchiveMode == "time" or S_ArchiveMode == "old/new comparison and time" or S_ArchiveMode == "old/new comparison or time" or S_ArchiveMode == "deadband or time" or S_ArchiveMode == "deadband and time":
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Archive Time Filter is required by the Archive Mode selected: forced to ($TimeFilter$).")

                # Default archive DB in case of Null
                if not (S_ArchiveMode == "no" or S_ArchiveMode == "old/new comparison" or S_ArchiveMode == "time" or S_ArchiveMode == "old/new comparison and time" or S_ArchiveMode == "old/new comparison or time"):
                    if S_DeadbandType == "" or DeadbandValue == "":
                        S_DeadbandType = "relative"
                        DeadbandValue = "5.0"
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Archive Deadband Type and Value are required : forced to ($S_DeadbandType$ $DeadbandValue$ %).")

                # Driver Deadband Value
                if DriverDeadbandValue == "":
                    DriverDeadbandValue = "0.0"
                    if S_DriverDeadbandType == "absolute" or S_DriverDeadbandType == "relative":
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Driver Deadband Value is required : forced to ($DriverDeadbandValue$).")

                # Driver Deadband versus Archive Deadband
                if (S_DriverDeadbandType == "absolute" or S_DriverDeadbandType == "relative") and (S_ArchiveMode == "deadband" or S_ArchiveMode == "deadband and time" or S_ArchiveMode == "deadband or time"):
                    if S_DriverDeadbandType == S_DeadbandType:  # both are of the same type relative or absolute
                        if float(DriverDeadbandValue) > float(DeadbandValue):
                            self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Driver Deadband ($DriverDeadbandValue$) and Archive Deadband ($DeadbandValue$) are not consistent.")
                    else:
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Driver Deadband and Archive Deadband are not of the same type (relative or aboslute).")

                # Driver Smooting
                if (S_DriverDeadbandType == "no"):
                    DriverDeadbandType = str("0")
                elif (S_DriverDeadbandType == "absolute"):
                    DriverDeadbandType = str("1")
                elif (S_DriverDeadbandType == "relative"):
                    DriverDeadbandType = str("2")
                elif (S_DriverDeadbandType == "old/new"):
                    DriverDeadbandType = str("3")
                else:
                    DriverDeadbandType = str("0")

                # Archiving Mode
                if (S_ArchiveMode == "no"):
                    ArchiveMode = str("N")
                elif (S_ArchiveMode == "deadband" and S_DeadbandType == "absolute"):
                    ArchiveMode = str("VA,$DeadbandValue$,N")
                elif (S_ArchiveMode == "deadband" and S_DeadbandType == "relative"):
                    ArchiveMode = str("VR,$DeadbandValue$,N")
                elif (S_ArchiveMode == "time"):
                    ArchiveMode = str("Y")
                elif (S_ArchiveMode == "deadband and time" and S_DeadbandType == "absolute"):
                    ArchiveMode = str("VA,$DeadbandValue$,A")
                elif (S_ArchiveMode == "deadband and time" and S_DeadbandType == "relative"):
                    ArchiveMode = str("VR,$DeadbandValue$,A")
                elif (S_ArchiveMode == "deadband or time" and S_DeadbandType == "absolute"):
                    ArchiveMode = str("VA,$DeadbandValue$,O")
                elif (S_ArchiveMode == "deadband or time" and S_DeadbandType == "relative"):
                    ArchiveMode = str("VR,$DeadbandValue$,O")
                elif (S_ArchiveMode == "old/new comparison"):
                    ArchiveMode = str("O")
                    TimeFilter = "0"
                elif (S_ArchiveMode == "old/new comparison and time"):
                    ArchiveMode = str("A")
                elif (S_ArchiveMode == "old/new comparison or time"):
                    ArchiveMode = str("O")
                else:
                    ArchiveMode = str("N")

                # 5. Addresses computation
                addr_StsReg01 = self.getAddressSCADA("$Alias$_StsReg01", PLCType)
                addr_StsReg02 = self.getAddressSCADA("$Alias$_StsReg02", PLCType)
                addr_EvStsReg01 = self.getAddressSCADA("$Alias$_EvStsReg01", PLCType)
                addr_EvStsReg02 = self.getAddressSCADA("$Alias$_EvStsReg02", PLCType)
                addr_ManReg01 = self.getAddressSCADA("$Alias$_ManReg01", PLCType)
                addr_PosSt = self.getAddressSCADA("$Alias$_PosSt", PLCType)
                addr_AuPosRSt = self.getAddressSCADA("$Alias$_AuPosRSt", PLCType)
                addr_MPosRSt = self.getAddressSCADA("$Alias$_MPosRSt", PLCType)
                addr_MPosR = self.getAddressSCADA("$Alias$_MPosR", PLCType)
                addr_PLiOn = self.getAddressSCADA("$Alias$_PLiOn", PLCType)
                addr_PLiOff = self.getAddressSCADA("$Alias$_PLiOff", PLCType)
                addr_PosRSt = self.getAddressSCADA("$Alias$_PosRSt", PLCType)

                # 6. New relationship information in all objects:
                PEnRstart = instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip()
                type = ""
                parents = []

                AllControllers = theRawInstances.findMatchingInstances("Controller", "'#FEDeviceOutputs:Controlled Objects#' != ''")
                theControllerObjects = ArrayList()
                for PID in AllControllers:
                    PIDName = PID.getAttributeData("DeviceIdentification:Name").replace(",", " ")
                    ControlledObjectNames = PID.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ")
                    ControlledObjectList = ControlledObjectNames.split()
                    for ControlledObjectName in ControlledObjectList:
                        if (ControlledObjectName == Name):
                            theControllerObjects.add(PIDName)

                inst_PID = theControllerObjects
                for PID_Name in inst_PID:
                    PID_NameWinCCOAName = self.thePlugin.getLinkedExpertName(PID_Name, "Controller")
                    parents.append(PID_NameWinCCOAName)

                uniqueListParents = []
                for parent in parents:
                    if parent not in uniqueListParents:
                        uniqueListParents.append(parent)

                parents = uniqueListParents
                stringparents = ",".join(parents)

                # AA, DA Children
                AlarmChildren = self.theUnicosProject.findMatchingInstances("AnalogAlarm, DigitalAlarm", "$Name$", "")
                for AlarmChild in AlarmChildren:
                    AlarmChild_Name = WinCCOA_CommonMethods.getExpertName(AlarmChild)
                    children.append(AlarmChild_Name)

                uniqueList = []
                for child in children:
                    if child not in uniqueList:
                        uniqueList.append(child)
                children = uniqueList
                stringchildren = ",".join(children)

                # Expert Name Logic
                S_Name = name.strip()
                S_ExpertName = expertName.strip()
                if S_ExpertName == "":
                    secondAlias = Alias
                else:
                    secondAlias = Alias
                    Alias = S_ExpertName

                # 7. Parameters field
                PFSPosOn = instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe")
                Parameters = ""
                if PFSPosOn.lower() == "off/close":
                    ParamPFSPosOn = "PFSPosOn=FALSE"
                else:
                    ParamPFSPosOn = "PFSPosOn=TRUE"
                if PEnRstart.lower() == "false":
                    ParamPEnRstart = "PEnRstart=FALSE"
                    ParamPRstartFS = "PRstartFS=FALSE"
                elif (PEnRstart.lower() == "true only if full stop disappeared"):
                    ParamPEnRstart = "PEnRstart=TRUE"
                    ParamPRstartFS = "PRstartFS=FALSE"
                else:
                    ParamPEnRstart = "PEnRstart=TRUE"
                    ParamPRstartFS = "PRstartFS=TRUE"
                if (LocalDrive.strip() != ""):
                    ParamPHLDrive = "PHLDrive=TRUE"
                else:
                    ParamPHLDrive = "PHLDrive=FALSE"
                if self.thePlugin.isString(WarningDelay) and WarningDelay <> "":
                    ParamWarningDelay = ",WARNING_DELAY_TIME=" + self.thePlugin.getLinkedExpertName(WarningDelay)
                else:
                    ParamWarningDelay = ""
                if self.thePlugin.isString(WarningDeadband) and WarningDeadband <> "":
                    ParamWarningDeadband = ",WARNING_DEADBAND=" + self.thePlugin.getLinkedExpertName(WarningDeadband)
                else:
                    ParamWarningDeadband = ""

                Parameters = ParamPFSPosOn + "," + ParamPEnRstart + "," + ParamPRstartFS + "," + ParamPHLDrive + ParamWarningDelay + ParamWarningDeadband

                # 8. write the instance information in the database file
                self.thePlugin.writeInstanceInfo("CPC_$DeviceTypeName$;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;$Unit$;$Format$;$RangeMax$;$RangeMin$;$PLiOn$;$PLiOff$;$DriverDeadbandValue$;$DriverDeadbandType$;$ArchiveMode$;$TimeFilter$;$NormalPosition$;$addr_StsReg01$;$addr_StsReg02$;$addr_EvStsReg01$;$addr_EvStsReg02$;$addr_PosSt$;$addr_PosRSt$;$addr_AuPosRSt$;$addr_MPosRSt$;$addr_ManReg01$;$addr_MPosR$;$addr_PLiOn$;$addr_PLiOff$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$MaskEvent$;$Parameters$;$MasterDevice$;$stringparents$;$stringchildren$;$type$;$secondAlias$;")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython $self.theDeviceType$ template")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython $self.theDeviceType$ template")

    def getAddressSCADA(self, DPName, PLCType):
        if PLCType.lower() == "quantum":
            address = str(int(self.thePlugin.computeAddress(DPName)) - 1)
        else:
            address = str(self.thePlugin.computeAddress(DPName))

        return address
