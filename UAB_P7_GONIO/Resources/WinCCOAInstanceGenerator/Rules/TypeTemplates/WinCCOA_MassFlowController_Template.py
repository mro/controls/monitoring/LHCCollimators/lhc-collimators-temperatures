# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for MassFlowController Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin  # REQUIRED
from research.ch.cern.unicos.core import CoreManager  # REQUIRED
from java.util import ArrayList
from java.util import Vector
from java.lang import System
import WinCCOA_CommonMethods
from research.ch.cern.unicos.plugins.interfaces import AGenerationPlugin
try:
    # Try to import the user privileges file
    import WinCCOA_Privileges
except:
    # If the previous import failed, try to import the privileges template file
    import WinCCOA_Privileges_Template


class MassFlowController_Template(IUnicosTemplate):
    thePlugin = 0
    theDeviceType = "MassFlowController"
    # Default name for the privileges file
    privilegesFileName = "WinCCOA_Privileges"

    def deviceFormat(self):
        return ["deviceType", "deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType", "flowUnit",
                "flowFormat", "driverFlowDbValue", "driverFlowDbType", "archiveFlowMode", "archiveFlowTime", "totalizerUnit", "totalizerFormat", "driverTotalizerDbValue",
                "driverTotalizerDbType", "archiveTotalizerMode", "archiveTotalizerTime", "archiveModeMode", "archiveModeTime", "normalPosition", "addressStsReg01",
                "addressStsReg02", "addressEvStsReg01", "addressEvStsReg02", "addressPosSt", "addressPosRSt", "addressMaxPosSt", "addressMPosRSt", "addressAuPosRSt",
                "addressAuVoTOfSt", "addressAuCCRSt", "addressAuDMoRSt", "addressAuVoTMRSt", "addressMVoTMRSt", "addressMDMoRSt", "addressMCCRSt", "addressVoTSt",
                "addressActOutOV", "addressActDMo", "addressActCC", "addressActVoTMo", "addressManReg01", "addressMPosR", "addressMDMoR", "addressMCCR", "addressMVoTMoR",
                "calibrationCurves", "calibrationUnit", "calibrationGasName", "booleanArchive", "analogArchive", "eventArchive", "maskEvent", "parameters", "master",
                "parents", "children", "type", "secondAlias"]

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython $self.theDeviceType$ template")
        reload(WinCCOA_CommonMethods)
        try:
            # Try to reload the user privileges file
            reload(WinCCOA_Privileges)
        except:
            # If the reload failed, reload the privileges template file
            self.privilegesFileName = "WinCCOA_Privileges_Template"
            reload(WinCCOA_Privileges_Template)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython $self.theDeviceType$ template")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython $self.theDeviceType$ template")

    def process(self, *params):
        self.thePlugin.writeInUABLog("processInstances in Jython $self.theDeviceType$ template")
        theCurrentDeviceType = params[0]
        self.theUnicosProject = theRawInstances = self.thePlugin.getUnicosProject()
        strAllDeviceTypes = theRawInstances.getAllDeviceTypesString()
        theApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        instancesNumber = str(len(instancesVector))
        DeviceTypeName = theCurrentDeviceType.getDeviceTypeName()
        deviceNumber = 0
        CRLF = System.getProperty("line.separator")

        # Get the config (UnicosApplication.xml)
        config = CoreManager.getITechnicalParameters().getXMLConfigMapper()
        # Query a PLC parameter
        PLCType = config.getPLCDeclarations().get(0).getPLCType().getValue()

        self.thePlugin.writeDBHeader("#$self.theDeviceType$: $instancesNumber$")
        DeviceTypeFormat = "CPC_" + DeviceTypeName + ";deviceNumber;Alias[,DeviceLinkList];Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;FlowUnit;FlowFormat;DriverFlowDbValue;DriverFlowDbType;ArchiveFlowMode;ArchiveFlowTime;TotUnit;TotFormat;DriverTotDbValue;DriverTotalizerDbType;ArchiveTotalizerMode;ArchiveTotTime;ArchiveModeMode;ArchiveModeTime;NormalPosition;addr_StsReg01;addr_StsReg02;addr_EvStsReg01;addr_EvStsReg02;addr_PosSt;addr_PosRSt;addr_MaxPosSt;addr_MPosRSt;addr_AuPosRSt;addr_AuVoTOfSt;addr_AuCCRSt;addr_AuDMoRSt;addr_AuVoTMRSt;addr_MVoTMRSt;addr_MDMoRSt;addr_MCCRSt;addr_VoTSt;addr_ActOutOV;addr_ActDMo;addr_ActCC;addr_ActVoTMo;addr_ManReg01;addr_MPosR;addr_MDMoR;addr_MCCR;addr_MVoTMoR;CalibCurves;CalibUnit;CalibGasName;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;Parameters;master;stringparents;stringchildren;type;secondAlias;"
        self.thePlugin.writeComment("#$CRLF$# Object: " + DeviceTypeName + "$CRLF$#$CRLF$#Config Line : " + DeviceTypeFormat + "$CRLF$#")

        # Set Privileges
        Privileges = eval(self.privilegesFileName).getPrivileges(DeviceTypeName)

        for instance in instancesVector:
            if (instance is not None):

                # Check if user pressed 'cancel' button:
                if AGenerationPlugin.isGenerationInterrupted():
                    return

                deviceNumber = int(int(deviceNumber) + 1)
                deviceNumber = str(deviceNumber)

                # 1. Common Unicos fields
                # 2. Specific fields
                # 3. SCADA Device Data Archiving
                # 4. Data Treatment
                # 5. Addresses computation
                # 6. write the instance information in the database file
                # 7. fill the parameters field

                # 1. Common Unicos fields
                name = Name = Alias = instance.getAttributeData("DeviceIdentification:Name")
                expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
                Description = instance.getAttributeData("DeviceDocumentation:Description")

                Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
                AccessControlDomain = instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain").replace(" ", "")
                Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
                DeviceLinkListSpec = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ", "")
                MaskEvent = instance.getAttributeData("SCADADeviceFunctionals:Mask Event")

                # parameters
                PFSPosOn = instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe").lower()
                PFeedbackSim = instance.getAttributeData("FEDeviceParameters:ParReg:Feedback Off").lower()
                PIhMTot = instance.getAttributeData("FEDeviceParameters:ParReg:Inhibit Totalizer cmd").lower()
                PPercent = instance.getAttributeData("FEDeviceParameters:ParReg:Convert ratio to Unit/time").lower()
                PNoiseFilter = instance.getAttributeData("FEDeviceParameters:ParReg:Noise Filter").lower()
                # PEnRstart 	= instance.getAttributeData ("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip()
                NormalPosition = "0"
                LinkOnOff = instance.getAttributeData("FEDeviceOutputs:Valve Order")

                # Scada Graphics
                Diagnostics = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
                WWWLink = instance.getAttributeData("SCADADeviceGraphics:WWW Link")
                Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
                WidgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
                CC0Name = instance.getAttributeData("SCADADeviceGraphics:CurveName:CC0")
                CC1Name = instance.getAttributeData("SCADADeviceGraphics:CurveName:CC1")
                CC2Name = instance.getAttributeData("SCADADeviceGraphics:CurveName:CC2")
                FlowUnit = instance.getAttributeData("SCADADeviceGraphics:Flow:Unit")
                FlowFormat = instance.getAttributeData("SCADADeviceGraphics:Flow:Format")
                CalibUnit = instance.getAttributeData("SCADADeviceGraphics:Calibration:Unit")
                CalibGasName = instance.getAttributeData("SCADADeviceGraphics:Calibration:Gas Name")
                TotUnit = instance.getAttributeData("SCADADeviceGraphics:Totalizer:Unit")
                TotFormat = instance.getAttributeData("SCADADeviceGraphics:Totalizer:Format")

                # 3. SCADA Device Data Archiving
                ArchiveFlowMode = instance.getAttributeData("SCADADeviceDataArchiving:Flow:Archive Mode")
                ArchiveFlowTime = instance.getAttributeData("SCADADeviceDataArchiving:Flow:Time Filter (s)")
                ArchiveFlowDbType = instance.getAttributeData("SCADADeviceDataArchiving:Flow:Deadband Type")
                ArchiveFlowDbValue = instance.getAttributeData("SCADADeviceDataArchiving:Flow:Deadband Value")
                ArchiveModeMode = instance.getAttributeData("SCADADeviceDataArchiving:Modes:Archive Mode")
                ArchiveTotMode = instance.getAttributeData("SCADADeviceDataArchiving:Totalizer:Archive Mode")
                ArchiveTotTime = instance.getAttributeData("SCADADeviceDataArchiving:Totalizer:Time Filter (s)")
                ArchiveTotDbType = instance.getAttributeData("SCADADeviceDataArchiving:Totalizer:Deadband Type")
                ArchiveTotDbValue = instance.getAttributeData("SCADADeviceDataArchiving:Totalizer:Deadband Value")
                BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
                AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
                EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")
                # Driver
                DriverFlowDbType = instance.getAttributeData("SCADADriverDataSmoothing:Flow:Deadband Type")
                DriverFlowDbValue = instance.getAttributeData("SCADADriverDataSmoothing:Flow:Deadband Value")
                DriverTotDbType = instance.getAttributeData("SCADADriverDataSmoothing:Totalizer:Deadband Type")
                DriverTotDbValue = instance.getAttributeData("SCADADriverDataSmoothing:Totalizer:Deadband Value")

                ChildValve = instance.getAttributeData("FEDeviceOutputs:Valve Order")
                MasterDevice = instance.getAttributeData("LogicDeviceDefinitions:Master")
                ExternalMasterDevice = instance.getAttributeData("LogicDeviceDefinitions:External Master")

                # 4. Data Treatment
                S_ArchiveFlowDbType = ArchiveFlowDbType.lower()
                S_ArchiveFlowMode = ArchiveFlowMode.lower()
                S_DriverFlowDbType = DriverFlowDbType.lower()

                S_ArchiveTotDbType = ArchiveTotDbType.lower()
                S_ArchiveTotMode = ArchiveTotMode.lower()
                S_DriverTotDbType = DriverTotDbType.lower()

                S_ArchiveModeMode = ArchiveModeMode.lower()

                # Build DeviceLinkList and children from related objects
                DeviceLinkList = ""
                # MasterDevice
                if (MasterDevice != ""):
                    MasterDevice = MasterDeviceWinCCOAName = self.thePlugin.getLinkedExpertName(MasterDevice, "ProcessControlObject")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, MasterDeviceWinCCOAName)
                elif (ExternalMasterDevice != ""):
                    MasterDevice = MasterDeviceWinCCOAName = ExternalMasterDevice
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, MasterDeviceWinCCOAName)

                children = []
                # Valve OnOff object!
                if (ChildValve != ""):
                    ChildValveWinCCOAName = self.thePlugin.getLinkedExpertName(LinkOnOff, "OnOff")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, ChildValveWinCCOAName)
                    children.append(ChildValveWinCCOAName)

                # # dependent object Detection

                # Link eventual controller objects to the analog Object
                # Gets all the controller objects that are child of the analog object with some properties
                AllControllers = theRawInstances.findMatchingInstances("Controller", "'#FEDeviceOutputs:Controlled Objects#' != ''")
                theControllerObjects = ArrayList()
                for PID in AllControllers:
                    PIDName = PID.getAttributeData("DeviceIdentification:Name").replace(",", " ")
                    ControlledObjectNames = PID.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ")
                    ControlledObjectList = ControlledObjectNames.split()
                    for ControlledObjectName in ControlledObjectList:
                        if (ControlledObjectName == Name):
                            theControllerObjects.add(PIDName)

                inst_PID = theControllerObjects
                for PID_Name in inst_PID:
                    PID_NameWinCCOAName = self.thePlugin.getLinkedExpertName(PID_Name, "Controller")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, PID_NameWinCCOAName)

                # Append Device Link list from Spec
                DeviceLinkList = WinCCOA_CommonMethods.appendDeviceLinkListFromSpec(DeviceLinkListSpec, DeviceLinkList, self.thePlugin, strAllDeviceTypes)

                # Avoid duplications on the deviceLinks
                DeviceLinkList = WinCCOA_CommonMethods.removeDuplicatesAndGivenObjects(DeviceLinkList, [Name, expertName])

                # Default values if domain or nature empty
                if Domain == "":
                    Domain = theApplicationName

                if AccessControlDomain != "":
                    AccessControlDomain = AccessControlDomain + "|"

                if Nature == "":
                    Nature = DeviceTypeName

                # Mask Value
                if (MaskEvent.lower() == "true"):
                    MaskEvent = "0"
                else:
                    MaskEvent = "1"

        #  ---------------------------------------------------------------- Calibration curves
                CalibCurves = "0=" + CC0Name
                if CC1Name != "":
                    CalibCurves = CalibCurves + ",1=" + CC1Name
                if CC2Name != "":
                    CalibCurves = CalibCurves + ",2=" + CC2Name

        #  ---------------------------------------------------------------- Mode Mode

                ArchiveModeTime = "0"		# not required but WinCCOA require it (Call function expect both infos)
                if (S_ArchiveModeMode == "no"):
                    ArchiveModeMode = str("N")
                elif (S_ArchiveModeMode == "old/new comparison"):
                    ArchiveModeMode = str("O")
                else:
                    ArchiveModeMode = str("N")

        #  ---------------------------------------------------------------- Flow
                # Flow : Archive Time Filter
                if ArchiveFlowTime == "":
                    ArchiveFlowTime = "0.0"
                    if S_ArchiveFlowMode == "time" or S_ArchiveFlowMode == "old/new comparison and time" or S_ArchiveFlowMode == "old/new comparison or time" or S_ArchiveFlowMode == "deadband or time" or S_ArchiveFlowMode == "deadband and time":
                        self.thePlugin.writeWarningInUABLog("MassFlowController instance: $Alias$. Archive Time Filter is required by the Archive Mode selected: forced to ($ArchiveFlowTime$).")
                # Flow : Default archive DB in case of Null
                if not (S_ArchiveFlowMode == "no" or S_ArchiveFlowMode == "old/new comparison" or S_ArchiveFlowMode == "time" or S_ArchiveFlowMode == "old/new comparison and time" or S_ArchiveFlowMode == "old/new comparison or time"):
                    if S_ArchiveFlowDbType == "" or ArchiveFlowDbValue == "":
                        S_ArchiveFlowDbType = "relative"
                        ArchiveFlowDbValue = "5.0"
                        self.thePlugin.writeWarningInUABLog("MassFlowController instance: $Alias$. Archive Deadband Type and Value are required : forced to ($S_ArchiveFlowDbType$ $ArchiveFlowDbValue$ %).")

                # Flow Driver Deadband Value
                if DriverFlowDbValue == "":
                    DriverFlowDbValue = "0.0"
                    if S_DriverFlowDbType == "absolute" or S_DriverFlowDbType == "relative":
                        self.thePlugin.writeWarningInUABLog("MassFlowController instance: $Alias$. Driver Deadband Value is required : forced to ($DriverFlowDbValue$).")

                # Flow Driver Deadband versus Archive Deadband
                if (S_DriverFlowDbType == "absolute" or S_DriverFlowDbType == "relative") and (S_ArchiveFlowMode == "deadband" or S_ArchiveFlowMode == "deadband and time" or S_ArchiveFlowMode == "deadband or time"):
                    if S_DriverFlowDbType == S_ArchiveFlowDbType:  # both are of the same type relative or absolute
                        if float(DriverFlowDbValue) > float(ArchiveFlowDbValue):
                            self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Driver Deadband ($DriverFlowDbValue$) and Archive Deadband ($ArchiveFlowDbValue$) are not consistent.")
                    else:
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Driver Deadband and Archive Deadband are not of the same type (relative or aboslute).")

                # this cannot be processes because, the RangeMax for the counter is unknown, the RangeMin is always 0

                # Flow Driver Smooting
                if (S_DriverFlowDbType == "no"):
                    DriverFlowDbType = str("0")
                elif (S_DriverFlowDbType == "absolute"):
                    DriverFlowDbType = str("1")
                elif (S_DriverFlowDbType == "relative"):
                    DriverFlowDbType = str("2")
                elif (S_DriverFlowDbType == "old/new"):
                    DriverFlowDbType = str("3")
                else:
                    DriverFlowDbType = str("0")

                # Flow Archiving Mode
                if (S_ArchiveFlowMode == "no"):
                    ArchiveFlowMode = str("N")
                elif (S_ArchiveFlowMode == "deadband" and S_ArchiveFlowDbType == "absolute"):
                    ArchiveFlowMode = str("VA,$ArchiveFlowDbValue$,N")
                elif (S_ArchiveFlowMode == "deadband" and S_ArchiveFlowDbType == "relative"):
                    ArchiveFlowMode = str("VR,$ArchiveFlowDbValue$,N")
                elif (S_ArchiveFlowMode == "time"):
                    ArchiveFlowMode = str("Y")
                elif (S_ArchiveFlowMode == "deadband and time" and S_ArchiveFlowDbType == "absolute"):
                    ArchiveFlowMode = str("VA,$ArchiveFlowDbValue$,A")
                elif (S_ArchiveFlowMode == "deadband and time" and S_ArchiveFlowDbType == "relative"):
                    ArchiveFlowMode = str("VR,$ArchiveFlowDbValue$,A")
                elif (S_ArchiveFlowMode == "deadband or time" and S_ArchiveFlowDbType == "absolute"):
                    ArchiveFlowMode = str("VA,$ArchiveFlowDbValue$,O")
                elif (S_ArchiveFlowMode == "deadband or time" and S_ArchiveFlowDbType == "relative"):
                    ArchiveFlowMode = str("VR,$ArchiveFlowDbValue$,O")
                elif (S_ArchiveFlowMode == "old/new comparison"):
                    ArchiveFlowMode = str("O")
                    ArchiveFlowTime = "0"
                elif (S_ArchiveFlowMode == "old/new comparison and time"):
                    ArchiveFlowMode = str("A")
                elif (S_ArchiveFlowMode == "old/new comparison or time"):
                    ArchiveFlowMode = str("O")
                else:
                    ArchiveFlowMode = str("N")

        #  ---------------------------------------------------------------- totalizer
                # Totalizer : Archive Time Filter
                if ArchiveTotTime == "":
                    ArchiveTotTime = "0.0"
                    if S_ArchiveTotMode == "time" or S_ArchiveTotMode == "old/new comparison and time" or S_ArchiveTotMode == "old/new comparison or time" or S_ArchiveTotMode == "deadband or time" or S_ArchiveTotMode == "deadband and time":
                        self.thePlugin.writeWarningInUABLog("MassFlowController instance: $Alias$. Archive Time Filter is required by the Archive Mode selected: forced to ($ArchiveTotTime$).")
                # Totalizer : Default archive DB in case of Null
                if not (S_ArchiveTotMode == "no" or S_ArchiveTotMode == "old/new comparison" or S_ArchiveTotMode == "time" or S_ArchiveTotMode == "old/new comparison and time" or S_ArchiveTotMode == "old/new comparison or time"):
                    if S_ArchiveTotDbType == "" or ArchiveTotDbValue == "":
                        S_ArchiveTotDbType = "relative"
                        ArchiveTotDbValue = "5.0"
                        self.thePlugin.writeWarningInUABLog("MassFlowController instance: $Alias$. Archive Deadband Type and Value are required : forced to ($S_ArchiveTotDbType$ $ArchiveTotDbValue$ %).")

                # Totalizer Driver Deadband Value
                if DriverTotDbValue == "":
                    DriverTotDbValue = "0.0"
                    if S_DriverTotDbType == "absolute" or S_DriverTotDbType == "relative":
                        self.thePlugin.writeWarningInUABLog("MassFlowController instance: $Alias$. Driver Deadband Value is required : forced to ($DriverTotDbValue$).")

                # Totalizer Driver Deadband versus Archive Deadband
                if (S_DriverTotDbType == "absolute" or S_DriverTotDbType == "relative") and (S_ArchiveTotMode == "deadband" or S_ArchiveTotMode == "deadband and time" or S_ArchiveTotMode == "deadband or time"):
                    if S_DriverTotDbType == S_ArchiveTotDbType:  # both are of the same type relative or absolute
                        if float(DriverTotDbValue) > float(ArchiveTotDbValue):
                            self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Driver Deadband ($DriverTotDbValue$) and Archive Deadband ($ArchiveTotDbValue$) are not consistent.")
                    else:
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Driver Deadband and Archive Deadband are not of the same type (relative or aboslute).")

                # Totalizer Driver Smooting
                if (S_DriverTotDbType == "no"):
                    DriverTotalizerDbType = str("0")
                elif (S_DriverTotDbType == "absolute"):
                    DriverTotalizerDbType = str("1")
                elif (S_DriverTotDbType == "relative"):
                    DriverTotalizerDbType = str("2")
                elif (S_DriverTotDbType == "old/new"):
                    DriverTotalizerDbType = str("3")
                else:
                    DriverTotalizerDbType = str("0")

                # Totalizer Archiving Mode
                if (S_ArchiveTotMode == "no"):
                    ArchiveTotalizerMode = str("N")
                elif (S_ArchiveTotMode == "deadband" and S_ArchiveTotDbType == "absolute"):
                    ArchiveTotalizerMode = str("VA,$ArchiveTotDbValue$,N")
                elif (S_ArchiveTotMode == "deadband" and S_ArchiveTotDbType == "relative"):
                    ArchiveTotalizerMode = str("VR,$ArchiveTotDbValue$,N")
                elif (S_ArchiveTotMode == "time"):
                    ArchiveTotalizerMode = str("Y")
                elif (S_ArchiveTotMode == "deadband and time" and S_ArchiveTotDbType == "absolute"):
                    ArchiveTotalizerMode = str("VA,$ArchiveTotDbValue$,A")
                elif (S_ArchiveTotMode == "deadband and time" and S_ArchiveTotDbType == "relative"):
                    ArchiveTotalizerMode = str("VR,$ArchiveTotDbValue$,A")
                elif (S_ArchiveTotMode == "deadband or time" and S_ArchiveTotDbType == "absolute"):
                    ArchiveTotalizerMode = str("VA,$ArchiveTotDbValue$,O")
                elif (S_ArchiveTotMode == "deadband or time" and S_ArchiveTotDbType == "relative"):
                    ArchiveTotalizerMode = str("VR,$ArchiveTotDbValue$,O")
                elif (S_ArchiveTotMode == "old/new comparison"):
                    ArchiveTotalizerMode = str("O")
                    ArchiveTotTime = "0"
                elif (S_ArchiveTotMode == "old/new comparison and time"):
                    ArchiveTotalizerMode = str("A")
                elif (S_ArchiveTotMode == "old/new comparison or time"):
                    ArchiveTotalizerMode = str("O")
                else:
                    ArchiveTotalizerMode = str("N")

                # 5. Addresses computation
                addr_ManReg01 = self.getAddressSCADA("$Alias$_ManReg01", PLCType)
                addr_MPosR = self.getAddressSCADA("$Alias$_MPosR", PLCType)
                addr_MDMoR = self.getAddressSCADA("$Alias$_MDMoR", PLCType)
                addr_MCCR = self.getAddressSCADA("$Alias$_MCCR", PLCType)
                addr_MVoTMoR = self.getAddressSCADA("$Alias$_MVoTMoR", PLCType)

                addr_AuDMoRSt = self.getAddressSCADA("$Alias$_AuDMoRSt", PLCType)
                addr_AuVoTMRSt = self.getAddressSCADA("$Alias$_AuVoTMRSt", PLCType)
                addr_AuCCRSt = self.getAddressSCADA("$Alias$_AuCCRSt", PLCType)
                addr_AuPosRSt = self.getAddressSCADA("$Alias$_AuPosRSt", PLCType)
                addr_AuVoTOfSt = self.getAddressSCADA("$Alias$_AuVoTOfSt", PLCType)

                addr_VoTSt = self.getAddressSCADA("$Alias$_VoTSt", PLCType)
                addr_ActOutOV = self.getAddressSCADA("$Alias$_ActOutOV", PLCType)
                addr_ActDMo = self.getAddressSCADA("$Alias$_ActDMo", PLCType)
                addr_ActCC = self.getAddressSCADA("$Alias$_ActCC", PLCType)
                addr_ActVoTMo = self.getAddressSCADA("$Alias$_ActVoTMo", PLCType)

                addr_PosSt = self.getAddressSCADA("$Alias$_PosSt", PLCType)
                addr_PosRSt = self.getAddressSCADA("$Alias$_PosRSt", PLCType)
                addr_MaxPosSt = self.getAddressSCADA("$Alias$_MaxPosSt", PLCType)
                addr_MDMoRSt = self.getAddressSCADA("$Alias$_MDMoRSt", PLCType)
                addr_MCCRSt = self.getAddressSCADA("$Alias$_MCCRSt", PLCType)
                addr_MPosRSt = self.getAddressSCADA("$Alias$_MPosRSt", PLCType)
                addr_MVoTMRSt = self.getAddressSCADA("$Alias$_MVoTMRSt", PLCType)
                addr_StsReg01 = self.getAddressSCADA("$Alias$_StsReg01", PLCType)
                addr_StsReg02 = self.getAddressSCADA("$Alias$_StsReg02", PLCType)
                addr_EvStsReg01 = self.getAddressSCADA("$Alias$_EvStsReg01", PLCType)
                addr_EvStsReg02 = self.getAddressSCADA("$Alias$_EvStsReg02", PLCType)

                # 6. New relationship information in all objects:
                PEnRstart = instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip()
                type = ""
                parents = []

                AllControllers = theRawInstances.findMatchingInstances("Controller", "'#FEDeviceOutputs:Controlled Objects#' != ''")
                theControllerObjects = ArrayList()
                for PID in AllControllers:
                    PIDName = PID.getAttributeData("DeviceIdentification:Name").replace(",", " ")
                    ControlledObjectNames = PID.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ")
                    ControlledObjectList = ControlledObjectNames.split()
                    for ControlledObjectName in ControlledObjectList:
                        if (ControlledObjectName == Name):
                            theControllerObjects.add(PIDName)

                inst_PID = theControllerObjects
                for PID_Name in inst_PID:
                    PID_NameWinCCOAName = self.thePlugin.getLinkedExpertName(PID_Name, "Controller")
                    parents.append(PID_NameWinCCOAName)

                uniqueListParents = []
                for parent in parents:
                    if parent not in uniqueListParents:
                        uniqueListParents.append(parent)

                parents = uniqueListParents
                stringparents = ",".join(parents)

                # AA, DA Children
                AlarmChildren = self.theUnicosProject.findMatchingInstances("AnalogAlarm, DigitalAlarm", "$Name$", "")
                for AlarmChild in AlarmChildren:
                    AlarmChild_Name = WinCCOA_CommonMethods.getExpertName(AlarmChild)
                    children.append(AlarmChild_Name)

                uniqueList = []
                for child in children:
                    if child not in uniqueList:
                        uniqueList.append(child)
                children = uniqueList
                stringchildren = ",".join(children)

                # Expert Name Logic
                S_Name = name.strip()
                S_ExpertName = expertName.strip()
                if S_ExpertName == "":
                    secondAlias = Alias
                else:
                    secondAlias = Alias
                    Alias = S_ExpertName

                # 7. Parameters field
                parametersArray = []
                if PFSPosOn == "off/close":
                    parametersArray.append("PFSPosOn=FALSE")
                else:
                    parametersArray.append("PFSPosOn=TRUE")

                if PFeedbackSim == "false":
                    parametersArray.append("PFeedbackSim=FALSE")
                else:
                    parametersArray.append("PFeedbackSim=TRUE")

                if PIhMTot == "false":
                    parametersArray.append("PIhMTot=FALSE")
                else:
                    parametersArray.append("PIhMTot=TRUE")

                if PNoiseFilter == "false":
                    parametersArray.append("PNoiseFilter=FALSE")
                else:
                    parametersArray.append("PNoiseFilter=TRUE")

                if PEnRstart.lower() == "false":
                    parametersArray.append("PEnRstart=FALSE")
                    parametersArray.append("PRstartFS=FALSE")
                elif (PEnRstart.lower() == "true only if full stop disappeared"):
                    parametersArray.append("PEnRstart=TRUE")
                    parametersArray.append("PRstartFS=FALSE")
                else:
                    parametersArray.append("PEnRstart=TRUE")
                    parametersArray.append("PRstartFS=TRUE")

                filterTime = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Filtering Time (s)").replace(" ", ""))
                if (filterTime != ""):
                    parametersArray.append("FIRST_ORDER_FILTER=$filterTime$")

                parameters = ",".join(parametersArray)

                # 8. write the instance information in the database file
                self.thePlugin.writeInstanceInfo("CPC_$DeviceTypeName$;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;$FlowUnit$;$FlowFormat$;$DriverFlowDbValue$;$DriverFlowDbType$;$ArchiveFlowMode$;$ArchiveFlowTime$;$TotUnit$;$TotFormat$;$DriverTotDbValue$;$DriverTotalizerDbType$;$ArchiveTotalizerMode$;$ArchiveTotTime$;$ArchiveModeMode$;$ArchiveModeTime$;$NormalPosition$;$addr_StsReg01$;$addr_StsReg02$;$addr_EvStsReg01$;$addr_EvStsReg02$;$addr_PosSt$;$addr_PosRSt$;$addr_MaxPosSt$;$addr_MPosRSt$;$addr_AuPosRSt$;$addr_AuVoTOfSt$;$addr_AuCCRSt$;$addr_AuDMoRSt$;$addr_AuVoTMRSt$;$addr_MVoTMRSt$;$addr_MDMoRSt$;$addr_MCCRSt$;$addr_VoTSt$;$addr_ActOutOV$;$addr_ActDMo$;$addr_ActCC$;$addr_ActVoTMo$;$addr_ManReg01$;$addr_MPosR$;$addr_MDMoR$;$addr_MCCR$;$addr_MVoTMoR$;$CalibCurves$;$CalibUnit$;$CalibGasName$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$MaskEvent$;$parameters$;$MasterDevice$;$stringparents$;$stringchildren$;$type$;$secondAlias$;")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython $self.theDeviceType$ template")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython $self.theDeviceType$ template")

    def getAddressSCADA(self, DPName, PLCType):
        if PLCType.lower() == "quantum":
            address = str(int(self.thePlugin.computeAddress(DPName)) - 1)
        else:
            address = str(self.thePlugin.computeAddress(DPName))

        return address
