# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Application general check rules
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.utilities import XMLConfigMapper
from research.ch.cern.unicos.plugins.interfaces import APlugin


class ApplicationGeneral_Template(IUnicosTemplate):
    theSemanticVerifier = 0
    thePlugin = 0
    isDataValid = 1

    def initialize(self):
        self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("Application General check rules: initialize")

    def check(self):
        self.thePlugin.writeInUABLog("Application General check rules: check")

    def begin(self):
        self.thePlugin.writeInUABLog("Application General check rules: begin")

    def process(self, *params):
        theXMLConfig = params[0]
        self.thePlugin.writeInUABLog("Application General check rules: processApplicationData")
        theUnicosProject = self.thePlugin.getUnicosProject()

        # Specs version
        specsVersion = theUnicosProject.getProjectDocumentation().getSpecsVersion()
        if specsVersion is None:
            self.thePlugin.writeErrorInUABLog("The spec version is missing.")
            self.thePlugin.writeInUABLog("Please, add the spec version in the ProjectDocumentation worksheet.")
        else:
            try:
                float(specsVersion.strip())
            except Exception:
                self.thePlugin.writeErrorInUABLog("The format of the spec version number ($specsVersion$) is not correct. It must be a number, either integer or real (X.Y). Please correct it in the ProjectDocumentation worksheet.")

        # PLC instance number
        thePlcInstanceNumber = int(0)

        # PLC declarations
        thePlcDeclarations = theXMLConfig.getPLCDeclarations()

        # Checking all the permitted values
        self.theSemanticVerifier.checkPermittedValues(theUnicosProject)

        # check local ID connection number
        thePLCName = thePlcDeclarations.get(0).getName()
        PLCType = theXMLConfig.getPLCParameter(thePLCName + ":PLCType")

        if PLCType.lower() == "s7-1500":
            LocalID = theXMLConfig.getPLCParameter(thePLCName + ":SiemensSpecificParameters:PLCS7Connection:LocalId")
            if len(LocalID) < 3:
                self.thePlugin.writeErrorInUABLog("Wrong Local ID value for S7-1500 PLC: " + LocalID + ". It must be greater than or equal to 100.")

    def end(self):
        self.thePlugin.writeInUABLog("Application General check rules: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("Application General check rules: shutdown")
