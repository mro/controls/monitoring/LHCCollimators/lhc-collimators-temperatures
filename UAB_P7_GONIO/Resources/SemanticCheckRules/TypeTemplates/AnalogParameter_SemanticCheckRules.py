# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

import SemanticCheckRules_CommonMethods
reload(SemanticCheckRules_CommonMethods)


class AnalogParameter_Template(IUnicosTemplate):
    theSemanticVerifier = 0
    thePlugin = 0
    isDataValid = 1
    theCurrentDeviceType = 0

    def initialize(self):
        self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("check rules: initialize")

    def check(self):
        self.thePlugin.writeInUABLog("check rules: check")

    def begin(self):
        self.thePlugin.writeInUABLog("check rules: begin")

    def process(self, *params):
        theCurrentDeviceTypeName = params[0]
        theCurrentDeviceTypeDefinition = params[1]
        theUnicosProject = self.thePlugin.getUnicosProject()
        theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        self.thePlugin.writeInUABLog("" + str(theCurrentDeviceTypeName) + " Specific Semantic Rules")

        theManufacturer = self.thePlugin.getPlcManufacturer()
        nameLengthLimit = SemanticCheckRules_CommonMethods.getMaxNameLength(self.thePlugin, str(theCurrentDeviceTypeName), theManufacturer)

        # 1. checking the length of the names
        for instance in instancesVector:
            Name = instance.getAttributeData("DeviceIdentification:Name")
            Description = instance.getAttributeData("DeviceDocumentation:Description")
            Remarks = instance.getAttributeData("DeviceDocumentation:Remarks")

            # 1. checking the length of the names
            SemanticCheckRules_CommonMethods.checkNameLength(Name, nameLengthLimit, self.thePlugin, theCurrentDeviceTypeName)

            # Ranges verification
            RangeMin = instance.getAttributeData("FEDeviceParameters:Range Min").strip()
            RangeMax = instance.getAttributeData("FEDeviceParameters:Range Max").strip()

            if RangeMin != "" and RangeMax != "" and float(RangeMin) > float(RangeMax):
                self.thePlugin.writeErrorInUABLog(str(theCurrentDeviceTypeName) + " instance: $Name$. Range Min: " + RangeMin + " is greater than Range Max: " + RangeMax)

    def end(self):
        self.thePlugin.writeInUABLog("check rules: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("check rules: shutdown")
