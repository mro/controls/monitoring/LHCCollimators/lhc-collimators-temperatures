# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.utilities import DeviceTypeFactory
from research.ch.cern.unicos.plugins.interfaces import APlugin

import SemanticCheckRules_CommonMethods
reload(SemanticCheckRules_CommonMethods)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


class DigitalAlarm_Template(IUnicosTemplate):
    theSemanticVerifier = 0
    thePlugin = 0
    isDataValid = 1
    theCurrentDeviceType = 0
    Decorator = None
    theUnicosProject = None

    def initialize(self):
        self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("check rules: initialize")
        self.Decorator = ucpc_library.shared_decorator.ExpressionDecorator()

    def check(self):
        self.thePlugin.writeInUABLog("check rules: check")

    def begin(self):
        self.thePlugin.writeInUABLog("check rules: begin")

    def process(self, *params):
        theCurrentDeviceTypeName = params[0]
        theCurrentDeviceTypeDefinition = params[1]
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        theCurrentDeviceType = self.theUnicosProject.getDeviceType(theCurrentDeviceTypeName)
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        self.thePlugin.writeInUABLog("" + str(theCurrentDeviceTypeName) + " Specific Semantic Rules")
        # Take the list with the differents types deffined in the DA deviceTypeDefinition
        deviceInstance = DeviceTypeFactory.getInstance()
        theDeviceTypeDefinition = deviceInstance.getDeviceType(theCurrentDeviceTypeName)
        deviceTypeFamilies = theDeviceTypeDefinition.getAttributeFamily()
        for deviceTypeFamily in deviceTypeFamilies:
            familyName = deviceTypeFamily.getAttributeFamilyName()
            if familyName == "FEDeviceAlarm":
                attributes = deviceTypeFamily.getAttribute()
                for attribute in attributes:
                    attributeName = attribute.getAttributeName()
                    if attributeName == None:
                        self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: There is not an attribute in the attributeFamily " + familyName)
                        return
                    if attributeName == "Type":
                        isSpecificationAttribute = attribute.getIsSpecificationAttribute()
                        if isSpecificationAttribute == None:
                            self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: There is not isSpecificationAttribute in the attribute " + attributeName)
                            return
                        permittedValueList = isSpecificationAttribute.getPermittedValue()

                break

        theManufacturer = self.thePlugin.getPlcManufacturer()
        nameLengthLimit = SemanticCheckRules_CommonMethods.getMaxNameLength(self.thePlugin, str(theCurrentDeviceTypeName), theManufacturer)

        # Specific semantic rules
        for instance in instancesVector:
            Name = instance.getAttributeData("DeviceIdentification:Name")
            Input = instance.getAttributeData("FEDeviceEnvironmentInputs:Input")
            Delay = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)")
            Description = instance.getAttributeData("DeviceDocumentation:Description")
            Remarks = instance.getAttributeData("DeviceDocumentation:Remarks")
            # EnableCondition = instance.getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter6")  # this should be replaced with a new column in the spec

            # 1. checking the length of the names
            SemanticCheckRules_CommonMethods.checkNameLength(Name, nameLengthLimit, self.thePlugin, theCurrentDeviceTypeName)

            # Delay Alarm verification
            if self.thePlugin.isString(Delay) and Delay.strip().lower() <> "logic" and Delay.strip().lower() <> "":
                DelayParamExist = self.theSemanticVerifier.doesObjectExist(Delay, self.theUnicosProject)
                if DelayParamExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: " + Name + ". The Delay Alarm $Delay$, defined as an AnalogParameter or an AnalogStatus, doesn't exist in the device " + str(theCurrentDeviceTypeName) + "")
            elif not self.thePlugin.isString(Delay):
                if (theManufacturer.lower() == "siemens") and (round(float(Delay)) != float(Delay)):
                    self.thePlugin.writeWarningInUABLog("" + str(theCurrentDeviceTypeName) + " instance: " + Name + ". The Delay Alarm time $Delay$ sec, is not an integer. It will be rounded to " + str(int(round(float(Delay)))) + " sec due to Siemens limitation of non integer delay times")

            # FEDevice inputs verification
            if self.thePlugin.isString(Input) and (Input.lower() != "logic") and (Input != ""):
                self.Decorator.plcExpressionSemanticCheck(self.theSemanticVerifier, self.theUnicosProject, Input, theCurrentDeviceTypeName, Name, 'Input', True)

            # if self.thePlugin.isString(EnableCondition) and (EnableCondition.lower() != "logic") and (EnableCondition!=""):
            #    self.Decorator.plcExpressionSemanticCheck(self.theSemanticVerifier, self.theUnicosProject, EnableCondition, theCurrentDeviceTypeName, Name)

        # Alarm Type verification. Now Multiple alarm definition is allowed
            typeAlarm = instance.getAttributeData("FEDeviceAlarm:Type").replace(",", " ")
            alarmMaster = instance.getAttributeData("LogicDeviceDefinitions:Master").replace(",", " ")
            MultipleTypesAlarm = instance.getAttributeData("FEDeviceAlarm:Multiple Types").replace(",", " ")

            # check if masters are not IO objects
            for master in alarmMaster.split():
                masterInstance = self.theUnicosProject.findInstanceByName(master)
                masterDeviceTypeName = masterInstance.getDeviceTypeName()
                if masterDeviceTypeName in ["DigitalInput", "AnalogInput", "AnalogInputReal", "Encoder", "DigitalOutput", "AnalogOutput", "AnalogOutputReal", "DigitalParameter", "AnalogParameter", "WordParameter", "WordStatus", "AnalogStatus", "Local", "Controller", "AnalogAlarm", "DigitalAlarm"]:
                    self.thePlugin.writeErrorInUABLog(str(theCurrentDeviceTypeName) + " instance: " + Name + ". Master cannot be of type: " + masterDeviceTypeName)

            alarmMasterList = alarmMaster.split()
            alarmMasterListLen = len(alarmMasterList)

        # Multiple Alarms
            if (typeAlarm == "Multiple"):
                typeAlarmList = MultipleTypesAlarm.split()
                typeAlarmListLen = len(typeAlarmList)

                # Checking if the numbers of Masters and Types are the same
                if ((typeAlarmListLen <> alarmMasterListLen) and (alarmMaster <> "")):
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: " + Name + ". The numbers of Masters and Types are not the same.")

                # Checking if all the Alarm Types defined in the EXCEL file are defined in the deviceTypeDefinition
                for type in typeAlarmList:
                    if type not in permittedValueList:
                        self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: " + Name + ". The Alarm type " + str(type) + " is not defined in the deviceType")
        # Single Alarm
            else:
                if alarmMasterListLen > 1:
                    self.thePlugin.writeErrorInUABLog(str(theCurrentDeviceTypeName) + " instance: " + Name + ". Alarm has " + str(alarmMasterListLen) + " masters defined, but its type is not set to multiple.")

                if typeAlarm == "" and alarmMaster <> "":
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: " + Name + ". When the Alarm master is defined the alarm type is mandatory.")
            # Checking if a master is defined when the alarm is a FS, TS or SI
                if (typeAlarm == "FS" or typeAlarm == "TS" or typeAlarm == "SI") and alarmMaster == "":
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: " + Name + ". When the Alarm is a FS, TS or SI the master definition is mandatory.")

                if (typeAlarm not in permittedValueList) and typeAlarm <> "":
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: " + Name + ". The Alarm type $typeAlarm$ is not defined in the deviceType")
                if MultipleTypesAlarm <> "":
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: " + Name + ". This Alarm has been defined as a simple alarm ($typeAlarm$). Then it's not allowed to add some information in the column: Multiple Types.")

    def end(self):
        self.thePlugin.writeInUABLog("check rules: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("check rules: shutdown")
