# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

import SemanticCheckRules_CommonMethods
reload(SemanticCheckRules_CommonMethods)


class AnalogDigital_Template(IUnicosTemplate):
    theSemanticVerifier = 0
    thePlugin = 0
    isDataValid = 1
    theCurrentDeviceType = 0

    def initialize(self):
        self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("check rules: initialize")

    def check(self):
        self.thePlugin.writeInUABLog("check rules: check")

    def begin(self):
        self.thePlugin.writeInUABLog("check rules: begin")

    def process(self, *params):
        theCurrentDeviceTypeName = params[0]
        theCurrentDeviceTypeDefinition = params[1]
        theUnicosProject = self.thePlugin.getUnicosProject()
        theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        self.thePlugin.writeInUABLog("" + str(theCurrentDeviceTypeName) + " Specific Semantic Rules")

        theManufacturer = self.thePlugin.getPlcManufacturer()
        nameLengthLimit = SemanticCheckRules_CommonMethods.getMaxNameLength(self.thePlugin, str(theCurrentDeviceTypeName), theManufacturer)

        # 1. checking the length of the names
        for instance in instancesVector:
            Name = instance.getAttributeData("DeviceIdentification:Name")
            Description = instance.getAttributeData("DeviceDocumentation:Description")
            Remarks = instance.getAttributeData("DeviceDocumentation:Remarks")

            # 1. checking the length of the names
            SemanticCheckRules_CommonMethods.checkNameLength(Name, nameLengthLimit, self.thePlugin, theCurrentDeviceTypeName)

            # FEDeviceParameters verification
            SemanticCheckRules_CommonMethods.checkIfSpecifiedObjectExists(self.thePlugin, instance, "FEDeviceParameters:Warning Time Delay (s)", self.theSemanticVerifier, theUnicosProject, theCurrentDeviceTypeName, Name)
            SemanticCheckRules_CommonMethods.checkIfSpecifiedObjectExists(self.thePlugin, instance, "FEDeviceParameters:Warning Deadband Value (Unit)", self.theSemanticVerifier, theUnicosProject, theCurrentDeviceTypeName, Name)

            # FEDevice inputs verification
            FeedbackOn = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On").strip()
            FeedbackOff = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off").strip()
            LocalDrive = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Drive").strip()
            FeedbackAnalog = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Analog").strip()
            HardwareAnalogOutput = instance.getAttributeData("FEDeviceEnvironmentInputs:Hardware Analog Output").strip()

            if LocalDrive <> "":
                subString = LocalDrive[0:4].lower()
                if subString == "not ":
                    LocalDrive = LocalDrive[4:]
                else:
                    LocalDrive = LocalDrive

            if FeedbackOn <> "":
                FeedbackOnExist = self.theSemanticVerifier.doesObjectExist(FeedbackOn, theUnicosProject)
                if FeedbackOnExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The FeedbackOn $FeedbackOn$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")
            if FeedbackOff <> "":
                FeedbackOffExist = self.theSemanticVerifier.doesObjectExist(FeedbackOff, theUnicosProject)
                if FeedbackOffExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The FeedbackOff $FeedbackOff$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")
            if LocalDrive <> "":
                LocalDriveExist = self.theSemanticVerifier.doesObjectExist(LocalDrive, theUnicosProject)
                if LocalDriveExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The LocalDrive $LocalDrive$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")
            if FeedbackAnalog <> "":
                FeedbackAnalogExist = self.theSemanticVerifier.doesObjectExist(FeedbackAnalog, theUnicosProject)
                if FeedbackAnalogExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: The FeedbackAnalog $FeedbackAnalog$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")
            if HardwareAnalogOutput <> "":
                HardwareAnalogOutputExist = self.theSemanticVerifier.doesObjectExist(HardwareAnalogOutput, theUnicosProject)
                if HardwareAnalogOutputExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The HardwareAnalogOutput $HardwareAnalogOutput$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")

            # FEDevice outputs verification
            OutputOn = instance.getAttributeData("FEDeviceOutputs:Output On").strip()
            OutputOff = instance.getAttributeData("FEDeviceOutputs:Output Off").strip()
            if OutputOn <> "":
                OutputOnExist = self.theSemanticVerifier.doesObjectExist(OutputOn, theUnicosProject)
                if OutputOnExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The OutputOn $OutputOn$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")
            if OutputOff <> "":
                OutputOffExist = self.theSemanticVerifier.doesObjectExist(OutputOff, theUnicosProject)
                if OutputOffExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The OutputOff $OutputOff$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")

            # Ranges verification
            RangeMin = instance.getAttributeData("FEDeviceParameters:Range Min").strip()
            RangeMax = instance.getAttributeData("FEDeviceParameters:Range Max").strip()

            if RangeMin != "" and RangeMax != "" and float(RangeMin) > float(RangeMax):
                self.thePlugin.writeErrorInUABLog(str(theCurrentDeviceTypeName) + " instance: $Name$. Range Min (" + RangeMin + ") is greater than Range Max (" + RangeMax + ")")

    def end(self):
        self.thePlugin.writeInUABLog("check rules: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("check rules: shutdown")
