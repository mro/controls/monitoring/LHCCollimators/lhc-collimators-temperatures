# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

import SemanticCheckRules_CommonMethods
reload(SemanticCheckRules_CommonMethods)


class WordStatus_Template(IUnicosTemplate):
    theSemanticVerifier = 0
    thePlugin = 0
    isDataValid = 1
    theCurrentDeviceType = 0

    def initialize(self):
        self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("check rules: initialize")

    def check(self):
        self.thePlugin.writeInUABLog("check rules: check")

    def begin(self):
        self.thePlugin.writeInUABLog("check rules: begin")

    def process(self, *params):
        theCurrentDeviceTypeName = params[0]
        theCurrentDeviceTypeDefinition = params[1]
        theUnicosProject = self.thePlugin.getUnicosProject()
        theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()

        self.thePlugin.writeInUABLog("" + str(theCurrentDeviceTypeName) + " Specific Semantic Rules")

        theManufacturer = self.thePlugin.getPlcManufacturer()
        nameLengthLimit = SemanticCheckRules_CommonMethods.getMaxNameLength(self.thePlugin, str(theCurrentDeviceTypeName), theManufacturer)

        for instance in instancesVector:
            Name = instance.getAttributeData("DeviceIdentification:Name")
            Description = instance.getAttributeData("DeviceDocumentation:Description")
            FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type").strip()
            InterfaceParam1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1")
            InterfaceParam2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2")
            S_InterfaceParam1 = InterfaceParam1.lower().strip()
            S_InterfaceParam2 = InterfaceParam2.lower().strip()

            # 1. checking the length of the names
            SemanticCheckRules_CommonMethods.checkNameLength(Name, nameLengthLimit, self.thePlugin, theCurrentDeviceTypeName)

            # 2. Checking the FE Encoding Type
            if (theManufacturer.lower() == "siemens"):
                if (FEType <> "") and (FEType <> "0") and (FEType <> "1") and (FEType <> "101") and (FEType <> "100"):
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The FE Encoding Type defined " + FEType + " is not allowed.")
                elif (FEType == "1"):
                    if S_InterfaceParam1 == "":
                        self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The InterfaceParam1 must be defined if the FE Encoding Type is $FEType$")
                    else:
                        if not (S_InterfaceParam1.startswith('piw') or S_InterfaceParam1.startswith('iw') or S_InterfaceParam1.startswith('pib') or S_InterfaceParam1.startswith('ib')):
                            self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is PIWxxx or IWxxx or PIBxxx or IBxxx, where xxx is a number")
                        else:
                            if not S_InterfaceParam1[S_InterfaceParam1.find('i') + 2:].isnumeric():
                                self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is PIWxxx or IWxxx or PIBxxx or IBxxx, where xxx is a number")

                elif (FEType == "101"):
                    if S_InterfaceParam1 == "" or S_InterfaceParam2 == "":
                        self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. InterfaceParam1 and InterfaceParam2 must *both* be defined if the FE Encoding Type is $FEType$")
                    else:
                        if len(S_InterfaceParam1) > 2 and S_InterfaceParam1[0:2] == "db":
                            S_InterfaceParam1 = S_InterfaceParam1[2:]
                        if len(S_InterfaceParam2) > 3 and (S_InterfaceParam2[0:3] == "dbw" or S_InterfaceParam2[0:3] == "dbb"):
                            S_InterfaceParam2 = S_InterfaceParam2[3:]
                        if not S_InterfaceParam1.isnumeric():
                            self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is DBxx, where xx is a number")
                        if not S_InterfaceParam2.isnumeric():
                            self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The InterfaceParam2 ($InterfaceParam2$) is not well defined. The correct format is DBWxx or DBBxx, where xx is a number")

                elif (FEType == "100"):
                    if S_InterfaceParam1 == "":
                        self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The InterfaceParam1 must be defined if the FE Encoding Type is $FEType$")
                    else:
                        if not (S_InterfaceParam1.startswith('mw') or S_InterfaceParam1.startswith('mb')):
                            self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is MWxxx or MBxxx, where xxx is a number")
                        else:
                            if not S_InterfaceParam1[S_InterfaceParam1.find('m') + 2:].isnumeric():
                                self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is MWxxx or MBxxx, where xxx is a number")
            pattern = instance.getAttributeData("SCADADeviceGraphics:Pattern")
            if (pattern == "-"):
                self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. Use of '-' as a pattern is depricated. Please change it to empty cell ('').")

            # Ranges verification
            RangeMin = instance.getAttributeData("FEDeviceParameters:Range Min").strip()
            RangeMax = instance.getAttributeData("FEDeviceParameters:Range Max").strip()

            if RangeMin != "" and RangeMax != "" and float(RangeMin) > float(RangeMax):
                self.thePlugin.writeErrorInUABLog(str(theCurrentDeviceTypeName) + " instance: $Name$. Range Min (" + RangeMin + ") is greater than Range Max (" + RangeMax + ")")

    def end(self):
        self.thePlugin.writeInUABLog("check rules: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("check rules: shutdown")
