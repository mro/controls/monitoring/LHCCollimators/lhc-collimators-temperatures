# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

import SemanticCheckRules_CommonMethods
reload(SemanticCheckRules_CommonMethods)


class SteppingMotor_Template(IUnicosTemplate):
    theSemanticVerifier = 0
    thePlugin = 0
    isDataValid = 1
    theCurrentDeviceType = 0

    def initialize(self):
        self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("check rules: initialize")

    def check(self):
        self.thePlugin.writeInUABLog("check rules: check")

    def begin(self):
        self.thePlugin.writeInUABLog("check rules: begin")

    def process(self, *params):
        theCurrentDeviceTypeName = params[0]
        theCurrentDeviceTypeDefinition = params[1]
        theUnicosProject = self.thePlugin.getUnicosProject()
        theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        self.thePlugin.writeInUABLog("" + str(theCurrentDeviceTypeName) + " Specific Semantic Rules")

        theManufacturer = self.thePlugin.getPlcManufacturer()
        nameLengthLimit = SemanticCheckRules_CommonMethods.getMaxNameLength(self.thePlugin, str(theCurrentDeviceTypeName), theManufacturer)

        # 1. checking the length of the names
        for instance in instancesVector:
            Name = instance.getAttributeData("DeviceIdentification:Name")
            Description = instance.getAttributeData("DeviceDocumentation:Description")
            Remarks = instance.getAttributeData("DeviceDocumentation:Remarks")
            Switches = instance.getAttributeData("FEDeviceParameters:ParReg:Switches Configuration").strip()
            Feedback = instance.getAttributeData("FEDeviceParameters:ParReg:Feedback").strip()
            FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type").strip()
            InterfaceParam1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1")
            InterfaceParam5 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5")
            S_InterfaceParam1 = InterfaceParam1.lower().strip()
            S_InterfaceParam5 = InterfaceParam5.lower().strip()

            # 1. checking the length of the names
            SemanticCheckRules_CommonMethods.checkNameLength(Name, nameLengthLimit, self.thePlugin, theCurrentDeviceTypeName)

            # FEDevice inputs verification
            FeedbackAnalog = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Analog").strip()
            ClockWiseLimit = instance.getAttributeData("FEDeviceEnvironmentInputs:ClockWise Limit").strip()
            CounterClockWiseLimit = instance.getAttributeData("FEDeviceEnvironmentInputs:CounterClockWise Limit").strip()

            if FeedbackAnalog <> "":
                FeedbackAnalogExist = self.theSemanticVerifier.doesObjectExist(FeedbackAnalog, theUnicosProject)
                if FeedbackAnalogExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The Feedback Analog $FeedbackAnalog$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")

            if ClockWiseLimit <> "":
                ClockWiseLimitExist = self.theSemanticVerifier.doesObjectExist(ClockWiseLimit, theUnicosProject)
                if ClockWiseLimitExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The ClockWise Limit $ClockWiseLimit$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")

            if CounterClockWiseLimit <> "":
                CounterClockWiseLimitExist = self.theSemanticVerifier.doesObjectExist(CounterClockWiseLimit, theUnicosProject)
                if CounterClockWiseLimitExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The CounterClockWise Limit $CounterClockWiseLimit$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")

            if Switches == "2 End Switches + Ref. Switch" or Switches == "2 End Switches with faked Ref. Switch":
                if ClockWiseLimit == "" or CounterClockWiseLimit == "":
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The Switches configuration is defined as $Switches$ and one of them is not defined.")
            else:
                if ClockWiseLimit <> "" or CounterClockWiseLimit <> "":
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The Switches configuration is defined as $Switches$ and there are end switches defined.")

            if Feedback <> "":
                if FeedbackAnalog == "":
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The is a Feedback configured in the parameters but is not defined.")

            if FeedbackAnalog <> "":
                if Feedback == "":
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The is a Feedback defined but is not configured to be used in the parameters.")
            # FEDevice outputs verification
            CWRef = instance.getAttributeData("FEDeviceOutputs:ClockWise to Reference Switch").strip()
            SimRef = instance.getAttributeData("FEDeviceOutputs:Simulated Reference Switch").strip()
            DrvEn = instance.getAttributeData("FEDeviceOutputs:Driver Enable").strip()

            if CWRef <> "":
                CWRefExist = self.theSemanticVerifier.doesObjectExist(CWRef, theUnicosProject)
                if CWRefExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The ClockWise to Reference Switch $CWRef$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")

            if SimRef <> "":
                SimRefExist = self.theSemanticVerifier.doesObjectExist(SimRef, theUnicosProject)
                if SimRefExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The Simulated Reference Switch $SimRef$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")

            if DrvEn <> "":
                DrvEnExist = self.theSemanticVerifier.doesObjectExist(DrvEn, theUnicosProject)
                if DrvEnExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The Driver Enable $DrvEn$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")

            if Feedback == "Encoder":
                if DrvEn == "":
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. There is no Driver Enable defined. Is mandatory when de Feedback is provided by an encoder.")
                if SimRef == "":
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. There is no Simulated Reference Switch defined. Is mandatory when the Feedback is provided by an encoder.")

            if Switches == "2 End Switches with faked Ref. Switch":
                if CWRef == "" or SimRef == "":
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The Simulated Reference Switch or ClockWise to Reference Switch is not defined. They are mandatory when the system is configured with faked Ref. Switch.")
            elif Switches == "2 End Switches + Ref. Switch" or Switches == "2 Switches plugged into the 1STEP":
                if CWRef <> "" or SimRef <> "":
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. There are digital outputs defined for simulate the reference Switch but they are not needed with this Switch configuartion.")
            # FEDevice IOConfig verifications
            if (FEType <> "") and (FEType <> "1"):
                self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The FE Encoding Type defined " + FEType + " is not allowed.")
            else:
                if (FEType == "1"):
                    if S_InterfaceParam1 == "":
                        self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. InterfaceParam1 must be defined if the FE Encoding Type is $FEType$")
                    else:
                        if not S_InterfaceParam1.startswith('pid'):
                            self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is a DOUBLE WORD (PIDxxx where xxx is a number)")
                        else:
                            if not S_InterfaceParam1[S_InterfaceParam1.find('id') + 2:].isnumeric():
                                self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The InterfaceParam1 ($InterfaceParam1$) is not well defined. The correct format is a DOUBLE WORD (PIDxxx where xxx is a number)")

                    if S_InterfaceParam5 == "":
                        self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. InterfaceParam5 must be defined if the FE Encoding Type is $FEType$")
                    else:
                        if not S_InterfaceParam5.startswith('pqd'):
                            self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The InterfaceParam5 ($InterfaceParam5$) is not well defined. The correct format is a DOUBLE WORD (PQDxxx where xxx is a number)")
                        else:
                            if not S_InterfaceParam5[S_InterfaceParam5.find('qd') + 2:].isnumeric():
                                self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The InterfaceParam5 ($InterfaceParam5$) is not well defined. The correct format is a DOUBLE WORD (PQDxxx where xxx is a number)")

    def end(self):
        self.thePlugin.writeInUABLog("check rules: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("check rules: shutdown")
