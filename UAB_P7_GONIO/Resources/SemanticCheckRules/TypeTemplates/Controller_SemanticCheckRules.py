# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin

import SemanticCheckRules_CommonMethods
reload(SemanticCheckRules_CommonMethods)


class Controller_Template(IUnicosTemplate):
    theSemanticVerifier = 0
    thePlugin = 0
    isDataValid = 1
    theCurrentDeviceType = 0

    def initialize(self):
        self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("check rules: initialize")

    def check(self):
        self.thePlugin.writeInUABLog("check rules: check")

    def begin(self):
        self.thePlugin.writeInUABLog("check rules: begin")

    def process(self, *params):
        theCurrentDeviceTypeName = params[0]
        theCurrentDeviceTypeDefinition = params[1]
        theUnicosProject = self.thePlugin.getUnicosProject()
        theCurrentDeviceType = theUnicosProject.getDeviceType(theCurrentDeviceTypeName)
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        self.thePlugin.writeInUABLog("" + str(theCurrentDeviceTypeName) + " Specific Semantic Rules")

        theManufacturer = self.thePlugin.getPlcManufacturer()
        nameLengthLimit = SemanticCheckRules_CommonMethods.getMaxNameLength(self.thePlugin, str(theCurrentDeviceTypeName), theManufacturer)

        # 1. checking the length of the names
        for instance in instancesVector:
            Name = instance.getAttributeData("DeviceIdentification:Name")
            Description = instance.getAttributeData("DeviceDocumentation:Description")
            Remarks = instance.getAttributeData("DeviceDocumentation:Remarks")

            # 1. checking the length of the names
            SemanticCheckRules_CommonMethods.checkNameLength(Name, nameLengthLimit, self.thePlugin, theCurrentDeviceTypeName)

            # FEDevice inputs verification
            MeasuredValue = instance.getAttributeData("FEDeviceEnvironmentInputs:Measured Value").strip()
            if MeasuredValue <> "":
                MeasuredValueExist = self.theSemanticVerifier.doesObjectExist(MeasuredValue, theUnicosProject)
                if MeasuredValueExist is not True:
                    self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The MeasuredValue $MeasuredValue$ doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")

            # FEDevice outputs verification
            ControlledObjects = instance.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ")
            ControlledObjectsList = ControlledObjects.split()
            for ControlledObject in ControlledObjectsList:
                if ControlledObject <> "":
                    ControlledObjectExist = self.theSemanticVerifier.doesObjectExist(ControlledObject, theUnicosProject)
                    if ControlledObjectExist is not True:
                        self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. The ControlledObject1 $ControlledObject$  doesn't exist in the device " + str(theCurrentDeviceTypeName) + ".")

            # Output Range verification
            OutputRangeMin = instance.getAttributeData("FEDeviceParameters:Controller Parameters:Output Range Min")
            OutputRangeMax = instance.getAttributeData("FEDeviceParameters:Controller Parameters:Output Range Max")
            if OutputRangeMin == "" and OutputRangeMax <> "":
                self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. If the Output Range Max is defined, the Output Range Min must be defined.")
            if OutputRangeMin <> "" and OutputRangeMax == "":
                self.thePlugin.writeErrorInUABLog("" + str(theCurrentDeviceTypeName) + " instance: $Name$. If the Output Range Min is defined, the Output Range Max must be defined.")

            # Ranges verification
            if OutputRangeMin != "" and OutputRangeMax != "" and float(OutputRangeMin) > float(OutputRangeMax):
                self.thePlugin.writeErrorInUABLog(str(theCurrentDeviceTypeName) + " instance: $Name$. Output Range Min (" + OutputRangeMin + ") is greater than Output Range Max (" + OutputRangeMax + ")")

            # Check default PID parameters exist if specified in spec
            SemanticCheckRules_CommonMethods.checkIfSpecifiedObjectExists(self.thePlugin, instance, "FEDeviceVariables:Default PID Parameters:Setpoint", self.theSemanticVerifier, theUnicosProject, theCurrentDeviceTypeName, Name)
            SemanticCheckRules_CommonMethods.checkIfSpecifiedObjectExists(self.thePlugin, instance, "FEDeviceVariables:Default PID Parameters:Kc", self.theSemanticVerifier, theUnicosProject, theCurrentDeviceTypeName, Name)
            SemanticCheckRules_CommonMethods.checkIfSpecifiedObjectExists(self.thePlugin, instance, "FEDeviceVariables:Default PID Parameters:Ti", self.theSemanticVerifier, theUnicosProject, theCurrentDeviceTypeName, Name)
            SemanticCheckRules_CommonMethods.checkIfSpecifiedObjectExists(self.thePlugin, instance, "FEDeviceVariables:Default PID Parameters:Td", self.theSemanticVerifier, theUnicosProject, theCurrentDeviceTypeName, Name)
            SemanticCheckRules_CommonMethods.checkIfSpecifiedObjectExists(self.thePlugin, instance, "FEDeviceVariables:Default PID Parameters:Tds", self.theSemanticVerifier, theUnicosProject, theCurrentDeviceTypeName, Name)
            SemanticCheckRules_CommonMethods.checkIfSpecifiedObjectExists(self.thePlugin, instance, "FEDeviceVariables:Default PID Parameters:SP High Limit", self.theSemanticVerifier, theUnicosProject, theCurrentDeviceTypeName, Name)
            SemanticCheckRules_CommonMethods.checkIfSpecifiedObjectExists(self.thePlugin, instance, "FEDeviceVariables:Default PID Parameters:SP Low Limit", self.theSemanticVerifier, theUnicosProject, theCurrentDeviceTypeName, Name)
            SemanticCheckRules_CommonMethods.checkIfSpecifiedObjectExists(self.thePlugin, instance, "FEDeviceVariables:Default PID Parameters:Out High Limit", self.theSemanticVerifier, theUnicosProject, theCurrentDeviceTypeName, Name)
            SemanticCheckRules_CommonMethods.checkIfSpecifiedObjectExists(self.thePlugin, instance, "FEDeviceVariables:Default PID Parameters:Out Low Limit", self.theSemanticVerifier, theUnicosProject, theCurrentDeviceTypeName, Name)

    def end(self):
        self.thePlugin.writeInUABLog("check rules: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("check rules: shutdown")
