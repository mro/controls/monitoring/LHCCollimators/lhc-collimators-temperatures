# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class ProcessControlObject_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        params = {
            'spec_version': spec_version,
            'instance_amount': instance_list.size(),
            'TYPE_ana_Status': [],
            'TYPE_ManRequest': [],
            'TYPE_bin_Status': [],
            'TYPE_event': [],
            'DB_ManRequest': [],
            'DB_bin_status': [],
            'DB_ana_status': [],
            'ASSIGNMENT': [],
            'INSTANCE_DB': []
        }

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        for instance in instance_list:
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['DB_bin_status'].append('''      %s : "PCO_bin_Status";''' % name)
            params['DB_ana_status'].append('''      %s : "PCO_ana_Status";''' % name)
            params['INSTANCE_DB'].append(self.get_instance_db(instance, name))

        self.writeDeviceInstances(self.process_template(".scl", params))

    @staticmethod
    def get_allowance_mode(instance, spec_field):
        allowance_mode = instance.getAttributeData(spec_field).strip()
        if allowance_mode == "":
            allowance_mode = '00000000'
        allowance_mode = str(hex(int(allowance_mode, 2))).replace('0x', '')
        return allowance_mode

    def get_instance_db(self, instance, name):
        instance_db = '''
DATA_BLOCK "%(name)s" "CPC_FB_PCO"
BEGIN
   // new: ParReg for PCO
   PPCO.PArReg := 2#%(PArReg)s;
   POpMoTa[0] := B#16#%(mode1)s;
   POpMoTa[1] := B#16#%(mode2)s;
   POpMoTa[2] := B#16#%(mode3)s;
   POpMoTa[3] := B#16#%(mode4)s;
   POpMoTa[4] := B#16#%(mode5)s;
   POpMoTa[5] := B#16#%(mode6)s;
   POpMoTa[6] := B#16#%(mode7)s;
   POpMoTa[7] := B#16#%(mode8)s;

END_DATA_BLOCK'''

        par_reg = ['0'] * 15
        par_reg[6] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop", off_value="false")
        par_reg[5] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop", on_value="true even if full stop still active")

        params = {
            'name': name,
            'PArReg': "".join(par_reg),
            'mode1': self.get_allowance_mode(instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 1 Allowance"),
            'mode2': self.get_allowance_mode(instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 2 Allowance"),
            'mode3': self.get_allowance_mode(instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 3 Allowance"),
            'mode4': self.get_allowance_mode(instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 4 Allowance"),
            'mode5': self.get_allowance_mode(instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 5 Allowance"),
            'mode6': self.get_allowance_mode(instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 6 Allowance"),
            'mode7': self.get_allowance_mode(instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 7 Allowance"),
            'mode8': self.get_allowance_mode(instance, "FEDeviceParameters:Option Mode Allowance Table:Option Mode 8 Allowance")
        }

        return instance_db % params
