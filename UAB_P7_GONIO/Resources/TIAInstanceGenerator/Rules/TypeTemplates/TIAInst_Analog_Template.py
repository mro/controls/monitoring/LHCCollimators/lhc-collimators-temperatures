# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class Analog_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()
        unicos_project = self.thePlugin.getUnicosProject()

        params = {'spec_version': spec_version,
                  'instance_amount': instance_list.size(),
                  'TYPE_ana_Status': [],
                  'TYPE_ManRequest': [],
                  'TYPE_bin_Status': [],
                  'TYPE_event': [],
                  'DB_ManRequest': [],
                  'DB_bin_status': [],
                  'DB_ana_status': [],
                  'FC_ANALOG': [],
                  'INSTANCE_DB': []
                  }

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['DB_bin_status'].append('''      %s : "ANALOG_bin_Status";''' % name)
            params['DB_ana_status'].append('''      %s : "ANALOG_ana_Status";''' % name)
            params['INSTANCE_DB'].append(self.get_instance_db(instance, name))

        limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "Analog"))
        air_limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "AnalogInputReal"))
        # instance_split contains the map of position and array of devices
        instance_split = {1: []}
        for instance in instance_list:
            pos = self.get_instance_pos(unicos_project, instance, air_limit_size)
            if pos not in instance_split:
                instance_split[pos] = []
            instance_split[pos].append(instance)
        positions = instance_split.keys()
        positions.sort()
        for pos in positions:
            optimized_blocks_amount = len(instance_split[pos]) / (limit_size + 1)
            start_idx = 1 if pos == 1 else pos * 10
            for block_number in range(optimized_blocks_amount + 1):
                begin_index = block_number * limit_size
                end_index = begin_index + limit_size
                instance_split_list = instance_split[pos][begin_index:end_index]
                params['FC_ANALOG'].append(self.get_fc_block(instance_split_list, start_idx + block_number))

        for idx, instance in enumerate(instance_list, 1):
            range_min, range_max = self.get_range(self.thePlugin, instance)
            range_threshold = 0.1 * (float(range_max) - float(range_min))
            limit_off = self.spec.get_attribute_value(instance, "FEDeviceManualRequests:Parameter Limit Off/Closed", float(range_min) + range_threshold)
            limit_on = self.spec.get_attribute_value(instance, "FEDeviceManualRequests:Parameter Limit On/Open", float(range_max) - range_threshold)
            params['DB_ManRequest'].append('''   ANALOG_Requests[%s].PLiOn := %s;''' % (idx, limit_on))
            params['DB_ManRequest'].append('''   ANALOG_Requests[%s].PLiOff := %s;''' % (idx, limit_off))

        self.writeDeviceInstances(self.process_template(".scl", params))

    @staticmethod
    def get_range(plugin, instance):
        range_min = "0.0"
        range_max = "100.0"
        analog_process_output = instance.getAttributeData("FEDeviceOutputs:Process Output")
        if analog_process_output != "":
            unicos_project = plugin.getUnicosProject()
            linked_devices = unicos_project.findMatchingInstances("AnalogOutput, AnalogOutputReal", "'#DeviceIdentification:Name#'='%s'" % analog_process_output)
            if len(linked_devices) > 0:
                range_min = plugin.formatNumberPLC(linked_devices[0].getAttributeData("FEDeviceParameters:Range Min"))
                range_max = plugin.formatNumberPLC(linked_devices[0].getAttributeData("FEDeviceParameters:Range Max"))

        return range_min, range_max

    def get_fc_block(self, instance_list, block_number):
        fc_block = '''(*EXEC of ANALOG%(block_number)s************************************************)
FUNCTION "FC_ANALOG%(block_number)s" : Void
TITLE = 'FC_ANALOG%(block_number)s'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : UNICOS
FAMILY : Analog
NAME : CallANA
//
// ANALOG calls
   VAR_TEMP 
      old_status1 : DWord;
      old_status2 : DWord;
   END_VAR


BEGIN
%(ASSIGNMENT)s
END_FUNCTION
'''
        if block_number == 1:
            block_number = ""
        params = {
            'block_number': block_number
        }
        params['ASSIGNMENT'] = []
        for instance in instance_list:
            params['ASSIGNMENT'].append(self.get_instance_assignment(instance))
        params['ASSIGNMENT'] = "\n".join(params['ASSIGNMENT'])
        return fc_block % params

    def get_instance_assignment(self, instance):
        instance_assignment = '''	// ----------------------------------------------
	// ---- Analog <%(index)s>: %(name)s
	// ----------------------------------------------
	#old_status1 := "DB_bin_status_ANALOG".%(name)s.StsReg01;
	#old_status2 := "DB_bin_status_ANALOG".%(name)s.StsReg02;
	#old_status1 := ROR(IN := #old_status1, N := 16);
	#old_status2 := ROR(IN := #old_status2, N := 16);
	
	"%(name)s".Manreg01 := "DB_ANALOG_ManRequest".ANALOG_Requests[%(index)s].ManReg01;
	"%(name)s".MPosR := "DB_ANALOG_ManRequest".ANALOG_Requests[%(index)s].MPosR;
	"%(name)s".PliOn := "DB_ANALOG_ManRequest".ANALOG_Requests[%(index)s].PLiOn;
	"%(name)s".PliOff := "DB_ANALOG_ManRequest".ANALOG_Requests[%(index)s].PLiOff;
	
	%(PWDt_link)s
	%(PWDb_link)s
	%(HFOn_link)s
	%(HFOff_link)s
	%(HFPos_link)s
	%(HLD_link)s
	// IO Error
	%(io_error)s
	
	// IOSimu
	%(io_simu)s
	
	// Calls the Baseline function
	"%(name)s"();
	%(OutOV_link)s
	
	//Reset AuAuMoR and AuAlAck
	"%(name)s".AuAuMoR := FALSE;
	"%(name)s".AuAlAck := FALSE;
	"%(name)s".AuRstart := FALSE;
	
	//Recopy new status
	"DB_bin_status_ANALOG".%(name)s.StsReg01 := "%(name)s".Stsreg01;
	"DB_bin_status_ANALOG".%(name)s.StsReg02 := "%(name)s".Stsreg02;
	"DB_ana_status_ANALOG".%(name)s.PosSt := "%(name)s".PosSt;
	"DB_ana_status_ANALOG".%(name)s.AuPosRSt := "%(name)s".AuPosRSt;
	"DB_ana_status_ANALOG".%(name)s.MPosRSt := "%(name)s".MPosRSt;
	"DB_ana_status_ANALOG".%(name)s.PosRSt := "%(name)s".PosRSt;
	"DB_Event_ANALOG".ANALOG_evstsreg[%(index)s].evStsReg01 := #old_status1 OR "DB_bin_status_ANALOG".%(name)s.StsReg01;
	"DB_Event_ANALOG".ANALOG_evstsreg[%(index)s].evStsReg02 := #old_status2 OR "DB_bin_status_ANALOG".%(name)s.StsReg02;
	
	
'''
        name = instance.getAttributeData("DeviceIdentification:Name")
        params = {
            'name': name,
            'index': instance.getInstanceNumber(),
            'PWDt_link': "",
            'PWDb_link': "",
            'HFOn_link': "",
            'HFOff_link': "",
            'HFPos_link': "",
            'HLD_link': "",
            'io_error': "",
            'io_simu': "",
            'OutOV_link': ""
        }

        warning_delay = self.spec.get_s7db_id(instance, "FEDeviceParameters:Warning Time Delay (s)", "AnalogParameter,AnalogStatus,AnalogInput,AnalogInputReal")
        if warning_delay:
            params['PWDt_link'] = '''"%s".PAnalog.PWDt := DINT_TO_TIME(REAL_TO_DINT(%s.PosSt*1000.0));''' % (name, warning_delay)

        deadband = self.spec.get_s7db_id(instance, "FEDeviceParameters:Warning Deadband Value (Unit)", "AnalogParameter,AnalogStatus,AnalogInput,AnalogInputReal")
        if deadband:
            params['PWDb_link'] = '''"%s".PAnalog.PWDb := %s.PosSt;''' % (name, deadband)

        feedback_on = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Feedback On", "DigitalInput")
        if feedback_on:
            params['HFOn_link'] = '''"%s".HFOn := %s.PosSt;''' % (name, feedback_on)

        feedback_off = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Feedback Off", "DigitalInput")
        if feedback_off:
            params['HFOff_link'] = '''"%s".HFOff := %s.PosSt;''' % (name, feedback_off)

        # TODO: use DB_SIMPLE?
        feedback_analog = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Feedback Analog", "AnalogInput, AnalogInputReal, AnalogStatus, WordStatus")
        if feedback_analog:
            params['HFPos_link'] = '''"%s".HFPos := %s.PosSt;''' % (name, feedback_analog)

        local_drive = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Local Drive", "DigitalInput")
        if local_drive:
            params['HLD_link'] = '''"%s".HLD := %s.PosSt;''' % (name, local_drive)

        process_output = self.spec.get_s7db_id(instance, "FEDeviceOutputs:Process Output", "AnalogOutput, AnalogOutputReal")
        if process_output:
            params['OutOV_link'] = '''%s.AuPosR := "%s".OutOV;''' % (process_output, name)

        linked_objects = [feedback_on, feedback_off, feedback_analog, local_drive, process_output]
        params['io_error'], params['io_simu'] = self.get_io_error_and_simu(name, linked_objects)

        return instance_assignment % params

    def get_parreg_value(self, instance):
        par_reg = ['0'] * 16

        par_reg[15] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Fail-Safe",
                                                       on_value=["on/open normal output", "on/open inverted output"])
        par_reg[14] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback On")
        par_reg[13] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback Off")
        par_reg[12] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback Analog")
        par_reg[11] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Local Drive")
        par_reg[10] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Hardware Analog Output")
        par_reg[9] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Fail-Safe",
                                                      on_value="on/open normal output")
        par_reg[7] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop",
                                                      off_value="false")
        par_reg[6] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop",
                                                      on_value="true even if full stop still active")
        return "".join(par_reg)

    def get_instance_db(self, instance, name):
        instance_db = '''DATA_BLOCK "%(name)s" "CPC_FB_ANALOG"
BEGIN
   PAnalog.ParReg := 2#%(PArReg)s;
   PAnalog.PMaxRan := %(PMaxRan)s;
   PAnalog.PMinRan := %(PMinRan)s;
   PAnalog.PMStpInV := %(PMStpInV)s;
   PAnalog.PMStpDeV := %(PMStpDeV)s;
   PAnalog.PMInSpd := %(PMinSpd)s;
   PAnalog.PMDeSpd := %(PMDeSpd)s;
   PAnalog.PWDt := T#%(PWDt)ss;
   PAnalog.PWDb := %(PWDb)s;

END_DATA_BLOCK'''
        params = {
            'name': name,
            'PArReg': self.get_parreg_value(instance)
        }

        range_min, range_max = self.get_range(self.thePlugin, instance)
        output_range = float(range_max) - float(range_min)
        params['PMaxRan'] = range_max
        params['PMinRan'] = range_min
        params['PWDt'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Warning Time Delay (s)", "5.0")
        # default increase/decrease speed makes if from 0% to 100% in 10 seconds
        params['PMinSpd'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Manual Increase Speed (Unit/s)", output_range / 10)
        params['PMDeSpd'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Manual Decrease Speed (Unit/s)", output_range / 10)
        # default step is 1% of the range
        params['PWDb'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Warning Deadband Value (Unit)", output_range / 100)
        params['PMStpInV'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Manual Increase Step (Unit)", output_range / 100)
        params['PMStpDeV'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Manual Decrease Step (Unit)", output_range / 100)

        return instance_db % params
