# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class DigitalAlarm_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "DigitalAlarm"))
        self.thePlugin.writeDebugInUABLog("LimitSize = " + str(limit_size))

        params = {
            'spec_version': spec_version,
            'instance_amount': instance_list.size(),
            'TYPE_ManRequest': [],
            'TYPE_bin_Status': [],
            'TYPE_event': [],
            'DB_bin_status': [],
            'OPTIMIZED_BLOCK': [],
            'OPTIMIZED_FB_CALL': []
        }

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        # split DB based on limit size
        optimized_blocks_amount = (instance_list.size() - 1) / limit_size
        for block_number in range(optimized_blocks_amount + 1):
            block = self.get_optimized_block(block_number, limit_size, instance_list)
            params['OPTIMIZED_BLOCK'].append(block)
            params['OPTIMIZED_FB_CALL'].append('''	"DB_DA_all%(block_number)s"();''' % {'block_number': DigitalAlarm_Template.get_block_number(block_number)})

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_optimized_block(self, block_number, limit, instance_list):
        begin_index = block_number * limit
        end_index = begin_index + limit

        params = {'block_number': DigitalAlarm_Template.get_block_number(block_number),
                  'DA_SET': [],
                  'DB_DA_all': [],
                  'begin_index': begin_index + 1,
                  'end_index': end_index if end_index < instance_list.size() else instance_list.size()}

        for idx, instance in enumerate(instance_list[begin_index:end_index], params['begin_index']):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['DA_SET'].append('''         %s : "CPC_DB_DA";   // DA number <%s>''' % (name, idx))
            params['DB_DA_all'].append(self.get_instance_assignment(instance, name))

        return self.process_template("_optimized.scl", params)

    def get_instance_assignment(self, instance, name):
        instance_assignment = '''   DA_SET.%(name)s.PAuAckAl := %(PAuAckAl)s;
   %(PAlDt)s'''

        params = {
            'name': name}

        auto_acknowledgement = instance.getAttributeData('FEDeviceAlarm:Auto Acknowledge').lower().strip()
        params['PAuAckAl'] = 'TRUE' if auto_acknowledgement == 'true' else 'FALSE'

        delay = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)").strip()
        if delay == "":
            params['PAlDt'] = '''DA_SET.%s.PAlDt := 0;''' % (name)
        elif self.thePlugin.isString(delay):
            params['PAlDt'] = '''// The Alarm Delay is defined in the logic'''
        else:
            params['PAlDt'] = '''DA_SET.%s.PAlDt := %s;''' % (name, int(round(float(delay))))

        return instance_assignment % params
