# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class DigitalInput_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "DigitalInput"))

        params = {'spec_version': spec_version,
                  'instance_amount': instance_list.size(),
                  'DB_DI_ALL_S': '',
                  'TYPE_ManRequest': [],
                  'TYPE_bin_Status': [],
                  'TYPE_event': [],
                  'OPTIMIZED_BLOCK': [],
                  'OPTIMIZED_FB_CALL': [],
                  'ERROR_DB': ''}

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
        if diagnostic == "true":
            params['ERROR_DB'] = self.get_error_db(instance_list)
            params['diagnostic_logic'] = '''	  // Assign the IOERROR
	  IF (#DI[#I].FEType = 1) THEN
	    #DI[#I].IOError := "DI_ERROR".IOERROR[#I].Err;   // IO Hardware
	  ELSIF (#DI[#I].FEType = 0) THEN
	    ; // Object without connections.
	  ELSE
	    #DI[#I].IOError := #IOError_Var;
	  END_IF;'''
        else:
            params['diagnostic_logic'] = '''	  // No diagnostic
	  // DI[I].IOError := #IOError_Var;'''


        is_large_application = self.spec.is_large_application()
        if is_large_application:
            params['DB_DI_ALL_S'] = self.get_simplified_block(instance_list)

        # split DB based on limit size
        optimized_blocks_amount = (instance_list.size() - 1) / limit_size
        for block_number in range(optimized_blocks_amount + 1):
            block = self.get_optimized_block(block_number, limit_size, instance_list, is_large_application)
            params['OPTIMIZED_BLOCK'].append(block)
            params['OPTIMIZED_FB_CALL'].append('''	"DB_DI_all%(block_number)s"();''' % {'block_number': DigitalInput_Template.get_block_number(block_number)})

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_simplified_block(self, instance_list):
        params = {'DI_SET': []}
        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['DI_SET'].append('''         %s : "CPC_DB_DI_S";   // DI number <%s>''' % (name, idx))
        return self.process_template("_simplified.scl", params)

    def get_optimized_block(self, block_number, limit, instance_list, is_large_application):
        begin_index = block_number * limit
        end_index = begin_index + limit

        params = {'block_number': DigitalInput_Template.get_block_number(block_number),
                  'DI_SET': [],
                  'LARGE_APPLICATION': '',
                  'DB_DI_all': [],
                  'begin_index': begin_index + 1,
                  'end_index': end_index if end_index < instance_list.size() else instance_list.size()}

        for idx, instance in enumerate(instance_list[begin_index:end_index], params['begin_index']):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['DI_SET'].append('''         %s : "CPC_DB_DI";   // DI number <%s>''' % (name, idx))
            params['DB_DI_all'].append(self.get_instance_assignment(instance, idx, name))
            params['DB_DI_all'].append(self.get_instance_io_config(instance, name))

        if is_large_application:
            params['LARGE_APPLICATION'] = '''"DB_DI_All_S".DX [((#I - 1) * SIZE_DB_DI_S_IN_BYTES) + OFFSET_POSST_IN_BYTES, BIT_POSST] := #DI[#I].PosSt;
	  "DB_DI_All_S".DX [((#I - 1) * SIZE_DB_DI_S_IN_BYTES) + OFFSET_FOMOST_IN_BYTES, BIT_FOMOST] := #DI[#I].FoMoSt;
	  "DB_DI_All_S".DX [((#I - 1) * SIZE_DB_DI_S_IN_BYTES) + OFFSET_IOERRORW_IN_BYTES, BIT_IOERRORW] := #DI[#I].IOErrorW;
	  "DB_DI_All_S".DX [((#I - 1) * SIZE_DB_DI_S_IN_BYTES) + OFFSET_IOSIMUW_IN_BYTES, BIT_IOSIMUW] := #DI[#I].IOSimuW;'''

        return self.process_template("_optimized.scl", params)

    def get_instance_assignment(self, instance, idx, name):
        instance_assignment = '''   DI_SET.%(name)s.FEType := %(FEType)s;
   DI_SET.%(name)s.index := %(index)s;'''
        params = {
            'name': name,
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0"),
            'index': idx
        }
        return instance_assignment % params

    def get_error_db(self, instance_list):
        ERROR_DB = '''
(*DB for IoError on Channels with OB82*)
DATA_BLOCK "DI_ERROR"
TITLE = 'DI_ERROR'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : 'EN/ICE'
FAMILY : Error
NAME : Error
//
// DB with IOError signals of DI
   STRUCT 
      IOERROR : Array[1..%(instancesNumber)s] of "CPC_IOERROR";
   END_STRUCT;


BEGIN
%(ASSIGNMENT)s
END_DATA_BLOCK'''
        params = {
            "instancesNumber": instance_list.size(),
            "ASSIGNMENT": ""
        }
        for idx, instance in enumerate(instance_list, 1):
            fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
            if fe_type == "1":
                interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip()
                if interface_param1.startswith("P"):
                    params["ASSIGNMENT"] += '''	IOERROR[%s].ADDR := %s;\n''' % (idx, interface_param1[2:8])
                elif interface_param1.startswith("I"):
                    params["ASSIGNMENT"] += '''	IOERROR[%s].ADDR := %s;\n''' % (idx, interface_param1[1:8])

        return ERROR_DB % params

    @staticmethod
    def get_instance_io_config(instance, name):
        result = []
        interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        interface_param2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
        interface_param3 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam3").strip().lower()
        interface_param4 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam4").strip().lower()
        interface_param5 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5").strip().lower()
        interface_param6 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam6").strip().lower()
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")

        interface_param1 = interface_param1.replace("i", "").replace("db", "")
        interface_param2 = interface_param2.replace("dbb", "")
        interface_param4 = interface_param4.replace("db", "")
        interface_param5 = interface_param5.replace("dbb", "")

        if fe_type == "1":
            per_byte, per_bit = interface_param1.split(".")
            result.append('''   DI_SET.%s.perByte := %s;''' % (name, per_byte))
            result.append('''   DI_SET.%s.perBit := %s;''' % (name, per_bit))

        if fe_type == "101":
            result.append('''   DI_SET.%s.DBnum := %s;''' % (name, interface_param1))
            result.append('''   DI_SET.%s.perByte := %s;''' % (name, interface_param2))
            result.append('''   DI_SET.%s.perBit := %s;''' % (name, interface_param3))

        if fe_type == "102" or fe_type == "103":
            result.append('''   DI_SET.%s.DBnum := %s;''' % (name, interface_param1))
            result.append('''   DI_SET.%s.perByte := %s;''' % (name, interface_param2))
            result.append('''   DI_SET.%s.perBit := %s;''' % (name, interface_param3))
            result.append('''   DI_SET.%s.DBnumIoError := %s;''' % (name, interface_param4))
            result.append('''   DI_SET.%s.DBposIoError := %s;''' % (name, interface_param5))
            result.append('''   DI_SET.%s.DBbitIoError := %s;''' % (name, interface_param6))

        return '''\n'''.join(result)
