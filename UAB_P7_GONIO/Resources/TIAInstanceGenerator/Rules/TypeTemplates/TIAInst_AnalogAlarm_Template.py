# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class AnalogAlarm_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "AnalogAlarm"))
        self.thePlugin.writeDebugInUABLog("LimitSize = " + str(limit_size))

        params = {
            'spec_version': spec_version,
            'instance_amount': instance_list.size(),
            'TYPE_ana_Status': [],
            'TYPE_ManRequest': [],
            'TYPE_bin_Status': [],
            'TYPE_event': [],
            'DB_ManRequest': [],
            'DB_ana_status': [],
            'OPTIMIZED_BLOCK': [],
            'OPTIMIZED_FB_CALL': []
        }

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        for idx, instance in enumerate(instance_list, 1):
            hh = self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(instance, "FEDeviceManualRequests:HH Alarm", "0.0"))
            params['DB_ManRequest'].append("   AA_Requests[%s].HH := %s;" % (idx, hh))
            h = self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(instance, "FEDeviceManualRequests:H Warning", "0.0"))
            params['DB_ManRequest'].append("   AA_Requests[%s].H := %s;" % (idx, h))
            l = self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(instance, "FEDeviceManualRequests:L Warning", "0.0"))
            params['DB_ManRequest'].append("   AA_Requests[%s].L := %s;" % (idx, l))
            ll = self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(instance, "FEDeviceManualRequests:LL Alarm", "0.0"))
            params['DB_ManRequest'].append("   AA_Requests[%s].LL := %s;" % (idx, ll))
            #params['DB_ManRequest'].append("")

        # split DB based on limit size
        optimized_blocks_amount = (instance_list.size() - 1) / limit_size
        for block_number in range(optimized_blocks_amount + 1):
            block = self.get_optimized_block(block_number, limit_size, instance_list)
            params['OPTIMIZED_BLOCK'].append(block)
            params['OPTIMIZED_FB_CALL'].append('''	"DB_AA_all%(block_number)s"();''' % {'block_number': AnalogAlarm_Template.get_block_number(block_number)})

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_threshold_bit(self, instance, param):
        value = instance.getAttributeData(param)
        if self.thePlugin.isString(value) or value.strip() == "":
            return '1'
        else:
            return '0'

    def get_parreg_value(self, instance):
        par_reg = ['0'] * 16
        par_reg[15] = self.get_threshold_bit(instance, "FEDeviceManualRequests:HH Alarm")
        par_reg[14] = self.get_threshold_bit(instance, "FEDeviceManualRequests:H Warning")
        par_reg[13] = self.get_threshold_bit(instance, "FEDeviceManualRequests:L Warning")
        par_reg[12] = self.get_threshold_bit(instance, "FEDeviceManualRequests:LL Alarm")
        par_reg[11] = self.spec.get_bit_from_attribute(instance, "FEDeviceAlarm:Auto Acknowledge",
                                                       on_value="true")
        return "".join(par_reg)

    def get_optimized_block(self, block_number, limit, instance_list):
        begin_index = block_number * limit
        end_index = begin_index + limit

        params = {'block_number': AnalogAlarm_Template.get_block_number(block_number),
                  'AA_SET': [],
                  'DB_AA_all': [],
                  'begin_index': begin_index + 1,
                  'end_index': end_index if end_index < instance_list.size() else instance_list.size()}

        for idx, instance in enumerate(instance_list[begin_index:end_index], params['begin_index']):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['AA_SET'].append('''         %s : "CPC_DB_AA";   // AA number <%s>''' % (name, idx))
            params['DB_AA_all'].append(self.get_instance_assignment(instance, name))

        return self.process_template("_optimized.scl", params)

    @staticmethod
    def get_threshold_param(instance, param):
        value = instance.getAttributeData(param)
        if value.strip() == "":
            return 'FALSE'
        else:
            return 'TRUE'

    def get_instance_assignment(self, instance, name):
        instance_assignment = '''   AA_SET.%(name)s.HHSt := %(HH)s;
   AA_SET.%(name)s.HSt := %(H)s;
   AA_SET.%(name)s.LSt := %(L)s;
   AA_SET.%(name)s.LLSt := %(LL)s;
   AA_SET.%(name)s.HH := %(HH)s;
   AA_SET.%(name)s.H := %(H)s;
   AA_SET.%(name)s.L := %(L)s;
   AA_SET.%(name)s.LL := %(LL)s;
   %(PAlDt)s
   AA_SET.%(name)s.PAA.ParReg := 2#%(PArReg)s;
   AA_SET.%(name)s.AuEHH := %(AuEHH)s;
   AA_SET.%(name)s.AuEH := %(AuEH)s;
   AA_SET.%(name)s.AuEL := %(AuEL)s;
   AA_SET.%(name)s.AuELL := %(AuELL)s;'''
        params = {
            'name': name,
            'PArReg': self.get_parreg_value(instance),
            'AuEHH': self.get_threshold_param(instance, "FEDeviceManualRequests:HH Alarm"),
            'AuEH': self.get_threshold_param(instance, "FEDeviceManualRequests:H Warning"),
            'AuEL': self.get_threshold_param(instance, "FEDeviceManualRequests:L Warning"),
            'AuELL': self.get_threshold_param(instance, "FEDeviceManualRequests:LL Alarm"),
            'HH': self.spec.get_attribute_value(instance, "FEDeviceManualRequests:HH Alarm", "0.0"),
            'H': self.spec.get_attribute_value(instance, "FEDeviceManualRequests:H Warning", "0.0"),
            'L': self.spec.get_attribute_value(instance, "FEDeviceManualRequests:L Warning", "0.0"),
            'LL': self.spec.get_attribute_value(instance, "FEDeviceManualRequests:LL Alarm", "0.0")
        }

        delay = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)").strip()
        if delay == "":
            params['PAlDt'] = '''AA_SET.%s.PAlDt := 0;''' % (name)
        elif self.thePlugin.isString(delay):
            params['PAlDt'] = '''// The Alarm Delay is defined in the logic'''
        else:
            params['PAlDt'] = '''AA_SET.%s.PAlDt := %s;''' % (name, int(round(float(delay))))

        return instance_assignment % params
