# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class OnOff_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()
        unicos_project = self.thePlugin.getUnicosProject()

        params = {
            'spec_version': spec_version,
            'instance_amount': len(instance_list),
            'TYPE_ManRequest': [],
            'TYPE_bin_Status': [],
            'TYPE_event': [],
            'DB_bin_status': [],
            'FC_ONOFF': [],
            'INSTANCE_DB': []
        }

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        # iterate over instances
        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['DB_bin_status'].append('''      %s : "ONOFF_bin_Status";''' % name)
            params['INSTANCE_DB'].append(self.get_instance_db(instance, name))

        limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "OnOff"))

        blocks_amount = len(instance_list) / (limit_size + 1)
        for block_number in range(blocks_amount + 1):
            begin_index = block_number * limit_size
            end_index = begin_index + limit_size
            intance_split = instance_list[begin_index:end_index]
            params['FC_ONOFF'].append(self.get_fc_block(intance_split, block_number + 1))

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_fc_block(self, instance_list, block_number):
        fc_block = '''(*EXEC of ONOFF************************************************)
FUNCTION "FC_ONOFF%(block_number)s" : Void
TITLE = 'FC_ONOFF%(block_number)s'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : UNICOS
FAMILY : OnOff
NAME : CallOO
//
// ONOFF calls
   VAR_TEMP 
      old_status1 : DWord;
      old_status2 : DWord;
   END_VAR


BEGIN
%(ASSIGNMENT)s
END_FUNCTION
'''
        if block_number == 1:
            block_number = ""
        params = {
            'block_number': block_number
        }
        params['ASSIGNMENT'] = []
        for instance in instance_list:
            params['ASSIGNMENT'].append(self.get_instance_assignment(instance))
        params['ASSIGNMENT'] = "\n".join(params['ASSIGNMENT'])
        return fc_block % params

    def get_instance_assignment(self, instance):
        """ this function returns a piece of content of FC_ONOFF function related to given instance """
        instance_fc = '''	// ----------------------------------------------
	// ---- OnOff <%(index)s>: %(name)s
	// ----------------------------------------------
	#old_status1 := "DB_bin_status_ONOFF".%(name)s.StsReg01;
	#old_status2 := "DB_bin_status_ONOFF".%(name)s.StsReg02;
	#old_status1 := ROR(IN := #old_status1, N := 16);
	#old_status2 := ROR(IN := #old_status2, N := 16);
	
	%(name)s.Manreg01 := "DB_ONOFF_ManRequest".ONOFF_Requests[%(index)s].ManReg01;
	%(PWDt_link)s
	%(HFOn_link)s
	%(HFOff_link)s
	%(HLD_link)s
	%(HOnR_link)s
	%(HOffR_link)s
	// IOError
	%(io_error)s
	// IOSimu
	%(io_simu)s
	// Calls the Baseline function
	"%(name)s"();
	%(OutOnOV_link)s
	%(OutOffOV_link)s
	
	//Reset AuAuMoR and AuAlAck
	"%(name)s".AuAuMoR := FALSE;
	"%(name)s".AuAlAck := FALSE;
	"%(name)s".AuRstart := FALSE;
	
	// Recopy new status
	"DB_bin_status_ONOFF".%(name)s.StsReg01 := "%(name)s".Stsreg01;
	"DB_bin_status_ONOFF".%(name)s.StsReg02 := "%(name)s".Stsreg02;
	"DB_Event_ONOFF".ONOFF_evstsreg[%(index)s].evStsReg01 := #old_status1 OR "DB_bin_status_ONOFF".%(name)s.StsReg01;
	"DB_Event_ONOFF".ONOFF_evstsreg[%(index)s].evStsReg02 := #old_status2 OR "DB_bin_status_ONOFF".%(name)s.StsReg02;'''
        name = instance.getAttributeData("DeviceIdentification:Name")
        params = {
            "name": name,
            "index": instance.getInstanceNumber(),
            "PWDt_link": "",
            "HFOn_link": "",
            "HFOff_link": "",
            "HLD_link": "",
            "HOnR_link": "",
            "HOffR_link": "",
            "OutOnOV_link": "",
            "OutOffOV_link": "",
            "io_error": "",
            "io_simu": ""
        }

        warning_delay = self.spec.get_s7db_id(instance, "FEDeviceParameters:Warning Time Delay (s)", "AnalogParameter,AnalogStatus,AnalogInput,AnalogInputReal")
        if warning_delay:
            params['PWDt_link'] = '''"%s".POnOff.PWDt := DINT_TO_TIME(REAL_TO_DINT(%s.PosSt * 1000.0));''' % (name, warning_delay)

        feedback_on = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Feedback On", "DigitalInput")
        if feedback_on:
            params['HFOn_link'] = '''"%s".HFOn := %s.PosSt;''' % (name, feedback_on)

        feedback_off = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Feedback Off", "DigitalInput")
        if feedback_off:
            params['HFOff_link'] = '''"%s".HFOff := %s.PosSt;''' % (name, feedback_off)

        local_drive = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Local Drive", "DigitalInput")
        if local_drive:
            params['HLD_link'] = '''"%s".HLD := %s.PosSt;''' % (name, local_drive)

        local_on = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Local On", "DigitalInput")
        if local_on:
            params['HOnR_link'] = '''"%s".HOnR := %s.PosSt;''' % (name, local_on)

        local_off = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Local Off", "DigitalInput")
        if local_off:
            params['HOffR_link'] = '''"%s".HOffR := %s.PosSt;''' % (name, local_off)

        process_output = self.spec.get_s7db_id(instance, "FEDeviceOutputs:Process Output", "DigitalOutput")
        if process_output:
            params['OutOnOV_link'] = '''%s.AuposR := "%s".OutOnOV;''' % (process_output, name)

        process_output_off = self.spec.get_s7db_id(instance, "FEDeviceOutputs:Process Output Off", "DigitalOutput")
        if process_output_off:
            params['OutOffOV_link'] = '''%s.AuposR := "%s".OutOffOV;''' % (process_output_off, name)

        linked_objects = [feedback_on, feedback_off, local_drive, local_on, local_off, process_output, process_output_off]
        params['io_error'], params['io_simu'] = self.get_io_error_and_simu(name, linked_objects)

        return instance_fc % params

    def get_parreg_value(self, instance):
        """this function returns parreg for given instance"""
        par_reg = ['0'] * 15
        par_reg[14] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Fail-Safe", on_value=["on/open", "2 do on"])
        par_reg[13] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback On")
        par_reg[12] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback Off")
        par_reg[11] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:Pulse Duration (s)", off_value=["", "0.0", "0"])
        par_reg[10] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Local Drive")
        local_on = instance.getAttributeData("FEDeviceEnvironmentInputs:Local On").strip().lower()
        local_off = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Off").strip().lower()
        if local_off == "" and local_on == "":
            par_reg[9] = '0'
        else:
            par_reg[9] = '1'
        par_reg[8] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Full/Empty Animation", on_value="full/empty")
        par_reg[7] = self.spec.get_bit_from_attribute(instance, "FEDeviceOutputs:Process Output Off")
        par_reg[6] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop", on_value=['true only if full stop disappeared', 'true even if full stop still active'])
        par_reg[5] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop", on_value='true even if full stop still active')
        par_reg[4] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Fail-Safe", on_value=["2 do on", "2 do off"])
        par_reg[3] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Constant Time Pulse", on_value='true')
        return "".join(par_reg)

    def get_instance_db(self, instance, name):
        """ this function returns DATA_BLOCK for given instance """
        instance_db = '''DATA_BLOCK "%(name)s" "CPC_FB_ONOFF"
BEGIN
	POnOff.ParReg := 2#%(ParReg)s;
	POnOff.PPulseLe := T#%(PPulseLe)ss;
	POnOff.PWDt := T#%(PWDt)ss;
END_DATA_BLOCK'''
        params = {
            'name': name,
            'ParReg': self.get_parreg_value(instance),
            'PPulseLe': self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(instance, "FEDeviceParameters:Pulse Duration (s)", "0.0")),
            'PWDt': self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(instance, "FEDeviceParameters:Warning Time Delay (s)", "5.0"))
        }

        return instance_db % params
