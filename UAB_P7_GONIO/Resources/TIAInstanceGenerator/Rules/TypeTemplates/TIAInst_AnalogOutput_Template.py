# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class AnalogOutput_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        params = {'spec_version': spec_version,
                  'TYPE_ManRequest': [],
                  'TYPE_bin_Status': [],
                  'TYPE_event': [],
                  'TYPE_ana_Status': [],
                  'ASSIGNMENTS': [],
                  'instance_amount': instance_list.size(),
                  'static_variables': [],
                  'diagnostic_logic': '',
                  'ERROR_DB': ''}

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)
        diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
        if diagnostic == "true":
            params['diagnostic_logic'] = '''	  #AO[#I].IOError := "AO_ERROR".IOERROR[#I].Err;'''
            params['ERROR_DB'] = self.get_error_db(instance_list)
        else:
            params['diagnostic_logic'] += '''	  #AO[#I].IOError := False;'''

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['static_variables'].append('''         %s : "CPC_DB_AO";''' % name)
            params['ASSIGNMENTS'].append(self.get_instance_assignment(instance, idx))
            params['ASSIGNMENTS'].append(self.get_instance_io_config(instance, name))

        self.writeDeviceInstances(self.process_template(".scl", params))

    @staticmethod
    def get_error_db(instance_list):
        error_db = '''(*DB for IoError on Channels with OB82*)
DATA_BLOCK "AO_ERROR"
TITLE = AO_ERROR
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : AB_CO_IS
FAMILY : Error
NAME : Error
//
// DB with IOError signals of AO
   STRUCT 
      IOERROR : Array[1..%(instance_amount)s] of "CPC_IOERROR";
   END_STRUCT;


BEGIN
%(assignments)s
END_DATA_BLOCK'''
        params = {'instance_amount': instance_list.size()}

        assignments = []
        for idx, instance in enumerate(instance_list, 1):
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
            if interface_param1.startswith('p'):
                interface_param1 = interface_param1.replace('pqw', '').replace('pqb', '')
            else:
                interface_param1 = interface_param1.replace('qw', '').replace('qb', '')
            if interface_param1:
                assignments.append('''	IOERROR[%s].ADDR := %s.0;''' % (idx, interface_param1))

        params['assignments'] = "\n".join(assignments)

        return error_db % params

    def get_instance_assignment(self, instance, idx):
        instance_assignment = '''   AO_SET.%(name)s.index := %(index)s;
   AO_SET.%(name)s.PMaxRan := %(PMaxRan)s;
   AO_SET.%(name)s.PMinRan := %(PMinRan)s;
   AO_SET.%(name)s.PMinRaw := %(PMinRaw)s;
   AO_SET.%(name)s.PMaxRaw := %(PMaxRaw)s;
   AO_SET.%(name)s.FEType := %(FEType)s;'''
        params = {
            'name': instance.getAttributeData("DeviceIdentification:Name"),
            'index': idx,
            'PMaxRan': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max")),
            'PMinRan': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min")),
            'PMaxRaw': instance.getAttributeData("FEDeviceParameters:Raw Max"),
            'PMinRaw': instance.getAttributeData("FEDeviceParameters:Raw Min"),
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
        }
        return instance_assignment % params

    @staticmethod
    def get_instance_io_config(instance, name):
        result = []
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        if fe_type == '1':
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
            if interface_param1.startswith('p'):
                pqw_def = 'true'
                interface_param1 = interface_param1.replace('pqw', '').replace('pqb', '')
            else:
                pqw_def = 'false'
                interface_param1 = interface_param1.replace('qw', '').replace('qb', '')
            result.append('''	AO_SET.%s.perAddress := %s;''' % (name, interface_param1))
            result.append('''	AO_SET.%s.PqwDef := %s;''' % (name, pqw_def))
        return '\n'.join(result)
