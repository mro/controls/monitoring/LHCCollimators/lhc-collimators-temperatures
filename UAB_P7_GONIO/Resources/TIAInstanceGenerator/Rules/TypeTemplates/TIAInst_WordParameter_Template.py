# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class WordParameter_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "WordParameter"))
        self.thePlugin.writeDebugInUABLog("LimitSize = " + str(limit_size))

        params = {'spec_version': spec_version,
                  'instance_amount': instance_list.size(),
                  'TYPE_ManRequest': [],
                  'TYPE_bin_Status': [],
                  'TYPE_event': [],
                  'TYPE_ana_Status': [],
                  'OPTIMIZED_BLOCK': [],
                  'OPTIMIZED_FB_CALL': [], }

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        optimized_blocks_amount = (instance_list.size() - 1) / limit_size
        for block_number in range(optimized_blocks_amount + 1):
            block = self.get_optimized_block(block_number, limit_size, instance_list)
            params['OPTIMIZED_BLOCK'].append(block)
            params['OPTIMIZED_FB_CALL'].append('''	"DB_WPAR_all%(block_number)s"();''' % {'block_number': WordParameter_Template.get_block_number(block_number)})

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_optimized_block(self, block_number, limit, instance_list):

        begin_index = block_number * limit
        end_index = begin_index + limit

        params = {'block_number': WordParameter_Template.get_block_number(block_number),
                  'WPAR_SET': [],
                  'DB_WPAR_all': [],
                  'begin_index': begin_index + 1,
                  'end_index': end_index if end_index < instance_list.size() else instance_list.size()}

        for idx, instance in enumerate(instance_list[begin_index:end_index], params['begin_index']):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['WPAR_SET'].append('''         %s : "CPC_DB_WPAR";   // WPAR number <%s>''' % (name, idx))
            params['DB_WPAR_all'].append(self.get_instance_assignment(instance, idx))
            params['DB_WPAR_all'].append(self.get_instance_io_config(instance, name))

        return self.process_template("_optimized.scl", params)

    def get_instance_assignment(self, instance, idx):
        optimized_db_all = '''   WPAR_SET.%(name)s.index := %(index)s;
   WPAR_SET.%(name)s.PosSt := W#16#%(default_value)s;
   WPAR_SET.%(name)s.FEType := %(FEType)s;'''
        params = {
            'name': instance.getAttributeData("DeviceIdentification:Name"),
            'index': idx,
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0"),
            'default_value': []
        }
        default_value = instance.getAttributeData("FEDeviceParameters:Default Value")
        range_max = instance.getAttributeData("FEDeviceParameters:Range Max").strip()
        range_min = instance.getAttributeData("FEDeviceParameters:Range Min").strip()
        # TODO move to semantic check
        # check consistency
        if float(default_value) < float(range_min) or float(default_value) > float(range_max):
            self.logger.write_warning(instance, "The default value:(" + str(default_value) + ") is outside its range [" + str(range_min) + " - " + str(range_max) + "].")
        if float(range_max) < float(range_min):
            self.logger.write_warning(instance, "The value Max:(" + str(range_max) + ") should be bigger than the Min:(" + str(range_min) + ").")
        params['default_value'] = hex(int(default_value))[2:]
        return optimized_db_all % params

    @staticmethod
    def get_instance_io_config(instance, name):
        result = []
        interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        interface_param2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        if fe_type == '1':
            if interface_param1.startswith('p'):
                piw_def = 'true'
                interface_param1 = interface_param1.replace('piw', '').replace('pib', '')
            else:
                piw_def = 'false'
                interface_param1 = interface_param1.replace('iw', '').replace('ib', '')
            result.append('''   WPAR_SET.%s.InterfaceParam1 := %s;''' % (name, interface_param1))
            result.append('''   WPAR_SET.%s.PQWDef := %s;''' % (name, piw_def))
        elif fe_type == '101':
            interface_param1 = interface_param1.replace('db', '')
            interface_param2 = interface_param2.replace('dbb', '')
            result.append('''   WPAR_SET.%s.InterfaceParam1 := %s;''' % (name, interface_param1))
            result.append('''   WPAR_SET.%s.InterfaceParam2 := %s;''' % (name, interface_param2))
        elif fe_type == '100':
            interface_param1 = interface_param1.replace('mb', '')
            result.append('''   WPAR_SET.%s.InterfaceParam1 := %s;''' % (name, interface_param1))
        return '\n'.join(result)
