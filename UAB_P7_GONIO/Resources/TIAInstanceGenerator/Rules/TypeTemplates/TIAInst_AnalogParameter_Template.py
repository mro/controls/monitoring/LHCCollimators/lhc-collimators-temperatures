# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class AnalogParameter_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "AnalogParameter"))
        self.thePlugin.writeDebugInUABLog("LimitSize = " + str(limit_size))

        params = {'spec_version': spec_version,
                  'instance_amount': instance_list.size(),
                  'TYPE_ManRequest': [],
                  'TYPE_bin_Status': [],
                  'TYPE_event': [],
                  'TYPE_ana_Status': [],
                  'OPTIMIZED_BLOCK': [],
                  'OPTIMIZED_FB_CALL': [], }

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        # split DB based on limit size
        optimized_blocks_amount = (instance_list.size() - 1) / limit_size
        for block_number in range(optimized_blocks_amount + 1):
            block = self.get_optimized_block(block_number, limit_size, instance_list)
            params['OPTIMIZED_BLOCK'].append(block)
            params['OPTIMIZED_FB_CALL'].append('''	"DB_APAR_all%(block_number)s"();''' % {'block_number': AnalogParameter_Template.get_block_number(block_number)})

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_optimized_block(self, block_number, limit, instance_list):
        begin_index = block_number * limit
        end_index = begin_index + limit

        params = {'block_number': AnalogParameter_Template.get_block_number(block_number),
                  'APAR_SET': [],
                  'DB_APAR_all': [],
                  'begin_index': begin_index + 1,
                  'end_index': end_index if end_index < instance_list.size() else instance_list.size()}

        for idx, instance in enumerate(instance_list[begin_index:end_index], params['begin_index']):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['APAR_SET'].append('''         %s : "CPC_DB_APAR";   // APAR number <%s>''' % (name, idx))
            params['DB_APAR_all'].append(self.get_instance_assignment(instance, idx))

        return self.process_template("_optimized.scl", params)

    def get_instance_assignment(self, instance, idx):
        optimized_db_all = '''   // APAR number <%(index)s>
   APAR_SET.%(name)s.PosSt := %(default_value)s;'''
        params = {
            'name': instance.getAttributeData("DeviceIdentification:Name"),
            'index': idx,
            'default_value': self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(instance, "FEDeviceParameters:Default Value", "0.0"))
        }

        default_value = self.thePlugin.formatNumberPLC(self.spec.get_attribute_value(instance, "FEDeviceParameters:Default Value", "0.0"))
        name = instance.getAttributeData("DeviceIdentification:Name")
        range_max = instance.getAttributeData("FEDeviceParameters:Range Max").strip()
        range_min = instance.getAttributeData("FEDeviceParameters:Range Min").strip()
        if float(default_value) < float(range_min) or float(default_value) > float(range_max):
            self.thePlugin.writeWarningInUABLog("Apar instance: " + name + ". The default value:(" + default_value + ") is outside its range [" + range_min + " - " + range_max + "].")
        if float(range_max) < float(range_min):
            self.thePlugin.writeWarningInUABLog("Apar instance: " + name + ". The value Max:(" + range_max + ") should be bigger than the Min:(" + range_min + ").")

        return optimized_db_all % params
