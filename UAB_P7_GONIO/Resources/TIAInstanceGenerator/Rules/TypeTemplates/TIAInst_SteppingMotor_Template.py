# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class SteppingMotor_Template(TIAInst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        params = {
            'spec_version': spec_version,
            'instance_amount': instance_list.size(),
            'TYPE_ana_Status': [],
            'TYPE_ManRequest': [],
            'TYPE_bin_Status': [],
            'TYPE_event': [],
            'DB_bin_status': [],
            'DB_ana_status': [],
            'INSTANCE_DB': [],
            'FC_instance': []
        }

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['DB_bin_status'].append('''      %s : "STPMOT_bin_Status";''' % name)
            params['DB_ana_status'].append('''      %s : "STPMOT_ana_Status";''' % name)
            params['INSTANCE_DB'].append(self.get_instance_db(instance, name))
            params['FC_instance'].append(self.get_instance_fc(instance, name, idx))

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_parreg_value(self, instance):
        par_reg = ['0'] * 16
        par_reg[14] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Switches Configuration",
                                                       on_value="2 switches plugged into the 1step")
        par_reg[13] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Switches Configuration",
                                                       on_value=["2 end switches + ref. switch", "2 end switches with faked ref. switch"])
        par_reg[12] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Switches Configuration",
                                                       on_value=["2 end switches + ref. switch", "2 end switches with faked ref. switch"])
        par_reg[11] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Switches Configuration",
                                                       on_value=["2 end switches + ref. switch"])

        par_reg[10] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Feedback",
                                                       on_value=["potentiometer", "potentiometer (support)"])
        par_reg[9] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Feedback",
                                                      on_value=["encoder"])
        par_reg[5] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Feedback",
                                                      on_value=["encoder"])
        # TODO what's the diff between par_reg[5] and par_reg[9]
        par_reg[8] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Feedback",
                                                      on_value=["potentiometer (support)"])

        par_reg[7] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop",
                                                      off_value="false")
        par_reg[6] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop",
                                                      on_value=["true even if full stop still active", ""])  # TODO remove emtpy value

        return "".join(par_reg)

    def get_instance_db(self, instance, name):
        instance_db = '''DATA_BLOCK "%(name)s" "CPC_FB_STPMOT"
BEGIN
   PSTPMOT.ParReg := 2#%(ParReg)s;
   PSTPMOT.PRefPos := %(PRefPos)s;
   PSTPMOT.PMinRan := %(PMinRan)s;
   PSTPMOT.PMaxRan := %(PMaxRan)s;
   PSTPMOT.PSca := %(PSca)s;
   PSTPMOT.POffset := %(POffset)s;
   PSTPMOT.PMaxSpd := %(PMaxSpd)s;
   PSTPMOT.PWDt := T#%(PWDt)ss;
   PSTPMOT.PWDb := %(PWDb)s;
   PSTPMOT.PDbT := %(PDbT)s;

END_DATA_BLOCK'''

        # TODO rename FEDeviceParameters:Minimum Range to FEDeviceParameters:Range Min
        range_min = self.spec.get_attribute_value(instance, "FEDeviceParameters:Minimum Range", "0.0")
        # TODO default max range is 0.0 ???
        range_max = self.spec.get_attribute_value(instance, "FEDeviceParameters:Maximum Range", "0.0")
        output_range = float(range_max) - float(range_min)

        #
        maxSpeed = instance.getAttributeData("FEDeviceParameters:Max Speed")
        if maxSpeed == "":
            maxSpeed = 1

        params = {
            'name': name,
            'ParReg': self.get_parreg_value(instance),
            'PRefPos': self.spec.get_attribute_value(instance, "FEDeviceParameters:Reference Position", "0.0"),
            'PMinRan': range_min,
            'PMaxRan': range_max,
            'PSca': self.spec.get_attribute_value(instance, "FEDeviceParameters:Scale", "1.0"),
            'POffset': self.spec.get_attribute_value(instance, "FEDeviceParameters:Offset", "0.0"),
            'PMaxSpd': maxSpeed,
            'PWDt': self.spec.get_attribute_value(instance, "FEDeviceParameters:Warning Time Delay (s)", "5.0"),
            'PWDb': self.spec.get_attribute_value(instance, "FEDeviceParameters:Warning Deadband Value (Unit)", output_range / 1000),
            'PDbT': self.spec.get_attribute_value(instance, "FEDeviceParameters:Deadband Trigger", output_range / 1000)
        }
        return instance_db % params

    def get_instance_fc(self, instance, name, idx):
        instance_fc = '''	// ----------------------------------------------
	// ---- STPMOT <%(index)s>: %(name)s
	// ----------------------------------------------
	#old_status1 := "DB_bin_status_STPMOT".%(name)s.StsReg01;
	#old_status2 := "DB_bin_status_STPMOT".%(name)s.StsReg02;
	#old_status1 := ROR(IN := #old_status1, N := 16);
	#old_status2 := ROR(IN := #old_status2, N := 16);
	
	"%(name)s".Manreg01 := "DB_STPMOT_ManRequest".STPMOT_Requests[%(index)s].ManReg01;
	"%(name)s".MPosR := "DB_STPMOT_ManRequest".STPMOT_Requests[%(index)s].MPosR;
	"%(name)s".MSpdR := "DB_STPMOT_ManRequest".STPMOT_Requests[%(index)s].MSpdR;
	
	%(io_config_dwi)s
	%(HFCW_link)s
	%(HFCCW_link)s
	%(HFPos_link)s
	// IO Error
	%(io_error)s
	// IOSimu
	%(io_simu)s
	// Calls the Baseline function
	"%(name)s"();
	%(CWRef_link)s
	%(SimRef_link)s
	%(DrvEn_link)s
	
	%(io_config_dwo)s
	
	//Reset AuAuMoR and AuAlAck
	"%(name)s".AuAuMoR := FALSE;
	"%(name)s".AuAlAck := FALSE;
	"%(name)s".AuRstart := FALSE;
	
	//Recopy new status
	"DB_bin_status_STPMOT".%(name)s.StsReg01 := "%(name)s".Stsreg01;
	"DB_bin_status_STPMOT".%(name)s.StsReg02 := "%(name)s".Stsreg02;
	"DB_ana_status_STPMOT".%(name)s.PosSt := "%(name)s".PosSt;
	"DB_ana_status_STPMOT".%(name)s.SpdSt := "%(name)s".SpdSt;
	"DB_ana_status_STPMOT".%(name)s.AuPosRSt := "%(name)s".AuPosRSt;
	"DB_ana_status_STPMOT".%(name)s.AuSpdRSt := "%(name)s".AuSpdRSt;
	"DB_ana_status_STPMOT".%(name)s.MPosRSt := "%(name)s".MPosRSt;
	"DB_ana_status_STPMOT".%(name)s.MSpdRSt := "%(name)s".MSpdRSt;
	"DB_ana_status_STPMOT".%(name)s.PosRSt := "%(name)s".PosRSt;
	"DB_ana_status_STPMOT".%(name)s.SpdRSt := "%(name)s".SpdRSt;
	"DB_Event_STPMOT".STPMOT_evstsreg[%(index)s].evStsReg01 := #old_status1 OR "DB_bin_status_STPMOT".%(name)s.StsReg01;
	"DB_Event_STPMOT".STPMOT_evstsreg[%(index)s].evStsReg02 := #old_status2 OR "DB_bin_status_STPMOT".%(name)s.StsReg02;
	'''
        params = {
            'name': name,
            'index': idx,
            # TODO check whether the two get_instance_io_config can be merged
            'io_config_dwi': self.get_instance_io_config_dwi(instance, name),
            'io_config_dwo': self.get_instance_io_config_dwo(instance, name),
            'HFCW_link': '',
            'HFCCW_link': '',
            'HFPos_link': '',
            'CWRef_link': '',
            'SimRef_link': '',
            'DrvEn_link': '',
            'io_error': "",
            'io_simu': ""
        }

        clock_wise_limit = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:ClockWise Limit", "DigitalInput")
        if clock_wise_limit:
            params['HFCW_link'] = '''"%s".HFCW := %s.PosSt;''' % (name, clock_wise_limit)

        counter_clock_wise_limit = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:CounterClockWise Limit", "DigitalInput")
        if counter_clock_wise_limit:
            params['HFCCW_link'] = '''"%s".HFCCW := %s.PosSt;''' % (name, counter_clock_wise_limit)

        feedback_analog = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Feedback Analog", "AnalogInput, AnalogInputReal, Encoder")
        if feedback_analog:
            params['HFPos_link'] = '''"%s".HFPos := %s.PosSt;''' % (name, feedback_analog)

        cw_ref = self.spec.get_s7db_id(instance, "FEDeviceOutputs:ClockWise to Reference Switch", "DigitalOutput")
        if cw_ref:
            params['CWRef_link'] = '''%s.AuposR := "%s".CWRef;''' % (cw_ref, name)

        sim_ref = self.spec.get_s7db_id(instance, "FEDeviceOutputs:Simulated Reference Switch", "DigitalOutput")
        if sim_ref:
            params['SimRef_link'] = '''%s.AuposR := "%s".SimRef;''' % (sim_ref, name)

        driver_enable = self.spec.get_s7db_id(instance, "FEDeviceOutputs:Driver Enable", "DigitalOutput")
        if driver_enable:
            params['DrvEn_link'] = '''%s.AuposR := "%s".DrvEn;''' % (driver_enable, name)

        linked_objects = [clock_wise_limit, counter_clock_wise_limit, feedback_analog, cw_ref, sim_ref, driver_enable]
        params['io_error'], params['io_simu'] = self.get_io_error_and_simu(name, linked_objects)

        return instance_fc % params

    @staticmethod
    def get_instance_io_config_dwi(instance, name):
        instance_io = '''// Call IO_ACCESS_DWI
	"IO_ACCESS_DWI"(
	                FEType := %(fe_type)s,
	                PIDDef := %(pid_def)s,
	                InterfaceParam1 := %(HFSTP1_addr)s,
	                HFPos := "%(name)s".HFSTP1
	);
	
	"IO_ACCESS_DWI"(
	                FEType := %(fe_type)s,
	                PIDDef := %(pid_def)s,
	                InterfaceParam1 := %(HFSTP2_addr)s,
	                HFPos := "%(name)s".HFSTP2
	);'''
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        if fe_type == "" or interface_param1 == "":
            return ""

        pid_def = "TRUE" if interface_param1.startswith("pid") else "FALSE"
        try:
            interface_param1 = int(interface_param1.replace("pid", ""))
        except ValueError:
            interface_param1 = 0
        params = {
            'name': name,
            'fe_type': fe_type,
            'pid_def': pid_def,
            'HFSTP1_addr': interface_param1,
            'HFSTP2_addr': interface_param1 + 4,
        }

        return instance_io % params

    @staticmethod
    def get_instance_io_config_dwo(instance, name):
        instance_io = '''	// Call IO_ACCESS_DWO
	"IO_ACCESS_DWO"(
	                FEType := %(fe_type)s,
	                PQDDef := %(pqd_def)s,
	                InterfaceParam1 := %(CtrlSTP1_addr)s,
	                OutOV := "%(name)s".CtrlSTP1
	);
	
	"IO_ACCESS_DWO"(
	                FEType := %(fe_type)s,
	                PQDDef := %(pqd_def)s,
	                InterfaceParam1 := %(CtrlSTP2_addr)s,
	                OutOV := "%(name)s".CtrlSTP2
	);'''
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        interface_param5 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5").strip().lower()
        if fe_type == "" or interface_param5 == "":
            return ""

        pqd_def = "TRUE" if interface_param5.startswith("pqd") else "FALSE"
        try:
            interface_param5 = int(interface_param5.replace("pqd", ""))
        except ValueError:
            interface_param5 = 0
        params = {
            'name': name,
            'fe_type': fe_type,
            'pqd_def': pqd_def,
            'CtrlSTP1_addr': interface_param5,
            'CtrlSTP2_addr': interface_param5 + 4,
        }

        return instance_io % params
