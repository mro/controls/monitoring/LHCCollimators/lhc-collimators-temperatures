# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class CompilationOB_Template(TIAInst_Generic_Template):

    def process(self, unicos_project, xml_config, generate_global_files, *_):
        """
        General Steps for the Compilation file:
        1. Starting OB (100,102)
        2. Diagnostic OB (82)
        3. Errors and Hot restart: 81, 83, 84, 101 (only S7-400)
        4. Diagnostic functions : FC_Diag1&2
        5. OB1(main cycle)
        6. Call the Recipes mechanism and provide the right values form WinCCOA
        7. OB32 (UNICOS live counter)
        8. OB35 (sampling PIDs)
        9. All other errors treatment are included in the baseline

        :param unicos_project:
        :param xml_config:
        :param generate_global_files: comes from "Global files scope" dropdown on Wizard.
                true = All types. false = Selected types.
        """
        params = {
            'FC_CONTROLLER': "",
            'S7_400_DB': "",
            'DB_DIAGNOSTIC': "",
            'OB82': "",
            'OB1_1': "", 'OB1_2': "", 'OB1_3': "", 'OB1_4': "",
            'DB_RECIPES': "",
            'OB1_COUNTER': "", 'OB32': "",
            'OB86': "",
            'OB35': ""
        }
        xml_config = self.thePlugin.getXMLConfig() # TODO: remove this 
        plc_declaration = xml_config.getPLCDeclarations()[0]
        plc_type = str(plc_declaration.getPLCType().getValue())

        types_to_process = self.get_types_to_process(self.thePlugin, unicos_project, xml_config, generate_global_files)
        type_names_to_process = [device_type.getDeviceTypeName() for device_type in types_to_process]

        diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
        if "Controller" in type_names_to_process:
            params['FC_CONTROLLER'] = '''"FC_CONTROLLER"(group := 0);   // All Controllers running (option=0)'''

        if plc_type.startswith("S7-400"):
            params['S7_400_DB'] = self.get_s7_400_db('Controller' in type_names_to_process)
            
        # order of devices is important and hard-coded in this file
        if "DigitalInput" in type_names_to_process:
            params['OB1_1'] += '''	"FC_DI"();   // Digital inputs\n'''
        if "AnalogInput" in type_names_to_process:
            params['OB1_1'] += '''	"FC_AI"();   // Analog inputs\n'''
        if "AnalogInputReal" in type_names_to_process:
            params['OB1_1'] += '''	"FC_AIR"();   // Analog inputs Real\n'''
        if "Encoder" in type_names_to_process:
            params['OB1_1'] += '''	"FC_ENC"();   // Encoders\n'''
        if "Controller" in type_names_to_process:
            params['OB1_2'] += '''	(* Controller functions called in FC_PCO_LOGIC(). Activate only if no PID Logic declared *)\n'''
            params['OB1_2'] += '''	// "FC_CONTROLLER"(group := 0); All Controllers running (option=0)\n'''
        if "ProcessControlObject" in type_names_to_process:
            params['OB1_2'] += '''	(*Process Logic Control *)\n'''
            params['OB1_2'] += '''	"FC_PCO_LOGIC"();   // Process Control object logic\n'''

        for device_type, instance_amount in types_to_process.iteritems():
            device_type_name = device_type.getDeviceTypeName()
            family_name = device_type.getObjectType()
            representation_name = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", device_type_name)
            if family_name == "InterfaceObjectFamily":
                params['OB1_1'] += '''	"FC_%s"();   // %s objects\n''' % (representation_name, device_type_name)
            elif family_name == "FieldObjectFamily":
                limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", device_type_name))
                optimized = self.thePlugin.getTargetDeviceInformationParam("Optimized", device_type_name).lower()
                if optimized == 'true' and device_type_name != 'Analog':  # TODO: check if this ever executed?
                    params['OB1_3'] += '''	"FB_%(repr)s_All".DB_%(repr)s_All();   // %(type)s objects\n''' % \
                        {'repr': representation_name, 'type': device_type_name}
                    if instance_amount > limit_size:
                        params['OB1_3'] += '''	"FB_%(repr)s_All2".DB_%(repr)s_All2();   // %(type)s objects\n''' % \
                            {'repr': representation_name, 'type': device_type_name}
                elif device_type_name != 'Analog':
                    params['OB1_3'] += '''	"FC_%(repr)s"();   // %(type)s objects\n''' % \
                        {'repr': representation_name, 'type': device_type_name}
                    if instance_amount > limit_size:  # TODO FC_2() ?
                        params['OB1_3'] += '''	"FC_%(repr)s2"();   // %(type)s objects\n''' % \
                            {'repr': representation_name, 'type': device_type_name}
                else:   # Analog needs special treatment
                    numbersToBeGenerated = []

                    instance_list = device_type.getAllDeviceTypeInstances()
                    air_limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "AnalogInputReal"))
                    instance_split = {1: []}
                    for instance in instance_list:
                        pos = self.get_instance_pos(unicos_project, instance, air_limit_size)
                        if pos not in instance_split:
                            instance_split[pos] = []
                        instance_split[pos].append(instance)
                    positions = instance_split.keys()
                    positions.sort()
                    for pos in positions:
                        optimized_blocks_amount = len(instance_split[pos]) / (limit_size + 1)
                        start_idx = 1 if pos == 1 else pos * 10
                        for block_number in range(optimized_blocks_amount + 1):
                            numbersToBeGenerated.append(start_idx + block_number)

                    # print:
                    for number in numbersToBeGenerated:
                        block_number = number
                        if block_number == 1:
                            block_number = ""
                        params['OB1_3'] += '''	"FC_%(repr)s%(number)s"();   // %(type)s objects\n''' % \
                            {'repr': representation_name, 'type': device_type_name, 'number': block_number}

        if "DigitalAlarm" in type_names_to_process:
            params['OB1_3'] += '''	"FC_DA"();   // DIGITAL ALARM objects\n'''
        if "AnalogAlarm" in type_names_to_process:
            params['OB1_3'] += '''	"FC_AA"();   // ANALOG ALARM objects\n'''
        if "DigitalOutput" in type_names_to_process:
            params['OB1_4'] += '''	"FC_DO"();   // Digital Outputs\n'''
        if "AnalogOutput" in type_names_to_process:
            params['OB1_4'] += '''	"FC_AO"();   // Analog Outputs\n'''
        if "AnalogOutputReal" in type_names_to_process:
            params['OB1_4'] += '''	"FC_AOR"();   // Analog Outputs REAL\n'''

        params['DB_RECIPES'] = self.get_recipe_db(xml_config)

        if plc_type.startswith("S7-300") or plc_type.startswith("s7-1500"):
            params['OB1_COUNTER'] = '''	// 1 second counter to simulate OB32 in a S7-300
	"UNICOS_Counter1" := "UNICOS_Counter1" + #OB1_PREV_CYCLE;
	IF "UNICOS_Counter1" > 1000 THEN
	  "UNICOS_Counter1" := "UNICOS_Counter1" - 1000;
	  "UNICOS_LiveCounter" := "UNICOS_LiveCounter" + 1;
	END_IF;'''
        else:
            params['OB32'] = '''
// UNICOS Live Counter 
ORGANIZATION_BLOCK OB32
{ S7_Optimized_Access := 'FALSE' }
   VAR_TEMP 
      Info : Array[0..19] of Byte;
   END_VAR


BEGIN
	"UNICOS_LiveCounter" := "UNICOS_LiveCounter" + 1;
END_ORGANIZATION_BLOCK
'''

        if diagnostic == 'true':
            params['DB_DIAGNOSTIC'] = self.get_diagnostic_db(unicos_project, types_to_process, plc_type)
            params['OB82'] = '''	"DB_IO_DIAGNOSTIC".IOFlag := #OB82_IO_FLAG;
	"DB_IO_DIAGNOSTIC".MDL_ADDR := #OB82_MDL_ADDR;
	"DB_IO_DIAGNOSTIC".FB_Diag1_Exec := TRUE;
	"DB_IO_DIAGNOSTIC"();'''
            params['OB86'] = '''	"DB_IO_DIAGNOSTIC".OB86_EV_CLASS := #OB86_EV_CLASS;   // 16#38/39 Event class 3
	"DB_IO_DIAGNOSTIC".OB86_FLT_ID := #OB86_FLT_ID;   // 16#C1/C4/C5, Fault identifcation code
	"DB_IO_DIAGNOSTIC".OB86_RACKS_FLTD := #OB86_RACKS_FLTD;   // Racks in fault
	
	// Profibus DP Diagnostics
	IF "DB_IO_DIAGNOSTIC".OB86_EV_CLASS = B#16#39 AND "DB_IO_DIAGNOSTIC".OB86_FLT_ID = B#16#C4 THEN
	  "DB_IO_DIAGNOSTIC".OB86_DP_MasterSystem := BYTE_TO_INT(#OB86_RACKS_FLTDb[2]);
	  "DB_IO_DIAGNOSTIC".OB86_DP_Slave := BYTE_TO_INT(#OB86_RACKS_FLTDb[3]);
	  "DB_IO_DIAGNOSTIC".FC_Diag2_Exec := TRUE;
	  "DB_IO_DIAGNOSTIC".OB86_DP_Slave_Error := TRUE;
	  "FC_Diag2"();
	END_IF;
	
	IF "DB_IO_DIAGNOSTIC".OB86_EV_CLASS = B#16#38 AND "DB_IO_DIAGNOSTIC".OB86_FLT_ID = B#16#C4 THEN
	  "DB_IO_DIAGNOSTIC".FC_Diag2_Exec := TRUE;
	  "DB_IO_DIAGNOSTIC".OB86_DP_Slave_Error := FALSE;
	  "FC_Diag2"();
	END_IF;
	
	// Profinet IO Diagnostics
	IF "DB_IO_DIAGNOSTIC".OB86_EV_CLASS = B#16#39 AND "DB_IO_DIAGNOSTIC".OB86_FLT_ID = B#16#CB THEN
	  #Temp1 := SHR(IN := #OB86_RACKS_FLTDw[1], N := 11);   // shift right 11 positions to isolate bits 11 to 14
	  "DB_IO_DIAGNOSTIC".OB86_PN_IOSystem := WORD_TO_INT(#Temp1 AND W#16#F);   // bits 11 to 14 of OB86_RACKS_FLTD
	  "DB_IO_DIAGNOSTIC".OB86_PN_Station := WORD_TO_INT(#OB86_RACKS_FLTDw[1] AND W#16#7FF);   // bits  0 to 10 of OB86_RACKS_FLTD  
	  "DB_IO_DIAGNOSTIC".FC_Diag2_Exec := TRUE;
	  "DB_IO_DIAGNOSTIC".OB86_PN_Station_Error := TRUE;
	  "FC_Diag2"();
	END_IF;
	
	IF "DB_IO_DIAGNOSTIC".OB86_EV_CLASS = B#16#38 AND "DB_IO_DIAGNOSTIC".OB86_FLT_ID = B#16#CB THEN
	  "DB_IO_DIAGNOSTIC".FC_Diag2_Exec := TRUE;
	  "DB_IO_DIAGNOSTIC".OB86_PN_Station_Error := FALSE;
	  "FC_Diag2"();
	END_IF;'''

        if 'Controller' not in type_names_to_process:
            params['OB35'] = '''// Scheduling: No controller defintion'''
        else:
            params['OB35'] = '''// PID sampling time management
ORGANIZATION_BLOCK OB35
{ S7_Optimized_Access := 'FALSE' }
   VAR_TEMP 
      OB35_EV_CLASS : Byte;   // Bits 0-3 = 1 (Coming event), Bits 4-7 = 1 (Event class 1)
      OB35_STRT_INF : Byte;   // 16#36 (OB 35 has started)
      OB35_PRIORITY : Byte;   // 11 (Priority of 1 is lowest)
      OB35_OB_NUMBR : Byte;   // 35 (Organization block 35, OB35)
      OB35_RESERVED_1 : Byte;   // Reserved for system
      OB35_RESERVED_2 : Byte;   // Reserved for system
      OB35_PHASE_OFFSET : Word;   // Phase offset (msec)
      OB35_RESERVED_3 : Int;   // Reserved for system
      OB35_EXC_FREQ : Int;   // Frequency of execution (msec)
      OB35_DATE_TIME : Date_And_Time;   // Date and time OB35 started
      I : Int;
   END_VAR


BEGIN
	"PID_EXEC_CYCLE" := DINT_TO_TIME(INT_TO_DINT(#OB35_EXC_FREQ));   // getting OB35 sampling time
	
"CPC_SCHED"(PID_EXEC_CYCLE := "PID_EXEC_CYCLE");
	
	// Reset bit activated DB_SCHED
	FOR #I := 1 TO %s DO
	  IF "DB_SCHED".LOOP_DAT[#I].ENABLE THEN
	    "FC_CONTROLLER"(group := #I);
	    "DB_SCHED".LOOP_DAT[#I].ENABLE := false;
	  END_IF;
	END_FOR;
END_ORGANIZATION_BLOCK''' % self.thePlugin.getGroupMaxNb()

        output = self.process_template(".scl", params)
        self.thePlugin.writeInstanceInfo("4_Compilation_OB.scl", output)

    def get_s7_400_db(self, controller_exists_in_generation):
        """ For S7-400 PLC create:
                OB81 (Power Supply Error detection)
                OB83 (Insert/Remove Module Interrupt detection),
                OB84 (CPU Hardware Fault detection)
                OB101 (Hot Restart).
        """
        s7_400_db = '''
// Power Supply Fault
ORGANIZATION_BLOCK OB81
{ S7_Optimized_Access := 'FALSE' }
   VAR_TEMP 
      Info: Array[0..19] OF Byte;   // Reserved
   END_VAR


BEGIN
	;
END_ORGANIZATION_BLOCK



// I/O Point fault 2
ORGANIZATION_BLOCK OB83
{ S7_Optimized_Access := 'FALSE' }
   VAR_TEMP 
      Info: Array[0..19] OF Byte;   // Reserved
   END_VAR


BEGIN
	;
END_ORGANIZATION_BLOCK



// CPU fault
ORGANIZATION_BLOCK OB84
{ S7_Optimized_Access := 'FALSE' }
   VAR_TEMP 
      Info: Array[0..19] OF Byte;   // Reserved
   END_VAR;


BEGIN

END_ORGANIZATION_BLOCK



(*HOT RESTART*********************************************)
ORGANIZATION_BLOCK OB101
{ S7_Optimized_Access := 'FALSE' }
   VAR_TEMP
      OB101_EV_CLASS : Byte;
      OB101_STRUP : Byte;
      OB101_PRIORITY : Byte;
      OB101_OB_NUMBR : Byte;
      OB101_RESERVED_1 : Byte;
      OB101_RESERVED_2 : Byte;
      OB101_STOP : Word;
      OB101_STRT_INFO : DWord;
      OB101_DATE_TIME : Date_And_Time;
   END_VAR


BEGIN
	// Time Smoothing (alarms)
	"UNICOS_TimeSmooth" := 10;
	
	// Init of First_Cycle variable
	"First_Cycle" := TRUE;
	
	// Init of Live counter marker form Live counter datablock
	"UNICOS_LiveCounter" := "DB_WINCCOA".UNICOS_LiveCounter;
	
	%(FC_CONTROLLER)s
END_ORGANIZATION_BLOCK



(*COLD RESTART**********************************************)
ORGANIZATION_BLOCK OB102
{ S7_Optimized_Access := 'FALSE' }
   VAR_TEMP 
      OB102_EV_CLASS : Byte;
      OB102_STRUP : Byte;
      OB102_PRIORITY : Byte;
      OB102_OB_NUMBR : Byte;
      OB102_RESERVED_1 : Byte;
      OB102_RESERVED_2 : Byte;
      OB102_STOP : Word;
      OB102_STRT_INFO : DWord;
      OB102_DATE_TIME : Date_And_Time;
   END_VAR
BEGIN
	// Time Smoothing (alarms)
	"UNICOS_TimeSmooth" := 10;
	
	// Init of First_Cycle variable
	"First_Cycle" := TRUE;
	
	// Init of Live counter marker form Live counter datablock
	"UNICOS_LiveCounter" := "DB_WINCCOA".UNICOS_LiveCounter;
	
	%(FC_CONTROLLER)s
END_ORGANIZATION_BLOCK
'''
        params = {
            'FC_CONTROLLER': ""
        }
        if controller_exists_in_generation:
            params['FC_CONTROLLER'] = '''FC_CONTROLLER(group:=0);   // All Controllers running (option=0)'''
        return s7_400_db % params

    def get_recipe_db(self, xml_config):
    
        generate_buffers = xml_config.getPLCParameter("RecipeParameters:GenerateBuffers").strip().lower()
        if generate_buffers != "true":
            return ""
            
        recipe_db = '''
	// Calling the Recipes Mechanism and passing the information from WinCCOA (DB_RECIPES_INTERFACE) to the CPC_DB_RECIPES
	"CPC_DB_RECIPES"(
	                 Header := "DB_RECIPES_INTERFACE".RecipeHeader,   // Header from DB_RECIPES_INTERFACE to CPC_DB_RECIPES
	                 DBnum := %(DBnum)s,   // DB number of Recipe buffers
	                 HeaderBase := %(HeaderBase)s,   // Header buffer base address
	                 StatusBase := %(StatusBase)s,   // Recipe Status buffer base address
	                 ManRegAddrBase := %(ManRegAddrBase)s,   // Manual Requests addresses buffer base address
	                 ManRegValBase := %(ManRegValBase)s,   // Manual Reuqests values buffer base address
	                 ReqAddrBase := %(ReqAddrBase)s,   // Request Address buffer base address
	                 ReqValBase := %(ReqValBase)s,   // Request Values buffer base address
	                 BuffersBase := %(ManRegAddrBase)s,   // Buffers base address
	                 BuffersEnd := %(BuffersEnd)s,   // Buffers last address
	                 Timeout := T#%(Timeout)ss   // Recipe transfer timeout
	);
	
	"DB_RECIPES_INTERFACE".RecipeStatus := "CPC_DB_RECIPES".Status;   // Status from CPC_DB_RECIPES to DB_RECIPES_INTERFACE'''
        
        header_buffer_size = int(xml_config.getPLCParameter("RecipeParameters:HeaderBufferSize").strip())
        buffer_size_calculated = self.get_recipe_buffer_size(xml_config)
        params = {
            'DBnum': self.thePlugin.getAddress("DB_RECIPES_INTERFACE"),
            'HeaderBase': '2',
            'StatusBase': header_buffer_size * 2 + 2,
            'ManRegAddrBase': header_buffer_size * 4 + 2,
            'ManRegValBase': header_buffer_size * 4 + buffer_size_calculated * 2 + 2,
            'ReqAddrBase': header_buffer_size * 4 + buffer_size_calculated * 4 + 2,
            'ReqValBase': header_buffer_size * 4 + buffer_size_calculated * 6 + 2,
            'BuffersEnd': header_buffer_size * 4 + buffer_size_calculated * 10 + 2,
            'Timeout': xml_config.getPLCParameter("RecipeParameters:ActivationTimeout")
        }

        return recipe_db % params

    def get_diagnostic_db(self, unicos_project, types_to_process, plc_type="S7-1500"):
        if ("S7-1500".lower() == plc_type.lower()):
            rolCall = '''ROL(IN := #Mask, N := INT_TO_UINT(#i));'''
        else:
            rolCall = '''ROL(IN := #Mask, N := #i);'''
        diagnostic_db = '''(* Diagnostic information from the OB82 and RECORD retreived from SFB52 *)
FUNCTION_BLOCK "FB_diag1"
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : UNICOS
FAMILY : DIAG
NAME : Diagnose
//
// DIAGNOSTIC FUNCTION FOR I/O CARDS
   VAR 
      IOFlag : Byte;   // = B#16#54 for an Input and B#16#55 for an Output
      MDL_ADDR : Word;   // Address of the module
      RDREC : RDREC;
      RDREC_Data : Array[0..15] of Byte;
      OB86_EV_CLASS : Byte;   // 16#38/39 Event class 3
      OB86_FLT_ID : Byte;   // 16#C1/C4/C5, Fault identifcation code
      OB86_RACKS_FLTD : DWord;   // Rack in fault
      OB86_DP_MasterSystem : Int;   // DP Master System number
      OB86_PN_IOSystem : Int;   // PN IO System number
      OB86_DP_Slave : Int;   // DP Slave number
      OB86_PN_Station : Int;   // PN IO Station number
      OB86_DP_Slave_Error : Bool;   // DP slave error
      OB86_PN_Station_Error : Bool;   // PN IO Station error
      FB_Diag1_Exec : Bool;   // Execution of FB_Diag1 in progress
      FC_Diag2_Exec : Bool;   // Execution of FC_Diag2 in progress
   END_VAR

   VAR_TEMP 
      PLC_ADDR : Real;
      T : Int;   // type of card (DI=1 AI=2 DO=3 AO=4)
      i : Int;
      j : Int;
      k : Int;
      l : Int;
      Nr_Of_Channels : Int;
      Index : Int;
      Index2 : Int;
      Index3 : Int;
      Mask : Byte;
      Channel_Error : Bool;
      Break : Bool;
      Card_Error : Bool;
   END_VAR


BEGIN
	// Call SFB52 (RDREC) constantly until the RECORD is retreived.
	IF #IOFlag = B#16#55 THEN
	  #RDREC.ID := #MDL_ADDR OR W#16#8000;
	ELSE
	  #RDREC.ID := #MDL_ADDR;
	END_IF;
	
	REPEAT
	  #RDREC(REQ := #FB_Diag1_Exec,
	         INDEX := 1,
	         MLEN := 16,
	         RECORD := #RDREC_Data
	  );
	  
	  #FB_Diag1_Exec := FALSE;
	UNTIL #RDREC.BUSY = FALSE
	END_REPEAT;
	
	// Type of card
	IF (B#16#7F AND #RDREC_Data[4]) = B#16#70 THEN
	  #T := 1;
	END_IF;   // DI
	IF ((B#16#7F AND #RDREC_Data[4]) = B#16#71) OR ((B#16#7F AND #RDREC_Data[4]) = B#16#7B) THEN
	  #T := 2;
	END_IF; // AI. Code 7B from ET200S AI modules
	IF (B#16#7F AND #RDREC_Data[4]) = B#16#72 THEN
	  #T := 3;
	END_IF; // DO
	IF ((B#16#7F AND #RDREC_Data[4]) = B#16#73) OR ((B#16#7F AND #RDREC_Data[4]) = B#16#7C) THEN
	  #T := 4;
	END_IF; // AO. Code 7C from ET200S AO modules
	
	// Number of channels in this module
	#Nr_Of_Channels := BYTE_TO_INT(#RDREC_Data[6]);
	
	// Detection of module error
	#Card_Error := ((B#16#F0 AND #RDREC_Data[0]) <> 0);
	
	// Number of bytes containing information o chanmnel in the RECORD
	#Index := (#Nr_Of_Channels / 8) - 1;   // Number of groups of 8 channels in the module (e.g if 12 AI module then Index = 1)
	IF #Index < 0 THEN   // If 8 or less AI modules, then Index = 0
	  #Index := 0;
	END_IF;
	#Index2 := #Nr_Of_Channels MOD 8;   // Number of channels outside gropus of 8 (e.g 12 AI module Index2 = 4)
	
	// Scan of all channel information in the RECORD BYTE 7 onwards
	FOR #k := 0 TO #Index DO
	  IF #k = #Index AND #Index2 > 0 THEN
	    #Index3 := #Index2 - 1;
	  ELSE
	    #Index3 := 7;
	  END_IF;
	  FOR #i := 0 TO #Index3 DO
	    // Init
	    #Break := 0;
	    #Mask := B#16#01;
	    #Mask := ''' + rolCall + '''
	    
	    // Calculation OF PLC Address according to the type of channel
	    CASE #T OF
	      1:
	        #PLC_ADDR := INT_TO_REAL(WORD_TO_INT(#MDL_ADDR)) + #k + INT_TO_REAL(#i) / 10;   // DI
	      2:
	        #PLC_ADDR := INT_TO_REAL(WORD_TO_INT(#MDL_ADDR)) + 16 * #k + #i * 2;   // AI
	      3:
	        #PLC_ADDR := INT_TO_REAL(WORD_TO_INT(#MDL_ADDR)) + #k + INT_TO_REAL(#i) / 10;   // DO
	      4:
	        #PLC_ADDR := INT_TO_REAL(WORD_TO_INT(#MDL_ADDR)) + 16 * #k + #i * 2;   // AO
	    END_CASE;
	    
	    // IO Error detection for every channel
	    IF ((#RDREC_Data[7 + #k] AND #Mask) = #Mask) OR #Card_Error THEN
	      #Channel_Error := 1;   // IO error
	    ELSE
	      #Channel_Error := 0;    // No IO error
	    END_IF;
	    
	    // Init
	    #j := 1;
	    CASE #T OF
	      0:
	        ;
%(DIAGNOSTIC_CASE1)s
%(DIAGNOSTIC_CASE2)s
%(DIAGNOSTIC_CASE3)s
%(DIAGNOSTIC_CASE4)s
	    END_CASE;
	  END_FOR;
	END_FOR;
END_FUNCTION_BLOCK

DATA_BLOCK "DB_IO_DIAGNOSTIC" "FB_Diag1"
BEGIN
END_DATA_BLOCK



(* Diagnostic of Profibus slaves *)
FUNCTION "FC_Diag2" : Void
{ S7_Optimized_Access := 'FALSE' }
AUTHOR : UNICOS
FAMILY : DIAG
NAME : Diagnose
//
// Diagnostic of profibus slaves

BEGIN
	"DB_IO_DIAGNOSTIC".FC_Diag2_Exec := FALSE;
%(FC_Diag2)s
END_FUNCTION
'''
        params = {
            'DIAGNOSTIC_CASE1': '',
            'DIAGNOSTIC_CASE2': '',
            'DIAGNOSTIC_CASE3': '',
            'DIAGNOSTIC_CASE4': '',
            'FC_Diag2': ''
        }

        for device_type, instance_amount in types_to_process.iteritems():
            device_type_name = device_type.getDeviceTypeName()
            if device_type_name == "DigitalInput":
                params['DIAGNOSTIC_CASE1'] = '''	      1:
	        WHILE (#j < %s + 1 AND #Break = 0) DO // DI
	          IF "DI_ERROR".IOERROR[#j].ADDR = #PLC_ADDR THEN
	            "DI_ERROR".IOERROR[#j].Err := #Channel_Error;
	            #Break := 1;
	          END_IF;
	          #j := #j + 1;
	        END_WHILE;''' % instance_amount
            if device_type_name == "AnalogInput":
                params['DIAGNOSTIC_CASE2'] = '''	      2:
	        WHILE (#j < %s + 1 AND #Break = 0) DO   // AI
	          IF "AI_ERROR".IOERROR[#j].ADDR = #PLC_ADDR THEN
	            "AI_ERROR".IOERROR[#j].Err := #Channel_Error;
	            #Break := 1;
	          END_IF;
	          #j := #j + 1;
	        END_WHILE;''' % instance_amount

            if device_type_name == "DigitalOutput":
                params['DIAGNOSTIC_CASE3'] = '''	      3:
	        WHILE (#j < %s + 1 AND #Break = 0) DO // DO
	          IF "DO_ERROR".IOERROR[#j].ADDR = #PLC_ADDR THEN
	            "DO_ERROR".IOERROR[#j].Err := #Channel_Error;
	            #Break := 1;
	          END_IF;
	          #j := #j + 1;
	        END_WHILE;''' % instance_amount

            if device_type_name == "AnalogOutput":
                params['DIAGNOSTIC_CASE4'] = '''	      4:
	        WHILE (#j < %s + 1 AND #Break = 0) DO // AO
	          IF "AO_ERROR".IOERROR[#j].ADDR = #PLC_ADDR THEN
	            "AO_ERROR".IOERROR[#j].Err := #Channel_Error;
	            #Break := 1;
	          END_IF;
	          #j := #j + 1;
	        END_WHILE;''' % instance_amount

        DPinstances = unicos_project.findMatchingInstances("DigitalInput,DigitalOutput,AnalogInput,AnalogOutput", "'#FEDeviceIOConfig:FEChannel:InterfaceParam10#'!=''")
        PNinstances = unicos_project.findMatchingInstances("DigitalInput,DigitalOutput,AnalogInput,AnalogOutput", "'#FEDeviceIOConfig:FEChannel:InterfaceParam9#'!=''")
        DPslaves = {}
        PNslaves = {}

        for instance in DPinstances:
            DPslave_id = int(instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam10"))
            if not DPslaves.has_key(DPslave_id):
                DPslaves[DPslave_id] = list()
            DPslaves[DPslave_id].append(instance)

        DPslave_ids = DPslaves.keys()
        DPslave_ids.sort()
        for DPslave_id in DPslave_ids:
            params['FC_Diag2'] += '''	(* I/O Objects mapped to DP Slave number %s *)\n''' % DPslave_id
            params['FC_Diag2'] += '''	IF "DB_IO_DIAGNOSTIC".OB86_DP_Slave = %s THEN\n''' % DPslave_id
            for instance in DPslaves[DPslave_id]:
                instance_type = instance.getDeviceTypeName()
                instance_number = instance.getInstanceNumber()
                if instance_type == "DigitalInput":
                    params['FC_Diag2'] += '''	  "DI_ERROR".IOERROR[''' + str(instance_number) + '''].Err := "DB_IO_DIAGNOSTIC".OB86_DP_Slave_Error;\n'''
                if instance_type == "DigitalOutput":
                    params['FC_Diag2'] += '''	  "DO_ERROR".IOERROR[''' + str(instance_number) + '''].Err := "DB_IO_DIAGNOSTIC".OB86_DP_Slave_Error;\n'''
                if instance_type == "AnalogInput":
                    params['FC_Diag2'] += '''	  "AI_ERROR".IOERROR[''' + str(instance_number) + '''].Err := "DB_IO_DIAGNOSTIC".OB86_DP_Slave_Error;\n'''
                if instance_type == "AnalogOutput":
                    params['FC_Diag2'] += '''	  "AO_ERROR".IOERROR[''' + str(instance_number) + '''].Err := "DB_IO_DIAGNOSTIC".OB86_DP_Slave_Error;\n'''
            params['FC_Diag2'] += '''	END_IF;\n'''

        for instance in PNinstances:
            PNslave_id = int(instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam9"))
            if not PNslaves.has_key(PNslave_id):
                PNslaves[PNslave_id] = list()
            PNslaves[PNslave_id].append(instance)

        PNslave_ids = PNslaves.keys()
        PNslave_ids.sort()
        for PNslave_id in PNslave_ids:
            params['FC_Diag2'] += '''	(* I/O Objects mapped to PN Station number %s *)\n''' % PNslave_id
            params['FC_Diag2'] += '''	IF "DB_IO_DIAGNOSTIC".OB86_PN_Station = %s THEN\n''' % PNslave_id
            for instance in PNslaves[PNslave_id]:
                instance_type = instance.getDeviceTypeName()
                instance_number = instance.getInstanceNumber()
                if instance_type == "DigitalInput":
                    params['FC_Diag2'] += '''	  "DI_ERROR".IOERROR[''' + str(instance_number) + '''].Err := "DB_IO_DIAGNOSTIC".OB86_PN_Station_Error;\n'''
                if instance_type == "DigitalOutput":
                    params['FC_Diag2'] += '''	  "DO_ERROR".IOERROR[''' + str(instance_number) + '''].Err := "DB_IO_DIAGNOSTIC".OB86_PN_Station_Error;\n'''
                if instance_type == "AnalogInput":
                    params['FC_Diag2'] += '''	  "AI_ERROR".IOERROR[''' + str(instance_number) + '''].Err := "DB_IO_DIAGNOSTIC".OB86_PN_Station_Error;\n'''
                if instance_type == "AnalogOutput":
                    params['FC_Diag2'] += '''	  "AO_ERROR".IOERROR[''' + str(instance_number) + '''].Err := "DB_IO_DIAGNOSTIC".OB86_PN_Station_Error;\\n'''
            params['FC_Diag2'] += '''	END_IF;\n'''

        return diagnostic_db % params
