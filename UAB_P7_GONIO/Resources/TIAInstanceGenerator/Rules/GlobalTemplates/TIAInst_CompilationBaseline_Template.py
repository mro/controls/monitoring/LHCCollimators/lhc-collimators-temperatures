# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from TIAInst_Generic_Template import TIAInst_Generic_Template


class CompilationBaseline_Template(TIAInst_Generic_Template):

    def process(self, unicos_project, xml_config, generate_global_files, *_):
        """
        General Steps for the Baseline compilation file:
        1. Launch the global SCL files from the Baseline
        2. Launch the DeviceType SCL files from the Baseline

        :param unicos_project:
        :param xml_config:
        :param generate_global_files: comes from "Global files scope" dropdown on Wizard.
                true = All types. false = Selected types.
        """
        global_scl = '''
(*Launch of the SCL files********************************************************)
//Data types and functions
CPC_BASE_Unicos.SCL;
%(1500_specific)s
(*FBs**************************)
%(device_types)s
'''
        params = {
            '1500_specific': "",
            'device_types': ""
        }

        self.thePlugin.writeInUABLog("processInstances in Jython for CompilationBaseline.")
        xml_config = self.thePlugin.getXMLConfig() # TODO: remove this 
        tiaDeclarations = xml_config.getTiaPLCDeclarations()
        thePLCName = tiaDeclarations.get(0).getName()
        plcType = xml_config.getPLCParameter(thePLCName + ":PLCType").lower()
        
        if ("S7-1500".lower() == plcType):
            params['1500_specific'] = '''CPC_DIF.SCL;
CPC_FILTER.SCL;
CPC_INTEG.SCL;
CPC_RAMP.SCL;'''

        types_to_process = self.get_types_to_process(self.thePlugin, unicos_project, xml_config, generate_global_files)
        for device_type in types_to_process:
            device_type_name = device_type.getDeviceTypeName()
            representation_name = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", device_type_name)
            params['device_types'] += '''CPC_FB_%s.SCL;\r\n''' % representation_name

        self.thePlugin.writeInstanceInfo("1_Compilation_Baseline.INP", global_scl % params)
