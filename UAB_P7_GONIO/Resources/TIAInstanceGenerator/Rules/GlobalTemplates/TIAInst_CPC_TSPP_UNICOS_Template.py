# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for CPC_TSPP_UNICOS Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from time import strftime

from TIAInst_Generic_Template import TIAInst_Generic_Template

from research.ch.cern.unicos.utilities import AbsolutePathBuilder
from java.io import File


class CPC_TSPP_UNICOS_Template(TIAInst_Generic_Template):

    def process(self, unicos_project, xml_config, generate_global_files, *_):
        
        xml_config = self.thePlugin.getXMLConfig() # TODO: remove this 
        tiaDeclarations = xml_config.getTiaPLCDeclarations()
        thePLCName = tiaDeclarations.get(0).getName()
        plcType = xml_config.getPLCParameter(thePLCName + ":PLCType")

        nbStatusTables = self.thePlugin.getSumNbStatusTable()
        if nbStatusTables > 150:
            maxTableInOneSend = 150
        else:
            maxTableInOneSend = nbStatusTables

        # constants to be set by user
        MaxNbOfTSEvent = 100           # Nb of event wich will trig the send to WinCCOA
        SpareEventNumber = 50            # spare places to be able to continue to ../.. record event when the send function is buzy
        Word_in_one_TSEvent = 2             # Number of data word in one Event
        MaxStatusTable = nbStatusTables
        # MaxStatusTable     = 100           # Max number of status tables (the length of one table is 200 bytes without the header)
        MaxTableInOneSend = maxTableInOneSend           # (MAX value S7-400:250  S7-300:100) Max nb of status tables in one TSPP message ; Must lower or equal to MaxStatusTable
        # MaxTableInOneSend  = 100          # (MAX value S7-400:250  S7-300:100) Max nb of status tables in one TSPP message ; Must lower or equal to MaxStatusTable
        ListEventSize = xml_config.getPLCParameter("SiemensSpecificParameters:GeneralConfiguration:EventBufferSize")
        # ListEventSize      = 1000          # Size of the Event List

        # calculated values
        ExtendedNbOfEvent = MaxNbOfTSEvent + SpareEventNumber         # MaxNbOfTSEvent + SpareEventNumber; // Full size of event buffer
        TSEventHeaderSize = 6                                         # TimeStamp + DB number + Address
        TSEventWordSize = Word_in_one_TSEvent + TSEventHeaderSize   # Word_in_one_TSEvent + TSEventHeaderSize;
        TSEventByteSize = TSEventWordSize * 2
        EventByteDataSize = Word_in_one_TSEvent * 2
        MaxStatusReqNb = MaxStatusTable
        StatusReqListSize = MaxStatusReqNb + 1
        StatusWordSize = 100                                       # Size of status table
        StatusByteSize = StatusWordSize * 2
        MaxReqNumber = 3
        ReqListSize = MaxReqNumber + 1
        MaxBufferSize_Event = 6 + (TSEventByteSize * ExtendedNbOfEvent)
        MaxBufferSize_Status = 6 + ((StatusByteSize + 12) * MaxTableInOneSend)

        MaxBufferSize = int(MaxBufferSize_Status + (MaxBufferSize_Event - MaxBufferSize_Status) * (1.0 / (1.0 + MaxBufferSize_Status / (MaxBufferSize_Event + 1))))
        EventExtension = (MaxBufferSize_Event - MaxBufferSize_Status) * (1 // (1 + (MaxBufferSize_Status // MaxBufferSize_Event)))

        # verification of values
        if (("S7-300".lower() == plcType.lower() and MaxTableInOneSend > 100) or (MaxTableInOneSend > 250)):
            self.thePlugin.writeErrorInUABLog("MaxTableInOneSend = " + MaxTableInOneSend + " is too large number for plc " + plcType)
        if (MaxTableInOneSend > MaxStatusTable):
            self.thePlugin.writeErrorInUABLog("MaxTableInOneSend value (" + MaxTableInOneSend + ") must be lower than MaxStatusTable value (" + MaxStatusTable + ")")

        
        params = {
            'ReqListSize'           : ReqListSize,
            'StatusReqListSize'     : StatusReqListSize,
            'Word_in_one_TSEvent'   : Word_in_one_TSEvent,
            'MaxNbOfTSEvent'        : MaxNbOfTSEvent,
            'SpareEventNumber'      : SpareEventNumber,
            'MaxStatusTable'        : MaxStatusTable,
            'MaxTableInOneSend'     : MaxTableInOneSend,
            'ListEventSize'         : ListEventSize,
            'ExtendedNbOfEvent'     : ExtendedNbOfEvent,
            'TSEventHeaderSize'     : TSEventHeaderSize,
            'TSEventWordSize'       : TSEventWordSize,
            'TSEventByteSize'       : TSEventByteSize,
            'EventByteDataSize'     : EventByteDataSize,
            'MaxStatusReqNb'        : MaxStatusReqNb,
            'StatusWordSize'        : StatusWordSize,
            'StatusByteSize'        : StatusByteSize,
            'MaxReqNumber'          : MaxReqNumber,
            'MaxBufferSize_Event'   : MaxBufferSize_Event,
            'MaxBufferSize_Status'  : MaxBufferSize_Status,
            'MaxBufferSize'         : MaxBufferSize,
            'EventExtension'        : EventExtension,
            'DB_pointer_type'       : "",
            'NumberOfTableDB'       : "",
            'TablesInDB'            : "",
            'LastTableSize'         : "",
            'ActualTableList'       : "",
            'OldTableList'          : "",
            'MoveActualToOld'       : "",
            'EventListElement'      : "",
            'TransferTimestamp'     : "",
            'TS_EventBuffer'        : "",
            'MoveActualToLocal'     : "",
            'MoveOldToLocal'        : "",
            'IPReceived'            : "",
            'TempCounter'           : "",
            'UpdateWatchdog'        : "",
            'EventBufferToBsend'    : "",
            'ActualToBsend'         : ""
        }
        
        if "S7-1500".lower() == plcType.lower():
            params['DB_pointer_type'] = '''DB_ANY'''
            params['NumberOfTableDB'] = '''PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := 0)'''
            params['TablesInDB']      = '''PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := (6 + ((#Index - 1) * 6)))'''
            params['LastTableSize']   = '''PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := (6 + ((#Index - 1) * 6)))'''
            params['ActualTableList'] = '''PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := (2 + ((#Index - 1) * 6)))'''
            params['OldTableList']    = '''PEEK_WORD(area := 16#84, dbNumber := #ListOfStatusTable, byteOffset := (4 + ((#Index - 1) * 6)))'''
            params['MoveActualToOld'] = '''	      POKE_BLK(
	               area_src := 16#84,
	               dbNumber_src := #CreateActual.DBNumber,
	               byteOffset_src := DWORD_TO_DINT(#CreateActual.Address),
	               area_dest := 16#84,
	               dbNumber_dest := #CreateOld.DBNumber,
	               byteOffset_dest := DWORD_TO_DINT(#CreateOld.Address),
	               count := #CreateActual.NbOfData
	      );
	      
	      #Result := 0; // !!'''
            params['EventListElement'] = '''	      #EventListElement.IDAndType := PEEK_WORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr);
	      #EventListElement.DataAndDBNb := PEEK_DWORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr + 2);
	      #EventListElement.Address := PEEK_DWORD(area := 16#84, dbNumber := #EventListDB, byteOffset := #EventListAdr + 6);'''
            params['TransferTimestamp'] = '''	          #TS_EventBuffer.Data[#CurrentEvent].TimeStamp := LDT_TO_DT(LWORD_TO_LDT(PEEK_LWORD(area := 16#84, dbNumber := #IN_Event.DBNumber, byteOffset := DWORD_TO_DINT(#IN_Event.Address))));
	          #Result := 0; // !!'''
            params['TS_EventBuffer'] = '''	        FOR #Index4 := 1 TO #Word_in_one_TSEvent DO
	          #TS_EventBuffer.Data[#CurrentEvent].Data[#Index4] := PEEK_WORD(area := 16#84,
	                                                                         dbNumber := #Event.DBNumber,
	                                                                         byteOffset := DWORD_TO_DINT(#Event.Address) + 2 * (#Index4 - 1));
	        END_FOR;
	        #Result := 0; // !!'''
            params['MoveActualToLocal'] = '''	      FOR #Index4 := 1 TO #CreateActual.NbOfData / 2 DO
	        #LocalActual[#Index4] := PEEK_WORD(area := 16#84,
	                                           dbNumber := #CreateActual.DBNumber,
	                                           byteOffset := DWORD_TO_DINT(#CreateActual.Address) + 2 * (#Index4 - 1));
	      END_FOR;
	      #Result := 0; // !!'''
            params['MoveOldToLocal'] = '''	      FOR #Index4 := 1 TO #CreateOld.NbOfData / 2 DO
	        #LocalOld[#Index4] := PEEK_WORD(area := 16#84,
	                                        dbNumber := #CreateOld.DBNumber,
	                                        byteOffset := DWORD_TO_DINT(#CreateOld.Address) + 2 * (#Index4 - 1));
	      END_FOR;
	      #Result := 0; // !!'''
            params['IPReceived'] = '''	  IF DWORD_TO_DINT(PEEK_DWORD(area := 16#84, dbNumber := WORD_TO_INT(#IN_WatchDog.DBNumber), byteOffset := 0)) <> -1 THEN // new IP received
	    POKE_DWORD(area := 16#84,
	               dbNumber := WORD_TO_INT(#IN_WatchDog.DBNumber), 
	               byteOffset := 0, 
	               value := DINT_TO_DWORD(-1));'''
            params['TempCounter'] = '''#TempCounter := WORD_TO_INT(PEEK_WORD(area := 16#84, dbNumber := WORD_TO_INT(#IN_WatchDog.DBNumber), byteOffset := #DWNumber));'''
            params['UpdateWatchdog'] = '''POKE_WORD(area := 16#84,
	         dbNumber := WORD_TO_INT(#IN_WatchDog.DBNumber),
	         byteOffset := #DWNumber,
	         value := INT_TO_WORD(#TempCounter));'''
            params['EventBufferToBsend'] = '''	        #Result := BLKMOV(SRCBLK := #TS_EventBuffer,
	                          DSTBLK => #BSendBuffer   // Struct
	        );'''
            params['ActualToBsend'] = '''	          FOR #Index4 := 1 TO #CreateActual.NbOfData / 2 DO
	            #BSendBuffer.TableList[#TSPPTableIndex].Table[#Index4] := PEEK_WORD(area := 16#84,
	                                                                                dbNumber := #CreateActual.DBNumber,
	                                                                                byteOffset := DWORD_TO_DINT(#CreateActual.Address) + 2 * (#Index4 - 1));
	          END_FOR;
	          POKE_BLK(
	                   area_src := 16#84,
	                   dbNumber_src := #CreateActual.DBNumber,
	                   byteOffset_src := DWORD_TO_DINT(#CreateActual.Address),
	                   area_dest := 16#84,
	                   dbNumber_dest := #CreateOld.DBNumber,
	                   byteOffset_dest := DWORD_TO_DINT(#CreateOld.Address),
	                   count := #CreateActual.NbOfData
	          );'''
        else:
            params['DB_pointer_type'] = "Block_DB"
            params['NumberOfTableDB'] = "#ListOfStatusTable.DW(0)"
            params['TablesInDB']      = "#ListOfStatusTable.DW(6 + ((#Index - 1) * 6))"
            params['LastTableSize']   = "#ListOfStatusTable.DW(6 + ((#Index - 1) * 6))"
            params['ActualTableList'] = "#ListOfStatusTable.DW(2 + ((#Index - 1) * 6))"
            params['OldTableList']    = "#ListOfStatusTable.DW(4 + ((#Index - 1) * 6))"
            params['MoveActualToOld'] = '''	      #Result := BLKMOV(
	                        SRCBLK :=  #ActualTable,
	                        DSTBLK => #OldTable
	      );'''
            params['EventListElement'] = '''	      #EventListElement.IDAndType := #EventListDB.DW(#EventListAdr);
	      #EventListElement.DataAndDBNb := #EventListDB.DD(#EventListAdr + 2);
	      #EventListElement.Address := #EventListDB.DD(#EventListAdr + 6);'''
            params['TransferTimestamp'] = '''	          #Result := BLKMOV(SRCBLK := #EventData,
	                            DSTBLK => #TS_EventBuffer.Data[#CurrentEvent].TimeStamp
	          ); // Transfer associated timestamp'''
            params['TS_EventBuffer'] = '''	        #Result := BLKMOV(SRCBLK := #EventPtr, // IN: Any
	                          DSTBLK => #TS_EventBuffer.Data[#CurrentEvent].Data // OUT: Any
	        ); // INT'''
            params['MoveActualToLocal'] = '''	      #Result := BLKMOV(SRCBLK :=  #ActualTable,   // IN: Any
	                        DSTBLK => #LocalActual   // OUT: Any
	      );                         // INT'''
            params['MoveOldToLocal'] = '''	      #Result := BLKMOV(SRCBLK :=  #OldTable,   // IN: Any
	                        DSTBLK => #LocalOld   // OUT: Any
	      );   // INT'''
            params['IPReceived'] = '''	  IF DWORD_TO_DINT(WORD_TO_BLOCK_DB(#IN_WatchDog.DBNumber).DD(0)) <> -1 THEN   // new IP received; corrected parentheses
	    WORD_TO_BLOCK_DB(#IN_WatchDog.DBNumber).DD(0) := DINT_TO_DWORD(-1);   // corrected parentheses'''
            params['TempCounter'] = '''#TempCounter := WORD_TO_INT(WORD_TO_BLOCK_DB(#IN_WatchDog.DBNumber).DW(#DWNumber));'''
            params['UpdateWatchdog'] = '''WORD_TO_BLOCK_DB(#IN_WatchDog.DBNumber).DW(#DWNumber):= INT_TO_WORD(#TempCounter);'''
            params['EventBufferToBsend'] = '''	        #ATSrc.NbOfData := #EventBufferSize;
	        #Result := BLKMOV(SRCBLK :=  #Src,   // IN: Any
	                          DSTBLK => #BSendBuffer   // OUT: Any
	        );   // INT '''
            params['ActualToBsend'] = '''                          #Result :=  BLKMOV(SRCBLK :=  #ActualTable,// Copy Actual table in BSend buffer
                                             DSTBLK => #BSendBuffer.TableList[#TSPPTableIndex].Table // OUT: Any
                                            ); // INT
                          
                          #Result :=  BLKMOV(SRCBLK :=  #ActualTable,// Copy actual table in old table
                                             DSTBLK => #OldTable // OUT: Any
                                            ); // INT'''
                                          
        output = self.process_template(".scl", params)
        self.thePlugin.writeInstanceInfo("CPC_TSPP_UNICOS.scl", output)
