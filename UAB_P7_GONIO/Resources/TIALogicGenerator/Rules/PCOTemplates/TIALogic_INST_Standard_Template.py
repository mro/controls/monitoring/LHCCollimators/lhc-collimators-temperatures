# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)


def INSTLogic(thePlugin, theRawInstances, master, name, LparamVector, currentRecordNumber):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeSiemensLogic('''
FUNCTION $name$_INST : VOID
TITLE = '$name$_INST'
//
// Instance of $name$
//
// Lparam1:	$Lparam1$
// Lparam2:	$Lparam2$
// Lparam3:	$Lparam3$
// Lparam4:	$Lparam4$
// Lparam5:	$Lparam5$
// Lparam6:	$Lparam6$
// Lparam7:	$Lparam7$
// Lparam8:	$Lparam8$
// Lparam9:	$Lparam9$
// Lparam10:	$Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_SP'
FAMILY: 'INST'
VAR_TEMP
    old_status1 : DWORD;
    old_status2 : DWORD;
END_VAR
BEGIN

''')
    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    # Step 1.1: Write the StsReg in the old_status, run the CPC_FB_PCO and write the status in the DB_bin_status_PCO and DB_ana_status_PCO
    thePlugin.writeSiemensLogic('''

old_status1 := DB_bin_status_PCO.$name$.stsreg01;
old_status2 := DB_bin_status_PCO.$name$.stsreg02;
old_status1 := ROR(IN:=old_status1, N:=16);
old_status2 := ROR(IN:=old_status2, N:=16);
''')

    thePlugin.writeSiemensLogic('''
// call to the FB
CPC_FB_PCO.$name$(

    // Passing the Manual Request from the DB_PCO_ManRequest to the DB $name$
    Manreg01 :=  DB_PCO_ManRequest.PCO_Requests[''' + str(currentRecordNumber) + '''].Manreg01,		// set by WinCCOA in the DB_PCO_ManRequest
    MOpMoR :=  DB_PCO_ManRequest.PCO_Requests[''' + str(currentRecordNumber) + '''].MOpMoR          // set by WinCCOA in the DB_PCO_ManRequest


); 
//reset AuAuMoR/AuAuDepR
$name$.AuAuMoR := FALSE;	
$name$.AuAuDepR := FALSE;	
$name$.AuAlAck := FALSE;
$name$.AuRStart := FALSE;

DB_bin_status_PCO.$name$.stsreg01:= $name$.Stsreg01; //copy to global DB
DB_bin_status_PCO.$name$.stsreg02:= $name$.Stsreg02;  //copy to global DB
DB_ana_status_PCO.$name$.AuOpMoSt:= $name$.AuOpMoSt; //copy to global DB
DB_ana_status_PCO.$name$.OpMoSt:= $name$.OpMoSt;  //copy to global DB

''')

    thePlugin.writeSiemensLogic('''

DB_Event_PCO.PCO_evstsreg[''' + str(currentRecordNumber) + '''].evstsreg01 := old_status1 OR DB_bin_status_PCO.$name$.stsreg01;
DB_Event_PCO.PCO_evstsreg[''' + str(currentRecordNumber) + '''].evstsreg02 := old_status2 OR DB_bin_status_PCO.$name$.stsreg02;

''')

    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
