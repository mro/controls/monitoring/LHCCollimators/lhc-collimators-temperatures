# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import TIALogic_DefaultAlarms_Template
# reload(TIALogic_DefaultAlarms_Template)


def ILLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = TIALogic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeSiemensLogic('''
FUNCTION $name$_IL : VOID
TITLE = '$name$_IL'
//
// Interlock Logic of $name$
//
//
// Master: 	$name$
// Name: 	Interlock
// Lparam1:	$Lparam1$
// Lparam2:	$Lparam2$
// Lparam3:	$Lparam3$
// Lparam4:	$Lparam4$
// Lparam5:	$Lparam5$
// Lparam6:	$Lparam6$
// Lparam7:	$Lparam7$
// Lparam8:	$Lparam8$
// Lparam9:	$Lparam9$
// Lparam10:	$Lparam10$

//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_IL'
FAMILY: 'IL'
VAR_TEMP
    old_status : DWORD;
END_VAR
BEGIN''')
    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    # Not configured alarms
    # Gets all the Digital Alarms that are child of the 'master' object
    theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition = TIALogic_DefaultAlarms_Template.getDigitalAlarms(theRawInstances, name)
    # Gets all the Analog Alarms that are child of the 'master' object
    theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition = TIALogic_DefaultAlarms_Template.getAnalogAlarms(theRawInstances, name)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredDAParameters" function writes default values for DA parameters:
    #
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # Else if Input is not empty, IOError, IOSimu and I will be written by main template function
    # If Delay (in spec) is "logic", writes PAlDt
    # Else if Delay is not "logic", PAlDt will be written by main template function
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    TIALogic_DefaultAlarms_Template.writeNotConfiguredDAParameters(thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredDAParameters" function writes default values for DA parameters:
    #
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # Else if Input is not empty, IOError, IOSimu and I will be written by main template function
    # If Delay (in spec) is "logic", writes PAlDt
    # Else if Delay is not "logic", PAlDt will be written by main template function
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    TIALogic_DefaultAlarms_Template.writeNotConfiguredAAParameters(thePlugin, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition)

    # Step 1.3: Fill the IOError and IOSimu from the Inputs linked by the user logic (if procceds)
    thePlugin.writeSiemensLogic('''
(*IoSimu and IoError*****************)
DB_ERROR_SIMU.$name$_IL_E := 0; // To complete
	
DB_ERROR_SIMU.$name$_IL_S := 0; // To complete
''')
    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
