# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2015 all rights reserved
# Jython source file for the Global Template File.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from time import strftime
from java.lang import System


class Global_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize in Jython for the Global Template.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for the Global Template.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for the Global Template.")
        self.dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'
        self.theApplicationName = self.thePlugin.getApplicationName()
        self.thePluginId = self.thePlugin.getId()
        self.thePluginVersion = self.thePlugin.getVersionId()
        self.theUserName = System.getProperty("user.name")

    def process(self, *params):
        self.thePlugin.writeInUABLog("Starting processing the Global Template.")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for the Global Template.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for the Global Template.")
