# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from time import strftime


class DB_ERROR_SIMU_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        #self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize in Jython for DB_ERROR_SIMU.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for DB_ERROR_SIMU.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for DB_ERROR_SIMU.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'

    def process(self, *params):
        deviceVector = params[0]
        theXMLConfig = params[1]
        genGlobalFilesForAllSections = params[2].booleanValue()  # Comes from "Global files scope" dropdown on Wizard. true = All sections. false = Selected sections.
        self.thePlugin.writeInUABLog("processInstances in Jython for Compilation_Logic.")
        # This method is called on every Instance of the current type by the Code Generation plug -in.
        self.thePlugin.writeSiemensLogic('''DATA_BLOCK "DB_ERROR_SIMU"
TITLE = 'DB_ERROR_SIMU'
{ S7_Optimized_Access := 'FALSE' }
AUTHOR: 'ICE/PLC'
FAMILY: UNICOS
NAME: Logic
//
// Error and Simu Signals of all PCO functions
//
STRUCT''')

        for theCurrentPco in deviceVector:
            thePcoSections = theCurrentPco.getSections()
            thePcoDependentSections = theCurrentPco.getDependentSections()
            theDependentDevices = theCurrentPco.getDependentDevices()
            theCurrentPcoName = theCurrentPco.getDeviceName()
            self.thePlugin.writeSiemensLogic('''// Creating ERROR-SIMU for ''' + theCurrentPcoName)
            for theCurrentSection in thePcoSections:
                if theCurrentSection.getGenerateSection() or genGlobalFilesForAllSections:
                    theCurrentSectionName = theCurrentSection.getFullSectionName()
                    theCurrentSectionNameLength = len(theCurrentSectionName)
                    sectionType = theCurrentSectionName[theCurrentSectionNameLength - 4:]
                    if not (sectionType == "CDOL" or sectionType == "INST"):
                        self.thePlugin.writeSiemensLogic(theCurrentSectionName + '''_E : Bool;''')
                        self.thePlugin.writeSiemensLogic(theCurrentSectionName + '''_S : Bool;''')

            for theCurrentSection in thePcoDependentSections:
                if theCurrentSection.getGenerateSection() or genGlobalFilesForAllSections:
                    theCurrentSectionName = theCurrentSection.getFullSectionName()
                    self.thePlugin.writeSiemensLogic(theCurrentSectionName + '''_E : Bool;''')
                    self.thePlugin.writeSiemensLogic(theCurrentSectionName + '''_S : Bool;''')

            for theCurrentDependentDevice in theDependentDevices:
                theDependentSections = theCurrentDependentDevice.getDependentSections()
                if theCurrentPcoName.lower() == "no_master":
                    for section in theDependentSections:
                        if section.getGenerateSection() or genGlobalFilesForAllSections:
                            theCurrentSectionName = section.getFullSectionName()
                            self.thePlugin.writeSiemensLogic(theCurrentSectionName + '''_E : Bool;''')
                            self.thePlugin.writeSiemensLogic(theCurrentSectionName + '''_S : Bool;''')
                else:
                    if theCurrentDependentDevice.getDeviceType() == "ProcessControlObject":
                        text = "Do nothing. These DL are already created in the PCO Dependent Sections"
                    else:
                        for section in theDependentSections:
                            if section.getGenerateSection() or genGlobalFilesForAllSections:
                                theCurrentSectionName = section.getFullSectionName()
                                self.thePlugin.writeSiemensLogic(theCurrentSectionName + '''_E : Bool;''')
                                self.thePlugin.writeSiemensLogic(theCurrentSectionName + '''_S : Bool;''')

        self.thePlugin.writeSiemensLogic('''END_STRUCT
BEGIN
END_DATA_BLOCK
	''')

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for DB_ERROR_SIMU.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for DB_ERROR_SIMU.")
