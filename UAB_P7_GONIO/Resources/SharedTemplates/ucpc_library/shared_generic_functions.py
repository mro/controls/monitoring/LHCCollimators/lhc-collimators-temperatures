# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved


class SharedGenericFunctions:

    def writeWarning(self, instance, text):
        if instance == None:
            self.thePlugin.writeWarningInUABLog(str(text))
        else:
            alias = instance.getAttributeData("DeviceIdentification:Name")
            theDeviceType = instance.getDeviceTypeName()
            self.thePlugin.writeWarningInUABLog("$theDeviceType$ instance: $alias$. " + str(text))

    def writeError(self, instance, text):
        if instance == None:
            self.thePlugin.writeErrorInUABLog(str(text))
        else:
            alias = instance.getAttributeData("DeviceIdentification:Name")
            theDeviceType = instance.getDeviceTypeName()
            self.thePlugin.writeErrorInUABLog("$theDeviceType$ instance: $alias$. " + str(text))

    def writeDebug(self, instance, text):
        if instance == None:
            self.thePlugin.writeDebugInUABLog(str(text))
        else:
            alias = instance.getAttributeData("DeviceIdentification:Name")
            theDeviceType = instance.getDeviceTypeName()
            self.thePlugin.writeDebugInUABLog("$theDeviceType$ instance: $alias$. " + str(text))

    def getWordStatusPattern(self, instance):
        ''' Function returns the pattern of WordStatus formatted as an array 
        e.g. for "a=1,b=2" it returns [["a", "1"], ["b", "2"]] '''
        pattern = instance.getAttributeData("SCADADeviceGraphics:Pattern")
        patternList = pattern.split(",") if (pattern != "" and pattern != "-") else []
        patternList = filter(lambda pair: pair.strip() != "", patternList)  # remove empty elements
        patternList = map(lambda pair: pair.split("="), patternList)  # split k=v
        for pair in patternList:
            while len(pair) < 2:
                pair.append("")
            pair[0] = pair[0].lower().strip()  # format key
            pair[1] = pair[1]                 # format value
        return patternList

    # http://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-in-python
    def is_number(self, s):
        try:
            float(s)
            return True
        except ValueError:
            return False

    # Courtesy of TE-CRG / Tomasz Wolak
    # https://git.cern.ch/web/cryo-controls-lhc-tunnel.git/blob/HEAD:/appskel/app_skel_crg/Resources/SharedTemplates/crgutil.py
    """ ***********************     java <-> python helpers 
    """

    def list_to_java_vector(self, pylist):
        from java.util import Vector
        vector = Vector()
        for i in pylist:
            vector.addElement(i)
        return vector

    def java_vector_to_list(self, vector):
        a_list = []
        for i in vector:
            a_list.append(i)
        return a_list

    def getPLCName(self):
        thePlcDeclaration = self.getPLCDeclaration()
        if thePlcDeclaration == None:
            return ""
        thePlcName = str(thePlcDeclaration.getPLCName().getValue())
        if thePlcName == "":
            thePlcName = str(thePlcDeclaration.getPLCName().getDefaultValue())
        return thePlcName

    def getPLCDeclaration(self):
        thePlcDeclarations = self.thePlugin.getXMLConfig().getPLCDeclarations()
        for thePlcDeclaration in thePlcDeclarations:
            if (thePlcDeclaration is not None):
                return thePlcDeclaration
        return None

    """ Device instance manipulating functions
    """
    # TODO To be removed once default values are defined for all required fields
    required_fields_no_default_values = {"SCADADeviceGraphics:Format": "#.##",
                                         "FEDeviceParameters:Default Value": (lambda instance, attr: "TRUE" if instance.getDeviceTypeName() == "DigitalParameter" else "0"),
                                         "FEDeviceAutoRequests:Default Setpoint Speed:Increase Speed": "0",
                                         "FEDeviceAutoRequests:Default Setpoint Speed:Decrease Speed": "0",
                                         "FEDeviceParameters:Flow Conversion": "0",
                                         "FEDeviceParameters:Volume Conversion": "0",
                                         "FEDeviceParameters:CC0 Max Flow (Unit/time)": "0",
                                         "SCADADeviceGraphics:CurveName:CC0": "unknown default value",
                                         "SCADADeviceGraphics:Flow:Unit": "unknown default value",
                                         "SCADADeviceGraphics:Flow:Format": "#.##",
                                         "SCADADeviceGraphics:Calibration:Unit": "unknown default value",
                                         "SCADADeviceGraphics:Totalizer:Unit": "unknown default value",
                                         "SCADADeviceGraphics:Totalizer:Format": "#.##",
                                         "FEDeviceParameters:ParReg:Fail-Safe": "off/close",
                                         "FEDeviceParameters:ParReg:Manual Restart after Full Stop": "TRUE even if Full Stop still active",
                                         "SCADADriverDataSmoothing:Deadband Type": "No",
                                         "SCADADeviceDataArchiving:Interlock Archiving:Archive Mode": "No",
                                         "FEDeviceParameters:ParReg:PWM Mode": "Bipolar",
                                         "FEDeviceParameters:ParReg:Outputs Maintained": "TRUE",
                                         "SCADADeviceDataArchiving:Archive Mode": "No",
                                         "FEDeviceParameters:Controller Parameters:Scaling Method": "Input Scaling",
                                         "FEDeviceParameters:Controller Parameters:RA": "TRUE",
                                         "SCADADriverDataSmoothing:MV and SP Smoothing:Deadband Type": "No",
                                         "SCADADriverDataSmoothing:Output Smoothing:Deadband Type": "No",
                                         "FEDeviceParameters:ParReg:Full/Empty Animation": "Full/Empty",
                                         "SCADADeviceDataArchiving:Modes:Archive Mode": "No",
                                         "SCADADeviceDataArchiving:Flow:Archive Mode": "No",
                                         "SCADADeviceDataArchiving:Totalizer:Archive Mode": "No",
                                         "SCADADriverDataSmoothing:Flow:Deadband Type": "No",
                                         "SCADADriverDataSmoothing:Totalizer:Deadband Type": "No",
                                         "FEDeviceParameters:ParReg:Switches Configuration": "No End Switches.",
                                         "SCADADeviceGraphics:Widget Type": (lambda instance, attr: attr.getEnumValues().get(0))}

    def transform_instance_to_spare(self, instance, custom_inputs=None):
        """
        Replace an instance's data with the default values for the particular device type,
        or with user's custom input (if given) and return it
        :param instance: The instance to be modified
        :param custom_inputs: Optional custom values for the instance's attributes.
               Example: custom_inputs = {"DeviceDocumentation:Description": "My description", "SCADADeviceDataArchiving:Archive Mode": "No"}
        :return:The given instance transformed into a spare one
        """
        attributes = instance.getDeviceType().getSpecificationAttributes()
        # Check if there are invalid field names in custom input
        for input_field in (custom_inputs if custom_inputs else {}):
            if not instance.doesSpecificationAttributeExist(input_field):
                raise NameError("You are trying to set a value for the field \"" + input_field + "\" that doesn't exist in the device type definition: " + instance.getDeviceTypeName())
        for attribute in attributes:
            attr_name = attribute.getSpecsPath()
            if custom_inputs and (attr_name in custom_inputs):
                attr_value = custom_inputs.get(attr_name)
            elif attr_name == "DeviceIdentification:Name":
                attr_value = instance.getDeviceTypeName() + "_SPARE_ID_" + str(instance.getInstanceNumber())
            elif attr_name == "DeviceDocumentation:Description":
                attr_value = "SPARE"
            elif attribute.isValueRequired():
                if attribute.isDefaultValueDefined():
                    attr_value = attribute.getDefaultValue()
                # TODO This check to be removed once all required fields have specified default values
                else:
                    attr_value = self.required_fields_no_default_values.get(attr_name)(instance, attribute) if callable(self.required_fields_no_default_values.get(attr_name)) else self.required_fields_no_default_values.get(attr_name)
            else:
                attr_value = ""
            instance.setAttributeData(attr_name, attr_value)
        return instance

    def print_inst_data(self, instance):
        """ Prints the instance data on the console."""
        print "\n"
        data = []
        attributes = instance.getDeviceType().getSpecificationAttributes()
        for attribute in attributes:
            data.append(attribute.getSpecsPath() + " --> \"" + instance.getAttributeData(attribute.getSpecsPath()) + "\"")
        data_string = "\n".join(data)
        print data_string
