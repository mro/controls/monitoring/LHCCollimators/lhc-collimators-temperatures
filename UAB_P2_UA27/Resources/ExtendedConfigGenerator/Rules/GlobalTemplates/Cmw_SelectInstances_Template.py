# Jython source file to create DIP Configs and DIP publications
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED


class SelectInstances_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.cmwService = self.thePlugin.getServiceInstance("Cmw")
        self.thePlugin.writeInUABLog("initialize in Jython for SelectInstances.")

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for SelectInstances.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for SelectInstances.")

    def process(self):
        self.thePlugin.writeInUABLog("processInstances in Jython for SelectInstances.")

        configName = "CmwConfig_1"
        domain = "TestDomain"
        project = "TestProject"
        system = "TestSystem"
        fec = "TestFec"
        accelerator = "TestAccelerator"

        # Creates a CMW config. The parameters of the createCMWConfig() method are:
        #   - CMW Config name.
        #   - Domain name
        #   - Project name
        #   - System name
        #   - Front End Computer name
        #   - Accelerator name
        self.cmwService.createCmwConfig(configName, domain, project, system, fec, accelerator)

        # Selects the instances from the SPECS file using the findMatchingInstances methods.
        di = self.theUnicosProject.findMatchingInstances("DigitalInput", "'#DeviceIdentification:Name#'!=''")
        ai = self.theUnicosProject.findMatchingInstances("AnalogInput", "'#DeviceIdentification:Name#'!=''")
        ai2 = self.theUnicosProject.findMatchingInstances("AnalogInput", "'#DeviceIdentification:Name#'!=''")

        # Adds the CMW Publications to the CMW Config.
        # The parameters of the addPublications method() are:
        #   - CMW Config name
        #   - Vector<IDeviceInstance> containing the instances to publish (returned by the findMatchingInstances() method)
        #   - Name of the element to publish (if not provided, the default element will be published)
        self.cmwService.addPublications(configName, di, "StsReg01")
        self.cmwService.addPublications(configName, ai, "PosSt")
        self.cmwService.addPublications(configName, ai2, "ManReg01")

        # Adds the free CMW Publication to the CMW Config.
        # The parameters of the addFreePublications method() are:
        #   - CMW Config name
        #   - Instance name
        #   - Element name
        #   - CMW Device name
        #   - CMW Property
        #   - Direction
        #   - Value
        #   - Time
        self.cmwService.addFreePublication(configName, "Object01", "MyElement", "Device", "Prop", "Out", "long", "long")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for SelectInstances.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for SelectInstances.")
