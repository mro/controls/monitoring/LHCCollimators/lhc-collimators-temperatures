# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate 	#REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin		#REQUIRED
#from research.ch.cern.unicos.types.ai import Instance                 	#REQUIRED
from time import strftime

class AnalogOutputReal_Template(IUnicosTemplate):
   thePlugin = 0
   theUnicosProject = 0   
   
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
	self.thePlugin.writeInUABLog("initialize in Jython for AOR.")

   def check(self):
	self.thePlugin.writeInUABLog("check in Jython for AOR.")

   def begin(self):
	self.thePlugin.writeInUABLog("begin in Jython for AOR.")
	dateAndTime = strftime("%Y-%m-%d %H:%M:%S")	#'2007-03-03 22:14:39'
	
   def process(self, *params):
	theCurrentDeviceType = params[0]
	theDeviceTypeAttributeFamilies = params[1]
	self.thePlugin.writeInUABLog("processInstances in Jython for AOR.")
			
	# General Steps for Optimized objects:
	# 1. Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances) It's called DB_Type
	# 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects
	# 3. Create all the needed Structures from DeviceTypeDefinition.xml created by TCT
	# 4. Create all the need DBs to store the required signal from the object
	# 5. Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance         
	# 6. Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#	 We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.
		
	
	# This method is called on every Instance of the current type by the Code Generation plug -in.
	instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
	Diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
	
	# This method returns all the instances objects from the Specification file.
	theRawInstances = self.thePlugin.getUnicosProject()
	
	# Step 1: Create a instance DB of CPC_FB_Type (only one! Instead of create one DB for device we create only one and we'll reuse this DB for all the instances)
	
	# DB_CREATION
	specsVersion = theRawInstances.getProjectDocumentation().getSpecsVersion()
	self.thePlugin.writeInstanceInfo('''//Specs version used for this generation: $specsVersion$

//Analog Output Real DB Creation file: UNICOS application
''')
	self.thePlugin.writeInstanceInfo('''
DATA_BLOCK DB_AOR CPC_FB_AOR
BEGIN
END_DATA_BLOCK
''')

	
	# Step 3: Create all the needed Structures from the TCT	

	# UDT for DB_AOR_ManRequest
	self.thePlugin.writeInstanceInfo('''
TYPE AOR_ManRequest
TITLE = AOR_ManRequest
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceManualRequests": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isCommVerification = attribute.getIsCommunicated()
				if isCommVerification:
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ attributePrimitiveTypePLC +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE AOR_bin_Status
TITLE = AOR_bin_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					attributePrimitiveType = attribute.getPrimitiveType()
					attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
					self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the evemts
	self.thePlugin.writeInstanceInfo('''
TYPE AOR_event
TITLE = AOR_event
//
// parameters of ANADIG Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommVerification = attribute.getIsCommunicated()
				if isEventAttribute and isCommVerification: 
					attributeName = attribute.getAttributeName()
					self.thePlugin.writeInstanceInfo('''ev'''+attributeName +''' : DWORD;
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# UDT for all the status DBs
	self.thePlugin.writeInstanceInfo('''
TYPE AOR_ana_Status
TITLE = AOR_ana_Status
//
// parameters of Analog Objects
//
AUTHOR: 'UNICOS'
NAME: 'DataType'
FAMILY: 'Base'
STRUCT
''')
	for family in theDeviceTypeAttributeFamilies:
		familyName = family.getAttributeFamilyName()
		if familyName == "FEDeviceOutputs": 
			attributeList = family.getAttribute()
			for attribute in attributeList:
				attributeName = attribute.getAttributeName()
				isEventAttribute = attribute.getIsEventAttribute()
				isCommunicatedAttribute = attribute.getIsCommunicated()
				if isCommunicatedAttribute:
					if (isEventAttribute == None or isEventAttribute == 0): 
						attributeName = attribute.getAttributeName()
						attributePrimitiveType = attribute.getPrimitiveType()
						attributePrimitiveTypePLC = self.thePlugin.getPLCequivalencePrimitiveType(attributePrimitiveType)
						self.thePlugin.writeInstanceInfo(attributeName +''' :'''+ str(attributePrimitiveTypePLC) +''';
''')
	self.thePlugin.writeInstanceInfo('''END_STRUCT
END_TYPE


''')

	# Step 4: Create all the need DBs to store the required signal from the object

	# DB_AOR_ManRequest
	self.thePlugin.writeInstanceInfo('''
// DB_AOR_ManRequest
(*DB for THE MAPPING OF UNICOS objects INPUTS*)
DATA_BLOCK DB_AOR_ManRequest
TITLE = DB_AOR_ManRequest
//
// Contains all Manual Request signals from WinCCOA
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT   
''')
	instanceNb = str(instancesVector.size())
	self.thePlugin.writeInstanceInfo('''    AOR_Requests : ARRAY [1..'''+instanceNb+'''] OF AOR_ManRequest;
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')


	# Step 2. DB_Event creation for this object: it contains all evstsreg signals of UNICOS objects 

	# DB_EVENT
	typeName = theCurrentDeviceType.getDeviceTypeName()
	attributeFamilyList = self.thePlugin.getAttributeFamilyDeviceTypeDefinition(typeName)
	self.thePlugin.writeInstanceInfo('''			

// DB_Event_AOR Creation 
(*DB for evstsreg of  UNICOS objects*)
DATA_BLOCK DB_Event_AOR
TITLE = 'DB_Event'
//
// Contains all evstsreg signals of UNICOS objects type AOR
//
AUTHOR: 'ICE/PLC'
NAME: 'Comm'
FAMILY: 'UNICOS'
STRUCT''')
	InstancesList = theCurrentDeviceType.getAllDeviceTypeInstances()
	NbType = InstancesList.size()
	self.thePlugin.writeInstanceInfo('''   
	Nb_AOR : INT := '''+str(NbType)+''';
	AOR_evstsreg : ARRAY [1..'''+str(NbType)+'''] OF AOR_event;''')
	self.thePlugin.writeInstanceInfo('''
END_STRUCT;
BEGIN
END_DATA_BLOCK

''')

	self.thePlugin.writeInstanceInfo('''
(********************** binary Status of the AORs ***************************)
DATA_BLOCK DB_bin_status_AOR
TITLE = 'DB_bin_status_AOR'
//
// Global binaries status DB of AOR
//
	// List of variables:
''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AOR_bin_Status)
''')

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF AOR_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(********************* old binary Status of the AORs ****************************)
DATA_BLOCK DB_bin_status_AOR_old
TITLE = 'DB_bin_status_AOR_old'
//
// Old Global binary status DB of AOR
//
''')
  
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AOR_bin_Status)
''')

	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   StsReg01: ARRAY [1..'''+str(RecordNumber)+'''] OF AOR_bin_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
(******************* analog Status of the AORs ******************************)
DATA_BLOCK DB_ana_Status_AOR
TITLE = 'DB_ana_status_AOR'
//
// Global analog status DB of AOR
//
// List of variables:
''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AOR_ana_Status)
''')	
		
	self.thePlugin.writeInstanceInfo('''
AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF AOR_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
// -----------------------------------------------------------------------------------------------------
(*old analog status of the AORs************************************************)
DATA_BLOCK DB_ana_Status_AOR_old
TITLE = 'DB_ana_Status_AOR_old'
//
// old Global Analog  status DB of AOR
//
  // List of variables:
''')
	
	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		if (instance is not None):
			self.thePlugin.writeInstanceInfo('''
//  ['''+str(RecordNumber)+''']    $Name$      (AOR_ana_Status)
''')		

	self.thePlugin.writeInstanceInfo('''

AUTHOR: 'UNICOS'
NAME: 'Status'
FAMILY: 'status'
STRUCT
   status: ARRAY [1..'''+str(RecordNumber)+'''] OF AOR_ana_Status;
END_STRUCT
BEGIN
END_DATA_BLOCK
	''')
	
	# Step 5: Create a FB to optimize the Object (FB_Type_all) with the Structure created in the Baseline (CPC_DB_Type) for each instance
	
	self.thePlugin.writeInstanceInfo('''
(************************** EXEC Of AORs ***********************)
FUNCTION_BLOCK FB_AOR_all
TITLE = 'FB_AOR_all'
//
// Call the AOR treatment
//
AUTHOR: 'UNICOS'
NAME: 'Call AOR'
FAMILY: 'AOR'

VAR
  // Static Variables
  AOR_SET: STRUCT
''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		# Diagnostic = instance.getAttributeData("FEDeviceCommunication:FEChannel:InterfaceParam2")
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		self.thePlugin.writeInstanceInfo('''
	$Name$       : CPC_DB_AOR;
''')
			
	self.thePlugin.writeInstanceInfo('''
   END_STRUCT;
  // Different variable view declaration
  AOR AT AOR_SET: ARRAY[1..'''+str(RecordNumber)+'''] OF CPC_DB_AOR;
  
  // Support variables
  old_status : DWORD;
  I: INT;
END_VAR
  FOR I:=1 TO '''+str(RecordNumber)+''' DO
   	
	// status
    old_status := DB_bin_status_AOR.StsReg01[I].StsReg01;
    old_status := ROR(IN:=old_status, N:=16);
	
	// Calls the Baseline function
	CPC_FB_AOR.DB_AOR(
		Manreg01 :=  DB_AOR_ManRequest.AOR_Requests[I].Manreg01    // set by WinCCOA in the DB_AOR
		,MposR :=  DB_AOR_ManRequest.AOR_Requests[I].MposR          // set by WinCCOA in the DB_AOR           
		,StsReg01 := DB_bin_status_AOR.StsReg01[I].StsReg01
		,Perst :=  AOR[I]  
	);

	// call the IO_ACCESS_AOR function
	IF (AOR[I].FEType <> 0) THEN
		IO_ACCESS_AOR(PQWDef := AOR[I].PqwDef,
			  FEType := AOR[I].FEType,
			  InterfaceParam1 := AOR[I].DBNum, 
			  InterfaceParam2 := AOR[I].DBpos, 
			  InterfaceParam3 := AOR[I].DBnumIOerror, 
			  InterfaceParam4 := AOR[I].DBposIOerror,
			  InterfaceParam5 := AOR[I].DBbitIOerror,
			  OutOV := AOR[I].OutOV,
			  IOError := AOR[I].IoErrorW,
			  ProfibusIOError := AOR[I].IoError);

	END_IF;	   
			  
	// Status
	DB_ana_Status_AOR.status[I].PosSt := AOR[I].PosSt;
	DB_ana_Status_AOR.status[I].AuPosRSt := AOR[I].AuPosRSt;
	
	// Event
	DB_Event_AOR.AOR_evstsreg[I].evstsreg01 := old_status OR DB_bin_status_AOR.StsReg01[I].StsReg01;
	
  END_FOR;
END_FUNCTION_BLOCK
  ''')

	# Step 6: Create a instance DB_Type_all from FB_Type_all where we can write config and parameters for the Front-End
	#		  We call the Function located in the Baseline with the FE Encoding Type and the Interface Parameters.

	self.thePlugin.writeInstanceInfo('''	
(******************* DB instance AOR ************************)
DATA_BLOCK DB_AOR_all  FB_AOR_all
//
// Instance DB for the whole AOR devices
//
BEGIN
''')

	RecordNumber = 0
	for instance in instancesVector :
		# Now looping on each device instance.
		# 1 Building shortcuts
		# 2 Write code to output
		RecordNumber = RecordNumber + 1
		Name = instance.getAttributeData("DeviceIdentification:Name")
		PLCChannel = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1")
		PMaxRan = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max"))
		PMinRan= self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min"))
		PMaxRaw = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Max"))
		PMinRaw = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Min"))
		FEType = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
		if (FEType.strip() == ""):
			FEType = "0"
		DBnum = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
		DBpos = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
		DBnumIOerror = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam3").strip().lower()
		DBposIOerror = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam4").strip().lower()
		DBbitIOerror = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5").strip().lower()	
		if DBnum.strip()<>"":
			if DBnum[0] == 'p':
				PQWDEF = "TRUE"
			else:
				PQWDEF = "FALSE"
		else:
			PQWDEF = "FALSE"
		self.thePlugin.writeInstanceInfo('''
		
AOR_SET.$Name$.PMaxRan := '''+str(PMaxRan)+''';
AOR_SET.$Name$.PMinRan := '''+str(PMinRan)+''';
AOR_SET.$Name$.PMaxRaw := '''+str(PMaxRaw)+''';
AOR_SET.$Name$.PMinRaw := '''+str(PMinRaw)+''';
AOR_SET.$Name$.index := '''+str(RecordNumber)+''';
AOR_SET.$Name$.FEType := '''+str(FEType)+''';
AOR_SET.$Name$.PQWDef:=$PQWDEF$;
''')
		#if (FEType == ""):
		#	self.thePlugin.writeWarningInUABLog("AOR instance: $Name$. Undefined Type, FEType = 0 is taken.")
		if (FEType == "101"):
			if DBnum[0:2] == "db":
				DBnum = DBnum[2:]
			if DBpos[0:2] ==  "db":
				DBpos = DBpos[3:]
			self.thePlugin.writeInstanceInfo('''
AOR_SET.$Name$.DBnum:=$DBnum$;
AOR_SET.$Name$.DBpos:=$DBpos$;
	 ''')
		if ((FEType == "102") or (FEType == "103")):
			if DBnum[0:2] == "db":
				DBnum = DBnum[2:]
			if DBpos[0:2] ==  "db":
				DBpos = DBpos[3:]
			if DBnumIOerror[0:2] == "db":
				DBnumIOerror = DBnumIOerror[2:]
			if DBposIOerror[0:2] ==  "db":
				DBposIOerror = DBposIOerror[3:]
			self.thePlugin.writeInstanceInfo('''
AOR_SET.$Name$.DBnum:=$DBnum$;
AOR_SET.$Name$.DBpos:=$DBpos$;
AOR_SET.$Name$.DBnumIOerror:=$DBnumIOerror$;
AOR_SET.$Name$.DBposIOerror:=$DBposIOerror$;
AOR_SET.$Name$.DBbitIOerror:=$DBbitIOerror$;
	 ''')
	 
		if (FEType == "201"):
			if DBnum[0] == "p":
				DBnum = DBnum[3:]
			else:
				DBnum = DBnum[2:]
			self.thePlugin.writeInstanceInfo('''
AOR_SET.$Name$.DBnum:=$DBnum$; // PA valve address IW$DBnum$
	 ''')
	
		if (FEType == "205"):
			if DBnum[0] == "p":
				DBnum = DBnum[3:]
			else:
				DBnum = DBnum[2:]
			self.thePlugin.writeInstanceInfo('''
AOR_SET.$Name$.DBnum:=$DBnum$; // Write Profibus DP device with 4 bytes consistency check (4 bytes for value only, IO error evaluation)
	 ''')
	
	self.thePlugin.writeInstanceInfo('''
END_DATA_BLOCK''')

	#Creation of a FC even for the optimized objects in order to call it in the OB1
	#This avoid the too many symbols problem of the OB1
	#The code is ready for all the object in case we need to split in several DBs one day. Note that for the moment,
	#this has sense only for the AIR and the DI where the split is working.
	NumberOfInstances = instancesVector.size()
	LimitSizeOfInstances = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", theCurrentDeviceType.getDeviceTypeName()))
	nameRepresentation = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", theCurrentDeviceType.getDeviceTypeName())
	self.thePlugin.writeInstanceInfo('''
FUNCTION FC_$nameRepresentation$ : VOID
    FB_$nameRepresentation$_all.DB_$nameRepresentation$_all();''')
	if (NumberOfInstances > LimitSizeOfInstances):
		self.thePlugin.writeInstanceInfo('''
    FB_$nameRepresentation$_all2.DB_$nameRepresentation$_all2();    // Too many '''+str(NumberOfInstances)+''' Need a second FB_$nameRepresentation$_All2''')
	self.thePlugin.writeInstanceInfo('''
END_FUNCTION
''')

   def end(self):
	self.thePlugin.writeInUABLog("end in Jython for AOR.")

   def shutdown(self):
	self.thePlugin.writeInUABLog("shutdown in Jython for AOR.")
		
